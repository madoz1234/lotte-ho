<?php

return array(


    'pdf' => array(
        'enabled' => true,
        // 'binary'  => 'C:/wkhtmltopdf/bin/wkhtmltopdf.exe',
        'binary'  => env('WKPDF_BINARY', false),
        'timeout' => 3600,
        'options' => array(
            'footer-center' => 'Page [page] of [toPage]',
            'footer-font-size' => 7,
            'footer-left' => 'TMUK'
        ),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        // 'binary'  => 'C:/wkhtmltopdf/bin/wkhtmltopdf.exe',
        'binary'  => env('WKPDF_BINARY', false),
        'timeout' => false,        
        'timeout' => 3600,
        'options' => array(),
        'env'     => array(),
    ),


);
