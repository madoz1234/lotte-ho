@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[kode]" placeholder="Kode Area" type="text">
	</div>
	<div class="field">
		<input name="filter[area]" placeholder="Area" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('js-filters')
    d.kode = $("input[name='filter[kode]']").val();
    d.area = $("input[name='filter[area]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		kode: ['empty', 'number', 'maxLength[5]'],
		nama: ['empty', 'maxLength[30]'],
	};
</script>
@endsection