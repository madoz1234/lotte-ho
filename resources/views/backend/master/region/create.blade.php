<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Region</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="field">
			<label>Kode Region</label>
			<input name="kode" placeholder="Kode Region" type="text" value="{{ $code }}" readonly>
		</div>
		<div class="field">
			<label>Nama Region</label>
			<input name="area" placeholder="Region" type="text" value="{{ old('area') }}">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>