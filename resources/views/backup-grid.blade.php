@extends('layouts.scaffold')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.semanticui.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.semanticui.min.js') }}"></script>
@append

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable({
	    	lengthChange: false,
	    	filter:false,
	    	language: {
                url: "{{ asset('plugins/datatables/Indonesian.json') }}"
            },
            drawCallback: function( settings ) {
            	$('button[data-content]')
				  .popup({
				    hoverable: true,
				    position : 'top center',
				    delay: {
				      show: 300,
				      hide: 800
				    }
				  })
				;
            }
	    });

	    $('#formModal')
		  .modal('attach events', '.add.button', 'show')
		;

		$('button[data-content]')
		  .popup({
		    hoverable: true,
		    position : 'top center',
		    delay: {
		      show: 300,
		      hide: 800
		    }
		  })
		;
	} );
</script>
@append

@section('content')
<div class="ui breadcrumb">
	<a href="#" class="section">Menu Utama</a>
	<i class="right chevron icon divider"></i>
	<a href="#" class="section">Data Toko</a>
	<i class="right chevron icon divider"></i>
	<div class="active section">Region (Area)</div>
</div>
<h2 class="ui header">
  <div class="content">
    Region (Area)
    <div class="sub header">Data Area Operasional</div>
  </div>
</h2>
<div class="ui grid">
	<div class="sixteen wide column main-content">
	    <div class="ui segments">
			<div class="ui segment">
				<form class="ui form" style="margin-bottom: -25px">
	  				<div class="fields">
						<div class="field">
							<input name="filter[kode]" placeholder="Kode Area" type="text">
						</div>
						<div class="field">
							<input name="filter[area]" placeholder="Area" type="text">
						</div>
						<button class="ui teal icon button" data-content="Cari Data">
							<i class="search icon"></i>
						</button>
						<button type="reset" class="ui icon button" data-content="Bersihkan Pencarian">
							<i class="refresh icon"></i>
						</button>
						<div style="margin-left: auto">
							<button type="button" class="ui blue add button">
								<i class="plus icon"></i>
								Tambah Data
							</button>
							<button type="button" class="ui green button">
								<i class="file excel outline icon"></i>
								Export Excel
							</button>
						</div>
					</div>
				</form>
	            <table id="example" class="ui celled compact red table" width="100%" cellspacing="0">
			        <thead>
			            <tr>
			                <th class="right aligned">#</th>
			                <th>Kode Area</th>
			                <th>Area</th>
			                <th></th>
			            </tr>
			        </thead>
			        <tbody>
			            <tr>
			                <td class="right aligned">1.</td>
			                <td>00001</td>
			                <td>Jabodetabek</td>
			                <td class="center aligned">
			                	<button class="ui mini orange icon button" data-content="Ubah Data"><i class="edit icon"></i></button>
			                	<button class="ui mini red icon button" data-content="Hapus Data"><i class="trash icon"></i></button>
			                </td>
			            </tr>
			            <tr>
			                <td class="right aligned">2.</td>
			                <td>00002</td>
			                <td>Daerah Istimewa Yogyakarta</td>
			                <td class="center aligned">
			                	<button class="ui mini orange icon button" data-content="Ubah Data"><i class="edit icon"></i></button>
			                	<button class="ui mini red icon button" data-content="Hapus Data"><i class="trash icon"></i></button>
			                </td>
			            </tr>
			            <tr>
			                <td class="right aligned">3.</td>
			                <td>00003</td>
			                <td>Bandung</td>
			                <td class="center aligned">
			                	<button class="ui mini orange icon button" data-content="Ubah Data"><i class="edit icon"></i></button>
			                	<button class="ui mini red icon button" data-content="Hapus Data"><i class="trash icon"></i></button>
			                </td>
			            </tr>
			            <tr>
			                <td class="right aligned">4.</td>
			                <td>00004</td>
			                <td>Bekasi</td>
			                <td class="center aligned">
			                	<button class="ui mini orange icon button" data-content="Ubah Data"><i class="edit icon"></i></button>
			                	<button class="ui mini red icon button" data-content="Hapus Data"><i class="trash icon"></i></button>
			                </td>
			            </tr>
			            <tr>
			                <td class="right aligned">5.</td>
			                <td>00005</td>
			                <td>Bogor</td>
			                <td class="center aligned">
			                	<button class="ui mini orange icon button" data-content="Ubah Data"><i class="edit icon"></i></button>
			                	<button class="ui mini red icon button" data-content="Hapus Data"><i class="trash icon"></i></button>
			                </td>
			            </tr>
			        </tbody>
			    </table>
	        </div>
	    </div>
	</div>
</div>
@endsection

@section('modals')
<div class="ui mini modal" id="formModal">
	<div class="ui inverted dimmer">
		<div class="ui text loader">Loading</div>
	</div>
	<div class="header">Tambah Data Region (Area)</div>
	<div class="content">
		<form class="ui form">
			<div class="field">
				<label>Kode Area</label>
				<input name="code" placeholder="Kode Area" type="text">
			</div>
			<div class="field">
				<label>Nama Area</label>
				<input name="area" placeholder="Area" type="text">
			</div>
		</form>
	</div>
	<div class="actions">
		<div class="ui negative button">
			Batal
		</div>
		<div class="ui positive right labeled icon button">
			Simpan
			<i class="checkmark icon"></i>
		</div>
	</div>
</div>
@append