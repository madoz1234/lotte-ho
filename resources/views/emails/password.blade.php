<!-- resources/views/emails/password.blade.php -->

{{-- Click here to reset your password: {{ url('password/reset/'.$token) }} --}}
<!DOCTYPE html>
<html lang="it"><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>Action Item Emails :</title><!--


COLORE INTENSE  #9C010F
COLORE LIGHT #EDE8DA

TESTO LIGHT #3F3D33
TESTO INTENSE #ffffff


--><meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<style type="text/css">
	* {
		margin: 0;
		padding: 0;
	}
	body {
		font: 14px/1.5 sans-serif;
		background: #ffffff;
		color: #555;
	}
	a {
		color: #0086cb;
		text-decoration: none;
	}
	.container {
		padding: 2em;
	}
	header {
		text-align: center;
		background: #f8f8ff;
		color: #fff;
		position: relative;
	}
	header img {
		vertical-align: middle;
    position: absolute;
    top: 50%;
    left: 2em;
    transform: translateY(-50%);
	}
	header h1 {
		font-size: 1.5em;
		margin-top: 0;
	}
	header small {
		display: block;
		font-size: 12px;
		font-weight: normal;
	}
	.btn {
		display: inline-block;
		padding: 10px 25px;
		font-size: 1.2em;
		background: #0086cb;
		color: #fff;
		text-decoration: none;
		margin-bottom: 20px;
	}
	p {
		margin-bottom: 20px;
	}
	footer {
		background: #f8f8ff;
		color: #fff;
		display: flex;
	}
	footer .container {
		width: 100%;
	}
	footer .one-two {
		display: block;
		width: 50%;
		float: left;
	}
	footer:after {
		content: " ";
		display: block;
		clear: both;
	}
</style>
</head>
<body>
	<header>
		<div class="container">
			<img src="http://immbo-ho.lottegrosir.co.id/img/auth/LG.png" alt="logo">
		</div>
	</header>
	<section>
		<table class="m_-1509637545618309675inner-body" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;margin:0 auto;padding:0;width:570px">
			<tbody><tr>
				<td class="m_-1509637545618309675content-cell" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:35px">
					<h1 style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#2f3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left">Hello!</h1>
					<p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">Anda menerima email ini karena kami menerima permintaan pengaturan ulang kata sandi untuk akun Anda.</p>
					<table class="m_-1509637545618309675action" align="center" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;margin:30px auto;padding:0;text-align:center;width:100%"><tbody><tr>
						<td align="center" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box"><tbody><tr>
								<td align="center" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
									<table border="0" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box"><tbody><tr>
										<td style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
											<a href="{{ url('password/reset/'.$token) }}" class="m_-1509637545618309675button m_-1509637545618309675button-blue" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1" target="_blank" data-saferedirecturl="{{ url('password/reset/'.$token) }}">Reset Password</a>
										</td>
									</tr></tbody></table>
								</td>
							</tr></tbody></table>
						</td>
					</tr></tbody></table>
					<p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">Jika Anda tidak meminta pengaturan ulang kata sandi, tidak ada tindakan lebih lanjut yang diperlukan.</p>

					<p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">Regards,<br>LotteGrosir</p>

					<table class="m_-1509637545618309675subcopy" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-top:1px solid #edeff2;margin-top:25px;padding-top:25px"><tbody><tr>
						<td style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
							<p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;line-height:1.5em;margin-top:0;text-align:left;font-size:12px">If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below
								into your web browser: <a href="{{ url('password/reset/'.$token) }}">{{ url('password/reset/'.$token) }}</a></p>
								</td>
							</tr></tbody></table>
						</td>
					</tr>
				</tbody></table>
	</section>
	<footer>
		<div class="container">
			{{-- LotteGrosir --}}
		</div>
	</footer>
</body>
</html>