 <!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #cecece;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
                background: #1C6BA0;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }

            a{
                color: #cecece;
                text-decoration: none;
                font-size: 2em;
            }
            a:hover{
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Oops.. Halaman tidak ditemukan.</div>
                <a href="{{ \URL::previous() }}">< Go Back </a>
            </div>
        </div>
    </body>
</html>
