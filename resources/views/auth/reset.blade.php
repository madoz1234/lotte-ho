@extends('layouts.auth')

@section('styles')
<style>
input {
    border-radius: 0!important
}
#formContent {
    padding-top: 1rem;
}
#mainForm {
    margin-top: 5rem;
    /*padding: 0;*/
    background: rgba(255,255,255,.25);
    border: none;
    box-shadow: none;
}
.no-padding {
    padding: 0!important
}
.no-padding-form {
    padding: 0 0 0 .0rem!important;
}
.image-fit {
    width: 100%;
    height: 100%;
    object-fit: cover;
}
.small-margin-top {
    display: block;
    margin-top: .5rem!important
}
</style>
@append

@section('content')

<div class="ui top aligned center aligned grid" id="formContent">
    <div class="column">
        <h2 class="ui red image header">
            <img src="{{ asset('img/auth/LVC.png') }}" style="margin-top:0; width:100%">
            <img src="{{ asset('img/auth/LG.png') }}" style="margin-top:0; width:100%">
        </h2>
        <form class="ui large form" role="form" method="POST" action="{{ url('/password/reset') }}">
            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="ui segment" id="mainForm">
                <div class="ui grid" style="margin: 0">
                    <div class="sixteen wide column no-padding-form">
                        <div class="ui small form field">
                            <div class="ui left icon input">
                                <i class="user icon"></i>
                                <input type="email" placeholder="E-mail address" name="email" value="{{ old('email') }}" required="">
                            </div>
                        </div>
                        <div class="ui small form field">
                            <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password" placeholder="Password" required="">
                            </div>
                        </div>
                        <div class="ui small form field">
                            <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password_confirmation" placeholder="Konfirmasi Password" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="ui fluid large blue submit button small-margin-top">Ganti Password</button>
                <span class="small-margin-top"><a class="text-white" href="{{ url('/auth/login') }}">Login</a></span>
                <span class="small-margin-top"><a class="text-white" href="{{ url('/password/email') }}">Reset Password</a></span>
            </div>

            <span class="text-white">New BIZ, IT 및 Pragma에 의해 권한을 부여 받은</span>

        </form>
        @if (count($errors) > 0)
        <div class="ui icon message warning">
            {{-- <i class="close icon"></i> --}}
            <i class="remove icon"></i>
            <div class="content">
                <div class="alert alert-warning center">
                    <p>
                        @foreach ($errors->all() as $error)
                        {{ $error }}
                        @endforeach
                    </p>
                </div>
            </div>
        </div>
        @endif
    {{-- </div> --}}
</div>
</div>
@endsection
