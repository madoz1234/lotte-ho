<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 12px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 12px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 12px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; padding: 1mm; font-size: 12px;}
table.page_content_1 {width: 100%; border-collapse: collapse; font-size: 12px;}

.border{
    border: solid 0.3mm #000000;
}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}
.header{
    height: 25px;text-align: center; background-color:#DDDDFF;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
-->
</style>
<page backtop="52mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="text_header">
                    <p class="title tittleMargin">REMITTANCE</p>
                    <barcode dimension="1D" type="C128" value="0600700018-PYR-20171226069" label="label"></barcode>
                </td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center" style="border-bottom: solid 0.5mm #000000;">
            <tr>
                <td style="width: 65%;"></td>
                <td style="width: 15%;">Date</td>
                <td style="width: 20%">31/05/2017</td>
            </tr>
            <tr>
                <td style="width: 65%;"></td>
                <td style="width: 15%;">Remittance No</td>
                <td style="width: 20%">0600700018-PYR-20171226069</td>
            </tr>
        </table>
    </page_header>

    <table class="page_content" border="0" align="center">
      <tr>
          <th style="width:50%">Order To</th>
          <th style="width:50%">Charge To</th>
      </tr>
      <tr>
          <td style="width: 50%">LOTTE GROSIR BANJARMASIN JL. ACHMAD YANI KM. 4,5</td>
          <td style="width: 50%">&nbsp;</td>
      </tr>
    </table>
    <table class="page_content_1" align="center" border="0">
       <thead>
        <tr>
          <th class="header" style="width:20%;">Customer's Reference</th>
          <th class="header" style="width:20%;">Type</th>
          <th class="header" style="width:20%;">Your VAT no.</th>
          <th class="header" style="width:20%;">Supplier's Reference</th>
          <th class="header" style="width:20%;">Due Date</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width:20%; text-align: center;">&nbsp;</td>
          <td style="width:20%; text-align: center;">Pembayaran ke Pemasok</td>
          <td style="width:20%; text-align: center;">&nbsp;</td>
          <td style="width:20%; text-align: center;">&nbsp;</td>
          <td style="width:20%; text-align: center;">31/05/2017</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table class="page_content" border="0" align="center">
      <tr>
          <td style="width:100%"><i>Payment Terms:</i></td>
      </tr>
    </table>
    <br>
    <table class="page_content_1" border="0" align="center">
      <thead>
        <tr>
          <th class="header" style="width: 15%;">Trans Type</th>
          <th class="header" style="width: 10%;">#</th>
          <th class="header" style="width: 15%;">Date</th>
          <th class="header" style="width: 15%;">Due Date</th>
          <th class="header" style="width: 15%;">Total Amount</th>
          <th class="header" style="width: 15%;">Left to Allocate</th>
          <th class="header" style="width: 15%;">This Allocation</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="6">As advance / full / part / payment towards:</td>
          <td style="width: 15%;text-align: right;"></td>
        </tr>
        <tr>
          <td height="100px" colspan="6">Prima Rasa Utama-Memo:03/tmuk/intan/2017</td>
          <td height="100px" style="width: 15%;text-align: right;"></td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
          <td colspan="2">Total Allocated</td>
          <td style="text-align: right;">0.00</td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
          <td colspan="2">Left to Allocate</td>
          <td style="text-align: right;">100,002,523.00</td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
          <td colspan="2">Discount</td>
          <td style="text-align: right;">-2,523.00</td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
          <th colspan="2">TOTAL REMITTANCE</th>
          <th style="text-align: right;">100,000,000.00</th>
        </tr>
        <tr>
          <td colspan="5" align="center">IDR: One Hundred Million and 00/100</td>
          <td style="text-align: center;">&nbsp;</td>
          <td style="text-align: center;">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table class="page_content" border="0" align="center">
      <tr>
          <td style="width:100%; text-align: center;"><i>All amounts stated in - IDR</i></td>
      </tr>
      <tr>
          <td style="width:100%; text-align: center;"><i>Bank: Escrow BRI - Etokokoe, Bank Account: 052201000295304</i></td>
      </tr>
    </table>

    <end_last_page end_height="37mm">
        {{-- <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    PIC Pengambilan Barang
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Di Serahkan Kepada
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
            </tr>
        </table> --}}
    </end_last_page>
</page>