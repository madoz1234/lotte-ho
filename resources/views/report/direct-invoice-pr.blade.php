<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
-->
</style>
<page backtop="70mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" width="" align="center">
            <tr>
                <td style="width: 480px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="150" style="margin-top: 10px;">
                </td>
                <td style="width: 480px;" id="text_header">
                    <p class="title" style="font-size: 21px;">DIRECT INVOICE PR</p>
                    <barcode dimension="1D" type="C128" value="{{ $record->nomor_pyr or "" }}" label="label" style="width:78mm; height:10mm;"></barcode>
                </td>
            </tr>
            <tr>
                <td>Halaman [[page_cu]] dari [[page_nb]]</td>
                <td></td>
            </tr>
            <tr>
                <td><b>User</b> : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }})</td>
                <td></td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 25%">NO PYR :</td>
                <td style="width: 10%">Tanggal PYR :</td>
                <td style="width: 25%">LSI :</td>
                <td style="width: 25%">TMUK :</td>
                <td style="width: 15%">Jumlah Produk :</td>
            </tr>
            <tr>
                <td style="width: 25%">{{ $record->nomor_pyr or "" }}</td>
                <td style="width: 10%">{{ $record->tgl_buat or "" }}</td>
                <td style="width: 25%">{{ $record->tmuk->lsi->kode }} - {{ $record->tmuk->lsi->nama }}</td>
                <td style="width: 25%">{{ $record->tmuk_kode }} - {{ $record->tmuk->nama }}</td>
                <td style="width: 15%">{{ isset($record->detailPyr) ? count($record->detailPyr) : '' }}</td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="text-align:center; width:10%">No.</th>
                <th style="text-align:center; width:20%">Kode Produk</th>
                <th style="text-align:center; width:30%">Nama Produk</th>
                <th style="text-align:center; width:20%">Quantity</th>
                <th style="text-align:center; width:20%">Harga</th>
            </tr>
        </table>
    </page_header>
    <br>

    <table class="page_content" border="0" align="center">
        <col style="text-align:center; width:10%">
        <col style="text-align:center; width:20%">
        <col style="text-align:center; width:30%">
        <col style="text-align:center; width:20%">
        <col style="text-align:center; width:20%">
        <?php $result = array(); ?>
        @foreach ($record->detailPyr as $key => $row)
            <tr>
                <td style="text-align:center; width:10%">{{ $key+1 }}</td>
                <td style="text-align:center; width:20%">{{ $row->produk_kode }}</td>
                <td style="text-align:center; width:30%">{{ $row->produks->nama}}</td>
                <td style="text-align:center; width:20%">{{ $row->qty }}</td>
                <td style="text-align:center; width:20%">{{ $row->price }}</td>
            </tr>
        @endforeach
    </table>

    <end_last_page end_height="37mm">
        <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    PIC Pengambilan Barang
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Diserahkan
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
            </tr>
        </table>
    </end_last_page>
</page>