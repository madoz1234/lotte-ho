<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: ;
}
-->
</style>
<page backtop="60mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="text_header">
                    <p class="title tittleMargin">Rekap Service Level (SL) LSI Ke TMUK</p>
                    {{-- <barcode dimension="1D" type="C128" value="lt-201803240600700018014" label="label" style="width:60mm; height:10mm;"></barcode> --}}
                </td>
            </tr>
            <tr>
                <td>Halaman [[page_cu]] dari [[page_nb]]</td>
            </tr>
            <tr>
                <td>User : Allan Asfar (28/03/2018 13:06)</td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center" style="border-bottom: solid 0.3mm #000000;">
            <tr>
                <th style="width: 40%">No PO:</th>
                <th style="width: 60%">Supplier:</th>
            </tr>
            <tr>
                <td style="width: 40%"></td>
                <td style="width: 60%">-</td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="width:14%; text-align: center;">TMUK</th>
                <th style="width:30%; text-align: center;">Item/Category</th>
                <th style="width:14%; text-align: center;">PR Quantity</th>
                <th style="width:14%; text-align: center;">PO Quantity</th>
                <th style="width:14%; text-align: center;">UOM</th>
                <th style="width:14%; text-align: center;">Service Level (%)</th>
            </tr>
        </table>
    </page_header>
    

    <end_last_page end_height="37mm">
        <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Return By:
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    {{-- <div style="border: 1px solid #000; width: 200px;height:80px;"></div> --}}
                </td>
            </tr>
        </table>
    </end_last_page>
</page>