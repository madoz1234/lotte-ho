<style type="text/Css">
/*design table 1*/
/*.table1 {
    font-family: sans-serif;
    color: #000000;
    border-collapse: collapse;
}
 
.table1, th, td {
    border: 1px solid #999;
    padding: 8px 20px;
}*/

/*td{
    border: 1px solid #000;
}*/

</style>
@foreach ($record as $records)
<page style="font-size: 14px;">
    <table style="border: 1px solid #000; margin: 10px; padding:15px;">
        <tr>
            <td style="padding-left: 20px; text-align: center; border: 0px solid #000; width: 990px; font-weight:bold; font-size:25pt; height: 200px">
                       {{ $records['tmuk_kode'] }}<br>{{ $records['tmuk_nama'] }}
            </td>
        </tr>
        <tr>
            <td style="padding-left: 50px;">
                <table style="border: solid 0px #440000; width: 80%; margin-bottom: 50px;" cellspacing="0">
                    <tr>
                        <td style="text-align: center; width: 57%"><span style="font-weight: bold; font-size: 20pt;">No PR : </span></td>
                        <td style="text-align: center; width: 57%"><span style="font-weight: bold; font-size: 20pt;">No PO : </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: center; width: 57%"><span style="font-size: 20pt;">{{ $records['nomor_pr'] }}</span></td>
                        <td style="text-align: center; width: 57%"><span style="font-size: 20pt;">{{ $records['nomor_po'] }}</span></td>
                    </tr>
                </table>

                <table style="border: solid 0px #440000; width: 80%;" cellspacing="0">
                    <tr>
                        <td style="text-align: center; width: 57%"><span style="font-weight: bold; font-size: 20pt;">No Container : </span></td>
                        <td style="text-align: center; width: 57%"><span style="font-weight: bold; font-size: 20pt;">Toko Lotte : </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: center; width: 57%"><span style="font-size: 20pt;">{{ $records['nomor_cl'] }}</span></td>
                        <td style="text-align: center; width: 57%"><span style="font-size: 20pt;">{{ $records['lsi_nama'] }}</span></td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td style="height:100px"></td>
        </tr>
        <tr>
            <td style="text-align: center;">
               <barcode style="width:300px; height:60px; font-size: 4mm; " dimension="1D" type="C128" value="{{ $records['nomor_cl'] }}" label="label"></barcode>
            </td>
        </tr>
    </table>
</page>
@endforeach