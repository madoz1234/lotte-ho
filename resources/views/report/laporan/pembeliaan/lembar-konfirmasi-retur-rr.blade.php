<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 93%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: ;
}
-->
</style>
@foreach ($record as $records)

<page backtop="60mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
        <td id="" style="; width: 90px; height: 5px;">
            <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
        </td>
        <td id="text_header" style=" width: 900px; height: 5px; text-align: center;">
            <p class="title" style=""><h3>Lembar Konfirmasi Retur (RR)</h3>
                <hr style="border: #F90606; margin-top: -6px;" >
            </p>

        </td>
    </tr>
    <tr>
        <td><br></td>
        <td><br></td>
    </tr>
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    <tr>
        <td>Tahun Fiskal</td>
        <td>:  (Active)</td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>:  </td>
    </tr>

    <tr>
        <td>Region</td>
        <td>:  </td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>:  </td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: </td>
    </tr>
            {{-- <tr>
                <td>User : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }})</td>
            </tr> --}}
        </table>
        <table class="page_header" border="0" align="center" style="border-bottom: solid 0.3mm #000000;">
            <tr>
                <th style="width: 30%">RR Number:</th>
                <th style="width: 20%">Date:</th>
                <th style="width: 30%">TMUK :</th>
                <th style="width: 20%">LSI :</th>
            </tr>
            <tr>
                <td style="width: 30%">{{ $records['nomor_retur'] }}</td>
                <td style="width: 20%">{{ $records['date'] }}</td>
                <td style="width: 30%">{{ $records['tmuk_kode'] }} - {{ $records['tmuk_nama'] }}</td>
                <td style="width: 20%">{{ $records['lsi_kode'] }} - {{ $records['lsi_nama'] }}</td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="width:10%; text-align: center;">Product Code</th>
                <th style="width:10%; text-align: center;">Barcode</th>
                <th style="width:20%; text-align: center;">Product Description</th>
                <th style="width:10%; text-align: center;">Selling Unit</th>
                <th style="width:10%; text-align: center;">Return Request Qty</th>
                <th style="width:10%; text-align: center;">Price</th>
                <th style="width:10%; text-align: center;">Total Price</th>
                <th style="width:10%; text-align: center;">Reason</th>
                <th style="width:10%; text-align: center;">Return Confirmed Qty</th>
            </tr>
        </table>
        <table class="page_content" border="0" align="center">
            <tr>
                <td style="width:10%; text-align: center;">{{ $records['produk_kode'] or '' }}</td>
                <td style="width:10%; text-align: center;">{{ $records['barcode'] or '' }}</td>
                <td style="width:20%; text-align: center;">{{ $records['produk_nama'] or '' }}</td>
                <td style="width:10%; text-align: center;">{{ $records['produk_kode'] or '' }}</td>
                <td style="width:10%; text-align: center;">{{ $records['qty'] or '' }}</td>
                <td style="width:10%; text-align: center;"></td>
                <td style="width:10%; text-align: center;"></td>
                <td style="width:10%; text-align: center;">{{ $records['reason'] or '' }}</td>
                <td style="width:10%; text-align: center;">{{ $records['qty_retur'] or '' }}</td>
            </tr>
        </table>
    </page_header>

    <end_last_page end_height="37mm">
        <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Return By:
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    {{-- <div style="border: 1px solid #000; width: 200px;height:80px;"></div> --}}
                </td>
            </tr>
        </table>
    </end_last_page>
</page>
@endforeach