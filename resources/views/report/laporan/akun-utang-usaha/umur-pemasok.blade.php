<style type="text/css">
    <!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#FFFFFF; border-top: solid 0.0mm #000000; border-bottom: solid 0.0mm #000000;}
table.page_content {border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    /*padding: 10;*/
    /*margin: 2px;*/
    font-size: 24px;
}

/*#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
*/
.boreder_head{
    background: #E1F5F3;
    border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;
    padding:5px 0 5px 0;
    font-weight: bold;
}
</style>
{{-- @foreach ($record as $records) --}}
<page backtop="50mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td id="" style="; width: 90px; height: 5px;">
            <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
        </td>
        <td id="text_header" style=" width: 652px; height: 5px; text-align: center;">
            <p class="title" style=""><h3>Umur Utang Usaha</h3>
                <hr style="border: #F90606; margin-top: -6px;" >
            </p>

        </td>
    </tr>
    <tr>
        <td><br></td>
        <td><br></td>
    </tr>
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    <tr>
        <td>Tahun Fiskal</td>
        <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>: {{ $request['start'] }} - {{ $request['end'] }} </td>
    </tr>

    <tr>
        <td>Region</td>
        <td>: {{ $region }}  </td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>: {{ $lsi }}  </td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: {{ $tmuk }} </td>
    </tr>
</table>
</page_header>
<table class="page_content" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center" style="border-spacing: 0; font-size: 11px; border-collapse: collapse; margin-left: -40px; border-bottom:1px solid #CCC">
    <tr class="boreder_head">
        <th class="boreder_head" style="text-align: center; width:170px;">Supplier</th>
        <th class="boreder_head" style="text-align: center; width:100px;">Currency</th>
        <th class="boreder_head" style="text-align: center; width:120px;">1-30 Days</th>
        <th class="boreder_head" style="text-align: center; width:120px;">31-60 Days</th>
        <th class="boreder_head" style="text-align: center; width:120px;">Over 60 Days</th>
        <th class="boreder_head" style="text-align: center; width:120px;">Total Balance</th>
    </tr>
    <?php
        $i=0;
        $vendor='';
    ?>
    @foreach($record as $row)
        <?php
            if($i==0){
                $vendor = $row->vendor_lokal;
            }else{
                if($row->vendor_lokal!=$vendor){
                    $i=0;
                    $vendor = $row->vendor_lokal;
                }
            }

            $st  = date_create(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->created_at)->format('Y-m-d'));
            $en    = date_create();
            $diff   = date_diff( $st, $en );
            $day = $diff->days;
        ?>
        @if($i==0)
            <tr>
                <td style="border-bottom: 2px solid black;text-align: left;width:170px;">Lokal Vendor {{ $row->vendors($row->id) }}</td>
                <td style="border-bottom: 2px solid black;text-align: center;" >IDR</td>
                <td style="border-bottom: 2px solid black;text-align: right;" >{{ rupiah(0) }}</td>
                <td style="border-bottom: 2px solid black;text-align: right;" >{{ rupiah(0) }}</td>
                <td style="border-bottom: 2px solid black;text-align: right;" >{{ rupiah(0) }}</td>
                <td style="border-bottom: 2px solid black;text-align: right;" >{{ rupiah(0) }}</td>
            </tr>
            <tr>
                <td style="text-align: left">Supplier Invoice</td>
                <td style="text-align: center;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                <td style="text-align: right;" >
                    @if($day <= 30)
                        {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                    @else
                        {{ rupiah(0) }}
                    @endif
                </td>
                <td style="text-align: right;" >
                    @if($day > 30 && $day <= 60)
                        {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                    @else
                        {{ rupiah(0) }}
                    @endif
                </td>
                <td style="text-align: right;" >
                    @if($day > 60)
                        {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                    @else
                        {{ rupiah(0) }}
                    @endif
                </td>
                <td style="text-align: right;">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
            </tr>
        @else
            <tr>
                <td style="text-align: left">Supplier Invoice</td>
                <td style="text-align: center;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                <td style="text-align: right;" >
                    @if($day <= 30)
                        {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                    @else
                        {{ rupiah(0) }}
                    @endif
                </td>
                <td style="text-align: right;" >
                    @if($day > 30 && $day <= 60)
                        {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                    @else
                        {{ rupiah(0) }}
                    @endif
                </td>
                <td style="text-align: right;" >
                    @if($day > 60)
                        {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                    @else
                        {{ rupiah(0) }}
                    @endif
                </td>
                <td style="text-align: right;">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
            </tr>
        @endif
        <?php 
            $i++;
        ?>
    @endforeach
</table>
</page>
{{-- @endforeach --}}