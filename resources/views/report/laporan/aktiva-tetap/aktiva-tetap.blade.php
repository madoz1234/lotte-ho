<style type="text/css">
    <!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    /*padding: 10;*/
    /*margin: 2px;*/
    font-size: 24px;
}

/*#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
*/
.boreder_head{
    background: #EFEFEF;
    border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;
    padding:5px 0 5px 0;
    font-weight: bold;
}
</style>
{{-- @foreach ($record as $records) --}}
<page backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td id="text_header" style=" width: 100%; height: 5px; text-align: center;">
            <p class="title" style="">
                <hr style="border: #F90606; margin-top: -6px;" >
            </p>

        </td>
    </tr>
    </table>

    <table>
    <tr>
        <td><h3>Daftar Aktiva Tetap</h3></td>
    </tr>
    </table>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    {{-- {{ dd($region) }} --}}
    <tr>
        <td>Tahun Fiskal</td>
        <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>: {{ $request['start'] }} </td>
    </tr>

    <tr>
        <td>Region</td>
        <td>: {{ $region }}  </td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>: {{ $lsi }}  </td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: {{ $tmuk }} </td>
    </tr>
</table>
<br>
<br>
<table class="page_content" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center" style="border-spacing: 0; font-size: 11px; border-collapse: collapse; margin-left: -40px; border-bottom:1px solid #CCC">
    <tr>
            <th class="boreder_head" rowspan="2" style="text-align: center;width: 30px">#</th>
            <th class="boreder_head" rowspan="2" style="text-align: center;width: 200px">Keterangan</th>
            <th class="boreder_head" colspan="2" style="text-align: center;width: 135px">Tanggal Perolehan</th>
            <th class="boreder_head" colspan="3" style="text-align: center;width: 135px">Perolehan</th>
            <th class="boreder_head" rowspan="2" style="text-align: center;width: 90px">Umur Ekonomis (Tahun)</th>
            <th class="boreder_head" rowspan="2" style="text-align: center;width: 90px">Penyusutan Perbulan</th>
            <th class="boreder_head" rowspan="2" style="text-align: center;width: 90px">Penyusutan Juli 2018</th>
            <th class="boreder_head" colspan="2" style="text-align: center;width: 115px">Penyusutan Tahun 2018</th>
            <th class="boreder_head" rowspan="2" style="text-align: center;width: 115px">Total Akm. Penyusutan</th>
            <th class="boreder_head" rowspan="2" style="text-align: center;width: 80px">Nilai Buku</th>
        </tr>
        <tr>
            <th class="boreder_head" style="text-align: center;">Bulan</th>
            <th class="boreder_head" style="text-align: center;">Tahun</th>
            <th class="boreder_head" style="text-align: center;">Unit</th>
            <th class="boreder_head" style="text-align: center;">Harga</th>
            <th class="boreder_head" style="text-align: center;">Jumlah</th>
            <th class="boreder_head" style="text-align: center;">Bulan</th>
            <th class="boreder_head" style="text-align: center;">Tahun</th>
        </tr>
    <tr>
        <td colspan="14" style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>
    <?php $perolehan_harga=0;
                $penyusutan_jumlah=0;
                $penyusutan_jumlah_bulan=0;
                $penyusutan_jumlah_tahun=0;
                $jum_unit=0;
                ?>
            <tr>
                <td></td>
                <td style="width: 200px"><b>SUMMARY {{ \Carbon\Carbon::createFromFormat('Y-m-d',$start)->format('d/m/Y') }} (Active)</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @foreach($aset as $v)
            <tr>
                <td></td>
                <td style="width: 200px">{{ isset($v->tipeaset->tipe) ? $v->tipeaset->tipe : '-' }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align: right;">{{ rupiah($v->jumlah) }}</td>
                <td></td>
                <td style="text-align: right;">{{ rupiah($v->jumlah/($v->getUmurEkonomis($v->tipe_id)*12)) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @endforeach
            <tr> 
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"><b>DETAIL</b></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
            </tr>
            @foreach($aset as $v)
                <tr>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black; width: 200px;"><b>{{ isset($v->tipeaset->tipe) ? $v->tipeaset->tipe : '-' }}</b></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                </tr>
                <?php
                    $num=0; 
                    $total=0;
                    $tot_penyusutan=0;
                    $tot_penyusutan_bulan=0;
                    $tot_penyusutan_tahun=0;
                ?>

                @foreach($v->getItemAset($v->tmuk_kode,$v->tipe_id,$start) as $d)
                    
                    <?php $total+=$d->nilai_pembelian;
                        $perolehan_harga+=$d->nilai_pembelian;
                        $num+=1;
                    ?>

                    <tr>
                        <td></td>
                        <td style="width: 200px;">{{ $d->nama }}</td>
                        <td style="text-align: center;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$d->tanggal_pembelian)->format('F') }}</td>
                        <td style="text-align: center;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$d->tanggal_pembelian)->format('Y') }}</td>
                        <td style="text-align: center;">1</td>
                        <td style="text-align: right;">{{ rupiah($d->nilai_pembelian) }}</td>
                        <td style="text-align: right;">{{ rupiah($d->nilai_pembelian) }}</td>
                        <td style="text-align: center;">{{ $d->getUmurEkonomis($d->tipe_id) }}</td>

                        <?php
                            $d1 = $d->getUmurEkonomis($d->tipe_id);

                            $penyusutan_bulan = (float) $d->nilai_pembelian / (float) ($d1*12);
                            $tot_penyusutan+=$penyusutan_bulan;
                            $penyusutan_jumlah+=$penyusutan_bulan;
                        ?>

                        <td style="text-align: right;">{{ rupiah($penyusutan_bulan) }}</td>
                        
                        <?php
                            $d1 = new DateTime(\Carbon\Carbon::createFromFormat('Y-m-d',$d->tanggal_pembelian)->format('Y-m-d'));
                            $d2 = new DateTime($start);

                            $d3 = $d1->diff($d2)->m + ($d1->diff($d2)->y*12);
                            $tot_penyusutan_bulan+=($d3 * $penyusutan_bulan);
                        ?>
                        
                        <td style="text-align: right;">{{ rupiah($d3 * $penyusutan_bulan) }}</td>

                        <?php
                            $month = \Carbon\Carbon::createFromFormat('Y-m-d',$start)->format('m');
                            $month = (int) $month;
                            $tot_penyusutan_tahun+=($month * $penyusutan_bulan);
                        ?>

                        <td style="text-align: center;">{{ $month }}</td>
                        <td style="text-align: right;">{{ rupiah($month * $penyusutan_bulan) }}</td>
                        <td style="text-align: right;">{{ rupiah($d3 * $penyusutan_bulan) }}</td>
                        <td style="text-align: right;">{{ rupiah(($d->nilai_pembelian) - ($d3 * $penyusutan_bulan)) }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"><b>Sub Total</b></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;text-align: center">{{ $v->getItemAset($v->tmuk_kode,$v->tipe_id,$start)->count() }}</td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;text-align: right;">{{ rupiah($total) }}</td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;text-align: right;">{{ rupiah($tot_penyusutan) }}</td>
                    <td style="border-top:2px solid black;text-align: right;">{{ rupiah($tot_penyusutan_bulan) }}</td>
                    <td style="border-top:2px solid black;"></td>
                    <td style="border-top:2px solid black;text-align: right;">{{ rupiah($tot_penyusutan_tahun) }}</td>
                    <td style="border-top:2px solid black;text-align: right;">{{ rupiah($tot_penyusutan_bulan) }}</td>
                    <td style="border-top:2px solid black;text-align: right;">{{ rupiah($total-$tot_penyusutan_bulan) }}</td>
                </tr>
                <?php
                    $penyusutan_jumlah_bulan+=$tot_penyusutan_bulan;    
                    $penyusutan_jumlah_tahun+=$tot_penyusutan_tahun;    
                    $jum_unit+=$num;    
                ?>
            @endforeach
            <tr>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"><b>Total</b></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;text-align: center;">{{ $jum_unit }}</td>
                <td style="border-top:2px solid black;text-align: right;">{{ rupiah($perolehan_harga) }}</td>
                <td style="border-top:2px solid black;text-align: right;">{{ rupiah($aset->sum('jumlah')) }}</td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;text-align: right;">{{ rupiah($penyusutan_jumlah) }}</td>
                <td style="border-top:2px solid black;text-align: right;">{{ rupiah($penyusutan_jumlah_bulan) }}</td>
                <td style="border-top:2px solid black;"></td>
                <td style="border-top:2px solid black;text-align: right;">{{ rupiah($penyusutan_jumlah_tahun) }}</td>
                <td style="border-top:2px solid black;text-align: right;">{{ rupiah($penyusutan_jumlah_bulan) }}</td>
                <td style="border-top:2px solid black;text-align: right;">{{ rupiah($aset->sum('jumlah')-$penyusutan_jumlah_bulan) }}</td>
            </tr>
</table>

</page>