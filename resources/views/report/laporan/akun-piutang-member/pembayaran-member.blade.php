<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 100%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
-->
</style>
<page backtop="62mm" backbottom="3mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
                </td>
                <td id="text_header" style=" width: 600px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Pembayaran Member</h3>
                        <hr style="border: #F90606; margin-top: -6px;" >
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['start'] }} - {{ $request['end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="text-align: center; width:5%;">#</th>
                <th style="text-align: center; width:15%;">Tanggal</th>
                <th style="text-align: center; width:20%;">No Member</th>
                <th style="text-align: center; width:20%;">Nama Member</th>
                <th style="text-align: center; width:20%;">Jumlah Pembayaran (Rp)</th>
                <th style="text-align: center; width:20%;">Sisa Piutang (Rp)</th>
            </tr>
        </table>
    </page_header><br>
    <table class="page_content" border="0" align="center">
        <col style="text-align: center; width:5%">
        <col style="text-align: center; width:15%">
        <col style="text-align: center; width:20%">
        <col style="text-align: center; width:20%">
        <col style="text-align: center; width:20%">
        <col style="text-align: center; width:20%">
        <?php $i = 1;?>
        @foreach ($records as $row)
            <tr>
                <td style="width:5%">{{ $i }}</td>
                <td style="width:15%">{{ $row->created_at }}</td>
                <td style="width:20%">{{ $row->no_member }}</td>
                <td style="width:20%">{{ $row->nama_member }}</td>
                <td style="width:20%">{{ FormatNumber(0) }}</td>
                <td style="width:20%">{{ FormatNumber(0) }}</td>
            </tr>
        <?php $i++; ?>
        @endforeach
         <tr>
            <td colspan="6" style="text-align: center;  border-bottom: 1px solid #000"></td>
        </tr>
        <tr>
            <th colspan="4" style="text-align: center; width:20%;">Grand Total (Rp)</th>
            <th style="text-align: center; width:20%;">{{ FormatNumber(0) }}</th>
            <th style="text-align: center; width:20%;">{{ FormatNumber(0) }}</th>
        </tr>
        <tr>
            <td colspan="6" style="text-align: center;  border-bottom: 1px solid #000"></td>
        </tr>

    </table>
</page>