<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
    }*/

    table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
    table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
    table.page_content_header {width: 100%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
    table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

    .title{
        font-weight: bold;
    }
    .tittleMargin{
        padding: 10;
        margin: 2px;
        font-size: 24px;
    }

    #text_header{
        width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
    }
    -->
</style>
<page backtop="65mm" backbottom="3mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header>
    <table class="page_header" border="0" align="center">
        <tr>
            <td id="" style="; width: 90px; height: 5px;">
                <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
            </td>
            <td id="text_header" style=" width: 600px; height: 5px; text-align: center;">
                <p class="title" style=""><h3>MUTASI ESCROW</h3>
                    <hr style="border: #F90606; margin-top: -6px;" >
                </p>
            </td>
        </tr>
        <tr>
            <td><br></td>
            <td><br></td>
        </tr>
        <tr>
            <td>Tanggal Cetak </td>
            <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
        </tr>
        {{-- {{ dd($region) }} --}}
        <tr>
            <td>Tahun Fiskal</td>
            <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
        </tr>
        <tr>
            <td>Periode</td>
            <td>: {{ $request['start'] }} - {{ $request['end'] }} </td>
        </tr>

        <tr>
            <td>Region</td>
            <td>: {{ $region }}  </td>
        </tr>
        <tr>
            <td>LSI</td>
            <td>: {{ $lsi }}  </td>
        </tr>
        <tr>
            <td>TMUK</td>
            <td>: {{ $tmuk }} </td>
        </tr>
    </table>
    <table class="page_content_header" border="0" align="center">
        <tr>
            <th style="text-align: center; width:10%">Tanggal</th>
            <th style="text-align: center; width:30%">Keterangan</th>
            <th style="text-align: center; width:20%">Debet</th>
            <th style="text-align: center; width:20%">Credit</th>
            <th style="text-align: center; width:20%">Ledger</th>
        </tr>
    </table>
    </page_header>
    <table class="page_content" border="0" align="center">
        <col style="text-align: center; width:10%">
        <col style="text-align: left; width:30%">
        <col style="text-align: right; width:20%">
        <col style="text-align: right; width:20%">
        <col style="text-align: right; width:20%">
        <?php 
                $kredit=0;
                $debit=0;
                $awal=$saldo;
                $i=0;
                $tanggal=[];
                $deskripsi=[];
                $debet=[];
                $credit=[];
                $ledger=[];
            ?>
            @foreach ($record as $row)
                    <?php
                        array_push($tanggal,\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->tanggal)->format('d F Y'));
                        if($row->reduce){
                            array_push($deskripsi,$row->reduce->po_nomor);
                        }else{
                            $strings = '';
                            if($row->pyr){
                                if($row->pyr->vendor_lokal == 0){
                                    $tmuk = Lotte\Models\Master\Tmuk::with('lsi')->where('kode', $row->pyr->tmuk->kode)->first();
                                    $strings = ' - '.$tmuk->lsi->nama;
                                }else if($row->pyr->vendor_lokal == 100){
                                    $strings = ' - '.$row->pyr->tmuk->nama;
                                }else{
                                    $vendor = Lotte\Models\Master\VendorLokalTmuk::where('id', $row->pyr->vendor_lokal)->first();
                                    $strings = ' - '.isset($vendor) ? $vendor->nama : '-';
                                }
                            }
                            $desk = $row->deskripsi.' '.$strings;
                            array_push($deskripsi,$desk);
                        }
                        array_push($debet,$row->posisi=='D' ? rupiah($row->jumlah) : '');
                        array_push($credit,$row->posisi=='K' ? rupiah($row->jumlah) : '');

                        if($row->posisi=='D'){
                            $debit+=$row->jumlah;
                        }else{
                            $kredit+=$row->jumlah;
                        }

                        if($row->coa->posisi=='D'){
                                $saldo = ($debit - $kredit) + $awal;
                           
                        }else{
                                $saldo = ($kredit - $debit) + $awal;
                        }
                        $i++;
                        array_push($ledger,rupiah($saldo));
                    ?>
            @endforeach
            @if(count($tanggal)>0)
            <?php
                krsort($tanggal);
                krsort($deskripsi);
                krsort($debet);
                krsort($credit);
                krsort($ledger);
            ?>
                @foreach ($tanggal as $key => $row)
                <tr>
                    <td style="text-align: left; width:10%">{{ $tanggal[$key] }}</td>
                    <td style="text-align: left; width:30%">
                        {{ $deskripsi[$key] }}
                    </td>
                    <td style="text-align: right; width:20%">{{ $debet[$key] }}</td>
                    <td style="text-align: right; width:20%">{{ $credit[$key] }}</td>
                    <td style="text-align: right; width:20%">{{ $ledger[$key] }}</td>
                </tr>
                @endforeach
            @endif
    </table>

    <br>
    <br>

    <table class="page_content_header" border="0" align="right">
        <tr>
            <th style="text-align: right; width:20%">&nbsp;</th>
            <th style="text-align: right; width:15%">OPENING BALANCE</th>
            <th style="text-align: right; width:15%">TOTAL DEBET</th>
            <th style="text-align: right; width:15%">TOTAL CREDIT</th>
            <th style="text-align: right; width:15%">CLOSING BALANCE</th>
        </tr>
    </table>
    <table class="page_content" border="0" align="right">
        <tr>
            <td style="text-align: right;width:20%;">&nbsp;</td>
            <td style="text-align: right;width:15%;"><b>{{ rupiah($awal) }}</b></td>
            <td style="text-align: right;width:15%;"><b>{{ rupiah($debit) }}</b></td>
            <td style="text-align: right;width:15%;"><b>{{ rupiah($kredit) }}</b></td>
            <td style="text-align: right;width:15%;"><b>{{ rupiah($saldo) }}</b></td>
        </tr>
    </table>
</page>