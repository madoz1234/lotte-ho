<style type="text/css">
    <!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    /*padding: 10;*/
    /*margin: 2px;*/
    font-size: 24px;
}

/*#text_header{
    width: 100%;text-align: left; padding-bottom: 5px; padding-top: 5px; padding-right: 20px;
}
*/
.boreder_head{
    background: #efefef;
    border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;
    padding:5px 0 5px 0;
    font-weight: bold;
}
</style>
{{-- @foreach ($record as $records) --}}
<page backtop="50mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td id="" style="; width: 90px; height: 5px;">
            <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 5px; margin-right: -3px;">
        </td>
        <td id="text_header" style=" width: 652px; height: 5px; text-align: center;">
            <p class="title" style=""><h3>LAPORAN PERFORMANCE TMUK</h3>
                <hr style="border: #F90606; margin-top: -6px;" >
            </p>

        </td>
    </tr>
    <tr>
        <td><br></td>
        <td><br></td>
    </tr>
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>: {{ $periode }} </td>
    </tr>

    <tr>
        <td>Region</td>
        <td>: {{ $region }} </td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>:  {{ $lsi }}</td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: {{ $tmuk_kode }} - {{ $tmuk }} </td>
    </tr>
</table>
@foreach ($kki as $row)
</page_header>
<table class="page_content" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center" style="border-spacing: 0; font-size: 11px; border-collapse: collapse; margin-left: -40px; border-bottom:1px solid #CCC">
    <tr class="boreder_head">
        <th class="boreder_head" style="text-align: center; width:72px;">ITEM</th>
        <th class="boreder_head" style="text-align: center; width:72px;">KKI</th>
        <th class="boreder_head" style="text-align: center; width:35px;">%</th>
        @for($m = 0; $m <= $diff; $m++)
            <th class="boreder_head" style="text-align: center; width:72px;">{{ $dayStart->copy()->addMonths($m)->format('F / Y') }}</th>
            <th class="boreder_head" style="text-align: center; width:5px;">%</th>
        @endfor
        <th class="boreder_head" style="text-align: center; width:72px;">Akumulasi</th>
        <th class="boreder_head" style="text-align: center; width:72px;">Average</th>

    </tr>
    {{-- @foreach ($records as $row) --}}
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Penjualan</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($row->ii_penjualan_normal) }}</td>
        <?php  $persen_penjualan=0; ?>
        <?php  $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  ($row->ii_penjualan_normal / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan) }}%</td>
            <?php $akumulasi = 0; $jurnal_penjualan=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php
                    $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                    $akumulasi += $jurnal_penjualan;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($jurnal_penjualan) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:5px;">{{ $jurnal_penjualan == 0 ? 0 : rupiah($jurnal_penjualan / $jurnal_penjualan * (100)) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">HPP</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2) }}</td>
        <?php  $persen_penjualan=0; ?>
        <?php  $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  (($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2) / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan) }}%</td>
            <?php $akumulasi = 0; $hppp=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php
                    $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);

                    $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');
                    $akumulasi += $hppp;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($hppp) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $hppp == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $hppp / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Margin</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($row->ii_penjualan_normal - ($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2) ) }}</td>
        <?php  $persen_penjualan=0; ?>
        <?php  $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  (($row->ii_penjualan_normal - ($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2)) / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan) }}%</td>
            <?php $akumulasi = 0; $margin=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');
                    $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $margin = $jurnal_penjualan - $hppp;
                    $akumulasi += $margin;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($margin) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $margin == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $margin / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">SPD</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($row->ii_sales_normal) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;"></td>
            <?php $akumulasi = 0; $jurnal_penjualan =0; $spd_jual =0; $toko=0; $toko_buka=0;  ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                    $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                    $toko_buka = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);

                    $toko = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? count($toko_buka)-1 : count($toko_buka);
                    $spd_jual = $jurnal_penjualan == 0 ? 0 : $jurnal_penjualan / $toko;
                    $akumulasi += $spd_jual;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($spd_jual) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:5px;"></td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">STD</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($row->ii_struk_normal) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;"></td>
            <?php $akumulasi = 0; $struk = 0; $std_jual=0; $toko_buka=0; $toko=0; $kondisi_struk=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $struk = Lotte\Models\Trans\TransRekapPenjualanTmuk::getCountStruk($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $kondisi_struk = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? $struk-$struk_akhir : $struk ;

                    $toko_buka = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $toko = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? count($toko_buka)-1 : count($toko_buka);


                    $std_jual = $kondisi_struk == 0 ? 0 : $kondisi_struk / $toko;


                    $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                    $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                    $akumulasi += $std_jual;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($std_jual) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:5px;"></td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">APC</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $row->ii_sales_normal == 0 ?  '0' : rupiah($row->ii_sales_normal / $row->ii_struk_normal) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:5px;"></td>
            <?php $akumulasi = 0; $spd_jual = 0; $std_jual = 0;  $apc_jual = 0; $struk = 0; $toko_buka=0; $toko=0; $kondisi_struk=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $jurnal_penjualan_backup = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode);

                    $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                    $toko_buka = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $toko = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? count($toko_buka)-1 : count($toko_buka);

                    $spd_jual = $jurnal_penjualan == 0 ? 0 : $jurnal_penjualan / $toko ;
                    $struk = Lotte\Models\Trans\TransRekapPenjualanTmuk::getCountStruk($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $kondisi_struk = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? $struk-$struk_akhir : $struk ;

                    $std_jual = $kondisi_struk == 0 ? 0 : $kondisi_struk / $toko;
                    $apc_jual =  $spd_jual == 0 ? 0 : $spd_jual / $std_jual;
                    $akumulasi += $apc_jual;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($apc_jual) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:5px;"></td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight;">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
    </tr>
    <tr style="">
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $akumulasi += $total;
                ?>
                <td style="text-align: center;  border-bottom: 1px solid #000"></td>
                <td style="text-align: center;  border-bottom: 1px solid #000"></td>
            @endfor
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>


    <tr class="boreder_head">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Operasional</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;"></td>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;"></td>
            <?php $akumulasi = 0; $spd_jual = 0; $std_jual = 0;  $apc_jual = 0; $struk = 0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $struk = Lotte\Models\Trans\TransRekapPenjualanTmuk::getCountStruk($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $toko = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $spd_jual = $total / 30;
                    $std_jual = $struk == 0 ? 0 : $struk / count($toko);
                    $apc_jual =  $spd_jual == 0 ? 0 : $spd_jual / $std_jual;
                    $akumulasi += $apc_jual;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:5px;"></td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;"></td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;"></td>
    </tr>
    <tr style="">
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <?php $dep_amor = ($row->iv_empat_a_estimasi2+$row->iv_empat_b_estimasi2) + ($row->iv_empat_c_estimasi2); ?>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $akumulasi += $total;
                ?>
                <td style="text-align: center;  border-bottom: 1px solid #000"></td>
                <td style="text-align: center;  border-bottom: 1px solid #000"></td>
            @endfor
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Utilities (Air , Listrik & Telp/Internet</td>
        <?php $utilities = ($row->iv_satu_a_estimasi2+$row->iv_satu_b_estimasi2) + ($row->iv_satu_c_estimasi2); ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utilities) }}</td>
        <?php  $persen_penjualan_listrik=0; ?>
        <?php  $persen_penjualan_listrik = $row->ii_penjualan_normal == 0 ? 0 :  ($utilities / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_listrik) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Perlengkapan Toko (ATK)</td>
        <?php $perlengapan_toko = 0; ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($perlengapan_toko) }}</td>
        <?php  $persen_penjualan_atk=0; ?>
        <?php  $persen_penjualan_atk = $row->ii_penjualan_normal == 0 ? 0 :  ($perlengapan_toko / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_atk) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.12');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:5px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Gaji Karyawan</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($row->iv_dua_estimasi2) }}</td>
        <?php  $persen_penjualan_gaji=0; ?>
        <?php  $persen_penjualan_gaji = $row->ii_penjualan_normal == 0 ? 0 :  ($row->iv_dua_estimasi2 / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_gaji) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Pembungkus</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($row->iv_satu_f_estimasi2) }}</td>
        <?php  $persen_penjualan_bungkus=0; ?>
        <?php  $persen_penjualan_bungkus = $row->ii_penjualan_normal == 0 ? 0 :  ($row->iv_satu_f_estimasi2 / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah(ceil($persen_penjualan_bungkus)) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Sewa Toko</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($row->iv_empat_d_estimasi2) }}</td>
        <?php  $persen_penjualan_sewa=0; ?>
        <?php  $persen_penjualan_sewa = $row->ii_penjualan_normal == 0 ? 0 :  ($row->iv_empat_d_estimasi2 / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_sewa) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Lain-lain</td>
        <?php $biaya_lain = $row->iv_satu_e_estimasi2 + $row->iv_tambahan_satu_b + $row->iv_tambahan_dua_b + $row->iv_tambahan_tiga_b + $row->iv_tambahan_empat_b ;?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($biaya_lain) }}</td>
        <?php  $persen_penjualan_lain=0; ?>
        <?php  $persen_penjualan_lain = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_lain / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_lain) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">0</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:5px;">0%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">0</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">0</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Keamanan</td>
        <?php $biaya_keamanan = $row->iv_satu_d_estimasi2;?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($biaya_keamanan) }}</td>
        <?php  $persen_penjualan_keamanan=0; ?>
        <?php  $persen_penjualan_keamanan = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_keamanan / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_keamanan) }}%</td>
        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Pengiriman</td>
        <?php $biaya_pengiriman = 0;?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($biaya_pengiriman) }}</td>
        <?php  $persen_penjualan_pengiriman=0; ?>
        <?php  $persen_penjualan_pengiriman = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_pengiriman / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_pengiriman) }}%</td>
        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.1');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Distribusi (Administrasi)</td>
        <?php $biaya_adm = $row->iv_satu_i_estimasi2;?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($biaya_adm) }}</td>
        <?php  $persen_penjualan_adm=0; ?>
        <?php  $persen_penjualan_adm = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_adm / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_adm) }}%</td>
        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Bensin</td>
        <?php $biaya_bensin = 0;?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($biaya_bensin) }}</td>
        <?php  $persen_penjualan_bensin=0; ?>
        <?php  $persen_penjualan_bensin = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_bensin / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_bensin) }}%</td>
        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.11');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Barang Rusak/Hilang</td>
        <?php $barang_rusak = $row->iv_satu_g_estimasi2 + $row->iv_satu_h_estimasi2;?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($barang_rusak) }}</td>
        <?php  $persen_penjualan_rusak=0; ?>
        <?php  $persen_penjualan_rusak = $row->ii_penjualan_normal == 0 ? 0 :  ($barang_rusak / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_rusak) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Premi Asuransi</td>
        <?php $biaya_premi = $row->iv_tiga_a_estimasi2 + $row->iv_tiga_b_estimasi2; ?>        
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($biaya_premi) }}</td>
        <?php $persen_penjualan_premi=0; ?>
        <?php  $persen_penjualan_premi = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_premi / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_premi) }} %</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Depresiasi & Amortiasi</td>
        <?php $dep_amor = $row->iv_empat_a_estimasi2 + $row->iv_empat_b_estimasi2 + $row->iv_empat_c_estimasi2; ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($dep_amor) }}</td>
        <?php $persen_penjualan_amor=0; ?>
        <?php $persen_penjualan_amor = $row->ii_penjualan_normal == 0 ? 0 :  ($dep_amor / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_amor) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Lain Operasional</td>
        <?php $biaya_lain_operasional = 0; ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($biaya_lain_operasional) }}</td>
        <?php $persen_penjualan_lain_operasional=0; ?>
        <?php $persen_penjualan_lain_operasional = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_lain_operasional / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_lain_operasional) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.14');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Biaya Lain Non-Operasional</td>
        <?php $biaya_lain_non_operasional = 0; ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($biaya_lain_non_operasional) }}</td>
        <?php $persen_penjualan_lain_non_operasional=0; ?>
        <?php $persen_penjualan_lain_non_operasional = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_lain_non_operasional / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_lain_non_operasional) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php
                    $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.15');
                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $utili;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utili) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Selisih Kas</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">0</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">0%</td>
             <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; $kas_kurang=0; $kas_lebih=0; $selisih_kas=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $kas_kurang = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '8.1.0.0');
                                $kas_lebih = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '7.1.0.0');

                                $selisih_kas = $kas_kurang - $kas_lebih ;


                            

                                $total_akumulasi += $selisih_kas;
                                $count_akumulasi++;
                            ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($selisih_kas) }}</td>
                 <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $selisih_kas == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $selisih_kas / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;">Total Biaya</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($utilities + $biaya_premi + $biaya_lain + $perlengapan_toko + $row->iv_dua_estimasi2 + $row->iv_satu_f_estimasi2 + $barang_rusak + $dep_amor + $row->iv_empat_d_estimasi2 + $biaya_adm + $biaya_keamanan + $biaya_pengiriman + $biaya_bensin + $biaya_lain_operasional + $biaya_lain_non_operasional ) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px;">{{ rupiah($persen_penjualan_amor + $persen_penjualan_premi + $persen_penjualan_rusak + $persen_penjualan_lain + $persen_penjualan_sewa + $persen_penjualan_bungkus + $persen_penjualan_gaji + $persen_penjualan_listrik + $persen_penjualan_atk + $persen_penjualan_keamanan + $persen_penjualan_adm + $persen_penjualan_bensin + $persen_penjualan_pengiriman + $persen_penjualan_lain_operasional + $persen_penjualan_lain_non_operasional) }}%</td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; $total_biaya_jual=0; $kas_kurang=0; $kas_lebih=0; $selisih_kas=0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php
                    $kas_kurang = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '8.1.0.0');
                    $kas_lebih = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '7.1.0.0');

                    $selisih_kas = $kas_kurang - $kas_lebih ;

                    $total_biaya_jual = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4')+ Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1') +  Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10')  + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.1') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.12') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.11') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.14') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.15') + $selisih_kas;


                    $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                    $total_akumulasi += $total_biaya_jual;
                    $count_akumulasi++;
                ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_biaya_jual) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $total_biaya_jual == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $total_biaya_jual / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ rupiah($total_akumulasi) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
    </tr>



    <tr style="">
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <?php $dep_amor = ($row->iv_empat_a_estimasi2+$row->iv_empat_b_estimasi2) + ($row->iv_empat_c_estimasi2); ?>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $akumulasi += $total;
                ?>
                <td style="text-align: center;  border-bottom: 1px solid #000"></td>
                <td style="text-align: center;  border-bottom: 1px solid #000"></td>
            @endfor
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;font-weight: bold;">Laba Bersih</td>
        <?php 
                                $laba_bersih = ($row->ii_penjualan_normal - ($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2)) - ($utilities + $biaya_premi + $biaya_lain + $perlengapan_toko + $row->iv_dua_estimasi2 + $row->iv_satu_f_estimasi2 + $barang_rusak + $dep_amor + $row->iv_empat_d_estimasi2 + $biaya_adm + $biaya_keamanan + $persen_penjualan_bensin + $persen_penjualan_pengiriman + $persen_penjualan_lain_operasional + $persen_penjualan_lain_non_operasional)   ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">{{ rupiah($laba_bersih) }}</td>
        <?php $persen_penjualan=0; ?>
                            <?php
                                $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  ($laba_bersih / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px; font-weight: bold;">{{ rupiah($persen_penjualan) }}%</td>
            <?php $akumulasi_laba_tmuk = 0; $laba_bersih_tmuk_b=0; $kas_kurang =0; $kas_lebih=0; $selisih_kas=0; ?>
                            @for($m = 0; $m <= $diff; $m++)
                            <?php 


                                $jurnal_penjualan_b = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $margin = $jurnal_penjualan_b - $hppp;

                                $kas_kurang = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '8.1.0.0');
                                $kas_lebih = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '7.1.0.0');

                                $selisih_kas = $kas_kurang - $kas_lebih ;

                                $total_biaya_b = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4')+ Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1') +  Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10')  + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.1') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.12') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.11') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.14') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.15') + $selisih_kas;


                                $jurnal_penjualan = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode);



                                $laba_bersih_tmuk_b = $margin - $total_biaya_b;
                                $akumulasi_laba_tmuk += $laba_bersih_tmuk_b;


                            ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">{{ rupiah($laba_bersih_tmuk_b) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $laba_bersih_tmuk_b == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $laba_bersih_tmuk_b / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">{{ rupiah($akumulasi_laba_tmuk) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">{{ rupiah($akumulasi_laba_tmuk / ($diff + 1)) }}</td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:72px;font-weight: bold;">Laba Cash</td>
        <?php
        $laba_cash =  $dep_amor +  $laba_bersih;?>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">{{ rupiah($laba_cash) }}</td>
        <?php $persen_penjualan=0; ?>
                            <?php 
                                $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  ($laba_cash / $row->ii_penjualan_normal) * 100 ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:35px; font-weight: bold;">{{ rupiah($persen_penjualan) }}%</td>
            <?php $akumulasi_laba_tmuk = 0; $laba_bersih_tmuk=0; ?>
                            @for($m = 0; $m <= $diff; $m++)
                            <?php
                                $jurnal_penjualan_b_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan_b = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $margin = $jurnal_penjualan_b - $hppp;

                                $kas_kurang = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '8.1.0.0');
                                $kas_lebih = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '7.1.0.0');

                                $selisih_kas = $kas_kurang - $kas_lebih ;


                                $total_biaya_b = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4')+ Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1') +  Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10')  + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.1') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.12') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.11') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.14') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.15') + $selisih_kas;

                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode);

                                $amor = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0');



                                $laba_bersih_tmuk = ($margin - $total_biaya_b) + $amor;
                                $akumulasi_laba_tmuk += $laba_bersih_tmuk;


                            ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">{{ rupiah($laba_bersih_tmuk) }}</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:72px;">{{ $laba_bersih_tmuk == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $laba_bersih_tmuk / $jurnal_penjualan * (100) : 0 ) }}%</td>
            @endfor
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">{{ rupiah($akumulasi_laba_tmuk) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:72px;font-weight: bold;">{{ rupiah($akumulasi_laba_tmuk / ($diff + 1)) }}</td>
    </tr>
    <tr style="">
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <?php $dep_amor = ($row->iv_empat_a_estimasi2+$row->iv_empat_b_estimasi2) + ($row->iv_empat_c_estimasi2); ?>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
            <?php $total_akumulasi = 0; $count_akumulasi = 0; ?>
            @for($m = 0; $m <= $diff; $m++)
                <?php 
                    $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                    $akumulasi += $total;
                ?>
                <td style="text-align: center;  border-bottom: 1px solid #000"></td>
                <td style="text-align: center;  border-bottom: 1px solid #000"></td>
            @endfor
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
        <td style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>
</table>

</page>
@endforeach
