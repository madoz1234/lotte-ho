<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 100%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
-->
</style>
<page backtop="66mm" backbottom="3mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
                </td>
                <td id="text_header" style=" width: 600px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>LAPORAN PIUTANG TMUK</h3>
                        <hr style="border: #F90606; margin-top: -6px;" >
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            {{-- {{ dd($region) }} --}}
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $tahun_fiskal['tgl_awal'] }} - {{ $tahun_fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $start.' - '.$end }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="text-align: center; width:10%;">No</th>
                <th style="text-align: center; width:20%;">Tanggal</th>
                <th style="text-align: center; width:40%;">Deskripsi</th>
                <th style="text-align: center; width:15%;">Debit</th>
                <th style="text-align: center; width:15%;">Kredit</th>
           </tr>
        </table>
    </page_header>
    <table class="page_content" border="0" align="center">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:20%">
        <col style="text-align: center; width:40%">
        <col style="text-align: center; width:15%">
        <col style="text-align: center; width:15%">
        <?php $i = 1; $total_debit = 0; $total_kredit = 0;?>
        @foreach ($record as $row)
            <tr>
                <td style="text-align: center; width:10%">{{ $i }}</td>
                <td style="text-align: center; width:20%">{{ \Carbon\Carbon::parse($row->tanggal)->format('d/m/Y') }}</td>
                <td style="text-align: left; width:40%">{{ ucfirst($row->deskripsi) }}</td>
                <td style="text-align: right; width:15%">{{  number_format($row->posisi=='D' ? $row->jumlah : '0') }}</td>
                <td style="text-align: right; width:15%">{{  number_format($row->posisi=='K' ? $row->jumlah : '0') }}</td>
                {{-- <td style="text-align: right; width:15%">{{  number_format($row->posisi=='D' ? $total_debit += $row->jumlah : '0') }}</td>
                <td style="text-align: right; width:15%">{{  number_format($row->posisi=='K' ? $total_kredit += $row->jumlah : '0') }}</td> --}}
            </tr>
        <?php $total_debit += $row->posisi=='D' ? $row->jumlah : '0' ?>
        <?php $total_kredit += $row->posisi=='K' ? $row->jumlah : '0' ?>
        <?php $i++; ?>
        @endforeach
        <tr>
            <td style="text-align: center; width:10%"></td>
            <td style="text-align: center; width:20%"></td>
            <td style="text-align: center; width:40%"><b>Total</b></td>
            <td style="text-align: right; width:15%">{{  number_format($total_debit) }}</td>
            <td style="text-align: right; width:15%">{{  number_format($total_kredit) }}</td>
        </tr>
    </table>
</page>