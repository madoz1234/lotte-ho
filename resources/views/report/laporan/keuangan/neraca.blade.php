<style type="text/css">
    <!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 98%; border: none; padding: 1mm; font-size: 11px; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    /*padding: 10;*/
    /*margin: 2px;*/
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
-->
</style>
<page backtop="68mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    {{-- <hr> --}}
    <table class="page_header" border="0" align="center" border="0">
        <tr>
            <td id="" style="; width: 90px; height: 5px;">
                <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -5px;">
            </td>
            <td id="text_header" style=" width: 600px; height: 5px; text-align: center;">
                <p class="title" style=""><h3>NERACA</h3>
                    <hr style="border: #F90606; margin-top: -4px;" >
                </p>

            </td>
        </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
        <tr>
            <td>Tanggal Cetak </td>
            <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
        </tr>
        <tr>
            <td>Tahun Fiskal</td>
            <?php $tahun = Lotte\Models\Master\TahunFiskal::where('status', 1 )->first(); ?>
            <td>: {{ is_null($tahun) ? '-' : \Carbon\Carbon::createFromFormat('Y-m-d', $tahun->tgl_awal)->format('d-m-Y') }} - {{ is_null($tahun) ? '-' : \Carbon\Carbon::createFromFormat('Y-m-d', $tahun->tgl_akhir)->format('d-m-Y') }} (Active)</td>
        </tr>
        <tr>
            <td>Periode</td>
            <td>: {{ \Carbon\Carbon::createFromFormat('d/m/Y', $start)->format('d-m-Y') }} - {{ \Carbon\Carbon::createFromFormat('d/m/Y', $end)->format('d-m-Y') }} </td>
        </tr>

        <tr>
            <td>Region</td>
            <td>:  {{ $region }} </td>
        </tr>
        <tr>
            <td>LSI</td>
            <td>: {{ $lsi }} </td>
        </tr>
        <tr>
            <td>TMUK</td>
            <td>: {{ $tmuk_kode }} - {{ $tmuk }} </td>
        </tr>
    </table>
    <br>
    <table style="width: 98%; border: none; padding: 1mm; font-size: 11px; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;" border="0" align="center" border="0">
        <tr>
            <td style="text-align: center; width:25%; font-weight: bold;">Description</td>
            @foreach($record['header'] as $row)
                <td style="text-align: center; width:25%; font-weight: bold;">{{ $row }}</td>
            @endforeach
        </tr>
    </table>
    <br>
    <table class="page_content_header" border="0" align="center" border="0">
        <tbody>
            @foreach($record['content'] as $row)
                @if($row['type'] == 'label')
                    @if(isset($row['child']))
                        <tr>
                            <td style="text-align: left; width:25%" colspan="{{ count($record['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                        </tr>
                        @foreach($row['child'] as $rowchild)
                            <tr>
                                <td style="text-align: left; width:25%">&nbsp;&nbsp;&nbsp;&nbsp;{{ $rowchild['text'] }}</td>
                                @foreach($rowchild['data'] as $val)
                                    <td style="text-align: right; width:25%">{{ curencyformat($val) }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td style="text-align: left; width:25%" colspan="{{ count($record['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                        </tr>
                    @endif
                @else
                    <tr>
                        <td style="text-align: left; width:25%"><b>{{ $row['text'] }}</b></td>
                        @foreach($row['data'] as $val)
                            <td style="text-align: right; width:25%"><b>{{ curencyformat($val) }}</b></td>
                        @endforeach
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    </page_header>
</page>

