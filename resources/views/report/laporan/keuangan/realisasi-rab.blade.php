<style type="text/css">
    <!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    /*padding: 10;*/
    /*margin: 2px;*/
    font-size: 24px;
}

/*#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
*/
.boreder_head{
    background: #E1F5F3;
    border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;
    padding:5px 0 5px 0;
    font-weight: bold;
}
</style>
{{-- @foreach ($record as $records) --}}
<page backtop="50mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td id="" style="; width: 90px; height: 5px;">
            <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
        </td>
        <td id="text_header" style=" width: 652px; height: 5px; text-align: center;">
            <p class="title" style=""><h3>LAPORAN REALISASI RAB</h3>
                <hr style="border: #F90606; margin-top: -6px;" >
            </p>

        </td>
    </tr>
    <tr>
        <td><br></td>
        <td><br></td>
    </tr>
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    <tr>
        <td>Tahun Fiskal</td>
        <td>:  (Active)</td>
    </tr>
   {{--  <tr>
        <td>Periode</td>
        <td>:  </td>
    </tr> --}}

    <tr>
        <td>Region</td>
        <td>:  {{ $region }}</td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>:  {{ $lsi }}</td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: {{ $tmuk_kode }} - {{ $tmuk }} </td>
    </tr>
</table>
</page_header>
<table class="page_content" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center" style="border-spacing: 0; font-size: 11px; border-collapse: collapse; margin-left: -40px; border-bottom:1px solid #CCC">
    <tr class="boreder_head">
        <th class="boreder_head" style="text-align: center; width:150px;">RAB</th>
        <th class="boreder_head" style="text-align: right; width:125px;">Total</th>
        <th class="boreder_head" style="text-align: center; width:30px;"></th>
        <th class="boreder_head" style="text-align: center; width:250px;">REALISASI</th>
        <th class="boreder_head" style="text-align: right; width:125px;">Total</th>
    </tr>
    @foreach ($record as $row)

    {{-- {{ dd($row) }} --}}
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:150px;">Perkiraan dana investasi</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
        <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
        <td style="padding:5px 0 5px 0; text-align: left; width:250px;">Pemasukan</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
    </tr>
    <?php $sum_tot = 0 ?>
    @foreach ($saldo as $jual)
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;"></td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
        <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
        <td style="padding:5px 0 5px 0; text-align: left; width:250px;">{{ \Carbon\Carbon::createFromFormat('m-Y',$jual->month.'-'.$jual->year)->format('F  Y') }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($jual->total_penjualan) }}</td>
    </tr>
    <?php $sum_tot += $jual->total_penjualan ?>
    @endforeach
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">DP</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_satu) }}</td>
        <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
        <td style="padding:5px 0 5px 0; text-align: left; width:250px;"></td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($sum_tot) }}</b></td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Dana investasi</td>
        <?php $invest = $row->i_dua + $row->i_tiga_a + $row->i_tiga_b + $row->i_tiga_c + $row->i_tiga_d + $row->i_tiga_e + $row->i_tiga_f + $row->i_empat_a + $row->i_empat_b + $row->i_empat_c + $row->i_lima_a + $row->i_lima_b + $row->i_bila_ada_biaya_tambahan + $row->i_sewa_ruko + $row->i_ppn; ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($invest) }}</td>
        <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
        <td style="padding:5px 0 5px 0; text-align: left; width:250px;">Pengeluaran</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
    </tr>
    @if($recordakuisisi)
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: right; width:150px;"></td>
                <?php $totaldana = $row->i_satu + $invest; ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($totaldana) }}</b> </td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Start & Biaya Promosi Opening</td>
                <?php $start = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 1,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($start) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Perijinan Usaha Toko</td>
                <?php $perijinan = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 2,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($perijinan) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;">Perkiraan pengeluaran</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Renovasi Sipil</td>
                <?php $renovasi = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 3,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($renovasi) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Stok awal</td>
                <?php $stokawal = $row->i_lima_a; ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($stokawal)}}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Kusen Aluminium + Kaca Cermin</td>
                <?php $kusen = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 4,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($kusen) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Folding Gate + Polycarbonate = Teras</td>
                <?php $folding = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 5,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($folding) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Pekerjaan sipil</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_tiga_a + $row->i_tiga_b + $row->i_tiga_c + $row->i_tiga_d + $row->i_tiga_e + $row->i_tiga_f) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Instalasi Listrik (lampu, regrouping)</td>
                <?php $instalasi = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 6,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($instalasi) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Instalasi Pemasangan AC</td>
                <?php $pemasangan = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 7,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($pemasangan) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Peralatan Elektonik</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_empat_a + $row->i_empat_c) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Signage (Lisping Papan Nama Toko) Repainting</td>
                <?php $signage = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 8,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($signage) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Peralatan AC Sipil</td>
                <?php $sipil = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 9,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($sipil) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Peralatan Non-Elektronik</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_empat_b) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Peralatan Non Elektronik (Rak, Furniture, Acrylic, dll)</td>
                <?php $non_elektronik = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 10,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($non_elektronik) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Peralatan Eletronika (Komputer, Cooler & Timbangan)</td>
                <?php $elektronik = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 11,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($elektronik) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Quantum 1 burner Qgc & selang</td>
                <?php $stock_awal = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 12,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($stock_awal) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Modal Kerja</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_lima_b) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Modal Kerja 1 bulan (ditransfer ke rek Ops TMUK)</td>
                <?php $modal_kerja = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 13,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($modal_kerja) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Lain-lain</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_bila_ada_biaya_tambahan) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px ; padding-left: 20px;">Lain-lain</td>
                <?php $lain_lain = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 14,$recordakuisisi->tmuk_kode); ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($lain_lain) }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;"></td>
                <?php $subtotal_perkiraan = $row->i_lima_a + $row->i_tiga_a + $row->i_tiga_b + $row->i_tiga_c + $row->i_tiga_d + $row->i_tiga_e + $row->i_tiga_f + $row->i_empat_a + $row->i_empat_c + $row->i_empat_b + $row->i_satu + $row->i_lima_b + $row->i_bila_ada_biaya_tambahan; ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($subtotal_perkiraan) }}</b></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px ; padding-left: 20px;"></td>
                @if($recordakuisisi)
                <?php $subtotal_pengeluaran = $start + $perijinan + $renovasi + $kusen + $folding + $instalasi + $pemasangan + $signage + $sipil + $non_elektronik + $elektronik + $stock_awal + $modal_kerja + $lain_lain; ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($subtotal_pengeluaran) }}</b></td>
                @else
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah(0) }}</b></td>
                @endif
            </tr>
    @else
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: right; width:150px;"></td>
            <?php $totaldana = $row->i_satu + $invest; ?>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($totaldana) }}</b> </td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Start & Biaya Promosi Opening</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Perijinan Usaha Toko</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;">Perkiraan pengeluaran</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Renovasi Sipil</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Stok awal</td>
            <?php $stokawal = $row->i_lima_a; ?>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($stokawal)}}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Kusen Aluminium + Kaca Cermin</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Folding Gate + Polycarbonate = Teras</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Pekerjaan sipil</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_tiga_a + $row->i_tiga_b + $row->i_tiga_c + $row->i_tiga_d + $row->i_tiga_e + $row->i_tiga_f) }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Instalasi Listrik (lampu, regrouping)</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Instalasi Pemasangan AC</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Peralatan Elektonik</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_empat_a + $row->i_empat_c) }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Signage (Lisping Papan Nama Toko) Repainting</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Peralatan AC Sipil</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Peralatan Non-Elektronik</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_empat_b) }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Peralatan Non Elektronik (Rak, Furniture, Acrylic, dll)</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Peralatan Eletronika (Komputer, Cooler & Timbangan)</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Quantum 1 burner Qgc & selang</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Modal Kerja</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_lima_b) }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Modal Kerja 1 bulan (ditransfer ke rek Ops TMUK)</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Lain-lain</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_bila_ada_biaya_tambahan) }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px ; padding-left: 20px;">Lain-lain</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">0</td>
        </tr>
        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;"></td>
            <?php $subtotal_perkiraan = $row->i_lima_a + $row->i_tiga_a + $row->i_tiga_b + $row->i_tiga_c + $row->i_tiga_d + $row->i_tiga_e + $row->i_tiga_f + $row->i_empat_a + $row->i_empat_c + $row->i_empat_b + $row->i_satu + $row->i_lima_b + $row->i_bila_ada_biaya_tambahan; ?>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($subtotal_perkiraan) }}</b></td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px ; padding-left: 20px;"></td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah(0) }}</b></td>
        </tr>
        
    @endif
    @endforeach
    <tr>
        <td colspan="5" style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>

    <tr >
        <td style="padding:5px 0 5px 0;text-align: center; width:150px;"></td>
        <?php $total_perkiraan = $totaldana - $subtotal_perkiraan; ?>
        <td style="padding:5px 0 5px 0;text-align: right; width:125px;"><b>{{ rupiah($total_perkiraan) }}</b></td>
        <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
        <td style="padding:5px 0 5px 0; text-align: center; width:250px;"></td>
        @if($recordakuisisi)
        <?php $total_pengeluaran = $sum_tot - $subtotal_pengeluaran; ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($total_pengeluaran) }}</b></td>
        @else
        <?php $total_pengeluaran = $sum_tot; ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($total_pengeluaran) }}</b></td>
        @endif
    </tr>
</table>

</page>
{{-- @endforeach --}}