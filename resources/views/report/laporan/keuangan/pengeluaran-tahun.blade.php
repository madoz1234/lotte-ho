<style type="text/css">

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
.tables td, th {    
    border: 1px solid #ddd;
}

.tables {
    border-collapse: collapse;
}

.tables th {
    font-size: 11px;
    padding: 5px;
    text-align: center;
}
.tables td {
    font-size: 10px;
    padding: 4px;
    text-align: left;
}
.title{
    font-weight: bold;
}
.tittleMargin{
    font-size: 24px;
}

.boreder_head{
    background: #E1F5F3;
    border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;
    padding:5px 0 5px 0;
    font-weight: bold;
}
</style>

<page backtop="60mm" backbottom="5mm" backleft="2mm" backright="2mm" style="font-size: 12pt" orientation="L">
    <page_header>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td id="" style="; width: 90px; height: 5px;">
            <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
        </td>
        <td id="text_header" style=" width: 952px; height: 5px; text-align: center;">
            <p class="title" style=""><h3>PENGELUARAN PERTAHUN</h3>
                <hr style="border: #F90606; margin-top: -3px;" >
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td></td>
    </tr>
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    <tr>
        <td>Tahun Fiskal</td>
        <td>: {{ $tahun_fiskal['tgl_awal'] }} - {{ $tahun_fiskal['tgl_akhir'] }} (Active)</td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>: {{ $start }} - {{ $end }} </td>
    </tr>
    <tr>
        <td>Region</td>
        <td>:  {{ $region }} </td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>: {{ $lsi }} </td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: {{ $tmuk_kode }} - {{ $tmuk }} </td>
    </tr>
</table>
</page_header>
<table class="ui celled compact tables"  cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" style="width:40mm;">Account Name</th>
            @foreach($header as $row)
                <th class="center aligned"  style="width:13.6mm">{{ $row }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $key => $elem)
            {{-- <tr>
                <td colspan="{{ count($header)+1 }}" style="font-size: 11px;">
                   <b>{{ $key }}</b>
                </td>
            </tr> --}}
                <?php
                    $head = $key; 
                    $nil   = $nilai_temp; 
                    $nilai = $nilai_temp;

                    foreach($nilai as $keys => $n){
                        $total[$keys]  = 0; 
                    }
                ?>
                @foreach ($elem as $key => $nils)
                    <tr>
                        <td style="width:40mm;">{{ $head }}</td>
                        @foreach ($nilai as $keys => $val)
                            <td style="width:13.6mm">
                                {{ rupiah(isset($nils[$keys])?$nils[$keys]:$nil[$keys]) }}
                                <?php 
                                    $total[$keys] +=isset($nils[$keys])?$nils[$keys]:$nil[$keys];
                                ?>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                {{-- <tr>
                    <td style="width:40mm; background: #E0FFE9; font-weight: bold;">TOTAL</td>
                    @foreach ($nilai as $keys => $val)
                        <td style="width:13.6mm; background: #E0FFE9; font-weight: bold;">{{ number_format($total[$keys],2) }}</td>
                    @endforeach
                </tr> --}}
        @endforeach
    </tbody>
</table>

</page>
{{-- @endforeach --}}