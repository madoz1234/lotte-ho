<style type="text/css">
    <!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 10px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 10px;}
table.page_content_header {width: 98%; border: none; padding: 1mm; font-size: 10px; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 10px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    /*padding: 10;*/
    /*margin: 2px;*/
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
.tableList th {
    border: 1px solid #CCC;
    padding: 5px;
    border-collapse: collapse;
    background: #f0f5f5;
}
.tableList td {
    border: 1px solid #CCC;
    padding: 5px;
    border-collapse: collapse;
}
-->
</style>
<page backtop="60mm" backbottom="10mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    {{-- <hr> --}}
    <table class="page_header" border="0" align="center">
        <tr>
            <td id="" style="; width: 90px; height: 5px;">
                <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -6px;">
            </td>
            <td id="text_header" style=" width: 620px; height: 5px; text-align: center;">
                <p class="title" style=""><h3>JURNAL PER AKUN</h3>
                    <hr style="border: #F90606; margin-top: -2px;" >
                </p>
            </td>
        </tr>

        <tr>
        <td><br></td>
        <td><br></td>
    </tr>
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    {{-- {{ dd($region) }} --}}
    <tr>
        <td>Tahun Fiskal</td>
        <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>: {{ $request['start'] }} - {{ $request['end'] }} </td>
    </tr>

    <tr>
        <td>Region</td>
        <td>: {{ $region }}  </td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>: {{ $lsi }}  </td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: {{ $tmuk }} </td>
    </tr>

    </table>
    </page_header>

    @foreach ($records as $row)
        @if($row->jurnal->count() !== 0)
        <table cellspacing="0" class="page_content tableList" cellpadding="0" border="1" style="font-size: 9px; margin-bottom: 50px;  margin-top: 10px;">
            <thead>
                <tr>
                    <th style="text-align: center; width:26%; font-weight: bold;">Nama Akun</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">Tanggal</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">ID Transaksi</th>
                    <th style="text-align: center; width:12%; font-weight: bold;">Saldo Awal</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">Perubahan Debit</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">Perubahan Kredit</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">Perubahan Bersih</th>
                    <th style="text-align: center; width:12%; font-weight: bold;">Saldo Akhir</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i           = 1; 
                    $saldo_awal = [];
                    $saldo_akhir = [];
                    ?>
                    @foreach ($row->jurnal->sortBy('tanggal') as $key => $rw)
                        <?php $tanggal = \Carbon\Carbon::parse($rw->tanggal)->format('Y-m-d')?>
                            <tr>
                                <td class="left aligned">{{ $rw->coa_kode }} - {{ $rw->coa->nama }}</td>
                                <td class="center aligned">{{ $tanggal }}</td>
                                <td class="center aligned">{{ $rw->idtrans }}</td>
                                <td class="right aligned">
                                    <?php 
                                        if ($i == 1) {
                                            $saldo_awal[$i] = $saldo_kemarin;
                                        }else{
                                            $saldo_awal[$i] = $saldo_akhir[$i-1];
                                        }
                                    ?>
                                    {{ rupiah($saldo_awal[$i]) }}
                                </td>
                                <td class="right aligned">
                                    <?php 
                                        $jumlah_debit = $rw->posisi=='D' ? $rw->jumlah : '0';
                                    ?>
                                    {{ rupiah(abs($jumlah_debit)) }}
                                </td>
                                <td class="right aligned">
                                    <?php 
                                        $jumlah_kredit = $rw->posisi=='K' ? $rw->jumlah : '0';
                                    ?>
                                    {{ rupiah(abs($jumlah_kredit)) }}
                                </td>
                                <td class="right aligned">
                                    <?php 
                                        $posisi_normal = $row->posisi;

                                        if ($posisi_normal == 'D') {
                                            $bersih = $jumlah_debit - $jumlah_kredit;
                                        }else{
                                            $bersih = $jumlah_kredit - $jumlah_debit;                                        
                                        }

                                    ?>
                                    {{ rupiah($bersih) }}
                                </td>
                                <td class="right aligned">
                                    <?php 
                                        $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                                    ?>
                                    {{ rupiah($saldo_akhir[$i]) }}
                                </td>
                            </tr>
                        <?php $i++; ?>
                    @endforeach
            </tbody>
        </table>
        @endif
    @endforeach
 {{--    <table class="page_footer" border="0" align="center">
        <tr>
            <td style="text-align: center; width:75%">adfsds</td>
            <td style="text-align: center; width:25%">asfd</td>
        </tr>
    </table> --}}


    <page_footer >
    </page_footer>
</page>