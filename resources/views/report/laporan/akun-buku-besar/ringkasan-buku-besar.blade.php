<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 100%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
-->
</style>
<page backtop="68mm" backbottom="3mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
                </td>
                <td id="text_header" style=" width: 600px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>RINGKASAN BUKU BESAR</h3>
                        <hr style="border: #F90606; margin-top: -6px;" >
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            {{-- {{ dd($region) }} --}}
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['start'] }} - {{ $request['end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="text-align: center; width:6%">No</th>
                <th style="text-align: center; width:10%">Tanggal</th>
                <th style="text-align: center; width:30%">Nama Akun</th>
                <th style="text-align: center; width:12%">Saldo Awal</th>
                <th style="text-align: center; width:10%">Perubahan Debit</th>
                <th style="text-align: center; width:10%">Perubahan Kredit</th>
                <th style="text-align: center; width:10%">Perubahan Bersih</th>
                <th style="text-align: center; width:12%">Saldo Akhir</th>
            </tr>
        </table>
    </page_header>
    <table class="page_content" border="0" align="center">
        <col style="text-align: center; width:6%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:30%">
        <col style="text-align: center; width:12%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:12%">
        <?php 
                $i           = 1; 
                $saldo_awal = [];
                $saldo_akhir = [];
            ?>
            @foreach ($records as $data)
                <tr>
                    <td style="width:6%">{{ $i }}</td>
                    <td style="text-align: left; width:10%">{{ \Carbon\Carbon::parse($data->tanggal)->format('Y-m-d') }}</td>
                    <td style="text-align: left; width:30%">{{ $data->coa_kode }} - {{ $data->coa->nama }}</td>
                    <td style="text-align: right; width:12%">
                        <?php 
                            if ($i == 1) {
                                $saldo_awal[$i] = 0;
                            }else{
                                $saldo_awal[$i] = $saldo_akhir[$i-1];
                            }
                        ?>
                        {{ rupiah($saldo_awal[$i]) }}
                    </td>
                    <td style="text-align: right; width:10%">
                        <?php 
                            $jumlah_debit = $data->posisi=='D' ? $data->jumlah : '0';
                        ?>
                        {{ rupiah($jumlah_debit) }}
                    </td>
                    <td style="text-align: right; width:10%">
                        <?php 
                            $jumlah_kredit = $data->posisi=='K' ? $data->jumlah : '0';
                        ?>
                        {{ rupiah($jumlah_kredit) }}
                    </td>
                    <td style="text-align: right; width:10%">
                        <?php 
                            $posisi_normal = $data->coa->posisi;

                            if ($posisi_normal == 'D') {
                                $bersih = $jumlah_debit - $jumlah_kredit;
                            }else{
                                $bersih = $jumlah_kredit - $jumlah_debit;                                        
                            }

                        ?>
                        {{ rupiah($bersih) }}
                    </td>
                    <td style="text-align: right; width:12%">
                        <?php 
                            $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                        ?>
                        {{ rupiah($saldo_akhir[$i]) }}
                    </td>
                </tr>
                <?php $i++; ?>
            @endforeach
    </table>
</page>