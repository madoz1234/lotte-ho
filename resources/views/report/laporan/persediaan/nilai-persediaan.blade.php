<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 100%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content_header_bottom {width: 10%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
-->
</style>
<page backtop="68mm" backbottom="3mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
                </td>
                <td id="text_header" style=" width: 600px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Nilai Persediaan</h3>
                        <hr style="border: #F90606; margin-top: -6px;" >
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['start'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="text-align: left; width:5%;">No</th>
                <th style="text-align: left; width:10%;">Category 1</th>
                <th style="text-align: center; width:10%;">Category 2</th>
                <th style="text-align: center; width:10%;">Kode Produk</th>
                <th style="text-align: center; width:25%;">Nama Produk</th>
                <th style="text-align: right; width:5%;">UOM</th>
                <th style="text-align: center; width:5%;">Qty</th>
                <th style="text-align: center; width:13%;">Cost Price (MAP)</th>
                <th style="text-align: center; width:5%;">Jumlah</th>
            </tr>
        </table>
    </page_header>
    <table class="page_content" border="0" align="center">
        <col style="text-align: center; width:5%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:15%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:25%">
        <col style="text-align: center; width:5%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:10%">
        <?php $i = 1; $total=0;?>
        @foreach ($detail as $row)
            <tr>
                <td style="text-align: center; width:5%">{{ $i }}</td>
                <td style="text-align: center; width:10%">{{ isset($row->produk->l1_nm) ? $row->produk->l1_nm : '-' }}</td>
                <td style="text-align: center; width:15%">{{ isset($row->produk->l2_nm) ? $row->produk->l2_nm : '-' }}</td>
                <td style="text-align: right; width:10%">{{ $row->produk->kode }}</td>
                <td style="text-align: center; width:15%">{{ $row->produk->nama }}</td>
                <td style="text-align: left; width:5%">{{ isset($row->produksetting->uom1_satuan) ? $row->produksetting->uom1_satuan : '-' }}</td>
                <td style="text-align: left; width:10%">
                    <?php
                        $qty = isset($row->detail) ? $row->detail->qty : 0;
                    ?>
                    {{ number_format($qty) }}</td>
                <td style="text-align: right; width:10%">
                    <?php
                            $map = isset($row->detail) ? $row->detail->hpp : 0;
                        ?>
                        {{ FormatNumber($map) }}
                </td>
                <td style="text-align: right; width:10%">
                    {{ FormatNumber($qty*$map) }}
                </td>
            </tr>
        <?php $i++; $total += ($qty*$map); ?>
        @endforeach
    </table>
    <br>

    <table class="page_content_header_bottom" border="0" align="right">
        <tr>
            <th style="text-align: center; width:15%">Total</th>
        </tr>
    </table>
    <table class="page_content" border="0" align="right">
        <tr>
            <td style="text-align: right;width:15%;"><b>{{number_format($total)}}</b></td>
        </tr>
    </table>
</page>