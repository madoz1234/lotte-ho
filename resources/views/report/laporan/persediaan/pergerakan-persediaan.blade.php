<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 100%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content_header2 {width: 100%; height: 5px; border: none; padding: 1mm; font-size: 11px; background-color:#FFFF00; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}

</style>
<page backtop="64mm" backbottom="3mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
                </td>
                <td id="text_header" style=" width: 600px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Pergerakan Persediaan</h3>
                        <hr style="border: #F90606; margin-top: -6px;" >
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk['0']['nama'] }} </td>
            </tr>
            <tr>
                <td>Kategori </td>
                <td>: {{ $tmuk['0']['kategori'] }}</td>
            </tr>
            
            <tr>
                <td>Kode Produk</td>
                <td>: {{ $tmuk['0']['kode'] }}</td>
            </tr>
            <tr>
                <td>Nama Produk</td>
                <td>: {{ $tmuk['0']['nama_produk'] }}</td>
            </tr>

            <tr>
                <td>Tanggal</td>
                <td>: {{ $tmuk['0']['awal'] }} - {{ $tmuk['0']['akhir'] }} </td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="text-align: center; width:15%;">Date</th>
                <th style="text-align: center; width:8%;">Receiving</th>
                <th style="text-align: center; width:8%;">Sales</th>
                <th style="text-align: center; width:8%;">Returning</th>
                <th style="text-align: center; width:8%;">Adjustment</th>
                <th style="text-align: center; width:8%;">Stock Opname</th>
                <th style="text-align: center; width:8%;">Payment Request</th>
                <th style="text-align: center; width:8%;">Balance Qty</th>
                <th style="text-align: center; width:12%;">HPP</th>
                <th style="text-align: center; width:15%;">Balance Amount</th>
            </tr>
        </table>
    </page_header>
    <table class="page_content_header2" border="0" align="center">
    	<tr>
    		<th style="text-align: center; width:15%;">Begining Stock</th>
    		<th style="text-align: center; width:8%;"></th>
    		<th style="text-align: center; width:8%;"></th>
    		<th style="text-align: center; width:8%;"></th>
    		<th style="text-align: center; width:8%;"></th>
    		<th style="text-align: center; width:8%;"></th>
    		<th style="text-align: center; width:8%;"></th>
    		<th style="text-align: center; width:8%;">{{ number_format($begining['0']['qty']) }}</th>
    		<th style="text-align: center; width:12%;">{{ number_format($begining['0']['hpp']) }}</th>
    		<th style="text-align: center; width:15%;">{{ number_format($begining['0']['nilai']) }}</th>
    	</tr>
    </table>
    <table class="page_content" border="0" align="center">
        	<col style="text-align: center; width:15%;">
	        <col style="text-align: center; width:8%;">
	        <col style="text-align: center; width:8%;">
	        <col style="text-align: center; width:8%;">
	        <col style="text-align: center; width:8%;">
	        <col style="text-align: center; width:8%;">
	        <col style="text-align: center; width:8%;">
	        <col style="text-align: center; width:8%;">
	        <col style="text-align: center; width:12%;">
	        <col style="text-align: center; width:15%;">
	        	@for($i=0;$i<$hitung;$i++)
		            <tr>
		                <td style="text-align: center; width:15%;">{{ $hasil[$i]['date'] }}</td>
		                <td style="text-align: center; width:8%;">{{ number_format($hasil[$i]['gr']) }}</td>
		                <td style="text-align: center; width:8%;">{{ number_format($hasil[$i]['jual']) }}</td>
		                <td style="text-align: center; width:8%;">{{ number_format($hasil[$i]['rr']) }}</td>
		                <td style="text-align: center; width:8%;">{{ number_format($hasil[$i]['ad']) }}</td>
		                <td style="text-align: center; width:8%;">{{ number_format($hasil[$i]['so']) }}</td>
		                <td style="text-align: center; width:8%;">{{ number_format($hasil[$i]['pyr']) }}</td>
		                <td style="text-align: center; width:8%;">{{ number_format($hasil[$i]['qty']) }}</td>
		                <td style="text-align: center; width:12%;">{{ number_format($hasil[$i]['hpp']) }}</td>
		                <td style="text-align: center; width:15%;">{{ number_format($hasil[$i]['nilai']) }}</td>
		            </tr>
	            @endfor
    </table>
</page>