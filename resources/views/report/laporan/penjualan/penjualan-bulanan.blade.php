<style type="text/css">
    <!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    /*padding: 10;*/
    /*margin: 2px;*/
    font-size: 24px;
}

/*#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
*/
.boreder_head{
    background: #E1F5F3;
    border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;
    padding:5px 0 5px 0;
    font-weight: bold;
}
</style>
{{-- @foreach ($record as $records) --}}
<page backtop="50mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td id="" style="; width: 90px; height: 5px;">
            <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
        </td>
        <td id="text_header" style=" width: 652px; height: 5px; text-align: center;">
            <p class="title" style=""><h3>Rekap Penjualan Bulanan</h3>
                <hr style="border: #F90606; margin-top: -6px;" >
            </p>

        </td>
    </tr>
    <tr>
        <td><br></td>
        <td><br></td>
    </tr>
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    <tr>
        <td>Tahun Fiskal</td>
        <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>: {{ $request['start'] }} - {{ $request['end'] }} </td>
    </tr>

    <tr>
        <td>Region</td>
        <td>: {{ $region }}  </td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>: {{ $lsi }}  </td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: {{ $tmuk }} </td>
    </tr>
</table>
</page_header>
<table class="page_content" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center" style="border-spacing: 0; font-size: 11px; border-collapse: collapse; margin-left: -40px; border-bottom:1px solid #CCC">
    <tr class="boreder_head">
        <th class="boreder_head" style="text-align: center; width:100px;">Periode</th>
        <th class="boreder_head" style="text-align: right; width:130px;">Jumlah Struk</th>
        <th class="boreder_head" style="text-align: right; width:140px;">Total Sales</th>
        <th class="boreder_head" style="text-align: right; width:150px;">Total Hpp</th>
        <th class="boreder_head" style="text-align: right; width:130px;">Profit</th>
        <th class="boreder_head" style="text-align: right; width:100px;">Margin</th>
    </tr>

    <?php $i = 1; ?>
    <?php $all_struk            = 0;?>
    <?php $all_total_penjualan  = 0;?>
    <?php $all_total_hpp        = 0;?>
    <?php $all_total_profit     = 0;?>
    @foreach ($records->sortBy('month') as $row)
    {{-- test --}}
    <?php
          $hasilprofit = $row->total_penjualan - $row->total_hpp; 
          $profitmargin = ($row->total_penjualan / $hasilprofit)*100; 
    ?>
    {{-- test --}}
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: center; width:100px;">{{ $row->tanggal }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:130px;">{{ rupiah($row->struk) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:140px;">{{ rupiah($row->total_penjualan) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:150px;">{{ rupiah($row->total_hpp) }}</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:130px;">{{ rupiah($hasilprofit) }}</td>
        @if ($row->total_hpp == '')
            <td style="padding:5px 0 5px 0; text-align: right; width:100px;">0</td>
        @else
            <td style="padding:5px 0 5px 0; text-align: right; width:110px;">{{ rupiah($profitmargin) }}%</td>
        @endif
    </tr>
    <?php $i++; ?>
    <?php $all_struk             += $row->struk; ?>
    <?php $all_total_penjualan   += $row->total_penjualan; ?>
    <?php $all_total_hpp         += $row->total_hpp; ?>
    <?php $all_total_profit      += $hasilprofit; ?>
    @endforeach
    
    <tr >
        <td style="padding:5px 0 5px 0;text-align: right; width:100px;"></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:130px;"></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:140px;"></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:150px;"></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:130px;"></td>
        <td style="padding:5px 0 5px 0; text-align: right; width:100px;"></td>
    </tr>
    <tr >
        <td style="padding:5px 0 5px 0;text-align: right; width:100px;"></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:130px;"></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:140px;"></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:150px;"></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:130px;"></td>
        <td style="padding:5px 0 5px 0; text-align: right; width:100px;"></td>
    </tr>
    <tr >
        <td style="padding:5px 0 5px 0;text-align: right; width:100px;"><b>Grand Total</b></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:130px;"><b>{{ rupiah($all_struk) }}</b></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:140px;"><b>{{ rupiah($all_total_penjualan) }}</b></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:150px;"><b>{{ rupiah($all_total_hpp) }}</b></td>
        <td style="padding:5px 0 5px 0;text-align: right; width:130px;"><b>{{ rupiah($all_total_profit) }}</b></td>
        <td style="padding:5px 0 5px 0; text-align: right; width:100px;"></td>
    </tr>
    <tr>
        <td colspan="6" style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>
</table>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <?php 
                $jumrow = $i - 1;
                $spd = $all_total_penjualan / $jumrow; 
             ?>
            <td>SPD</td>
            <td>: {{ rupiah($spd) }}  </td>
        </tr>
        <tr>
            <?php
                $jumrow = $i - 1;
                $std = $all_struk / $jumrow;
            ?>
            <td>STD</td>
            <td>: {{ rupiah($std) }}  </td>
        </tr>
        <tr>
            <?php
                $apc = $all_total_penjualan / $all_struk;
            ?>
            <td>APC</td>
            <td>: {{ rupiah($apc) }} </td>
        </tr>
    </table>
</page>
{{-- @endforeach --}}