<style type="text/css">
    <!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    /*padding: 10;*/
    /*margin: 2px;*/
    font-size: 24px;
}

/*#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
*/
.boreder_head{
    background: #E1F5F3;
    border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;
    padding:5px 0 5px 0;
    font-weight: bold;
}
</style>
{{-- @foreach ($record as $records) --}}
<page backtop="50mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td id="" style="; width: 90px; height: 5px;">
            <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
        </td>
        <td id="text_header" style=" width: 652px; height: 5px; text-align: center;">
            <p class="title" style=""><h3>Setoran Penjualan</h3>
                <hr style="border: #F90606; margin-top: -6px;" >
            </p>

        </td>
    </tr>
    <tr>
        <td><br></td>
        <td><br></td>
    </tr>
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    <tr>
        <td>Tahun Fiskal</td>
        <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>: {{ $request['start'] }} - {{ $request['end'] }} </td>
    </tr>

    <tr>
        <td>Region</td>
        <td>: {{ $region }}  </td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>: {{ $lsi }}  </td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: {{ $tmuk }} </td>
    </tr>
</table>
</page_header>
<table class="page_content" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center" style="border-spacing: 0; font-size: 11px; border-collapse: collapse; margin-left: -40px; border-bottom:1px solid #CCC">
    <tr class="boreder_head">
        <th class="boreder_head" style="text-align: center; width:250px;">Tanggal</th>
        <th class="boreder_head" style="text-align: center; width:250px;">Penjualan</th>
        <th class="boreder_head" style="text-align: center; width:250px;">Setoran</th>
    </tr>
    {{-- {{ dd($records) }} --}}
    <?php $tot_penjualan = 0 ?>
    <?php $tot_setoran = 0 ?>
    @foreach ($records->sortBy('date') as $row)
    {{-- {{ dd($row->date?) }} --}}
    <tr>
        <td style="padding:5px 0 5px 0; text-align: center; width:250px;">{{ $row->date }}</td>
        <td style="padding:5px 0 5px 0; text-align: center; width:250px;">{{ rupiah($row->total_penjualan) }}</td>
        <td style="padding:5px 0 5px 0; text-align: center; width:250px;">
            <?php $setoran=0; ?>
                @foreach($saldo as $tn)
                    @if($tn->date==$row->date)
                        <?php $setoran=$setoran+($tn->total);?>
                    @endif
                @endforeach
                {{ rupiah($setoran) }}
        </td>
    </tr>
    <?php $tot_penjualan +=$row->total_penjualan; ?>
    <?php $tot_setoran +=$setoran; ?>
    @endforeach
    <tr>
        <td colspan="3" style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>

    <tr >
        <td style="padding:5px 0 5px 0;text-align: center; width:250px;"></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:250px;"><b></b></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:250px;"></td>
    </tr>

    <tr >
        <td style="padding:5px 0 5px 0;text-align: center; width:250px;">Grand Total (Rp)</td>
        <td style="padding:5px 0 5px 0;text-align: center; width:250px;">{{ rupiah($tot_penjualan) }}</td>
        <td style="padding:5px 0 5px 0;text-align: center; width:250px;">{{ rupiah($tot_setoran) }}</td>
    </tr>

    <tr>
        <td colspan="3" style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>
</table>
</page>
{{-- @endforeach --}}