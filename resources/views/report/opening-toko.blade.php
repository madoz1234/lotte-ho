<style type="text/css">

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-bottom: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
</style>
<page backtop="73mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="text_header">
                    <p class="title tittleMargin">PURCHASE ORDER &nbsp;<span style="font-size: 12px">(Opening Toko)</span></p>
                    <barcode dimension="1D" type="C128" value="{{ $record->nomor_pr or "" }}" label="label" style="width:78mm; height:10mm;"></barcode>
                </td>
            </tr>
            <tr>
                <td>Halaman [[page_cu]] dari [[page_nb]]</td>
            </tr>
            <tr>
                <td>User : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i') }})</td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 40%">LSI :</td>
                <td style="width: 40%">TMUK :</td>
                <td style="width: 20%"></td>
            </tr>
            <tr>
                <td style="width: 40%; vertical-align: text-top;">{{ $record->tmuk->lsi->kode or "" }} - {{ $record->tmuk->lsi->nama or "" }}</td>
                <td style="width: 40%; vertical-align: text-top;">{{ $record->tmuk->kode or "" }} - {{ $record->tmuk->nama or "" }}</td>
                <td style="width: 20%; vertical-align: text-top;"></td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 25%">No PO : {{ $record->nomor_pr or "" }}</td>
                <td style="width: 20%">Tanggal PO : {{ $record->tgl_buat or "" }}</td>
                <td style="width: 20%">PO Type : Lotte</td>
                <td style="width: 20%">Expected Delivery Date : -{{-- 30/03/2018 --}}</td>
                <td style="width: 15%"></td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="width:8%">Kode Produk</th>
                <th style="width:10%">Barcode</th>
                <th style="width:27%">Nama Produk</th>
                <th style="width:8%; text-align: center;">Qty/UOM</th>
                <th style="width:8%; text-align: center;">UOM</th>
                <th style="width:10%; text-align: center;">Order Qty</th>
                <th style="width:12%; text-align: right;">Harga Satuan</th>
                <th style="width:12%; text-align: right;">Total Harga</th>
                <th style="width:5%; text-align: right;"></th>
            </tr>
        </table>
    </page_header>

    <table class="page_content" border="0" align="center">
        <col style="width: 8%">
        <col style="width: 10%">
        <col style="width: 32%">
        <col style="width: 8%">
        <col style="width: 8%">
        <col style="width: 10%">
        <col style="width: 12%">
        <col style="width: 12%">

        <?php $i = 1; ?>
        {{-- @foreach ($record->detailOpening as $row) --}}
        @foreach ($detail as $row)
            <tr>
                <td style="width:8%">{{ $row['produk_kode'] }}</td>
                <td style="width:10%">{{ $row['uom1_barcode'] }}</td>
                <td style="width:27%">{{ $row['nama'] }}</td>
                <td style="width:8%; text-align: center;">{{ $row['stok_gmd'] }}</td>
                <td style="width:8%; text-align: center;">{{ $row['uom1_satuan'] }}</td>
                <td style="width:10%; text-align: center;">{{ $row['qty_po'] }}</td>
                <td style="width:12%; text-align: right;"><?php echo number_format($row['curr_sale_prc']).'.00'; ?></td>
                <td style="width:12%; text-align: right;"><?php echo number_format($row['total_harga']).'.00'; ?></td>
                <th style="width:5%; text-align: right;"></th>
            </tr>
        <?php $i++; ?>
        @endforeach
    </table>

    <end_last_page end_height="37mm">
        <table class="page_footer" align="center" border="0">
           
            <tr>
                <th style="width: 8%; text-align: right;">&nbsp;</th>
                <th style="width: 8%; text-align: right;">&nbsp;</th>
                <th style="width: 37%; text-align: right;">&nbsp;</th>
                {{-- <th style="width: 37%; text-align: left; font-size: 14px; font-weight: bold; border-bottom: solid 0.3mm #000000;">Bill no = </th> --}}
                <th style="width: 30%; text-align: center;">TOTAL PO</th>
                <th style="width: 12%; text-align: right;"><?php echo number_format($sub_total).'.00'; ?></th>
                <th style="width: 5%; text-align: right;">&nbsp;</th>
            </tr>

            <tr>
                <th colspan="6">&nbsp;</th>
            </tr>
        </table>
    </end_last_page>
</page> 