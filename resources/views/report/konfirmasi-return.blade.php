<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
-->
</style>
<page backtop="68mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="text_header">
                    <p class="title tittleMargin">RETURN CONFIRMED</p>
                    <barcode dimension="1D" type="C128" value="2018012906007000180029" label="label" style="width:70mm; height:10mm;"></barcode>
                </td>
            </tr>
            <tr>
                <td>Halaman [[page_cu]] dari [[page_nb]]</td>
            </tr>
            <tr>
                <td>User : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i') }})</td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 30%">RR Number :</td>
                <td style="width: 20%">Date :</td>
                <td style="width: 25%">IMM/TMUK Store :</td>
                <td style="width: 25%">Lotte Store :</td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="width:10%">Product Code</th>
                <th style="width:10%">Barcode</th>
                <th style="width:20%">Product Description</th>
                <th style="text-align: center; width:6%">Selling Unit</th>
                <th style="text-align: center; width:10%">Return <br>Request Qty</th>
                <th style="text-align: center; width:8%">Price</th>
                <th style="text-align: center; width:8%; text-align: center">Total Price</th>
                <th style="text-align: center; width:18%">Reason</th>
                <th style="text-align: center; width:10%">Return <br>Confirmed Qty</th>
            </tr>
        </table>
    </page_header>

    <table class="page_content" border="0" align="center">
        <col style="width: 10%;">
        <col style="width: 10%;">
        <col style="width: 20%;">
        <col style="text-align: center; width: 6%;">
        <col style="text-align: center; width: 10%;">
        <col style="text-align: center; width: 8%;">
        <col style="text-align: center; width: 8%;">
        <col style="text-align: center; width: 18%;">
        <col style="text-align: center; width: 10%;">
        {{-- @foreach ($detail as $row)
            <tr>
                <td style="width:10%">{{ $row['bumun_nm'] }}</td>
                <td style="width:12%">{{ $row['l1_nm'] }}</td>
                <td style="width:8%">{{ $row['produk_kode'] }}</td>
                <td style="width:10%">{{ $row['uom1_barcode'] }}</td>
                <td style="width:28%">{{ $row['nama'] }}</td>
                <td style="width:6%">{{ $row['qty_order'] }} {{ $row['unit_order'] }}</td>
                <td style="text-align: center; width:6%;">{{ $row['qty_sell'] }} {{ $row['unit_sell'] }}</td>
                <td style="text-align: center; width:6%;">{{ $row['stok_gmd'] }}</td>
                <td style="text-align: center; width:6%;">{{ $row['qty_pr'] }}</td>
                <td style="text-align: center; width:8%;"></td>
            </tr>
        @endforeach --}}
    </table>

    <end_last_page end_height="37mm">
        <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Return By
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
            </tr>
        </table>
    </end_last_page>
</page>