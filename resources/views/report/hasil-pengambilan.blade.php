<style type="text/Css">
<!--
.test1
{
    border: solid 1px #FF0000;
    background: #FFFFFF;
    border-collapse: collapse;
}

/*td{
    border: 1px solid #000;
}*/
-->
</style>
<page style="font-size: 14px">
    <p style="text-align: right;"><span style="font-weight: bold; font-size: 18pt;">HASIL PENGAMBILAN<br></span><br>
    <barcode dimension="1D" type="CODABAR" value="201803280600700018659" label="label"></barcode></p>
    <br>
    Halaman 1 dari 1 <br>
    User : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i') }})
    <br>
    <br>
    <table>
        
    </table>
    <table style="border: solid 0px #440000; width: 80%" cellspacing="0">
        <tr>
            <td style="width: 23%">No Picking :</td>
            <td style="width: 25%">NO PR :</td>
            <td style="width: 15%">Tanggal</td>
            <td style="width: 21%">Toko Lotte</td>
            <td style="width: 21%">IMM/TMUK</td>
            <td style="width: 21%">Jumlah Produk</td>
        </tr>
        <tr>
            <td style="width: 23%">pt-201803280600700018659</td>
            <td style="width: 25%">PR-2018032806007000180007</td>
            <td style="width: 15%">28/03/2018</td>
            <td style="width: 21%">LOTTE GROSIR ALAM SUTERA, TANGERANG (06007)</td>
            <td style="width: 21%">Pragma Informatika</td>
            <td style="width: 21%">3</td>
        </tr>
    </table><br>
<hr>
    <table style="border: solid 0px #440000; width: 80%" cellspacing="0">
        <tr>
            <th style="width: 10%">BUMUN</th>
            <th style="width: 15%">Kategori 1</th>
            <th style="width: 15%">Kode Produk</th>
            <th style="width: 15%">Barcode</th>
            <th style="width: 18%">Nama Produk</th>
            <th style="width: 10%">Order</th>
            <th style="width: 10%">Selling</th>
            <th style="width: 8%">Stock</th>
            <th style="width: 10%">Pick Qty</th>
            <th style="width: 13%">Hasil Pick Qty</th>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td>Unit</td>
            <td>Unit</td>
            <td>GMD</td>
            <td style="text-align: center;">Qty</td>
            <td style="text-align: center;">Qty</td>
        </tr>
    </table><br>
<hr>

    <table style="border: solid 0px #440000; width: 80%" cellspacing="0">
        <tr>
            <td style="width: 10%">DRY FOOD</td>
            <td style="width: 15%">Biscuit/Snacks</td>
            <td style="width: 15%">1054024000</td>
            <td style="width: 15%">089686611908</td>
            <td style="width: 18%">QTELA KERIPIK TEMPE ORIG 70/60 G</td>
            <td style="width: 10%">1 Pcs</td>
            <td style="width: 10%">1 Pcs</td>
            <td style="text-align: center; width: 8%">79</td>
            <td style="text-align: center; width: 10%">3 Pcs</td>
            <td style="text-align: center; width: 13%">3 Pcs</td>
        </tr>
        <tr>
            <td style="width: 10%"></td>
            <td style="width: 15%">Drink/Milk</td>
            <td style="width: 15%">1039747000</td>
            <td style="width: 15%">9415007009146</td>
            <td style="width: 18%">ANLENE GOLD BOX 600 G</td>
            <td style="width: 10%">1 Pcs</td>
            <td style="width: 10%">1 Pcs</td>
            <td style="text-align: center; width: 8%">25</td>
            <td style="text-align: center; width: 10%">3 Pcs</td>
            <td style="text-align: center; width: 13%">3 Pcs</td>
        </tr>
        <tr>
            <td style="width: 10%"></td>
            <td style="width: 15%">H&B</td>
            <td style="width: 15%">1061791000</td>
            <td style="width: 15%">8991038775416</td>
            <td style="width: 18%">SELECTION COTTON BUD BOX 180 PCS</td>
            <td style="width: 10%">1 Pcs</td>
            <td style="width: 10%">1 Pcs</td>
            <td style="text-align: center; width: 8%">56</td>
            <td style="text-align: center; width: 10%">3 Pcs</td>
            <td style="text-align: center; width: 13%">3 Pcs</td>
        </tr>
    </table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <hr>
</page>