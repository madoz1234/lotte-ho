<link rel="stylesheet" href="{{ asset("font/code128/stylesheet.css") }}" type="text/css" charset="utf-8" />
<style type="text/css">

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 100%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}

.digital {
    font-family: "code_128regular";
    width: 100%;
}
</style>


<page backtop="74mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" width="" align="center">
            <tr>
                <td style="width: 480px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="150" style="margin-top: 10px;">
                </td>
                <td style="width: 480px;" id="text_header">
                    <p class="title" style="font-size: 21px;">PICKING RESULT</p>
                    <div class="digital" style="font-size: 50px;">{{ $record->nomor or "" }}</div>
                    <div style="font-size: 25px;">{{ $record->nomor or "" }}</div>
                    <!-- <barcode dimension="1D" type="C128" value="{{ $record->nomor or "" }}" label="label" style="width:78mm; height:10mm;"></barcode> -->
                </td>
            </tr>
            <tr>
                <!-- <td>Halaman [[page_cu]] dari [[page_nb]]</td> -->
                <td></td>
            </tr>
            <tr>
                <td><b>User</b> : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }})</td>
                <td></td>
            </tr>
        </table>



        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 18%">No Picking :</td>
                <td style="width: 10%">Tanggal Picking :</td>
                <td style="width: 18%">No PR :</td>
                <td style="width: 8%">Tanggal PR :</td>
                <td style="width: 18%">LSI :</td>
                <td style="width: 18%">TMUK :</td>
                <td style="width: 10%">Jumlah Produk :</td>
            </tr>
            <tr>
                <td style="width: 18%">{{ $record->nomor or "" }}</td>
                <td style="width: 10%">{{ $record->updated_at or "" }}</td>
                {{-- <td style="width: 10%">{{ \Carbon\Carbon::$record->updated_at->format('d/m/Y h:i:s') or "" }}</td> --}}
                <td style="width: 18%">{{ $record->nomor or "" }}</td>
                <td style="width: 8%">{{ $record->tgl_buat or "" }}</td>
                <td style="width: 18%">{{ $record->tmuk->lsi->kode }} - {{ $record->tmuk->lsi->nama }}</td>
                <td style="width: 18%">{{ $record->tmuk_kode }} - {{ $record->tmuk->nama }}</td>
                <td style="width: 10%">{{ isset($detail) ? count($detail) : '' }}</td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="text-align: center; width:10%">BUMUN</th>
                <th style="text-align: center; width:12%">Kategori 1</th>
                <th style="text-align: center; width:8%">Kode Produk</th>
                <th style="text-align: center; width:10%">Barcode</th>
                <th style="text-align: left; width:28%">Nama Produk</th>
                <th style="text-align: left; width:6%">Order <br>Unit</th>
                <th style="text-align: center; width:6%; text-align: center">Selling Unit</th>
                <th style="text-align: center; width:6%">Stock GMD</th>
                <th style="text-align: center; width:6%">Pick Qty Qty</th>
                <th style="text-align: center; width:8%">Hasil Pick Qty Qty</th>
            </tr>
        </table>
    </page_header>
    <br>

    <table class="page_content" border="0" align="center">
        <col style="width: 10%">
        <col style="width: 12%">
        <col style="width: 8%">
        <col style="width: 10%">
        <col style="width: 28%">
        <col style="width: 6%">
        <col style="width: 6%">
        <col style="width: 6%">
        <col style="width: 6%; border-bottom: solid 0.3mm #000000;">
        <col style="width: 8%; border-bottom: solid 0.3mm #000000;">
        <?php $result = array(); ?>
        @foreach ($detail as $key => $row)
            <tr>
                <td style="text-align: center; width:10%">{{ $row['bumun_nm'] }}</td>
                <td style="text-align: center; width:12%">
                    @if($key == 0)
                        <?php 
                            $result[$key] = $row['l1_nm'];
                        ?>
                        
                        {{ $row['l1_nm'] }}
                    @else
                        @if($row['l1_nm'] === $result[$key-1])
                            <?php 
                                $result[$key] = $row['l1_nm'];
                            ?>
                        @else
                            <?php 
                                $result[$key] = $row['l1_nm'];
                            ?>
                            
                            {{ $row['l1_nm'] }}
                        @endif
                    @endif
                </td>
                <td style="text-align: center; width:8%">{{ $row['produk_kode'] }}</td>
                <td style="text-align: center; width:10%">{{ $row['uom1_barcode'] }}</td>
                <td style="width:28%">{{ $row['nama'] }}</td>
                <td style="width:6%">{{ $row['qty_order'] }} {{ ($row['unit_order'] != "") ? ($row['unit_order']) : 'Pcs' }}</td>
                <td style="text-align: center; width:6%;">{{ $row['qty_sell'] }} {{ ($row['unit_sell'] != "") ? ($row['unit_sell']) : 'Pcs' }}</td>
                <td style="text-align: center; width:6%;">{{ $row['stok_gmd'] }}</td>
                <td style="text-align: center; width:6%;">{{ $row['qty_pr'] }}</td>
                <td style="text-align: center; width:8%;">{{ $row['qty_pick'] }}</td>
            </tr>
        @endforeach
    </table>

    <!-- <end_last_page end_height="37mm">
        <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    PIC Pengambilan Barang
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Di Serahkan Kepada
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
            </tr>
        </table>
    </end_last_page> -->
</page>