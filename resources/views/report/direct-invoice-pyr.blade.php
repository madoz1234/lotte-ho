<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 96%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 96%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 96%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 0px;
}
-->
</style>
<page backtop="82mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" width="" align="center">
            <tr>
                <td style="width: 300px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="120" style="margin-top: 10px;">
                </td>
                <td style="width: 400px;" id="text_header">
                    <p class="title" style="font-size: 21px;">DIRECT INVOICE PYR</p>
                    <barcode dimension="1D" type="C128" value="{{ $record->nomor_pyr or "" }}" label="label" style="width:78mm; height:10mm;"></barcode>
                </td>
            </tr>
            <tr>
                <td>Halaman [[page_cu]] dari [[page_nb]]</td>
                <td></td>
            </tr>
            <tr>
                <td><b>User</b> : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }})</td>
                <td></td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 35%; font-weight: bold;">Order To :</td>
                <td style="width: 30%"></td>
                <td style="text-align: right; width: 35%; font-weight: bold;">IMM/TMUK Store : </td>
            </tr>
            <tr>
                <td style="width: 35%;">{{ $vendor or '-'}}</td>
                <td style="width: 30%"></td>
                <td style="text-align: right; width: 35%">{{ $record->tmuk->nama.' ('.$record->tmuk->kode.')' }}</td>
            </tr>
            <tr>
                <td style="width: 35%;">{{ $alamat_vendor or '-' }}</td>
                <td style="width: 30%"></td>
                <td style="text-align: right; width: 35%">{{ $record->tmuk->alamat }}</td>
            </tr>
            <tr>
                <td style="width: 35%;">{{ $tlp_vendor or '-'}}</td>
                <td style="width: 30%"></td>
                <td style="text-align: right; width: 35%">{{ $record->tmuk->telepon }}</td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 50%">No PYR : {{ $record->nomor_pyr }}</td>
                <td style="width: 50%">Tanggal PYR : {{ $record->tgl_buat }}</td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="text-align:center; width:5%">No.</th>
                <th style="text-align:center; width:15%">Kode Produk</th>
                <th style="text-align:center; width:30%">Nama Produk</th>
                <th style="text-align:center; width:15%">Quantity</th>
                <th style="text-align:center; width:15%">Unit</th>
                <th style="text-align:center; width:20%">Harga (Rp)</th>
            </tr>
        </table>
        <table class="page_content" border="0" align="center">
            <col style="text-align:center; width:5%">
            <col style="text-align:center; width:15%">
            <col style="text-align:center; width:30%">
            <col style="text-align:center; width:15%">
            <col style="text-align:center; width:15%">
            <col style="text-align:center; width:20%">
            @foreach ($record->detailPyr as $key => $row)
                <tr>
                    <td style="text-align:center; width:5%">{{ $key+1 }}</td>
                    <td style="width:15%">{{ $row->produk_kode }}</td>
                    <td style="width:30%">{{ $row->produks->nama}}</td>
                    <td style="text-align:center; width:15%">{{ $row->qty }}</td>
                    <td style="text-align:center; width:15%">{{ ($row->unit != 0) ? $row->unit : '-' }}</td>
                    <td style="text-align:right; width:20%">{{ FormatNumber($row->price) }}</td>
                </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center; font-weight: bold; width:15%">Total PYR</td>
                <td style="text-align:right; font-weight: bold; width:20%">{{ FormatNumber($record->total) }}</td>
            </tr>
        </table>
    </page_header>

    <end_last_page end_height="47mm">
        <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 35%;text-align: center;">
                    Diajukan Oleh,
                </td>
                <td style=" width: 30%;text-align: left;">
                </td>
                <td style=" width: 35%;text-align: center;">
                    Disetujui Oleh,
                </td>
            </tr>
            <tr>
                <td style=" width: 35%;text-align: center; padding: 10px; padding-top: 60px">
                    ( Kepala Toko )
                </td>
                <td style=" width: 30%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 35%;text-align: center; padding: 10px; padding-top: 60px">
                    ( {{ $record->tmuk->rekeningescrow->nama_pemilik }} )
                </td>
            </tr>
        </table>
    </end_last_page>
</page>