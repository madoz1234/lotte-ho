<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
    }*/

    table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
    table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
    table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
    table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

    .title{
        font-weight: bold;
    }
    .tittleMargin{
        padding: 10;
        margin: 2px;
        font-size: 24px;
    }

    #text_header{
        width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
    }
    -->
</style>
<page backtop="68mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    <table class="page_header" style="width: 100%;margin: 5px;" border="0">
        <tr>
            <td style="width: 20%">
                <img src="{{ asset('img/logo_print.jpeg') }}" style="height: 100px; width: 200px;">
            </td>
            <td style="width: 50%"></td>
            <td id="text_header" style="width: 30%">
                <p class="title tittleMargin">SURAT JALAN</p>
                <barcode dimension="1D" type="C128" value="{{ $record->nomor }}" label="label" style="width:50mm; height:10mm;"></barcode>
            </td>
        </tr>
    </table>
    <br>
    <br>
    {{-- <table class="page_header" border="0" align="center">
        <tr>
            <td id="text_header">
                <p class="title tittleMargin">SURAT JALAN</p>
                <barcode dimension="1D" type="C128" value="{{ $record->nomor }}" label="label" style="width:50mm; height:10mm;"></barcode>
            </td>
        </tr>
        <tr>
            <td>Halaman [[page_cu]] dari [[page_nb]]</td>
        </tr>
        <tr>
            <td>User : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i') }})</td>
        </tr>
    </table> --}}
    <table class="page_header" border="0" align="center">
            {{-- <tr>
                <th style="width: 35%">Pemasok :</th>
                <th style="width: 35%">IMM/TMUK :</th>
                <th style="width: 30%"></th>
            </tr> --}}
            <tr>
                {{-- <td style="width: 35%; vertical-align: top;">{{ $record->tmuk->lsi->nama }}</td>
                <td style="width: 35%; vertical-align: top;">{{ $record->tmuk->nama }} ({{ $record->tmuk->kode }}) {{ $record->tmuk->alamat }}</td>
                <td style="width: 30%"></td> --}}
                <td style="width: 35%; vertical-align: top;"></td>
                <td style="width: 35%; vertical-align: top;"></td>
                <td style="width: 30%"></td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center" style="border-bottom: solid 0.3mm #000000;">
            <tr>
                <th style="width: 20%">No Daftar Muatan:</th>
                <th style="width: 15%">Nama Supir:</th>
                <th style="width: 15%">Plat No Kendaran:</th>
                <th style="width: 15%">Total Label:</th>
                <th style="width: 20%">No Surat Jalan:</th>
                <th style="width: 15%">Tanggal Pengiriman:</th>
            </tr>
            <tr>
                <?php $jum_label=0;?>
                @foreach($record->muatan->detail as $md)
                <?php $jum_label = $jum_label + $md->po->kontainer->detail->max('no_kontainer'); ?>
                @endforeach
                <td style="width: 20%">{{ $record->muatan->nomor }}</td>
                <td style="width: 15%">{{ $record->supir }}</td>
                <td style="width: 15%">{{ $record->nomor_kendaraan }}</td>
                <td style="width: 15%">{{ $jum_label }}</td>
                <td style="width: 20%">{{ $record->nomor }}</td>
                <td style="width: 15%">{{ \Carbon\Carbon::now()->format('d/m/Y') }}</td>
            </tr>
        </table>
        </page_header>

        <table class="page_content" border="0" align="center" cellspacing="0">
            <col style="width: 20%">
            <col style="width: 15%">
            <col style="width: 35%">
            <col style="width: 10%; text-align: center">
            <col style="width: 10%; text-align: center">
            <col style="width: 10%; text-align: center">

            {{-- @for($i=0; $i<3; $i++) --}}
            @foreach($record->muatan->detail as $det)
            <tr>
                <th colspan="6">{{ $det->po_nomor }}</th>
            </tr>
            <tr>
                <th style="width: 20%">No Kontainer</th>
                <th style="width: 15%">Barcode</th>
                <th style="width: 35%">Nama Produk</th>
                <th style="width: 10%; text-align: center">Jumlah</th>
                <th style="width: 10%; text-align: center">UOM</th>
                <th style="width: 10%; text-align: center">Jumlah Kirim</th>
            </tr>
            @foreach($det->po->kontainer->detail as $detail)
            <tr>
                <td style="width: 20%">{{ $det->po->kontainer->nomor }}</td>
                <td style="width: 15%">{{ $detail->produk->produksetting->uom2_barcode }}</td>
                <td style="width: 35%">{{ $detail->produk->kode }} {{ $detail->produk->nama }}</td>
                <td style="width: 10%; text-align: center">{{ $detail->jumlah_produk }}</td>
                <td style="width: 10%; text-align: center">{{ $detail->produk->produksetting->uom2_satuan }}</td>
                <td style="width: 10%; text-align: center">{{ $detail->jumlah_produk }}</td>
            </tr>
            @endforeach
            <tr>
                <th colspan="6">&nbsp;</th>
            </tr>
            @endforeach
            {{-- @endfor --}}
        </table>

        <end_last_page end_height="37mm">
        <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Diserahkan Oleh
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Transporter
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
            </tr>
        </table>
        </end_last_page>
    </page>