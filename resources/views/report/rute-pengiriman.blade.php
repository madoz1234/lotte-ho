<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
    }*/

    table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
    table.page_footer {width: 100%; border: none; border-bottom: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
    table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
    table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

    .title{
        font-weight: bold;
    }
    .tittleMargin{
        padding: 10;
        margin: 2px;
        font-size: 24px;
    }

    #text_header{
        width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
    }
    tr.border_bottom td {
      border-bottom:1pt solid black;
  }
  -->
</style>
<page backtop="68mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    <table class="page_header" style="width: 100%;margin: 5px;" border="0">
        <tr>
            <td style="width: 20%">
                <img src="{{ asset('img/logo_print.jpeg') }}" width="150" style="margin-top: 10px;">
            </td>
            <td style="width: 40%"></td>
            <td id="text_header" style="width: 40%">
                <p class="title tittleMargin">Rute Pengiriman</p>
                <barcode dimension="1D" type="C128" value="{{ $record->nomor or "" }}" label="label" style="width:60mm; height:10mm;"></barcode>
            </td>
        </tr>
    </table>
    <br>
    <br>
        {{-- <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 480px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="150" style="margin-top: 10px;">
                </td>
                <td style="width: 10px;" id="text_header">
                    <p class="title tittleMargin">Rute Pengiriman</p>
                    <barcode dimension="1D" type="C128" value="{{ $record->nomor or "" }}" label="label" style="width:60mm; height:10mm;"></barcode>
                </td>
            </tr>
            <tr>
                <td>Halaman [[page_cu]] dari [[page_nb]]</td>
            </tr>
            <tr>
                <td>User : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }})</td>
            </tr>
        </table> --}}
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="width:10%;text-align: center;">Nomor</th>
                <th style="width:30%;text-align: center;">TMUK</th>
                <th style="width:20%;text-align: center;">Nomor Telepon</th>
                <th style="width:40%; text-align: center;">Alamat</th>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <?php $i=1; ?>
            @foreach($record->muatan->detail->groupBy('tmuk_kode') as $detail)
            <tr>
                <td style="width:10%;text-align: center;">{{ $i++ }}</td>
                <td style="width:30%;text-align: center;">{{ $detail->first()->tmuk->nama }}</td>
                <td style="width:20%;text-align: center;">{{ $detail->first()->tmuk->telepon }}</td>
                <td style="width:40%; text-align: center;">{{ $detail->first()->tmuk->alamat }}</td>
            </tr>
            @endforeach
        </table>
        </page_header>
        <br>

        <end_last_page end_height="37mm">
        
        </end_last_page>
    </page>