<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
    }*/

    table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
    table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
    table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
    table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

    .title{
        font-weight: bold;
    }
    .tittleMargin{
        padding: 10;
        margin: 2px;
        font-size: 24px;
    }

    #text_header{
        width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
    }
    -->
</style>
<page backtop="60mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    <table class="page_header" style="width: 100%;margin: 5px;" border="0">
        <tr>
            <td style="width: 20%">
                <img src="{{ asset('img/logo_print.jpeg') }}" style="height: 100px; width: 200px;">
            </td>
            <td style="width: 50%"></td>
            <td id="text_header" style="width: 30%">
                <p class="title tittleMargin">DAFTAR MUATAN</p>
                <barcode dimension="1D" type="C128" value="{{ $record->nomor }}" label="label" style="width:60mm; height:10mm;"></barcode>
            </td>
        </tr>
    </table>
    <br>
    <br>
        {{-- <table class="page_header" border="0" align="center">
            <tr>
                <td id="text_header">
                    <p class="title tittleMargin">DAFTAR MUATAN</p>
                    <barcode dimension="1D" type="C128" value="{{ $record->nomor }}" label="label" style="width:60mm; height:10mm;"></barcode>
                </td>
            </tr>
            <tr>
                <td>Halaman [[page_cu]] dari [[page_nb]]</td>
            </tr>
            <tr>
                <td>User : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i') }})</td>
            </tr>
        </table> --}}
        <table class="page_header" border="0" align="center" style="border-bottom: solid 0.3mm #000000;">
            <tr>
                <th style="width: 20%">No Daftar Muatan</th>
                <th style="width: 10%">Tanggal</th>
                <th style="width: 15%">Total Unit</th>
                <th style="width: 15%">Berat Total</th>
                <th style="width: 10%">Total Label</th>
                <th style="width: 10%">Total Truk</th>
                <th style="width: 10%">Total IMM/TMUK</th>
                <th style="width: 10%">Total PO</th>
            </tr>
            <tr>
                <td style="width: 20%">{{ $record->nomor }}</td>
                <td style="width: 10%">{{ \Carbon\Carbon::now()->format('d/m/Y') }}</td>
                <td style="width: 15%">1</td>
                <td style="width: 15%">0.00 gr</td>
                <td style="width: 10%">1</td>
                <td style="width: 10%">1</td>
                <td style="width: 10%">{{ $record->detail->groupBy('tmuk_kode')->count() }}</td>
                <td style="width: 10%">{{ $record->detail->count() }}</td>
            </tr>
        </table>
        </page_header>

        <table class="page_content" border="0" align="center" cellspacing="0">
            <col style="width: 10%">
            <col style="width: 20%">
            <col style="width: 20%">
            <col style="width: 10%">
            <col style="width: 10%">
            <col style="width: 10%">
            <col style="width: 20%">

            @foreach($detil as $detail)
            <tr>
                <td style="width: 10%; padding-bottom: 8px;">IMM/TMUK</td>
                <td style="width: 40%; padding-bottom: 8px;" colspan="2">{{ $detail->tmuk->nama }} ({{$detail->tmuk->kode}})</td>
                <td style="width: 50%; padding-bottom: 8px;" colspan="4">Toko Lotte : {{ $detail->tmuk->lsi->nama }} ({{ $detail->tmuk->lsi->kode }})</td>
            </tr>
            <tr>
                <td style="width: 10%">&nbsp;</td>
                <td style="width: 20%">ID Kontainer</td>
                <td style="width: 20%">No Picking</td>
                <td style="width: 10%">Berat</td>
                <td style="width: 10%">Jumlah Kontainer</td>
                <td style="width: 10%">Jumlah Outerbox</td>
                <td style="width: 20%">No PO</td>
            </tr>
            <tr>
                <td style="width: 10%">&nbsp;</td>
                <td style="width: 20%">{{ $detail->po->kontainer->nomor }}</td>
                <td style="width: 20%">{{ $detail->po->pr->nomor }}</td>
                <td style="width: 10%">0.00 gr</td>
                <?php $jum_kontainer = $detail->po->kontainer->detail()->whereNotNull('id_kontainer')->max('no_kontainer');?>
                <?php $jum_outer = $detail->po->kontainer->detail->where('id_kontainer',null)->count();?>
                <td style="width: 10%">{{ $jum_kontainer }}</td>
                <td style="width: 10%">{{ $jum_outer }}</td>
                <td style="width: 20%">{{ $detail->po_nomor }}</td>
            </tr>
            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>
            @endforeach

        {{-- @for($i=0; $i<10; $i++)
             <tr>
                <td style="width: 10%; padding-bottom: 8px;">IMM/TMUK</td>
                <td style="width: 40%; padding-bottom: 8px;" colspan="2">Pragma Informatika (0600700018)</td>
                <td style="width: 50%; padding-bottom: 8px;" colspan="4">Toko Lotte : LOTTE GROSIR ALAM SUTERA, TANGERANG (06007)</td>
            </tr>
            <tr>
                <td style="width: 10%">&nbsp;</td>
                <td style="width: 20%">ID Kontainer</td>
                <td style="width: 20%">No Picking</td>
                <td style="width: 10%">Berat</td>
                <td style="width: 10%">Jumlah Kontainer</td>
                <td style="width: 10%">Jumlah Outerbox</td>
                <td style="width: 20%">No PO</td>
            </tr>
            <tr>
                <td style="width: 10%">&nbsp;</td>
                <td style="width: 20%">cl-201803240600700018001</td>
                <td style="width: 20%">pt-201803240600700018645</td>
                <td style="width: 10%">0.00 gr</td>
                <td style="width: 10%">1</td>
                <td style="width: 10%">0</td>
                <td style="width: 20%">PO-1803240600700018769</td>
            </tr>
            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>
            @endfor --}}
        </table>

        <end_last_page end_height="37mm">
        <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Diserahkan Oleh
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Transporter
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
            </tr>
        </table>
        </end_last_page>
    </page>