<style type="text/Css">
/*design table 1*/
/*.table1 {
    font-family: sans-serif;
    color: #000000;
    border-collapse: collapse;
}
 
.table1, th, td {
    border: 1px solid #999;
    padding: 8px 20px;
}*/

/*td{
    border: 1px solid #000;
}*/

</style>
<?php $jum_kontainer = $record->detail()->whereNotNull('id_kontainer')->count();?>
<?php $jum_outer = $record->detail->where('id_kontainer',null)->count();?>
<?php $max = $record->detail->max('no_kontainer');?>
<?php $i=1;?>
@for ($xx = 0; $xx < $max ; $xx++)
<page style="font-size: 14px" backtop="10" backbottom="10">
    <table>
        <tr>
            <td style="padding-left: 20px;">
                <div style="border: 0px solid #000; width: 990px; height:30px;"></div>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 50px;">
                <div style="border: 1px solid #000; width: 990px; height:100px;">
                    <table style="width: 100%;margin: 5px;" border="0">
                        <tr>
                            <td style="width: 20%;padding: 5px;border: 1px solid black;">
                                {{-- @foreach($record->detail as $detail) --}}
                                <h1 style="font-size: 45px;text-align: center">{{ $i }}/{{$max}}</h1>  
                                {{-- @endforeach --}}
                            </td>
                            <td style="width: 60%"><p style="text-align: center;"><span style="font-weight: bold; font-size: 25pt;">{{ $record->tmuk->kode }}<br>{{ $record->tmuk->nama }}</span></p></td>
                            <td style="width: 20%"></td>
                        </tr>
                    </table>
                    {{-- <p style="text-align: center;"><span style="font-weight: bold; font-size: 25pt;">{{ $record->tmuk->kode }}<br>{{ $record->tmuk->nama }}</span></p> --}}
                    <br><br><br><br>
                    <table style="border: solid 0px #440000; width: 80%" cellspacing="0">
                        <tr>
                            <td style="text-align: center; width: 60%"><span style="font-weight: bold; font-size: 20pt;">No PR : </span></td>
                            <td style="text-align: center; width: 60%"><span style="font-weight: bold; font-size: 20pt;">No PO : </span></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 60%"><span style="font-size: 20pt;">{{ $record->po->pr->nomor }}</span></td>
                            <td style="text-align: center; width: 60%"><span style="font-size: 20pt;">{{ $record->po_nomor }}</span></td>
                        </tr>
                    </table><br><br><br>
                    <table style="border: solid 0px #440000; width: 80%" cellspacing="0">
                        <tr>
                            <td style="text-align: center; width: 60%"><span style="font-weight: bold; font-size: 20pt;">No Container : </span></td>
                            <td style="text-align: center; width: 60%"><span style="font-weight: bold; font-size: 20pt;">Toko Lotte : </span></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 60%"><span style="font-size: 20pt;">{{ $record->nomor }}</span></td>
                            <td style="text-align: center; width: 60%"><span style="font-size: 20pt;">{{ $record->tmuk->lsi->nama }}</span></td>
                        </tr>
                    </table><br><br><br><br>
                    <p style="text-align: center;"><span style="font-weight: bold; font-size: 25pt;"><barcode style="width:300px; height:60px; font-size: 4mm" dimension="1D" type="C128" value="{{ $record->nomor }}" label="label"></barcode></span></p><br><br><br><br>

                </div>
            </td>
        </tr>
    </table>
    <?php $i++;?>
</page>
@endfor