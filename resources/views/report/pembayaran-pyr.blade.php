<style type="text/css">

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-bottom: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}

</style>
<page backtop="73mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" width="" align="center">
            <tr>
                <td style="width: 480px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="150" style="margin-top: 10px;">
                </td>
                <td style="width: 480px;" id="text_header">
                    <p class="title" style="font-size: 21px;">PEMBAYARAN PYR</p>
                    <barcode dimension="1D" type="C128" value="{{ $record->nomor_pyr or "" }}" label="label" style="width:78mm; height:10mm;"></barcode>
                </td>
            </tr>
            <tr>
                <td>Halaman [[page_cu]] dari [[page_nb]]</td>
                <td></td>
            </tr>
            <tr>
                <td><b>User</b> : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }})</td>
                <td></td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 40%">LSI :</td>
                <td style="width: 40%">TMUK :</td>
                <td style="width: 20%"></td>
            </tr>
            <tr>
                <td style="width: 40%; vertical-align: text-top;">{{ $record->tmuk->lsi->kode or "" }} - {{ $record->tmuk->lsi->nama or "" }}</td>
                <td style="width: 40%; vertical-align: text-top;">{{ $record->tmuk->kode or "" }} - {{ $record->tmuk->nama or "" }}</td>
                <td style="width: 20%; vertical-align: text-top;"></td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 25%">Reduct Date : {{ $record->verifikasi4_date or $record->created_at }}</td>
                <td style="width: 25%">NO PYR : {{ $record->nomor_pyr or "" }}</td>
                <td style="width: 25%">Due Date : {{ $record->pyr->tgl_jatuh_tempo or "" }}</td>
                <?php 
                    $tipe = $record->pyr->tipe;
                    $string = '';

                    if ($tipe == '001') {
                        $string = 'Trade Lotte';
                    }else if($tipe == '002'){
                        $string = 'Trade Non-Lotte';
                    }else if($tipe == '003'){
                        $string = 'Non Trade Lotte';
                    }else{
                        $string = 'Non Trade Non-Lotte';
                    }
                ?>
                <td style="width: 25%">Pyr Type : {{ $string }}</td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="width:15%">Nama Rekening Asal</th>
                <th style="width:15%">No Rek Bank Asal</th>
                <th style="width:15%">Nama Penerima</th>
                <th style="width:15%">Nama Bank Tujuan</th>
                <th style="width:15%">No Rek Bank Tujuan</th>
                <th style="width:10%">Admin Bank (Rp)</th>
                <th style="width:15%; text-align: right;">Jumlah (Rp)</th>
            </tr>
        </table>
    </page_header>
    <br>
    <table class="page_content" border="0" align="center">
        <col style="width: 15%">
        <col style="width: 15%">
        <col style="width: 15%">
        <col style="width: 15%">
        <col style="width: 15%">
        <col style="width: 10%; text-align: right;">
        <col style="width: 15%; text-align: right;">
            <tr>
                <td style="width:15%">{{ $record->tmuk->rekeningescrow->nama_pemilik or "" }}</td>
                <td style="width:15%">{{ $record->tmuk->rekeningescrow->nomor_rekening or "" }}</td>
                <td style="width:15%">{{ $nama_penerima or "" }}</td>
                <td style="width:15%">{{ $nama_bank_tujuan or "" }}</td>
                <td style="width:15%">{{ $no_rek_tujuan or "" }}</td>
                <td style="width:10%; text-align: right;">{{  FormatNumber($record->where('group', $record->group)->sum('biaya_admin')) }}</td>
                <td style="width:15%; text-align: right;">{{  FormatNumber($record->pyr->where('group', $record->group)->sum('total')) }}</td>
            </tr>
    </table>

    <end_last_page end_height="70mm">
        <table class="page_footer" align="center" border="0">
            <tr>
                <td style="width: 100%; height: 10mm;text-align: left; vertical-align: text-top;">Memo:</td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td style="width: 100%; text-align: left;">Note: Jumlah (Rp) sudah termasuk biaya admin bank (Rp)</td>
            </tr>
        </table>
        <table class="page_content" border="0" align="center">  
            <tr>
                <td style="width:15%">&nbsp;</td>
                <td style="width:15%">&nbsp;</td>
                <td style="width:20%">&nbsp;</td>
                <td style="width:20%">&nbsp;</td>
                <td style="width:15%">&nbsp;</td>
                <td style="width:15%; text-align: right; font-weight: bold;">{{  FormatNumber($record->pyr->where('group', $record->group)->sum('total')) }}</td>
            </tr>
        </table>
        <table class="" border="0" align="">
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">Approve 1</div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">Approve 2</div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">Approve 3</div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">Approve 4</div>
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px; text-align: center;">
                        @if($record->verifikasiuser1->pengguna)
                            @if($record->verifikasiuser1->pengguna->ttd)
                                <img src="{{ asset('storage/'.$record->verifikasiuser1->pengguna->ttd) }}" width="118" style="margin-top: 0px;">
                            @else
                                <img src="{{ asset('img/no_image.png') }}" width="80" style="margin-top: 0px;">
                            @endif
                        @else
                            <img src="{{ asset('img/no_image.png') }}" width="80" style="margin-top: 0px;">
                        @endif
                    </div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px; text-align: center;">
                        @if($record->verifikasiuser2->pengguna)
                            @if($record->verifikasiuser2->pengguna->ttd)
                                <img src="{{ asset('storage/'.$record->verifikasiuser2->pengguna->ttd) }}" width="118" style="margin-top: 0px;">
                            @else
                                <img src="{{ asset('img/no_image.png') }}" width="80" style="margin-top: 0px;">
                            @endif
                        @else
                            <img src="{{ asset('img/no_image.png') }}" width="80" style="margin-top: 0px;">
                        @endif
                    </div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px; text-align: center;">
                        @if($record->verifikasiuser3->pengguna)
                            @if($record->verifikasiuser3->pengguna->ttd)
                                <img src="{{ asset('storage/'.$record->verifikasiuser3->pengguna->ttd) }}" width="118" style="margin-top: 0px;">
                            @else
                                <img src="{{ asset('img/no_image.png') }}" width="80" style="margin-top: 0px;">
                            @endif
                        @else
                            <img src="{{ asset('img/no_image.png') }}" width="80" style="margin-top: 0px;">
                        @endif
                    </div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px; text-align: center;">
                        @if($record->verifikasiuser4->pengguna)
                            @if($record->verifikasiuser4->pengguna->ttd)
                                <img src="{{ asset('storage/'.$record->verifikasiuser4->pengguna->ttd) }}" width="118" style="margin-top: 0px;">
                            @else
                                <img src="{{ asset('img/no_image.png') }}" width="80" style="margin-top: 0px;">
                            @endif
                        @else
                            <img src="{{ asset('img/no_image.png') }}" width="80" style="margin-top: 0px;">
                        @endif
                    </div>
                </td>
            </tr>
        </table>
    </end_last_page>
</page>