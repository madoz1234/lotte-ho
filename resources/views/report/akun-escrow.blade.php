<style type="text/css">
    <!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 93%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 93%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    /*padding: 10;*/
    /*margin: 2px;*/
    font-size: 24px;
}

/*#text_header{
    width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
*/
.boreder_head{
    background: #E1F5F3;
    border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;
    padding:5px 0 5px 0;
    font-weight: bold;
}
</style>
{{-- @foreach ($record as $records) --}}
<page backtop="50mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td id="text_header" style=" width: 100%; height: 5px; text-align: center;">
            <p class="title" style="">
                <hr style="border: #F90606; margin-top: -6px;" >
            </p>

        </td>
    </tr>
    </table>

    <table>
    <tr>
        <td><h3>Daftar Akun Escrow TMUK</h3></td>
    </tr>
    </table>
    <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
    <tr>
        <td>Tanggal Cetak </td>
        <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
    </tr>
    <tr>
        <td>Tahun Fiskal</td>
        <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
    </tr>

    <tr>
        <td>Region</td>
        <td>: {{ $region }}  </td>
    </tr>
    <tr>
        <td>LSI</td>
        <td>: {{ $lsi }}  </td>
    </tr>
    <tr>
        <td>TMUK</td>
        <td>: {{ $tmuk }} </td>
    </tr>
</table>
</page_header>
<table class="page_content" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center" style="border-spacing: 0; font-size: 11px; border-collapse: collapse; margin-left: -40px; border-bottom:1px solid #CCC">
    <tr class="boreder_head">
        <th class="boreder_head" style="text-align: center; width:135px;">Kode TMUK</th>
        <th class="boreder_head" style="text-align: center; width:135px;">Nama TMUK</th>
        <th class="boreder_head" style="text-align: center; width:135px;">Saldo Escrow</th>
        <th class="boreder_head" style="text-align: center; width:135px;">Norek Escrow</th>
        <th class="boreder_head" style="text-align: center; width:135px;">Atas Nama</th>
        <th class="boreder_head" style="text-align: center; width:135px;">Induk LSI</th>
        <th class="boreder_head" style="text-align: center; width:135px;">Tanggal Buka</th>
        <th class="boreder_head" style="text-align: center; width:135px;">No Member</th>
    </tr>
    <tr>
        <td colspan="8" style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>
    @foreach($records as $row)
    <tr >
        <td style="padding:5px 0 5px 0;text-align: center; width:135px;">{{ $row->kode }}</td>
        <td style="padding:5px 0 5px 0;text-align: center; width:135px;">{{ $row->nama }}</td>
        <td style="padding:5px 0 5px 0;text-align: center; width:135px;">{{ rupiah($row->saldo_escrow) }}</td>
        <td style="padding:5px 0 5px 0;text-align: center; width:135px;">{{ $row->rekeningescrow->nomor_rekening }}</td>
        <td style="padding:5px 0 5px 0;text-align: center; width:135px;">{{ $row->rekeningescrow->nama_pemilik }}</td>
        <td style="padding:5px 0 5px 0;text-align: center; width:135px;">{{ $row->lsi->kode }}</td>
        <td style="padding:5px 0 5px 0;text-align: center; width:135px;">{{ $row->aktual_pembukaan }}</td>
        <td style="padding:5px 0 5px 0;text-align: center; width:135px;">{{ $row->membercard->nomor }}</td>
    </tr>
    @endforeach
</table>

</page>
{{-- @endforeach --}}