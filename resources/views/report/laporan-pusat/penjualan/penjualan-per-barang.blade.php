<!DOCTYPE html>
<html>

  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
<body onload="setPageNumbers()">
    </script> 
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 850px; height: 5px; text-align: center;">
                    <p class="title"><h3>Rekap Penjualan Perbarang</h3>
                    </p>
                </td>
            </tr>
            <tr style="font-size: 11px;">
                <td><br></td>
                <td><br></td>
            </tr>
            <tr style="font-size: 11px;">
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr style="font-size: 11px;">
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr style="font-size: 11px;">
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr style="font-size: 11px;">
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr style="font-size: 11px;">
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr style="font-size: 11px;">
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
    <table class="page_content tables" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center">
        <thead style="display:table-header-group;">
          <tr class="boreder_head">
            <th class="center aligned" ><i>Kode TMUK</i></th>
            <th class="center aligned" ><i>Tanggal</i></th>
            <th class="center aligned" ><i>Kode Produk</i></th>
            <th class="left aligned" ><i>Nama Produk</i></th>
            <th class="center aligned" ><i>Qty</i></th>
            <th class="center aligned" ><i>Harga</i></th>
            <th class="center aligned" ><i>Total</i></th>
            <th class="center aligned" ><i>Hpp</i></th>
            <th class="center aligned" ><i>Profit</i></th>
            <th class="center aligned" ><i>Profit Margin</i></th>
        </tr>
    </thead>
    <tbody>   
        <?php
            $data =[];
            //dd($records->toArray());
            foreach($records->toArray() as $val){
                $data[$val['tmuk_kode']]['child'][] = $val;      
            }
            $temps = [];
            $all_qty           = 0;
            $all_produk        = 0;
            foreach($data as $key=>$val){
                $qty           = 0;
                $produk        = 0;
                $nama          = 0;
                $harga         = 0;
                $total         = 0;
                $hpp           = 0;
                $profit        = 0;
                $hasilprofit   = 0;
                foreach($val['child'] as $tot){
                    $qty            +=(integer)$tot['qty'];
                    $produk         +=(integer)$tot['produk'];
                    $nama           +=(integer)$tot['nama'];
                    $harga          +=(integer)$tot['harga'];
                    $total          +=(integer)$tot['total'];
                    $hpp            +=(integer)$tot['hpp'];
                }
                $all_qty           += $qty;

                $temps[$key]['total'] = [
                  "tmuk_kode"       => $key,
                  "produk"          => $produk,
                  "nama"            => $nama,
                  "qty"             => $qty,
                  "harga"           => $harga,
                  "total"           => $total,
                  "hpp"             => $hpp
                ];
                
                $temps[$key]['child'] = $val['child'];
            }
        ?>
        @foreach ($temps as $elm)
            <?php $i =1; ?>
            @foreach ($elm['child'] as $elms)
                @if($i==1)
                    <tr style="{{ $i==1?'border-top:1px solid #CCC':'' }}">
                        <td colspan="10" style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: left; width:100px;">{{ $i==1?$elm['total']['tmuk_kode']:'' }}</td>
                    </tr>
                @endif
                <tr>
                    <td style="padding:5px 0 5px 0;text-align: center; width:100px;"></td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:80px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$elms['tanggal'])->format('d/m/Y') }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:80px;">{{ $elms['produk'] }}</td>
                    <td style="padding:5px 0 5px 0;text-align: left; width:230px;">{{ $elms['nama'] }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:50px;">{{ $elms['qty'] }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:110px;">{{ rupiah($elms['harga']) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:110px;">{{ rupiah($elms['total']) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:110px;">{{ rupiah($elms['hpp']) }}</td>
                    <?php $profit = $elms['total'] - $elms['hpp'] ?>
                    <td style="padding:5px 0 5px 0;text-align: center; width:110px;">{{ rupiah($profit) }}</td>
                    <?php $hasilprofit = $profit / $elms['total'] ?>
                    <td style="padding:5px 0 5px 0;text-align: center; width:110px;">{{ round($hasilprofit,2) }}</td>
                </tr>
            <?php $i++; ?>
            @endforeach
        @endforeach
        
    </tbody>
  </body>
</html>