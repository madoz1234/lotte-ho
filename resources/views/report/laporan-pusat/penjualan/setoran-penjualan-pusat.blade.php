<!DOCTYPE html>
<html>

  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
<body onload="setPageNumbers()">
    </script> 
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 850px; height: 5px; text-align: center;">
                    <p class="title"><h3>Rekap Setoran Penjualan</h3>
                    </p>
                </td>
            </tr>
            <tr style="font-size: 11px;">
                <td><br></td>
                <td><br></td>
            </tr>
            <tr style="font-size: 11px;">
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr style="font-size: 11px;">
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr style="font-size: 11px;">
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr style="font-size: 11px;">
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr style="font-size: 11px;">
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr style="font-size: 11px;">
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
    <table class="page_content tables" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center">
        <thead style="display:table-header-group;">
          <tr class="boreder_head">
            <th class="center aligned" ><i>Kode TMUK</i></th>
            <th class="center aligned" ><i>Tanggal</i></th>
            {{-- <th class="center aligned" ><i>Jumlah Struk</i></th> --}}
            <th class="center aligned" ><i>Total Penjualan (Rp)</i></th>
            <th class="center aligned" ><i>Total Setoran (Rp)</i></th>
            {{-- <th class="center aligned" ><i>Profit</i></th>
            <th class="center aligned" ><i>Margin</i></th> --}}
        </tr>
    </thead>
    <tbody>   
        <?php
            $data =[];
            //dd($records->toArray());
            foreach($records->toArray() as $val){
                $data[$val['tmuk_kode']]['child'][] = $val;      
            }
            $temps = [];
            $all_struk           = 0;
            $all_tunai           = 0;
            $all_total_hpp       = 0;
            $all_profit          = 0;
            $all_total_penjualan = 0;
            $all_total_setoran   = 0;
            $all_total_spd       = 0;
            $all_total_std       = 0;
            foreach($data as $key=>$val){
                $struk           = 0;
                $tunai           = 0;
                $total_hpp       = 0;
                $total_setoran   = 0;
                $profit          = 0;
                $total_penjualan = 0;
                $margin          = 0;
                foreach($val['child'] as $tot){
                    $struk           +=(integer)$tot['struk'];
                    $tunai           +=(integer)$tot['tunai'];
                    $total_hpp       +=(integer)$tot['total_hpp'];
                    $total_setoran   +=(integer)$tot['setoran'];
                    $profit          +=(integer)$tot['total_penjualan']-(integer)$tot['total_hpp'];
                    $total_penjualan +=(integer)$tot['total_penjualan'];
                }
                //dd(count($val['child']));
                $all_struk           += $struk;
                $all_tunai           += $tunai;
                $all_total_hpp       += $total_hpp;
                $all_profit          += $profit;
                $all_total_penjualan += $total_penjualan;
                $all_total_setoran   += $total_setoran;
                $all_total_spd       += $total_penjualan/count($val['child']);
                $all_total_std       += $struk/count($val['child']);

                $temps[$key]['total'] = [
                  "tmuk_kode"       => $key,
                  "tanggal"         => $tot['nama_tmuk'],
                  "struk"           => $struk,
                  "tunai"           => $tunai,
                  "total_hpp"       => $total_hpp,
                  "profit"          => $profit,
                  "total_penjualan" => $total_penjualan,
                  "total_setoran" => $total_setoran,
                  "margin"          => ((integer)$profit/(integer)$total_penjualan)*100
                ];
                
                $temps[$key]['child'] = $val['child'];
            }
        ?>
        @foreach ($temps as $elm)
            <?php $i =1; ?>
            @foreach ($elm['child'] as $elms)
                @if($i==1)
                    <tr style="{{ $i==1?'border-top:1px solid #CCC':'' }}">
                        {{-- <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: center; width:150px;">{{ $i==1?$elm['total']['tmuk_kode']:'' }}</td> --}}
                        <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: center; width:100px;">{{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }}</td>
                        <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: center; width:200px;">{{ $elm['total']['tanggal'] }}</td>
                        <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: right; width:200px;">{{ rupiah($elm['total']['total_penjualan']) }}</td>
                        <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: right; width:200px;">{{ rupiah($elm['total']['total_setoran']) }}</td>
                    </tr>
                @endif
                <tr>
                    <td style="padding:5px 0 5px 0;text-align: center; width:150px;">{{ $elms['tmuk_kode'] }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:200px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$elms['tanggal'])->format('d/m/Y') }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:200px;">{{ rupiah($elms['total_penjualan']) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:200px;">{{ rupiah($elms['setoran']) }}</td>
                </tr>
            <?php $i++; ?>
            @endforeach
        @endforeach
        <tr>
            <td style="padding:15px 0 5px 0;text-align:  center; vertical-align:middle;  width:150px; font-weight:bold;background: #e6ffe6;" colspan="2">
                TOTAL KESELURUHAN :
            </td>
            <td style="padding:5px 0 5px 0;text-align: right; width:200px; font-weight:bold;background: #e6ffe6;">{{ rupiah($all_total_penjualan) }}</td>
            <td style="padding:5px 0 5px 0;text-align: right; width:200px; font-weight:bold;background: #e6ffe6;">{{ rupiah($all_total_setoran) }}</td>
        </tr>
        
    </tbody>
</table>
<table style="padding-left: -30px; font-size: 10px;" padding="0" margin="0">
    <tr>
        <td>SPD</td>
        <td>: {{ rupiah($all_total_spd/count($temps)) }}</td>
    </tr>
    <tr>
        <td>STD</td>
        <td>: {{ number_format($all_total_std/count($temps),2) }}</td>
    </tr>
    <tr>
        <td>APC</td>
        <td>: {{ rupiah(($all_total_spd/count($temps))/($all_total_std/count($temps))) }} </td>
    </tr>
</table>
  </body>
</html>