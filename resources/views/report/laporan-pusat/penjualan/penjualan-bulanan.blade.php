<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body onload="setPageNumbers()">
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 850px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Penjualan Bulanan </h3>
                    </p>
                </td>
            </tr>
            <tr style="font-size: 11px;">
                <td><br></td>
                <td><br></td>
            </tr>
            <tr style="font-size: 11px;">
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr style="font-size: 11px;">
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr style="font-size: 11px;">
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr style="font-size: 11px;">
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr style="font-size: 11px;">
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr style="font-size: 11px;">
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <thead style="display:table-header-group;">
                <tr class="boreder_head">
                    <th class="center aligned">Kode TMUK</th>
                    <th style="text-align: center;">Periode</th>
                    <th style="text-align: right;">Jumlah Struk</th>
                    <th style="text-align: right;">Total Sales</th>
                    <th style="text-align: right;">Total HPP</th>
                    <th style="text-align: right;">Profit</th>
                    <th style="text-align: right;">Margin</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $data = [];
                    foreach($records->toArray() as $val){
                        $data[$val['tmuk_kode']]['child'][] = $val;      
                    }
                    $temps = [];
                    $all_struk           = 0;
                    $all_total_hpp       = 0;
                    $all_profit          = 0;
                    $all_margin          = 0;
                    $all_total_penjualan = 0;
                    $all_total_spd       = 0;
                    $all_total_std       = 0;
                    foreach($data as $key=>$val){
                        $struk           = 0;
                        $total_hpp       = 0;
                        $total_profit    = 0;
                        $total_penjualan = 0;
                        foreach($val['child'] as $tot){
                            $struk           +=(integer)$tot['struk'];
                            $total_hpp       +=(integer)$tot['total_hpp'];
                            $total_profit    +=(integer)$tot['total_profit'];
                            $total_penjualan +=(integer)$tot['total_penjualan'];
                        }
                        $all_struk           += $struk;
                        $all_total_hpp       += $total_hpp;
                        $all_profit          += $total_profit;
                        $all_total_penjualan += $total_penjualan;
                        $all_total_spd       += $total_penjualan/count($val['child']);
                        $all_total_std       += $struk/count($val['child']);
                        $temps[$key]['total'] = [
                            "tmuk_kode"       => $key,
                            "periode"         => $tot['nama_tmuk'],
                            "struk"           => $struk,
                            "total_hpp"       => $total_hpp,
                            "total_profit"    => $total_profit,
                            "total_penjualan" => $total_penjualan,
                        ];
                        $temps[$key]['child'] = $val['child'];
                    }
                ?>
                @foreach ($temps as $elm)
                    <?php 
                            //dd($elm);
                            $i =1;
                     ?>
                    @foreach ($elm['child'] as $elms)
                        @if($i==1)
                            <tr style="{{ $i==1?'border-top:1px solid #CCC':'' }}">
                                <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: center; width:100px;">{{ $i==1?$elm['total']['tmuk_kode']:'' }}</td>
                                <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: center; width:110px;">{{ $elm['total']['periode'] }}</td>
                                <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: right; width:110px;">{{ rupiah($elm['total']['struk']) }}</td>
                                <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: right; width:110px;">{{ rupiah($elm['total']['total_penjualan']) }}</td>
                                <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: right; width:110px;">{{ rupiah($elm['total']['total_hpp']) }}</td>
                                <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: right; width:110px;">{{ rupiah($elm['total']['total_profit']) }}</td>
                                <td style="{{ $i==1?'background:#f5f5f5;':'' }}padding:5px 0 5px 0;text-align: right; width:110px;"></td>
                            </tr>
                        @endif
                        <tr>
                            <td style="padding:5px 0 5px 0;text-align: center; width:100px;"></td>
                            <td style="padding:5px 0 5px 0;text-align: center; width:110px;">{{ \Carbon\Carbon::parse($elms['tanggal'])->format('F Y') }}</td>
                            <td style="padding:5px 0 5px 0;text-align: right; width:110px;">{{ rupiah($elms['struk']) }}</td>
                            <td style="padding:5px 0 5px 0;text-align: right; width:110px;">{{ rupiah($elms['total_penjualan']) }}</td>
                            <td style="padding:5px 0 5px 0;text-align: right; width:110px;">{{ rupiah($elms['total_hpp']) }}</td>
                            <td style="padding:5px 0 5px 0;text-align: right; width:110px;">{{ rupiah($elms['total_profit']) }}</td>
                            {{-- <td style="padding:5px 0 5px 0;text-align: right; width:110px;">{{ rupiah(0) }} %</td> --}}
                            @if ($elms['total_profit'] == '')
                                <td style="padding:5px 0 5px 0;text-align: right; width:110px;">0%</td>
                            @else
                                <?php $margin = $elms['total_penjualan']/$elms['total_profit']; ?>
                                <td style="padding:5px 0 5px 0;text-align: right; width:110px;">{{ round(substr($margin,0,2)) }}%</td>
                            @endif
                        </tr>
                    <?php $i++; ?>
                    @endforeach
                @endforeach
                <tr>
                    <td style="padding:10px 0 5px 0;text-align: center; width:100px; background: #e6ffe6;" colspan="2">Grand Total</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:110px; background: #e6ffe6;">{{ rupiah($all_struk) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:110px; background: #e6ffe6;">{{ rupiah($all_total_penjualan) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:110px; background: #e6ffe6;">{{ rupiah($all_total_hpp) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:110px; background: #e6ffe6;">{{ rupiah($all_profit) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:110px; background: #e6ffe6;"></td>
                </tr>

                <tr>
                    <td colspan="7" style="text-align: center;  border-bottom: 1px solid #000"></td>
                </tr>
            </tbody>
        </table>
        <br>
        <table border="0" padding="0" cellpadding="-5" cellspacing="0 align="center" style="font-size: 11px; border-spacing: 0;
    border-collapse: collapse;">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    {{-- <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;"> --}}
                </td>
                <td id="text_header" style=" border-bottom:1px solid #ffffff; width: 850px; height: 5px; text-align: center;">
                    
                </td>
            </tr>
            <tr style="font-size: 11px;">
                <?php 
                    $jumrow = $i - 1;
                    $spd = $all_total_penjualan / $jumrow; 
                 ?>
                <td>SPD </td>
                <td>: {{ rupiah($spd) }}</td>
            </tr>
            <tr style="font-size: 11px;">
                <?php
                    $jumrow = $i - 1;
                    $std = $all_struk / $jumrow;
                ?>
                <td>STD</td>
                <td>: {{ rupiah($std) }}</td>
            </tr>
            <tr style="font-size: 11px;">
                <?php
                    $apc = $all_total_penjualan / $all_struk;
                ?>
                <td>APC</td>
                <td>: {{ rupiah($apc) }} </td>
            </tr>
        </table>
  </body>
</html>