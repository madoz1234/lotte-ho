<style type="text/css">

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
.page_content_header {width: 100%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
</style>
<page backtop="62mm" backbottom="3mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
                </td>
                <td id="text_header" style=" width: 610px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Pergerakan Persediaan</h3>
                        <hr style="border: #F90606; margin-left:-3px; margin-top: -3px;" >
                    </p>
                </td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            {{-- {{ dd($region) }} --}}
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }}</td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
       {{--  <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="text-align: center; width:5%;">#</th>
                <th style="text-align: center; width:10%;">Kode Produk</th>
                <th style="text-align: center; width:25%;">Nama Produk</th>
                <th style="text-align: center; width:10%;">UOM</th>
                <th style="text-align: center; width:15%;">Barcode 1</th>
                <th style="text-align: center; width:15%;">Barcode 2</th>
                <th style="text-align: center; width:10%;">Tanggal Pergerakan Stok</th>
                <th style="text-align: center; width:10%;">Stok Awal</th>
                <th style="text-align: center; width:10%;">Stok Akhir</th>
            </tr>
        </table> --}}
    </page_header>
    <table class="page_content" border="0" align="center" cellspacing="0" cellpadding="0" margin='0' padding='0'>
        <tr>
            {{-- <th class="page_content_header" style="text-align: center; width:5%;">#</th> --}}
            <th class="page_content_header" style="text-align: center; width:10%;">Kode</th>
            <th class="page_content_header" style="text-align: center; width:35%;">Nama Produk</th>
            <th class="page_content_header" style="text-align: center; width:8%;">UOM</th>
            <th class="page_content_header" style="text-align: center; width:10%;">Stok Awal</th>
            <th class="page_content_header" style="text-align: center; width:12%;">Stok Masuk</th>
            <th class="page_content_header" style="text-align: center; width:12%;">Stok Keluar</th>
            <th class="page_content_header" style="text-align: center; width:10%;">Stok Akhir</th>
        </tr>
        <col style="text-align: center; width:5%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:25%">
        <col style="text-align: center; width:15%">
        <col style="text-align: center; width:15%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:10%">
        <col style="text-align: center; width:10%">
        <?php $i = 1;?>
        @foreach ($records as $row)
            <tr>
                {{-- <td style="width:5%">{{ $i }}</td> --}}
                <td style="text-align: left; width:10%">{{ $row->produk->kode }}</td>
                <td style="text-align: left; width:35%">{{ $row->produk->nama }}</td>
                <td style="width:8%">{{ $row->produk->produksetting->uom1_satuan }}</td>
                <td style="width:10%">{{ $row->stock_awal=''?number_format($row->stock_akhir):number_format($row->stock_awal) }}</td>
                <td style="width:12%">0</td>
                <td style="width:12%">0</td>
                <td style="width:10%">{{ $row->stock_akhir?number_format($row->stock_akhir):number_format($row->stock_awal) }}</td>
            </tr>
        <?php $i++; ?>
        @endforeach
    </table>
</page>