<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
.th{width: 100%; border: none; padding: 4px; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

table.page_header.th td{
    padding:5px;
}
.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
table td{
    padding: 5px;
}
-->
</style>
<page backtop="68mm" backbottom="3mm" backleft="0mm" backright="0mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td id="" style="; width: 80px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
                </td>
                <td id="text_header" style=" width: 625px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Nilai Persediaan</h3>
                        <hr style="border: #F90606; margin-left: -7px; margin-top: -3px;" >
                    </p>
                </td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            {{-- {{ dd($region) }} --}}
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="text-align: right;font-size: 10px;padding-right: 5px;">Page [[page_cu]]</div>
                </td>
            </tr>
        </table>
    </page_header>
    <table class="page_content" border="0" align="center" cellpadding="0" cellspacing="0">
        <thead  style="display:table-header-group;">
             <tr>
                <th class='th' style="text-align: center; width:20%;">Kode Produk</th>
                <th class='th' style="text-align: center; width:35%;">Nama Produk</th>
                <th class='th' style="text-align: center; width:10%;">UOM</th>
                <th class='th' style="text-align: center; width:10%;">Qty</th>
                <th class='th' style="text-align: right; width:15%;">HPP (MAP)</th>
                <th class='th' style="text-align: right; width:10%;">Value</th>
            </tr>
        </thead>
        @foreach ($records as $key=>$row)
            <tr>
                <td colspan="6" colspan="6" style="font-size: 11;font-weight: bold; text-align: center;">{{ $key }}</td>
            </tr>
            @foreach ($row as $keys=>$elm)
                <?php $jum_total= 0; ?>
                @foreach ($elm as $child)
                <tr style="font-size: 9px;">
                    <td style="width:20%">{{ $child['kode_produk'] }}</td>
                    <td style="width:35%">{{ $child['nama_produk'] }}</td>
                    <td style="text-align: center; width:10%">{{ $child['uom'] }}</td>
                    <td style="text-align: center; width:10%">{{ $child['qty'] }}</td>
                    <td style="text-align: right; width:15%">{{ $child['cost_price'] }}</td>
                    <td style="text-align: right; width:10%">{{ number_format($child['jumlah']) }}</td>
                    <?php $jum_total += $child['jumlah']; ?>
                </tr>
                @endforeach
                <tr>
                    <td  colspan="5" style="background: #DEFAEE;font-size: 11;font-weight: bold;">SUB TOTAL {{ $keys }}</td>
                    <td  style="text-align: right;background: #DEFAEE;font-size: 11;font-weight: bold;">{{ number_format($jum_total) }}</td>
                </tr>
                <tr>
                    <td  colspan="5" style="font-size: 11;font-weight: bold;"></td>
                    <td  style="text-align: right;font-size: 11;font-weight: bold;"></td>
                </tr>
            @endforeach
        @endforeach
    </table>
</page>