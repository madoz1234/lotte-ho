<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Aktiva Tetap per Tipe </h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <thead style="display:table-header-group;">
                <tr>
                    <th style="text-align: center; width:150px;">Nama Aktiva</th>
                    <th style="text-align: center; width:80px;">Harga Perolehan</th>
                    <th style="text-align: center; width:80px;">Akumulasi Depresiasi</th>
                    <th style="text-align: center; width:80px;">Book Value</th>
                    <th style="text-align: center; width:120px;">Depresiasi Tahun Ini</th>
                    <th style="text-align: center; width:120px;">Tanggal Pembelian</th>

                </tr>
            </thead>

            @foreach ($records as $row)
                <tr style="">
                    {{-- <td style="padding:5px 0 5px 0; text-align: left; width:80px;">{{ $row->tmuk->nama }}</td> --}}
                    <td style="padding:5px 0 5px 0; text-align: left; width:150px;">{{ $row->coa->nama }}</td>
                    <td style="padding:5px 0 5px 0; text-align: right; width:80px;">{{ rupiah($row->jumlah) }}</td>
                    <td style="padding:5px 0 5px 0; text-align: left; width:80px;"></td>
                    <td style="padding:5px 0 5px 0; text-align: center; width:80px;"></td>
                    <td style="padding:5px 0 5px 0; text-align: right; width:120px;"></td>
                    <td style="padding:5px 0 5px 0; text-align: right; width:120px;">{{ $row->tanggal }}</td>
                </tr>
            @endforeach
        </table>
  </body>
</html>