<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>DAFTAR HISTORY GL</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <thead style="display:table-header-group;">
                <tr>
                    <th style="text-align: center; width:10%; font-weight: bold;">#</th>
                    <th style="text-align: center; width:26%; font-weight: bold;">Nama Akun</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">Tanggal</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">ID Transaksi</th>
                    <th style="text-align: center; width:12%; font-weight: bold;">Saldo Awal</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">Perubahan Debit</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">Perubahan Kredit</th>
                    <th style="text-align: center; width:10%; font-weight: bold;">Perubahan Bersih</th>
                    <th style="text-align: center; width:12%; font-weight: bold;">Saldo Akhir</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=1; ?>
                @foreach ($records as $row)
                        <tr>
                            <td class="center aligned">{{ $i }}</td>
                            <td class="left aligned">{{ $row['nama_akun'] }}</td>
                            <td class="center aligned">{{ $row['tanggal'] }}</td>
                            <td class="center aligned">{{ $row['id_tsransaksi'] }}</td>
                            <td class="right aligned">{{ $row['saldo_awal'] }}</td>
                            <td class="right aligned">{{ $row['jumlah_debit'] }}</td>
                            <td class="right aligned">{{ $row['jumlah_kredit'] }}</td>
                            <td class="right aligned">{{ $row['bersih'] }}</td>
                            <td class="right aligned">{{ $row['saldo_akhir'] }}</td>
                        </tr>
                    <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
  </body>
</html>