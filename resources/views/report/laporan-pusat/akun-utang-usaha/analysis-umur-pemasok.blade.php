<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Analysis Umur Utang Usaha</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <tr class="boreder_head">
                <th class="boreder_head" style="text-align: center; width:170px;">Supplier</th>
                <th class="boreder_head" style="text-align: center; width:100px;">Currency</th>
                <th class="boreder_head" style="text-align: center; width:120px;">1-30 Days</th>
                <th class="boreder_head" style="text-align: center; width:120px;">31-60 Days</th>
                <th class="boreder_head" style="text-align: center; width:120px;">Over 60 Days</th>
                <th class="boreder_head" style="text-align: center; width:120px;">Total Balance</th>
            </tr>
            <?php
                $i=0;
                $vendor='';
            ?>
            @foreach($records as $row)
                <?php
                    if($i==0){
                        $vendor = $row->vendor_lokal;
                    }else{
                        if($row->vendor_lokal!=$vendor){
                            $i=0;
                            $vendor = $row->vendor_lokal;
                        }
                    }

                    $st  = date_create(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->created_at)->format('Y-m-d'));
                    $en    = date_create();
                    $diff   = date_diff( $st, $en );
                    $day = $diff->days;
                ?>
                @if($i==0)
                    <tr>
                        <td style="border-bottom: 2px solid black;text-align: left;width:170px;">Lokal Vendor {{ $row->vendors($row->id) }}</td>
                        <td style="border-bottom: 2px solid black;text-align: center;" >IDR</td>
                        <td style="border-bottom: 2px solid black;text-align: right;" >{{ rupiah(0) }}</td>
                        <td style="border-bottom: 2px solid black;text-align: right;" >{{ rupiah(0) }}</td>
                        <td style="border-bottom: 2px solid black;text-align: right;" >{{ rupiah(0) }}</td>
                        <td style="border-bottom: 2px solid black;text-align: right;" >{{ rupiah(0) }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: left">Supplier Invoice</td>
                        <td style="text-align: center;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                        <td style="text-align: right;" >
                            @if($day <= 30)
                                {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                            @else
                                {{ rupiah(0) }}
                            @endif
                        </td>
                        <td style="text-align: right;" >
                            @if($day > 30 && $day <= 60)
                                {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                            @else
                                {{ rupiah(0) }}
                            @endif
                        </td>
                        <td style="text-align: right;" >
                            @if($day > 60)
                                {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                            @else
                                {{ rupiah(0) }}
                            @endif
                        </td>
                        <td style="text-align: right;">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
                    </tr>
                @else
                    <tr>
                        <td style="text-align: left">Supplier Invoice</td>
                        <td style="text-align: center;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                        <td style="text-align: right;" >
                            @if($day <= 30)
                                {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                            @else
                                {{ rupiah(0) }}
                            @endif
                        </td>
                        <td style="text-align: right;" >
                            @if($day > 30 && $day <= 60)
                                {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                            @else
                                {{ rupiah(0) }}
                            @endif
                        </td>
                        <td style="text-align: right;" >
                            @if($day > 60)
                                {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                            @else
                                {{ rupiah(0) }}
                            @endif
                        </td>
                        <td style="text-align: right;">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
                    </tr>
                @endif
                <?php 
                    $i++;
                ?>
            @endforeach
        </table>
  </body>
</html>