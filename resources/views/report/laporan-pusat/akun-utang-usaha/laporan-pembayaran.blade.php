<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Laporan Pembayaran</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <tr>
                <th style="text-align: center; width:250px;">Tipe Transaksi</th>
                <th style="text-align: center; width:100px;">#</th>
                <th style="text-align: center; width:250px;">Jatuh Tempo</th>
                <th style="text-align: center; width:150px;">Total</th>
            </tr>
            <?php
                $i=0;
                $vendor='';
                $total=0;
            ?>
            @foreach($records as $row)
                <?php
                    if($i==0){
                        $vendor = $row->vendor_lokal;
                    }else{
                        if($row->vendor_lokal!=$vendor){
                            echo '
                            <tr>
                                <td colspan="3" style="border-bottom:2px solid black;border-top:1px solid black;text-align:left;">Total</td>
                                <td style="border-bottom:2px solid black;border-top:1px solid black;text-align:right;">'.rupiah($total).'</td>
                            </tr>
                            ';
                            $i=0;
                            $total=0;
                            $vendor = $row->vendor_lokal;
                        }
                    }
                ?>
                @if($i==0)
                    <tr>
                        <td colspan="3" style="text-align:left;" >Lokal Vendor {{ $row->vendors($row->id) }} - Cash Only</td>
                        <td style="text-align:right;" >IDR</td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">Supplier Invoice</td>
                        <td style="text-align:center;">{{ $row->urutan($row->id) }}</td>
                        <td style="text-align:center;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                        <td style="text-align:right;">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
                    </tr>
                @else
                    <tr>
                        <td style="text-align:left;">Supplier Invoice</td>
                        <td style="text-align:center;">{{ $row->urutan($row->id) }}</td>
                        <td style="text-align:center;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                        <td style="text-align:right;">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
                    </tr>
                @endif
                <?php 
                    $i++;
                    $total+=isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0;
                ?>
            @endforeach
            <tr>
                <td colspan="3" style="border-bottom:2px solid black;border-top:1px solid black;text-align:left;">Total</td>
                <td style="border-bottom:2px solid black;border-top:1px solid black;text-align:right;">{{ rupiah($total) }}</td>
            </tr>
        </table>
  </body>
</html>