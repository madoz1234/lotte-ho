<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Laporan Saldo Pemasok</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <tr class="boreder_head">
            <th class="center aligned">Trans Type</th>
            <th class="center aligned">Date</th>
            <th class="center aligned">Due Date</th>
            <th class="center aligned">Charges</th>
            <th class="center aligned">Credits</th>
            <th class="center aligned">Allocated</th>
            <th class="center aligned">Outstanding</th>
        </tr>
        <?php
        $i=0;
        $vendor='';
        ?>
 
        @foreach ($records as $data)
            <?php
            if($i==0){
                $vendor = $data->vendor_lokal;
            }else{
                if($data->vendor_lokal!=$vendor){
                    $i=0;
                    $vendor = $data->vendor_lokal;
                }
            }
            ?>
            @if($i==0)
            {{-- {{ dd($data->verifikasi4_user == ) }} --}}
                <tr>
                    {{-- <td style="padding:5px 0 5px 0;text-align: left; width:100px;">Lokal Vendor {{ $data->vendors($data->vendor_lokal) }}</td> --}}
                    <td style="padding:5px 0 5px 0;text-align: left; width:100px;">Lokal Vendor {{ $data->vendor_lokal }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:90px;"></td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:90px;"></td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;"></td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;"></td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;"></td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;"></td>
                </tr>
                <tr>
                    <td style="padding:5px 0 5px 0;text-align: left; width:110;">{{ $data->deskripsi }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:90px;">{{ $data->tanggal }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:90px;">{{ $data->deskripsi == 'Pembayaran PyR - Trade Non Lotte' ? '' : $data->tanggal }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;">{{ $data->posisi == 'D' ? '0' : rupiah($data->charges_jurnal) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;">{{ $data->posisi == 'K' ? '0' : rupiah($data->charges_jurnal) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;">{{ $data->verifikasi4_user != NULL ? '0': rupiah($data->charges_jurnal) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;">{{ $data->verifikasi4_user != NULL ? rupiah($data->charges_jurnal) : '0' }}</td>
                </tr>
          {{--       <tr>
                    <td colspan="5" style="text-align: center;  border-bottom: 1px solid #000"></td>
                </tr> --}}
            @else
                <tr>
                    <td style="padding:5px 0 5px 0;text-align: left; width:110;">{{ $data->deskripsi }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:90px;">{{ $data->tanggal }}</td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:90px;">{{ $data->deskripsi == 'Pembayaran PyR - Trade Non Lotte' ? '' : $data->tanggal }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;">{{ $data->posisi == 'D' ? '0' : rupiah($data->charges_jurnal) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;">{{ $data->posisi == 'K' ? '0' : rupiah($data->charges_jurnal) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;">{{ $data->verifikasi4_user != NULL ? '0': rupiah($data->charges_jurnal) }}</td>
                    <td style="padding:5px 0 5px 0;text-align: right; width:90px;">{{ $data->verifikasi4_user != NULL ? rupiah($data->charges_jurnal) : '0' }}</td>
                </tr>
                {{-- <tr>
                    <td colspan="5" style="text-align: center;  border-bottom: 1px solid #000"></td>
                </tr> --}}
            @endif
            <?php 
                $i++;
            ?>
        @endforeach
        <tr>
            <td style="padding:15px 0 5px 0;text-align:  center; vertical-align:middle;  width:300px; font-weight:bold;background: #e6ffe6;" colspan="2">
               {{--  TOTAL KESELURUHAN : --}}
            </td>
            <td style="padding:5px 0 5px 0;text-align: center; width:90px; font-weight:bold;background: #e6ffe6;"></td>
            <td style="padding:5px 0 5px 0;text-align: right; width:90px; font-weight:bold;background: #e6ffe6;"></td>
            <td style="padding:5px 0 5px 0;text-align: right; width:90px; font-weight:bold;background: #e6ffe6;"></td>
            <td style="padding:5px 0 5px 0;text-align: right; width:90px; font-weight:bold;background: #e6ffe6;"></td>
            <td style="padding:5px 0 5px 0;text-align: right; width:90px; font-weight:bold;background: #e6ffe6;"></td>
        </tr>
        </table>
  </body>
</html>