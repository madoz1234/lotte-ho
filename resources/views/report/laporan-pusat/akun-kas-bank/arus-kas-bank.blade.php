<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>JURNAL KAS DAN BANK</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <thead style="display:table-header-group;">
                <tr>
                    <th style="text-align: center; width:5%">No</th>
                    <th style="text-align: center; width:10%">Tanggal</th>
                    <th style="text-align: center; width:20%">Nama Akun</th>
                    <th style="text-align: center; width:10%">Saldo Awal</th>
                    <th style="text-align: center; width:10%">Perubahan <br>Debit</th>
                    <th style="text-align: center; width:10%">Perubahan <br>Kredit</th>
                    <th style="text-align: center; width:10%">Perubahan <br>Bersih</th>
                    <th style="text-align: center; width:10%">Saldo Akhir</th>
                    <th style="text-align: center; width:15%">Keterangan</th>
                </tr>
            </thead>
            <?php 
                $i           = 1; 
                $saldo_awal = [];
                $saldo_akhir = [];
            ?>
            @foreach ($records as $row)
                <tr>
                    <td style="text-align: center; width:5%">{{ $i }}</td>
                    <td style="text-align: center; width:10%">{{ \Carbon\Carbon::parse($row->tanggal)->format('Y-m-d') }}</td>
                    <td style="text-align: left; width:20%">{{ $row->coa_kode }} - {{ $row->coa->nama }}</td>
                    <td style="text-align: right; width:10%">
                        <?php 
                            if ($i == 1) {
                                $saldo_awal[$i] = 0;
                            }else{
                                $saldo_awal[$i] = $saldo_akhir[$i-1];
                            }
                        ?>
                        {{ rupiah($saldo_awal[$i]) }}
                    </td>
                    <td style="text-align: right; width:10%">
                        <?php 
                            $jumlah_debit = $row->posisi=='D' ? $row->jumlah : '0';
                        ?>
                        {{ rupiah($jumlah_debit) }}
                    </td>
                    <td style="text-align: right; width:10%">
                        <?php 
                            $jumlah_kredit = $row->posisi=='K' ? $row->jumlah : '0';
                        ?>
                        {{ rupiah($jumlah_kredit) }}
                    </td>
                    <td style="text-align: right; width:10%">
                        <?php 
                            $posisi_normal = $row->coa->posisi;

                            if ($posisi_normal == 'D') {
                                $bersih = $jumlah_debit - $jumlah_kredit;
                            }else{
                                $bersih = $jumlah_kredit - $jumlah_debit;                                        
                            }

                        ?>
                        {{ rupiah($bersih) }}
                    </td>
                    <td style="text-align: right; width:10%">
                        <?php 
                            $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                        ?>
                        {{ rupiah($saldo_akhir[$i]) }}
                    </td>
                    <td style="text-align: left; width:15%"> {{ $row->deskripsi }}</td>
                </tr>
                <?php $i++; ?>
            @endforeach
        </table>
  </body>
</html>