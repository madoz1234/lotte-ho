<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>MUTASI ESCROW</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <thead style="display:table-header-group;">
                <tr>
                    <th style="text-align: center; width:10%">Tanggal</th>
                    <th style="text-align: center; width:30%">Keterangan</th>
                    <th style="text-align: center; width:20%">Debet</th>
                    <th style="text-align: center; width:20%">Credit</th>
                    <th style="text-align: center; width:20%">Ledger</th>
                </tr>
            </thead>
        <?php $kredit=0;$debit=0;$awal=$saldo;?>
        @foreach ($records as $row)
        {{-- {{ dd($row) }} --}}
        <tr>
            <td style="text-align: center; width:10%">{{ \Carbon\Carbon::parse($row->tanggal)->format('Y-m-d') }}</td>
            <td style="text-align: left; width:30%">
                @if($row->reduce)
                {{ $row->reduce->po_nomor }}
                @else
                {{ $row->deskripsi }}
                @endif
            </td>
            <td style="text-align: right; width:20%">{{ $row->posisi=='D' ? rupiah($row->jumlah) : '0' }}</td>
            <td style="text-align: right; width:20%">{{ $row->posisi=='K' ? rupiah($row->jumlah) : '0' }}</td>
            <td style="text-align: right; width:20%">{{ rupiah($saldo) }}</td>
        </tr>
        <?php
        if($row->posisi=='D'){
            $debit+=$row->jumlah;
            $saldo=$saldo-$row->jumlah;
        }else{
            $kredit+=$row->jumlah;
            $saldo+=$row->jumlah;
        }
        ?>
        @endforeach
    </table>

    <br>
    <br>

    <table class="page_content_header" border="0" align="right">
        <tr>
            <th style="text-align: center; width:15%">OPENING BALANCE</th>
            <th style="text-align: center; width:15%">TOTAL DEBET</th>
            <th style="text-align: center; width:15%">TOTAL CREDIT</th>
            <th style="text-align: center; width:15%">CLOSING BALANCE</th>
        </tr>
    </table>
    <table class="page_content" border="0" align="right">
        <tr>
            <td style="text-align: right;width:15%;"><b>{{ rupiah($awal) }}</b></td>
            <td style="text-align: right;width:15%;"><b>{{ rupiah($debit) }}</b></td>
            <td style="text-align: right;width:15%;"><b>{{ rupiah($kredit) }}</b></td>
            <td style="text-align: right;width:15%;"><b>{{ rupiah($saldo) }}</b></td>
        </tr>
    </table>
  </body>
</html>