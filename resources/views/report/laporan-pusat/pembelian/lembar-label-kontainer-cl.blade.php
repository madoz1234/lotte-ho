<style type="text/Css">
/*design table 1*/
/*.table1 {
    font-family: sans-serif;
    color: #000000;
    border-collapse: collapse;
}
 
.table1, th, td {
    border: 1px solid #999;
    padding: 8px 20px;
}*/

/*td{
    border: 1px solid #000;
}*/
 /***Always insert a page break before the element***/
        /*.pb_before {
           page-break-before: always !important;
        }

        /***Always insert a page break after the element***/
        .pb_after {
           page-break-after: always !important;
        }

        /***Avoid page break before the element (if possible)***/
        .pb_before_avoid {
           page-break-before: avoid !important;
        }

        /***Avoid page break after the element (if possible)***/
        .pb_after_avoid {
           page-break-after: avoid !important;
        }

        /* Avoid page break inside the element (if possible) */
        .pbi_avoid {
           page-break-inside: avoid !important;
        }*/
</style>
@foreach ($records as $row)
{{-- {{ dd($row) }} --}} 
<page style="font-size: 14px;" orientation="landscape" format="A4" backtop="1mm" backbottom="1mm" backleft="1mm" backright="1mm">
{{-- <div class="pb_after"> --}}
    <table style="border: 1px solid #000; margin: 10px; padding:15px;">
        <tr>
            <td style="padding-left: 20px; text-align: center; border: 0px solid #000; width: 990px; font-weight:bold; font-size:25pt; height: 115px">
                       {{ $row['tmuk_kode'] }}<br>{{ $row['tmuk_nama'] }}
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px;">
                <table style="border: solid 0px #440000; width: 100%; margin-bottom: 50px;" cellspacing="0">
                    <tr>
                        <td style="text-align: center; width: 57%"><span style="font-weight: bold; font-size: 20pt;">No PR : </span></td>
                        <td style="text-align: center; width: 57%"><span style="font-weight: bold; font-size: 20pt;">No PO : </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: center; width: 57%"><span style="font-size: 20pt;">{{ $row->po->nomor_pr }}</span></td>
                        <td style="text-align: center; width: 57%"><span style="font-size: 20pt;">{{ $row['po_nomor'] }}</span></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px;">
                <table style="border: solid 0px #440000; width: 100%; margin-bottom: 50px;" cellspacing="0">
                    <tr>
                        <td style="text-align: center; width: 57%"><span style="font-weight: bold; font-size: 20pt;">No Container : </span></td>
                        <td style="text-align: center; width: 57%"><span style="font-weight: bold; font-size: 20pt;">Toko Lotte : </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: center; width: 57%"><span style="font-size: 20pt;">{{ $row['nomor'] }}</span></td>
                        <td style="text-align: center; width: 57%"><span style="font-size: 20pt;">{{ $row->tmuk->lsi->nama }}</span></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
               <barcode style="width:300px; height:60px; font-size: 4mm; " dimension="1D" type="C128" value="{{ $row['nomor'] }}" label="label"></barcode>
            </td>
        </tr>
    </table>
{{-- </div> --}}
</page>
@endforeach