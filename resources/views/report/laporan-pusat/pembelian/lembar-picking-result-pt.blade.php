<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  {{-- {{ dd($records) }} --}}
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 850px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Lembar Picking Result (PT)</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <tr class="boreder_head">
                <th class="boreder_head" style="text-align: center; width:150px;">LSI</th>
                <th class="boreder_head" style="text-align: center; width:150px;">TMUK</th>
                <th class="boreder_head" style="text-align: center; width:150px;">Tanggal PR</th>
                <th class="boreder_head" style="text-align: center; width:150px;">Nomor PR</th>
                <th class="boreder_head" style="text-align: center; width:150px;">Nilai PR</th>
                <th class="boreder_head" style="text-align: center; width:150px;">Tanggal Picking</th>
            </tr>
            @foreach ($records as $row)
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;">{{ $row->tmuk->lsi->nama }}</td>
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;">{{ $row->tmuk->nama }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:150px;">{{ $row->tgl_buat }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:150px;"> {{ $row->nomor }} </td>
                <td style="padding:5px 0 5px 0; text-align: right; width:150px;"> {{ rupiah($row->total) }} </td>
                <td style="padding:5px 0 5px 0; text-align: center; width:150px;"> {{ $row->updated_at }}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="6" style="text-align: center;  border-bottom: 1px solid #000"></td>
            </tr>

            <tr >
                <td style="padding:5px 0 5px 0;text-align: center; width:150px;"></td>
                <td style="padding:5px 0 5px 0;text-align: center; width:150px;"><b></b></td>
                <td style="padding:5px 0 5px 0;text-align: center; width:150px;"></td>
                <td style="padding:5px 0 5px 0;text-align: center; width:150px;"><b></b></td>
                <td style="padding:5px 0 5px 0;text-align: center; width:150px;"><b></b></td>
            </tr>
        </table>
  </body>
</html>