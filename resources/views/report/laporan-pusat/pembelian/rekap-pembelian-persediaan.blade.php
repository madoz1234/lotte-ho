<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 840px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Pembelian Persediaan</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="page_content tables" padding='0' margin="0" cellpadding="0" cellspacing="0" border="0" align="center">
            <thead style="display:table-header-group;">
                <tr class="boreder_head">
                    <th class="boreder_head" style="text-align: center; width:10px;">#</th>
                    <th class="boreder_head" style="text-align: center; width:95px;">LSI</th>
                    <th class="boreder_head" style="text-align: center; width:95px;">TMUK</th>
                    <th class="boreder_head" style="text-align: center; width:150px;">Tanggal Penjualan</th>
                    <th class="boreder_head" style="text-align: center; width:95px;">QTY</th>
                    <th class="boreder_head" style="text-align: center; width:150px;">Penjualan (Rp)</th>
                    <th class="boreder_head" style="text-align: center; width:150px;">Cost Price (Rp)</th>
                    <th class="boreder_head" style="text-align: center; width:150px;">Profit (Rp)</th>
                    <th class="boreder_head" style="text-align: center; width:150px;">Profit Margin (%)</th>
                </tr>
            </thead>
            <?php $is=1; ?>
            <body>
                @foreach($records as $row)
                    <tr>
                        <td style="padding:5px 0 5px 0; text-align: center; width:10px;">{{ $is++ }}</td>
                        <td style="padding:5px 0 5px 0; text-align: left; width:95px;">{{ $row->tmuk->lsi->nama }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:95px;">{{ $row->tmuk->nama }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:150px;">{{ $row->tanggal }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:95px;">{{ $row->qty }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:150px;">{{ rupiah($row->total) }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:150px;">{{ rupiah($row->harga) }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:150px;">{{ number_format($row->profit) }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:150px;">{{ number_format($row->margin) }}</td>

                    </tr>
                @endforeach
            </body>
        </table>
</html>