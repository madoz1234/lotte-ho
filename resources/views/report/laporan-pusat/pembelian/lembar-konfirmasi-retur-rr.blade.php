<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  {{-- {{ dd($rec) }} --}}
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 850px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Lembar Konfirmasi Retur (RR)</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        @foreach ($records as $rec)
        <table class="break-after tables" border="0">
            <tr>
                <th style="width: 30%">RR Number:</th>
                <th style="width: 20%">Date:</th>
                <th style="width: 30%">TMUK :</th>
                <th style="width: 20%">LSI :</th>
            </tr>
            <tr>
                <td style="width: 30%">{{ $rec['nomor_retur'] }}</td>
                <td style="width: 20%">{{ $rec->created_at }}</td>
                <td style="width: 30%">{{ $rec['tmuk_kode'] }} - {{ $rec->tmuk->nama }}</td>
                <td style="width: 20%">{{ $rec->tmuk->lsi->kode }} - {{ $rec->tmuk->lsi->nama }}</td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="width:10%; text-align: center;">Product Code</th>
                <th style="width:10%; text-align: center;">Barcode</th>
                <th style="width:20%; text-align: center;">Product Description</th>
                <th style="width:10%; text-align: center;">Selling Unit</th>
                <th style="width:10%; text-align: center;">Return Request Qty</th>
                <th style="width:10%; text-align: center;">Price</th>
                <th style="width:10%; text-align: center;">Total Price</th>
                <th style="width:10%; text-align: center;">Reason</th>
                <th style="width:10%; text-align: center;">Return Confirmed Qty</th>
            </tr>
        </table>
        <table class="page_content" border="0" align="center">
            <tr>
                <td style="width:10%; text-align: center;">{{ $rec['produk_kode'] or '' }}</td>
                <td style="width:10%; text-align: center;">{{ $rec['barcode'] or '' }}</td>
                <td style="width:20%; text-align: center;">{{ $rec['produk_nama'] or '' }}</td>
                <td style="width:10%; text-align: center;">{{ $rec['produk_kode'] or '' }}</td>
                <td style="width:10%; text-align: center;">{{ $rec['qty'] or '' }}</td>
                <td style="width:10%; text-align: center;"></td>
                <td style="width:10%; text-align: center;"></td>
                <td style="width:10%; text-align: center;">{{ $rec['reason'] or '' }}</td>
                <td style="width:10%; text-align: center;">{{ $rec['qty_retur'] or '' }}</td>
            </tr>
        </table>
    </page_header>

    <end_last_page end_height="37mm">
        <table class="page_footer" border="0" align="center">
            <tr>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    Return By:
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                    
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
                <td style=" width: 25%;text-align: left; padding: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 200px;height:80px;"></div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    {{-- <div style="border: 1px solid #000; width: 200px;height:80px;"></div> --}}
                </td>
            </tr>
        </table>
        <br><br><br><br><br>
        @endforeach
  </body>
</html>