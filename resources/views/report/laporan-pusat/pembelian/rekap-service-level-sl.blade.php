<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  {{-- {{ dd($records) }} --}}
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 850px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Service Level</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <tr class="boreder_head">
        <th class="boreder_head" style="text-align: center; width:83px;">LSI</th>
        <th class="boreder_head" style="text-align: center; width:83px;">TMUK</th>
        <th class="boreder_head" style="text-align: center; width:83px;">Tanggal PR</th>
        <th class="boreder_head" style="text-align: center; width:83px;">Nomor PR</th>
        <th class="boreder_head" style="text-align: center; width:83px;">QTY PR</th>
        <th class="boreder_head" style="text-align: center; width:83px;">Tanggal PO</th>
        <th class="boreder_head" style="text-align: center; width:83px;">Nomor PO</th>
        <th class="boreder_head" style="text-align: center; width:83px;">Qty PO</th>
        <th class="boreder_head" style="text-align: center; width:83px;">SL %</th>
    </tr>
    @foreach ($records as $row)
     {{-- {{ dd($row) }} --}}

        <tr style="">
            <td style="padding:5px 0 5px 0; text-align: left; width:83px;">{{ $row->tmuk->lsi->nama }}</td>
            <td style="padding:5px 0 5px 0; text-align: left; width:83px;">{{ $row->tmuk->nama }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:83px;">{{ $row->tanggal_pr }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:83px;">{{ $row->nomor_pr }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:83px;">{{ $row->qty_pr }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:83px;">{{ $row->tanggal_po }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:83px;">{{ $row->nomor_po }}</td>
            <td style="padding:5px 0 5px 0; text-align: center; width:83px;">{{ $row->qty_po }}</td>
            <?php $sl = $row->qty_po / $row->qty_pr ?>
            <td style="padding:5px 0 5px 0; text-align: center; width:83px;">{{ rupiah($sl*100) }}</td>
        </tr>
    @endforeach
    {{-- {{ dd($records) }} --}}
    <tr>
        <td colspan="9" style="text-align: center;  border-bottom: 1px solid #000"></td>
    </tr>

    <tr >
        <td style="padding:5px 0 5px 0;text-align: center; width:83px;"></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:83px;"><b></b></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:83px;"></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:83px;"><b></b></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:83px;"><b></b></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:83px;"><b></b></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:83px;"><b></b></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:83px;"><b></b></td>
        <td style="padding:5px 0 5px 0;text-align: center; width:83px;"><b></b></td>
    </tr>
        </table>
  </body>
</html>