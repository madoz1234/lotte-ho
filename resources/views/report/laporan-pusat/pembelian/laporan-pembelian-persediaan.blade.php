<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  {{-- {{ dd($records) }} --}}
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 850px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap Pembelian Persediaan</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <thead style="display:table-header-group;">
                <tr>
                    <th style="text-align: center; width:240px;">#</th>
                    <th style="text-align: center; width:240px;">Item Code</th>
                    <th style="text-align: center; width:70px;">Unit</th>
                    <th style="text-align: center; width:120px;">Harga</th>
                    <th style="text-align: center; width:95px;">Quantity Order</th>
                    <th style="text-align: center; width:150px;">Total</th>
                </tr>
            </thead>

        <?php $i=1; ?>
            @foreach ($records as $tmuk)
        <tr>
            <td colspan="6" style="background-color: #E7E9EC; font-weight: bold;">{{ $tmuk->kode }} - {{ $tmuk->nama }}</td>
        </tr>
        @foreach ($tmuk->pyr as $pyr)
            <tr>
                <td style="font-weight: bold;width:10px;">#</td>
                <td style="font-weight: bold;width:240px;">{{ $pyr->nomor_pyr }}</td>
                <td style="font-weight: bold;width:70px; text-align: center;">{{ $pyr->tgl_buat }}</td>
                <td style="width:120px;"></td>
                <td style="width:95px;"></td>
                <td style="width:150px;"></td>
            </tr>
            <?php
                $total_price = 0;
                $total_unit  = 0;
                $total_all   = 0;
            ?>
            @foreach($pyr->detailPyr as $dpyr)
                <?php
                    $total_price += (integer)$dpyr->price;
                    $total_unit  += $dpyr->qty;
                    $total_all   += (integer)$dpyr->price*(integer)$dpyr->qty;
                ?>
                <tr>
                    <td style="width:10px;">{{ $i }}</td>
                    <td style="width:240px;">{{ $dpyr->produk_kode }} - {{ $dpyr->produks->nama }}</td>
                    <td style="width:70px; text-align: center;">{{ $dpyr->unit }}</td>
                    <td style="text-align: right;width:120px;">{{ number_format((integer)$dpyr->price,2) }}</td>
                    <td style="text-align: center;width:95px;">{{ $dpyr->qty }}</td>
                    <td style="text-align: right;width:150px;">{{ number_format((integer)$dpyr->price*(integer)$dpyr->qty, 2) }}</td>
                </tr>
            @endforeach
            <tr>
                <td style="font-weight:bold;background-color:#EAFFE7; width:240px;"></td>
                <td style="font-weight:bold;background-color:#EAFFE7; width:240px;">SUB TOLTAL</td>
                <td style="font-weight:bold;background-color:#EAFFE7; width:70px; text-align: center;"></td>
                <td style="font-weight:bold;background-color:#EAFFE7; text-align: right;width:120px;">{{ number_format($total_price,2) }}</td>
                <td style="font-weight:bold;background-color:#EAFFE7; text-align: center;width:95px;">{{ $total_unit }}</td>
                <td style="font-weight:bold;background-color:#EAFFE7; text-align: right;width:150px;">{{ number_format($total_all,2) }}</td>
            </tr>
            <?php $i++; ?>
        @endforeach
    @endforeach
        </table>
  </body>
</html>