<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  @foreach ($records as $rec)
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 850px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>Rekap PO Barang Trade ke LSI </h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
            <tr>
                <td>Nomor PyR</td>
                <td>:  {{ $rec->nomor_pyr or '' }}</td>
            </tr>
            <tr>
                <td></td>
                <td>{{ $rec->tmuk->lsi->nama or '' }} - {{ $rec->tmuk->nama or '' }}</td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <tr class="boreder_head">
                <th class="boreder_head" style="text-align: center; width:90px;">Item Code</th>
                <th class="boreder_head" style="text-align: center; width:250px;">Description</th>
                <th class="boreder_head" style="text-align: center; width:100px;">Unit</th>
                <th class="boreder_head" style="text-align: center; width:100px;">Harga</th>
                <th class="boreder_head" style="text-align: center; width:90px;">QTY Order</th>
                <th class="boreder_head" style="text-align: center; width:100px;">Total</th>
            </tr>
            @if($rec['detailPyr'])
            {{-- {{ dd($rec['detailPyr']) }} --}}
                <?php
                    $total_harga        = 0;
                    $total_qty          = 0;
                    $total_sluruh_harga = 0;
                ?>
                @foreach ($rec['detailPyr'] as $data)
                {{-- {{ dd($data) }} --}}
                    <?php
                        $total_harga        += (integer)$data['price'] or 0;
                        $total_qty          += $data['qty'] or 0;
                        $total_sluruh_harga += ($data['qty']*((integer)$data['price']));
                    ?>
                    <tr style="">
                        <td style="padding:5px 0 5px 0; text-align: center; width:90px;">{{ $data['produk_kode'] or '' }}</td>
                        <td style="padding:5px 0 5px 0; text-align: ; width: 271px;">{{ $data->produks->nama or '' }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:100px;">{{ $data['unit'] or 0 }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:100px;">{{  Rupiah($data['price']) }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:90px;">{{ $data['qty'] or 0 }}</td>
                        <td style="padding:5px 0 5px 0; text-align: center; width:100px;">{{ Rupiah(($data['qty']*((integer)$data['price']))) }}</td>
                    </tr>
                @endforeach

                <tr>
                    <td colspan="6" style="text-align: center;  border-bottom: 1px solid #000"></td>
                </tr>

                <tr >
                    <td style="padding:5px 0 5px 0;text-align: center; width:90px;"></td>
                    <td style="padding:5px 0 5px 0;text-align: center; width: 271px;"><b>Sub Total :</b></td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:100px;"></td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:100px;"><b>{{ Rupiah($total_harga) }}</b></td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:90px;"><b>{{ $total_qty }}</b></td>
                    <td style="padding:5px 0 5px 0;text-align: center; width:100px;"><b>{{ Rupiah($total_sluruh_harga) }}</b></td>
                </tr>
            @endif
        </table>
        <br><br>
  </body>
  @endforeach
</html>