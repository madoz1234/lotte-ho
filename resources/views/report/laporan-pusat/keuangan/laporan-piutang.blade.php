<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
                font-size: 11px;
                font-family: sans-serif;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-4px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>MUTASI ESCROW</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table class="break-after tables" border="0">
            <thead style="display:table-header-group;">
                <tr class="boreder_head">
                    <th style="text-align: center; width:10mm;">No</th>
                    <th style="text-align: center; width:50mm;">TMUK</th>
                    <th style="text-align: center; width:20mm;">Tanggal</th>
                    <th style="text-align: center; width:60mm;">Deskripsi</th>
                    <th style="text-align: center; width:25mm;">Debit</th>
                    <th style="text-align: center; width:25mm;">Kredit</th>
               </tr>
            </thead>
            <tbody>
                <?php $i = 1; $total_debit = 0; $total_kredit = 0;?>
                @foreach ($records as $row)
                    <tr>
                        <td style="text-align: center; width:10mm">{{ $i }}</td>
                        <td style="text-align: left; width:50mm">{{ $row->tmuk->nama }}</td>
                        <td style="text-align: center; width:20mm">{{ \Carbon\Carbon::parse($row->tanggal)->format('d/m/Y') }}</td>
                        <td style="text-align: left; width:60mm">{{ ucfirst($row->deskripsi) }}</td>
                        <td style="text-align: right; width:25mm">{{  number_format($row->posisi=='D' ? $total_debit += $row->jumlah : '0') }}</td>
                        <td style="text-align: right; width:25mm">{{  number_format($row->posisi=='K' ? $total_kredit += $row->jumlah : '0') }}</td>
                    </tr>
                <?php $i++; ?>
                @endforeach
                {{-- <tr>
                    <td style="text-align: center; width:10mm"></td>
                    <td style="text-align: center; width:50mm"></td>
                    <td style="text-align: center; width:20mm"></td>
                    <td style="text-align: center; width:60mm"><b>Total</b></td>
                    <td style="text-align: right; width:25mm">{{  number_format($total_debit) }}</td>
                    <td style="text-align: right; width:25mm">{{  number_format($total_kredit) }}</td>
                </tr> --}}
            </tbody>
            </table>
  </body>
</html>