<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
                font-size: 11px;
                font-family: sans-serif;
        }
        * {
          overflow: visible !important;
        }
        .tables td, th {    
            border: 1px solid #ddd;
        }

        .tables {
            border-collapse: collapse;
        }

        .tables th {
            font-size: 12px;
            padding: 5px;
            text-align: center;
        }
        .tables td {
            font-size: 10px;
            padding: 4px;
            text-align: left;
        }

        .title{
            font-weight: bold;
        }
        .tittleMargin{
            /*padding: 10;*/
            /*margin: 2px;*/
            font-size: 24px;
        }

        #text_header{
            width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
        }

    /***Always insert a page break before the element***/
           .pb_before {
               page-break-before: always !important;
           }

    /***Always insert a page break after the element***/
           .pb_after {
               page-break-after: always !important;
           }

    /***Avoid page break before the element (if possible)***/
           .pb_before_avoid {
               page-break-before: avoid !important;
           }

    /***Avoid page break after the element (if possible)***/
           .pb_after_avoid {
               page-break-after: avoid !important;
           }

    /* Avoid page break inside the element (if possible) */
           .pbi_avoid {
               page-break-inside: avoid !important;
           }
    </style>
  </head>
  <body>
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-4px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px;text-align: center;">
                    <p class="title" style=""><h3>MUTASI ESCROW</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $tmuk }} </td>
            </tr>
        </table>
        <br>
        <table id="reportTable" class="ui celled compact red tables" width="100%" cellspacing="0">
            <thead class="">
                <tr>
                    <th class="center aligned" width="10px">#</th>
                    <th class="center aligned" width="25px">Tanggal</th>
                    <th class="center aligned" width="25px">Deskripsi</th>
                    <th class="center aligned" width="25px">Saldo</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=1; ?>
                @foreach ($records as $row)
                    <tr>
                        <td style="text-align: center;">{{ $i }}</td>
                        <td>{{ $row->tanggal }}</td>
                        <td>{{ ucfirst($row->deskripsi) }}</td>
                        <td  style="text-align: right;">{{ FormatNumber($row->jumlah) }}</td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
  </body>
</html>
