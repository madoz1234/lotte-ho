<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
        /* Page Breaks */

        /***Always insert a page break before the element***/
        .pb_before {
           page-break-before: always !important;
        }

        /***Always insert a page break after the element***/
        .pb_after {
           page-break-after: always !important;
        }

        /***Avoid page break before the element (if possible)***/
        .pb_before_avoid {
           page-break-before: avoid !important;
        }

        /***Avoid page break after the element (if possible)***/
        .pb_after_avoid {
           page-break-after: avoid !important;
        }

        /* Avoid page break inside the element (if possible) */
        .pbi_avoid {
           page-break-inside: avoid !important;
        }
    </style>
  </head>
  <body>
  @foreach ($records as $row)
    <div class="pb_after">
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-6px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>LAPORAN REALISASI RAB </h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $row->tmuk->nama }} </td>
            </tr>
        </table>
        <br>
         <table class="break-after tables" border="0">
    
    <tr class="boreder_head">
        <th class="boreder_head" style="text-align: center; width:150px;">RAB</th>
        <th class="boreder_head" style="text-align: right; width:125px;">Total</th>
        <th class="boreder_head" style="text-align: center; width:30px;"></th>
        <th class="boreder_head" style="text-align: center; width:250px;">REALISASI</th>
        <th class="boreder_head" style="text-align: right; width:125px;">Total</th>
    </tr>
    {{-- {{ dd($row) }} --}}
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:150px;">Perkiraan dana investasi</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
        <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
        <td style="padding:5px 0 5px 0; text-align: left; width:250px;">Pemasukan</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
    </tr>
    <?php $sum_tot = 0 ?>
    @foreach ($request['saldo'] as $jual)
        <tr style="">
        @if ($row->tmuk->kode == $jual->tmuk_kode)
            <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;"></td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
            <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
            <td style="padding:5px 0 5px 0; text-align: left; width:250px;">{{ \Carbon\Carbon::createFromFormat('m-Y',$jual->month.'-'.$jual->year)->format('F  Y') }}</td>
            <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($jual->total_penjualan) }}</td>
        <?php $sum_tot += $jual->total_penjualan ?>
        @endif
        </tr>
    @endforeach
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">DP</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_satu) }}</td>
        <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
        <td style="padding:5px 0 5px 0; text-align: left; width:250px;"></td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($sum_tot) }}</b></td>
    </tr>
    <tr style="">
        <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Dana investasi</td>
        <?php $invest = $row->i_dua + $row->i_tiga_a + $row->i_tiga_b + $row->i_tiga_c + $row->i_tiga_d + $row->i_tiga_e + $row->i_tiga_f + $row->i_empat_a + $row->i_empat_b + $row->i_empat_c + $row->i_lima_a + $row->i_lima_b + $row->i_bila_ada_biaya_tambahan + $row->i_sewa_ruko + $row->i_ppn; ?>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($invest) }}</td>
        <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
        <td style="padding:5px 0 5px 0; text-align: left; width:250px;">Pengeluaran</td>
        <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
    </tr>
        <?php 
            $start          = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 1,$row->tmuk->kode); 
            $perijinan      = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 2,$row->tmuk->kode);
            $renovasi       = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 3,$row->tmuk->kode);
            $kusen          = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 4,$row->tmuk->kode);
            $folding        = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 5,$row->tmuk->kode);
            $instalasi      = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 6,$row->tmuk->kode);
            $pemasangan     = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 7,$row->tmuk->kode);
            $signage        = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 8,$row->tmuk->kode);
            $sipil          = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 9,$row->tmuk->kode);
            $non_elektronik = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 10,$row->tmuk->kode);
            $elektronik     = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 11,$row->tmuk->kode);
            $stock_awal     = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 12,$row->tmuk->kode);
            $modal_kerja    = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 13,$row->tmuk->kode);
            $lain_lain      = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 14,$row->tmuk->kode);
        ?>

            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: right; width:150px;"></td>
                <?php $totaldana = $row->i_satu + $invest; ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($totaldana) }}</b> </td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Start & Biaya Promosi Opening</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($start ? $start : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Perijinan Usaha Toko</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($perijinan ? $perijinan : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;">Perkiraan pengeluaran</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Renovasi Sipil</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($renovasi ? $renovasi : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Stok awal</td>
                <?php $stokawal = $row->i_lima_a; ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($stokawal)}}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Kusen Aluminium + Kaca Cermin</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($kusen ? $kusen : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Folding Gate + Polycarbonate = Teras</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($folding ? $folding : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Pekerjaan sipil</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_tiga_a + $row->i_tiga_b + $row->i_tiga_c + $row->i_tiga_d + $row->i_tiga_e + $row->i_tiga_f) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Instalasi Listrik (lampu, regrouping)</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($instalasi ? $instalasi : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Instalasi Pemasangan AC</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($pemasangan ? $pemasangan : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Peralatan Elektonik</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_empat_a + $row->i_empat_c) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Pekerjaan Signage (Lisping Papan Nama Toko) Repainting</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($signage ? $signage : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Peralatan AC Sipil</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($sipil ? $sipil : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Peralatan Non-Elektronik</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_empat_b) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Peralatan Non Elektronik (Rak, Furniture, Acrylic, dll)</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($non_elektronik ? $non_elektronik : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Peralatan Eletronika (Komputer, Cooler & Timbangan)</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($elektronik ? $elektronik : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;"></td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Quantum 1 burner Qgc & selang</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($stock_awal ? $stock_awal : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Modal Kerja</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_lima_b) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px; padding-left: 20px;">Modal Kerja 1 bulan (ditransfer ke rek Ops TMUK)</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($modal_kerja ? $modal_kerja : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;">Lain-lain</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($row->i_bila_ada_biaya_tambahan) }}</td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px ; padding-left: 20px;">Lain-lain</td>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;">{{ rupiah($lain_lain ? $lain_lain : '0') }}</td>
            </tr>
            <tr style="">
                <td style="padding:5px 0 5px 0; text-align: left; width:150px;padding-left: 20px;"></td>
                <?php $subtotal_perkiraan = $row->i_lima_a + $row->i_tiga_a + $row->i_tiga_b + $row->i_tiga_c + $row->i_tiga_d + $row->i_tiga_e + $row->i_tiga_f + $row->i_empat_a + $row->i_empat_c + $row->i_empat_b + $row->i_satu + $row->i_lima_b + $row->i_bila_ada_biaya_tambahan; ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($subtotal_perkiraan) }}</b></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: left; width:250px ; padding-left: 20px;"></td>
                <?php $subtotal_pengeluaran = $start + $perijinan + $renovasi + $kusen + $folding + $instalasi + $pemasangan + $signage + $sipil + $non_elektronik + $elektronik + $stock_awal + $modal_kerja + $lain_lain; ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($subtotal_pengeluaran) }}</b></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: center;  border-bottom: 1px solid #000"></td>
            </tr>

            <tr >
                <td style="padding:5px 0 5px 0;text-align: center; width:150px;"></td>
                <?php $total_rab = $totaldana - $subtotal_perkiraan; ?>
                <td style="padding:5px 0 5px 0;text-align: right; width:125px;"><b>{{ rupiah($total_rab) }}</b></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:30px;"></td>
                <td style="padding:5px 0 5px 0; text-align: center; width:250px;"></td>
                <?php $total_realisasi = $sum_tot - $subtotal_pengeluaran; ?>
                <td style="padding:5px 0 5px 0; text-align: right; width:125px;"><b>{{ rupiah($total_realisasi) }}</b></td>
            </tr>
        </table>
    </div>
    @endforeach

  </body>

</html>