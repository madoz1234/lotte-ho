<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
                font-size: 11px;
                font-family: sans-serif;
        }
        * {
          overflow: visible !important;
        }
        .tables td, th {    
            border: 1px solid #ddd;
        }

        .tables {
            border-collapse: collapse;
        }

        .tables th {
            font-size: 12px;
            padding: 5px;
            text-align: center;
        }
        .tables td {
            font-size: 10px;
            padding: 4px;
            text-align: left;
        }

        .title{
            font-weight: bold;
        }
        .tittleMargin{
            /*padding: 10;*/
            /*margin: 2px;*/
            font-size: 24px;
        }

        #text_header{
            width: 100%;text-align: left; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
        }

    /***Always insert a page break before the element***/
           .pb_before {
               page-break-before: always !important;
           }

    /***Always insert a page break after the element***/
           .pb_after {
               page-break-after: always !important;
           }

    /***Avoid page break before the element (if possible)***/
           .pb_before_avoid {
               page-break-before: avoid !important;
           }

    /***Avoid page break after the element (if possible)***/
           .pb_after_avoid {
               page-break-after: avoid !important;
           }

    /* Avoid page break inside the element (if possible) */
           .pbi_avoid {
               page-break-inside: avoid !important;
           }
    </style>
  </head>
  <body>
    {{-- {{ dd($records) }} --}}
    @foreach($records as $record)
    <div class="pb_after">
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-4px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px;text-align: center;">
                    <p class="title" style=""><h3>NERACA ({{ $record['tmuk_nama'] }})</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $request['tanggal_start'] }} - {{ $request['tanggal_end'] }} </td>
            </tr>

            <tr>
                <td>Region</td>
                <td>: {{ $region }}  </td>
            </tr>
            <tr>
                <td>LSI</td>
                <td>: {{ $lsi }}  </td>
            </tr>
            <tr>
                <td>TMUK</td>
                <td>: {{ $record['tmuk_nama'] }} </td>
            </tr>
        </table>
        <br>
        @if(!isset($record['tmuk_list']['header']))
            <div class="ui segment">
              <p align="center">Filter tanggal terlebih dahulu</p>
            </div>
        @else
            <table  class="ui celled compact red table tables" width="90%" cellspacing="0" cellpadding="0" margin="0" padding ="0">
                    <tr>
                        <th class="center aligned page_content_header">Description</th>
                        @foreach($record['tmuk_list']['header'] as $row)
                            <th class="center aligned page_content_header">{{ $row }}</th>
                        @endforeach
                    </tr>
                    @foreach($record['tmuk_list']['content'] as $row)
                        @if($row['type'] == 'label')
                            @if(isset($row['child']))
                                <tr>
                                    <td class="left aligned" colspan="{{ count($record['tmuk_list']['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                                </tr>
                                @foreach($row['child'] as $rowchild)
                                    <tr>
                                        <td class="left aligned">&nbsp;&nbsp;&nbsp;&nbsp;{{ $rowchild['text'] }}</td>
                                        @foreach($rowchild['data'] as $val)
                                            <td class="right aligned">{{ curencyformat($val) }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="left aligned" colspan="{{ count($record['tmuk_list']['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                                </tr>
                            @endif
                        @else
                            <tr>
                                <td class="left aligned"><b>{{ $row['text'] }}</b></td>
                                @foreach($row['data'] as $val)
                                    <td class="right aligned"><b>{{ curencyformat($val) }}</b></td>
                                @endforeach
                            </tr>
                        @endif
                    @endforeach
            </table>
        @endif
    </div>
    @endforeach
  </body>
</html>

