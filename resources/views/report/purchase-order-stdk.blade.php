<link rel="stylesheet" href="{{ asset("font/code128/stylesheet.css") }}" type="text/css" charset="utf-8" />
<style type="text/css">

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;}
table.page_footer {width: 100%; border: none; border-bottom: solid 0.3mm #000000; padding: 2mm; font-size: 11px;}
table.page_content_header {width: 100%; border: none; padding: 1mm; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}

.digital {
    font-family: "code_128regular";
    width: 100%;
}
</style>
<page backtop="68mm" backbottom="5mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" width="" align="center">
            <tr>
                <td style="width: 480px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="150" style="margin-top: 10px;">
                </td>
                <td style="width: 480px;" id="text_header">
                    <p class="title tittleMargin">PURCHASE ORDER &nbsp;<span style="font-size: 12px">(STDK)</span></p>
                    <div class="digital" style="font-size: 50px;">{{ $record->nomor or "" }}</div>
                    <div style="font-size: 25px;">{{ $record->nomor or "" }}</div>
                    {{-- <barcode dimension="1D" type="C128" value="{{ $record->nomor or "" }}" label="label" style="width:78mm; height:10mm;"></barcode> --}}
                </td>
            </tr>
            <tr>
                {{-- <td>Halaman [[page_cu]] dari [[page_nb]]</td> --}}
                <td></td>
            </tr>
            <tr>
                <td><b>User</b> : {{\Auth::user()->name}} ({{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }})</td>
                <td></td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 40%">LSI :</td>
                <td style="width: 40%">TMUK :</td>
                <td style="width: 20%"></td>
            </tr>
            <tr>
                <td style="width: 40%; vertical-align: text-top;">{{ $record->tmuk->lsi->kode or "" }} - {{ $record->tmuk->lsi->nama or "" }}</td>
                <td style="width: 40%; vertical-align: text-top;">{{ $record->tmuk->kode or "" }} - {{ $record->tmuk->nama or "" }}</td>
                <td style="width: 20%; vertical-align: text-top;"></td>
            </tr>
        </table>
        <table class="page_header" border="0" align="center">
            <tr>
                <td style="width: 25%">No PO : {{ $record->nomor or "" }}</td>
                <td style="width: 25%">Tanggal Pemesanan : {{ \Carbon\Carbon::createFromFormat('Y-m-d', $record->tgl_buat)->format('d/m/Y') }}</td>
                <td style="width: 25%">PO Type : Lotte</td>
                <td style="width: 25%">Expected Delivery Date : {{ \Carbon\Carbon::createFromFormat('Y-m-d', $record->pr->tgl_kirim)->format('d/m/Y') }}</td>
            </tr>
        </table>
        <table class="page_content_header" border="0" align="center">
            <tr>
                <th style="width:8%">Kode Produk</th>
                <th style="width:10%">Barcode</th>
                <th style="width:30%">Nama Produk</th>
                <th style="width:6%; text-align: center;">Qty/UOM</th>
                <th style="width:7%; text-align: center;">UOM</th>
                <th style="width:6%; text-align: center;">Order Qty</th>
                <th style="width:7%; text-align: center;">Order Unit</th>
                <th style="width:12%; text-align: right;">Harga Satuan</th>
                <th style="width:12%; text-align: right;">Total Harga</th>
                <th style="width:2%; text-align: right;"></th>
            </tr>
        </table>
    </page_header>
    <br>

    <table class="page_content" border="0" align="center">
        <col style="width: 8%">
        <col style="width: 10%">
        <col style="width: 32%">
        <col style="width: 6%">
        <col style="width: 7%">
        <col style="width: 6%">
        <col style="width: 7%">
        <col style="width: 12%">
        <col style="width: 12%">
        
        @foreach ($detail as $row)
            <tr>
                <td style="width:8%">{{ $row['produk_kode'] }}</td>
                <td style="width:10%">{{ $row['uom1_barcode'] }}</td>
                <td style="width:30%">{{ $row['nama'] }}</td>
                <td style="width:6%; text-align: center;">{{ $row['qty_sell'] }}</td>
                <td style="width:7%; text-align: center;">{{ $row['unit_sell'] }}</td>
                <td style="width:6%; text-align: center;">{{ $row['qty_po'] }}</td>
                <td style="width:7%; text-align: center;">{{ $row['unit_po'] }}</td>
                <?php 
                    if ($row['qty_po'] != 0) {
                        $price_pcs = $row['harga_aktual'] / $row['qty_po'];
                    }else{
                        $price_pcs = 0;
                    }
                ?>
                <td style="width:12%; text-align: right;">{{ FormatNumber($price_pcs) }}</td>
                <td style="width:12%; text-align: right;">{{ FormatNumber($row['harga_aktual']) }}</td>
                <th style="width:2%; text-align: right;"></th>
            </tr>
        @endforeach
    </table>

    <end_last_page end_height="37mm">
        <table class="page_footer" align="center" border="0">
            <tr>
                <th style="width: 8%; text-align: right;"></th>
                <th style="width: 8%; text-align: right;"></th>
                <th style="width: 40%; text-align: center;"></th>
                <th style="width: 30%; text-align: center">Sub-total</th>
                <th style="width: 12%; text-align: right;">{{ FormatNumber($detail->sum('harga_aktual')) }}</th>
                <th style="width: 2%; text-align: right;"></th>
            </tr>
            <tr>
                <th style="width: 8%; text-align: right;"></th>
                <th style="width: 8%; text-align: right;"></th>
                <th style="width: 40%; text-align: center; font-size: 14px; font-weight: bold;">No Faktur Lotte:</th>
                <th style="width: 30%; text-align: center;">TOTAL PO</th>
                <th style="width: 12%; text-align: right;">{{ FormatNumber($detail->sum('harga_aktual')) }}</th>
                <th style="width: 2%; text-align: right;">&nbsp;</th>
            </tr>
            <tr>
                <th style="width: 8%; text-align: right;"></th>
                <th style="width: 8%; text-align: right;"></th>
                <th style="width: 40%; text-align: center;"></th>
                <th style="width: 30%; text-align: center;">NILAI transaksi di kasir</th>
                <th style="width: 12%; text-align: right;">{{ FormatNumber($escrow->saldo_dipotong) }}</th>
                <th style="width: 2%; text-align: right;"></th>
            </tr>
            <tr>
                <th colspan="6">&nbsp;</th>
            </tr>
        </table>
    </end_last_page>
</page>