@section('scripts') 
	<script type="text/javascript">
		$(document).ready(function() {
		// Highcharts.setOptions({
		//     lang: {
		//         numericSymbols: [' Rb', ' Jt', ' M', ' T']
		//     }
		// });
		// komment dulu
		var dtpenkat = '';
		var dtdevpen = '';
		var dtstatpo = '';
		$('button[data-content]').popup({
			hoverable: true,
			position : 'top center',
			delay: {
				show: 300,
				hide: 800
			}
		});
		// komment dulu

			//Fungsi Penjualan
			penjualan_mom = function(){
				$('#penjualan_mom .load').show();
				$.ajax({
					url: '{{ url('penjualan/charpenjualantmom') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token	  : '{!! csrf_token() !!}',
						start 	  : $("[name=mom_start]").val(),
						end       : $("[name=mom_end]").val(),
						region_id : $("#mom_penjualan_region_id").val(),
						lsi_id    : $("#mom_penjualan_lsi_id").val(),
						tmuk_code : $("#mom_penjualan_tmuk").val()
					},
				})
				.done(function(response) {

					$('#penjualan_mom').highcharts(response);
					$('#penjualan_mom .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

			penjualan_qoq = function(){
				// alert('tara');
				$('#penjualan_qoq .load').show();
				$.ajax({
					url: '{{ url('penjualan/charpenjualantqoq') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token:'{!! csrf_token() !!}',
						start 	  : $("[name=qoq_penjualan_triwulan_start]").val(),
						end       : $("[name=qoq_penjualan_triwulan_end]").val(),
						region_id : $("#qoq_penjualan_region_id").val(),
						lsi_id    : $("#qoq_penjualan_lsi_id").val(),
						tmuk_code : $("#qoq_penjualan_tmuk").val()
					},
				})
				.done(function(response) {
					$('#penjualan_qoq').highcharts(response);
					$('#penjualan_qoq .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

			penjualan_yoy = function(){
				// alert('tara');
				$('#penjualan_yoy .load').show();
				$.ajax({
					url: '{{ url('penjualan/charpenjualantyoy') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token:'{!! csrf_token() !!}',
						start 	  : $("[name=yoy_penjualan_start]").val(),
						end       : $("[name=yoy_penjualan_end]").val(),
						region_id : $("[name=yoy_penjualan_region_id]").val(),
						lsi_id    : $("[name=yoy_penjualan_lsi_id]").val(),
						tmuk_code : $("#yoy_penjualan_tmuk").val()
					},
				})
				.done(function(response) {
					$('#penjualan_yoy').highcharts(response);
					$('#penjualan_yoy .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

			//Fungsi Pembelian
			pembelian_mom = function(){
				$('#pembelian_mom .load').show();
				$.ajax({
					url: '{{ url('penjualan/charpembelianmom') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token : '{!! csrf_token() !!}',
						start  		: $('[name=mom_pembelian_start]').val(),
						end    		: $('[name=mom_pembelian_end]').val(),
						region_id   : $('#mom_pembelian_region_id').val(),
						lsi_id    	: $('#mom_pembelian_lsi_id').val(),
						tmuk_code   : $('[name=mom_pembelian_tmuk]').val(),
					},
				})
				.done(function(response) {
					$('#pembelian_mom').highcharts(response);
					$('#pembelian_mom .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

			pembelian_qoq = function(){
				// alert('tara');
				$('#pembelian_qoq .load').show();
				$.ajax({
					url: '{{ url('penjualan/charpembelianqoq') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token:'{!! csrf_token() !!}',
						start 	  : $("[name=qoq_pembelian_triwulan_start]").val(),
						end       : $("[name=qoq_pembelian_triwulan_end]").val(),
						region_id : $("#qoq_pembelian_triwulan_region_id").val(),
						lsi_id    : $("#qoq_pembelian_triwulan_lsi_id").val(),
						tmuk_code : $("#qoq_pembelian_triwulan_tmuk").val()
					},
				})
				.done(function(response) {
					$('#pembelian_qoq').highcharts(response);
					$('#pembelian_qoq .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

			pembelian_yoy = function(){
				// alert('tara');
				$('#pembelian_yoy .load').show();
				$.ajax({
					url: '{{ url('penjualan/charpembelianyoy') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token:'{!! csrf_token() !!}',
						start 	    : $("[name=yoy_pembelian_start]").val(),
						end         : $("[name=yoy_pembelian_end]").val(),
						region_id   : $('#yoy_pembelian_region_id').val(),
						lsi_id    	: $('#yoy_pembelian_lsi_id').val(),
						tmuk_code   : $("#yoy_pembelian_tmuk").val()
					},
				})
				.done(function(response) {
					$('#pembelian_yoy').highcharts(response);
					$('#pembelian_yoy .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

			//Fungsi IO Level
			io_level = function(){
				$('#io_level .load').show();
				$.ajax({
					url: '{{ url('penjualan/chariolevel') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token:'{!! csrf_token() !!}',
						start 	  : $("[name=io_level_start]").val(),
						end       : $("[name=io_level_end]").val(),
						region_id : $("#io_level_region_id").val(),
						lsi_id    : $("#io_level_lsi_id").val(),
						tmuk_code : $("#io_level_tmuk").val()
					},
				})
				.done(function(response) {
					$('#io_level').highcharts(response);
					$('#io_level .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

			//Fungsi Service Level
			service_level = function(){
				$('#service_level .load').show();
				$.ajax({
					url: '{{ url('penjualan/charservicelevel') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token:'{!! csrf_token() !!}',
						start 	  : $("[name=service_level_start]").val(),
						end       : $("[name=service_level_end]").val(),
						region_id : $("#service_level_region_id").val(),
						lsi_id    : $("#service_level_lsi_id").val(),
						tmuk_code : $("#service_level_tmuk").val()
					},
				})
				.done(function(response) {
					$('#service_level').highcharts(response);
					$('#service_level .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

			// Fungsi Deviasi Penjualan
			// defiasi_penjualan = function(){
			// 	$('#defiasi_penjualan .load').show();
			// 	$.ajax({
			// 		url: '{{ url('dashboard/deviasipenjualan') }}',
			// 		type: 'POST',
			// 		dataType: 'json',
			// 		data: {
			// 			_token:'{!! csrf_token() !!}',
			// 			tmuk_code : $("#filter[tmuk_kode]").val()
			// 		},
			// 	})
			// 	.done(function(response) {
			// 		$('#defiasi_penjualan').highcharts(response);
			// 		$('#defiasi_penjualan .load').hide();
			// 	})
			// 	.fail(function() {
			// 		console.log("error");
			// 	})
			// 	.always(function() {
			// 		console.log("complete");
			// 	});
			// };

		defiasi_penjualan =  function(){
		 	// alert($("#defiasi_tmuk_kode").val());
		 	dtdevpen.ajax.reload();

		 };

			//Fungsi Penjualan Kategori 1
			var dtpenkat = $('#dataTablePenjualanKategori').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
	        destroy: true,
			responsive: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			lengthChange: true,
			pageLength: 10,
			filter: false,
			sorting: [],
			info:true,
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			ajax:  {
				url: "{{ url($pageUrl) }}/penjualan-kategori/grid",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
					d.tmuk_kode = $("#penkat_tmuk_kode").val();
				}
			}, 
			columns: {!! json_encode($tableStructPenjualanKategori) !!},
			drawCallback: function() {
				var api = this.api();
			// drawCallback: function(response) {
				// alert('tara');
				// var api = this.api();

				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML)+i+1;
				} );

				var json = api.ajax.json();
				$("#pen_tmuk").html(json.tmuk);
				$("#pen_lsi").html(json.lsi);
				$("#pen_region").html(json.region);
				// alert(json.tmuk);

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});
			},
		});

			// $('#search-form').on('click', function(e) {
		 //        dtpenkat.draw();
		 //        e.preventDefault();
		 //    });
		 // filter_penjualan =  function(){
		 // 	alert('tara');
		 // 	dtpenkat.ajax.reload();

		 // };

		 penjualan_kategori =  function(){
		 	// alert('tara');
		 	dtpenkat.ajax.reload();

		 };

		 //    $('.filter.button').on('click', function(e) {
		 //    	// alert('tara');
			// 	var length = $("input[name='filter[item_code]']").val();
			// 	length = (length != '') ? length : 10;
			// 	// dtpenkat.page.len(length).draw();
			// 	e.preventDefault();
			// });
			// $('.filter.button').change( function(e) { 
			//     dtpenkat.page.len( $(this).val() ).draw();
			// });

			$('.reset.button').on('click', function(e) {
				$('.dropdown .delete').trigger('click');
				$('.dropdown').dropdown('clear');
				setTimeout(function(){
					var length = $("input[name='filter[entri]']").val();
					length = (length != '') ? length : 10;
					dtpenkat.page.len(length).draw();
				}, 100);
			});



			//Fungsi Deviasi
			var dtdevpen = $('#dataTableDeviasiPenjualan').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
			destroy: true,
			responsive: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			lengthChange: true,
			pageLength: 10,
			filter: false,
			sorting: [],
			info:true,
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			ajax:  {
				url: "{{ url($pageUrl) }}/deviasi-penjualan/grid",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
					d.tmuk_kode = $("#defiasi_tmuk_kode").val();
				}
			}, 
			columns: {!! json_encode($tableStructDeviasiPenjualan) !!},
			drawCallback: function() {
				var api = this.api();

				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML)+i+1;
				} );

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});
			}
		});



			//statistik po
			var dtstatpo = $('#dataTableStatistikPo').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
			destroy: true,
			responsive: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			lengthChange: true,
			pageLength: 10,
			filter: false,
			sorting: [],
			info:true,
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			ajax:  {
				url: "{{ url($pageUrl) }}/statistik-po/grid",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
				}
			}, 
			columns: {!! json_encode($tableStructStatistikPo) !!},
			drawCallback: function() {
				var api = this.api();

				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML)+i+1;
				} );

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});
			}
		});

			penjualan_mom();
			// penjualan_yoy();
			pembelian_mom();
			io_level();
			service_level();
			// penjualan_kategori();
			// deviasi_penjualan();
			// statistik_periode();


			$('.date_').calendar({
				type: 'date',
			});

			$('.mom_statrt').calendar({
				type: 'month',
			});

			$('.mom_end').calendar({
				type: 'month',
				onChange: function (date, text) {
			     penjualan_mom();
   			 	},
			});


			$('.ui.month').calendar({
				type: 'month'
			});

			$('.ui.year').calendar({
				type: 'year'
			});
			$(document).ready(function(){
				$('.tabular.menu .item').tab();
			});

		});	
	</script>
@append