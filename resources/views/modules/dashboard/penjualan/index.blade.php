<?php

	// dd($opt);
	// foreach ($array as $element)
?>

@include('modules.dashboard.penjualan.js')

<script type="text/javascript">

</script>
<div class="ui equal width grid">
	<div class="column">
		<div class="ui top attached tabular menu">
			{{-- <a class="active item" data-tab="penjualan-all">Semua Penjualan</a> --}}
			<a class="active item" data-tab="penjualan-mom" onclick="penjualan_mom();">MoM</a>
			<a class="item" data-tab="penjualan-qoq" onclick="penjualan_qoq();">QoQ</a>
			<a class="item" data-tab="penjualan-yoy" onclick="penjualan_yoy();">YoY</a>
		</div>
		<div class="ui bottom attached tab active segment" data-tab="penjualan-mom">
			<form action="" class="ui form">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui mom_statrt" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" name="mom_start" value="{{ date('F Y', strtotime(date('M Y') . ' -5 month')) }}">
										</div>
									</div>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui mom_end" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" name="mom_end" value="{{ date('F Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field pull-right">		
								<button type="button" class="ui right floated default left labeled icon save button" onclick="penjualan_mom();">
									<i class="search icon"></i>
									Cari
								</button>
							</div> 
						</div>
					</div>
				</div>
				{{-- <div class="two fields">
					<div class="field">		
						<div class="ui mom_statrt" id="from">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Dari Bulan" name="mom_start" value="{{ date('F Y', strtotime(date('M Y') . ' -5 month')) }}">
							</div>
						</div>
					</div> 
					<p style="text-align: center; margin: 10px; ">s.d.</p>
					<div class="field">		
						<div class="ui mom_end" id="to">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Sampai Bulan" name="mom_end" value="{{ date('F Y') }}">
							</div>
						</div>
					</div>
				</div> --}}

				<div class="three fields">
					<div class="field">
						<select name="region_id" class="" id='mom_penjualan_region_id' style="width: 100%;">
							<option value="0">-- All Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="lsi_id" class="" id="mom_penjualan_lsi_id" style="width: 100%;">
							<option value="0">-- All LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="filter[tmuk_kode]" class="" id="mom_penjualan_tmuk">
							<option value="0">-- All TMUK --</option>
						</select>
					</div>
				</div>

			</form>
			<div id="penjualan_mom" style="width: 100%; height: 400px; margin: 0 auto"></div>
		</div>
		<div class="ui bottom attached tab segment" data-tab="penjualan-qoq">
			<form action="" class="ui form">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">
									<select class="ui fluid search" name="qoq_penjualan_triwulan_start">
										@foreach (option_triwulan() as $opt)
											<option value="{{ $opt['triwulan'] }}">{{ $opt['labels'] }}</option>
										@endforeach
									</select>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">	
									<select class="ui fluid search" name="qoq_penjualan_triwulan_end">
										@foreach (option_triwulan() as $opt)
											<option value="{{ $opt['triwulan'] }}"{{ $opt['triwulan'] == date("Y")."."."4"? "selected":"" }}>{{ $opt['labels'] }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<button type="button" class="ui right floated default left labeled icon save button" onclick="penjualan_qoq();">
									<i class="search icon"></i>
									Cari
								</button>
							</div> 
						</div>
					</div>
				</div>

				<div class="three fields">
					<div class="field">
						<select name="region_id" class="" id='qoq_penjualan_region_id' style="width: 100%;">
							<option value="0">-- All Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="lsi_id" class="" id="qoq_penjualan_lsi_id" style="width: 100%;">
							<option value="0">-- All LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="filter[tmuk_kode]" class="" id="qoq_penjualan_tmuk">
							<option value="0">-- All TMUK --</option>
						</select>
					</div>
				</div>
			</form>
			<div id="penjualan_qoq" style="width: 100%; height: 400px; margin: 0 auto"></div>
		</div>
		<div class="ui bottom attached tab segment" data-tab="penjualan-yoy">
			<form action="" class="ui form">
				{{-- <div class="field">
					<div class="fields">
						<div style="margin-left: auto; margin-right: 1px;">
							<button type="button" class="small ui blue button icon" onclick="penjualan_yoy();">
								<i class="search icon"></i>
							</button>&nbsp;
							<button type="button" class="small ui green button icon">
								<i class="download icon"></i>
							</button>&nbsp;
						</div>
					</div>
				</div> --}}
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui year" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" name="yoy_penjualan_start" value="{{ date('Y', strtotime(date('Y') . ' -5 year')) }}">
										</div>
									</div>
								</div>
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui year" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Tahun" name="yoy_penjualan_end" value="{{ date('Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<button type="button" class="ui right floated default left labeled icon save button" onclick="penjualan_yoy();">
									<i class="search icon"></i>
									Cari
								</button>
							</div> 
						</div>
					</div>
				</div>
				{{-- <div class="two fields">
					<div class="field">		
						<div class="ui year" id="from">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Dari Bulan" name="yoy_penjulan_start" value="{{ date('Y', strtotime(date('Y') . ' -5 year')) }}">
							</div>
						</div>
					</div>
					<p style="text-align: center; margin: 10px; ">s.d.</p>
					<div class="field">		
						<div class="ui year" id="to">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Tahun" name="yoy_penjulan_end" value="{{ date('Y') }}">
							</div>
						</div>
					</div>
				</div> --}}

				<div class="three fields">
					<div class="field">
						<select name="yoy_penjualan_region_id" class="" id='yoy_penjualan_region_id' style="width: 100%;">
							<option value="0">-- All Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="yoy_penjualan_lsi_id" class="" id="yoy_penjualan_lsi_id" style="width: 100%;">
							<option value="0">-- All LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="yoy_penjualan_tmuk" class="" id="yoy_penjualan_tmuk">
							<option value="0">-- All TMUK --</option>
						</select>
					</div>
				</div>
			</form>
			<div id="penjualan_yoy" style="width: 100%; height: 400px; margin: 0 auto"></div>
		</div>
	</div>

	<div class="column">
		<div class="ui top attached tabular menu">
			{{-- <a class="active item" data-tab="pembelian-all">Semua Pembelian</a> --}}
			<a class="item active" data-tab="pembelian-mom" onclick="pembelian_mom();">MoM</a>
			<a class="item" data-tab="pembelian-qoq" onclick="pembelian_qoq();">QoQ</a>
			<a class="item" data-tab="pembelian-yoy" onclick="pembelian_yoy();">YoY</a>
		</div>
		<div class="ui bottom attached active tab segment" data-tab="pembelian-mom">
			<form action="" class="ui form">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui mom_statrt" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" name="mom_pembelian_start" value="{{ date('F Y', strtotime(date('M Y') . ' -5 month')) }}">
										</div>
									</div>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui mom_end" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" name="mom_pembelian_end" value="{{ date('F Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<button type="button" class="ui right floated default left labeled icon save button" onclick="pembelian_mom();">
									<i class="search icon"></i>
									Cari
								</button>
							</div> 
						</div>
					</div>
				</div>

				<div class="three fields">
					<div class="field">
						<select name="mom_pembelian_region_id" class="" id='mom_pembelian_region_id' style="width: 100%;">
							<option value="0">-- All Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="mom_pembelian_lsi_id" class="" id="mom_pembelian_lsi_id" style="width: 100%;">
							<option value="0">-- All LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="mom_pembelian_tmuk" class="" id="mom_pembelian_tmuk">
							<option value="0">-- All TMUK --</option>
						</select>
					</div>
				</div>
			</form>
			<div id="pembelian_mom" style="width: 100%; height: 400px; margin: 0 auto"></div>
		</div>
		<div class="ui bottom attached tab segment" data-tab="pembelian-qoq">
			<form action="" class="ui form">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">
									<select class="ui fluid search" name="qoq_pembelian_triwulan_start">
										@foreach (option_triwulan() as $opt)
											<option value="{{ $opt['triwulan'] }}">{{ $opt['labels'] }}</option>
										@endforeach
									</select>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">	
									<select class="ui fluid search" name="qoq_pembelian_triwulan_end">
										@foreach (option_triwulan() as $opt)
											<option value="{{ $opt['triwulan'] }}" {{ $opt['triwulan'] == date("Y")."."."4"? "selected":"" }}>{{ $opt['labels'] }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<button type="button" class="ui right floated default left labeled icon save button" onclick="pembelian_qoq();">
									<i class="search icon"></i>
									Cari
								</button>
							</div> 
						</div>
					</div>
				</div>

				<div class="three fields">
					<div class="field">
						<select name="region_id" class="" id='qoq_pembelian_triwulan_region_id' style="width: 100%;">
							<option value="0">-- All Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="lsi_id" class="" id="qoq_pembelian_triwulan_lsi_id" style="width: 100%;">
							<option value="0">-- All LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="filter[tmuk_kode]" class="" id="qoq_pembelian_triwulan_tmuk">
							<option value="0">-- All TMUK --</option>
						</select>
					</div>
				</div>
			</form>
			<div id="pembelian_qoq" style="width: 100%; height: 400px; margin: 0 auto"></div>
		</div>
		<div class="ui bottom attached tab segment" data-tab="pembelian-yoy">
			<form action="" class="ui form">
				{{-- <div class="field">
					<div class="fields">
						<div style="margin-left: auto; margin-right: 1px;">
							<button type="button" class="small ui blue button icon" onclick="pembelian_yoy();">
								<i class="search icon"></i>
							</button>&nbsp;
							<button type="button" class="small ui green button icon">
								<i class="download icon"></i>
							</button>&nbsp;
						</div>
					</div>
				</div> --}}
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui year" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" name="yoy_pembelian_start" value="{{ date('Y', strtotime(date('Y') . ' -5 year')) }}">
										</div>
									</div>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui year" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Tahun" name="yoy_pembelian_end" value="{{ date('Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<button type="button" class="ui right floated default left labeled icon save button" onclick="pembelian_yoy();">
									<i class="search icon"></i>
									Cari
								</button>
							</div> 
						</div>
					</div>
				</div>
				{{-- <div class="two fields">
					<div class="field">		
						<div class="ui year" id="from">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Dari Bulan" name="yoy_pembelian_start" value="{{ date('Y', strtotime(date('Y') . ' -5 year')) }}">
							</div>
						</div>
					</div>
					<p style="text-align: center; margin: 10px; ">s.d.</p>
					<div class="field">		
						<div class="ui year" id="to">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Tahun" name="yoy_pembelian_end" value="{{ date('Y') }}">
							</div>
						</div>
					</div>
				</div> --}}

				<div class="three fields">
					<div class="field">
						<select name="yoy_pembelian_region_id" class="" id='yoy_pembelian_region_id' style="width: 100%;">
							<option value="0">-- All Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="yoy_pembelian_lsi_id" class="" id="yoy_pembelian_lsi_id" style="width: 100%;">
							<option value="0">-- All LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="yoy_pembelian_tmuk" class="" id="yoy_pembelian_tmuk">
							<option value="0">-- All TMUK --</option>
						</select>
					</div>
				</div>
			</form>
			<div id="pembelian_yoy" style="width: 100%; height: 400px; margin: 0 auto"></div>
		</div>
	</div>
</div>
<!-- Penjulan & Pembelian -->

<!-- I/O & Service Level --> 
<div class="ui equal width grid">

	<div class="column">
		<div class="ui segment">
			<form action="" class="ui form">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui mom_statrt" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" name="io_level_start" value="{{ date('F Y', strtotime(date('M Y') . ' -5 month')) }}">
										</div>
									</div>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui mom_end" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" name="io_level_end" value="{{ date('F Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<button type="button" class="ui right floated default left labeled icon save button" onclick="io_level();">
									<i class="search icon"></i>
									Cari
								</button>
							</div> 
						</div>
					</div>
				</div>
				<div class="three fields">
					<div class="field">
						<select name="region_id" class="" id='io_level_region_id' style="width: 100%;">
							<option value="0">-- All Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="lsi_id" class="" id="io_level_lsi_id" style="width: 100%;">
							<option value="0">-- All LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="filter[tmuk_kode]" class="" id="io_level_tmuk">
							<option value="0">-- All TMUK --</option>
						</select>
					</div>
				</div>
			</form>
			<div id="io_level" style="width: 100%; height: 400px; margin: 0 auto"></div>
			
			<table>
				<tr>
					<th rowspan="2" valign="top">Keterangan : </th>
					<td>I/O Level (%)&nbsp; = &nbsp;</td>
					<td> Ʃ Pembelian TMUK Ke LSI<hr></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>  Ʃ Penjualan TMUK Ke Kustomer</td>
				</tr>
			</table>
		</div>
	</div>
	{{-- service level --}}
	<div class="column">
		<div class="ui segment">
			<form action="" class="ui form">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui mom_statrt" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" name="service_level_start" value="{{ date('F Y', strtotime(date('M Y') . ' -5 month')) }}">
										</div>
									</div>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui mom_end" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" name="service_level_end" value="{{ date('F Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<button type="button" class="ui right floated default left labeled icon save button" onclick="service_level();">
									<i class="search icon"></i>
									Cari
								</button>
							</div> 
						</div>
					</div>
				</div>

				{{-- <div class="two fields">
					<div class="field">		
						<div class="ui mom_statrt" id="from">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Dari Bulan" name="service_level_start" value="{{ date('F Y', strtotime(date('M Y') . ' -5 month')) }}">
							</div>
						</div>
					</div> 
					<p style="text-align: center; margin: 10px; ">s.d.</p>
					<div class="field">		
						<div class="ui mom_end" id="to">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Sampai Bulan" name="service_level_end" value="{{ date('F Y') }}">
							</div>
						</div>
					</div>
				</div> --}}

				<div class="three fields">
					<div class="field">
						<select name="region_id" class="" id='service_level_region_id' style="width: 100%;">
							<option value="0">-- All Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="lsi_id" class="" id="service_level_lsi_id" style="width: 100%;">
							<option value="0">-- All LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="filter[tmuk_kode]" class="" id="service_level_tmuk">
							<option value="0">-- All TMUK --</option>
						</select>
					</div>
				</div>
			</form>
			<div id="service_level" style="width: 100%; height: 400px; margin: 0 auto"></div>
			<table>
				<tr>
					<th rowspan="2" valign="top">Keterangan : </th>
					<td>Service Level (%)&nbsp; = &nbsp;</td>
					<td> Ʃ Qty PO<hr></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>  Ʃ Qty PR</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<!-- I/O & Service Level -->

<!-- Table -->
<div class="ui equal width grid">
	<!-- Deviasi -->
	
	
	<!-- Deviasi -->
</div>
<div class="ui equal width grid">
	<!-- Penjualan -->
	<div class="eight wide column">
		<!-- Statistik -->
		<div class="ui segments">
			<h5 class="ui top attached header">
				<div class="ui accordion field">
					<div class="title">
						{{-- <i class="icon dropdown"></i> --}}
						Penjualan Category 1 TMUK
					</div>
					<div class="active content field">
						<form class="ui filter form">
			  				<div class="fields">
			  					<div class="field">
									<div class="ui fluid selection dropdown">
									  <input type="hidden" name="filter[entri]">
									  <i class="dropdown icon"></i>
									  <div class="default text">10</div>
									  <div class="menu">
									    <div class="item" data-value="10">10</div>
									    <div class="item" data-value="25">25</div>
									    <div class="item" data-value="50">50</div>
									    <div class="item" data-value="100">100</div>
									  </div>
									</div>
								</div>
								<div class="six wide field" style="width: 30%;">
									<select name="filter[tmuk_kode]" id="penkat_tmuk_kode"  class="ui fluid search selection dropdown" style="width: 100%;">
									    {!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
								    </select>
								</div>
								{{-- <div class="field">
									<input name="filter[item_code]" placeholder="Kode Produk" type="text">
								</div> --}}
								<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="penjualan_kategori();">
									<i class="search icon"></i>
								</button>
								<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
									<i class="refresh icon"></i>
								</button>
								{{-- <button type="button" class="ui green button icon" style="position: relative; left: 330px;">
										<i class="download icon"></i> 
								</button> --}}
								<button type="button" class="ui right floated green button icon">
										<i class="download icon"></i> Download
								</button>
							</div>
						</form>
						{{-- <form class="ui filter form">
							<div class="ui grid">
								<div class="two column row">
									<div class="left floated eight wide column">
										<div class="ui icon input">
											<input name="filter[item_code]" placeholder="Kode Produk" type="text">
										</div>
									</div>
									<div class="fields">
										<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui teal icon filter button" data-content="Cari Data">
										<i class="search icon"></i>
										</button>
										<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
											<i class="refresh icon"></i>
										</button>
										<button type="button" class="ui green button icon">
											<i class="download icon"></i> 
										</button>&nbsp;
										</div>
									</div>
								</div>
							</div>
						</form> --}}
					</div>
				</div>
			</h5>
			<div class="ui packed yellow segment">
				<table class="ui table">
					<tr align="center">
						<td><b>Region <span id="pen_region"></span> / LSI <span id="pen_lsi"></span> / TMUK <span id="pen_tmuk"></span> </b><br>Tahun {{ \Carbon\Carbon::now()->format('Y') }} YTD
						</td>
					</tr>
				</table>

				<table class="ui very basic striped small compact table" id="dataTablePenjualanKategori">
					<thead>
						<tr>
							<th style="text-align: center;">#</th>
							{{-- <th style="text-align: center;">TMUK </th> --}}
							<th style="text-align: center;">Category 1 </th>
							<th style="text-align: center;">Penjualan (Rp.)</th>
							<th style="text-align: center;">Kontribusi dari Sales (%)</th>
							{{-- <th style="text-align: center;">Sales</th> --}}
						</tr>
					</thead>
					<tbody>
						@yield('tableBody')
					</tbody>
				</table>
			</div>
		</div>
		<!-- Statistik -->
	</div>
	<!-- Penjualan -->
	<div class="eight wide column">
		<!-- Statistik -->
		<div class="ui segments">
			<h5 class="ui top attached header">
				<div class="ui accordion field">
					<div class="title">
						{{-- <i class="icon dropdown"></i> --}}
						Deviasi Penjualan Harian TMUK 
					</div>
					<div class="active content field">
						
						<form class="ui filter form">
			  				<div class="fields">
			  					<div class="field">
									<div class="ui fluid selection dropdown">
									  <input type="hidden" name="filter[entri]">
									  <i class="dropdown icon"></i>
									  <div class="default text">10</div>
									  <div class="menu">
									    <div class="item" data-value="10">10</div>
									    <div class="item" data-value="25">25</div>
									    <div class="item" data-value="50">50</div>
									    <div class="item" data-value="100">100</div>
									  </div>
									</div>
								</div>
								<div class=" six wide field" style="width: 30%;">
									<select name="defiasi[tmuk_kode]" id="defiasi_tmuk_kode"  class="ui fluid search selection dropdown" style="width: 100%;">
									    {!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
								    </select>
								</div>
								{{-- <div class="field">
									<input name="filter[item_code]" placeholder="Kode Produk" type="text">
								</div> --}}
								{{-- <div class="six field"> --}}
								<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="defiasi_penjualan();">
									<i class="search icon"></i>
								</button>
								<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
									<i class="refresh icon"></i>
								</button>
								<button type="button" class="ui right floated green button icon">
										<i class="download icon"></i> Download
								</button>
								{{-- </div> --}}
								{{-- <div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div> --}}
							</div>
						</form>
					</div>
				</div>
			</h5>
			<div class="ui packed yellow segment">
				<table class="ui table">
					<tr align="center">
						<td>{{-- <b>TMUK $tmuk</b> --}}<br>Tahun {{ \Carbon\Carbon::now()->format('Y') }} YTD 
						</td>
					</tr>
				</table>

				<table class="ui very basic striped small compact table" id="dataTableDeviasiPenjualan">
					<thead>
						<tr>
							<th style="text-align: center;">#</th>
							<th style="text-align: center;">TMUK </th>
							<th style="text-align: center;">Penjualan (KKI)</th>
							<th style="text-align: center;">Penjualan (Aktual)</th>
							<th style="text-align: center;">Selisih (Rp.)</th>
							<th style="text-align: center;">% dari Target</th>
						</tr>
					</thead>
					<tbody>
						@yield('tableBody')
					</tbody>
				</table>
			</div>
		</div>
		<!-- Statistik -->
	</div>

	<div class="eight wide  column">
		<div class="ui segments">
			<h5 class="ui top attached header">
				Statistik
			</h5>
			<div class="ui packed yellow segment">
				{{-- <table class="ui very basic striped small compact table">
					<tbody>
						<tr>
							<td width="50px">1</td>
							<td width="500px">Rata - rata Frekuensi PO/Bulan </td>
							<td>: 5 </td>
						</tr>
						<tr>
							<td width="50px">2</td>
							<td width="500px">Rata - rata Nilai PO/Bulan </td>
							<td>: Rp 1.000.000 /Bulan</td>
						</tr>
					</tbody>
				</table> --}}

				<table class="ui very basic striped small compact table" id="dataTableStatistikPo">
					<thead>
						<tr>
							<th style="text-align: center;">#</th>
							<th style="text-align: center;">Tahun</th>
							<th style="text-align: center;">Rata - rata Frekuensi PO/Bulan</th>
							<th style="text-align: center;">Rata - rata Nilai PO/Bulan (Rp)</th>
						</tr>
					</thead>
					<tbody>
						@yield('tableBody')
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>

{{-- @section('js-filters')
	d.item_code = $("input[name='filter[item_code]']").val();
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_penjualan_kategori = function(){
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-dashboard-penjualan-kategori') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var item_code = document.createElement("input");
		            item_code.setAttribute("type", "hidden");
		            item_code.setAttribute("name", 'item_code');
		            item_code.setAttribute("value", $('[name="filter[item_code]"]').val());
		        form.appendChild(item_code);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append --}}