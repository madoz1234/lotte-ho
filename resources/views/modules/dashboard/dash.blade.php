@extends('layouts.grid')
@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
<script src="{{ asset('plugins/highchart/highcharts.js') }}"></script>
@append

@section('scripts')
<script type="text/javascript">
	$('.menu .item')
	  .tab()
	;
</script>
@append

@section('breadcrumb-content')

<div style="text-align: right; margin-top: -15px;">
	{{ \Carbon\Carbon::now()->format('l, d F Y H:i') }}  
	{{-- <h5 class="item" data-content="Tanggal Hari ini">{{ indonesian_date(\Carbon\Carbon::now()) }}  </h5> --}}
</div>
@append

@section('content-body')

<div class="ui grid">
	<div class="eight wide column">
		<div class="ui top attached tabular menu">
		  {{-- <a class="item" data-tab="first"><i class="envelope icon"></i> Message</a> --}}
		  <a class="active item" data-tab="second"><i class="user icon"></i> Data Pribadi Anda</a>
		</div>
		<div class="ui bottom attached tab segment" data-tab="first">
			<br>
		  		<center><i>Selamat Datang <b>{{ auth()->user()->name }}</b></i></center>
			<br>
			<br>
		</div>
		<div class="ui bottom attached active tab segment" data-tab="second">
			<div class="ui attached segment">			
				<div class="ui defalut icon message">
				  <i class="user icon"></i>
				  <div class="content">
				    <p>Terlampir data pribadi anda</p>
				  </div>
				</div>	
				<div class="ui two column grid">
						<div class="five wide column"><b>Nama Lengkap</b></div>
						<div class="column">: {{ auth()->user()->pengguna->nama_lengkap }}</div>
						<div class="five wide column"><b>Tlp</b></div>
						<div class="column">: {{ auth()->user()->pengguna->telepon }}</div>
						<div class="five wide column"><b>Email</b></div>
						<div class="column">: {{ auth()->user()->pengguna->email }}</div>
						<div class="five wide column"><b>Role</b></div>
						<div class="column">: 
							@foreach (auth()->user()->roles as $elm)
								<a class="ui teal tag label">{{ $elm->display_name }}</a>
							@endforeach
						</div>
				</div>
					<table class="ui single line table" style="width: 100%">
						<thead>
							<tr>
								<th colspan="2"><center>DATA LSI</center></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width: 31%;"><b>Kode</b></td>
								<td>: {{ auth()->user()->pengguna->lsi->kode }}</td>
							</tr>
							<tr>
								<td style="width: 31%;"><b>Nama</b></td>
								<td>: {{ auth()->user()->pengguna->lsi->nama }}</td>
							</tr>
							<tr>
								<td style="width: 31%;"><b>Region</b></td>
								<td>: {{ auth()->user()->pengguna->lsi->region->area }}</td>
							</tr>
							<tr>
								<td style="width: 31%;"><b>Kota</b></td>
								<td>: {{ auth()->user()->pengguna->lsi->kota->nama }}</td>
							</tr>
							<tr>
								<td style="width: 31%;"><b>Alamat</b></td>
								<td>: {{ auth()->user()->pengguna->lsi->alamat }}</td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="eight wide column">
		<div class="ui defalut icon message">
		  <i class="blue user icon"></i>
		  <div class="content">
		    <center><i>Selamat Datang <b>{{ auth()->user()->name }}</b></i></center>
		  </div>
		</div>	
		<div class="ui blue icon message">
		  <i class="info icon"></i>
		  <div class="content">
		    <div class="header">
		      Anda tidak menemukan monitoring dasboard?
		    </div>
		    <p>Ma'af anda tidak mempunyai kewenangan untuk mengakses dashboard monitoring ... </p>
		  </div>
		</div>	
		<br>
		<br>
		<center><i class="massive users icon"></i></center>
	</div>
</div>
@endsection


@section('scripts')
@include('modules.dashboard.scripts.finance')
@include('modules.dashboard.scripts.penjualan')
@include('modules.dashboard.scripts.pembelian')
@include('modules.dashboard.penjualan.js')
@include('modules.dashboard.outstanding.js')
@include('modules.dashboard.monitoring.js')
@include('modules.dashboard.peta.js')
@include('modules.dashboard.finance.js')
@include('modules.dashboard.chat.js')
<script type="text/javascript">
	$('#tab-dashboard .menu .item')
	.tab();
</script>
@append