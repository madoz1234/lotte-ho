@foreach($records as $chat)
@if ((auth()->user()->name == $chat->nama))
<div class="comment">
	<div class="ui filter form">
		<div class="fields">
			<div style="margin-left: auto; margin-right: 1px;">
				<div class="ui olive label content">
					<div class="content">
						<a class="author">{{ $chat->nama }}</a>
						<div class="metadata">
							<span class="date">{{ date_format($chat->created_at,'l M Y G:ia') }}</span>
						</div>
						{{-- <div class="wrap" style="width: 500px">
							<p>{{ $chat->pesan }}</p>
						</div> --}}
						<div class="text">
							<p>{{ $chat->pesan }}</p>
						</div>
					</div>
				</div>
			</div>&nbsp;
			<a class="avatar">
				<img src="{{ asset('img/profile.png')}}" style="width: 50px">
			</a>
		</div>
	</div>
</div>
	
@else
<div class="comment">
	<a class="avatar">
		<img src="{{ asset('img/profile.png')}}" style="width: 50px">
	</a>
	<div class="ui tear label content">
		<a class="author">{{ $chat->nama }}</a>
		<div class="metadata">
			<span>{{ date_format($chat->created_at,'l M Y G:ia') }}</span>
		</div>
		<div class="text">
			<p>{{ $chat->pesan }}</p>
		</div>
		<div class="actions">{{ (auth()->user()->name == $chat->nama) ? 'Delete' : '' }}
			@if((auth()->user()->name == $chat->nama))
			<div class="text">
				<a class="deletebtn" data-pesan="{{ $chat->id }}"> Delete</a>
			</div>
			@else
			@endif
		</div>
	</div>
</div>
@endif
  @endforeach