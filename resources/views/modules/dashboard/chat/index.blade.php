@section('css')
    <style type="text/css">
        .wrap {
            word-wrap: break-word;
        }
    </style>
@append

<!-- Chat -->
<h3 class="ui dividing header" id="kontak">Obrolan</h3>
	<div id="comments-container" style="width:100%;height:500px;overflow-y: scroll;">
		@include('modules.dashboard.chat.data')
	</div>

<form class="ui reply form" id="my-form" method="POST">
	{!! csrf_field() !!}
	<div class="field">
		<input name="nama" type="hidden" value="{{ auth()->user()->name }}">
		<textarea name="pesan" rows="2" placeholder="Tulis Komentar..."></textarea>
	</div>
	{{-- <div class="ui blue labeled submit icon button">
		<i class="icon edit"></i> Komentar
	</div> --}}
	<button class="ui blue labeled submit icon button">
      <i class="icon edit"></i> Kirim
    </button>
</form>
	<!-- Chat -->
