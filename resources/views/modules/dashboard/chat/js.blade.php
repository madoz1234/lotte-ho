<script type="text/javascript" charset="utf-8" async defer>
		$(function() {
			var d = $('#comments-container');
			d.scrollTop(d.prop("scrollHeight"));
			$('#kontak').scrollTop($('#kontak')[0].scrollHeight);

			@if ($errors->any())
			$('html, body').animate({
				scrollTop: $("#contactForm").offset().top
			}, 2000);
			@endif
		});

//obrolan
var page = 1;
var element = $("#comments-container");
element.scroll(function() {
	if(element.scrollTop() <= 0) {
		page++;
		loadMoreData(page);
	}
	
});

function loadMoreData(page){
	$.ajax(
	{
		url: "{{ url('dashboard/obrolan/data?page=') }}"+page,
		type: "get",
		beforeSend: function()
		{
			$('.ajax-load').show();
		}
	})
	.done(function(data)
	{
// console.log(data);
if(data.html == " "){
	$('.ajax-load').html("No more records found");
	return;
}
$('.ajax-load').hide();
$("#comments-container").prepend(data);
})
	.fail(function(jqXHR, ajaxOptions, thrownError)
	{
		alert('server not responding...');
	});
}

//chat data save
(function($){
	function processForm( e ){
		$.ajax({
			url: '{{ url('dashboard/obrolan/save') }}',
			dataType: 'text',
			type: 'post',
			contentType: 'application/x-www-form-urlencoded',
			data: $(this).serialize(),
			success: function( data, textStatus, jQxhr ){
				var d = $('#comments-container');
				d.html('');
				page = 1;
				loadMoreData(page);
				d.scrollTop(d.prop("scrollHeight"));
			},
			error: function( jqXhr, textStatus, errorThrown ){
				console.log( errorThrown );
			}
		});

		e.preventDefault();
	}

	$('#my-form').submit( processForm );
})(jQuery);

$(document).on('click', '.delete-button', function () {
	var target = $(this).data('url');
	swal({
		title: 'Apakah anda yakin menghapus data?',
		text: "Data yang terhapus tidak bisa dikembalikan",
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Hapus',
		cancelButtonText: 'Batal'
	}).then(function () {
// get data URL
$.ajax({
	type: 'GET',
	url: target,
	data: {},
	dataType: 'html',
	success: function(data) { 
		swal(
			'Sukses',
			'Data telah terhapus.',
			'success').then(function(){ location.reload(); })
	},
	error: function() { 
		swal(
			'Gagal Terhapus',
			'Terjadi kesalahan dalam proses',
			'error')
	}
});
})
});

$("p").click(function(){
	alert("The paragraph was clicked.");
});
$(".deletebtn").click(function(ev){
	let pesan = $(this).attr("data-pesan");

	$.ajax({
		type: 'GET',
		url: '{{ url('dashboard/obrolan/destroy') }}',
// dataType: 'json',
headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
data: {id:$(this).attr("data-pesan"),"_token": "{{ csrf_token() }}"},

success: function (data) {
// alert('success').then(function(){ location.reload(); };  
location.reload();          

},
error: function (data) {
	location.reload();
// alert('error').then(function(){ location.reload(); };
}
});
});
</script>