<script type="text/javascript">
	$(document).ready(function() {
		$.ajax({
			url: '{{ url('api/tmuk/online') }}',
			type: 'GET',
			success: function(resp){
				console.log(resp.online);
				document.getElementById('m-online').innerHTML = resp.online;
				document.getElementById('m-offline').innerHTML = resp.offline;
			},
			error : function(resp){
				console.log('gagal boy');
			}
		});

		var dtmo = '';
		$('button[data-content]').popup({
			hoverable: true,
			position : 'top center',
			delay: {
				show: 300,
				hide: 800
			}
		});

		dtmo = $('#dataTableMonitoringOnline').DataTable({
			dom: 'rt<"bottom"ip><"clear">',
			responsive: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			lengthChange: true,
			pageLength: 10,
			filter: false,
			sorting: [],
			info:false,
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			ajax:  {
				url: "{{ url($pageUrl) }}/monitoring-online/grid",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
				}
			}, 
			columns: {!! json_encode($tableStructMonitoringOnline) !!},
			drawCallback: function() {
				var api = this.api();

				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML)+i+1;
				} );

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});
			}
		});

		setInterval(function(){
			$.ajax({
				url: '{{ url('api/tmuk/online') }}',
				type: 'GET',
				success: function(resp){
					console.log(resp.online);
					document.getElementById('m-online').innerHTML = resp.online;
					document.getElementById('m-offline').innerHTML = resp.offline;
				},
				error : function(resp){
					console.log('gagal boy');
				}
			});
			dtmo.draw();
		}, 60000);
	});
</script>
