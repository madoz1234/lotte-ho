<div class="ui equal width centered grid">
	<div class="twelve wide column">
		<div class="ui segments">
			<h5 class="ui top attached header">
				Monitoring Online/Offline
			</h5>
			<div class="ui packed yellow segment">
				<table class="ui very basic striped small compact table" id="dataTableMonitoringOnline">
					<thead>
						<tr>
							<th style="text-align: center;">#</th>
							<th style="text-align: left;">Nama TMUK</th>
							<th style="text-align: center;">Terakhir Aktif</th>
							<th style="text-align: center;">Status</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						@yield('tableBody')
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="four wide column">
		<div class="ui segments">
			<h5 class="ui top attached header">
				Total Online/Offline
			</h5>
			<div class="ui packed yellow segment">
				<table class="ui very basic striped small compact table">
					<tbody>
						<tr>
							<td style="width: 150px;">Jumlah TMUK Online </td>
							<th id="m-online">:&nbsp;5</th>
							<th><i class="signal icon"></i></th>
						</tr>
						<tr>
							<td style="width: 150px;">Jumlah TMUK Offline </td>
							<th id="m-offline">:&nbsp;15</th>
							<th><div class="autumn"><i class="signal icon"></i></div></th>
						</tr>
						<tr>
							<td style="width: 150px; text-align: center;">Total TMUK </td>
							<th>:&nbsp;{{ $jum_tmuk or '0' }}</th>
							<th>&nbsp;</th>
						</tr>
					</tbody>
				</table>
				<br>
				<br>
			</div>
		</div>
	</div>
</div>