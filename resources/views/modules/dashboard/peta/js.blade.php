  <?php
    $rombong_array = [];
    if(isset($record)){
        foreach($record as $key => $row){
            $tmp = [
              'romb_'.$key => [
                  'lat' => $row->latitude,
                  'lng' => $row->longitude,
                  'nama' => $row->nama,
              ]
            ];

            $rombong_array[] = $tmp;
        }   
    }
    


    $test_array = [];
    if(isset($lsi)){
      foreach($lsi as $key => $row){
          $tmp_lsi = [
            'koor_'.$key => [
                'lat' => $row->latitude,
                'lng' => $row->longitude,
                'nama' => $row->nama,
            ]
          ];

          $test_array[] = $tmp_lsi;
      }
    }

    $rombong_mini_array = [];
    if(isset($rombong)){
      foreach($rombong as $key => $row){
          $tmp_rombong = [
            'koor_'.$key => [
                'lat' => $row->latitude,
                'lng' => $row->longitude,
                'nama' => $row->membercard->nama,
            ]
          ];

          $rombong_mini_array[] = $tmp_rombong;
      }
    }
  ?>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFlm1JrVjXexh-QSlQJYVcP_4dKSNVCJU&callback=initMap&libraries=geometry">
// src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFlm1JrVjXexh-QSlQJYVcP_4dKSNVCJU&callback=initMap&libraries=geometry&sensor=true">
</script>
{{-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"> </script> --}}
<script>
      function initMap() {
        var new_marker;
        var new_marker_rombong;
        var new_marker_rombong_mini;
        var markers = [];
        var markers_rombong = [];
        var markers_rombong_mini = [];
        var rombong_array = {!! json_encode($rombong_array) !!};
        var test_array = {!! json_encode($test_array) !!};
        var rombong_mini_array = {!! json_encode($rombong_mini_array) !!};


        // var rombong_array = [
        //     {
        //       romb_1 : {
        //         lat : -6.310604,
        //         lng : 106.864843  
        //       }
        //     },
        //     {
        //       romb_2 : {
        //         lat : -6.311841,
        //         lng : 106.864307
        //       }
        //     },
        //     {
        //       romb_3 : {
        //         lat : -6.302016,
        //         lng : 106.868204
        //       }
        //     },
        //     {
        //       romb_4 : {
        //         lat : -6.312344,
        //         lng : 106.867828
        //       }
        //     },
        //     {
        //       romb_5 : {
        //         lat : -6.311907,
        //         lng : 106.869802
        //       }
        //     }
        // ];

        // var test_array = [
        //     {
        //       koor_1 : {
        //         lat : -6.308374,
        //         lng : 106.867283  
        //       }
        //     },
        //     {
        //       koor_2 : {
        //         lat : -6.310022,
        //         lng : 106.864015
        //       }
        //     },
        //     {
        //       koor_3 : {
        //         lat : -6.303605,
        //         lng : 106.866970
        //       }
        //     },
        //     {
        //       koor_4 : {
        //         lat : -6.313549,
        //         lng : 106.869019
        //       }
        //     }
        // ];

        var center_posision = new google.maps.LatLng(-6.309626, 106.867640);
        
        var icon_center ={
          url: '{{ asset('img/icon/lotte-grosir.png') }}',
          scaledSize: new google.maps.Size(30, 30),
        };

        var icon = {
          url : '{{ asset('img/icon/tmuk.png') }}',
          scaledSize: new google.maps.Size(30, 30), // scaled size
        };

        var iconrombong = {
          url : '{{ asset('img/icon/rombong-mini.jpg') }}',
          scaledSize: new google.maps.Size(30, 30), // scaled size
        };
        
        // Create the map.
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: {lat: -6.309626, lng: 106.867640},
          // mapTypeId: 'Map'
        });

        // MARKER LSI
        var post;
        $.each(test_array, function(d,i){
            $.each(i, function(k,e){
                var post = new google.maps.LatLng(e.lat, e.lng);
                var new_marker = new google.maps.Marker({
                    position: post,
                    map: map,
                    icon:icon_center,
                    title: e.nama,
                });
                markers.push(new_marker);
                
                var circle = new google.maps.Circle({
                  map: map,
                  center:{lat:e.lat, lng:e.lng},
                  radius: 10000,    // 10 miles in metres
                  fillColor: "#4e4efc",//'#fc4e59',
                  strokeColor: "#4e4efc",
                  strokeOpacity: 0.8,
                  strokeWeight: 0,
                  fillOpacity: 0.4,
                });
                circle.bindTo('center', new_marker, 'position');

                //Create Marker Inside Circle
                var post_2;
                $.each(rombong_array, function(d,i){
                    $.each(i, function(k,e){
                        var post_2 = new google.maps.LatLng(e.lat, e.lng);
                        var distance_from_location_z = google.maps.geometry.spherical.computeDistanceBetween(post, post_2); //Lsi 
                        // if (distance_from_location_z <= 1000) {
                          var new_marker_rombong = new google.maps.Marker({
                              position: post_2,
                              map: map,
                              icon:icon,
                              title: e.nama,
                          });
                          markers_rombong.push(new_marker_rombong);

                          // Rombong
                          var circles = new google.maps.Circle({
                            map: map,
                            center:{lat:e.lat, lng:e.lng},
                            radius: 3000,    // 10 miles in metres
                            
                            fillColor: "#fc4e59", //'#4e4efc',
                          strokeColor: "#fc4e59",
                            strokeOpacity: 0.8,
                            strokeWeight: 0,
                            fillOpacity: 0.02,
                          });
                          circles.bindTo('center', new_marker_rombong, 'position');

                          //Create Marker Inside Circle
                          var post_3;
                          $.each(rombong_mini_array, function(d,i){
                              $.each(i, function(k,e){
                                  var post_3 = new google.maps.LatLng(e.lat, e.lng);
                                  var distance_from_location_y = google.maps.geometry.spherical.computeDistanceBetween(post_2, post_3); //rombong 
                                  // if (distance_from_location_y <= 250) {
                                    var new_marker_rombong_mini = new google.maps.Marker({
                                        position: post_3,
                                        map: map,
                                        icon:iconrombong,
                                        title: e.nama,
                                    });
                                    markers_rombong_mini.push(new_marker_rombong_mini);
                                  // }
                              });
                          });
                        // }
                    });
                });
            });
        });

        // // Create marker 
        // var marker = new google.maps.Marker({
        //   map: map,
        //   position: center_posision,
        //   icon: icon_center,
        //   title: 'LSI'
        // });

        // // Add circle overlay and bind to marker
        // var circle = new google.maps.Circle({
        //   map: map,
        //   radius: 1000,    // 10 miles in metres
        //   fillColor: '#0000FF',
        //   strokeColor: "#0000FF",
        //   strokeOpacity: 0.8,
        //   strokeWeight: 2,
        //   fillOpacity: 0.4,
        // });
        // circle.bindTo('center', marker, 'position');

        // //Create Marker Inside Circle
        // var post;
        // $.each(test_array, function(d,i){
        //     $.each(i, function(k,e){
        //         var post = new google.maps.LatLng(e.lat, e.lng);
        //         var distance_from_location = google.maps.geometry.spherical.computeDistanceBetween(center_posision, post); //Lsi 
        //         if (distance_from_location <= 1000) {
        //           var new_marker = new google.maps.Marker({
        //               position: post,
        //               map: map,
        //               icon:icon,
        //               title: 'TMUK'
        //           });
        //           markers.push(new_marker);
        //         }

        //         var circle = new google.maps.Circle({
        //           map: map,
        //           center:{lat:e.lat, lng:e.lng},
        //           radius: 250,    // 10 miles in metres
        //           fillColor: 'red',
        //           strokeColor: "red",
        //           strokeOpacity: 0.8,
        //           strokeWeight: 2,
        //           fillOpacity: 0.4,
        //         });

        //         //Create Marker Inside Circle
        //         var post_2;
        //         $.each(rombong_array, function(d,i){
        //             $.each(i, function(k,e){
        //                 var post_2 = new google.maps.LatLng(e.lat, e.lng);
        //                 var distance_from_location_z = google.maps.geometry.spherical.computeDistanceBetween(post, post_2); //Lsi 
        //                 if (distance_from_location_z <= 250) {
        //                   var new_marker_rombong = new google.maps.Marker({
        //                       position: post_2,
        //                       map: map,
        //                       icon:iconrombong,
        //                       title: 'Rombong'
        //                   });
        //                   markers_rombong.push(new_marker_rombong);
        //                 }
        //             });
        //         });
        //     });
        // });          
      };
    </script>