<script type="text/javascript">
	$(document).ready(function() {
		// $('.ui.accordion').accordion();
		// $('.tabular.menu .item').tab({history:false});
		$('.autumn').transition('set looping').transition('flash', '2000ms');
	});
	$('#tab-dashboard .menu .item')
	.tab();
	$(document).ready(function() {
		ajax_options = function(model='', target='', input='', id=''){
			$.ajax({
				url: "{{ url('ajax/option') }}/"+model,
				type: 'GET',
				data:{
					id_region : id,
					id_lsi : id,
				},
			})
			.success(function(response) {
				$(".loading").removeClass('active');
				$('#'+target).html(response);
				// alert(target);
			})
			.fail(function() {
				console.log("error");
				$(".loading").removeClass('active');
			});
		}

		//PENJUALAN
		//MoM
		ajax_options('reg','mom_penjualan_region_id');		
		ajax_options('reg','qoq_penjualan_region_id');		
		// $('#mom_penjualan_region_id').on('click', function(){
		// 	alert('tara');
		// });

		$('#mom_penjualan_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','mom_penjualan_lsi_id', 'id_lsi', id);
		});

		$('#mom_penjualan_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','mom_penjualan_tmuk', 'id_lsi', id);
		});

		//QOQ
		ajax_options('reg','qoq_penjualan_region_id');		
		$('#qoq_penjualan_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','qoq_penjualan_lsi_id', 'id_lsi', id);
		});

		$('#qoq_penjualan_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','qoq_penjualan_tmuk', 'id_lsi', id);
		});

		//QOQ
		ajax_options('reg','qoq_finance_region_id');		
		$('#qoq_finance_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','qoq_finance_lsi_id', 'id_lsi', id);
		});

		$('#qoq_finance_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','qoq_finance_tmuk', 'id_lsi', id);
		});

		//YoY
		ajax_options('reg','yoy_penjualan_region_id');

		$('#yoy_penjualan_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','yoy_penjualan_lsi_id', 'id_lsi', id);
		});

		$('#yoy_penjualan_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','yoy_penjualan_tmuk', 'id_lsi', id);
		});

		//PEMBELIAN
		ajax_options('reg','mom_pembelian_region_id');		
		// $('#mom_pembelian_region_id').on('click', function(){
		// 	alert('tara');
		// });

		$('#mom_pembelian_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','mom_pembelian_lsi_id', 'id_lsi', id);
		});

		$('#mom_pembelian_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','mom_pembelian_tmuk', 'id_lsi', id);
		});
		//QOQ
		ajax_options('reg','qoq_pembelian_triwulan_region_id');		
		$('#qoq_pembelian_triwulan_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','qoq_pembelian_triwulan_lsi_id', 'id_lsi', id);
		});

		$('#qoq_pembelian_triwulan_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','qoq_pembelian_triwulan_tmuk', 'id_lsi', id);
		});

		//YoY
		ajax_options('reg','yoy_pembelian_region_id');

		$('#yoy_pembelian_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','yoy_pembelian_lsi_id', 'id_lsi', id);
		});

		$('#yoy_pembelian_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','yoy_pembelian_tmuk', 'id_lsi', id);
		});

		//Service Level
		ajax_options('reg','service_level_region_id');

		$('#service_level_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','service_level_lsi_id', 'id_lsi', id);
		});

		$('#service_level_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','service_level_tmuk', 'id_lsi', id);
		});

		//Service Level
		ajax_options('reg','io_level_region_id');

		$('#io_level_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','io_level_lsi_id', 'id_lsi', id);
		});

		$('#io_level_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','io_level_tmuk', 'id_lsi', id);
		});

		//finance : penjualan vs setoran dod
		ajax_options('reg','escrow_dod_region_id');

		$('#escrow_dod_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','escrow_dod_lsi_id', 'id_lsi', id);
		});

		$('#escrow_dod_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','escrow_dod_tmuk', 'id_lsi', id);
		});

		//finance : penjualan vs setoran mom
		ajax_options('reg','escrow_mom_region_id');

		$('#escrow_mom_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','escrow_mom_lsi_id', 'id_lsi', id);
		});

		$('#escrow_mom_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','escrow_mom_tmuk', 'id_lsi', id);
		});

		//finance : penjualan n laba
		ajax_options('reg','reduce_mom_region_id');

		$('#reduce_mom_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','reduce_mom_lsi_id', 'id_lsi', id);
		});

		$('#reduce_mom_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','reduce_mom_tmuk', 'id_lsi', id);
		});

		//finance : penjualan n laba
		ajax_options('reg','reduce_yoy_region_id');

		$('#reduce_yoy_region_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('region','reduce_yoy_lsi_id', 'id_lsi', id);
		});

		$('#reduce_yoy_lsi_id').on('change',function(e){
			var id =$(this).val();
			ajax_options('lsi-tmuk-code','reduce_yoy_tmuk', 'id_lsi', id);
		});
		
	});
</script>