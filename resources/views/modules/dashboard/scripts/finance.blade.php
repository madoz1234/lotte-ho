{{-- <script type="text/javascript">
	 	laba_mom = function(){
 		$.ajax({
 			url: '{{ url('finance/chartmom') }}',
 			type: 'POST',
 			dataType: 'json',
 			data: {
 				_token:'{!! csrf_token() !!}',
 			},
 		})
 		.done(function(response) {
 			$('#laba_mom').highcharts(response);

 		})
 		.fail(function() {
 			console.log("error");
 		})
 		.always(function() {
 			console.log("complete");
 		});
 		
 	};
	laba_qoq = function(){
 		$.ajax({
 			url: '{{ url('finance/chartqoq') }}',
 			type: 'POST',
 			dataType: 'json',
 			data: {
 				_token:'{!! csrf_token() !!}',
 			},
 		})
 		.done(function(response) {
 			$('#laba_qoq').highcharts(response);

 		})
 		.fail(function() {
 			console.log("error");
 		})
 		.always(function() {
 			console.log("complete");
 		});
 		
 	};
	laba_yoy = function(){
 		$.ajax({
 			url: '{{ url('finance/chartyoy') }}',
 			type: 'POST',
 			dataType: 'json',
 			data: {
 				_token:'{!! csrf_token() !!}',
 			},
 		})
 		.done(function(response) {
 			$('#laba_yoy').highcharts(response);

 		})
 		.fail(function() {
 			console.log("error");
 		})
 		.always(function() {
 			console.log("complete");
 		});
 	};
 	//ESCROW
 	escrow_dod = function (){
  		$.ajax({
 			url: '{{ url('finance/chartreducedod') }}',
 			type: 'POST',
 			dataType: 'json',
 			data: {
 				_token:'{!! csrf_token() !!}',
 			},
 		})
 		.done(function(response) {
 			$('#escrow_dod').highcharts(response);

 		})
 		.fail(function() {
 			console.log("error");
 		})
 		.always(function() {
 			console.log("complete");
 		});
 	}

 	escrow_mom = function (){
  		$.ajax({
 			url: '{{ url('finance/chartreducemom') }}',
 			type: 'POST',
 			dataType: 'json',
 			data: {
 				_token:'{!! csrf_token() !!}',
 			},
 		})
 		.done(function(response) {
 			$('#escrow_mom').highcharts(response);

 		})
 		.fail(function() {
 			console.log("error");
 		})
 		.always(function() {
 			console.log("complete");
 		});
 	}

    laba_mom();
    escrow_dod();

	$(document).ready(function() {

		$('.tabular.menu .item').tab({history:false});
		$('.ui.accordion').accordion();

		$('.ui.calendar').calendar({type: 'date'});

		$('.ui.month').calendar({type: 'month'});

		$('.ui.year').calendar({type: 'year'});

		$('#home-menu .item').tab({history:false});

		$('#home-menu .item').click(function() {
	        $('#home-menu .item').removeClass('active');
	        $(this).addClass('active');
		 });

		$('.ui.dropdown').dropdown({
            onChange: function(value) {
                var target = $(this).dropdown();
                if (value!="") {
                    target
                        .find('.dropdown.icon')
                        .removeClass('dropdown')
                        .addClass('delete')
                        .on('click', function() {
                            target.dropdown('clear');
                            $(this).removeClass('delete').addClass('dropdown');
                            return false;
                        });
                }
            }
        });

	$('.ui.dropdown')
            .closest('.ui.selection')
            .find('.item.active').addClass('qwerty').end()
            .dropdown('clear')
                .find('.qwerty').removeClass('qwerty')
            .trigger('click');


		//Posisi Escrow Balance
		// Highcharts.chart('escrow_dod', {
		// 		chart: {
		// 			type: 'column'
		// 		},
		// 		title: {
		// 			text: 'Penjualan vs Setoran Penjualan TMUK' + '<br>' + 'TMUK Jernih'
		// 		},
		// 		subtitle: {
		// 			text: 'Periode 01/01/18 - 28/01/18'
		// 		},
		// 		xAxis: {
		// 			categories: [
		// 			'Jan-18',
		// 			'Feb-18',
		// 			'Mar-18',
		// 			'Apr-18',
		// 			'Mei-18',
		// 			'Jun-18',
		// 			'Jul-18'


		// 			],
		// 			crosshair: true
		// 		},
		// 		yAxis: {
		// 			min: 0,
		// 			title: {
		// 				text: '(Rp)'
		// 			}
		// 		},
		// 		tooltip: {
		// 			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		// 			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		// 			'<td style="padding:0">Rp.<b>{point.y:.1f} </b></td></tr>',
		// 			footerFormat: '</table>',
		// 			shared: true,
		// 			useHTML: true
		// 		},
		// 		plotOptions: {
		// 			column: {
		// 				pointPadding: 0.2,
		// 				borderWidth: 0
		// 			}
		// 		},
		// 		credits: {
		// 			enabled:false
		// 		},
		// 		series: [{
		// 			name: 'Topup',
		// 			data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6]

		// 		}, {
		// 			name: 'Setoran',
		// 			data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4]

		// 		}]
		// 	});

		// Highcharts.chart('escrow_mom', {
		// 		chart: {
		// 			type: 'column'
		// 		},
		// 		title: {
		// 			text: 'Penjualan vs Setoran Penjualan TMUK' + '<br>' + 'TMUK Jernih'
		// 		},
		// 		subtitle: {
		// 			text: 'Periode 01/01/18 - 28/01/18'
		// 		},
		// 		xAxis: {
		// 			categories: [
		// 			'Jan-18',
		// 			'Feb-18',
		// 			'Mar-18',
		// 			'Apr-18',
		// 			'Mei-18',
		// 			'Jun-18',
		// 			'Jul-18'


		// 			],
		// 			crosshair: true
		// 		},
		// 		yAxis: {
		// 			min: 0,
		// 			title: {
		// 				text: '(Rp)'
		// 			}
		// 		},
		// 		tooltip: {
		// 			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		// 			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		// 			'<td style="padding:0">Rp.<b>{point.y:.1f} </b></td></tr>',
		// 			footerFormat: '</table>',
		// 			shared: true,
		// 			useHTML: true
		// 		},
		// 		plotOptions: {
		// 			column: {
		// 				pointPadding: 0.2,
		// 				borderWidth: 0
		// 			}
		// 		},
		// 		credits: {
		// 			enabled:false
		// 		},
		// 		series: [{
		// 			name: 'Topup',
		// 			data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4]

		// 		}, {
		// 			name: 'Setoran',
		// 			data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6]

		// 		}]
		// 	});

		});
</script>
 --}}