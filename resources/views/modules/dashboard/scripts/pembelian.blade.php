<script type="text/javascript">
	jQuery(document).ready(function($) {
		//Pembelian Charts
		// Highcharts.chart('pembelian_all', {
		// 	credits: false,
		// 	chart: {
		// 		type: 'column'
		// 	},
		// 	title: {
		// 	text: 'Kinerja Pembelian' + '<br>' + 'Region Timur / Lsi Lotte Pasar Rebo / TMUK Jernih'
		// 	},
		// 	subtitle: {
		// 		text: 'Periode 02/18 - 03/18'
		// 	},
		// 	xAxis: {
		// 		type: 'category',
		// 		labels: {
		// 			rotation: -45,
		// 			style: {
		// 				fontSize: '13px',
		// 				fontFamily: 'Verdana, sans-serif'
		// 			}
		// 		}
		// 	},
		// 	yAxis: {
		// 		min: 0,
		// 		title: {
		// 			text: 'Pembelian'
		// 		}
		// 	},
		// 	legend: {
		// 		enabled: false
		// 	},
		// 	series: [{
		// 		name: 'Pembelian',
		// 		data: [
		// 		['Jan 18', 23.7],
		// 		['Feb 18', 16.1],
		// 		['Mar 18', 14.2],
		// 		['Apr 18', 44.0],
		// 		['Mei 18', 80.0],
		// 		['Jun 18', 19.0],
		// 		['Jul 18', 23.0],
		// 		['Agu 18', 15.0],
		// 		['Sep 18', 74.0],
		// 		['Okt 18', 33.0],
		// 		['Nov 18', 10.0],
		// 		['Des 18', 55.0]
		// 		],
		// 	}]
		// });

		Highcharts.chart('pembelian_mom', {
			credits: false,
			chart: {
				type: 'column'
			},
			title: {
			text: 'Kinerja Pembelian TMUK' + '<br>' + 'Region Timur / LSI Lotte Pasar Rebo / TMUK Jernih'
			},
			subtitle: {
				text: 'Periode 02/18 - 03/18'
			},
			xAxis: {
				type: 'category',
				labels: {
					rotation: -0,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Pembelian (Rp)'
				}
			},
			legend: {
				enabled: false
			},
			series: [{
				name: 'Pembelian',
				data: [
				['Jan-18', 23.7],
				['Feb-18', 16.1],
				['Mar-18', 14.2],
				['Apr-18', 14.0]
				],
			}]
		});

		Highcharts.chart('pembelian_qoq', {
			credits: false,
			chart: {
				type: 'column'
			},
			title: {
			text: 'Kinerja Pembelian TMUK' + '<br>' + 'Region Timur / LSI Lotte Pasar Rebo / TMUK Jernih'
			},
			subtitle: {
				text: 'Periode 02/18 - 03/18'
			},
			xAxis: {
				type: 'category',
				labels: {
					rotation: -0,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Pembelian (Rp)'
				}
			},
			legend: {
				enabled: false
			},
			series: [{
				name: 'Pembelian',
				data: [
				['I/17', 23.7],
				['II/17', 16.1],
				['III/17', 14.2],
				['IV/17', 14.0],
				['I/18', 14.0]
				],
			}]
		});

		Highcharts.chart('pembelian_ytd', {
			credits: false,
			chart: {
				type: 'column'
			},
			title: {
			text: 'Kinerja Pembelian TMUK' + '<br>' + 'Region Timur / LSI Lotte Pasar Rebo / TMUK Jernih'
			},
			subtitle: {
				text: 'Periode 02/18 - 03/18'
			},
			xAxis: {
				type: 'category',
				labels: {
					rotation: -0,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Pembelian (Rp)'
				}
			},
			legend: {
				enabled: false
			},
			series: [{
				name: 'Pembelian',
				data: [
				['2015', 23.7],
				['2016', 16.1],
				['2017', 14.2],
				['2018', 5.0]
				],
			}]
		});
		//Pembelian Charts
	});
</script>