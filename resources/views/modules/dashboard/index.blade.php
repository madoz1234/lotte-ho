@extends('layouts.grid')
@section('css')
{{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
{{-- <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script> --}}
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
<script src="{{ asset('plugins/highchart/highcharts.js') }}"></script>
<script src="{{ asset('plugins/highchart/modules/exporting.js') }}"></script>
@append



@section('breadcrumb-content')

<div style="text-align: right; margin-top: -15px;">
	{{ \Carbon\Carbon::now()->format('l, d F Y H:i') }}  
	{{-- <h5 class="item" data-content="Tanggal Hari ini">{{ indonesian_date(\Carbon\Carbon::now()) }}  </h5> --}}
</div>
@append

@section('content-body')

<div class="ui grid">
	<div class="thirteen wide stretched column" style="height: 100%">
		<div class="ui tab active" data-tab="penjualan">
			@include('modules.dashboard.penjualan.index')
		</div>
		<div class="ui tab" data-tab="finance">
			@include('modules.dashboard.finance.index')
		</div>
		<div class="ui tab" data-tab="outstanding">
			@include('modules.dashboard.outstanding.index')
		</div>
		<div class="ui tab" data-tab="peta">
			@include('modules.dashboard.peta.index')
		</div>
		<div class="ui tab" data-tab="chat">
			@include('modules.dashboard.chat.index')
		</div>
		<div class="ui tab" data-tab="monitoring">
			@include('modules.dashboard.monitoring.index')
		</div>
	</div>
	<div class="three wide column">
		<div id="tab-dashboard">
			<div class="ui vertical fluid right tabular menu">
				<a class="item active" data-tab="penjualan">
					Penjualan
				</a>
				<a class="item" data-tab="finance" onclick="finances();">
					Finance
				</a>
				<a class="item" data-tab="outstanding" onclick="outstanding_all();">
					Outstanding
				</a>
				<a class="item" data-tab="peta">
					Peta Lokasi
				</a>
				<a class="item" data-tab="chat">
					Chat Box
				</a>
				<a class="item" data-tab="monitoring">
					Monitoring Online/Offline
				</a>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')

@include('modules.dashboard.monitoring.js')
@include('modules.dashboard.outstanding.js')
@include('modules.dashboard.peta.js')
@include('modules.dashboard.chat.js')
@include('modules.dashboard.scripts.js')

@append