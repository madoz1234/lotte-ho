<script type="text/javascript">

	$(document).ready(function() {
		$('#menu .item').tab();

		var dtops = '';
		var dto1 = '';
		var dto2 = '';
		var dto3 = '';
		var dtlastsohar = '';
		var dtlastsobul = '';
		$('button[data-content]').popup({
			hoverable: true,
			position : 'top center',
			delay: {
				show: 300,
				hide: 800
			}
		});

		outstanding_pr =  function(){
		 	// alert($("#outpr").val());
		 	// dto1.ajax.reload();
			dto1 = $('#dataTableOutstanding1').DataTable({
		        dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: true,
				pageLength: 10,
				filter: false,
				sorting: [],
				info:true,
				language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
				ajax:  {
					url: "{{ url($pageUrl) }}/outstanding-pr/grid",
					type: 'POST',
					data: function (d) {
						d._token = "{{ csrf_token() }}";
						d.tmuk_kode = $("#outpr").val();
					}
				}, 
				columns: {!! json_encode($tableStructOutstandingPr) !!},
				drawCallback: function() {
					var api = this.api();

					api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						cell.innerHTML = parseInt(cell.innerHTML)+i+1;
					} );

					$('[data-content]').popup({
						hoverable: true,
						position : 'top center',
						delay: {
							show: 300,
							hide: 800
						}
					});
				}
			});

		 };

		 picking_ot =  function(){
		 	// alert($("#picking").val());
		 	// dto2.ajax.reload();
			dto2 = $('#dataTableOutstanding2').DataTable({
		        dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: true,
				pageLength: 10,
				filter: false,
				sorting: [],
				info:true,
				language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
				ajax:  {
					url: "{{ url($pageUrl) }}/outstanding-picking/grid",
					type: 'POST',
					data: function (d) {
						d._token = "{{ csrf_token() }}";
						d.tmuk_kode = $("#picking").val();
					}
				}, 
				columns: {!! json_encode($tableStructOutstandingPicking) !!},
				drawCallback: function() {
					var api = this.api();

					api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						cell.innerHTML = parseInt(cell.innerHTML)+i+1;
					} );

					$('[data-content]').popup({
						hoverable: true,
						position : 'top center',
						delay: {
							show: 300,
							hide: 800
						}
					});
				}
			});

		 };

		 delivery_note =  function(){
		 	// alert($("#del_note").val());
		 	// dto3.ajax.reload();
			dto3 = $('#dataTableOutstanding3').DataTable({
		        dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: true,
				pageLength: 10,
				filter: false,
				sorting: [],
				info:true,
				language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
				ajax:  {
					url: "{{ url($pageUrl) }}/outstanding-delivery/grid",
					type: 'POST',
					data: function (d) {
						d._token = "{{ csrf_token() }}";
						d.tmuk_kode = $("#del_note").val();
					}
				}, 
				columns: {!! json_encode($tableStructOutstandingDelivery) !!},
				drawCallback: function() {
					var api = this.api();

					api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						cell.innerHTML = parseInt(cell.innerHTML)+i+1;
					} );

					$('[data-content]').popup({
						hoverable: true,
						position : 'top center',
						delay: {
							show: 300,
							hide: 800
						}
					});
				}
			});

		 };

		 so_har =  function(){
		 	// alert($("#so_hari").val());
		 	// dtlastsohar.ajax.reload();
			dtlastsohar = $('#dataTableLastSoHarian').DataTable({
				dom: 'rt<"bottom"ip><"clear">',
				destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: true,
				pageLength: 10,
				filter: false,
				sorting: [],
				info:false,
				language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
				ajax:  {
					url: "{{ url($pageUrl) }}/last-so-harian/grid",
					type: 'POST',
					data: function (d) {
						d._token = "{{ csrf_token() }}";
						d.tmuk_kode = $("#so_hari").val();
					}
				}, 
				columns: {!! json_encode($tableStructLastSoHarian) !!},
				drawCallback: function() {
					var api = this.api();

					api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						cell.innerHTML = parseInt(cell.innerHTML)+i+1;
					} );

					$('[data-content]').popup({
						hoverable: true,
						position : 'top center',
						delay: {
							show: 300,
							hide: 800
						}
					});
				}
			});

		 };

		 so_bul =  function(){
		 	// alert($("#so_hari").val());
		 	// dtlastsobul.ajax.reload();
			dtlastsobul = $('#dataTableLastSoBulanan').DataTable({
		        dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: true,
				pageLength: 10,
				filter: false,
				sorting: [],
				info:false,
				language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
				ajax:  {
					url: "{{ url($pageUrl) }}/last-so-bulanan/grid",
					type: 'POST',
					data: function (d) {
						d._token = "{{ csrf_token() }}";
						d.tmuk_kode = $("#so_bulan").val();
					}
				}, 
				columns: {!! json_encode($tableStructLastSoBulanan) !!},
				drawCallback: function() {
					var api = this.api();

					api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						cell.innerHTML = parseInt(cell.innerHTML)+i+1;
					} );

					$('[data-content]').popup({
						hoverable: true,
						position : 'top center',
						delay: {
							show: 300,
							hide: 800
						}
					});
				}
			});
		 };

		 last_sync =  function(){
		 	// alert($("#so_hari").val());
		 	// dtops.ajax.reload();
			dtops = $('#outstandingProdukSync').DataTable({
		        dom: 'rt<"bottom"ip><"clear">',
		        destroy: true,
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				lengthChange: true,
				pageLength: 10,
				filter: false,
				sorting: [],
				info:false,
				language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
				ajax:  {
					url: "{{ url($pageUrl) }}/outstanding-produk-sync/grid",
					type: 'POST',
					data: function (d) {
						d._token = "{{ csrf_token() }}";
						d.tmuk_kode = $("#last_syncron").val();
					}
				}, 
				columns: {!! json_encode($tableStructOutstandingProdukSync) !!},
				drawCallback: function() {
					var api = this.api();

					api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						cell.innerHTML = parseInt(cell.innerHTML)+i+1;
					} );

					$('[data-content]').popup({
						hoverable: true,
						position : 'top center',
						delay: {
							show: 300,
							hide: 800
						}
					});
				}
			});

		 };
		 outstanding_all = function(){
		 	last_sync();
		 	so_har();
		 	outstanding_pr();
		 }

	});
</script>
