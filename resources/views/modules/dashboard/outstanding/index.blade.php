
<div class="ui grid">
	<div class="sixteen wide column">
		<div class="ui top attached tabular menu" id="menu">
			<a class="active item" data-tab="outstanding-1">Outstanding PR</a>
			<a class="item" data-tab="picking" onclick="picking_ot();">Picking</a>
			<a class="item" data-tab="delivery" onclick="delivery_note();">Delivery Note</a>
		</div>

		<div class="ui bottom attached active tab segment" data-tab="outstanding-1">
			<div class="ui segments">
				<h5 class="ui top attached header">
					<form class="ui filter form">
					<div class="three column row">
							<div class="field">
								<div class="title">
									&nbsp;&nbsp;	Outstanding PR yang Belum di-Picking
								</div>
							</div>
						</div><br>
						<div class="fields">
							<div class="four wide field">
								<select name="op[tmuk_kode]" id="outpr" class="ui fluid search selection dropdown" style="width: 100%; text-align: center;">
									{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
								</select>
							</div>
							<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="outstanding_pr();">
								<i class="search icon"></i>
							</button>
							<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
								<i class="refresh icon"></i>
							</button>
							<button type="button" class="ui right floated green button icon">
								<i class="download icon"></i> Download
							</button>
						</div>
					</form>
				</h5>
				
				<div class="ui packed yellow segment">
					{{-- <table class="ui table">
						<tr align="center">
							<td><b>TMUK Pragma Informatika</b><br>Tahun 2018 YTD
							</td>
						</tr>
					</table> --}}
					<table class="ui very basic striped small compact table" id="dataTableOutstanding1">
						<thead>
							<tr>
								<th style="text-align: center; width: 50px;">No</th>
								<th style="text-align: center; width: 200px;">LSI</th>
								<th style="text-align: center; width: 100px;">TMUK</th>
								<th style="text-align: center; width: 100px;">No PR</th>
								<th style="text-align: center; width: 50px;">Qty PR</th>
								<th style="text-align: center; width: 50px;">Nilai PR (Rp)</th>
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="ui bottom attached tab segment" data-tab="picking">
			<div class="ui segments">
				<h5 class="ui top attached header">
					<form class="ui filter form">
					<div class="three column row">
							<div class="field">
								<div class="title">
									&nbsp;&nbsp;	Outstanding Picking yang Belum di-PO (STDK)
								</div>
							</div>
						</div><br>
						<div class="fields">
							<div class="four wide field">
								<select name="filter[tmuk_kode]" id="picking" class="ui fluid search selection dropdown" style="width: 100%; text-align: center;">
									{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
								</select>
							</div>
							<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="picking_ot();">
								<i class="search icon"></i>
							</button>
							<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
								<i class="refresh icon"></i>
							</button>
							<button type="button" class="ui right floated green button icon">
								<i class="download icon"></i> Download
							</button>
						</div>
					</form>
				</h5>
				<div class="ui packed yellow segment">
					{{-- <table class="ui table">
						<tr align="center">
							<td><b>TMUK Pragma Informatika</b><br>Tahun 2018 YTD
							</td>
						</tr>
					</table> --}}
					<table class="ui very basic striped small compact table" id="dataTableOutstanding2">
						<thead>
							<tr>
								<th style="text-align: center; width: 50px;">No</th>
								<th style="text-align: center; width: 200px;">LSI</th>
								<th style="text-align: center; width: 100px;">TMUK</th>
								<th style="text-align: center; width: 100px;">No Picking</th>
								<th style="text-align: center; width: 50px;">Qty PR</th>
								<th style="text-align: center; width: 50px;">Nilai PR (Rp)</th>
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="ui bottom attached tab segment" data-tab="delivery">
			<div class="ui segments">
				<h5 class="ui top attached header">
					<form class="ui filter form">
					<div class="three column row">
							<div class="field">
								<div class="title">
									&nbsp;&nbsp;	Outstanding Delivery Note yang Belum di-GR
								</div>
							</div>
						</div><br>
						<div class="fields">
							<div class="four wide field">
								<select name="filter[tmuk_kode]" id="del_note" class="ui fluid search selection dropdown" style="width: 100%; text-align: center;">
									{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
								</select>
							</div>
							<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="delivery_note();">
								<i class="search icon"></i>
							</button>
							<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
								<i class="refresh icon"></i>
							</button>
							<button type="button" class="ui right floated green button icon">
								<i class="download icon"></i> Download
							</button>
						</div>
					</form>
				</h5>
				<div class="ui packed yellow segment">
					{{-- <table class="ui table">
						<tr align="center">
							<td><b>TMUK Pragma Informatika</b><br>Tahun 2018 YTD
							</td>
						</tr>
					</table> --}}
					<table class="ui very basic striped small compact table" id="dataTableOutstanding3">
						<thead>
							<tr>
								<th style="text-align: center; width: 50px;">No</th>
								<th style="text-align: center; width: 100px;">LSI</th>
								<th style="text-align: center; width: 100px;">TMUK</th>
								<th style="text-align: center; width: 100px;">No Surat Jalan</th>
								<th style="text-align: center; width: 100px;">Qty PO (STDK)</th>
								<th style="text-align: center; width: 100px;">Nilai Delivery Note (Rp)</th>
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



{{-- ==================================================================== --}}
<div class="ui grid">
	<div class="nine wide column">
		<div class="ui top attached tabular menu" id="menu">
			<a class="active item" data-tab="soharian" onclick="so_har();">Harian</a>
			<a class="item" data-tab="sobulanan" onclick="so_bul();">Bulanan</a>
		</div>

		<div class="ui bottom attached active tab segment" data-tab="soharian">
			<div class="ui segments">
				<h5 class="ui top attached header">
					<form class="ui filter form">
						<div class="three column row">
							<div class="field">
								<div class="title">
									&nbsp;&nbsp;	Last Stock Opname Harian
								</div>
							</div>
						</div><br>
						<div class="fields">
							<div class="six wide field">
								<select name="filter[tmuk_kode]" id="so_hari" class="ui fluid search selection dropdown" style="width: 100%; text-align: center;">
									{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
								</select>
							</div>
								{{-- <div class="field">
									<input name="filter[item_code]" placeholder="Kode Produk" type="text">
								</div> --}}
								{{-- <div class="six field"> --}}
								<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="so_har();">
									<i class="search icon"></i>
								</button>
								<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
									<i class="refresh icon"></i>
								</button>
								<button type="button" class="ui right floated green button icon">
									<i class="download icon"></i> Download
								</button>
								{{-- </div> --}}
								{{-- <div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div> --}}
							</div>
						</form>

				{{-- 	<form class="ui filter form">
					<div class="ui grid">
							<div class="three column row">
								<div class="field">
									<div class="title">
										&nbsp;&nbsp;	Last Stock Opname Harian
									</div>
								</div>
							</div>
						</div><br>
			  				<div class="fields">
								<div class="field" style="width: 5%;">
									<select name="filter[region_id]" class="ui fluid search selection dropdown" style="width: 100%; text-align: center;">
									    {!! \Lotte\Models\Master\Tmuk::options('nama', 'id',[], '-- Pilih TMUK --') !!}
								    </select>
								</div>
								<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="filter_penjualan();">
									<i class="search icon"></i>
								</button>
								<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
									<i class="refresh icon"></i>
								</button>
								<button type="button" class="ui green button icon" style="position: relative; left: 30%;">
										<i class="download icon"></i> Download
								</button>
							</div>
						</form> --}}


					</h5>

					<div class="ui packed yellow segment">
						<table class="ui very basic striped small compact table" id="dataTableLastSoHarian">
							<thead>
								<tr>
									<th style="text-align: center; width: 50px;">No</th>
									<th style="text-align: center; width: 100px;">TMUK</th>
									<th style="text-align: center; width: 100px;">So Harian yg Belum Ter-sync</th>
									<th style="text-align: center; width: 50px;">Last SO Harian</th>
								</tr>
							</thead>
							<tbody>
								@yield('tableBody')
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="ui bottom attached tab segment" data-tab="sobulanan">
				<div class="ui segments">
					<h5 class="ui top attached header">
						<form class="ui filter form">
							<div class="three column row">
								<div class="field">
									<div class="title">
										&nbsp;&nbsp;	Last Stock Opname Bulanan
									</div>
								</div>
							</div><br>
							<div class="fields">
								<div class="six wide field">
									<select name="filter[tmuk_kode]" id="so_bulan" class="ui fluid search selection dropdown" style="width: 100%; text-align: center;">
										{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
									</select>
								</div>
								{{-- <div class="field">
									<input name="filter[item_code]" placeholder="Kode Produk" type="text">
								</div> --}}
								{{-- <div class="six field"> --}}
								<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="so_bul();">
									<i class="search icon"></i>
								</button>
								<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
									<i class="refresh icon"></i>
								</button>
								<button type="button" class="ui right floated green button icon">
									<i class="download icon"></i> Download
								</button>
								{{-- </div> --}}
								{{-- <div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div> --}}
							</div>
						</form>
					</h5>
					<div class="ui packed yellow segment">
						<table class="ui very basic striped small compact table" id="dataTableLastSoBulanan">
							<thead>
								<tr>
									<th style="text-align: center; width: 50px;">No</th>
									<th style="text-align: center; width: 100px;">TMUK</th>
									<th style="text-align: center; width: 100px;">So Bulanan yg Belum Ter-sync</th>
									<th style="text-align: center; width: 50px;">Last SO Bulanan</th>
								</tr>
							</thead>
							<tbody>
								@yield('tableBody')
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		{{-- =========================================================================================================================== --}}


		<div class="seven wide column">
			<!-- Last Sync -->
			<div class="ui segments">
				<h5 class="ui top attached header">
					<form class="ui filter form"><br><br><br>
							<div class="three column row">
								<div class="field">
									<div class="title">
										&nbsp;&nbsp;	Last Sync Produk & Harga
									</div>
								</div>
							</div><br>
							<div class="fields">
								<div class="seven wide field" style="width: 30%;">
									<select name="filter[tmuk_kode]" id="last_syncron" class="ui fluid search selection dropdown" style="width: 100%; text-align: center;">
										{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
									</select>
								</div>
								{{-- <div class="field">
									<input name="filter[item_code]" placeholder="Kode Produk" type="text">
								</div> --}}
								{{-- <div class="six field"> --}}
								<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="last_sync();">
									<i class="search icon"></i>
								</button>
								<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
									<i class="refresh icon"></i>
								</button>
								<button type="button" class="ui right floated green button icon">
									<i class="download icon"></i> Download
								</button>
								{{-- </div> --}}
								{{-- <div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div> --}}
							</div>
						</form>
				</h5>
				<div class="ui packed yellow segment">
					<table class="ui very basic striped small compact table" id="outstandingProdukSync">
						<thead>
							<tr>
								<th style="text-align: center;">No</th>
								<th style="text-align: center;">TMUK</th>
								<th style="text-align: center;">Last Sync Produk & Harga</th>
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				</div>
			</div>
			<!-- Last Sync -->
		</div>
	</div>



