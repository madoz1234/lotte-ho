@section('scripts')
{{-- $('#loading-image').show();

$('#loading-image').hide(); --}}
<script type="text/javascript">
	$(document).ready(function($) {

		var dtoutsetpen = '';
		var dtreduce = '';

		laba_mom = function(){
				$('#laba_mom .load').show();
				$.ajax({
					url: '{{ url('finance/chartmom') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token	  : '{!! csrf_token() !!}',
						start 	  : $("[name=mom_start]").val(),
						end       : $("[name=mom_end]").val(),
						tmuk_code : $("#reduce_mom_tmuk").val()
					},
				})
				.done(function(response) {
					$('#laba_mom').highcharts(response);
					$('#laba_mom .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};
			
		laba_qoq = function(){
			$.ajax({
				url: '{{ url('finance/chartqoq') }}',
				type: 'POST',
				dataType: 'json',
				data: {
					_token:'{!! csrf_token() !!}',
				},
			})
			.done(function(response) {
				$('#laba_qoq').highcharts(response);

			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});

		};

		laba_yoy = function(){
				$('#laba_yoy .load').show();
				$.ajax({
					url: '{{ url('finance/chartyoy') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token	  : '{!! csrf_token() !!}',
						start 	  : $("[name=yoy_start]").val(),
						end       : $("[name=yoy_end]").val(),
						tmuk_code : $("#reduce_yoy_tmuk").val()
					},
				})
				.done(function(response) {
					$('#laba_yoy').highcharts(response);
					$('#laba_yoy .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

			
		escrow_dod = function(){
				$('#escrow_dod .load').show();
				$.ajax({
					url: '{{ url('finance/chartreducedod') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token : '{!! csrf_token() !!}',
						start  		: $('[name=escrow_dod_start]').val(),
						end    		: $('[name=escrow_dod_end]').val(),
						region_id : $("#escrow_dod_region_id").val(),
						lsi_id    : $("#escrow_dod_lsi_id").val(),
						tmuk_code   : $("#escrow_dod_tmuk").val(),
					},
				})
				.done(function(response) {
					$('#escrow_dod').highcharts(response);
					$('#escrow_dod .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};

		escrow_mom = function(){
				$('#escrow_mom .load').show();
				$.ajax({
					url: '{{ url('finance/chartreducemom') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						_token : '{!! csrf_token() !!}',
						start  		: $('[name=escrow_mom_start]').val(),
						end    		: $('[name=escrow_mom_end]').val(),
						region_id : $("#escrow_mom_region_id").val(),
						lsi_id    : $("#escrow_mom_lsi_id").val(),
						tmuk_code   : $("#escrow_mom_tmuk").val(),
					},
				})
				.done(function(response) {
					$('#escrow_mom').highcharts(response);
					$('#escrow_mom .load').hide();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			};


		reduce_ecrow = function(){
			$.ajax({
				url: '{{ url('finance/escrow-balance') }}',
				type: 'POST',
				dataType: 'html',
				data: {_token:'{{ csrf_token() }}'},
			})
			.done(function(response) {
				console.log("success tes");
				// var html = $(response);
				$(".reduce_ecrow").html(response);

			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		};

		// reduce_ecrow =  function(){
		//  	// alert('tara');
		//  	dtreduce.ajax.reload();

		//  };

		// reduce_ecrow
		var dtreduce = $('#dataTableReduce').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
			destroy: true,
			responsive: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			lengthChange: true,
			pageLength: 10,
			filter: false,
			sorting: [],
			info:true,
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			ajax:  {
				url: '{{ url('finance/escrow-balance') }}',
				type: 'POST',
				dataType: 'html',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
				}
			}, 
			drawCallback: function() {
				var api = this.api();

				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML)+i+1;
				} );

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});
			}
		});

		//outstanding setoran penjualan
			var dtoutsetpen = $('#dataTableSetoranPenjualan').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
			destroy: true,
			responsive: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			lengthChange: true,
			pageLength: 10,
			filter: false,
			sorting: [],
			info:true,
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			ajax:  {
				url: "{{ url($pageUrl) }}/setoran-penjualan/grid",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
				}
			}, 
			columns: {!! json_encode($tableStructSetoranPenjualan) !!},
			drawCallback: function() {
				var api = this.api();

				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML)+i+1;
				} );

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});
			}
		});

		finances = function(){
			laba_mom();
			escrow_dod();
			reduce_ecrow();
			escrow_mom();
		};	
	});

</script>
@append
