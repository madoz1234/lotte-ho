<script type="text/javascript">
	$("#dataTableReduceEscrowssss").DataTable({
		dom: 'rt<"bottom"ip><"clear">',
		destroy: true,
		responsive: true,
		autoWidth: false,
		// processing: true,
		// serverSide: true,
		lengthChange: true,
		pageLength: 10,
		filter: false,
		sorting: [],
		info:true,
		language: {
			url: "{{ asset('plugins/datatables/Indonesian.json') }}"
		},
	});
</script>

<div class="eight wide column" style="padding-top: 15px;">
<div class="ui segments">
	<div style="margin: 10px;" class="active content field">
		<div class="ui grid">
			<div class="two column row">
				<div class="left floated eight wide column">&nbsp;
					<div class="title" style="font-weight: bold;">
						Escrow Balance<BR>
						Tahun {{ \Carbon\Carbon::now()->format('Y') }} YTD
					</div>
				</div>
				<div class="fields">
					<div style="margin-left: auto; margin-right: 1px;">
						<button type="button" class="ui green button">
							<i class="download icon"></i> Download
						</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="ui packed yellow segment" style="overflow-x: auto;">
		<table class="ui very basic striped small compact table" id="dataTableReduceEscrowssss">
			<thead>
				<tr>
					<th style="text-align: center; ">#</th>
					<th width="150" style="text-align: center;">TMUK</th>
					@foreach ($header_reduce as $elm)
					<th style="text-align: center;">{{ $elm }}</th>
					@endforeach
				</tr>
				
			</thead>
			<?php $i = 1; ?>
				@foreach ($reduce as $row)
				<tr>
					<td style="text-align: center; ">{{ $i }}</td>
					<td width="150" style="text-align: left; ">{{ $row['nama'] }}</td>
					@foreach ($row['nilai'] as $elm)
					<td style="text-align: center; ">{{ $elm }}</td>
					@endforeach
				</tr>
				<?php $i++; ?>
				@endforeach
		</table>
	</div>
	
</div>
</div>
