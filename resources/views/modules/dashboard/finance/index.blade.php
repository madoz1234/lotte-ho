@include('modules.dashboard.finance.js')

<div class="ui equal width grid">
	<!-- Laba bersih -->
	<div class="column">
		<div class="ui top attached tabular menu">
			{{-- <a class="active item" data-tab="laba-all">Semua Laba</a> --}}
			<a class="active item" data-tab="laba-mom" onclick="laba_mom();">MoM</a>
			<a class="item" data-tab="laba-qoq" onclick="laba_qoq();">QoQ</a>
			<a class="item" data-tab="laba-ytd" onclick="laba_yoy();">YoY</a>
		</div>
		<div class="ui bottom attached active tab segment" data-tab="laba-mom" >
			<form action="" class="ui form" style="padding-bottom: 33px; ">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui mom_statrt" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" name="mom_start" value="{{ date('F Y', strtotime(date('M Y') . ' -5 month')) }}">
										</div>
									</div>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui mom_end" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" name="mom_end" value="{{ date('F Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<div class="ui right floated default left labeled icon save button" onclick="laba_mom();">
									<i class="search icon"></i>
									Cari
								</div>
							</div> 
						</div>
					</div>
				</div>

				<div class="three fields">
					<div class="field">
						<select name="region_id" class="" id='reduce_mom_region_id' style="width: 100%;">
							<option value="0">-- Pilih Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="lsi_id" class="" id="reduce_mom_lsi_id" style="width: 100%;">
							<option value="0">-- Pilih LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="reduce_mom_tmuk" class="" id="reduce_mom_tmuk">
							<option value="0">-- Pilih TMUK --</option>
						</select>
					</div>
				</div>
{{-- <br><br> --}}
			</form>
			<div id="laba_mom" style="width: 100%; height: 400px; margin: 0 auto"><div class="load"><center>Loading ...</center></div></div>
		</div>
		<div class="ui bottom attached tab segment" data-tab="laba-qoq">
			<form action="" class="ui form">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">
									<select class="ui fluid search" name="qoq_finance_triwulan_start">
										@foreach (option_triwulan() as $opt)
											<option value="{{ $opt['triwulan'] }}">{{ $opt['labels'] }}</option>
										@endforeach
									</select>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">	
									<select class="ui fluid search" name="qoq_finance_triwulan_end">
										@foreach (option_triwulan() as $opt)
											<option value="{{ $opt['triwulan'] }}"{{ $opt['triwulan'] == date("Y")."."."4"? "selected":"" }}>{{ $opt['labels'] }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<button type="button" class="ui right floated default left labeled icon save button" onclick="finance_qoq();">
									<i class="search icon"></i>
									Cari
								</button>
							</div> 
						</div>
					</div>
				</div>

				<div class="three fields">
					<div class="field">
						<select name="region_id" class="" id='qoq_finance_region_id' style="width: 100%;">
							<option value="0">-- All Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="lsi_id" class="" id="qoq_finance_lsi_id" style="width: 100%;">
							<option value="0">-- All LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="filter[tmuk_kode]" class="" id="qoq_finance_tmuk">
							<option value="0">-- All TMUK --</option>
						</select>
					</div>
				</div>
			</form>
			<div id="laba_qoq" style="width: 100%; height: 400px; margin: 0 auto"><div class="load"><center>Loading ...</center></div></div>
		</div>
		<div class="ui bottom attached tab segment" data-tab="laba-ytd">
			<form action="" class="ui form">
				{{-- <div class="field">
					<div class="fields">
						<div style="margin-left: auto; margin-right: 1px;">
							<button type="button" class="small ui blue button icon" onclick="penjualan_yoy();">
								<i class="search icon"></i>
							</button>&nbsp;
							<button type="button" class="small ui green button icon">
								<i class="download icon"></i>
							</button>&nbsp;
						</div>
					</div>
				</div> --}}
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui year" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" name="yoy_start" value="{{ date('Y', strtotime(date('Y') . ' -5 year')) }}">
										</div>
									</div>
								</div>
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui year" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Tahun" name="yoy_end" value="{{ date('Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<div class="ui right floated default left labeled icon save button" onclick="laba_yoy();">
									<i class="search icon"></i>
									Cari
								</div>
							</div> 
						</div>
					</div>
				</div>

				<div class="three fields">
					<div class="field">
						<select name="reduce_yoy_region_id" class="" id='reduce_yoy_region_id' style="width: 100%;">
							<option value="0">-- Pilih Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="reduce_yoy_lsi_id" class="" id="reduce_yoy_lsi_id" style="width: 100%;">
							<option value="0">-- Pilih LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="reduce_yoy_tmuk" class="" id="reduce_yoy_tmuk">
							<option value="0">-- Pilih TMUK --</option>
						</select>
					</div>
				</div>
			</form>
			<div id="laba_yoy" style="width: 100%; height: 400px; margin: 0 auto"><div class="load"><center>Loading ...</center></div></div>
		</div>
	</div>
	<!-- laba bersih -->

	<!-- Escrow -->
	<div class="column">
		<div class="ui top attached tabular menu">
			<a class="active item" data-tab="escrow-dod" onclick="escrow_dod();">DoD</a>
			<a class="item" data-tab="escrow-mom" onclick="escrow_mom();">MoM</a>
		</div>

		<div class="ui bottom attached active tab segment" data-tab="escrow-dod">
			<div class="ui segment">
				<form action="" class="ui form">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui date_" id="from">
										<div class="ui input left icon">
											<i class="calendar icon date"></i>
											<input type="text" placeholder="Dari Tanggal" name="escrow_dod_start" value="{{ date('D F Y', strtotime(date('D M Y') . ' -2 month')) }}">
										</div>
									</div>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui date_" id="to">
										<div class="ui input left icon">
											<i class="calendar icon date"></i>
											<input type="text" placeholder="Sampai Tanggal" name="escrow_dod_end" value="{{ date('D F Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<div class="ui right floated default left labeled icon save button" onclick="escrow_dod();">
									<i class="search icon"></i>
									Cari
								</div>
							</div> 
						</div>
					</div>
				</div>
				<div class="three fields">
					<div class="field">
						<select name="region_id" class="" id='escrow_dod_region_id' style="width: 100%;">
							<option value="0">-- Pilih Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="lsi_id" class="" id="escrow_dod_lsi_id" style="width: 100%;">
							<option value="0">-- Pilih LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="escrow_dod_tmuk" class="" id="escrow_dod_tmuk">
							<option value="0">-- Pilih TMUK --</option>
						</select>
					</div>
				</div>

			</form>
				<div id="escrow_dod" style="width: 100%; height: 400px; margin: 0 auto"><div class="load"><center>Loading ...</center></div></div>
			</div>
		</div>

		<div class="ui bottom attached tab segment" data-tab="escrow-mom">
			<div class="ui segment">
				<form action="" class="ui form">
				<div class="field">		
					<div class="fields">
						<div class="eleven wide field">
							<div class="two fields">
								<div class="field">		
									<div class="ui mom_statrt" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" name="escrow_mom_start" value="{{ date('F Y', strtotime(date('M Y') . ' -5 month')) }}">
										</div>
									</div>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui mom_end" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" name="escrow_mom_end" value="{{ date('F Y') }}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="five wide field">
							<div class="field">		
								<div class="ui right floated default left labeled icon save button" onclick="escrow_mom();">
									<i class="search icon"></i>
									Cari
								</div>
							</div> 
						</div>
					</div>
				</div>
				{{-- <div class="two fields">
					<div class="field">		
						<div class="ui mom_statrt" id="from">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Dari Bulan" name="mom_start" value="{{ date('F Y', strtotime(date('M Y') . ' -5 month')) }}">
							</div>
						</div>
					</div> 
					<p style="text-align: center; margin: 10px; ">s.d.</p>
					<div class="field">		
						<div class="ui mom_end" id="to">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="Sampai Bulan" name="mom_end" value="{{ date('F Y') }}">
							</div>
						</div>
					</div>
				</div> --}}

				<div class="three fields">
					<div class="field">
						<select name="region_id" class="" id='escrow_mom_region_id' style="width: 100%;">
							<option value="0">-- Pilih Region --</option>
						</select>
					</div>
					<div class="field">
						<select name="lsi_id" class="" id="escrow_mom_lsi_id" style="width: 100%;">
							<option value="0">-- Pilih LSI --</option>
						</select>
					</div>
					<div class="field">
						<select name="escrow_mom_tmuk" class="" id="escrow_mom_tmuk">
							<option value="0">-- Pilih TMUK --</option>
						</select>
					</div>
				</div>

			</form>
				<div id="escrow_mom" style="width: 100%; height: 400px; margin: 0 auto"><div class="load"><center>Loading ...</center></div></div>
			</div>
		</div>
	</div>
	<!-- Escrow -->


</div>

<div class="ui equal width grid">

	<!-- Escrow Balance -->
	<div class="eight wide column">
		<div class="reduce_ecrow"></div>
	</div>
	<!-- Escrow Balance -->

	<!-- Outstanding Setoran -->
	<div class="eight wide column" style="padding-top: 27px;">
		<div class="ui segments">
			<div style="margin: 10px;" class="active content field">
				<div class="ui grid">
					<div class="two column row">
						<div class="left floated eight wide column">&nbsp;
							<div class="title" style="font-weight: bold;">
								Outstanding Setoran Penjualan<br>
								Tahun {{ \Carbon\Carbon::now()->format('Y') }} YTD
							</div>
						</div>
						<div class="fields">
							<div style="margin-left: auto; margin-right: 1px;">
								<button type="button" class="ui green button">
									<i class="download icon"></i> Download
								</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="ui packed yellow segment">
				<table class="ui very basic striped small compact table" id="dataTableSetoranPenjualan">
					<thead>
						<tr>
							<th style="text-align: center;">#</th>
							<th style="text-align: center;">TMUK</th>
							<th style="text-align: center;">Penjualan(Rp)</th>
							<th style="text-align: center;">Setoran Penjualan (Rp)</th>
							<th style="text-align: center;">Outstanding (Rp)</th>
							<th style="text-align: center;">Last Setor</th>
						</tr>
					</thead>
					<tbody>
						@yield('tableBody')
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Outstanding Setoran -->
</div>




