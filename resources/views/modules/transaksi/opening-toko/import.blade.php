{{-- <form action="{{ url('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input type="file" name="upload_data_produk">
	<button type="submit" name="submit" class="ui yellow button">
	Upload
	</button>
</form> --}}

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Import Data Excel</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ URL::to('transaksi/openingtoko/opening-toko/postimportexcel') }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">		
				<label>Tanggal Opening</label>
				<div class="ui calendar" id="start">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tgl_buat" type="text" placeholder="Tanggal Buat" value="">
					</div>
				</div>
			</div>
			<div class="field">		
				<label>Tanggal Kirim</label>
				<div class="ui calendar" id="end">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tgl_kirim" type="text" placeholder="Tanggal Kirim" value="">
					</div>
				</div>
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label>TMUK</label>
				<select name="tmuk_id" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\Tmuk::options(function($q){
						return $q->kode.' - '.$q->nama;
					}, 'id',[], '-- Pilih TMUk --') !!}
				</select>
			</div>
			<div class="field">
				<label>Nomor PR</label>
				<input name="nomor_pr" placeholder="Generate Number" type="text" value="" readonly="">
			</div>
		</div>
		<div class="field">
		    <div class="ui left action input">
		      <button type="button" class="ui blue button btn-template disabled" onclick="javascript:download_template_opening();" style="margin-right: 20px">
			  	<i class="download icon icon"></i>
			  	Template
			  </button>
			  <label for="file" class="ui icon labeled button">
			  	<i class="file icon"></i>
			    Pilih File
			  </label>
			  <input type="file" name="upload_data_produk" id="file" style="display: none" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
			  <input type="text" id="filename" value="" readonly="">
			</div>
		</div>
	</form>
</div>
<div class="actions">
	
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('#tgl_opening').calendar({
		type: 'date',
		endCalendar: $('#tgl_kirim')
	})

	$('#tgl_kirim').calendar({
		type: 'date',
		startCalendar: $('#tgl_opening')
	})

	$('input[type=file]').change(function () {
		$('#filename').val(this.files[0]['name']);
	})

	$('#filename').click(function () {
		$('#file').click();
	})


	$(document).ready(function() {
		download_template_opening = function(){
			// alert('tara');
			// create form
	        var form = document.createElement("form");
	            form.setAttribute("method", 'POST');
	            form.setAttribute("action", "{{ url('export/download-opening-toko') }}");
	            form.setAttribute("target", "_blank");

	        var csrf = document.createElement("input");
	            csrf.setAttribute("type", "hidden");
	            csrf.setAttribute("name", '_token');
	            csrf.setAttribute("value", '{{ csrf_token() }}');
	        form.appendChild(csrf);

	        var tmuk_kode = document.createElement("input");
	            tmuk_kode.setAttribute("type", "hidden");
	            tmuk_kode.setAttribute("name", 'tmuk_id');
	            tmuk_kode.setAttribute("value", $('[name="tmuk_id"]').val());
	        form.appendChild(tmuk_kode);

	        document.body.appendChild(form);
	        form.submit();

	        document.body.removeChild(form);
		}
	});
</script>

