<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Opening Toko</div>
<div class="content">
<form class="ui data form" id="dataForm">

		<div class="ui error message"></div>

		<div class="two fields">
					<div class="field">
						<label>Tanggal PR</label>
						<input name="tgl_pr" placeholder="Tanggal PR" type="text">
					</div>
					<div class="field">
						<label>LSI</label>
						<input name="lsi" placeholder="LSI" type="text">
					</div>
					
				</div>
				<div class="two fields">
					
					<div class="field">
						<label>Nomor PR</label>
						<input name="no_pr" placeholder="Nomor PR" type="text">
					</div>
					<div class="field">
						<label>TMUK</label>
						<input name="tmuk" placeholder="TMUK" type="text">
					</div>
				</div>

		{{-- <div class="two fields">
			<div class="field">
				<label>LSI :</label>
				<input name="" placeholder="Inputkan LSI" type="text" value="LOTTE GROSIR PASAR REBO">
			</div>
			<div class="field">
				<label>Nomor PR :</label>
				<input name="" placeholder="Inputkan Nomor PR" type="text" value="0999887878786">
			</div>
			
		</div>
		<div class="two fields">
		
			<div class="field">
				<label>TMUK :</label>
				<input name="" placeholder="Inputkan TMUK" type="text" value="Pragma Informatika">
			</div>
			<div class="field">
				<label>Tanggal PR :</label>
				<input name="" placeholder="Tanggal PR" type="text" value="08/02/18">
			</div>
		</div> --}}
	</form>
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<!-- Filter -->
		{{-- <div class="fields">
			<div class="ui grid">
				<div class="field">
					<input name="filter[lsi]" placeholder="LSI" type="text">
				</div>

				<div class="field">
					<input name="filter[tmuk]" placeholder="TMUK" type="text">
				</div>

				<div class="field">
					<input name="filter[nomor_pr]" placeholder="Nomor PR" type="text">
				</div>

				<div class="field">
					<input name="filter[tgl_pr]" placeholder="Tgl PR" type="text">
				</div>
				<button type="button" class="ui teal icon filter button" data-content="Cari Data">
					<i class="search icon"></i>
				</button>
				<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
					<i class="refresh icon"></i>
				</button>
			</div>
		</div> --}}
		<!-- Filter -->
		<br>
		<!-- List Table -->
		<div class="field">

			<table class="ui celled striped table">
				<thead>
					<tr align="center">
						<th rowspan="2">No</th>
						<th rowspan="2">BUMUN</th>
						<th rowspan="2">Kategori 1</th>
						<th rowspan="2">Kode Produk</th>
						<th rowspan="2">Barcode</th>
						<th rowspan="2">Nama Produk</th>
						<th colspan="2">Order Unit Info</th>
						<th colspan="2">PR Qty</th>
						<th rowspan="2">Picking Qty</th>
					</tr>
					<tr>

						<th>Qty UoM</th>
						<th>UoM</th>
						<th>Qty UoM</th>
						<th>UoM</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Makanan</td>
						<td>Makanan</td>
						<td>1052362000</td>
						<td>089686917335</td>
						<td>SARIMI 2 SOTO AYAM 24PCS/CT</td>
						<td>1</td>
						<td>Box</td>
						<td>1</td>
						<td>Box</td>
						<td><input name="tinggi" type="number"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- List Table -->
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	<a target="_blank" href="{{ url($pageUrl) }}/print-opening-toko" class="ui default right labeled icon save button">
		Print
		<i class="print icon"></i>
	</a>
</div>