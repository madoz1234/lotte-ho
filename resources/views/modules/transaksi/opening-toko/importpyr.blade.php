{{-- <form action="{{ url('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input type="file" name="upload_data_produk">
	<button type="submit" name="submit" class="ui yellow button">
	Upload
	</button>
</form> --}}

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Import Data Excel</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ URL::to('transaksi/pembelian/direct-invoice/postimportpyrexcel') }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<input name="tmuk_kode" type="hidden" placeholder="Tanggal Buat" value="{{ $record->tmuk_kode }}" readonly="">
		<div class="two fields">
			<div class="field">		
				<label>Tanggal Opening</label>
				<div class="ui calendar" id="tgl_opening">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tgl_buat" type="text" placeholder="Tanggal Buat" value="{{ $record->tgl_buat }}" readonly="">
					</div>
				</div>
			</div>
			<div class="field">		
				<label>Tanggal Jatuh Tempo</label>
				<div class="ui calendar" id="tgl_jatuh_tempo">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tgl_jatuh_tempo" type="text" placeholder="Tanggal Jatuh Tempo">
					</div>
				</div>
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label>Nomor PyR</label>
				<input name="nomor_pyr" placeholder="Generate Number" type="text" value="PYR-{{ date('Ymd').$record->tmuk_kode.'00000' }}" readonly="">
			</div>
			<div class="field">
				<label>Vendor Lokal Tmuk</label>
				<select name="vendor_lokal" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\VendorLokalTmuk::options('nama', 'id',[], '-- Pilih Vendor Lokal Tmuk --') !!}
				</select>
			</div>
		</div>
		<div class="field">
			<div class="ui left action input">
			  <label for="file" class="ui icon labeled button">
			  	<i class="file icon"></i>
			    Pilih File
			  </label>
			  <input type="file" name="upload_data_produk" id="file" style="display: none" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
			  <input type="text" id="filename" value="" readonly="">
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('#tgl_opening').calendar({
		type: 'date',
		endCalendar: $('#tgl_jatuh_tempo')
	})

	$('#tgl_jatuh_tempo').calendar({
		type: 'date',
		startCalendar: $('#tgl_opening')
	})
	
	$('input[type=file]').change(function () {
		$('#filename').val(this.files[0]['name']);
	})

	$('#filename').click(function () {
		$('#file').click();
	})
</script>