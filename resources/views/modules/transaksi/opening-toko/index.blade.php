@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field">
	<div class="ui calendar" id="tgl_buat">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tgl_buat]" type="text" placeholder="Tanggal Opening">
		</div>
	</div>
</div>
<div class="field">
	<select name="filter[tmuk_kode]" class="ui search dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'TMUK') !!}
	</select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
{{-- <div class="ui grey buttons" style="margin-right: 5px">
	<a href="{{ asset('template\opening-toko.xls') }}" class="ui grey button" target="_blank"><i class="download icon icon"></i>Template</a>
</div> --}}
<div class="ui blue buttons" style="margin-right: 5px">
	<button type="button" class="ui blue importexcel button"><i class="upload icon icon"></i>Upload</button>
</div>
<button type="button" class="ui green button" onclick="javascript:export_opening();">
	<i class="file excel outline icon"></i>
	Export Excel
</button>
@endsection

@section('js-filters')
d.tgl_buat = $("input[name='filter[tgl_buat]']").val();
d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tgl_buat: {
			identifier: 'tgl_buat',
			rules: [{
				type   : 'empty',
				prompt : 'Isian Tanggal Opening Harus Terisi'
			}]
		},

		tgl_kirim: {
			identifier: 'tgl_kirim',
			rules: [{
				type   : 'empty',
				prompt : 'Isian Tanggal Kirim Harus Terisi'
			}]
		},

		tmuk_id: {
			identifier: 'tmuk_id',
			rules: [{
				type   : 'empty',
				prompt : 'Isian Tmuk Harus Terisi'
			}]
		},

		nomor_pr: {
			identifier: 'nomor_pr',
			rules: [{ 
				type   : 'empty', 
				prompt : 'Isian Nomor PR Harus Terisi'
			}]
		},

		upload_data_produk: {
			identifier: 'upload_data_produk',
			rules: [{
				type   : 'empty',
				prompt : 'File Upload Harus Terisi'
			}]
		},

			//Untuk Pyr
			tgl_jatuh_tempo: {
				identifier: 'tgl_jatuh_tempo',
				rules: [{
					type   : 'empty',
					prompt : 'Isian Tanggal Jatuh Tempo Harus Terisi'
				}]
			},
			
			vendor_lokal: {
				identifier: 'vendor_lokal',
				rules: [{
					type   : 'empty',
					prompt : 'Pilihan Vendor Lokal Harus Terisi'
				}]
			},
		};
	</script>
	@endsection

	@section('init-modal')
	<script type="text/javascript">
		initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();

		$('#tgl_buat').calendar({
			type: 'date',
			endCalendar: $('#tgl_kirim')
		})

		$('#tgl_kirim').calendar({
			type: 'date',
			startCalendar: $('#tgl_buat')
		})

		$('#start').calendar({
			type: 'date',
			endCalendar: $('#end')
		})

		$('#end').calendar({
			type: 'date',
			startCalendar: $('#start')
		})
		
		$('#tgl_jatuh_tempo').calendar({
			type: 'date',
			startCalendar: $('#tgl_buat')
		})

		$('#tabelDetailPo').DataTable({
			paging: false,
			filter: false,
			lengthChange: false,
			ordering:false,
			scrollX: true,
			scrollY: 300,
		})
		//untuk Pr
		$(document).ready(function(e){
			$('select[name="tmuk_id"]').on('change', function(){
				$.ajax({
					url: "{{url($pageUrl)}}/on-change-pop-tmuk/"+this.value,
					type: 'GET',
					dataType: 'json',
				})
				.done(function(response) {
					// console.log(response);
					$('input[name="nomor_pr"]').val("PR-"+{{ date('Ymd') }}+response.kode+"0000")
					$('.btn-template').removeClass('disabled');
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			});
		});

		//untuk Pyr
		$(document).ready(function(e){
			$('select[name="tmuk_kode"]').on('change', function(){
				$.ajax({
					url: "{{url($pageUrl)}}/on-change-pop-tmuk/"+this.value,
					type: 'GET',
					dataType: 'json',
				})
				.done(function(response) {
					// console.log(response);
					$('input[name="nomor_pr"]').val("PYR-"+{{ date('Ymd') }}+response.kode+"0000")
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			});
		});
	};
</script>
@endsection


@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>

		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});
		
		$(document).on('click', '.detail-pyr.button', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/detail-pyr/" + id;

			loadModal(url);
		});

		$(document).ready(function() {
			export_opening = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-opening-toko') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var tgl_buat = document.createElement("input");
		            tgl_buat.setAttribute("type", "hidden");
		            tgl_buat.setAttribute("name", 'tgl_buat');
		            tgl_buat.setAttribute("value", $('[name="filter[tgl_buat]"]').val());
		        form.appendChild(tgl_buat);

		        var tmuk_kode = document.createElement("input");
		            tmuk_kode.setAttribute("type", "hidden");
		            tmuk_kode.setAttribute("name", 'tmuk_kode');
		            tmuk_kode.setAttribute("value", $('[name="filter[tmuk_kode]"]').val());
		        form.appendChild(tmuk_kode);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append