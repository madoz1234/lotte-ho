<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Purchase Order (PO)</div>
<div class="content">
	<form class="ui data form" id="dataForm">

		<div class="ui error message"></div>

		<div class="two fields">
			<div class="field">
				<label>Tanggal PR</label>
				<input name="tgl_pr" placeholder="Tanggal PR" type="text" value="{{ $record->tgl_buat or "" }}" readonly="">
			</div>
			<div class="field">
				<label>LSI</label>
				<input name="lsi" placeholder="LSI" type="text" value="{{ $record->tmuk->lsi->nama or "" }}" readonly="">
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label>Nomor PR</label>
				<input name="no_pr" placeholder="Nomor PR" type="text" value="{{ $record->nomor_pr or "" }}" readonly="">
			</div>
			<div class="field">
				<label>TMUK</label>
				<input name="tmuk" placeholder="TMUK" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div>
		</div>
		<!-- List Table -->
		<table class="ui celled striped table" id="tabelDetailPo">
			<thead>
				<tr align="center">
					<th rowspan="2" class="ui center aligned">No</th>
					<th rowspan="2" class="ui center aligned">BUMUN</th>
					<th rowspan="2" class="ui center aligned">Kategori 1</th>
					<th rowspan="2" class="ui center aligned">Barcode</th>
					<th rowspan="2" class="ui center aligned">Kode Produk</th>
					<th rowspan="2" class="ui center aligned">Nama Produk</th>
					<th colspan="2" class="ui center aligned">Order Unit Info</th>
				</tr>
				<tr>
					<th class="ui center aligned">Qty UoM</th>
					<th class="ui center aligned">UoM</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; ?>
				@foreach ($record->detailOpening->sortBy('produk.l1_nm') as $row)
				<tr>
					<td class="ui center aligned">{{ $i }}</td>
					<td class="ui aligned">{{ $row->produk->bumun_nm }}</td>
					<td class="ui aligned">{{ $row->produk->l1_nm }}</td>
					<td class="ui center aligned">{{ $row->produk->produksetting->uom1_barcode }}</td>
					<td class="ui center aligned">{{ $row->produk_kode }}</td>
					<td class="ui aligned">{{ $row->produk->nama }}</td>
					<td class="ui center aligned">{{ $row->qty_po }}</td>
					<td class="ui center aligned">{{ $row->produk->produksetting->uom1_satuan }}</td>
				</tr>
				<?php $i++;	?>
				@endforeach
			</tbody>
		</table>
		<!-- List Table -->
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div> --}}
	{{-- <a target="_blank" href="{{ url($pageUrl) }}/print-opening-toko" class="ui default right labeled icon save button">
		Print
		<i class="print icon"></i>
	</a> --}}
	{{-- <a target="_blank" href="opening-toko/print-opening-toko/'.$record->id.'" class="ui mini default icon print button" data-content="Print Picking"><i class="print icon"></i> Print</a> --}}
</div>