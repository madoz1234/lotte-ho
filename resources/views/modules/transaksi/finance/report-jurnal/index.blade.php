@extends('layouts.scaffold')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.semanticui.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
{{-- <script src="{{ asset('plugins/datatables/dataTables.semanticui.min.js') }}"></script> --}}
<script src="{{ asset('plugins/sweetalert/sweetalert2.js') }}"></script>
@append

@section('scripts')
<script type="text/javascript">
		// global
		var dt = "";
		var formRules = [];
		var initModal = function(){
			return false;
		};
		var unModal = function(){
			return false;
		};
		var doneApproved = function(){
			return false;
		};

		$.fn.form.settings.prompt = {
			empty                : '{name} tidak boleh kosong',
			checked              : '{name} harus dipilih',
			email                : '{name} tidak valid',
			url                  : '{name} must be a valid url',
			regExp               : '{name} is not formatted correctly',
			integer              : '{name} must be an integer',
			decimal              : '{name} must be a decimal number',
			number               : '{name} hanya boleh berisikan angka',
			is                   : '{name} must be "{ruleValue}"',
			isExactly            : '{name} must be exactly "{ruleValue}"',
			not                  : '{name} cannot be set to "{ruleValue}"',
			notExactly           : '{name} cannot be set to exactly "{ruleValue}"',
			contain              : '{name} cannot contain "{ruleValue}"',
			containExactly       : '{name} cannot contain exactly "{ruleValue}"',
			doesntContain        : '{name} must contain  "{ruleValue}"',
			doesntContainExactly : '{name} must contain exactly "{ruleValue}"',
			minLength            : '{name} must be at least {ruleValue} characters',
			length               : '{name} must be at least {ruleValue} characters',
			exactLength          : '{name} must be exactly {ruleValue} characters',
			maxLength            : '{name} cannot be longer than {ruleValue} characters',
			match                : '{name} must match {ruleValue} field',
			different            : '{name} must have a different value than {ruleValue} field',
			creditCard           : '{name} must be a valid credit card number',
			minCount             : '{name} must have at least {ruleValue} choices',
			exactCount           : '{name} must have exactly {ruleValue} choices',
			maxCount 			 : '{name} must have {ruleValue} or less choices'
		};

		filter = function(e){
			// alert('tara');
			var id = $(this).data('[name="filter[tmuk_kode]');
			showTable(id);
		};

		showTable = function(e){
		// alert('tara');
		// var tmuk_kode   = $("#tmuk_kode").val();
		$.ajax({
			url: '{{  url($pageUrl) }}/cari/'+ $('[name="filter[tmuk_kode]"]').val(),
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				tmuk_kode 	: $('[name="filter[tmuk_kode]"]').val(),

			},
		})
		.done(function(response) {

// alert('tara');	
$('.twelve.wide.column').html(response);

})
		.fail(function(response) {
			console.log("error");
		});
		
	}

	</script>
	@yield('rules')
	@yield('init-modal')
	
	@include('layouts.scripts.datatable')
	@include('layouts.scripts.actions')
	@append

	@section('content')
	@section('content-header')
	<div class="ui breadcrumb">
		<?php $i=1; $last=count($breadcrumb);?>
		@foreach ($breadcrumb as $name => $link)
		@if($i++ != $last)
		<a href="{{ $link }}" class="section">{{ $name }}</a>
		<i class="right chevron icon divider"></i>
		@else
		<div class="active section">{{ $name }}</div>
		@endif
		@endforeach
	</div>
	<h2 class="ui header">
		<div class="content">
			{!! $title or '-' !!}
			<div class="sub header">{!! $subtitle or ' ' !!}</div>
		</div>
	</h2>
	@show

	@section('content-body')
	<div class="ui grid">
		<div class="sixteen wide column main-content">
			<div class="ui segments">
				<div class="ui segment">
					<form class="ui filter form">
						<div class="fields">
							<div class="field">
								<div class="ui fluid selection dropdown">
									<input type="hidden" name="filter[entri]">
									<i class="dropdown icon"></i>
									<div class="default text">Entries</div>
									<div class="menu">
										<div class="item" data-value="10">10</div>
										<div class="item" data-value="25">25</div>
										<div class="item" data-value="50">50</div>
										<div class="item" data-value="100">100</div>
									</div>
								</div>
							</div>
							@section('filters')
							<div class="field">
								{{-- <label>TMUK</label> --}}
								<select name="filter[tmuk_kode]" class="ui fluid search selection dropdown " style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
								</select>
							</div>
							<button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="filter('cari');">
								<i class="search icon"></i>
							</button>
							<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
								<i class="refresh icon"></i>
							</button>
							@show
							<div style="margin-left: auto; margin-right: 1px;">
								@section('toolbars')
								{{-- <button type="button" class="ui blue add button">
									<i class="plus icon"></i>
									Tambah Data
								</button>
								<button type="button" class="ui green button">
									<i class="file excel outline icon"></i>
									Export Excel
								</button> --}}
								@show
							</div>
						</div>
					</form>

					@section('subcontent')
					<div class="content">
						<form class="ui data form" id="dataForm" {{-- action="{{ url($pageUrl.$record[0]->idtrans) }}" --}} {{-- method="POST" --}}>
							{{-- {!! csrf_field() !!} --}}
							{{-- <input type="hidden" name="_method" value="PUT"> --}}
							{{-- <input type="hidden" name="id" value="{{ $record[0]->id }}"> --}}
							{{-- {{ dd($row) }} --}}
							<table id="logsheet" class="ui celled compact red table" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th class="center aligned">TMUK</th>
										<th class="center aligned">Id Transaksi</th>
										<th class="center aligned">Waktu Transaksi</th>
										<th class="center aligned">Kode Akun</th>
										<th class="center aligned">Nama Akun</th>
										<th class="center aligned">Debit</th>
										<th class="center aligned">Kredit</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($temp as $row)
									@foreach ($row as $data)

									{{-- {{ dd($data) }} --}}

									<tr data-row="">
										<td>
											<div class="field">
												<div class="ui labeled input">
													{{ $data['tmuk'] }}
												</div>
											</div>	
										</td>
										<td>
											<div class="field">
												<div class="ui labeled input">
													{{ $data['idtrans'] }}
												</div>
											</div>	
										</td>
										<td>
											<div class="field">
												<div class="ui labeled input">
													{{ $data['tanggal'] }}
												</div>
											</div>
										</td>
										<td>
											<div class="field">
												<div class="ui labeled input">
													{{ $data['coa_kode'] }}
												</div>
											</div>
										</td>
										<td>
											<div class="field">
												<div class="ui labeled input">
													{{ $data['coa_nama'] }}
												</div>
											</div>
										</td>
										<td>
											<div class="field">
												<div class="ui labeled input">
													@if($data['posisi'] == 'D')
													<div for="" class="ui label">Rp. </div>
													<input type="text" data-inputmask="'alias' : 'numeric'" style="text-align: right;" value="{{ rupiah($data['jumlah']) }}" readonly />
													@endif
												</div>
											</div>
										</td>
										<td>
											<div class="field">
												<div class="ui labeled input">
													@if($data['posisi'] == 'K')
													<label for="" class="ui label">Rp.</label>
													<input type="text" data-inputmask="'alias' : 'numeric'" style="text-align: right;" value="{{ rupiah($data['jumlah']) }}" readonly />
													@endif
												</div>
											</div>
										</td>
									</tr>
									@endforeach
									<tr>
										<td colspan="7" style="background-color: #f9fafb"></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</form>
					</div>
					@show
				</div>
			</div>
		</div>
	</div>
	@show
	@endsection

	@section('modals')
	<div class="ui {{ $modalSize or 'mini' }} modal" id="formModal">
		<div class="ui inverted loading dimmer">
			<div class="ui text loader">Loading</div>
		</div>
	</div>
	@append