<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail</div>

<div class="content">

	<form class="ui data form" id="dataForm">

		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>Tanggal Buat :</label>
				<input name="" placeholder="Inputkan Tanggal PYR" type="text" value="{{ $record->tgl_buat or "" }}" readonly="">
			</div>
			<div class="field">
				<label>TMUK :</label>
				<input name="" placeholder="Inputkan TMUK" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Nomor :</label>
				<input name="" placeholder="Inputkan Nomor PYR" type="text" value="{{ $record->nomor or "" }}" readonly="">
			</div>
			{{-- <div class="field">
				<label>Saldo Escrow Saat Ini (Rp) :</label>
				<input name="" placeholder="Inputkan Saldo Escrow" type="text" value="{{ FormatNumber($record->tmuk->saldo_escrow) }}" readonly="">
			</div> --}}
		</div>
		<div class="three fields">
			{{-- <div class="field">
				<label>Nomor PYR :</label>
				<input name="" placeholder="Inputkan Nomor PYR" type="text" value="{{ $record->nomor or "" }}" readonly="">
			</div> --}}
			{{-- <div class="field">
				<label>Tipe Barang :</label>
				<input name="" placeholder="Inputkan Vendor Non Lotte" type="text" value="Non Trade Lotte" readonly="">
			</div> --}}
			{{-- <div class="field">
				<label>Vendor Local TMUK :</label>
				<input name="" placeholder="Inputkan Vendor Non Lotte" type="text" value="{{ $record->vendorlokal->nama or "" }}" readonly="">
			</div> --}}
			{{-- <div class="field">
				<label>Alamat Pengiriman :</label>
				<input name="" placeholder="Inputkan Alamat Pengiriman" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div> --}}
		</div>
	</form>

	<table id="tabelDetailDPR" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th class="ui center aligned">No</th>
				<th class="ui center aligned">Kode Produk</th>
				<th class="ui center aligned">Nama Produk</th>
				<th class="ui center aligned">Qty</th>
				<th class="ui center aligned">Unit</th>
				<th class="ui center aligned">Harga (Rp)</th>
			</tr>

		</thead>
		<tbody>

			@if($record->detaildirect->count() > 0)
			{{-- {{ dd($total) }} --}}
			<?php $i = 1; ?>
			@foreach ($record->detaildirect as $row)
			<tr>
				<td class="center aligned">{{ $i }}</td>
				<td class="center aligned">{{ $row->produk_kode }}</td>
				<td class="left aligned">{{ $row->produk->nama }}</td>
				<td class="center aligned">{{ $row->qty_po or ''}}</td>
				<td class="center aligned">{{ $row->produk->produksetting->uom1_satuan }}</td>
				<td class="ui right aligned">{{ FormatNumber($row->price)}}</td>
				{{-- <td class="ui right aligned">{{ $record->vendor_lokal_id == null ?  '-' : FormatNumber($row->price) }}</td> --}}
			</tr>
			<?php $i++;	?>
			@endforeach
			@else
				<tr>
					<td colspan="11">
						<center><i>Maaf tidak ada data</i></center>
					</td>
				</tr>
			@endif
		</tbody>
		{{-- <tfoot class="full-width">
			<tr>
				<th colspan="2" rowspan="2" style="background-color: white; border-bottom-color: white"></th>
				<th colspan="3" class="ui right aligned">Total (Rp)</th>
				<th colspan="1" class="ui right aligned">{{ FormatNumber($record->total) }}</th>
			</tr>
		</tfoot> --}}
	</table>

				</div>
				<div class="actions">
					<div class="ui negative button" style="background: grey;">
						Tutup
					</div>
	{{-- <div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	<div class="ui blue button">
		Cetak
	</div> --}}
</div>