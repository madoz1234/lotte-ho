{{-- <form action="{{ url('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input type="file" name="upload_data_produk">
	<button type="submit" name="submit" class="ui yellow button">
	Upload
	</button>
</form> --}}

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Import Data Excel PR</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ URL::to('transaksi/finance/direct/postimportexcel2') }}" method="POST" enctype="multipart/form-data">

		{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">		
				<label>Tanggal Buat</label>
				<div class="ui calendar vali_date_start">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tgl_buat" type="text" placeholder="Tanggal Buat" value="">
					</div>
				</div>
			</div>
			<div class="field">		
				<label>Tanggal Kirim</label>
				<div class="ui calendar vali_date_end">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tgl_kirim" type="text" placeholder="Tanggal Kirim" value="">
					</div>
				</div>
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label>TMUK</label>
				<select name="tmuk_id" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\Tmuk::options(function($q){
						return $q->kode.' - '.$q->nama;
					}, 'id',[], '-- Pilih TMUk --') !!}
				</select>
			</div>
			<div class="field">
				<label>Nomor PR</label>
				<input name="nomor" placeholder="Generate Number" type="text" value="" readonly="">
			</div>
		</div>
		<div class="field">
		    <div class="ui left action input">
			  <label for="file" class="ui icon labeled button">
			  	<i class="file icon"></i>
			    Pilih File
			  </label>
			  <input type="file" name="upload_data_produk" id="file" style="display: none" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
			  <input type="text" id="filename" value="" readonly="">
			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('#tgl_opening').calendar({
		type: 'date',
		endCalendar: $('#tgl_kirim')
	})

	$('#tgl_kirim').calendar({
		type: 'date',
		startCalendar: $('#tgl_opening')
	})

	$('input[type=file]').change(function () {
		$('#filename').val(this.files[0]['name']);
	})

	$('#filename').click(function () {
		$('#file').click();
	})

	//untuk Pr
		$('select[name="tmuk_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-po/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nomor"]').val(response.nomor)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})
</script>