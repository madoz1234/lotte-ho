@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field">
	<div class="ui calendar" id="tgl_buat">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tgl_buat]" type="text" placeholder="Tanggal">
		</div>
	</div>
</div>
<div class="field">
	<select name="filter[tmuk_kode]" class="ui search dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'TMUK') !!}
	</select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
	{{-- <div class="ui grey export buttons" style="margin-right: 5px">
		<div class="ui button"><i class="download icon icon"></i>Template</div>
		<div class="ui floating dropdown icon button">
			<i class="dropdown icon"></i>
			<div class="menu">
				<a href="{{ asset('template\dpr.xls') }}" class="item"><i class="file outline icon"></i> PR</a>
				<a href="{{ asset('template\dpyr.xls') }}" class="item"><i class="file outline icon"></i>PyR</a>
				<a href="{{ asset('template\pyr-opening.xls') }}" class="item"><i class="file outline icon"></i>Opening</a>
			</div>
		</div>
	</div>

	<div class="ui blue export buttons" style="margin-right: 5px;">
		<div class="ui button"><i class="plus icon icon"></i>Tambah Data</div>
		<div class="ui floating dropdown icon button">
			<i class="dropdown icon"></i>
			<div class="menu">
				<div class="item importexcel2 button"><i class="file outline icon"></i> PR</div>
				<div class="item importexcel button"><i class="file outline icon"></i>PyR</div>
				<div class="item importexcelopening button"><i class="file outline icon"></i>Opening</div>
			</div>
		</div>
	</div> --}}
	<div class="ui grey floating labeled icon dropdown button">
	  <i class="download icon icon"></i>
	  <span class="text">Template</span>
	  <div class="menu">
		<a href="{{ asset('template\dpr.xls') }}" class="item"><i class="sticky note outline icon"></i> PR</a>
		<a href="{{ asset('template\dpyr.xls') }}" class="item"><i class="sticky note outline icon"></i>PyR</a>
		<a href="{{ asset('template\pyr-opening.xls') }}" class="item"><i class="sticky note outline icon"></i>Opening</a>
	  </div>
	</div>

	<div class="ui blue floating labeled icon dropdown button">
	  <i class="plus icon icon"></i>
	  <span class="text">Tambah Data</span>
	  <div class="menu">
	    <div class="item importexcel2 button">
	      <i class="file outline icon"></i>
	      PR
	    </div>
	    <div class="item importexcel button">
	      <i class="file outline icon"></i>
	      Pyr
	    </div>
	    <div class="item importexcelopening button">
	      <i class="file outline icon"></i>
	      Opening
	    </div>
	  </div>
	</div>

{{-- 	<div class="ui blue buttons" style="margin-right: 5px">
	<button type="button" class="ui blue importexcel button"><i class="upload icon icon"></i>Upload</button>
</div> --}}


{{-- <div class="ui grey buttons" style="margin-right: 5px">
	<a href="{{ asset('template\opening-toko.xls') }}" class="ui grey button" target="_blank"><i class="download icon icon"></i>Template PO</a>
</div>
<div class="ui blue buttons" style="margin-right: 5px">
	<button type="button" class="ui blue importexcel button"><i class="upload icon icon"></i>Upload PyR</button>
</div> --}}
	{{-- <button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button> --}}
	{{-- <button type="button" class="ui green button" onclick="javascript:export_tahun_fiskal();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button> --}}
	@endsection

	@section('js-filters')
	d.tgl_buat = $("input[name='filter[tgl_buat]']").val();
	d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
	@endsection

	@section('rules')
	<script type="text/javascript">
		formRules = {
			tgl_buat: {
				identifier: 'tgl_buat',
				rules: [{
					type   : 'empty',
					prompt : 'Isian Tanggal Harus Terisi'
				}]
			},

			tgl_kirim: {
				identifier: 'tgl_kirim',
				rules: [{
					type   : 'empty',
					prompt : 'Isian Tanggal Kirim Harus Terisi'
				}]
			},

			tmuk_id: {
				identifier: 'tmuk_id',
				rules: [{
					type   : 'empty',
					prompt : 'Isian Tmuk Harus Terisi'
				}]
			},

			nomor: {
				identifier: 'nomor',
				rules: [{ 
					type   : 'empty', 
					prompt : 'Isian Nomor Harus Terisi'
				}]
			},

			upload_data_produk: {
				identifier: 'upload_data_produk',
				rules: [{
					type   : 'empty',
					prompt : 'File Upload Harus Terisi'
				}]
			},

			//Untuk Pyr
			tgl_jatuh_tempo: {
				identifier: 'tgl_jatuh_tempo',
				rules: [{
					type   : 'empty',
					prompt : 'Isian Tanggal Jatuh Tempo Harus Terisi'
				}]
			},
			
			vendor_lokal_id: {
				identifier: 'vendor_lokal_id',
				rules: [{
					type   : 'empty',
					prompt : 'Pilihan Vendor Lokal Harus Terisi'
				}]
			},
		};
	</script>
	@endsection

	@section('init-modal')
	<script type="text/javascript">
		initModal = function(){
		$('.vali_date_start').calendar({
			type 			: 'date',
			minDate : new Date({{ $startdate->year }}, {{ $startdate->month-1 }}, {{ $startdate->day }}),
			maxDate : new Date({{ $enddate->year }}, {{ $enddate->month-1 }}, {{ $enddate->day }}),
		});

		$('.vali_date_end').calendar({
			type 			: 'date',
			maxDate : new Date({{ $enddate->year }}, {{ $enddate->month-1 }}, {{ $enddate->day }}),
			startCalendar: $('.vali_date_start')
		});

		//radio checkbox
		$('.ui.radio.checkbox').checkbox();

		$('#tgl_buat').calendar({
			type: 'date',
			endCalendar: $('#tgl_kirim')
		})

		$('#tgl_kirim').calendar({
			type: 'date',
			startCalendar: $('#tgl_buat')
		})

		$('#start').calendar({
			type: 'date',
			endCalendar: $('#end')
		})

		$('#end').calendar({
			type: 'date',
			startCalendar: $('#start')
		})
		
		$('#tgl_jatuh_tempo').calendar({
			type: 'date',
			startCalendar: $('#tgl_buat')
		})

		$('#tabelDetailDPR').DataTable({
			pageLength: 10,
			filter: false,
			lengthChange: false,
			ordering:false,
		})
	};
</script>
@endsection


@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>

		//Calender
		$('.ui.calendar').calendar({
			type: 'date'
		});
		
		$(document).on('click', '.upload-pyr.button', function(e){
			// var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/upload-pyr/";

			loadModal(url);
		});

		$(document).ready(function() {
			export_opening = function(){
				// alert('tara');
				// create form
				var form = document.createElement("form");
				form.setAttribute("method", 'POST');
				form.setAttribute("action", "{{ url('export/export-opening-toko') }}");
				form.setAttribute("target", "_blank");

				var csrf = document.createElement("input");
				csrf.setAttribute("type", "hidden");
				csrf.setAttribute("name", '_token');
				csrf.setAttribute("value", '{{ csrf_token() }}');
				form.appendChild(csrf);

				var tgl_buat = document.createElement("input");
				tgl_buat.setAttribute("type", "hidden");
				tgl_buat.setAttribute("name", 'tgl_buat');
				tgl_buat.setAttribute("value", $('[name="filter[tgl_buat]"]').val());
				form.appendChild(tgl_buat);

				var tmuk_kode = document.createElement("input");
				tmuk_kode.setAttribute("type", "hidden");
				tmuk_kode.setAttribute("name", 'tmuk_kode');
				tmuk_kode.setAttribute("value", $('[name="filter[tmuk_kode]"]').val());
				form.appendChild(tmuk_kode);

				document.body.appendChild(form);
				form.submit();

				document.body.removeChild(form);
			}
		});
	</script>
	@append