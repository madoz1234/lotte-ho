@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field">
	<input name="filter[no_pr]" placeholder="TMUK" type="number">
</div>

{{-- <div class="field">		
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[fiscal_selesai]" type="text" placeholder="Tahun Fiskal">
		</div>
	</div>
</div> --}}

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>

<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>

@endsection

@section('toolbars')
<div class="ui grey buttons" style="margin-right: 5px">
	<button type="button" class="ui grey button"><i class="download icon icon"></i>Template</button>
</div>
<div class="ui blue buttons" style="margin-right: 5px">
<button type="button" class="ui blue button"><i class="upload icon icon"></i>Upload</button>
</div>
<button type="button" class="ui green button">
	<i class="file excel outline icon"></i>
	Export Excel
</button>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		// validasi
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

		$('.ui.dropdown').dropdown();
	};
</script>
@endsection

@section('tableBody')
<tr>
	<td>1</td>
	<td>0601800002 - Pragma Informatika</td>
	<td>
		<button type="button" class="ui mini orange icon add button" data-content="Buat Budget Entri"><i class="edit icon"></i> Detil</button>
	</td>
</tr>
@endsection