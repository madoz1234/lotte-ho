<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Budget Entri</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		
		<div class="three fields">
			<div class="field">
				<label>Toko Lsi</label>
				<div class="ui labeled input">
					<div class="ui basic label">00001</div>
					<select name="filter[region]" class="ui fluid search selection dropdown">
						<option value="">Pilih Lsi</option>
						<option value="00001" selected>Jabodetabek</option>
						<option value="00002">Surabaya</option>
						<option value="00003">Bekasi</option>
						<option value="00004">Bandung</option>
					</select>
				</div>
			</div>

			<div class="field">
				<label>Tmuk</label>
				<div class="ui labeled input">
					<div class="ui basic label">0600100010</div>
					<select name="filter[region]" class="ui fluid search selection dropdown">
						<option value="">Pilih TMUK</option>
						<option value="0600100010" selected>Etokokoe</option>
						<option value="0602600012">Berkah</option>
						<option value="0601800002">Jernih</option>
						<option value="0600700018">Pragma Informatika</option>
					</select>
				</div>
			</div>

			<div class="field">
				<label>Tahun Fiskal</label>
				<div class="ui labeled input">
					<select name="filter[region]" class="ui fluid search selection dropdown">
						<option value="">Tahun Fiskal</option>
						<option value="00001" selected>01/01/2017 - 31/12/2017  Active</option>
						<option value="00002">01/01/2016 - 31/12/2016  Active</option>
						<option value="00003">01/01/2015 - 31/12/2015  Active</option>
						<option value="00004">01/01/2014 - 31/12/2014  Active</option>
					</select>
				</div>
			</div>
		</div>

		<div class="ui form" style="float: right;">
			<div class="fields">			
				<div class="inline field">
					<label>&nbsp;</label>
					<div class="ui labeled input">
						<div class="ui basic label">3010001</div>
						<select name="filter[region]" class="ui fluid search selection dropdown">
							<option value="">Pilih Lsi</option>
							<option value="3010001" selected>Modal Awal</option>
							<option value="3020001">Laba ditahan</option>
							<option value="6010007">Biaya Listrik</option>
							<option value="6010008">Biaya Air</option>
						</select>
					</div>
				</div>
				<button type="button" class="ui primary icon add button" data-content="Tambah Data" data-id="1"><i class="add icon"></i> Tambah</button>
			</div>
		</div>

		<table class="ui compact celled table">
				<thead class="full-width center">
					<tr align="center">
						<th>Kode Akun</th>
						<th>Nama Akun</th>
						<th>Jan</th>
						<th>Feb</th>
						<th>Mar</th>
						<th>Mar</th>
						<th>Apr</th>
						<th>Mei</th>
						<th>Jun</th>
						<th>Jul</th>
						<th>Agu</th>
						<th>Sep</th>
						<th>Okt</th>
						<th>Nov</th>
						<th>Des</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>-</td>
						<td>-</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
						<td>0.00</td>
					</tr>
				</tbody>
			</table>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	<div class="ui default right labeled icon save button">
		Print
		<i class="print icon"></i>
	</div>
</div>