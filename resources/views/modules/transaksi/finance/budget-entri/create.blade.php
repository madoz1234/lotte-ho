<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail KKI</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>TMUK :</label>
				<input name="" placeholder="Inputkan Tanggal PYR" type="text" value="0601800002 - Pragma Informatika">
			</div>
						<div class="field">
				<label>LSI :</label>
				<input name="" placeholder="Inputkan Vendor Non Lotte" type="text" value="Lotte Bandung">
			</div>
			<div class="field">
				<label>Tanggal Submit KKI:</label>
				<input name="" placeholder="Inputkan Saldo Escrow" type="text" value="25/01/18">
			</div>
		</div>
	</form>
</div>

<div class="scrolling content">
	<form class="ui data form" id="dataForm">
		<div class="ui equal width grid">
			<div class="column">
				<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                    	<th class="ui left aligned">I. Perkiraan Investasi</th>
	                    	<th class="ui center aligned">Nilai (Rp.)</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	<tr>
	                		<td class="left aligned" width="80%">1. Startup & Biaya Promosi Opening</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">2. Perijinan Usaha toko</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">3.a. Pekerjaan Renovasi Sipil</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">&nbsp;&nbsp;&nbsp;b. Pekerjaan Kusen Aluminium + Kaca Cermin</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">&nbsp;&nbsp;&nbsp;c. Pekerjaan Folding Gate + Polycarbonate + Teralis</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">&nbsp;&nbsp;&nbsp;d. Pekerjaan Instalasi Listrik (lampu, regrouping)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">&nbsp;&nbsp;&nbsp;e. Instalasi Pemasangan AC</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">&nbsp;&nbsp;&nbsp;f. Pekerjaan Signage (Lisping Papan Nama Toko) Repainting</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">4.a. Peralatan AC Spil (LG 1/2 PK 2 until)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">&nbsp;&nbsp;&nbsp;b. Peralatan Non Elektronik (Rak, Furniture, Acrylic, dll)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">&nbsp;&nbsp;&nbsp;c. Peralatan Elektronik (Komputer, Cooler & Timbangan)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">5.a. Stock Awal</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">&nbsp;&nbsp;&nbsp;b. Modal Kerja 1 bulan (ditransfer ke rek Ops TMUK)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<th class="center aligned" width="80%">Sub Total Investasi (Sebelum PPN)</th>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="80%">Sewa Ruko selama % tahun @ Rp. 33.000.000,-/tahun (inc Pph)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<th class="center aligned" width="80%">Total Investasi (Sebelum PPN)</th>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<th class="center aligned" width="80%">PPN</th>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<th class="center aligned" width="80%">Total Investasi (Sesudah PPN)</th>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                </tbody>
	            </table>
	            <table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                    	<th class="ui left aligned">II. Perkiraan Penjualan per bulan</th>
	                    	<th class="ui center aligned">Terburuk (BEP)</th>
	                    	<th class="ui center aligned">Normal (Layak)</th>
	                    	<th class="ui center aligned">Terbaik (Aman)</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	<tr>
	                		<td class="left aligned" width="40%">1. Struk Per Day (STD)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">2. Average Purchase per Customer (APC)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">3. Sales Per Day (SPD)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">4. Penjualan per Bulan (SPD x 30 hari)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                </tbody>
	            </table>
			</div>
			<div class="column">
				
	            <table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                    	<th class="ui left aligned">III. Perkiraan Pendapatan per bulan</th>
	                    	<th class="ui center aligned">Estimasi</th>
	                    	<th class="ui center aligned">Estimasi</th>
	                    	<th class="ui center aligned">Estimasi</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	<tr>
	                		<td class="left aligned" width="40%">1. Laba Kotor Penjualan (Estimasi Gross MArgin = 15%)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">2. Pendapatan Lain-lain</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">3. Pendapatan Sewa Tempat Pemanjangan</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<th class="canter aligned" width="40%">Total Perkiraan Pendapatan / bulan</th>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                </tbody>
	            </table>
	            <table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                    	<th class="ui left aligned">IV. Perkiraan Pendapatan per bulan</th>
	                    	<th class="ui center aligned">&nbsp</th>
	                    	<th class="ui center aligned">&nbsp</th>
	                    	<th class="ui center aligned">&nbsp</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	<tr>
	                		<td class="left aligned" width="40%">1. Biaya Type A -a. Biaya Air</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-b. Biaya Listrik (3300VA)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-c. Biaya Telp & Internet</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-d. Biaya Keamanan</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-e. Biaya Lainnya (Sumbangan, dll jika ada)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-f. Plastik Pembungkus (struk sales X days)X Rp 25,-</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-g. Barang Rusak</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-h. Barang Hilang</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-i. Biaya Distribusi (Administrasi) 2%</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">2. Biaya Type B :a. Karyawan Toko 2 org/2.0jt</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">3. Biaya Type C -a. Premi lainnya</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-b. PPh Final Sewa Tempat Pemajangan (10% Stp)</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">4. Biaya Type D -a. Amortisasi Biaya Lainnya</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-b. Amortisasi Perijinan, Renovasi, dll</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-c. Depresiasi Investasi Peralatan, dll</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;-d. Amortisasi Sewa Bangunan</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<th class="left aligned" width="40%">Total Pengeluaran / bulan</th>
	                		<th class="right aligned" width="20%"><input type="number" value=""></th>
	                		<th class="right aligned" width="20%"><input type="number" value=""></th>
	                		<th class="right aligned" width="20%"><input type="number" value=""></th>
	                	</tr>
	                </tbody>
	            </table>
	            <table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                    	<th colspan="4" class="ui left aligned">V. Perkiraan Rata-rata Laba /(Rugi) perbulan selama 5 tahun (sebelum pajak penghasilan)</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	<tr>
	                		<td class="left aligned" width="40%">1. Laba Bersih</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                	<tr>
	                		<td class="left aligned" width="40%">2. Laba Cash</td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                		<td class="right aligned" width="20%"><input type="number" value=""></td>
	                	</tr>
	                </tbody>
	            </table>
			</div>
		</div>

		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>