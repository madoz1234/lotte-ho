@include('layouts.scripts.inputmask')

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Akuisisi Aset</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		<div class="ui error message"></div>
		{!! csrf_field() !!}
		<div class="field">
			<label>TMUK</label>
			<div class="ui labeled input">
				<select name="tmuk_kode" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
				</select>
			</div>
		</div>

		<div class="two fields">
			<div class="field">
				<label>Nama Aset</label>
				<input name="nama" placeholder="Inputkan Nama Aset" type="text">
			</div>

			<div class="field">
			<label>Tipe Aset</label>
				<div class="ui labeled input">
					<select name="tipe_id" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\TipeAset::options('tipe', 'id',[], '-- Pilih Tipe Aset --') !!}
					</select>
				</div>
			</div>
		</div>
		
		<div class="two fields">
			<div class="field">
				<label>Nomor Seri</label>
				<input name="no_seri" placeholder="Inputkan Nomor Seri" type="text">
			</div>

			<div class="field">
				<label>Tanggal Pembelian</label>
				<div class="ui calendar" id="from">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tanggal_pembelian" type="text" placeholder="Tanggal Pembelian">
					</div>
				</div>
			</div>
		</div>

		<div class="two fields">
			<div class="field">
				<label>Nilai Pembelian</label>
				<div class="ui labeled input">
					<div class="ui basic label">
						Rp.
					</div>
					{{-- <input name="nilai_pembelian" data-inputmask="'alias' : 'numeric'" placeholder="Inputkan Nilai Pembelian" type="number"> --}}
					<input type="text" name="nilai_pembelian" maxlength="20" data-inputmask="'alias' : 'numeric'" placeholder="Inputkan Nilai Pembelian" value="{{ $nilai_pembelian or "" }}">
				</div>
			</div>
			<div class="field">
				<label>Kondisi Barang</label>
				<div class="ui fluid search selection dropdown">
					<input type="hidden" name="kondisi">
					<i class="dropdown icon"></i>
					<div class="default text">-- Pilih Kondisi Barang --</div>
					<div class="menu">
						<div class="item" data-value="1">Baru</div>
						<div class="item" data-value="2">Bekas</div>
					</div>
				</div>
			</div>
		</div>

		{{-- <div class="two fields">
			<div class="field">
				<label>Kondisi Barang</label>
				<div class="ui fluid search selection dropdown">
					<input type="hidden" name="kondisi">
					<i class="dropdown icon"></i>
					<div class="default text">-- Pilih --</div>
					<div class="menu">
						<div class="item" data-value="Baru">Baru</div>
						<div class="item" data-value="Bekas">Bekas</div>
					</div>
				</div>
			</div>
			<div class="field">
				<label>Akuisisi</label>
				<input type="text" name="akuisisi" placeholder="Akuisisi Aset">
			</div>
		</div>

		<div class="two fields">
			<div class="field">
				<label>Tanggal Pembuangan</label>
				<div class="ui calendar" id="from">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tanggal_pembuangan" type="text" placeholder="Tanggal Pembuangan">
					</div>
				</div>
			</div>
			<div class="field">
				<label>Nilai Pembuangan</label>
				<div class="ui labeled input">
					<div class="ui basic label">
						Rp.
					</div>
					<input name="nilai_pembuangan" placeholder="Inputkan Nilai Pembuangan" type="number">
				</div>
			</div>
		</div> --}}

	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>