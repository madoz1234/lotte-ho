@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field" style="width: 15%;">
	<select name="filter[tmuk_kode]" class="ui fluid search selection dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- TMUK --') !!}
	</select>
</div>
<div class="field" style="width: 15%;">
	<select name="filter[tipe_id]" class="ui fluid search selection dropdown">
		{!! \Lotte\Models\Master\TipeAset::options('tipe', 'id',[], '-- Tipe Aset --') !!}
	</select>
</div>

<div class="field">
	<input type="text" name="filter[nama]" placeholder="Nama Aset">
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
{{-- <button type="button" class="ui blue add button">
	<i class="plus icon"></i>
	Tambah Data
</button> --}}
<button type="button" class="ui green button" onclick="javascript:export_akuisisi_aset();">
	<i class="file excel outline icon"></i>
	Export Excel
</button>
@endsection

@section('js-filters')
d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
d.tipe_id = $("select[name='filter[tipe_id]']").val();
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tmuk_kode: {
			identifier: 'tmuk_kode',
			rules: [{
				type   : 'empty',
				prompt : 'Isian TMUK tidak boleh kosong'
			}]
		},

		tipe_id: {
			identifier: 'tipe_id',
			rules: [{
				type   : 'empty',
				prompt : 'Isian Tipe Aset tidak boleh kosong'
			}]
		},

		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Nama Aset tidak boleh kosong'
			}]
		},

		no_seri: {
			identifier: 'no_seri',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Nomor Seri tidak boleh kosong'
			}]
		},

		tanggal_pembelian: {
			identifier: 'tanggal_pembelian',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Tanggal Pembuatan tidak boleh kosong'
			}]
		},

		nilai_pembelian: {
			identifier: 'nilai_pembelian',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Nilai Pembelian tidak boleh kosong'
			}]
		},

		kondisi: {
			identifier: 'kondisi',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Kondisi tidak boleh kosong'
			}]
		},
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
			type: 'date',
			startMode: 'year',
		});
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_akuisisi_aset = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export_akuisisi_aset') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var tmuk_kode = document.createElement("input");
		            tmuk_kode.setAttribute("type", "hidden");
		            tmuk_kode.setAttribute("name", 'tmuk_kode');
		            tmuk_kode.setAttribute("value", $('[name="filter[tmuk_kode]"]').val());
		        form.appendChild(tmuk_kode);

		        var tipe_id = document.createElement("input");
		            tipe_id.setAttribute("type", "hidden");
		            tipe_id.setAttribute("name", 'tipe_id');
		            tipe_id.setAttribute("value", $('[name="filter[tipe_id]"]').val());
		        form.appendChild(tipe_id);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append