@include('layouts.scripts.inputmask')

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Akuisisi Aset</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		<div class="ui error message"></div>
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="field">
			<label>TMUK</label>
			<input type="text" placeholder="Tanggal Pembelian" value="{{ $record->tmuk->nama  or '' }}" disabled="">
			{{-- <div class="ui labeled input">
				<select name="tmuk_kode" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',['selected' => isset($record->tmuk_kode) ? $record->tmuk_kode : ''], '-- Pilih TMUK --') !!}
				</select>
			</div> --}}
		</div>

		<div class="two fields">
			<div class="field">
				<label>Nama Aset</label>
				<input placeholder="Inputkan Nama Aset" type="text" value="{{ $record->nama or '' }}" disabled="">
			</div>

			<div class="field">
			<label>Tipe Aset</label>
				<input placeholder="Inputkan Nama Aset" type="text" value="{{ $record->tipe->tipe or '' }}" disabled="">
				{{-- <div class="ui labeled input">
					<select name="tipe_id" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\TipeAset::options('tipe', 'id',['selected' => isset($record->tipe_id) ? $record->tipe_id : ''], '-- Pilih TMUK --') !!}
					</select>
				</div> --}}
			</div>
		</div>
		
		<div class="two fields">
			<div class="field">
				<label>Nilai Pembelian</label>
				<div class="ui labeled input">
					<div class="ui basic label">
						Rp.
					</div>
					{{-- <input name="nilai_pembelian" data-inputmask="'alias' : 'numeric'" placeholder="Inputkan Nilai Pembelian" type="number"> --}}
					<input type="text" maxlength="20" data-inputmask="'alias' : 'numeric'" placeholder="Inputkan Nilai Pembelian" value="{{ $record->nilai_pembelian or '' }}" disabled="">
				</div>
			</div>

			<div class="field">
				<label>Tanggal Pembelian</label>
				<input placeholder="Inputkan Nomor Seri" type="text" value="{{ $record->tanggal_pembelian or '' }}" disabled="">
				{{-- <div class="ui calendar" id="from">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tanggal_pembelian" type="text" placeholder="Tanggal Pembelian" value="{{ $record->tanggal_pembelian or '' }}">
					</div>
				</div> --}}
			</div>
		</div>

		<div class="two fields">
			<div class="field">
				<label>Nomor Seri</label>
				<input name="no_seri" placeholder="Inputkan Nomor Seri" type="text" value="{{ $record->no_seri or '' }}">
			</div>
			<div class="field">
				<label>Kondisi Barang</label>
				<div class="ui fluid search selection dropdown">
					<input type="hidden" name="kondisi" value="{{ $record->kondisi or '' }}">
					<i class="dropdown icon"></i>
					<div class="default text">-- Pilih Kondisi Barang --</div>
					<div class="menu">
						<div class="item" data-value="1">Baru</div>
						<div class="item" data-value="2">Bekas</div>
					</div>
				</div>
			</div>
		</div>

		{{-- <div class="two fields">
			<div class="field">
				<label>Kondisi Barang</label>
				<div class="ui fluid search selection dropdown">
					<input type="hidden" name="kondisi">
					<i class="dropdown icon"></i>
					<div class="default text">-- Pilih --</div>
					<div class="menu">
						<div class="item" data-value="Baru">Baru</div>
						<div class="item" data-value="Bekas">Bekas</div>
					</div>
				</div>
			</div>
			<div class="field">
				<label>Akuisisi</label>
				<input type="text" name="akuisisi" placeholder="Akuisisi Aset">
			</div>
		</div>

		<div class="two fields">
			<div class="field">
				<label>Tanggal Pembuangan</label>
				<div class="ui calendar" id="from">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tanggal_pembuangan" type="text" placeholder="Tanggal Pembuangan">
					</div>
				</div>
			</div>
			<div class="field">
				<label>Nilai Pembuangan</label>
				<div class="ui labeled input">
					<div class="ui basic label">
						Rp.
					</div>
					<input name="nilai_pembuangan" placeholder="Inputkan Nilai Pembuangan" type="number">
				</div>
			</div>
		</div> --}}

	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>