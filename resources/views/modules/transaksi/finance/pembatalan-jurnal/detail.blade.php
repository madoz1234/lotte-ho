<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Pembatalan Jurnal Manual</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record[0]->idtrans) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record[0]->id }}">
		<div class="two fields">
			<div class="field">
				<label>Tanggal Jurnal</label>		
				<div class="ui calendar" id="to">
					<div class="ui input left icon disabled">
						<i class="calendar icon date"></i>
						<input name="tanggal" type="text" placeholder="Tanggal Selesai" value="{{ $record[0]->tanggal or "" }}">
					</div>
				</div>
			</div>

			<div class="field">
				<label for="fruit">TMUK</label>
				<select name="tmuk_kode" class="ui fluid search selection dropdown disabled" style="width: 100%;">
					{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',['selected' => isset($record[0]->tmuk_kode) ? $record[0]->tmuk_kode : '',], '-- Pilih TMUK --') !!}
				</select>
			</div>
		</div>

		<table id="logsheet" class="ui celled compact red table" width="100%" cellspacing="0">
			<thead>
				<tr>
					{{-- <th class="center aligned">#</th> --}}
					<th class="center aligned">Kode Akun</th>
					<th class="center aligned">Nama Akun</th>
					<th class="center aligned">Debit</th>
					<th class="center aligned">Kredit</th>
					<th class="center aligned">Memo</th>
					{{-- <th class="center aligned">Aksi</th> --}}
				</tr>
			</thead>
			<tbody>
				@foreach($record as $row)
				<tr data-row="">
					<input type="hidden" name="id_jurnal[{{ $row->id }}]" value="{{ $row->id }}">
					{{-- <td class="center aligned"><input type="checkbox" name="" class="case" value=""></td> --}}
					<td>
						<div class="field">
							<div class="ui input">
								{{ $row->coa_kode }}
							</div>
						</div>
					</td>
					<td>
						<div class="field">
							<div class="ui labeled input">
								{{ $row->coa->nama }}
							</div>
						</div>
					</td>
					<td>
						<div class="field">
							<div class="ui labeled input disabled">
								<label for="" class="ui label">Rp.</label>
								<input type="text" name="jumlah_debit[{{ $row->id }}]" placeholder="" data-inputmask="'alias' : 'numeric'" value="{{ $row->posisi == 'D' ? $row->jumlah : 0  }}">
							</div>
						</div>
					</td>
					<td>
						<div class="field">
							<div class="ui labeled input disabled">
								<label for="" class="ui label">Rp.</label>
								<input type="text" name="jumlah_kredit[{{ $row->id }}]" placeholder="" data-inputmask="'alias' : 'numeric'" value="{{ $row->posisi == 'K' ? $row->jumlah : 0  }}">
							</div>
						</div>
					</td>
					<td>
						<div class="field">
							<div class="ui labeled input disabled">
								{{-- <label for="" class="ui label">Rp.</label> --}}
								<input type="text" name="deskripsi_memo[{{ $row->id }}]" placeholder="" value="{{ $row->deskripsi or ""  }}">
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{{--  <button type="button" onclick="add_row()" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Tambah</button> --}}
		{{-- <button type="button" style="margin-left:30px; margin-bottom: 10px" class="hapus ui mini red icon trash button"><i class="icon trash button"></i></button>
		<button type="button" onclick="add_row()"  style="margin-bottom: 10px" class="ui mini blue icon plus button"><i class="icon plus button"></i></button> --}}

	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div> --}}
</div>

@include('layouts.scripts.inputmask')