@extends('layouts.grid', ['type'=>'autorefresh'])

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')

<div class="field">		
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tanggal]" type="text" placeholder="Tanggal Jurnal">
		</div>
	</div>
</div>
<div class="field">
	<input type="text" name="filter[no_jurnal]" placeholder="Nomor Jurnal">
</div>
<div class="field">
	<select name="filter[tmuk_kode]" class="ui fluid search selection dropdown" style="width: 100%;">
	    {!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
    </select>
</div>
<div class="field">
	<select name="filter[coa_kode]" class="ui search dropdown" style="width: 100%">
	    {!! \Lotte\Models\Master\Coa::options('nama', 'kode',[], '-- Pilih Akun --') !!}
    </select>
</div>



<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>

<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>

@endsection

@section('js-filters')
	d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
    d.coa_kode = $("select[name='filter[coa_kode]']").val();
    d.tanggal = $("input[name='filter[tanggal]']").val();
    d.no_jurnal = $("input[name='filter[no_jurnal]']").val();
@endsection

@section('toolbars')

@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tgl_buat: {
			identifier: 'tgl_buat',
			rules: [{
				type   : 'empty',
				prompt : 'Tanggal Buat Harus Terisi'
			}]
		},

		nomor_pr: {
			identifier: 'nomor_pr',
			rules: [{
				type   : 'empty',
				prompt : 'Nomor PR Harus Terisi'
			}]
		},

		tmuk_kode: {
			identifier: 'tmuk_kode',
			rules: [{
				type   : 'empty',
				prompt : 'TMUK Harus Terisi'
			}]
		},
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
			type: 'date'
		});

		$('#listTableDetail').DataTable({
			pageLength: 5,
			filter: false,
			lengthChange: false,
			filter: false,
			ordering:false,
		});
	};
</script>
@endsection

@section('scripts')
<script type="text/javascript">
	$('.ui.calendar').calendar({
		type: 'date'
	});


$(document).on('click', '#pembatalan', function(e){
			var id = $(this).data("id");
			swal({
				title: 'Batalkan Jurnal ?',
				text: "Data yang sudah dibatalkan, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then((result) => {
				if (result) {
					$.ajax({
		                url: "{{url($pageUrl)}}/on-change-pop-pembatalan/"+id,
		                type: 'GET',
		                dataType: 'json',
		            })
		            .done(function(response) {
		                window.location.reload();
		            })
		            .fail(function() {
		                console.log("error");
		            })
		            .always(function() {
		                console.log("complete");
		            });
				}
			})
			
		});
</script>
@endsection