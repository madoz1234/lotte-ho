@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@if (session('success'))
<div class="flash-message">
	<div class="alert alert-success">

	</div>
</div>
@endif



@section('filters')
<div class="field">
	<div class="ui calendar" id="filter_tanggal">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tanggal]" type="text" placeholder="Tanggal Jurnal">
		</div>
	</div>
</div>
<div class="field">
	<input type="text" name="filter[no_jurnal]" placeholder="Nomor Jurnal">
</div>
<div class="field">
	<select name="filter[tmuk_kode]" class="ui search selection dropdown" style="width: 100%;">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
	</select>
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
<button type="button" class="ui blue add button">
	<i class="plus icon"></i>
	Tambah Data
</button>
	{{-- <button type="button" class="ui green button" onclick="javascript:export_kustomer();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button> --}}
	@endsection

	@section('js-filters')
    d.tanggal = $("input[name='filter[tanggal]']").val();
	d.no_jurnal = $("input[name='filter[no_jurnal]']").val();
	d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
	@endsection

	@section('rules')
	<script type="text/javascript">
		formRules = {
			tmuk_kode: {
				identifier: 'tmuk_kode',
				rules: [{
					type   : 'empty',
					prompt : 'TMUK Harus Terisi'
				}]
			},
			tanggal: {
				identifier: 'tanggal',
				rules: [{
					type   : 'empty',
					prompt : 'Tanggal Harus Terisi'
				}]
			},
		};
	</script>
	@endsection

	@section('init-modal')
	<script type="text/javascript">
		initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();

		//Calender
		@if (auth()->user()->hasRole('system-administrator'))
            $('.tanggal_jurnal').calendar({
				type 			: 'datetime',
				minDate : new Date({{ $startdate->year }}, {{ $startdate->month-3 }}, {{ $startdate->day }}),
				maxDate : new Date({{ $enddate->year }}, {{ $enddate->month-1 }}, {{ $enddate->day }}),
			});
        @else
			$('.tanggal_jurnal').calendar({
				type 			: 'datetime',
				minDate : new Date({{ $startdate->year }}, {{ $startdate->month-1 }}, {{ $startdate->day }}),
				maxDate : new Date({{ $enddate->year }}, {{ $enddate->month-1 }}, {{ $enddate->day }}),
			});
        @endif

        $('.tanggal_jurnal_detail').calendar({
				type 			: 'datetime',
			});
        
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		//Calender
		$('#filter_tanggal').calendar({
		  type: 'date'
		});

	</script>
@append