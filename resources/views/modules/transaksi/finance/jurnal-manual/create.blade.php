
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Jurnal Manual</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>Tanggal Jurnal</label>		
				<div class="ui calendar tanggal_jurnal">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tanggal" type="text" placeholder="Tanggal Selesai" value="{{ $tanggal or "" }}">
					</div>
				</div>
			</div>
			<div class="field">
				<label for="fruit">TMUK</label>
				<select name="tmuk_kode" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
				</select>
			</div>
			{{-- <input name="tahun_fiskal_id" type="text" placeholder="Tanggal Selesai" value="{{ $tahun_fiskal_id or "" }}" disabled=""> --}}
		</div>

		<table id="logsheet" class="ui celled compact red table" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th class="center aligned">#</th>
					<th class="center aligned">Kode Akun</th>
					<th class="center aligned">Nama Akun</th>
					<th class="center aligned">Debit</th>
					<th class="center aligned">Kredit</th>
					<th class="center aligned">Memo</th>
					{{-- <th class="center aligned">Aksi</th> --}}
				</tr>
			</thead>
			<tbody>
				{{-- asas --}}
			</tbody>
			<tfoot>
				<tr>
					<th colspan="3" class="ui right aligned">Total</th>
					<th colspan="1" class="ui center aligned">
						<div class="field">
							<div class="ui labeled input">
								<label for="" class="ui label">Rp.</label>
								<input type="text" name="total_debit" id="total_debit" class="form-control" placeholder="" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="">
							</div>
						</div>
					</th>
					<th colspan="1" class="ui center aligned">
						<div class="field">
							<div class="ui labeled input">
								<label for="" class="ui label">Rp.</label>
								<input type="text" name="total_kredit" id="total_kredit" class="form-control" placeholder="" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="">
							</div>
						</div>
					</th>
					<th></th>
				</tr>
			</tfoot>
		</table>
		<button type="button" style="margin-left:30px; margin-bottom: 10px" class="hapus ui mini red icon trash button"><i class="icon trash button"></i></button>
		<button type="button" onclick="add_row()"  style="margin-bottom: 10px" class="ui mini blue icon plus button"><i class="icon plus button"></i></button>

		<template id="template-row">
			<tr>
				<td class="center aligned"><input type="checkbox" class="case"></td>
				<td>
					<div class="field">
						<div class="ui labeled input">
							<input type="text" name="kode_coa" class="kode_coa" value="" placeholder="Kode Akun" readonly="" style="background-color: #edf1f6;" >
						</div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui labeled input">
							<select name="coa_kode_kode[]" class="nama_coa ui fluid search selection dropdown" onchange="setKodeCoa(this)" style="width: 100%;">
								{{-- {!! \Lotte\Models\Master\Coa::options('nama', 'kode',[], '-- Pilih COA --') !!} --}}
								{!! \Lotte\Models\Master\Coa::options('nama', 'kode',['filters' => [function($query){ $query->where('status', [1]); }]], '-- Pilih COA --') !!}


							</select>
						</div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui labeled input">
							<label for="" class="ui label">Rp.</label>
							<input type="text" name="jumlah_debit[]" class="form-control debit" onkeyup="setDebit(this)" placeholder="" data-inputmask="'alias' : 'numeric'">
						</div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui labeled input">
							<label for="" class="ui label">Rp.</label>
							<input type="text" name="jumlah_kredit[]" class="form-control kredit" onkeyup="setKredit(this)" placeholder="" data-inputmask="'alias' : 'numeric'">
						</div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui labeled input">
							{{-- <label for="" class="ui label">Rp.</label> --}}
							<input type="text" name="deskripsi_memo[]" placeholder="" value="{{ $deskripsi or ""  }}">
						</div>
					</div>
				</td>
			</tr>
		</template>

	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button disabled" id="btn-save">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

@include('layouts.scripts.inputmask')

<script type:"text/javascript">
	$(document).ready(function(){
		$('#dataForm').keydown(function(event){
		    if(event.keyCode == 13) {
		      event.preventDefault();
		      return false;
		    }
		});

		$(".hapus").on('click', function(elm) {
			$('.case:checkbox:checked').parents("tr").remove();
			$('.check_all').prop("checked", false);
			check();

		});

        // untuk manipulasi tabel
        get_index = function(elm)
        {
        	return $(elm).closest("tr").index();
        }

        delete_row = function(elm)
        {
        	var idx = get_index(elm);

        	if (idx != 0) {
        		var tr = $(elm).closest("tr");
        		$(tr).fadeOut(500, function(){
        			$(this).remove();
        		});
        	}
        }

        add_row = function()
        {
            // get from template
            var tr = $('#template-row').html();

            // append to table
            $('table#logsheet > tbody').append(tr);

        	reloadMask();
        }

        // event
        setKodeCoa = function(elm){
        	var tr = $(elm).closest('tr').find('.kode_coa').val(elm.value);
        }

        setDebit = function(elm){
        	var tr = $(elm).closest('tr').find('.kredit').val(0);
        	sum();
        }

        setKredit = function(elm){
        	var tr = $(elm).closest('tr').find('.debit').val(0);
        	sum();
        }

        function sum() {
			var element_debit   = document.getElementsByClassName('debit');
			var element_kredit  = document.getElementsByClassName('kredit');
			var total_debit     = 0;
			var total_kredit    = 0;
			var validasi_debit  = 0;
			var validasi_kredit = 0;
			var nilai_debit     = 0;
			var nilai_kredit    = 0;

			//total debit
		    for (var i = 0; i < element_debit.length; i++){
		    	nilai_debit = element_debit[i].value;
		    	if (nilai_debit != undefined) {
		    		validasi_debit =  nilai_debit.replace(/[^0-9\,-]+/g, "");
		    		total_debit += parseFloat(validasi_debit);
		    	}
		    }
		    
		    if (!isNaN(total_debit)) {
		    	$('#total_debit').val(total_debit+',00');
		    }else{
		    	$('#total_debit').val('0,00');
		    }

		    //total kredit
		    for (var i = 0; i < element_kredit.length; i++){
		    	nilai_kredit = element_kredit[i].value;
		    	if (nilai_kredit != undefined) {
		    		validasi_kredit =  nilai_kredit.replace(/[^0-9\,-]+/g, "");
		    		total_kredit += parseFloat(validasi_kredit);
		    	}
		    }
		    
		    if (!isNaN(total_kredit)) {
		    	$('#total_kredit').val(total_kredit+',00');
		    }else{
		    	$('#total_kredit').val('0,00');
		    }

		    checkBalance();
		}

        function checkBalance() {
			var txtFirstNumberValue = document.getElementById('total_debit').value;
			var val1 = txtFirstNumberValue.replace(',00', "");
			var new_val1 = val1.replace(/[^0-9\,-]+/g, "");

			var txtSecondNumberValue = document.getElementById('total_kredit').value;
			var val2 = txtSecondNumberValue.replace(',00', "");
			var new_val2 = val2.replace(/[^0-9\,-]+/g, "");

			if (new_val1 != new_val2) {
				$('#btn-save').addClass('disabled');
			}else{
				$('#btn-save').removeClass('disabled');
			}
		}
        // init on load
        add_row();

    });
</script>