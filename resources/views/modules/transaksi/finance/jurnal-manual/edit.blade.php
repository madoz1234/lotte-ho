<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Jurnal Manual</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record[0]->idtrans) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record[0]->id }}">
		<div class="two fields">
			<div class="field">
				<label>Tanggal Jurnal</label>		
				<div class="ui calendar tanggal_jurnal_detail">
					<div class="ui input left icon disabled">
						<i class="calendar icon date"></i>
						<input name="tanggal" type="text" placeholder="Tanggal Selesai" value="{{ $record[0]->tanggal or "" }}">
					</div>
				</div>
			</div>
			<div class="field">
				<label>No Jurnal</label>		
				<div class="ui input disabled">
					<input name="idtrans" type="text" placeholder="Id Transaksi" value="{{ $record[0]->idtrans or "" }}">
				</div>
			</div>
			<div class="field">
				<label for="fruit">TMUK</label>
				<select name="tmuk_kode" class="ui fluid search selection dropdown disabled" style="width: 100%;">
					{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',['selected' => isset($record[0]->tmuk_kode) ? $record[0]->tmuk_kode : '',], '-- Pilih TMUK --') !!}
				</select>
			</div>
		</div>

		<table id="logsheet" class="ui celled compact red table" width="100%" cellspacing="0">
			<thead>
				<tr>
					{{-- <th class="center aligned">#</th> --}}
					<th class="center aligned">Kode Akun</th>
					<th class="center aligned">Nama Akun</th>
					<th class="center aligned">Debit</th>
					<th class="center aligned">Kredit</th>
					<th class="center aligned">Memo</th>
					{{-- <th class="center aligned">Aksi</th> --}}
				</tr>
			</thead>
			<tbody>
				<?php
					$total_debit=0;
					$total_kredit=0;
				?>
				@foreach($record as $row)
				<tr data-row="">
					<input type="hidden" name="id_jurnal[{{ $row->id }}]" value="{{ $row->id }}">
					{{-- <td class="center aligned"><input type="checkbox" name="" class="case" value=""></td> --}}
					<td>
						<div class="field">
							<div class="ui input">
								{{ $row->coa_kode }}
							</div>
						</div>
					</td>
					<td>
						<div class="field">
							<div class="ui labeled input">
								{{ $row->coa->nama }}
							</div>
						</div>
					</td>
					<td>
						<div class="field">
							<div class="ui labeled input">
								<label for="" class="ui label">Rp.</label>
								<input type="text" name="jumlah_debit[{{ $row->id }}]" class="debit" placeholder="" data-inputmask="'alias' : 'numeric'" value="{{ $row->posisi == 'D' ? $row->jumlah : 0  }}" onkeyup="setDebit(this)" readonly="">
								<?php 
									if ($row->posisi == 'D') {
										$total_debit += $row->jumlah;
									}
								?>
							</div>
						</div>
					</td>
					<td>
						<div class="field">
							<div class="ui labeled input">
								<label for="" class="ui label">Rp.</label>
								<input type="text" name="jumlah_kredit[{{ $row->id }}]" class="kredit" placeholder="" data-inputmask="'alias' : 'numeric'" value="{{ $row->posisi == 'K' ? $row->jumlah : 0  }}" onkeyup="setKredit(this)" readonly="">
								<?php 
									if ($row->posisi == 'K') {
										$total_kredit += $row->jumlah;
									}
								?>
							</div>
						</div>
					</td>
					<td>
						<div class="field">
							<div class="ui labeled input">
								{{-- <label for="" class="ui label">Rp.</label> --}}
								<input type="text" name="deskripsi_memo[{{ $row->id }}]" placeholder="" value="{{ $row->deskripsi or ""  }}" readonly="">
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th colspan="2" class="ui right aligned">Total</th>
					<th colspan="1" class="ui center aligned">
						<div class="field">
							<div class="ui labeled input">
								<label for="" class="ui label">Rp.</label>
								<input type="text" name="total_debit" id="total_debit" class="form-control" placeholder="" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="" value="{{ $total_debit }}">
							</div>
						</div>
					</th>
					<th colspan="1" class="ui center aligned">
						<div class="field">
							<div class="ui labeled input">
								<label for="" class="ui label">Rp.</label>
								<input type="text" name="total_kredit" id="total_kredit" class="form-control" placeholder="" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="" value="{{ $total_kredit }}">
							</div>
						</div>
					</th>
					<th></th>
				</tr>
			</tfoot>
		</table>
		{{--  <button type="button" onclick="add_row()" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Tambah</button> --}}
		{{-- <button type="button" style="margin-left:30px; margin-bottom: 10px" class="hapus ui mini red icon trash button"><i class="icon trash button"></i></button>
		<button type="button" onclick="add_row()"  style="margin-bottom: 10px" class="ui mini blue icon plus button"><i class="icon plus button"></i></button> --}}

	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <div class="ui positive right labeled icon save button" id="btn-save">
		Simpan
		<i class="checkmark icon"></i>
	</div> --}}
</div>

@include('layouts.scripts.inputmask')

<script type:"text/javascript">
	$(document).ready(function(){
        setDebit = function(elm){
        	var tr = $(elm).closest('tr').find('.kredit').val(0);

        	var element = document.getElementsByClassName('debit');
	    	var total = 0;
	    	var validasi = 0;
	    	var nilai = 0;

		    for (var i = 0; i < element.length; i++){
		    	nilai = element[i].value;
		    	if (nilai != undefined) {
		    		validasi =  nilai.replace(/[^0-9\,-]+/g, "");
		    		total += parseFloat(validasi);
		    	}
		    }
		    
		    if (!isNaN(total)) {
		    	$('#total_debit').val(total+',00');
		    }else{
		    	$('#total_debit').val('0,00');
		    }
		    checkBalance();
        }

        setKredit = function(elm){
        	var tr = $(elm).closest('tr').find('.debit').val(0);

        	var element = document.getElementsByClassName('kredit');
	    	var total = 0;
	    	var validasi = 0;
	    	var nilai = 0;

		    for (var i = 0; i < element.length; i++){
		    	nilai = element[i].value;
		    	if (nilai != undefined) {
		    		validasi =  nilai.replace(/[^0-9\,-]+/g, "");
		    		total += parseFloat(validasi);
		    	}
		    }
		    
		    if (!isNaN(total)) {
		    	$('#total_kredit').val(total+',00');
		    }else{
		    	$('#total_kredit').val('0,00');
		    }
		    checkBalance();
        }

        function checkBalance() {
			var txtFirstNumberValue = document.getElementById('total_debit').value;
			var val1 = txtFirstNumberValue.replace(',00', "");
			var new_val1 = val1.replace(/[^0-9\,-]+/g, "");

			var txtSecondNumberValue = document.getElementById('total_kredit').value;
			var val2 = txtSecondNumberValue.replace(',00', "");
			var new_val2 = val2.replace(/[^0-9\,-]+/g, "");

			if (new_val1 != new_val2) {
				$('#btn-save').addClass('disabled');
			}else{
				$('#btn-save').removeClass('disabled');
			}
		}
    });
</script>