<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detil Picking Task</div>
<div class="content">
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
{!! csrf_field() !!}
<input type="hidden" name="_method" value="PUT">
<input type="hidden" name="id" value="{{ $record->id }}">

	<div class="ui error message"></div>

		<div class="four fields">
			<div class="field">
				<label>Tanggal PR</label>
				<input placeholder="Inputkan" type="text" value="{{ $record->tgl_buat or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Nomor PR</label>
				<input placeholder="Inputkan" type="text" value="{{ $record->nomor or "" }}" readonly="">
			</div>
			<div class="field">
				<label>LSI</label>
				<input placeholder="Inputkan" type="text" value="{{ isset($record->tmuk->lsi->nama)?$record->tmuk->lsi->nama : "" }}" readonly="">
			</div>
			<div class="field">
				<label>TMUK</label>
				<input placeholder="Inputkan" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Notes</label>
				<input placeholder="Notes . . ." type="text" value="{{ ($record->keterangan != "") ? ($record->keterangan) : '-' }}" readonly="">
			</div>
		</div>
		<!-- List Table -->
		<div class="field">
			<table class="ui celled striped table" id="tabelDetailTask">
				<thead>
					<tr>
						<th rowspan="2" class="ui center aligned">No</th>
						<th rowspan="2" class="ui center aligned">BUMUN</th>
						<th rowspan="2" class="ui center aligned">Kategori 1</th>
						<th rowspan="2" class="ui center aligned">Kode Produk</th>
						<th rowspan="2" class="ui center aligned">Barcode</th>
						<th rowspan="2" class="ui center aligned">Nama Produk</th>
						<th colspan="2" class="ui center aligned">Order Unit Info</th>
						<th colspan="2" class="ui center aligned">PR Qty</th>
						{{-- <th rowspan="2">Picking Qty</th> --}}
					</tr>
					<tr>

						<th class="ui center aligned">Stock GMD</th>
						<th class="ui center aligned">UoM</th>
						<th class="ui center aligned">Qty UoM</th>
						<th class="ui center aligned">UoM</th>
					</tr>
				</thead>
				
				<tbody>
					@if($record->count() > 0)
						<?php $i = 1; ?>
						
						@foreach ($detail as $row)
						<tr>
							<td width="3%" class="ui center aligned">{{ $i }}</td>
							<td width="10%" class="ui aligned">{{ $row['bumun_nm'] }}</td>
							<td width="10%" class="ui aligned">{{ $row['l1_nm'] }}</td>
							<td width="10%" class="ui center aligned">{{ $row['produk_kode'] }}</td>
							<td width="10%" class="ui center aligned">{{ $row['uom1_barcode'] }}</td>
							<td width="25%" class="ui aligned">{{ $row['nama'] }}</td>
							<td width="8%" class="ui center aligned">{{ $row['stok_gmd'] }}</td>
							<td width="8%" class="ui center aligned">{{ $row['unit_pr'] }}</td>
							<td width="8%" class="ui center aligned">{{ $row['qty_pr'] }}</td>
							<td width="8%" class="ui center aligned">{{ $row['unit_pr'] }}</td>
							{{-- <td><input type="number" style="background-color: #edf1f6;" name="qty_pr[{{ $row['id_detail'] }}]" value="{{ $row['qty_pr'] }}"></td> --}}
							{{-- {{ ($record->uom2_file != "") ? url('storage/'.$record->uom2_file) : asset('img/no_image.png') }} --}}
						</tr>
						<?php $i++;	?>
						@endforeach
					@else
						<tr>
							<td colspan="11">
								<center><i>Maaf tidak ada data</i></center>
							</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
		<!-- List Table -->
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <a class="ui default right labeled icon save button" target="_blank" href="{{ url($pageUrl) }}/picking-task/'.$record->id.'">
		Print
		<i class="print icon"></i>
	</a> --}}
	{{-- <a target="_blank" href="list-picking/picking-task/'.$record->id.'" class="ui mini default icon print button" data-content="Print Picking"><i class="print icon"></i> Print</a> --}}
</div>