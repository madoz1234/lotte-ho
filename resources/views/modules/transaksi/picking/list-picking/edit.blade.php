<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Picking Result</div>
<div class="content">
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
{!! csrf_field() !!}
<input type="hidden" name="_method" value="PUT">
<input type="hidden" name="id" value="{{ $record->id }}">
<input type="hidden" name="lsi_kode" value="{{ $record->tmuk->lsi->kode }}">

	<div class="ui error message"></div>

		<div class="four fields">
			<div class="field">
				<label>Tanggal PR</label>
				<input placeholder="Inputkan" type="text" value="{{ $record->tgl_buat or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Nomor PR</label>
				<input name="nomor" placeholder="Inputkan" type="text" value="{{ $record->nomor or "" }}" readonly="">
			</div>
			<div class="field">
				<label>LSI</label>
				<input placeholder="Inputkan" type="text" value="{{ isset($record->tmuk->lsi->nama)?$record->tmuk->lsi->nama : "" }}" readonly="">
			</div>
			<div class="field">
				<label>TMUK</label>
				<input placeholder="TMUK . . ." type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Notes</label>
				<input placeholder="Notes . . ." type="text" value="{{ ($record->keterangan != "") ? ($record->keterangan) : '-' }}" readonly="">
			</div>
		</div>

		<!-- List Table -->
		<div class="field">
			{{-- $dt = $record->tanggal;
echo $dt->diffInDays(Carbon::now()); --}}
{{-- {{ dd($diff) }} --}}
			<table class="ui celled striped table" id="tabelDetailResult">
				<thead>
					<tr>
						<th rowspan="2" class="ui center aligned">No</th>
						<th rowspan="2" class="ui center aligned">BUMUN</th>
						<th rowspan="2" class="ui center aligned">Kategori 1</th>
						<th rowspan="2" class="ui center aligned">Kode Produk</th>
						<th rowspan="2" class="ui center aligned">Barcode</th>
						<th rowspan="2" class="ui center aligned">Nama Produk</th>
						<th colspan="2" class="ui center aligned">Order Unit Info</th>
						<th colspan="2" class="ui center aligned">PR Qty</th>
						<th rowspan="2" class="ui center aligned">Picking Qty</th>
					</tr>
					<tr>

						<th class="ui center aligned">Stock GMD</th>
						<th class="ui center aligned">UoM</th>
						<th class="ui center aligned">Qty UoM</th>
						<th class="ui center aligned">UoM</th>
					</tr>
				</thead>
				
				<tbody>
					@if($record->count() > 0)
						<?php $i = 0; ?>
						
						@foreach ($detail as $row)
						<tr>
							<td class="ui center aligned">{{ $i+1 }}</td>
							<td class="aligned">{{ $row['bumun_nm'] }}</td>
							<td class="aligned">{{ $row['l1_nm'] }}</td>
							<td class="ui center aligned">{{ $row['produk_kode'] }}</td>
							<td class="ui center aligned">{{ $row['uom1_barcode'] }}</td>
							<td class="aligned">{{ $row['nama'] }}</td>
							<td class="ui center aligned">{{ $row['stok_gmd'] }}</td>
							<td class="ui center aligned">{{ $row['unit_pr'] }}</td>
							<td class="ui center aligned">{{ $row['qty_pr'] }}</td>
							<td class="ui center aligned">{{ $row['unit_pr'] }}</td>
							<td class="ui center aligned">
								<input type="hidden" name="produk[kode][{{ $i }}]" value="{{ $row['produk_kode'] }}">
								<input type="hidden" name="produk[id_detail][{{ $row['produk_kode'] }}]" value="{{ $row['id_detail'] }}">
								<input type="hidden" class="qty-pr" name="produk[qty_pr][{{ $row['produk_kode'] }}]" value="{{ $row['qty_pr'] }}">
								<input type="hidden" name="produk[qty_order][{{ $row['produk_kode'] }}]" value="{{ $row['qty_order'] }}">
								<input type="hidden" name="produk[qty_aktual][{{ $row['produk_kode'] }}]" value="{{ $row['qty_aktual'] }}">
								<div class="ui fluid left action input">
									<button class="ui teal icon button btn-balance">
									  <i class="balance scale icon"></i>
									</button>
									<input type="text" class="qty-pick" name="produk[qty_pick][{{ $row['produk_kode'] }}]" 
									value="{{ (!is_null($row['qty_pick'])) ? $row['qty_pick'] : $row['qty_pr'] }}" onkeypress="return isNumberKey(event)" style="background-color: #edf1f6;" onkeyup="validate(this)">
								</div>
							</td>
						</tr>
						<?php $i++;	?>
						@endforeach
					@else
						<tr>
							<td colspan="11">
								<center><i>Maaf tidak ada data</i></center>
							</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
		<!-- List Table -->
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button btn-submit">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	validate = function(elm)
	{
        var qty_pr = $(elm).closest('tr').find('.qty-pr').val();
        var qty_pick = $(elm).closest('tr').find('.qty-pick').val();
        // console.log(qty_pick);
        if ((qty_pick[0] == 0) && (qty_pick[1] != null)) {
        	swal(
				'Oops!',
				'Inputan tidak sesuai',
				'warning'
			).then((result) => {
				$('.btn-submit').addClass('disabled');
			})
        }
        if (parseInt(qty_pick) > parseInt(qty_pr)) {
        	swal(
				'Oops!',
				'Tidak Boleh Melebihi Qty PR',
				'warning'
			).then((result) => {
				$(elm).closest('tr').find('.qty-pick').val(qty_pr);
				// $('.btn-submit').addClass('disabled');
			})
        }else if(qty_pick == ''){
			$('.btn-submit').addClass('disabled');
        }else{
			$('.btn-submit').removeClass('disabled');
        }
    }

    $(document).on('click', '.btn-balance', function(e){
		var _this = $(this);
		var nomor = _this.closest('tr').find('.qty-pr').val();
		
		if (nomor != null) {
			_this.closest('tr').find('.qty-pick').val(nomor);
			validate(_this);
		}
	});
</script>