@extends('layouts.scaffold')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.semanticui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.semanticui.min.js') }}"></script>
    <script src="{{ asset('plugins/sweetalert/sweetalert2.js') }}"></script>
@append

@section('scripts')
	<script type="text/javascript">
		// global
	    var dt = "";
	    var formRules = [];
	    var initModal = function(){
	    	return false;
	    };

	    $.fn.form.settings.prompt = {
		    empty                : '{name} tidak boleh kosong',
		    checked              : '{name} harus dipilih',
		    email                : '{name} tidak valid',
		    url                  : '{name} must be a valid url',
		    regExp               : '{name} is not formatted correctly',
		    integer              : '{name} must be an integer',
		    decimal              : '{name} must be a decimal number',
		    number               : '{name} hanya boleh berisikan angka',
		    is                   : '{name} must be "{ruleValue}"',
		    isExactly            : '{name} must be exactly "{ruleValue}"',
		    not                  : '{name} cannot be set to "{ruleValue}"',
		    notExactly           : '{name} cannot be set to exactly "{ruleValue}"',
		    contain              : '{name} cannot contain "{ruleValue}"',
		    containExactly       : '{name} cannot contain exactly "{ruleValue}"',
		    doesntContain        : '{name} must contain  "{ruleValue}"',
		    doesntContainExactly : '{name} must contain exactly "{ruleValue}"',
		    minLength            : '{name} must be at least {ruleValue} characters',
		    length               : '{name} must be at least {ruleValue} characters',
		    exactLength          : '{name} must be exactly {ruleValue} characters',
		    maxLength            : '{name} cannot be longer than {ruleValue} characters',
		    match                : '{name} must match {ruleValue} field',
		    different            : '{name} must have a different value than {ruleValue} field',
		    creditCard           : '{name} must be a valid credit card number',
		    minCount             : '{name} must have at least {ruleValue} choices',
		    exactCount           : '{name} must have exactly {ruleValue} choices',
			maxCount 			 : '{name} must have {ruleValue} or less choices'
		};

	</script>
	@yield('rules')
	@yield('init-modal')
	
	@include('layouts.scripts.datatable')
	@include('layouts.scripts.actions')
@append

@section('content')
	@section('content-header')
	<div class="ui breadcrumb">
		<?php $i=1; $last=count($breadcrumb);?>
		@foreach ($breadcrumb as $name => $link)
			@if($i++ != $last)
				<a href="{{ $link }}" class="section">{{ $name }}</a>
				<i class="right chevron icon divider"></i>
			@else
				<div class="active section">{{ $name }}</div>
			@endif
		@endforeach
	</div>
	<h2 class="ui header">
	  <div class="content">
	    {!! $title or '-' !!}
	    <div class="sub header">{!! $subtitle or ' ' !!}</div>
	  </div>
	</h2>
	@show

	@section('content-body')
	<div class="ui grid">
		<div class="sixteen wide column main-content">
		    <div class="ui segments">
				<div class="ui segment">
					<form class="ui filter form">
		  				<div class="fields">
							@section('filters')
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input type="hidden" name="filter[region]">
										<i class="dropdown icon"></i>
										<div class="default text">No Surat Jalan</div>
										<div class="menu">
											<div class="item" data-value="1">lt-21212579765</div>
											<div class="item" data-value="2">lt-12124697544</div>
											<div class="item" data-value="3">lt-12124567865</div>
											<div class="item" data-value="4">lt-12134556654</div>
										</div>
									</div>
								</div>
								<button type="button" class="ui teal icon filter button" data-content="Cari Data">
									<i class="search icon"></i>
								</button>
								<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
									<i class="refresh icon"></i>
								</button>
							@show
							<div style="margin-left: auto; margin-right: 1px;">
								@section('toolbars')
								{{-- <button type="button" class="ui blue add button">
									<i class="plus icon"></i>
									Tambah Data
								</button>
								<button type="button" class="ui green button">
									<i class="file excel outline icon"></i>
									Export Excel
								</button> --}}
								@show
							</div>
						</div>
					</form>

					@section('subcontent')
						@if(isset($tableStruct))
			            <table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
			                <thead>
			                    <tr>
			                        @foreach ($tableStruct as $struct)
			                            <th>{{ $struct['label'] or $struct['name'] }}</th>
			                        @endforeach
			                    </tr>
			                </thead>
			                <tbody>
			                	<tr>
			                		<td>1</td>
			                		<td>lt - 12212123536753</td>
			                		<td>Pragma Informatika</td>
			                		<td>Dikirim</td>
			                		<td><button type="button" class="ui mini blue icon edit button" data-content="aksi" data-id="1"><i>Aksi?</i></button></td>
			                	</tr>
			                </tbody>
			            </table>
			            @endif
		            @show
		        </div>
		    </div>
		</div>
	</div>
	@show
@endsection

@section('modals')
<div class="ui {{ $modalSize or 'mini' }} modal" id="formModal">
	<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
	</div>
</div>
@append