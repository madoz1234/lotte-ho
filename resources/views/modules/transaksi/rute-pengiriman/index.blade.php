@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')

<div class="field">
	<input name="filter[surat_jalan]" placeholder="No Surat jalan" type="text">
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>

<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>

@endsection

@section('js-filters')
d.surat = $("input[name='filter[surat_jalan]']").val();
@endsection

@section('toolbars')
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		// validasi
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).on('click', '.print.button', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/print/"+id;

			window.open(
			  url,
			  '_blank' // <- This is what makes it open in a new window.
			);

			// window.location = url;
		});
	</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

		$('#tabelKontainer').DataTable({
			pageLength: 5,
			filter: false,
			lengthChange: false,
			filter: false,
			ordering:false,
		})
	};
</script>
@endsection

{{-- @section('tableBody')
<tr>
	<td style="text-align: center;">1</td>
	<td style="text-align: center;">29/01/18</td>
	<td style="text-align: center;">PO-1707070600700017411</td>
	<td style="text-align: left;">0600700018 - Pragma Informatika</td>
	<td style="text-align: center;">2</td>
	<td style="text-align: center;">2</td>
	<td style="text-align: center;">
		<button type="button" class="ui mini orange icon edit button" data-content="Detil Kontainer" data-id="1"><i class="edit icon"></i> Detil</button>
		<a target="_blank" href="{{ url($pageUrl) }}/print-label-kontainer" class="ui mini default icon print button" data-content="Print Detil Kontainer" data-id="1"><i class="print icon"></i> Print</a>
	</td>
</tr>
@endsection --}}