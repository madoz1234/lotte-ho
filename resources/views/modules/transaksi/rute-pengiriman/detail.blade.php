<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Rute Pengiriman</div>
<div class="content">
	<h4>Nomor Surat Jalan : {{$record->nomor}}</h4>
	<div class="ui threaded comments">
		<h3 class="ui dividing header"></h3>
		<?php $jum =  $record->muatan->detail->groupBy('tmuk_kode')->count(); $count = 1;?>
		@foreach($record->muatan->detail->groupBy('tmuk_kode') as $detail)
		<div class="comment">
			<a class="avatar">
				<img src="{{ asset('img/black.png') }}">
			</a>
			<div class="content">
				<a class="author">{{ $detail->first()->tmuk->nama }}</a>
				<div class="metadata">
					<span class="date">{{ $detail->first()->tmuk->telepon }}</span>
				</div>
				<div class="text">
					<p>{{ $detail->first()->tmuk->alamat }}</p>
				</div>
			</div>
			@if($jum>1 && $count!=$jum)
			<div class="comments">
				
			</div>
			@endif
			<?php $count++; ?>
		</div>
		@endforeach
	</div>
	
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui default right labeled icon print button" data-id="{{ $record->id }}">
		Print
		<i class="print icon"></i>
	</div>
</div>