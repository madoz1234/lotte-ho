<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Konfirmasi Retur</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<!-- Filter -->
		<div class="fields">
			<div class="ui grid">
				<div class="field">
					<input name="filter[lsi]" placeholder="LSI" type="text">
				</div>

				<div class="field">
					<input name="filter[tmuk]" placeholder="TMUK" type="text">
				</div>

				<div class="field">
					<input name="filter[nomor_rr]" placeholder="Nomor RR" type="text">
				</div>

				<div class="field">
					<input name="filter[tgl_rr]" placeholder="Tgl RR" type="text">
				</div>


				<div class="field">
					<input name="filter[nomor_pr]" placeholder="Nomor PR" type="text">
				</div>
				<button type="button" class="ui teal icon filter button" data-content="Cari Data">
					<i class="search icon"></i>
				</button>
				<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
					<i class="refresh icon"></i>
				</button>
			</div>
		</div>
		<!-- Filter -->
		<br>
		<!-- List Table -->
		<div class="field">

			<table class="ui celled striped table">
				<thead>
					<tr align="center">
						<th rowspan="2">No</th>
						<th rowspan="2">Kode Produk</th>
						<th rowspan="2">Barcode</th>
						<th rowspan="2">Nama Produk</th>
						<th colspan="2">PR Qty</th>
						<th rowspan="2">Harga (Rp)</th>
						<th rowspan="2">Total Harga (Rp)</th>
						<th rowspan="2">Alasan</th>
						<th colspan="2">PR Qty Disetujui</th>
					</tr>
					<tr>

						<th>Qty</th>
						<th>Unit</th>
						<th>Qty</th>
						<th>Unit</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>1052362000</td>
						<td>089686917335</td>
						<td>SARIMI 2 SOTO AYAM 24PCS/CT</td>
						<td>1</td>
						<td>1</td>
						<td>Rp.2000,00</td>
						<td>Rp.2000,00</td>
						<td>Barang Bagus</td>
						<td>1</td>
						<td>1</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- List Table -->
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	<div class="ui default right labeled icon save button">
		Print
		<i class="print icon"></i>
	</div>
</div>