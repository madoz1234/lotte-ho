<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detil Picking Result</div>
<div class="content">
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
{!! csrf_field() !!}
<input type="hidden" name="_method" value="PUT">
<input type="hidden" name="id" value="{{ $record->id }}">

	<div class="ui error message"></div>

		<div class="four fields">
			<div class="field">
				<label>Tanggal PR</label>
				<input placeholder="Inputkan" type="text" value="{{ $record->tgl_buat or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Nomor PR</label>
				<input placeholder="Inputkan" type="text" value="{{ $record->nomor or "" }}" readonly="">
			</div>
			<div class="field">
				<label>LSI</label>
				<input placeholder="Inputkan" type="text" value="{{ isset($record->tmuk->lsi->nama)?$record->tmuk->lsi->nama : "" }}" readonly="">
			</div>
			<div class="field">
				<label>TMUK</label>
				<input placeholder="Inputkan" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div>
		</div>
		<!-- List Table -->
		<div class="field">

			<table class="ui celled striped table" id="tabelDetailResult">
				<thead>
					<tr>
						<th rowspan="2" class="ui center aligned">No</th>
						<th rowspan="2" class="ui center aligned">BUMUN</th>
						<th rowspan="2" class="ui center aligned">Kategori 1</th>
						<th rowspan="2" class="ui center aligned">Kode Produk</th>
						<th rowspan="2" class="ui center aligned">Barcode</th>
						<th rowspan="2" class="ui center aligned">Nama Produk</th>
						<th colspan="2" class="ui center aligned">Order Unit Info</th>
						<th colspan="2" class="ui center aligned">Picking Request</th>
						<th colspan="2" class="ui center aligned">Picking Result</th>
						{{-- <th rowspan="2">Picking Qty</th> --}}
					</tr>
					<tr>

						<th class="ui center aligned">Stock Gmd</th>
						<th class="ui center aligned">UoM</th>
						<th class="ui center aligned">Qty</th>
						<th class="ui center aligned">Unit</th>
						<th class="ui center aligned">Qty</th>
						<th class="ui center aligned">Unit</th>
					</tr>
				</thead>
				
				<tbody>
					@if($record->count() > 0)
						<?php $i = 1; ?>
						
						@foreach ($detail as $row)
						<tr>
							<td class="ui center aligned">{{ $i }}</td>
							<td class="ui left aligned">{{ $row['bumun_nm'] }}</td>
							<td class="ui left aligned">{{ $row['l1_nm'] }}</td>
							<td class="ui center aligned">{{ $row['produk_kode'] }}</td>
							<td class="ui center aligned">{{ $row['uom1_barcode'] }}</td>
							<td class="ui left aligned">{{ $row['nama'] }}</td>
							<td class="ui center aligned">{{ $row['stok_gmd'] }}</td>
							<td class="ui center aligned">{{ ($row['unit_sell'] != "") ? ($row['unit_sell']) : 'Pcs' }}</td>
							<td class="ui center aligned">{{ $row['qty_pr'] }}</td>
							<td class="ui center aligned">{{ ($row['unit_pr'] != "") ? ($row['unit_pr']) : 'Pcs' }}</td>
							<td class="ui center aligned">{{ $row['qty_pick'] }}</td>
							<td class="ui center aligned">{{ ($row['unit_pr'] != "") ? ($row['unit_pr']) : 'Pcs' }}</td>
							{{-- <td><input type="number" style="background-color: #edf1f6;" name="qty_pr[{{ $row['id_detail'] }}]" value="{{ $row['qty_pr'] }}"></td> --}}
						</tr>
						<?php $i++;	?>
						@endforeach
					@else
						<tr>
							<td colspan="11">
								<center><i>Maaf tidak ada data</i></center>
							</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
		<!-- List Table -->
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <a class="ui default right labeled icon save button" target="_blank" href="{{ url($pageUrl) }}/picking-task/'.$record->id.'">
		Print
		<i class="print icon"></i>
	</a> --}}
	{{-- <a target="_blank" href="list-picking/picking-task/'.$record->id.'" class="ui mini default icon print button" data-content="Print Picking"><i class="print icon"></i> Print</a> --}}
</div>