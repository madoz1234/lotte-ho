<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat PO (BTDK) dari PR</div>
					
<div class="content">

	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>Tanggal PR :</label>
				<input name="tgl_buat" placeholder="Inputkan Tanggal PYR" type="text" value="{{ $record->tgl_buat or "" }}" readonly="">
			</div>
			<div class="field">
				<label>LSI :</label>
				<input placeholder="Inputkan Vendor Non Lotte" type="text" value="{{ isset($record->tmuk->lsi->nama)?$record->tmuk->lsi->nama : "" }}" readonly="">
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label>Nomor PR :</label>
				<input name="nomor_pr" placeholder="Inputkan Nomor PYR" type="text" value="{{ $record->nomor or "" }}" readonly="">
			</div>
			<div class="field">
				<label>TMUK :</label>
				<input placeholder="Inputkan TMUK" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
				<input name="tmuk_kode" placeholder="Inputkan TMUK" type="hidden" value="{{ $record->tmuk_kode or "" }}" readonly="">
			</div>

			<div class="field">
				<label>Alamat Pengiriman :</label>
				<input placeholder="Inputkan Alamat Pengiriman" type="text" value="{{ $record->tmuk->alamat or "" }}" readonly="">
			</div>
		</div>
		<table id="tabelPo" class="ui celled compact red table trace_value" width="100%" cellspacing="0">
            <thead>
                <tr>
                	<th rowspan="2" class="ui center aligned">No</th>
                	<th rowspan="2" class="ui center aligned">Kode Produk</th>
                	<th rowspan="2" class="ui center aligned">Nama Produk</th>
                	<th rowspan="2" class="ui center aligned">Barcode</th>
                	<th colspan="2" class="ui center aligned">Order Unit Info</th>
                	<th colspan="2" class="ui center aligned">Selling Unit Info</th>
                	<th colspan="2" class="ui center aligned">PR QTY</th>
                	<th colspan="2" class="ui center aligned">PO QTY</th>
                	<th rowspan="2" class="ui center aligned">Harga Estimasi (Rp)</th>
                	<th class="sum" rowspan="2" class="ui center aligned">Harga Aktual (Rp)</th>
                	<th rowspan="2" class="ui center aligned">Total Estimasi (Rp)</th>
                	<th class="sum" rowspan="2" class="ui center aligned">Total Aktual (Rp)</th>
                	<th rowspan="2" class="ui center aligned">Aksi</th>
                </tr>
                <tr>
                	<th class="center aligned">Stock GMD</th>
                	<th class="center aligned">UOM</th>
                	<th class="center aligned">Qty/UOM</th>
                	<th class="center aligned">UOM</th>
                	<th class="center aligned">Qty</th>
                	<th class="center aligned">Unit</th>
                	<th class="center aligned">Qty</th>
                	<th class="center aligned">Unit</th>
                </tr>
            </thead>
            <tbody>
            @if($detail->count() > 0)
				<?php $i = 1; $all_total_estimasi = 0; ?>
		
				@foreach ($detail as $row)
				
            	<tr>
					<td class="ui center aligned">{{ $i }}</td>
					<td class="ui right aligned">{{ $row['produk_kode'] }}</td>
					<td class="ui aligned">{{ $row['nama'] }}</td>
					<td class="ui aligned">{{ $row['uom1_barcode'] }}</td>
					<td class="ui center aligned">{{ $row['stok_gmd'] }}</td>
					<td class="ui center aligned">{{ ($row['unit_order'] != "") ? ($row['unit_order']) : 'Pcs' }}</td>
					<td class="ui center aligned">{{ $row['qty_sell'] }}</td>
					<td class="ui center aligned">{{ ($row['unit_sell'] != "") ? ($row['unit_sell']) : 'Pcs' }}</td>
					<td class="ui right aligned">{{ $row['qty_pr'] }}</td>
					<td class="ui center aligned">{{ ($row['unit_pr'] != "") ? ($row['unit_pr']) : 'Pcs' }}</td>
					<td class="ui center aligned">
						<div class="ui fluid input disabled" id='qty-{{ $i }}'>
							<input type="hidden" name="detail_id[]" value="{{ $row['id_detail'] }}">
							<input type="hidden" name="qty_from_pr[]" value="{{ $row['qty_from_pr'] }}" id="qty_from_pr_{{$i}}">
							<input type="text" name="detail_qty_po[]" value="{{ $row['qty_po'] }}" id="qty_{{$i}}" onkeyup="calculate({{$i}});" onkeypress="return isNumberKey(event)">
						</div>
					</td>
					<td class="ui center aligned">{{ ($row['unit_po'] != "") ? ($row['unit_po']) : 'Pcs' }}</td>
					<td class="ui right aligned">
						<div class="ui fluid input disabled">
							<input name="harga_estimasi[]" id='harga_estimasi_{{ $i }}' type="text" value="{{ $row['harga_estimasi'] }}" data-inputmask="'alias' : 'numeric'">
						</div>
					
					</td>
					<td class="ui center aligned">
						<div class="ui fluid input disabled" id='input-price-{{ $i }}'>
							<?php 
								if ($row['qty_po'] != 0) {
									$price_pcs = $row['harga_aktual'] / $row['qty_po'];
								}else{
									$price_pcs = 0;
								}
							?>
							<input name="detail_price_pcs[]" id='detail_price_pcs_{{ $i }}' type="text" value="{{ FormatNumber($price_pcs) }}" data-inputmask="'alias' : 'numeric'" onkeyup="calculate({{$i}});">
						</div>
					</td>
					<td class="ui center aligned">
						<div class="ui fluid input disabled">
							<?php 
								if ($row['qty_po'] != 0) {
									$total_estimasi = $row['harga_estimasi'] * $row['qty_po'];
								}else{
									$total_estimasi = 0;
								}
								$all_total_estimasi += $total_estimasi;
							?>
							<input name="total_estimasi[]" id="total_estimasi_{{$i}}" type="text" value="{{ FormatNumber($total_estimasi) }}" data-inputmask="'alias' : 'numeric'">
						</div>
					</td>
					<td class="ui center aligned">
						<div class="ui fluid input disabled">
							<input name="detail_price[]" id='detail_price_{{ $i }}' type="text" value="{{ $row['harga_aktual'] }}" data-inputmask="'alias' : 'numeric'" onkeyup="sum();">
						</div>
					</td>
					<td class="ui center aligned">
						<button type="button" class="ui mini blue icon button" id="btn-edit-{{$i}}" onclick="editable({{$i}})"><i class="edit icon"></i></button>
						<button type="button" class="ui mini green icon button btn-save" id="btn-save-{{$i}}" onclick="uneditable({{$i}})"><i class="checkmark icon"></i></button>
					</td>
				</tr>
				<?php $i++;	?>
				@endforeach
			@else
				<tr>
					<td colspan="11">
						<center><i>Maaf tidak ada data</i></center>
					</td>
				</tr>
			@endif
            </tbody>
            <tfoot>
			   <tr>
			   	 <th colspan="13" style="background-color: white; border-bottom-color: white"></th>
			     <th class="ui center aligned">Total (Rp)</th>
			     <th class="ui right aligned" colspan="1">
			     	<div class="ui fluid input">
						<input id="all_total_estimasi" type="text" value="{{ FormatNumber($all_total_estimasi) }}" data-inputmask="'alias' : 'numeric'" disabled="">
					</div>
			     </th>
			     <th class="ui right aligned" colspan="1">
			     	<div class="ui fluid input">
						<input id="total_price" type="text" value="{{ $record->total }}" data-inputmask="'alias' : 'numeric'" disabled="">
					</div>
			     </th>
			     <th class="ui right aligned" colspan="1">&nbsp;</th>
			   </tr>
			 </tfoot>
        </table>
	</form>

	
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button" id="btn-submit">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

@include('layouts.scripts.inputmask')

<script type="text/javascript">
	$('.btn-save').hide();

	function calculate(id) {
		var qty_from_pr = $('#qty_from_pr_'+id).val();

		var harga_estimasi = $('#harga_estimasi_'+id).val();
		var validasi_estimasi = harga_estimasi.replace(',00', "");
		var new_harga_estimasi = validasi_estimasi.replace(/[^0-9\,-]+/g, "");

		var qty = $('#qty_'+id).val();
		var validasi = qty.replace(',00', "");
		var new_qty = validasi.replace(/[^0-9\,-]+/g, "");

		var price = $('#detail_price_pcs_'+id).val();
		var validasi = price.replace(',00', "");
		var new_price = validasi.replace(/[^0-9\,-]+/g, "");

		if (new_price == 0) {
			new_price = new_harga_estimasi;
			document.getElementById('detail_price_pcs_'+id).value = new_price;
		}

		if (parseInt(new_qty) > parseInt(qty_from_pr)) {
        	swal(
				'Oops!',
				'Tidak Boleh Melebihi Qty PR',
				'warning'
			).then((result) => {
				$('#btn-submit').addClass('disabled');
				$('#btn-save-'+id).addClass('disabled');
			})
        }else{
			$('#btn-save-'+id).removeClass('disabled');
        }

		var total_estimasi = parseFloat(new_harga_estimasi) * parseFloat(new_qty);
		var total_aktual = parseFloat(new_price) * parseFloat(new_qty);

		document.getElementById('total_estimasi_'+id).value = total_estimasi;
		document.getElementById('detail_price_'+id).value = total_aktual;
		
		sum();
	}

	function sum() {
		var count = {{$i}}-1;
		var sum_aktual = 0;
		var sum_estimasi = 0;

		for (var i = 1; i <= count; i++) {
			var val = document.getElementById('detail_price_'+i).value;
			var val2 = val.replace(',00', "");
			var new_val = val2.replace(/[^0-9\,-]+/g, "");
			sum_aktual += parseFloat(new_val);

			var val = document.getElementById('total_estimasi_'+i).value;
			var val2 = val.replace(',00', "");
			var new_val = val2.replace(/[^0-9\,-]+/g, "");
			sum_estimasi += parseFloat(new_val);
		}

		document.getElementById('all_total_estimasi').value = sum_estimasi+',00';
		document.getElementById('total_price').value = sum_aktual+',00';

		if (sum_aktual == 0) {
			$('#btn-submit').addClass('disabled');
			swal(
				'Oops!',
				'Total (Rp) tidak boleh 0.',
				'warning'
			).then(function(e){
				dt.draw();
			});
		}else{
			$('#btn-submit').removeClass('disabled');
		}
	}

	function editable(id) {
		// $('#input-price-'+id).removeClass('disabled');
		$('#qty-'+id).removeClass('disabled');
		$('#btn-edit-'+id).hide();
		$('#btn-save-'+id).show();
	}

	function uneditable(id) {
		// $('#input-price-'+id).addClass('disabled');
		$('#qty-'+id).addClass('disabled');
		$('#btn-edit-'+id).show();
		$('#btn-save-'+id).hide();
	}
</script>