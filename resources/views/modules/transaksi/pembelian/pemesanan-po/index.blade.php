@extends('layouts.grid', ['type'=>'autorefresh'])

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field">		
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tgl_buat]" type="text" placeholder="Tanggal PR">
		</div>
	</div>
</div>
<div class="field">
	<input name="filter[nomor_pr]" placeholder="Nomor PR" type="text">
</div>

<div class="field">
	<select name="filter[tmuk_kode]" class="ui search dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'TMUK') !!}
	</select>
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>

<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>

@endsection

@section('js-filters')
    d.tgl_buat = $("input[name='filter[tgl_buat]']").val();
    d.nomor_pr = $("input[name='filter[nomor_pr]']").val();
    d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
@endsection

@section('toolbars')
	
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	tgl_buat: {
		identifier: 'tgl_buat',
		rules: [{
			type   : 'empty',
			prompt : 'Tanggal Buat Harus Terisi'
		}]
	},

	nomor_pr: {
		identifier: 'nomor_pr',
		rules: [{
			type   : 'empty',
			prompt : 'Nomor PR Harus Terisi'
		}]
	},

	tmuk_kode: {
		identifier: 'tmuk_kode',
		rules: [{
			type   : 'empty',
			prompt : 'TMUK Harus Terisi'
		}]
	},
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

		$('#tabelDetailResult').DataTable({
			paging: false,
			filter: false,
			lengthChange: false,
			ordering:false,
			scrollX: true,
			scrollY: 300,
		});

		$('#tabelPo').DataTable({
			paging: false,
			filter: false,
			lengthChange: false,
			ordering:false,
			scrollX: true,
			scrollY: 300,
		});
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});
	</script>
@append