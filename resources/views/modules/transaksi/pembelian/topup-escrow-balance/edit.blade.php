<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Top Up</div>
					
<div class="content">

{{-- <form class="ui filter form">
	<div class="fields">
		@section('filters')
			<div class="field">
				<input name="filter[kode]" placeholder="Kode Area" type="text">
			</div>
			<div class="field">
				<input name="filter[area]" placeholder="Area" type="text">
			</div>
			<button type="button" class="ui teal icon filter button" data-content="Cari Data">
				<i class="search icon"></i>
			</button>
			<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
				<i class="refresh icon"></i>
			</button>
		@show
	</div>
</form> --}}

	{{-- <form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="field">
			<div class="field">
				<label>Kode TMUK :</label>
				<input name="" placeholder="Inputkan Tanggal PYR" type="text" value="20182501777">
			</div>
		</div>
		<div class="field">
			<div class="field">
				<label>Nama TMUK :</label>
				<input name="" placeholder="Inputkan TMUK" type="text" value="Pragma Informatika">
			</div>
		</div>
		<div class="field">
			<div class="field">
				<label>Tanggal Top Up :</label>
				<input name="" placeholder="Inputkan Saldo Escrow" type="text" value="25/01/2018">
			</div>
		</div>
		<div class="field">
			<div class="field">
				<label>Jumlah Top Up (Rp) </label>
				<input name="" placeholder="Inputkan Nomor PYR" type="text" value="1.000.000">
			</div>
		</div>
	</form> --}}

				<div class="ui form">

				  <div class="inline fields">
				    <label class="four wide field">Nama TMUK</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan TMUK" type="text" value="133113 - Pragma Informatika">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">Tanggal Top Up</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan Tanggal Top Up" type="text">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">Jumlah Top Up (Rp)</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan Jumalah Top Up" type="text">
				    </div>
				  </div>
				</div>
	
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	<div class="ui default right labeled icon save button disabled">
		Print
		<i class="print icon"></i>
	</div>
</div>