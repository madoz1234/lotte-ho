@extends('layouts.grid', ['type'=>'autorefresh'])

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')

<div class="field">
	<select name="filter[tmuk_kode]" class="ui search dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'TMUK') !!}
	</select>
</div>
<div class="field">		
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tanggal]" type="text" placeholder="Tanggal Top Up">
		</div>
	</div>
</div>



<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>

<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>

@endsection

@section('js-filters')
d.tanggal = $("input[name='filter[tanggal]']").val();
d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
@endsection

@section('toolbars')

@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tgl_buat: {
			identifier: 'tgl_buat',
			rules: [{
				type   : 'empty',
				prompt : 'Tanggal Buat Harus Terisi'
			}]
		},

		nomor_pr: {
			identifier: 'nomor_pr',
			rules: [{
				type   : 'empty',
				prompt : 'Nomor PR Harus Terisi'
			}]
		},

		tmuk_kode: {
			identifier: 'tmuk_kode',
			rules: [{
				type   : 'empty',
				prompt : 'TMUK Harus Terisi'
			}]
		},
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
			type: 'date'
		});

		$('#listTableDetail').DataTable({
			pageLength: 5,
			filter: false,
			lengthChange: false,
			filter: false,
			ordering:false,
		});
	};
</script>
@endsection

@section('scripts')
<script type="text/javascript">
	$('.ui.calendar').calendar({
		type: 'date'
	});
	$(document).on('click', '.verify.button', function(e){
		var id = $(this).data('id');

		swal({
			title: 'Apakah anda yakin?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Verify',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/verify/'+id,
					type: 'POST',
					data: {_token: "{{ csrf_token() }}", _method: "get"},
					success: function(resp){
						swal(
							'Terverifikasi!',
							'Data berhasil diverify.',
							'success'
							).then(function(e){
								dt.draw();
							});
						},
						error : function(resp){
							swal(
								'Gagal!',
								'Data gagal diverify.',
								'error'
								).then(function(e){
									dt.draw();
								});
							}
						});
			}
		})
	});

	$(document).on('click', '.tolak.button', function(e){
		var id = $(this).data('id');

		swal({
			title: 'Apakah anda yakin?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Tolak',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/tolak/'+id,
					type: 'POST',
					data: {_token: "{{ csrf_token() }}", _method: "get"},
					success: function(resp){
						swal(
							'Ditolak!',
							'Data berhasil ditolak.',
							'success'
							).then(function(e){
								dt.draw();
							});
						},
						error : function(resp){
							swal(
								'Gagal!',
								'Data gagal ditolak.',
								'error'
								).then(function(e){
									dt.draw();
								});
							}
						});
			}
		})
	});
</script>
@endsection