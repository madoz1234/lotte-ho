<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Riwayat Top Up</div>
					
<div class="content">

{{-- <form class="ui filter form">
	<div class="fields">
		@section('filters')
			<div class="field">
				<input name="filter[kode]" placeholder="Kode Area" type="text">
			</div>
			<div class="field">
				<input name="filter[area]" placeholder="Area" type="text">
			</div>
			<button type="button" class="ui teal icon filter button" data-content="Cari Data">
				<i class="search icon"></i>
			</button>
			<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
				<i class="refresh icon"></i>
			</button>
		@show
	</div>
</form> --}}

	{{-- <form class="ui data form" id="dataForm">

		<div class="ui error message"></div>
		<div class="field">
			<div class="field">
				<label>Kode TMUK :</label>
				<input name="" placeholder="Inputkan Kode TMUK" type="text" value="20182501777">
			</div>
		</div>
		<div class="field">
			<div class="field">
				<label>Nama TMUK :</label>
				<input name="" placeholder="Inputkan Nama TMUK" type="text" value="Pragma Informatika">
			</div>
		</div>
		<div class="field">
			<div class="field">
				<label>Bank Escrow :</label>
				<input name="" placeholder="Inputkan Bank Escrow" type="text" value="Mandiri">
			</div>
		</div>
		<div class="field">
			<div class="field">
				<label>No. Rekening Bank :</label>
				<input name="" placeholder="Inputkan No. Rekening Bank" type="text" value="134-07-3323-2322">
			</div>
		</div>
		<div class="one field">
			<div class="field">
				<label>Nama Pemilik Rekening :</label>
				<input name="" placeholder="Inputkan Nama Pemilik Rekening" type="text" value="Pragma Informatika">
			</div>
		</div>
		<div class="two field">
			<div class="field">
				<label>Periode Transaksi </label>
				<input name="" placeholder="Inputkan Periode Transaksi" type="text" value="21/01/2018">
			</div>
			<div class="field">
				<label>s/d</label>
				<input name="" placeholder="Inputkan Periode Transaksi" type="text" value="25/01/2018">
			</div>
		</div>
	</form> --}}

				<div class="ui form">
				  <div class="inline fields">
				    <label class="four wide field">TMUK</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan TMUK" type="text" value="131313 - Pragma Informatika">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">Bank Escrow</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan TMUK" type="text" value="Mandiri">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">No. Rekening Bank</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan TMUK" type="text" value="134-07-3323-2322">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">Nama Pemilik Rekening</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan TMUK" type="text" value="Pragma Informatika">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">Periode Transaksi</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan Tanggal PR" type="text" value="21/01/18">-&nbsp;&nbsp;&nbsp;&nbsp;
				      <input placeholder="Inputkan Tanggal PR" type="text" value="21/01/18">
				    </div>
				  </div>
				</div>

						<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
			                <thead>
			                    <tr>
			                    	<th class="ui center aligned">No</th>
			                    	<th class="ui center aligned">Tanggal</th>
			                    	<th class="ui center aligned">Transaksi</th>
			                    	<th class="ui center aligned">Top Up (Rp)</th>
			                    </tr>

			                </thead>
			                <tbody>
			                	<tr>
									<td class="ui center aligned">1</td>
									<td class="ui center aligned">25/02/18</td>
									<td class="ui center aligned">201825010079900045</td>
									<td class="ui right aligned">1.000.000</td>
								</tr>
			                </tbody>
			                <tfoot class="full-width">
 							   <tr>
 							   	 <th colspan="2" rowspan="3" style="background-color: white"></th>
 							    
 							     <th class="ui center aligned">Total (Rp)</th>
 							     <th class="ui right aligned">1.000.000</th>
 							   </tr>
 							 </tfoot>
			            </table>
	
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui blue button">
		Download
	</div>
	<div class="ui default right labeled icon save button">
		Print
		<i class="print icon"></i>
	</div>
</div>