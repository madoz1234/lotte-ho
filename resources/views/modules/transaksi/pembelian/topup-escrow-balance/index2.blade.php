@extends('layouts.scaffold')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.semanticui.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.semanticui.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert/sweetalert2.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('scripts')
<script type="text/javascript">
		// global
		var dt = "";
		var formRules = [];
		var initModal = function(){
			return false;
		};

		$.fn.form.settings.prompt = {
			empty                : '{name} tidak boleh kosong',
			checked              : '{name} harus dipilih',
			email                : '{name} tidak valid',
			url                  : '{name} must be a valid url',
			regExp               : '{name} is not formatted correctly',
			integer              : '{name} must be an integer',
			decimal              : '{name} must be a decimal number',
			number               : '{name} hanya boleh berisikan angka',
			is                   : '{name} must be "{ruleValue}"',
			isExactly            : '{name} must be exactly "{ruleValue}"',
			not                  : '{name} cannot be set to "{ruleValue}"',
			notExactly           : '{name} cannot be set to exactly "{ruleValue}"',
			contain              : '{name} cannot contain "{ruleValue}"',
			containExactly       : '{name} cannot contain exactly "{ruleValue}"',
			doesntContain        : '{name} must contain  "{ruleValue}"',
			doesntContainExactly : '{name} must contain exactly "{ruleValue}"',
			minLength            : '{name} must be at least {ruleValue} characters',
			length               : '{name} must be at least {ruleValue} characters',
			exactLength          : '{name} must be exactly {ruleValue} characters',
			maxLength            : '{name} cannot be longer than {ruleValue} characters',
			match                : '{name} must match {ruleValue} field',
			different            : '{name} must have a different value than {ruleValue} field',
			creditCard           : '{name} must be a valid credit card number',
			minCount             : '{name} must have at least {ruleValue} choices',
			exactCount           : '{name} must have exactly {ruleValue} choices',
			maxCount 			 : '{name} must have {ruleValue} or less choices'
		};

	</script>
	@yield('rules')
	@yield('init-modal')
	
	@include('layouts.scripts.datatable')
	@include('layouts.scripts.actions')
	@append

	@section('content')
	@section('content-header')
	<div class="ui breadcrumb">
		<?php $i=1; $last=count($breadcrumb);?>
		@foreach ($breadcrumb as $name => $link)
		@if($i++ != $last)
		<a href="{{ $link }}" class="section">{{ $name }}</a>
		<i class="right chevron icon divider"></i>
		@else
		<div class="active section">{{ $name }}</div>
		@endif
		@endforeach
	</div>
	<h2 class="ui header">
		<div class="content">
			{!! $title or '-' !!}
			<div class="sub header">{!! $subtitle or ' ' !!}</div>
		</div>
	</h2>
	@show

	@section('content-body')
	<div class="ui grid">
		<div class="sixteen wide column main-content">
			<div class="ui segments">
				<div class="ui segment">
					<form class="ui filter form">
						<div class="fields">
							@section('filters')
							<div class="field">
								<div class="ui fluid selection dropdown">
									<input type="hidden" name="filter[entri]">
									<i class="dropdown icon"></i>
									<div class="default text">Entries</div>
									<div class="menu">
										<div class="item" data-value="10">10</div>
										<div class="item" data-value="25">25</div>
										<div class="item" data-value="50">50</div>
										<div class="item" data-value="100">100</div>
									</div>
								</div>
							</div>
							<div class="field">
								<input name="filter[kode]" placeholder="TMUK" type="text">
							</div>
							<button type="button" class="ui teal icon filter button" data-content="Cari Data">
								<i class="search icon"></i>
							</button>
							<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
								<i class="refresh icon"></i>
							</button>
							@show
							<div style="margin-left: auto">
								<button type="button" class="ui blue edit button">
									{{-- <i class="plus icon"></i> --}}
									Konfirmasi Return
								</button>
							</div>
						</div>
					</form>

					{{-- @section('subcontent')
					<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
						<thead>
							<tr>
								@foreach ($tableStruct as $struct)
								<th>{{ $struct['label'] or $struct['name'] }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="text-align: center;">1</td>
								<td style="text-align: left;">131113 - Pragma Informatika</td>
								<td style="text-align: center;"><button type="button" class="ui mini green icon add button" data-content="View" >View</button></td>
								<td style="text-align: center;">25/01/18</td>
								<td style="text-align: right;">12.000.000</td>
								<td><img src="{{ asset('img/tf.jpg') }}" style="width: 100px; height: 100px; object-fit:cover"></td>
								
								<td style="text-align: center;"><button type="button" class="ui circular mini red icon button" data-content="Validasikan">Validasi</button></td>
							</tr>
						</tbody>
					</table>
					@show --}}
				</div>
			</div>
		</div>
	</div>
	@show

	<br>
	<div class="ui grid">
		<div class="sixteen wide column main-content">
			<div class="ui segments">
				<div class="ui segment">
					<form class="ui filter form">
						<form class="ui data form" id="dataForm">

							<div class="ui error message"></div>
							<div class="two fields">
								<div class="field">
									<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th style="text-align: center;">Checked Tasks Total Transaksi</th>
												<th style="text-align: center; width: 60%;">Total Pembayaran (Rp)</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td style="text-align: center;">2</td>
												<td style="text-align: center;">7.011.000</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div style="padding-left: 34%;" class="field">
									<div class="ui buttons" style="margin-right: 5px">
										<button type="reset" class="ui icon reset button" data-content="Refresh">
											<i class="refresh icon"></i>
										</button>
									</div>
									<div class="ui green buttons" style="margin-right: 5px">
										<button type="button" class="ui grey button">Setujui</button>
									</div>
									<div class="ui red buttons" style="margin-right: 5px">
										<button type="button" class="ui blue button">Tidak Setujui</button>
									</div>
								</div>
							</div>
						</form>
					</form>
				</div>
			</div>
		</div>
	</div>

	
	@endsection

	@section('modals')
	<div class="ui {{ $modalSize or 'mini' }} modal" id="formModal">
		<div class="ui inverted loading dimmer">
			<div class="ui text loader">Loading</div>
		</div>
	</div>
	@append