<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Riwayat Top Up</div>
					
<div class="content">
				<div class="ui form">
				  <div class="inline fields">
				    <label class="four wide field">TMUK</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan TMUK" type="text" value="{{ $record->kode }} - {{ $record->nama }}">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">Bank Escrow</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan TMUK" type="text" value="{{ $record->bankescrow->nama }}">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">No. Rekening Bank</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan TMUK" type="text" value="{{ $record->rekeningescrow->nomor_rekening }}">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">Nama Pemilik Rekening</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan TMUK" type="text" value="{{ $record->rekeningescrow->nama_pemilik }}">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="four wide field">Periode Transaksi</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan Tanggal PR" type="text" value="21/01/18">-&nbsp;&nbsp;&nbsp;&nbsp;
				      <input placeholder="Inputkan Tanggal PR" type="text" value="21/01/18">
				    </div>
				  </div>
				</div>

						<table id="listTableDetail" class="ui celled compact red table" width="100%" cellspacing="0">
			                <thead>
			                    <tr>
			                    	<th class="ui center aligned">No</th>
			                    	<th class="ui center aligned">Tanggal</th>
			                    	
			                    	<th class="ui center aligned">Jenis Top Up</th>
			                    	<th class="ui center aligned">Top Up (Rp)</th>
			                    	<th class="ui center aligned">Status</th>
			                    </tr>

			                </thead>
			                <tbody>
			                	<?php $i=1;?>
			                	@foreach($topup as $tp)
			                	<tr>
									<td class="ui center aligned">{{ $i++ }}</td>
									<td class="ui center aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $tp->tanggal)->format('d/m/Y') }}</td>
									<td class="ui center aligned">
										<?php 
											 $string = '';
								                if ($tp->jenis == 1) {
								                    $string = 'Setoran Penjualan Tunai';
								                }else if($tp->jenis == 2){
								                    $string = 'Setoran Penjualan Kredit';
								                }else{
								                    $string = 'Setoran Modal';
								                }								                
										?>
										{{ $string }}
									</td>
									<td class="ui center aligned">{{ 'Rp '. number_format($tp->jumlah,2,',','.').'' }}</td>
									<td class="ui center aligned">
										@if($tp->status == 1)
											Terverifikasi
										@else
											Ditolak
										@endif
									</td>
								</tr>
			                	@endforeach
			                	
			                </tbody>
			                {{-- <tfoot class="full-width">
 							   <tr>
 							   	 <th colspan="2" rowspan="3" style="background-color: white"></th>
 							    
 							     <th class="ui center aligned">Total (Rp)</th>
 							     <th class="ui right aligned">1.000.000</th>
 							   </tr>
 							 </tfoot> --}}
			            </table>
	
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <div class="ui blue button">
		Download
	</div>
	<div class="ui default right labeled icon save button">
		Print
		<i class="print icon"></i>
	</div> --}}
</div>