<div class="ui inverted loading dimmer">
    <div class="ui text loader">Loading</div>
</div>
<div class="header">Detail PO (BTDK)</div>
                    
<div class="content">

    <form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
    {!! csrf_field() !!}
    <input type="hidden" name="_method" value="PUT">
        <div class="ui error message"></div>
        <div class="three fields">
            <div class="field">
                <label>Tanggal PR :</label>
                <input name="tgl_buat" placeholder="Inputkan Tanggal PYR" type="text" value="{{ $record->tgl_buat or "" }}" readonly="">
            </div>
            <div class="field">
                <label>LSI :</label>
                <input placeholder="Inputkan Vendor Non Lotte" type="text" value="{{ isset($record->tmuk->lsi->nama)?$record->tmuk->lsi->nama : "" }}" readonly="">
            </div>
            <div class="field">
                <label>TMUK :</label>
                <input placeholder="Inputkan TMUK" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
                <input name="tmuk_kode" placeholder="Inputkan TMUK" type="hidden" value="{{ $record->tmuk_kode or "" }}" readonly="">
            </div>
        </div>
        <div class="three fields">
            <div class="field">
                <label>Nomor PR :</label>
                <input name="nomor_pr" placeholder="Inputkan Nomor PYR" type="text" value="{{ $record->nomor or "" }}" readonly="">
            </div>
            <div class="field">
                <label>Nilai PO BTDK (Rp) :</label>
                <input type="text" value="{{ $record->total or "" }}" readonly="">
            </div>
            <div class="field">
                <label>Alamat Pengiriman :</label>
                <input placeholder="Inputkan Alamat Pengiriman" type="text" value="{{ $record->tmuk->alamat or "" }}" readonly="">
            </div>
        </div>
        <table id="tabelDetailPo" class="ui celled compact red table trace_value" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2" class="ui center aligned">No</th>
                    <th rowspan="2" class="ui center aligned">Kode Produk</th>
                    <th rowspan="2" class="ui center aligned">Nama Produk</th>
                    <th rowspan="2" class="ui center aligned">Barcode</th>
                    <th colspan="2" class="ui center aligned">Order Unit Info</th>
                    <th colspan="2" class="ui center aligned">Selling Unit Info</th>
                    <th colspan="2" class="ui center aligned">PR QTY</th>
                    <th colspan="2" class="ui center aligned">PO QTY</th>
                    <th rowspan="2" class="ui center aligned">Harga Estimasi (Rp)</th>
                    <th class="sum" rowspan="2" class="ui center aligned">Harga Aktual (Rp)</th>
                    <th rowspan="2" class="ui center aligned">Total Estimasi (Rp)</th>
                    <th class="sum" rowspan="2" class="ui center aligned">Total Aktual (Rp)</th>
                </tr>
                <tr>
                    <th class="center aligned">Stock GMD</th>
                    <th class="center aligned">UOM</th>
                    <th class="center aligned">Qty/UOM</th>
                    <th class="center aligned">UOM</th>
                    <th class="center aligned">Qty</th>
                    <th class="center aligned">Unit</th>
                    <th class="center aligned">Qty</th>
                    <th class="center aligned">Unit</th>
                </tr>
            </thead>
            <tbody>
            @if($detail->count() > 0)
                <?php $i = 1; $all_total_estimasi = 0; ?>
        
                @foreach ($detail as $row)
                
                <input type="hidden" name="detail_id[]" value="{{ $row['id_detail'] }}">
                <tr>
                    <td class="center aligned">{{ $i }}</td>
                    <td class="right aligned">{{ $row['produk_kode'] }}</td>
                    <td class="aligned">{{ $row['nama'] }}</td>
                    <td class="aligned">{{ $row['uom1_barcode'] }}</td>
                    <td class="center aligned">{{ $row['stok_gmd'] }}</td>
                    <td class="center aligned">{{ ($row['unit_order'] != "") ? ($row['unit_order']) : 'Pcs' }}</td>
                    <td class="center aligned">{{ $row['qty_sell'] }}</td>
                    <td class="center aligned">{{ ($row['unit_sell'] != "") ? ($row['unit_sell']) : 'Pcs' }}</td>
                    <td class="right aligned">{{ $row['qty_pr'] }}</td>
                    <td class="center aligned">{{ ($row['unit_pr'] != "") ? ($row['unit_pr']) : 'Pcs' }}</td>
                    <td class="center aligned">{{ $row['qty_po'] }}</td>
                    <td class="center aligned">{{ ($row['unit_po'] != "") ? ($row['unit_po']) : 'Pcs' }}</td>
                    <td class="right aligned">{{ FormatNumber($row['harga_estimasi']) }}</td>
                    <td class="right aligned">
                        <?php 
                            if ($row['qty_po'] != 0) {
                                $price_pcs = $row['harga_aktual'] / $row['qty_po'];
                            }else{
                                $price_pcs = 0;
                            }
                        ?>
                        {{ FormatNumber($price_pcs) }}
                    </td>
                    <td class="right aligned">
                        <?php 
                            if ($row['qty_po'] != 0) {
                                $total_estimasi = $row['harga_estimasi'] * $row['qty_po'];
                            }else{
                                $total_estimasi = 0;
                            }
                            $all_total_estimasi += $total_estimasi;
                        ?>
                        {{ FormatNumber($total_estimasi) }}
                    </td>
                    <td class="right aligned">{{ FormatNumber($row['harga_aktual']) }}</td>
                </tr>
                <?php $i++; ?>
                @endforeach
            @else
                <tr>
                    <td colspan="11">
                        <center><i>Maaf tidak ada data</i></center>
                    </td>
                </tr>
            @endif
            </tbody>
            <tfoot>
               <tr>
                 <th colspan="13" style="background-color: white; border-bottom-color: white"></th>
                 <th class="ui center aligned">Total (Rp)</th>
                 <th class="right aligned" colspan="1">{{ FormatNumber($all_total_estimasi) }}</th>
                 <th class="right aligned" colspan="1">{{ FormatNumber($record->total) }}</th>
               </tr>
             </tfoot>
        </table>
    </form>

    
</div>
<div class="actions">
    <div class="ui negative button" style="background: grey;">
        Tutup
    </div>
</div>

@include('layouts.scripts.inputmask')