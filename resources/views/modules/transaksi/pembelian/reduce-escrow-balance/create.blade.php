<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Akun Bank</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>Nama Akun Bank</label>
				<input name="nama_akun" placeholder="Akun Bank" type="text">
			</div>

			<div class="field">
				<label>Nama Bank</label>
				<input name="nama_bank" placeholder="Inputkan Nama Bank" type="text">
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label>No Rekening</label>
				<input name="no_rekening" placeholder="Inputkan No Rekening Bank" type="text">
			</div>

			<div class="field">
				<label>Bank Akun GL</label>
				<input name="akun_bank_gl" placeholder="Inputkan Akun Bank GL Kode" type="text">
			</div>
		</div>
		
		<div class="two fields">
			<div class="field">
				<label>Tipe Akun</label>
				<div class="ui fluid search selection dropdown">
					<input type="hidden" name="tipe_akun">
					<i class="dropdown icon"></i>
					<div class="default text">-- Pilih --</div>
					<div class="menu">
						<div class="item" data-value="Savings Account">Rekening Tabungan</div>
						<div class="item" data-value="Chequing Account">Akun Chequing</div>
						<div class="item" data-value="Credit Account">Akun Kredit</div>
						<div class="item" data-value="Cash Account">Rekening Kas</div>
					</div>
				</div>
			</div>

			<div class="field">
				<label>Mata Uang</label>
				<div class="ui fluid search selection dropdown">
					<input type="hidden" name="mata_uang">
					<i class="dropdown icon"></i>
					<div class="default text">-- Pilih --</div>
					<div class="menu">
						<div class="item" data-value="CAD">CA Dolar</div>
						<div class="item" data-value="EUR">Euro</div>
						<div class="item" data-value="IDR">rupiah</div>
						<div class="item" data-value="GBP">Pounds</div>
						<div class="item" data-value="USD">US Dolar</div>
					</div>
				</div>
			</div>
		</div>

		<div class="field">
			<label>Alamat Bank</label>
			<textarea name="alamat_bank" rows="2"></textarea>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>