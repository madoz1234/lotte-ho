<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Struk</div>
<div class="content">
    <form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="id" value="{{ $record->id }}">
        <div class="ui error message"></div>
        <div class="two fields">
         <div class="field">
            <label>Tanggal PO</label>
            <input placeholder="" type="text" value="{{ $record->tanggal or "" }}" readonly="">
        </div>

        <div class="field">
            <label>TMUK</label>
            <input placeholder="" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
        </div>
    </div>

    <div class="two fields">
     <div class="field">
        <label>Nomor PO/PYR</label>
        <input placeholder="" type="text" value="{{ $record->po_nomor or "" }}" readonly="">
    </div>
    <div class="field">
        <label>Nilai PO BTDK (Rp)</label>
        <input placeholder="" type="text" value="{{ FormatNumber($record->transpo->total) }}" readonly="">
    </div>
</div>
<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
 <thead>
     <tr>
        <th class="center aligned">#</th>
        <th class="center aligned">Nomor Struk</th>
        <th class="center aligned">Total Belanja Kasir</th>
    </tr>
</thead>
<tbody>
    <?php $i = 1; ?>

    @foreach ($detail as $row)
    <tr>
        <td class="center aligned">{{ $i }}</td>
        <td class="left aligned">{{ $row->nomor_struk }}</td>
        <td class="center aligned">{{ FormatNumber($row->total_belanja) }}</td>
    </tr>
    <?php $i++; ?>
    @endforeach
</tbody>
<tfoot>
 <tr>
    <input type="hidden" name="total" value="{{ $total }}" readonly=""></th>
    <th class="right aligned" colspan="1">&nbsp;</th>
    <th class="ui right aligned">Total (Rp)</th>
    <th class="right aligned" colspan="1"><span id="total_price"><center>{{ FormatNumber($total)}}</center></span>
</tr>
</tfoot>
</table>

</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	{{-- <div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div> --}}
</div>