@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field">
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tanggal]" type="text" placeholder="Tanggal PO">
		</div>
	</div>
</div>
<div class="field">
	<input name="filter[po_nomor]" placeholder="PO Nomor" type="text">
</div>
<div class="field">
	<select name="filter[tmuk_kode]" class="ui search dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'TMUK') !!}
	</select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')

@endsection

@section('js-filters')
    d.tanggal = $("input[name='filter[tanggal]']").val();
    d.po_nomor = $("input[name='filter[po_nomor]']").val();
    d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
@endsection

@section('rules')
<script type="text/javascript">
//
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

		$('#tabelDetailPo').DataTable({
			paging: false,
			filter: false,
			lengthChange: false,
			ordering:false,
			scrollX: true,
			scrollY: 300,
		});
	};
</script>
@endsection

@section('subcontent')
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui top demo tabular menu">
  <div class="active item" data-tab="first">Validasi</div>
  <div class="item" data-tab="second">Histori</div>
</div>
<div class="ui bottom demo active tab segment" data-tab="first">
	<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th rowspan="2" class="ui center aligned">No</th>
				<th rowspan="2" class="ui center aligned">Tanggal PO</th>
				<th rowspan="2" class="ui center aligned">Nomor PO</th>
				<th rowspan="2" class="ui center aligned">TMUK</th>
				<th rowspan="2" class="ui center aligned">PO-BTDK (Rp)</th>
				<th colspan="2" class="ui center aligned">Struk Kasir</th>
				<th colspan="2" class="ui center aligned">Saldo Deposit Lotte</th>
				<th colspan="2" class="ui center aligned">Saldo Escrow</th>
				<th colspan="4" class="ui center aligned">Proses Approve</th>
				<th rowspan="2" class="ui center aligned">Keterangan</th>
				<th rowspan="2" class="ui center aligned">Aksi</th>
			</tr>
			<tr>
				<th class="ui center aligned">Nomor Struk</th>
				<th class="ui center aligned">Saldo Dipotong (Rp)</th>
				<th class="ui center aligned">Awal (Rp) deposit</th>
				<th class="ui center aligned">Akhir (Rp) deposit</th>
				<th class="ui center aligned">Awal (Rp) escrow</th>
				<th class="ui center aligned">Akhir (Rp) escrow</th>
				<th class="ui center aligned">Persetujuan 1</th>
				<th class="ui center aligned">Persetujuan 2</th>
				<th class="ui center aligned">Persetujuan 3</th>
				<th class="ui center aligned">Persetujuan 4</th>
			</tr>
		</thead>
	</table>
</div>
<div class="ui bottom demo tab segment" data-tab="second">
	<table id="listTable2" class="ui celled compact green table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th rowspan="2" class="ui center aligned">No</th>
				<th rowspan="2" class="ui center aligned">Tanggal PO</th>
				<th rowspan="2" class="ui center aligned">Nomor PO</th>
				<th rowspan="2" class="ui center aligned">TMUK</th>
				<th rowspan="2" class="ui center aligned">PO-BTDK (Rp)</th>
				<th colspan="2" class="ui center aligned">Struk Kasir</th>
				<th colspan="2" class="ui center aligned">Saldo Deposit Lotte</th>
				<th colspan="2" class="ui center aligned">Saldo Escrow</th>
				<th colspan="4" class="ui center aligned">Proses Approve</th>
				<th rowspan="2" class="ui center aligned">Keterangan</th>
				<th rowspan="2" class="ui center aligned">Aksi</th>
			</tr>
			<tr>
				<th class="ui center aligned">Nomor Struk</th>
				<th class="ui center aligned">Saldo Dipotong (Rp)</th>
				<th class="ui center aligned">Awal (Rp) deposit</th>
				<th class="ui center aligned">Akhir (Rp) deposit</th>
				<th class="ui center aligned">Awal (Rp) escrow</th>
				<th class="ui center aligned">Akhir (Rp) escrow</th>
				<th class="ui center aligned">Persetujuan 1</th>
				<th class="ui center aligned">Persetujuan 2</th>
				<th class="ui center aligned">Persetujuan 3</th>
				<th class="ui center aligned">Persetujuan 4</th>
			</tr>
		</thead>
	</table>
</div>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		//disable klik kanan & f12 & Ctrl+Shift+I
		$(document).ready(function(){
		$("body").bind("contextmenu", function(e) {
		    e.preventDefault();
		});
		document.onkeypress = function (event) {
		        event = (event || window.event);
		        if (event.keyCode == 123) {
		            return false;
		        }
		    }
		    document.onmousedown = function (event) {
		        event = (event || window.event);
		        if (event.keyCode == 123) {
		            return false;
		        }
		    }
		document.onkeydown = function (event) {
		        event = (event || window.event);
		        if (event.keyCode == 123) { //F12
		            return false;
		        } else if(event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
		        return false;
		    	}
		    }

		});
		//disable klik kanan & f12 & Ctrl+Shift+I
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

		$(document).ready(function(){
		    $('.demo.menu .item').tab();
		});

		//aproval 1
		$(document).on('click', '#btn-approval-1', function(e){
			var id = $(this).data("id");

			swal({
				title: 'Apakah anda setuju ?',
				text: "Data yang sudah disetujui, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Setuju',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result) {
					$.ajax({
		                url: "{{url($pageUrl)}}/on-change-pop-approval1/"+id,
		                type: 'GET',
		                dataType: 'json',
		            })
		            .done(function(response) {
		                // window.location.reload();
		                dt.draw();
		            })
		            .fail(function() {
		                console.log("error");
		            })
		            .always(function() {
		                console.log("complete");
		            });
				}
			})
			
		});

		//aproval 2
		$(document).on('click', '#btn-approval-2', function(e){
			var id = $(this).data("id");

			swal({
				title: 'Apakah anda setuju ?',
				text: "Data yang sudah disetujui, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Setuju',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result) {
					$.ajax({
		                url: "{{url($pageUrl)}}/on-change-pop-approval2/"+id,
		                type: 'GET',
		                dataType: 'json',
		            })
		            .done(function(response) {
		                dt.draw();
		                // window.location.reload();
		            })
		            .fail(function() {
		                console.log("error");
		            })
		            .always(function() {
		                console.log("complete");
		            });
				}
			})
			
		});

		//aproval 3
		$(document).on('click', '#btn-approval-3', function(e){
			var id = $(this).data("id");

			swal({
				title: 'Apakah anda setuju ?',
				text: "Data yang sudah disetujui, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Setuju',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result) {
					$.ajax({
		                url: "{{url($pageUrl)}}/on-change-pop-approval3/"+id,
		                type: 'GET',
		                dataType: 'json',
		            })
		            .done(function(response) {
		                dt.draw();
		                // window.location.reload();
		            })
		            .fail(function() {
		                console.log("error");
		            })
		            .always(function() {
		                console.log("complete");
		            });
				}
			})

			
		});

		//aproval 4
		$(document).on('click', '#btn-approval-4', function(e){
			var id = $(this).data("id");
			$(".loading").addClass('active');
			swal({
				title: 'Apakah anda setuju ?',
				text: "Data yang sudah disetujui, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Setuju',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result) {
					$.ajax({
		                url: "{{url($pageUrl)}}/on-change-pop-approval4/"+id,
		                type: 'GET',
		                dataType: 'json',
		            })
		            .done(function(response) {
		                dt.draw();
		                dt2.draw();
						$(".loading").removeClass('active');
		                // window.location.reload();
		            })
		            .fail(function() {
		            	dt.draw();
		                dt2.draw();
						$(".loading").removeClass('active');
		                console.log("error");
		            })
		            .always(function() {
		                console.log("complete");
		            });
				}
			})
			
		});

		//ditolak
		$(document).on('click', '#btn-tolak', function(e){
			var id = $(this).data("id");
			$(".loading").addClass('active');
			swal({
				title: 'Apakah anda yakin ?',
				text: "Data yang sudah ditolak, tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Setuju',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result) {
					$.ajax({
		                url: "{{url($pageUrl)}}/on-change-pop-tolak/"+id,
		                type: 'GET',
		                dataType: 'json',
		            })
		            .done(function(response) {
		                dt.draw();
		                dt2.draw();
						$(".loading").removeClass('active');
		                // window.location.reload();
		            })
		            .fail(function() {
		            	dt.draw();
		                dt2.draw();
						$(".loading").removeClass('active');
		                console.log("error");
		            })
		            .always(function() {
		                console.log("complete");
		            });
				}
			})
			
		});

		$(document).on('click', '.detail-po.button', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/detail-po/" + id;

			loadModal(url);
		});
	</script>
@append