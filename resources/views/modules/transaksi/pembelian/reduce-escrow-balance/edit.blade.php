<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Entri Struk</div>
<div class="content">
    <form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="id" id="id_escrow" value="{{ $record->id }}">
        <div class="ui error message"></div>
        <div class="two fields">
         <div class="field">
            <label>Tanggal PO</label>
            <input placeholder="" type="text" value="{{ $record->tanggal or "" }}" readonly="">
        </div>

        <div class="field">
            <label>TMUK</label>
            <input placeholder="" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
        </div>
    </div>

    <div class="two fields">
     <div class="field">
        <label>Nomor PO/PYR</label>
        <input placeholder="" type="text" value="{{ $record->po_nomor or "" }}" readonly="">
    </div>
    <div class="field">
        <label>Nilai PO BTDK (Rp)</label>
        <input placeholder="" type="text" id="total_po" value="{{ FormatNumber($record->transpo->total) }}" readonly="">
    </div>
</div>
<table id="logsheet" class="ui celled compact red table" width="100%" cellspacing="0">
 <thead>
     <tr>
        <th class="center aligned">#</th>
        <th class="center aligned">Nomor Struk</th>
        <th class="center aligned">Total Belanja Kasir</th>
    </tr>
</thead>
<tbody>
    @if(isset($record))
        <?php 
            $detail = $record->detailreduce;

        ?>
        @foreach($detail as $row)
            <tr data-row="{{ $row->id }}">
                <td class="center aligned"><input type="checkbox" name="escrow_detail_id[]" class="case" value="{{ $row->id }}"></td>
                <td>
                    <div class="field">
                        <div class="ui input">
                            <input type="hidden" name="escrow_detail_id[]" value="{{ $row->id }}">
                            <input type="text" name="escrow_nomor_struk[]" class="form-control" onkeypress="return isNumberKey(event)" value="{{ $row->nomor_struk }}" maxlength="30">
                        </div>
                    </div>
                </td>
                <td>
                    <div class="field">
                        <div class="ui labeled input">
                            <label for="" class="ui label">Rp.</label>
                            <input type="text" name="escrow_total_belanja[]" class="escrow_total_belanja" placeholder="Inputkan Nama" data-inputmask="'alias' : 'numeric'" value="{{ $row->total_belanja }}" onkeyup="sum()" {{ $role }}>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
</tbody>
<tfoot>
    <tr>
        <th colspan="2" class="ui right aligned"><b>Total</b></th>
        <th class="ui center aligned">
           <div class="field">
                <div class="ui labeled input">
                    <label for="" class="ui label">Rp.</label>
                    <input type="text" name="escrow_total[]" id="total" placeholder="Inputkan Nama" data-inputmask="'alias' : 'numeric'" value="{{ FormatNumber(0) }}" readonly="" style="background-color: #edf1f6;">
                </div>
            </div>
        </th>
    </tr>
</tfoot>
</table>
{{--  <button type="button" onclick="add_row()" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Tambah</button> --}}
<button type="button" style="margin-left:30px; margin-bottom: 10px" class="hapus ui mini red icon trash button"><i class="icon trash button"></i></button>
<button type="button" onclick="add_row()"  style="margin-bottom: 10px" class="ui mini blue icon plus button"><i class="icon plus button"></i></button>

<template id="template-row">
    <tr>
        <td class="center aligned"><input type="checkbox" class="case"></td>
        <td>
            <div class="field">
                <div class="ui action input">
                    <input type="text" name="escrow_nomor_struk[]" class="form-control escrow_nomor_struk" onkeypress="return isNumberKey(event)" maxlength="30" value="">
                    <button class="ui button btn-cek-struk"><i class="search icon"></i> Cari</button>
                </div>
            </div>
        </td>
        <td>
            <div class="field">
                <div class="ui labeled input">
                    <label for="" class="ui label">Rp.</label>
                    <input type="text" name="escrow_total_belanja[]" class="form-control escrow_total_belanja" data-inputmask="'alias' : 'numeric'" value="0,00" onkeyup="sum()" {{ $role }}>
                </div>
            </div>
        </td>
    </tr>
</template>

</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button" id="btn-save-struk">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

@include('layouts.scripts.inputmask')

<script type:"text/javascript">
    $(document).ready(function(){

        $(".hapus").on('click', function(elm) {
            $('.case:checkbox:checked').parents("tr").remove();
            $('.check_all').prop("checked", false);
            // check();
            sum();
        });

        // untuk manipulasi tabel
        get_index = function(elm)
        {
            return $(elm).closest("tr").index();
        }

        delete_row = function(elm)
        {
            var idx = get_index(elm);

            if (idx != 0) {
                var tr = $(elm).closest("tr");
                $(tr).fadeOut(500, function(){
                    $(this).remove();
                });
            }
        }

        add_row = function()
        {
            // get from template
            var tr = $('#template-row').html();

            // append to table
            $('table#logsheet > tbody').append(tr);

            reloadMask();

        }

        // init on load
        add_row();

        sum = function() {
            var element = document.getElementsByClassName('escrow_total_belanja');
            var total = 0;
            var validation1 = 0;
            var validation2 = 0;
            var nilai = 0;

            for (var i = 0; i < element.length; i++){
                nilai = element[i].value;
                if (nilai != undefined) {
                    validation1 = nilai.replace(',00', "");
                    validation2 =  validation1.replace(/[^0-9\,-]+/g, "");
                    total += parseFloat(validation2);
                }
            }
            
            if (!isNaN(total)) {
                $('#total').val(total+',00');

                var total_po_btdk = $('#total_po').val();
                validation1 = total_po_btdk.replace(',00', "");
                validation2 =  validation1.replace(/[^0-9\,-]+/g, "");
                
                // if (total != validation2) {
                    // $('#btn-save-struk').addClass('disabled');
                    // if (total > validation2) {
                    //     swal(
                    //         'Oops!',
                    //         'Total harus sama dengan nilai PO BTDK (Rp)',
                    //         'warning'
                    //     ).then(function(e){
                    //         dt.draw();
                    //     });
                    // }
                // }else{
                    $('#btn-save-struk').removeClass('disabled');
                // }
            }else{
                $('#total').val('0,00');
                $('#btn-save-struk').addClass('disabled');
            }
        }

    });

    //cek struk
    $(document).on('click', '.btn-cek-struk', function(e){
        var _this = $(this);
        var nomor_struk = _this.closest('tr').find('.escrow_nomor_struk').val();
        var id_escrow = $('#id_escrow').val();

        $(".loading").addClass('active');

        $.ajax({
            url: "{{url($pageUrl)}}/on-change-pop-struk/"+id_escrow+"/"+nomor_struk,
            type: 'GET',
            dataType: 'json',
        })
        .success(function(response) {
            $(".loading").removeClass('active');
            console.log(response);
            _this.closest('tr').find('.escrow_total_belanja').val(response.total+',00')
            $('#btn-save-struk').removeClass('disabled');

            sum();
        })
        .error(function(resp) {
            console.log("error");
            $(".loading").removeClass('active');
            var error = $('<ul class="list"></ul>');

            _this.closest('tr').find('.escrow_total_belanja').val(resp.responseJSON.total+',00')

            $.each(resp.responseJSON.status, function(index, val) {
                error.append('<li>'+val+'</li>');
            });
            error.append(resp.responseJSON.log);

            $('#formModal').find('.ui.error.message').html(error).show();
            $('#btn-save-struk').addClass('disabled');

            sum();
        });
    });
</script>