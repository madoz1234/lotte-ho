@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
@append

@section('filters')
<div class="field">
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tgl_buat]" type="text" placeholder="Tanggal PYR">
		</div>
	</div>
</div>
<div class="field" style="width: 17%;">
	<input name="filter[nomor_pyr]" placeholder="Nomor PYR" type="text">
</div>
<div class="field">
	<select name="tmuk_kode" class="ui search dropdown tmuk">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'TMUK') !!}
	</select>
</div>
<div class="field">
	<select name="vendor_lokal" class="ui search dropdown vendor disabled">
	    {{-- {!! \Lotte\Models\Master\VendorLokalTmuk::options('nama', 'id',[], 'Vendor Lokal') !!} --}}
		<option value="0">Vendor Lokal</option>
    </select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
    d.tgl_buat = $("input[name='filter[tgl_buat]']").val();
    d.nomor_pyr = $("input[name='filter[nomor_pyr]']").val();
    d.tmuk_kode = $("select[name='tmuk_kode']").val();
    d.vendor_lokal = $("select[name='vendor_lokal']").val();
@endsection

@section('toolbars')
	
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	tgl_buat: {
		identifier: 'tgl_buat',
		rules: [{
			type   : 'empty',
			prompt : 'Tanggal Buat Harus Terisi'
		}]
	},

	nomor_pr: {
		identifier: 'nomor_pr',
		rules: [{
			type   : 'empty',
			prompt : 'Nomor PR Harus Terisi'
		}]
	},

	tmuk_kode: {
		identifier: 'tmuk_kode',
		rules: [{
			type   : 'empty',
			prompt : 'TMUK Harus Terisi'
		}]
	},
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

		$('#tabelDetailPyr').DataTable({
			paging: false,
			filter: false,
			lengthChange: false,
			ordering:false,
			scrollX: true,
			scrollY: 300,
		});
	};
</script>
@endsection

@section('subcontent')
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui top demo tabular menu">
  <div class="active item" data-tab="first">Validasi</div>
  <div class="item" data-tab="second">Histori</div>
</div>
<div class="ui bottom demo active tab segment" data-tab="first">
	<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th rowspan="2" class="ui center aligned">#</th>
				<th rowspan="2" class="ui center aligned">Tanggal PYR</th>
				<th rowspan="2" class="ui center aligned">Nomor PYR</th>
				<th rowspan="2" class="ui center aligned">Vendor</th>
				<th rowspan="2" class="ui center aligned">Tanggal Jatuh Tempo</th>
				<th rowspan="2" class="ui center aligned">Jumlah Tagihan (Rp)</th>
				<th rowspan="2" class="ui center aligned">Saldo Escrow Saat Ini (Rp)</th>
				<th rowspan="2" class="ui center aligned">Nama Bank</th>
				<th rowspan="2" class="ui center aligned">Biaya Administrasi Bank (Rp)</th>
				<th rowspan="2" class="ui center aligned">Total (Rp)</th>
				<th colspan="4" class="ui center aligned">Proses Approve</th>
				<th rowspan="2" class="ui center aligned">Aksi</th>
				{{-- <th rowspan="2" class="ui center aligned">Keterangan</th> --}}
			</tr>
			<tr>
				<th class="ui center aligned">Persetujuan 1</th>
				<th class="ui center aligned">Persetujuan 2</th>
				<th class="ui center aligned">Persetujuan 3</th>
				<th class="ui center aligned">Persetujuan 4</th>
			</tr>
		</thead>
	</table>
</div>
<div class="ui bottom demo tab segment" data-tab="second">
	<table id="listTable2" class="ui celled compact green table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th rowspan="2" class="ui center aligned">#</th>
				<th rowspan="2" class="ui center aligned">Tanggal PYR</th>
				<th rowspan="2" class="ui center aligned">Nomor PYR</th>
				<th rowspan="2" class="ui center aligned">Vendor</th>
				<th rowspan="2" class="ui center aligned">Tanggal Jatuh Tempo</th>
				<th rowspan="2" class="ui center aligned">Jumlah Tagihan (Rp)</th>
				<th rowspan="2" class="ui center aligned">Saldo Escrow Saat Ini (Rp)</th>
				<th rowspan="2" class="ui center aligned">Nama Bank</th>
				<th rowspan="2" class="ui center aligned">Biaya Administrasi Bank (Rp)</th>
				<th rowspan="2" class="ui center aligned">Total (Rp)</th>
				<th colspan="4" class="ui center aligned">Proses Approve</th>
				<th rowspan="2" class="ui center aligned">Aksi</th>
				{{-- <th rowspan="2" class="ui center aligned">Keterangan</th> --}}
			</tr>
			<tr>
				<th class="ui center aligned">Persetujuan 1</th>
				<th class="ui center aligned">Persetujuan 2</th>
				<th class="ui center aligned">Persetujuan 3</th>
				<th class="ui center aligned">Persetujuan 4</th>
			</tr>
		</thead>
	</table>
</div>
@endsection

@section('scripts')
<script>
	//disable klik kanan & f12 & Ctrl+Shift+I
	$(document).ready(function(){
		$("body").bind("contextmenu", function(e) {
		    e.preventDefault();
		});
		document.onkeypress = function (event) {
	        event = (event || window.event);
	        if (event.keyCode == 123) {
	            return false;
	        }
	    }
	    document.onmousedown = function (event) {
	        event = (event || window.event);
	        if (event.keyCode == 123) {
	            return false;
	        }
	    }
		document.onkeydown = function (event) {
	        event = (event || window.event);
	        if (event.keyCode == 123) { //F12
	            return false;
	        } else if(event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
	        return false;
	    	}
	    }
	    $('[name=tmuk_kode]').change(function(e){
			//generate produk by lsi
			$(".loading").addClass('active');

			$.ajax({
				url: "{{ url('ajax/option/vendor') }}",
				type: 'GET',
				data:{
					id_tmuk : $(this).val(),
				},
			})
			.success(function(response) {
				$(".loading").removeClass('active');
				$('.vendor').removeClass('disabled');
				$('.vendor').dropdown('clear');
				$('[name=vendor_lokal]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});
	});
	//disable klik kanan & f12 & Ctrl+Shift+I
	//Calender
	$('.ui.calendar').calendar({
	  type: 'date'
	});

	$(document).ready(function(){
	    $('.demo.menu .item').tab();
	});

	//aproval 1
	$(document).on('click', '#btn-approval-1', function(e){
		var id = $(this).data("id");
		var biaya = $('[name="biaya_admin_'+id+'"]').val();

		swal({
			title: 'Apakah anda setuju ?',
			text: "Data yang sudah disetujui, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Setuju',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
	                url: "{{url($pageUrl)}}/on-change-pop-approval1/"+id+"/"+biaya,	                
	                type: 'GET',
	                dataType: 'json',
	            })
	            .done(function(response) {
	                // window.location.reload();
	                dt.draw();
	            })
	            .fail(function() {
	                console.log("error");
	            })
	            .always(function() {
	                console.log("complete");
	            });
			}
		})
		
	});

	//aproval 2
	$(document).on('click', '#btn-approval-2', function(e){
		var id = $(this).data("id");

		swal({
			title: 'Apakah anda setuju ?',
			text: "Data yang sudah disetujui, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Setuju',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
	                url: "{{url($pageUrl)}}/on-change-pop-approval2/"+id,
	                type: 'GET',
	                dataType: 'json',
	            })
	            .done(function(response) {
	                dt.draw();
	                // window.location.reload();
	            })
	            .fail(function() {
	                console.log("error");
	            })
	            .always(function() {
	                console.log("complete");
	            });
			}
		})
		
	});

	//aproval 3
	$(document).on('click', '#btn-approval-3', function(e){
		var id = $(this).data("id");

		swal({
			title: 'Apakah anda setuju ?',
			text: "Data yang sudah disetujui, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Setuju',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
	                url: "{{url($pageUrl)}}/on-change-pop-approval3/"+id,
	                type: 'GET',
	                dataType: 'json',
	            })
	            .done(function(response) {
	                dt.draw();
	                // window.location.reload();
	            })
	            .fail(function() {
	                console.log("error");
	            })
	            .always(function() {
	                console.log("complete");
	            });
			}
		})

		
	});

	//aproval 4
	$(document).on('click', '#btn-approval-4', function(e){
		var id = $(this).data("id");
		// $(".loading").addClass('active');
		swal({
			title: 'Apakah anda setuju ?',
			text: "Data yang sudah disetujui, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Setuju',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
	                url: "{{url($pageUrl)}}/on-change-pop-approval4/"+id,
	                type: 'GET',
	                dataType: 'json',
	            })
	            .done(function(response) {
	                dt.draw();
	                dt2.draw();
					$(".loading").removeClass('active');
	                // window.location.reload();
	            })
	            .fail(function() {
	            	dt.draw();
	                dt2.draw();
					$(".loading").removeClass('active');
	                console.log("error");
	            })
	    		// .done(function(response) {
	      //           dt.draw();
	      //       })
	      //       .fail(function() {
	      //           console.log("error");
	      //       })
	            .always(function() {
	                console.log("complete");
	            });
			}
		})
		
	});

	$(document).on('click', '.detail.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/detail/" + id;

		loadModal(url);
	});

	$(document).on('click', '.detail-pyrs.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/detail-pyr/" + id;

		loadModal2(url);
	});
</script>
@append
