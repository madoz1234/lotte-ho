<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Aksi Pembayaran ke Pemasok</div>
					
<div class="content">

{{-- <form class="ui filter form">
	<div class="fields">
		@section('filters')
			<div class="field">
				<input name="filter[kode]" placeholder="Kode Area" type="text">
			</div>
			<div class="field">
				<input name="filter[area]" placeholder="Area" type="text">
			</div>
			<button type="button" class="ui teal icon filter button" data-content="Cari Data">
				<i class="search icon"></i>
			</button>
			<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
				<i class="refresh icon"></i>
			</button>
		@show
	</div>
</form> --}}

	<form class="ui data form" id="dataForm">

		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>Amount Of Discount (Rp) :</label>
				<input name="" placeholder="Amount Of Discount" type="text">
			</div>
			<div class="field">
				<label>Amount of Payment (Rp) :</label>
				<input name="" placeholder="Amount of Payment" type="text">
			</div>
		</div>
		<div class="field">
			<div class="field">
				<label>Memo :</label>
				<input name="" placeholder="Memo" type="text">
			</div>
		</div>
	</form>
	
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>