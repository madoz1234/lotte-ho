<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Persetujuan Pembayaran ke Vendor Lokal TMUK</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<!-- Filter -->
		{{-- <div class="fields">
			<div class="ui grid">
				<div class="field">
					<input name="filter[lsi]" placeholder="LSI" type="text" value="LOTTE GROSIR PASAR REBO">
				</div>

				<div class="field">
					<input name="filter[tmuk]" placeholder="TMUK" type="text" value="131 - Pragma Informatika">
				</div>

				<div class="field">
					<input name="filter[nomor_rr]" placeholder="Nomor RR" type="text" value="PR-2018012906007000180029">
				</div>

				<div class="field">
					<input name="filter[tgl_rr]" placeholder="Nomor PO" type="text" value="PO-2018012906007000180029">
				</div>


				<div class="field">
					<input name="filter[nomor_pr]" placeholder="Tanggal RR" type="text" value="29/01/18">
				</div>
				<button type="button" class="ui teal icon filter button" data-content="Cari Data">
					<i class="search icon"></i>
				</button>
				<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
					<i class="refresh icon"></i>
				</button>
			</div>
		</div> --}}

		<div class="ui form">
			<div class="two fields">
				  <div class="inline fields">
				    <label class="three wide field">TMUK</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="" type="text" value="0600700018 - Pragma Informatika">
				    </div>
				  </div>
				  <div class="inline fields">
				    <label class="three wide field">Vendor Lokal TMUK</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="" type="text" value="Tokoku">
				    </div>
				  </div>
			</div>
			<div class="two fields">
				  <div class="inline fields">
				    <label class="three wide field">Tanggal Jatuh Tempo</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="" type="text" value="22/02/18">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="three wide field">Jumlah Tagihan (Rp)</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="" type="text" value="3.400.000">
				    </div>
				  </div>
			</div>
			
			{{-- <div class="two fields">
				  <div class="inline fields">
				    <label class="two wide field">Nomor PO</label>
				    <div class="ten wide field">:&nbsp;&nbsp;
				      <input placeholder="" type="text" value="PO-20180129060070001800299">
				    </div>
				  </div>
			</div> --}}
		</div>


		<!-- Filter -->
		<br>
		<!-- List Table -->
		<div class="field">

			<table class="ui celled striped table">
				<thead>
					<tr align="center">
						<th rowspan="2">No</th>
						<th rowspan="2">Kode Produk</th>
						<th rowspan="2">Nama Produk</th>
						<th rowspan="2">Barcode</th>
						<th colspan="2">PR Qty</th>
						<th rowspan="2">Harga (Rp)</th>
						<th rowspan="2">Total Harga (Rp)</th>
						<th rowspan="2">Alasan</th>
						<th rowspan="2">Persetujuan</th>
						{{-- <th colspan="2">PR Qty Disetujui</th> --}}
					</tr>
					<tr>
						<th class="ui center aligned">Qty</th>
						<th class="ui center aligned">Unit</th>
						{{-- <th class="ui center aligned">Qty</th>
						<th class="ui center aligned">Unit</th> --}}
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="ui center aligned">1</td>
						<td class="ui center aligned">1052362000</td>
						<td class="ui center aligned">SARIMI 2 SOTO AYAM 24PCS/CT</td>
						<td class="ui center aligned">089686917335</td>
						<td class="ui center aligned">1</td>
						<td class="ui center aligned">pcs</td>
						<td class="ui right aligned">2.000</td>
						<td class="ui right aligned">2.000</td>
						<td class="ui center aligned">Barang Bagus</td>
						<td class="ui center aligned"><button type="button" class="ui mini green icon button" data-content="Tervalidasi" data-id="1">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">30/01/18, 10:45</i></td>
						{{-- <td class="ui center aligned"><input type="number"></td>
						<td class="ui center aligned">pcs</td> --}}
					</tr>
				</tbody>
			</table>
		</div>
		<!-- List Table -->
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	<div class="ui default right labeled icon save button">
		Print
		<i class="print icon"></i>
	</div>
</div>