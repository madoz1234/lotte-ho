<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail dari PYR</div>

<div class="content">

	<form class="ui data form" id="dataForm">
	{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>Tanggal PYR :</label>
				<input name="" placeholder="Inputkan Tanggal PYR" type="text" value="{{ $record->tgl_buat or "" }}" readonly="">
			</div>
			<div class="field">
				<label>TMUK :</label>
				<input name="" placeholder="Inputkan TMUK" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Saldo Escrow Saat Ini (Rp) :</label>
				<input name="" placeholder="Inputkan Saldo Escrow" type="text" value="{{ FormatNumber($record->tmuk->saldo_escrow) }}" readonly="">
			</div>
		</div>
		<div class="three fields">
			<div class="field">
				<label>Nomor PYR :</label>
				<input name="" placeholder="Inputkan Nomor PYR" type="text" value="{{ $record->nomor_pyr or "" }}" readonly="">
			</div>
			{{-- {{ dd($record->detailPyr->first()->produks->produksetting->tipebarang->nama) }} --}}
			<div class="field">
				<label>Tipe PYR :</label>
				<?php
					$tipe_pyr = '-';
					if ($record->tipe == 001) {
						$tipe_pyr = 'Trade Lotte';
					}else if($record->tipe == 002){
						$tipe_pyr = 'Trade Non-Lotte';
					}else if($record->tipe == 003){
						$tipe_pyr = 'Non Trade Lotte';
					}else if($record->tipe == 004){
						$tipe_pyr = 'Non Trade Non-Lotte';
					}
				?>
				<input name="" placeholder="Inputkan Vendor Non Lotte" type="text" value="{{ $tipe_pyr }}" readonly="">
			</div>
			<div class="field">
				<label>Vendor :</label>
				<input name="" placeholder="Inputkan Vendor Non Lotte" type="text" value="{{ $vendor or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Alamat Pengiriman :</label>
				<input name="" placeholder="Inputkan Alamat Pengiriman" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div>
		</div>
	</form>

	<table id="tabelDetailPyr" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th class="ui center aligned">No</th>
				<th class="ui center aligned">Kode Produk</th>
				<th class="ui center aligned">Nama Produk</th>
				<th class="ui center aligned">Qty</th>
				<th class="ui center aligned">Unit</th>
				<th class="ui center aligned">Jumlah (Rp)<br>( Qty x Harga Satuan )</th>
			</tr>
		</thead>
		<tbody>
			@if($record->detailPyr->count() > 0)
				{{-- {{ dd($total) }} --}}
				<?php $i = 1; ?>
				@foreach ($record->detailPyr as $row)
				<tr>
					<td width="5%" class="center aligned">{{ $i }}</td>
					<td width="20%" class="center aligned">{{ $row->produk_kode }}</td>
					<td width="35%" class="left aligned">{{ $row->produks->nama }}</td>
					<td width="15%" class="center aligned">{{ $row->qty }}</td>
					<td width="10%" class="center aligned">{{ $row->produks->produksetting->uom1_satuan or '-' }}</td>
					<td width="20%" class="ui right aligned">{{ FormatNumber($row->price) }}</td>
				</tr>
				<?php $i++;	?>
				@endforeach
			@else
				<tr>
					<td colspan="11">
						<center><i>Maaf tidak ada data</i></center>
					</td>
				</tr>
			@endif
		</tbody>
		<tfoot class="full-width">
			<tr>
				<th class="ui right aligned" style="background-color: white; border-bottom-color: white"></th>
				<th class="ui right aligned" style="background-color: white; border-bottom-color: white"></th>
				<th class="ui right aligned" style="background-color: white; border-bottom-color: white"></th>
				<th class="ui right aligned" style="background-color: white; border-bottom-color: white"></th>
				<th class="ui right aligned">Total (Rp)</th>
				<th class="ui right aligned">{{ FormatNumber($record->total) }}</th>
			</tr>
		</tfoot>
	</table>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <div class="ui blue button">
		Cetak
	</div> --}}
</div>