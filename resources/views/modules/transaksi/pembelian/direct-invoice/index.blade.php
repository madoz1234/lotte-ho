@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
@append

@section('filters')
<div class="field">
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tgl_buat]" type="text" placeholder="Tanggal PYR">
		</div>
	</div>
</div>
<div class="field" style="width: 17%;">
	<input name="filter[nomor_pyr]" placeholder="Nomor PYR" type="text">
</div>
<div class="field">
	<select name="filter[tmuk_kode]" class="ui search dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'TMUK') !!}
	</select>
</div>
<div class="field" style="width: 20%;">
	<select name="filter[vendor_lokal]" class="ui search selection dropdown">
		{!! \Lotte\Models\Master\VendorLokalTmuk::options('nama', 'id',[], '-- Pilih Vendor --') !!}
	</select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
<button type="button" class="ui green button" onclick="javascript:export_pyr();">
	<i class="file excel outline icon"></i>
	Export Excel
</button>
@endsection


@section('subcontent')
<div class="ui top demo tabular menu">
	<div class="active item" data-tab="first">Validasi</div>
	<div class="item" data-tab="second">Histori</div>
</div>
<div class="ui bottom demo active tab segment" data-tab="first">
	@if(isset($tableStruct))
	<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($tableStruct as $struct)
				<th>{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
	@endif
	<br>
	<br>
	<div class="ui grid">
		<div class="twelve wide column">
		</div>
		<div class="four wide column">
			<table class="ui celled compact red table" width="30%" cellspacing="0" align="right">
				<thead>
					<tr>
						<th class="center aligned">Total Pembelian</th>
						<th class="center aligned">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="center aligned"><div id="total-pyr">0</div></td>
						<td class=" center aligned">
							<button type="button" data-content="Proses" class="ui mini green icon proses-pyr  button"><i class="checkmark icon"></i></button>
							<button type="button" data-content="Tolak" class="ui mini red icon tolak-pyr  button"><i class="minus circle icon"></i></button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<br>
	<br>
	<form class="ui data form" id="dataFormGroup" action="{{ url($pageUrl) }}/group" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="status">
		<div id="group-post">
		</div>
	</form>
</div>
<div class="ui bottom demo tab segment" data-tab="second">
	<table id="listTable2" class="ui celled compact green table" width="100%" cellspacing="0">
		<thead>
			<tr>
				@foreach ($tableStruct as $struct)
				<th>{{ $struct['label'] or $struct['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@yield('tableBody')
		</tbody>
	</table>
</div>
@endsection

@section('js-filters')
d.tgl_buat = $("input[name='filter[tgl_buat]']").val();
d.nomor_pyr = $("input[name='filter[nomor_pyr]']").val();
d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
d.vendor_lokal = $("select[name='filter[vendor_lokal]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tgl_buat: {
			identifier: 'tgl_buat',
			rules: [{
				type   : 'empty',
				prompt : 'Tanggal Buat Harus Terisi'
			}]
		},

		nomor_pr: {
			identifier: 'nomor_pr',
			rules: [{
				type   : 'empty',
				prompt : 'Nomor PR Harus Terisi'
			}]
		},

		tmuk_kode: {
			identifier: 'tmuk_kode',
			rules: [{
				type   : 'empty',
				prompt : 'TMUK Harus Terisi'
			}]
		},
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
			type: 'date'
		});

		$('#tabelPyr').DataTable({
			paging: false,
			filter: false,
			lengthChange: false,
			ordering:false,
			scrollX: true,
			scrollY: 300,
		});

		$('#tabelDetailPyr').DataTable({
			paging: false,
			filter: false,
			lengthChange: false,
			ordering:false,
			scrollX: true,
			scrollY: 300,
		});
	};
</script>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	//Calender
	var tot=0;
	$(document).ready(function(){
		$('.demo.menu .item').tab();
	});
	$(document).ready(function(){
		$(document).on('click', '.proses-pyr.button', function(e){
			var cektv='';
			var sama=true;
			$('.ada-post').each(function(){
				if(cektv==''){
					cektv = $(this).data('tmuk') +'-'+$(this).data('vendor');
				}else{
					var sementara = $(this).data('tmuk') +'-'+$(this).data('vendor');
					if(cektv!=sementara){
						sama=false;
					}else{
						cektv=sementara;
					}
				}
			});
			if(sama==true){
				$('#dataFormGroup').find('input[name="status"]').val("1");
				var ada = document.getElementsByClassName("ada-post")
				if($(".ada-post")[0]){
					swal({
						title: 'Apakah anda yakin?',
						text: "Proses Data PYR Yang Sudah Dipilih",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Proses',
						cancelButtonText: 'Batal'
					}).then((result) => {
						$("#dataFormGroup").ajaxSubmit({
							success: function(resp){
								var myNode = document.getElementById("group-post");
								while (myNode.firstChild) {
									myNode.removeChild(myNode.firstChild);
								}
								tot = 0;
								document.getElementById('total-pyr').innerHTML = 0;
								swal(
									'Berhasil!',
									'Data berhasil diproses.',
									'success'
									).then((result) => {
										dt.draw();
										dt2.draw();
										return true;
									})
								},
								error: function(resp){
									if(resp.status==402){
										swal(
											'Gagal!',
											'Beberapa Data sudah di proses sebelumnya, silahkan cek kembali untuk melanjutkan proses.',
											'error'
											).then((result) => {
												return true;
											})
									}else{
										swal(
											'Gagal!',
											'Data gagal diproses.',
											'error'
											).then((result) => {
												return true;
											})
										
									}
									},
								});
					});
				}else{
					swal(
						'Warning!',
						'Harap Pilih Data Untuk Diproses',
						'warning'
						).then((result) => {
							return true;
						});
					}
				}else{
					swal(
						'Warning!',
						'Harap Pilih Data Dengan TMUK Dan Vendor Sama',
						'warning'
						).then((result) => {
							return true;
						});
					}
				});

		$(document).on('click', '.tolak-pyr.button', function(e){
			var cektv='';
			var sama=true;
			$('.ada-post').each(function(){
				if(cektv==''){
					cektv = $(this).data('tmuk') +'-'+$(this).data('vendor');
				}else{
					var sementara = $(this).data('tmuk') +'-'+$(this).data('vendor');
					if(cektv!=sementara){
						sama=false;
					}else{
						cektv=sementara;
					}
				}
			});
			if(sama==true){
				$('#dataFormGroup').find('input[name="status"]').val("3");
				var ada = document.getElementsByClassName("ada-post")
				if($(".ada-post")[0]){
					swal({
						title: 'Apakah anda yakin?',
						text: "Tolak Data PYR Yang Sudah Dipilih",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Tolak',
						cancelButtonText: 'Batal'
					}).then((result) => {
						$("#dataFormGroup").ajaxSubmit({
							success: function(resp){
								var myNode = document.getElementById("group-post");
								while (myNode.firstChild) {
									myNode.removeChild(myNode.firstChild);
								}
								document.getElementById('total-pyr').innerHTML = 0;
								swal(
									'Berhasil!',
									'Data berhasil ditolak.',
									'success'
									).then((result) => {
										dt.draw();
										dt2.draw();
										return true;
									})
								},
								error: function(resp){
									if(resp.status==402){
										swal(
											'Gagal!',
											'Beberapa Data sudah di tolak sebelumnya, silahkan cek kembali untuk melanjutkan.',
											'error'
											).then((result) => {
												return true;
											})
									}else{
										swal(
											'Gagal!',
											'Data gagal ditolak.',
											'error'
											).then((result) => {
												return true;
											})
										
									}
									},
								});
					});
				}else{
					swal(
						'Warning!',
						'Harap Pilih Data Untuk Ditolak',
						'warning'
						).then((result) => {
							return true;
						});
					}
				}else{
					swal(
						'Warning!',
						'Harap Pilih Data Dengan TMUK Dan Vendor Sama',
						'warning'
						).then((result) => {
							return true;
						});
					}
				});
	});

	function convertToRupiah(angka)
	{
		var rupiah = '';		
		var angkarev = angka.toString().split('').reverse().join('');
		for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
			return rupiah.split('',rupiah.length-1).reverse().join('');
	}

	function convertToAngka(rupiah)
	{
		return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
	}

	$(document).on('change', ".group-pyr", function() {
		var ischecked= $(this).is(':checked');
		if(!ischecked){
			// alert('unchecked')
			tot = tot - parseInt($(this).data('beli'));
			$(this).removeAttr("checked");
			var group = "#group"+$(this).val();
			$(group).remove();
		}else{
			// alert('checked')
			$(this).attr('checked', true);
			var html = `<input type="hidden" class="ada-post" name="pyr[][`+$(this).data('tmuk')+`]" data-tmuk="`+$(this).data('tmuk')+`" data-vendor="`+$(this).data('vendor')+`" value="`+$(this).val()+`" id="group`+$(this).val()+`">`;
			$('#group-post').append(html);
			tot = tot + parseInt($(this).data('beli'));
		}
		var total = convertToRupiah(tot);
		// $('.total-pyr').val(tot);
		document.getElementById('total-pyr').innerHTML = total;
	});

	$('.ui.calendar').calendar({
		type: 'date'
	});

	$(document).on('click', '.select.pyr.btn-update', function(e){
		var tr = $(this).closest("tr");
		if($(this).is(':checked')){
			tr.addClass('selected');
		}else{
			tr.removeClass('selected');
		}
		var row = $("#listTable").DataTable().rows('.selected').data();
		var total = 0;
		var count = 0;
		//update status
		//   var id = $(this).data("id");
		// // alert('taraa');

		// $.ajax({
			//              url: "{{url($pageUrl)}}/update-status/"+id,
			//              type: 'GET',
			//              dataType: 'json',
			//          })
		//          .done(function(response) {
			//              window.location.reload();
			//          })
		//          .fail(function() {
			//              console.log("error");
			//          })
		//          .always(function() {
			//              console.log("complete");
			//          });
		//update status
		$.each(row, function(index, val) {
			count += 1;
			total += parseFloat(val.total);
		});

		$("#countPyr").html(count);
		$("#totalPyr").html(total);
	})

	//update status trans_pyr
	$(document).on('click', '.btn-update', function(e){
		var id = $(this).data("id");
		// alert('taraa');

		swal({
			title: 'Apakah anda yakin ?',
			text: "Payment Request akan dilanjutkan ke proses pembayaran !",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: "{{url($pageUrl)}}/update-status/"+id,
					type: 'GET',
					dataType: 'json',
				})
				.done(function(response) {
					swal(
						'Tersimpan!',
						'Data berhasil disimpan, dan akan dilanjutkan ke proses pembayaran.',
						'success'
						).then((result) => {
							dt.draw();
						})
					})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			}
		})
	});

	//tolak
	$(document).on('click', '.btn-tolak', function(e){
		var id = $(this).data("id");
		// alert('taraa');

		swal({
			title: 'Apakah anda yakin ?',
			text: "Data yang sudah ditolak tidak dapat dikembalikan !",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: "{{url($pageUrl)}}/update-status-tolak/"+id,
					type: 'GET',
					dataType: 'json',
				})
				.done(function(response) {
					swal(
						'Tersimpan!',
						'Data berhasil ditolak.',
						'success'
						).then((result) => {
							dt.draw();
						})
					})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			}
		})
	});

	//export pyr
	$(document).ready(function() {
		export_pyr = function(){
			// alert('tara');
			// create form
			var form = document.createElement("form");
			form.setAttribute("method", 'POST');
			form.setAttribute("action", "{{ url('export/export-pyr') }}");
			form.setAttribute("target", "_blank");

			var csrf = document.createElement("input");
			csrf.setAttribute("type", "hidden");
			csrf.setAttribute("name", '_token');
			csrf.setAttribute("value", '{{ csrf_token() }}');
			form.appendChild(csrf);

			var tgl_buat = document.createElement("input");
			tgl_buat.setAttribute("type", "hidden");
			tgl_buat.setAttribute("name", 'tgl_buat');
			tgl_buat.setAttribute("value", $('[name="filter[tgl_buat]"]').val());
			form.appendChild(tgl_buat);

			var nomor_pyr = document.createElement("input");
			nomor_pyr.setAttribute("type", "hidden");
			nomor_pyr.setAttribute("name", 'nomor_pyr');
			nomor_pyr.setAttribute("value", $('[name="filter[nomor_pyr]"]').val());
			form.appendChild(nomor_pyr);

			var tmuk_kode = document.createElement("input");
			tmuk_kode.setAttribute("type", "hidden");
			tmuk_kode.setAttribute("name", 'tmuk_kode');
			tmuk_kode.setAttribute("value", $('[name="filter[tmuk_kode]"]').val());
			form.appendChild(tmuk_kode);

			var vendor_lokal = document.createElement("input");
			vendor_lokal.setAttribute("type", "hidden");
			vendor_lokal.setAttribute("name", 'vendor_lokal');
			vendor_lokal.setAttribute("value", $('[name="filter[vendor_lokal]"]').val());
			form.appendChild(vendor_lokal);

			document.body.appendChild(form);
			form.submit();

			document.body.removeChild(form);
		}
	});
</script>
@append