<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail dari PYR</div>

<div class="content">

	<table id="tabelDetailPyr" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th class="ui center aligned">No</th>
				<th class="ui center aligned">Tanggal PYR</th>
				<th class="ui center aligned">Nomor PYR</th>
				<th class="ui center aligned">TMUK</th>
				<th class="ui center aligned">Vendor</th>
				<th class="ui center aligned">Tanggal Jatuh Tempo</th>
				<th class="ui center aligned">Jumlah Pembelian (Rp)</th>
				<th class="ui center aligned">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@if(count($record->groups)>0)
			<?php $i=1;?>
			@foreach($record->groups as $data)
			<tr>
				<td class="center aligned" width="5%">{{ $i++ }}</td>
				<td class="center aligned" width="10%">{{ $data->tgl_buat }}</td>
				<td class="center aligned" width="15%">{{ $data->nomor_pyr }}</td>
				<td class="center aligned" width="20%">{{ $data->tmuk->kode }} - {{ $data->tmuk->nama }}</td>
				<td class="center aligned" width="20%">{{ $data->vendors($data->id) }}</td>
				<td class="center aligned" width="10%">{{ $data->tgl_jatuh_tempo }}</td>
				<td class="center aligned" width="12%">{{ $data->totals($data->id) }}</td>
				<td class="center aligned" width="8%"><button type="button" class="ui mini orange icon detail-pyrs button" data-content="Detail Pyr" data-id="{{$data->id}}"><i class="eye icon"></i>  Detail</button></td>
			</tr>
			@endforeach
			@else
			<tr>
				<td class="center aligned" width="5%">1</td>
				<td class="center aligned" width="10%">{{ $record->tgl_buat }}</td>
				<td class="center aligned" width="15%">{{ $record->nomor_pyr }}</td>
				<td class="center aligned" width="20%">{{ $record->tmuk->kode }} - {{ $record->tmuk->nama }}</td>
				<td class="center aligned" width="20%">{{ $record->vendors($record->id) }}</td>
				<td class="center aligned" width="10%">{{ $record->tgl_jatuh_tempo }}</td>
				<td class="center aligned" width="12%">{{ $record->totals($record->id) }}</td>
				<td class="center aligned" width="8%"><button type="button" class="ui mini orange icon detail-pyrs button" data-content="Detail Pyr" data-id="{{$record->id}}"><i class="eye icon"></i>  Detail</button></td>
			</tr>
			@endif
		</tbody>
	</table>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <div class="ui blue button">
		Cetak
	</div> --}}
</div>