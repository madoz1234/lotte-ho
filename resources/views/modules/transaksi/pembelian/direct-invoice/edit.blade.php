<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah dari PYR</div>

<div class="content">

	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>Tanggal PYR :</label>
				<input name="" placeholder="Inputkan Tanggal PYR" type="text" value="{{ $record->tgl_buat or "" }}" readonly="">
				{{-- <div class="ui calendar" id="tgl_pyr">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tgl_pyr" type="text" placeholder="Tanggal PYR" value="{{ $record->tgl_buat or "" }}">
					</div>
				</div> --}}
			</div>
			<div class="field">
				<label>TMUK :</label>
				<input name="" placeholder="Inputkan TMUK" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Saldo Escrow Saat Ini (Rp) :</label>
				<input name="" placeholder="Inputkan Saldo Escrow" type="text" value="{{ FormatNumber($record->tmuk->saldo_escrow) }}" readonly="">
			</div>
		</div>
		<div class="three fields">
			<div class="field">
				<label>Nomor PYR :</label>
				<input name="" placeholder="Inputkan Nomor PYR" type="text" value="{{ $record->nomor_pyr or "" }}" readonly="">
			</div>
			{{-- {{ dd($record->detailPyr->first()->produks->produksetting->tipebarang->nama) }} --}}
			<div class="field">
				<label>Tipe Barang :</label>
				<input name="" placeholder="Inputkan Vendor Non Lotte" type="text" value="{{ $record->detailPyr->first()->produks->produksetting->tipebarang->nama }}" readonly="">
			</div>
			<div class="field">
				<label>Vendor :</label>
				<input name="" placeholder="Inputkan Vendor Non Lotte" type="text" value="{{ $vendor or "" }}" readonly="">
			</div>
			<div class="field">
				<label>Alamat Pengiriman :</label>
				<input name="" placeholder="Inputkan Alamat Pengiriman" type="text" value="{{ $record->tmuk->nama or "" }}" readonly="">
			</div>
		</div>

		<table id="tabelPyr" class="ui celled compact red table" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th class="ui center aligned">No</th>
					<th class="ui center aligned">Kode Produk</th>
					<th class="ui center aligned">Nama Produk</th>
					<th class="ui center aligned">Qty</th>
					<th class="ui center aligned">Unit</th>
					<th class="ui center aligned">Jumlah (Rp)<br>( Qty x Harga Satuan )</th>
					<th class="ui center aligned">Aksi</th>
				</tr>
			</thead>
			<tbody>
				@if($record->detailPyr->count() > 0)
					{{-- {{ dd($total) }} --}}
					<?php $i = 1; ?>
					@foreach ($record->detailPyr as $row)
					<tr>
						<td width="5%" class="ui center aligned">{{ $i }}</td>
						<td width="20%" class="ui center aligned">{{ $row->produk_kode }}</td>
						<td width="35%" class="ui left aligned">{{ $row->produks->nama }}</td>
						<td width="15%" class="ui center aligned">
							<input type="hidden" name="detail_id[]" value="{{ $row->id }}">
							<div class="ui fluid input disabled" id='qty-{{ $i }}'>
								<input type="text" name="detail_qty[]" value="{{ $row->qty }}" id="detail-qty-{{$i}}" data-inputmask="'alias' : 'numeric'" onkeyup="validasi({{$i}})">
							</div>
						</td>
						<td width="10%" class="ui center aligned">{{ $row->produks->produksetting->uom1_satuan or '-' }}</td>
						<td width="20%" class="ui center aligned">
							<div class="ui fluid input disabled" id='price-{{ $i }}'>
								<input type="text" name="detail_price[]" value="{{ $row->price }}" id="detail-price-{{$i}}" data-inputmask="'alias' : 'numeric'" onkeyup="sum()">
							</div>
						</td>
						<td width="5%" class="ui center aligned">
							<button type="button" class="ui mini blue icon button" id="btn-edit-{{$i}}" onclick="editable({{$i}})"><i class="edit icon"></i></button>
							<button type="button" class="ui mini green icon button btn-save" id="btn-save-{{$i}}" onclick="uneditable({{$i}})"><i class="checkmark icon"></i></button>
						</td>
					</tr>
					<?php $i++;	?>
					@endforeach
				@else
					<tr>
						<td colspan="11">
							<center><i>Maaf tidak ada data</i></center>
						</td>
					</tr>
				@endif
			</tbody>
			<tfoot class="full-width">
				<tr>
					<th class="ui right aligned" style="background-color: white; border-bottom-color: white"></th>
					<th class="ui right aligned" style="background-color: white; border-bottom-color: white"></th>
					<th class="ui right aligned" style="background-color: white; border-bottom-color: white"></th>
					<th class="ui right aligned" style="background-color: white; border-bottom-color: white"></th>
					<th class="ui right aligned">Total (Rp)</th>
					<th class="ui center aligned">
						<div class="ui fluid input disabled">
							<input type="text" name="total" value="{{ $record->total }}" id="total-price" data-inputmask="'alias' : 'numeric'">
						</div>
					</th>
					<th>
					</th>
				</tr>
			</tfoot>
		</table>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button" id="btn-submit">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	{{-- <div class="ui blue button">
		Cetak
	</div> --}}
</div>

@include('layouts.scripts.inputmask')

<script type="text/javascript">
	$('#tgl_pyr').calendar({
		type: 'date',
	})
	
	$('.btn-save').hide();

	function validasi(id) {
		var val = document.getElementById('detail-qty-'+id).value;

		if (val == '') {
			$('#btn-submit').addClass('disabled');
			swal(
				'Oops!',
				'Qty Tidak Boleh Kosong.',
				'warning'
			).then(function(e){
				dt.draw();
			});
		}else{
			$('#btn-submit').removeClass('disabled');
		}
	}

	function sum() {
		var count = {{$i}}-1;
		var sum = 0;

		for (var i = 1; i <= count; i++) {
			var val = document.getElementById('detail-price-'+i).value;
			var val2 = val.replace(',00', "");
			var new_val = val2.replace(/[^0-9\,-]+/g, "");
			sum += parseFloat(new_val);
		}

		document.getElementById('total-price').value = sum+',00';

		if (sum == 0) {
			$('#btn-submit').addClass('disabled');
			swal(
				'Oops!',
				'Total (Rp) tidak boleh 0.',
				'warning'
			).then(function(e){
				dt.draw();
			});
		}else{
			$('#btn-submit').removeClass('disabled');
		}
	}

	function editable(id) {
		$('#price-'+id).removeClass('disabled');
		$('#qty-'+id).removeClass('disabled');
		$('#btn-edit-'+id).hide();
		$('#btn-save-'+id).show();
	}

	function uneditable(id) {
		$('#price-'+id).addClass('disabled');
		$('#qty-'+id).addClass('disabled');
		$('#btn-edit-'+id).show();
		$('#btn-save-'+id).hide();
	}
</script>