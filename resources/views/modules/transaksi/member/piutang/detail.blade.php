<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Hutang</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>

		<form class="ui data form" id="dataForm">

			<div class="ui error message"></div>
			<div class="two fields">
				<div class="field">
					<label>No Member Card :</label>
					<input name="" placeholder="Nomor Member Card" type="text" value="{{ $tmuk->nomor_member or '' }}" readonly="">
				</div>
				<div class="field">
					<label>Nama Member :</label>
					<input name="" placeholder="Nomor PO" type="text" value="{{ $tmuk->br_name or '' }}" readonly="">
				</div>
			</div>
			<div class="field">
				<label>TMUK :</label>
				<input name="" placeholder="TOTAL" type="text" value="{{ $tmuk->tmuk ? $tmuk->tmuk->kode : '' }} - {{ $tmuk->tmuk ? $tmuk->tmuk->nama : '' }}" readonly="">
			</div>
		</form>
		<br>


		<!-- Produk -->
		<table class="ui compact celled table" id="listTableDetail">
			<thead class="full-width center">
				<tr align="center">
					<th>Tanggal</th>
					<th>Hutang</th>
				</tr>
			</thead>
			<tbody>
				@if($record)
				@foreach($record->detail->sortByDesc('tanggal') as $detail)
				<tr>
					<td style="text-align: center;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $detail->tanggal)->format('m/d/Y H:i') }}</td>
					<td style="text-align: center;">{{ $detail->hutang }}</td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
		<!-- Produk -->
	</form>

</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
</div>