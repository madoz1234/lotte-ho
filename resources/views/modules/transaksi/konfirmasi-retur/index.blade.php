@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
@append
@section('filters')
<div class="field">		
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[created_at]" type="text" placeholder="Tanggal PR">
		</div>
	</div>
</div>

<div class="field">
	<input name="filter[nomor_retur]" placeholder="Nomor RR" type="text">
</div>

<div class="field">
	<select name="filter[tmuk_kode]" class="ui search dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'TMUK') !!}
	</select>
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
    d.created_at = $("input[name='filter[created_at]']").val();
    d.nomor_retur = $("input[name='filter[nomor_retur]']").val();
    d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
@endsection

@section('toolbars')
<button type="button" class="ui green button" onclick="javascript:export_konfirmasi_retur();">
	<i class="file excel outline icon"></i>
	Export Excel
</button>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
		$('.demo.menu .item').tab({
			history:false,
		});
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});
	};
</script>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

	$(document).ready(function() {
		export_konfirmasi_retur = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-konfirmasi-retur') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var created_at = document.createElement("input");
		            created_at.setAttribute("type", "hidden");
		            created_at.setAttribute("name", 'created_at');
		            created_at.setAttribute("value", $('[name="filter[created_at]"]').val());
		        form.appendChild(created_at);

		        var nomor_retur = document.createElement("input");
		            nomor_retur.setAttribute("type", "hidden");
		            nomor_retur.setAttribute("name", 'nomor_retur');
		            nomor_retur.setAttribute("value", $('[name="filter[nomor_retur]"]').val());
		        form.appendChild(nomor_retur);

		        var tmuk_kode = document.createElement("input");
		            tmuk_kode.setAttribute("type", "hidden");
		            tmuk_kode.setAttribute("name", 'tmuk_kode');
		            tmuk_kode.setAttribute("value", $('[name="filter[tmuk_kode]"]').val());
		        form.appendChild(tmuk_kode);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
	@append

	@section('subcontent')
		@parent
	@endsection