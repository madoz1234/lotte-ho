<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Konfirmasi Retur</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
    	<input type="hidden" name="id" value="{{ $record->id }}">
    	<input type="hidden" name="tmuk_kode" value="{{ $record->tmuk_kode }}">
		<div class="ui error message"></div>

		<div class="three fields">
			<div class="field">
				<label>Tanggal RR</label>
				<input placeholder="Tanggal PR" type="text" readonly="" value="{{ $record->created_at->format('Y-m-d') }}">
			</div>

			<div class="field">
				<label>Nomor RR</label>
				<input placeholder="Nomor PR" type="text" readonly="" value="{{ $record->nomor_retur }}">
			</div>
			<div class="field">
				<label>Nomor PO</label>
				<input placeholder="Nomor PO" type="text" readonly="" value="{{ $record->po->nomor }}">
			</div>
		</div>
		<div class="three fields">
			<div class="field">
				<label>LSI</label>
				<input placeholder="LSI" type="text" readonly="" value="{{ $record->tmuk->lsi->nama }}">
			</div>

			<div class="field">
				<label>TMUK</label>
				<input placeholder="TMUK" type="text" readonly="" value="{{ $record->tmuk->nama }}">
			</div>

			<div class="field">
				<label>Alamat Pengiriman :</label>
				<input placeholder="Inputkan Alamat Pengiriman" type="text" value="{{ $record->tmuk->alamat or "" }}" readonly="">
			</div>
		</div>


		<!-- Filter -->
		<br>
		<!-- List Table -->
		<div class="field">

			<table class="ui celled striped table">
				<thead>
					<tr align="center">
						<th rowspan="2">No</th>
						<th rowspan="2">Kode Produk</th>
						<th rowspan="2">Nama Produk</th>
						<th rowspan="2">Barcode</th>
						<th colspan="2">PR Qty</th>
						<th rowspan="2">Harga (Rp)</th>
						<th rowspan="2">Total Harga (Rp)</th>
						<th rowspan="2">Alasan</th>
						{{-- <th rowspan="2">Persetujuan</th> --}}
					</tr>
					<tr>
						<th class="ui center aligned">Qty</th>
						<th class="ui center aligned">Unit</th>
					</tr>
				</thead>
				<tbody>
					@if($detail->count() > 0)
					<?php $i = 1;?>
					@foreach ($detail as $row)
					<tr>
						<td class="ui center aligned">{{ $i }}</td>
						<td class="ui center aligned">{{ $row['produk_kode'] }}</td>
						<td class="ui center aligned">{{ $row['nama'] }}</td>
						<td class="ui center aligned">{{ $row['uom1_barcode'] }}</td>
						<td class="ui center aligned">{{ $row['qty'] }}</td>
						<td class="ui center aligned">{{ $row['uom1_satuan'] }}</td>
						<td class="ui right aligned">{{ $row['harga_satuan'] }}</td>
						<td class="ui right aligned">{{ $row['harga_total'] }}</td>
						<td class="ui center aligned">{{ $row['catatan'] }}</td>
						{{-- <td class="ui center aligned"><button type="button" class="ui mini green icon button" data-content="Tervalidasi" data-id="1">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">30/01/18, 10:45</i></td> --}}
					</tr>
				<?php $i++;	?>
				@endforeach
			@else
				<tr>
					<td colspan="11">
						<center><i>Maaf tidak ada data</i></center>
					</td>
				</tr>
			@endif
				</tbody>
			</table>
		</div>
		<!-- List Table -->
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Setujui
		<i class="checkmark icon"></i>
	</div>
	{{-- <a class="ui default right labeled icon save button" target="_blank" href="{{ url($pageUrl) }}/print-konfirmasi">
		Print
		<i class="print icon"></i>
	</a> --}}
	
</div>