<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Konfirmasi Retur</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<form class="ui data form" id="dataForm">

			<div class="ui error message"></div>

			<div class="three fields">
				<div class="field">
					<label>Tanggal RR</label>
					<input placeholder="Tanggal PR" type="text" readonly="">
				</div>

				<div class="field">
					<label>Nomor RR</label>
					<input placeholder="Nomor PR" type="text" readonly="">
				</div>
				<div class="field">
					<label>Nomor PO</label>
					<input placeholder="Nomor PO" type="text" readonly="">
				</div>
			</div>
			<div class="three fields">
				<div class="field">
					<label>LSI</label>
					<input placeholder="LSI" type="text" readonly="">
				</div>

				<div class="field">
					<label>TMUK</label>
					<input placeholder="TMUK" type="text" readonly="">
				</div>

				<div class="field">
					<label>Note</label>
					<input placeholder="Note" type="text" readonly="">
				</div>
			</div>
		</form>


		<!-- Filter -->
		<br>
		<!-- List Table -->
		<div class="field">

			<table class="ui celled striped table">
				<thead>
					<tr align="center">
						<th rowspan="2">No</th>
						<th rowspan="2">Kode Produk</th>
						<th rowspan="2">Nama Produk</th>
						<th rowspan="2">Barcode</th>
						<th colspan="2">PR Qty</th>
						<th rowspan="2">Harga (Rp)</th>
						<th rowspan="2">Total Harga (Rp)</th>
						<th rowspan="2">Alasan</th>
						<th rowspan="2">Persetujuan</th>
						{{-- <th colspan="2">PR Qty Disetujui</th> --}}
					</tr>
					<tr>
						<th class="ui center aligned">Qty</th>
						<th class="ui center aligned">Unit</th>
						{{-- <th class="ui center aligned">Qty</th>
						<th class="ui center aligned">Unit</th> --}}
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="ui center aligned">1</td>
						<td class="ui center aligned">1052362000</td>
						<td class="ui center aligned">SARIMI 2 SOTO AYAM 24PCS/CT</td>
						<td class="ui center aligned">089686917335</td>
						<td class="ui center aligned">1</td>
						<td class="ui center aligned">pcs</td>
						<td class="ui right aligned">2.000</td>
						<td class="ui right aligned">2.000</td>
						<td class="ui center aligned">Barang Bagus</td>
						<td class="ui center aligned"><button type="button" class="ui mini green icon button" data-content="Tervalidasi" data-id="1">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">30/01/18, 10:45</i></td>
						{{-- <td class="ui center aligned"><input type="number"></td>
						<td class="ui center aligned">pcs</td> --}}
					</tr>
				</tbody>
			</table>
		</div>
		<!-- List Table -->
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
	<a class="ui default right labeled icon save button" target="_blank" href="{{ url($pageUrl) }}/print-konfirmasi">
		Print
		<i class="print icon"></i>
	</a>
	
</div>