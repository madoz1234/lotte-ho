@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field">		
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tgl_po]" type="text" placeholder="Tgl PO">
		</div>
	</div>
</div>

<div class="field">
	<input name="filter[no_po]" placeholder="No PO" type="text">
</div>

<div class="field">
	<select name="filter[tmuk_kode]" class="ui search dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'TMUK') !!}
	</select>
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>

<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>

@endsection

@section('js-filters')
d.po = $("input[name='filter[no_po]']").val();
d.tgl = $("input[name='filter[tgl_po]']").val();
d.tmuk = $("select[name='filter[tmuk_kode]']").val();
@endsection

@section('toolbars')
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		// validasi
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).on('click', '.print.label-kontainer.button', function(e){
			var id = $(this).data('id');
			var url = "{{ url($pageUrl) }}/print-label-kontainer/"+id;

			window.open(
			  url,
			  '_blank' // <- This is what makes it open in a new window.
			);

			// window.location = url;
		});
		$('.ui.calendar').calendar({
			type: 'date'
		});
	</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

		$('#tabelKontainer').DataTable({
			pageLength: 5,
			filter: false,
			lengthChange: false,
			filter: false,
			ordering:false,
		})
		$('#tabelKontainer2').DataTable({
			pageLength: 5,
			filter: false,
			lengthChange: false,
			filter: false,
			ordering:false,
		})
	};
</script>
@endsection

{{-- @section('tableBody')
<tr>
	<td style="text-align: center;">1</td>
	<td style="text-align: center;">29/01/18</td>
	<td style="text-align: center;">PO-1707070600700017411</td>
	<td style="text-align: left;">0600700018 - Pragma Informatika</td>
	<td style="text-align: center;">2</td>
	<td style="text-align: center;">2</td>
	<td style="text-align: center;">
		<button type="button" class="ui mini orange icon edit button" data-content="Detil Kontainer" data-id="1"><i class="edit icon"></i> Detil</button>
		<a target="_blank" href="{{ url($pageUrl) }}/print-label-kontainer" class="ui mini default icon print button" data-content="Print Detil Kontainer" data-id="1"><i class="print icon"></i> Print</a>
	</td>
</tr>
@endsection --}}