<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detil Kontainer</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<form class="ui data form" id="dataForm">

			<div class="ui error message"></div>
			<div class="two fields">
				<div class="field">
					<label>Tanggal PO :</label>
					<input name="" placeholder="Tanggal PO" type="text" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $record->po->tgl_buat)->format('d/m/y') }}" disabled="">
				</div>
				<div class="field">
					<label>TMUK :</label>
					<input name="" placeholder="TMUK" type="text" value="{{ $record->tmuk->kode }} - {{ $record->tmuk->nama }}" disabled="">
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>Nomor PO :</label>
					<input name="" placeholder="Nomor PO" type="text" value="{{ $record->po_nomor }}" disabled="">
				</div>
				<div class="field">
					<label>TOTAL :</label>
					<?php $max = $record->detail->max('no_kontainer');?>
					<?php $jum_kontainer = $record->detail()->whereNotNull('id_kontainer')->max('no_kontainer');?>
					<?php $jum_outer = $record->detail->where('id_kontainer',null)->count();?>
					<input name="" placeholder="TOTAL" type="text" value="{{ $jum_kontainer }} Kontainer & {{ $jum_outer }} Outer Box" disabled="">
				</div>
			</div>
		</form>


		<!-- Produk -->
		<?php $ii=1;?>
		<table class="ui compact celled table" id="tabelKontainer">
			<thead class="full-width center">
				<tr align="center">
					<th>Kontainer</th>
					<th>Kategori</th>
					<th>Nama Produk</th>
					<th>Qty</th>
				</tr>
			</thead>
			<tbody>
				@foreach($record->detail as $detail)
					@if($detail->id_kontainer!=null)
					<tr>
						<td>{{ $detail->no_kontainer }}/{{$max}} - {{ $detail->kontainer->nama }}</td>
						<td>{{ $detail->produk->produksetting->jenisbarang->jenis }}</td>
						<td>{{ $detail->produk->nama }}</td>
						<td style="text-align: center;">{{ $detail->jumlah_produk }}</td>
					</tr>
					@endif
				@endforeach
			</tbody>
		</table>
		<div class="ui divider"></div>
		<table class="ui compact celled table" id="tabelKontainer2">
			<thead class="full-width center">
				<tr align="center">
					<th>Outer Box</th>
					<th>Nama Produk</th>
					{{-- <th>Qty</th> --}}
				</tr>
			</thead>
			<tbody>
				<?php $out = $jum_kontainer+1;?>
				@foreach($record->detail as $detail)
					@if($detail->id_kontainer==null)
						<tr>
							<td style="text-align: center;">{{ $out++ }}/{{$max}}</td>
							<td>{{ $detail->produk->nama }}</td>
							{{-- <td style="text-align: center;">{{ $detail->jumlah_produk }}</td> --}}
						</tr>
					@endif
				@endforeach

			</tbody>
		</table>
		<!-- Produk -->
	</form>

</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<a class="ui default right labeled icon save button" target="_blank" href="{{ url($pageUrl) }}/print-label-kontainer/{{$record->id}}">
		Print
		<i class="print icon"></i>
	</a>
	{{-- <a target="_blank" href="{{ url($pageUrl) }}/print-label-kontainer" class="ui mini default icon print button" data-content="Print Picking" data-id="1"><i class="print icon"></i> Print</a> --}}
</div>