@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field">		
	<div class="ui calendar" id="from">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[tgl_po]" type="text" placeholder="Tanggal PO">
		</div>
	</div>
</div>

<div class="field">
	<input name="filter[no_po]" placeholder="Nomor PO" type="text">
</div>

<div class="field">
	<select name="filter[tmuk_kode]" class="ui search dropdown">
		{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode', [], 'Semua TMUK') !!}
	</select>
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>

<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>

@endsection

@section('js-filters')
d.po = $("input[name='filter[no_po]']").val();
d.tgl = $("input[name='filter[tgl_po]']").val();
d.tmuk = $("select[name='filter[tmuk_kode]']").val();
@endsection

@section('toolbars')
<button type="button" class="ui green add-group-truk button">
	<i class="plus icon"></i>
	Group Data
</button>
@endsection


@section('subcontent')
@if(isset($tableStruct))
<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
	<thead>
		<tr>
			@foreach ($tableStruct as $struct)
			<th>{{ $struct['label'] or $struct['name'] }}</th>
			@endforeach
		</tr>
	</thead>
	<tbody>
		@yield('tableBody')
	</tbody>
</table>
@endif

<div class="ui small modal" id="formGroup">
	{{-- <div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
	</div> --}}
	<div class="header">Data Kendaraan</div>
	<div class="content">
		<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}/group" method="POST">
			{!! csrf_field() !!}

			<div id="group-post">
			</div>
			<div class="two fields">
				<div class="field">
					<label>Nama Supir</label>	
					<input type="text" name="supir" placeholder="Nama Supir">
				</div>
				<div class="field">
					<label>Nomor Kendaraan</label>	
					<input type="text" name="nomor_kendaraan" placeholder="Nomor Kendaraan">
				</div>
			</div>
		</form>
	</div>
	<div class="actions">
		<div class="ui negative button" style="background: grey;">
			Batal
		</div>
		<div class="ui green right labeled icon group button">
			Simpan
			<i class="checkmark icon"></i>
		</div>
	</div>
</div>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		supir: 'empty',
		nomor_kendaraan: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
			type: 'date'
		});
	};
</script>
@endsection

@section('scripts')
<script type="text/javascript">
	$('.ui.calendar').calendar({
		type: 'date'
	});
	$(document).ready(function(){
		$(document).on('click', '.add-group-truk.button', function(e){
			var ada = document.getElementsByClassName("ada-post")
			if($(".ada-post")[0]){
				$('#formGroup').modal({
					inverted: false,
					observeChanges: true,
					closable: false,
					detachable: false, 
					autofocus: false,
					centered: true,
					onShow: function(){
						$('#dataForm').form({
							inline: true,
							fields: formRules
						});
					}
				}).modal('show');
			}else{
				swal(
					'Warning!',
					'Harap Pilih Data Untuk Digroup',
					'warning'
					).then((result) => {
						return true;
					});
				}
			});
	});
	$(document).on('click', '.group.button', function(e){
		$("#dataForm").form('validate form');
		if($("#dataForm").form('is valid')){

			$("#dataForm").ajaxSubmit({
				success: function(resp){
					var myNode = document.getElementById("group-post");
					while (myNode.firstChild) {
						myNode.removeChild(myNode.firstChild);
					}
					$('#formGroup').modal('hide');
					swal(
						'Berhasil!',
						'Data berhasil digroup.',
						'success'
						).then((result) => {
							dt.draw();
							return true;
						})
					},
					error: function(resp){
						swal(
							'Gagal!',
							'Data gagal digroup.',
							'error'
							).then((result) => {
								return true;
							})
						},
					});
		}
	});

	$(document).on('change', ".group-truk", function() {
		var ischecked= $(this).is(':checked');
		if(!ischecked){
			// alert('unchecked')
			$(this).removeAttr("checked");
			var group = "#group"+$(this).val();
			$(group).remove();
		}else{
			// alert('checked')
			$(this).attr('checked', true);
			var html = `<input type="hidden" class="ada-post" name="truk[][`+$(this).data('tmuk')+`]" value="`+$(this).val()+`" id="group`+$(this).val()+`">`;
			$('#group-post').append(html);
		}
	});
</script>
@endsection
