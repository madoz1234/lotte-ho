<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        html, body {
                height: 100%;
                overflow: hidden;
        }
        * {
          overflow: visible !important;
        }
        .tables, .tables td, .tables th {    
            /*border: 1px solid #ddd;*/
            text-align: left;
            font-size: 12px;
        }
        .tables th {
            background-color: #e1f5f3;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font-size: 11px !important;
        }
        .tables td{
            font-size: 10px !important;
        }
        .tables {
            border-collapse: collapse;
            width: 100%;
        }

        .tables th, .tables td {
            padding: 4px;
        }
        /* Page Breaks */

        /***Always insert a page break before the element***/
        .pb_before {
           page-break-before: always !important;
        }

        /***Always insert a page break after the element***/
        .pb_after {
           page-break-after: always !important;
        }

        /***Avoid page break before the element (if possible)***/
        .pb_before_avoid {
           page-break-before: avoid !important;
        }

        /***Avoid page break after the element (if possible)***/
        .pb_after_avoid {
           page-break-after: avoid !important;
        }

        /* Avoid page break inside the element (if possible) */
        .pbi_avoid {
           page-break-inside: avoid !important;
        }
    </style>
  </head>
  <body>
    <div class="pb_after">
        <table class="" border="0" align="center">
            <tr>
                <td id="" style="; width: 90px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -4px;margin-bottom:-4px;">
                </td>
                <td id="text_header" style=" border-bottom:1px solid #F90606; width: 820px;text-align: center;">
                    <p class="title" style=""><h3>Laporan Audit </h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>Tanggal Cetak </td>
                <td>: {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
            </tr>
            <tr>
                <td>Tahun Fiskal</td>
                <td>: {{-- {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }}  --}}(Active)</td>
            </tr>
            <tr>
                <td>Periode</td>
                <td>: {{ $req->start_date }} - {{ $req->end_date }} </td>
            </tr>
            <tr>
                <td>User</td>
                <td>: {{ $user }} </td>
            </tr>
        </table>
        <br>
        <table class="ui celled compact red table tables" width="100%" cellspacing="0" cellpadding="0" margin="0" padding ="0">
                <tr>
                    <th class="center aligned page_content_header">Date</th>
                    <th class="center aligned page_content_header">User</th>
                    <th class="center aligned page_content_header">Trans Date</th>
                    <th class="center aligned page_content_header">Type</th>
                    <th class="center aligned page_content_header">Acrion</th>
                    <th class="center aligned page_content_header">Amount</th>
                </tr>
               @foreach ($record as $row)
                   <tr>
                       <td>{{ $row->tanggal }}</td>
                       <td>{{ $row->user->name }}</td>
                       <td>{{ $row->tanggal_transaksi }}</td>
                       <td>{{ $row->type }}</td>
                       <td>{{ $row->aksi }}</td>
                       <td>{{ rupiah($row->amount) }}</td>
                   </tr>
               @endforeach
        </table>
    </div>
  </body>
</html>

