<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Toko LSI</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm">
		<div class="ui equal width grid">
			<div class="column">
				<h4 class="ui dividing header">Data Toko LSI</h4>
				<div class="required field">
					<label>Region</label>		
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="region_id">
						<i class="dropdown icon"></i>
						<div class="default text">Pilih Region</div>
						<div class="menu">
							<div class="item" data-value="1">Jabodetabek</div>
							<div class="item" data-value="2">Bandung</div>
							<div class="item" data-value="3">Surabaya</div>
							<div class="item" data-value="4">Banten</div>
						</div>
					</div>
				</div>
				<div class="field">		
					<div class="required fields">
						<div class="four wide field">
							<label>Kode LSI</label>
							<input type="text" name="kode" placeholder="Kode LSI">
						</div>
						<div class="twelve wide field">
							<label>Nama LSI</label>
							<input type="text" name="nama" placeholder="Nama LSI" value="LOTTE GROSIR ">
						</div>
					</div>
				</div>
				<div class="required field">
					<label>Alamat</label>
					<div class="fields">
						<div class="thirteen wide field">
							<input type="text" name="alamat" placeholder="Alamat Jalan">
						</div>
						<div class="three wide field">
							<input type="text" name="kode_pos" placeholder="Kode Pos">
						</div>
					</div>
					<div class="four fields">
						<div class="field">
							<label class="hidden">Provinsi</label>
							<div class="ui fluid search selection dropdown">
								<input type="hidden" name="provinsi_id">
								<i class="dropdown icon"></i>
								<div class="default text">Provinsi</div>
								<div class="menu">
									<div class="item" data-value="1">DKI Jakarta</div>
									<div class="item" data-value="2">Jawa Barat</div>
									<div class="item" data-value="3">Jawa Tengah</div>
									<div class="item" data-value="4">Jawa Timur</div>
									<div class="item" data-value="..">...</div>
								</div>
							</div>
						</div>
						<div class="field">
							<label class="hidden">Kota</label>
							<div class="ui fluid search selection dropdown">
								<input type="hidden" name="kota_id">
								<i class="dropdown icon"></i>
								<div class="default text">Kota / Kabupaten</div>
								<div class="menu">
									<div class="item" data-value="1">Bandung</div>
									<div class="item" data-value="2">Sumedang</div>
									<div class="item" data-value="3">Tasik</div>
									<div class="item" data-value="4">Garut</div>
									<div class="item" data-value="..">...</div>
								</div>
							</div>
						</div>
						<div class="field">
							<label class="hidden">Kecamatan</label>
							<div class="ui fluid search selection dropdown">
								<input type="hidden" name="kecamatan_id">
								<i class="dropdown icon"></i>
								<div class="default text">Kecamatan</div>
								<div class="menu">
									<div class="item" data-value="1">Cileunyi</div>
									<div class="item" data-value="2">Soreang</div>
									<div class="item" data-value="3">Arjasari</div>
									<div class="item" data-value="..">...</div>
								</div>
							</div>
						</div>
						<div class="field">
							<label class="hidden">Kelurahan</label>
							<div class="ui fluid search selection dropdown">
								<input type="hidden" name="kelurahan_id">
								<i class="dropdown icon"></i>
								<div class="default text">Kelurahan</div>
								<div class="menu">
									<div class="item" data-value="1">Lebakwangi</div>
									<div class="item" data-value="2">Batu Karut</div>
									<div class="item" data-value="3">Arjasari</div>
									<div class="item" data-value="..">...</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="field">
					<label>GPS Coordinates</label>
					<div class="two fields">
						<div class="field">
							<input type="text" name="long" placeholder="Longitude">
						</div>
						<div class="field">
							<input type="text" name="lat" placeholder="Latitude">
						</div>
					</div>
				</div>
				<div class="two required fields">
					<div class="field">
						<label>Telepon</label>
						<input type="text" name="telepon" placeholder="Telepon">
					</div>
					<div class="field">
						<label>Email</label>
						<input type="text" name="email" placeholder="Email">
					</div>
				</div>
			</div>
			<div class="column">
				<h4 class="ui dividing header">Keuangan &amp; Pajak</h4>
				<div class="field">
					<div class="two required fields">
						<div class="field">
							<label>NPWP</label>
							<input type="text" name="npwp" placeholder="Nomor NPWP">
						</div>
						<div class="field">
							<label>Nama Perusahaan</label>
							<input type="text" name="nama_perusahaan" placeholder="Nama Perusahaan (Sesuai NPWP)">
						</div>
					</div>
					<div class="required field">
						<label>Alamat</label>
						<input type="text" name="alamat_perusahaan" placeholder="Alamat Perusahaan (Sesuai NPWP)">
					</div>
				</div>
				<div class="required field">
					<label>PKP</label>
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
						        <input name="pkp" class="hidden" type="radio">
						    	<label>Ya</label>
						    </div>
						</div>
						<div class="field">
							<div class="ui radio checkbox">
						        <input name="pkp" class="hidden" type="radio">
						    	<label>Tidak</label>
						    </div>
					    </div>
					</div>
				</div>
				<div class="required field">
					<label>Akun Escrow</label>
					<input type="text" name="escrow" placeholder="Akun Escrow">
				</div>
				<div class="field">
					<div class="two required fields">
						<div class="field">
							<label>Kode Bank</label>
							<input type="text" name="kode_bank" placeholder="Kode Bank">
						</div>
						<div class="field">
							<label>Nama Bank</label>
							<input type="text" name="nama_bank" placeholder="Nama Bank">
						</div>
					</div>
				</div>
				<div class="required field">
					<label>Nomor Rekening</label>
					<input type="text" name="rekening_bank" placeholder="Nomor Rekening Bank">
				</div>
			</div>
		</div>

		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>