@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append


@section('filters')

<div class="field inline">
	<div class="ui calendar labeled input start_date" id="start">
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[start_date]" type="text" value="{{ $start_date or '' }}" placeholder="Tanggal Mulai">
		</div>
	</div>
	<div class="ui calendar labeled input end_date" id="end"> 
		s.d.&nbsp;
		<div class="ui input left icon">
			<i class="calendar icon date"></i>
			<input name="filter[end_date]" type="text" value="{{ $end_date or '' }}" placeholder="Tanggal Selesai">
		</div>
	</div>
</div>

<div class="field" style="width: 15%;">
	<select name="filter[user]" class="ui fluid selection dropdown">
		{{-- {!! \Lotte\Models\Entrust\Pengguna::options('nama_lengkap', 'user_id',[], '-- Pilih User --') !!} --}}
		@foreach(\Lotte\Models\User::get() as $data)
			<option value="">Nama User</option>
			<option value="{{ $data->id }}">{{ $data->name }}</option>
		@endforeach
	</select>
</div>
{{-- <div class="field" style="width: 15%;">
	<select name="filter[tes]" class="ui fluid selection dropdown">
		{!! \Lotte\Models\User::options('name', 'id',[], '-- Pilih User --') !!}
	</select>
</div> --}}
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
{{-- <button type="button" class="ui green button btn-export" onclick="javascript:export_region();"> Export</button> --}}
@endsection
@section('toolbars')
<div class="ui form" style="float:right">
<a class="ui default right labeled icon save button" onclick="printAudit();">
		Print
		<i class="print icon"></i>
	</a>
	{{-- <div class="ui green export buttons" style="margin-right: 5px">
		<div class="ui button" onclick="printLaporan('exel-laporan-akun-kas-bank','akun-kas-bank');"><i class="file excel outline icon"></i>Export</div>
	</div> --}}
</div>
	{{-- <button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_region();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button> --}}
	@endsection

	@section('js-filters')
	d.user = $("select[name='filter[user]']").val();
	d.start_date = $("input[name='filter[start_date]']").val();
	d.end_date = $("input[name='filter[end_date]']").val();
	@endsection

	@section('rules')
	<script type="text/javascript">
		formRules = {
			kode: {
				identifier: 'kode',
				rules: [{
					type   : 'empty',
					prompt : 'Isian kode tidak boleh kosong'
				}]
			},
			area: {
				identifier: 'area',
				rules: [{
					type   : 'empty',
					prompt : 'Isian area tidak boleh kosong'
				},{
					type   : 'minLength[2]',
					prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
				},{
					type   : 'maxLength[30]',
					prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
				}]
			}
		};
	</script>
	@endsection

	@section('init-modal')
	<script type="text/javascript">
		initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
			type: 'date'
		});
	};


</script>
@endsection

@section('scripts')
<script type="text/javascript">
	$('.start_date').calendar({
		type: 'date',
		endCalendar: $('.end_date'),
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day+'/'+month+'/'+year;
			}
		}
	});

	$('.end_date').calendar({
		type: 'date',
		startCalendar: $('.start_date'),
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day+'/'+month+'/'+year;
			}
		}
	});
	$(document).ready(function() {
		// $('.ui.calendar').calendar({
		// 	type: 'date',
		// 	formatter: {
		// 		date: function (date, settings) {
		// 			if (!date) return '';
		// 			var day = date.getDate();
		// 			var month = date.getMonth() + 1;
		// 			var year = date.getFullYear();
		// 			return day+'/'+month+'/'+year;
		// 		}
		// 	}
		// });

			// New Post Tab
			postNewTab = function(url, param)
			{
				var form = document.createElement("form");
				form.setAttribute("method", 'POST');
				form.setAttribute("action", url);
				form.setAttribute("target", "_blank");

				$.each(param, function(key, val) {
					var inputan = document.createElement("input");
					inputan.setAttribute("type", "hidden");
					inputan.setAttribute("name", key);
					inputan.setAttribute("value", val);
					form.appendChild(inputan);
				});

				document.body.appendChild(form);
				form.submit();

				document.body.removeChild(form);
			}

			printAudit = function(){
	    	// alert('tara');
	    	var url = '{{ url($pageUrl) }}/audit';

		    	postNewTab(url, {
		    		_token 	 : "{{ csrf_token() }}",
					user  	: $('[name="filter[user]"]').val(),
					start_date  	: $('[name="filter[start_date]"]').val(),
					end_date  	: $('[name="filter[end_date]"]').val(),
		    	});
		    }
		});
	</script>
	@append