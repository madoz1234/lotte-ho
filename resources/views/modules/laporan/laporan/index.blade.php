@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style>

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@append

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		filter = function(v, menu=''){
			showTableFilter(v,menu);
		};

		$('.treeview.accordion')
		.accordion({
			selector: {
				trigger: '.title'
			}
		})
		;

		$(".treeview.accordion").click(function(event) {
			$('.title').removeAttr('style');
			$('.title.active').css("font-weight","Bold");
		});

		var dt = $('#reportTable').DataTable({
			dom: 'rt<"bottom"ip><"clear">',
		});

		$("#searchbox").keyup(function() {
			dt.search(this.value).draw();
		});   

		$('.ui.calendars').calendar({
			type: 'text',
			formatDate: 'yyyy-mm-dd'
		});

		$(document).on('click', '#filter_tmuk', function(e){
			var tmuk = $('#tmuk').val();
			if (tmuk != 0) {
				$('#menu').removeClass('hidden');
				$('#alert').addClass('hidden');
			}else{
				$('#menu').addClass('hidden');
				$('#alert').removeClass('hidden');
			}
		});

		$(document).on('click', '#reset_tmuk', function(e){
			$('#menu').addClass('hidden');
			$('.lsi').addClass('disabled');
			$('#lsi_id').html('<option value="0">-- Pilih LSI --</option>');
			$('.tmuk').addClass('disabled');
			$('#tmuk').html('<option value="0">-- Pilih TMUK --</option>');
			$('#alert').removeClass('hidden');

		    $('.twelve.wide.column').html('');
		    $('.title').removeClass('active');
		    $('.content').removeClass('active');
		    $(".title").removeAttr("style");
		});
	});

	$(document).ready(function(e){
				$('[name=region_id]').change(function(e){
					$(".loading").addClass('active');
					$.ajax({
						url: "{{ url('ajax/option/region') }}",
						type: 'GET',
						data:{
							id_region : $(this).val(),
						},
					})
					.success(function(response) {
						$(".loading").removeClass('active');
						
						$('.lsi').removeClass('disabled');
						$('[name=lsi_id]').html(response);
						//clear select all
						// $('.tmuk').addClass('disabled');
						// $('.tmuk').dropdown('clear');
						$('#selectall').prop("checked", false);

					})
					.fail(function() {
						console.log("error");
						$(".loading").removeClass('active');
					});
				});


				$('[name=lsi_id]').change(function(e){
					//generate produk by lsi
					$(".loading").addClass('active');

					$.ajax({
						url: "{{ url('ajax/option/lsi') }}",
						type: 'GET',
						data:{
							id_lsi : $(this).val(),
						},
					})
					.success(function(response) {
						$(".loading").removeClass('active');

						// $('.kopro').removeClass('disabled');
						// $('.btn-delete').removeClass('disabled');
						// $('.btn-add').removeClass('disabled');
						// $('#trigger_kode_produk_x').html(response);
						// $('#trigger_kode_produk_y').html(response);
						// $('#trigger_purchase_kode_produk').html(response);
					})
					.fail(function() {
						console.log("error");
					});

					//generate tmuk by lsi
					$.ajax({
						url: "{{ url('ajax/option/lsi-tmuk') }}",
						type: 'GET',
						data:{
							id_lsi : $(this).val(),
						},
					})
					.success(function(response) {
						// $('.tmuk').dropdown('clear');
						// $('#selectall').prop("checked", false);

						$('.tmuk').removeClass('disabled');
						$('#tmuk').html(response);
					})
					.fail(function() {
						console.log("error");
					});
				});

			});

	showTable = function(v, menu=''){
		$(".loading").addClass('active');
		$('#mulai_filter').val('');
		$('#selesai_filter').val('');
		$.ajax({
			url: '{{  url($pageUrl) }}/grid'+menu,
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				type 		: v,
				status  	: $("#status").val(),
				tmuk_kode 	: $('[name="filter[tmuk_kode]"]').val(),
				start 		: $('#mulai_filter').val(),
				end 		: $('#selesai_filter').val(),
				tipe   		: $('[name="filter[tipe]"]').val(),
				vendor 		: $('[name="filter[vendor]"]').val(),

			},
		})
		.done(function(response) {
			// New Post Tab
			$(".loading").removeClass('active');
			
			postNewTab = function(url, param)
			{
				var form = document.createElement("form");
				form.setAttribute("method", 'POST');
				form.setAttribute("action", url);
				form.setAttribute("target", "_blank");

				$.each(param, function(key, val) {
					var inputan = document.createElement("input");
					inputan.setAttribute("type", "hidden");
					inputan.setAttribute("name", key);
					inputan.setAttribute("value", val);
					form.appendChild(inputan);
				});

				document.body.appendChild(form);
				form.submit();

				document.body.removeChild(form);
			}

			printLaporan = function(menu, laporan){
		    	// alert('tara');
		    	var url = '{{ url($pageUrl) }}/'+menu;

		    	postNewTab(url, {
		    		_token 		: "{{ csrf_token() }}",
		    		status  	: $("#status").val(),
		    		tmuk_kode  	: $('[name="filter[tmuk_kode]"]').val(),
		    		laporan  	: laporan,
		    		start  		: $("#mulai_filter").val(),
		    		end   		: $("#selesai_filter").val(),
		    		tipe   		: $('[name="filter[tipe]"]').val(),
					vendor 		: $('[name="filter[vendor]"]').val(),
		    	});
		    }

		    exelLaporan = function(menu, laporan){
		    	var url = '{{ url($pageUrl) }}/'+menu;
		    	postNewTab(url, {
		    		_token 		: "{{ csrf_token() }}",
		    		status  	: $("#status").val(),
		    		tmuk_kode  	: $('[name="filter[tmuk_kode]"]').val(),
		    		laporan  	: laporan,
		    		start  		: $("#mulai_filter").val(),
		    		end   		: $("#selesai_filter").val(),
		    		tipe   		: $('[name="filter[tipe]"]').val(),
					vendor 		: $('[name="filter[vendor]"]').val(),
		    	});
		    }


		    $('.twelve.wide.column').html(response);

		    if (v == 'pergerakan-persediaan'){
				// console.log($tmuk);
				$.ajax({
					url: "{{ url('ajax/option/produk-by-tmuk') }}",
					type: 'GET',
					data:{
						id_tmuk : $("select[name='filter[tmuk_kode]']").val(),
					},
				})
				.success(function(response) {
					$('.produk').removeClass('disabled');
					$('.produk').html(response);
				})
				.fail(function() {
					console.log("error");
				});
			}
		})
		.fail(function(response) {
			console.log("error");
		});
		
	}

	showTableFilter = function(v, menu=''){	
		$(".loading").addClass('active');
		$.ajax({
			url: '{{  url($pageUrl) }}/grid'+menu,
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				type 		: v,
				status  	: $("#status").val(),
				tmuk_kode 	: $('[name="filter[tmuk_kode]"]').val(),
				start 		: $('#mulai_filter').val(),
				end 		: $('#selesai_filter').val(),
				coa 		: $('[name="filter[coa]"]').val(),
				tipe   		: $('[name="filter[tipe]"]').val(),
				kode_produk	: $('[name="filter[produk]"]').val(),
				vendor 		: $('[name="filter[vendor]"]').val(),
			},
		})
		.done(function(response) {
			// New Post Tab
			$(".loading").removeClass('active');

			postNewTab = function(url, param)
			{
				var form = document.createElement("form");
				form.setAttribute("method", 'POST');
				form.setAttribute("action", url);
				form.setAttribute("target", "_blank");

				$.each(param, function(key, val) {
					var inputan = document.createElement("input");
					inputan.setAttribute("type", "hidden");
					inputan.setAttribute("name", key);
					inputan.setAttribute("value", val);
					form.appendChild(inputan);
				});

				document.body.appendChild(form);
				form.submit();

				document.body.removeChild(form);
			}

			printLaporan = function(menu, laporan){
		    	// alert('tara');
		    	var url = '{{ url($pageUrl) }}/'+menu;

		    	postNewTab(url, {
		    		_token 		: "{{ csrf_token() }}",
		    		status  	: $("#status").val(),
		    		tmuk_kode  	: $('[name="filter[tmuk_kode]"]').val(),
		    		laporan  	: laporan,
		    		start  		: $("#mulai_filter").val(),
		    		end   		: $("#selesai_filter").val(),
					coa 		: $('[name="filter[coa]"]').val(),
					tipe   		: $('[name="filter[tipe]"]').val(),
					kode_produk	: $('[name="filter[produk]"]').val(),
					vendor 		: $('[name="filter[vendor]"]').val(),
		    	});
		    }

		    exelLaporan = function(menu, laporan){
		    	console.log($('[name="filter[tipe]"]').val());
		    	var url = '{{ url($pageUrl) }}/'+menu;
		    	postNewTab(url, {
		    		_token 		: "{{ csrf_token() }}",
		    		status  	: $("#status").val(),
		    		tmuk_kode  	: $('[name="filter[tmuk_kode]"]').val(),
		    		laporan  	: laporan,
		    		start  		: $("#mulai_filter").val(),
		    		end   		: $("#selesai_filter").val(),
					coa 		: $('[name="filter[coa]"]').val(),
					tipe   		: $('[name="filter[tipe]"]').val(),
					kode_produk	: $('[name="filter[produk]"]').val(),
					vendor 		: $('[name="filter[vendor]"]').val(),
		    	});
		    }


		    $('.twelve.wide.column').html(response);
			
		})
		.fail(function(response) {
			console.log("error");
		});
		
	}


</script>
@append

@section('js-filters')
d.region_kode = $("select[name='filter[region_kode]']").val();
d.lsi_kode = $("select[name='filter[lsi_kode]']").val();
d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
d.tipe = $("select[name='filter[tipe]']").val();
d.kode_produk = $("select[name='filter[produk]']").val();
d.vendor =   $("select[name='filter[vendor]']").val();
@endsection

@section('content-body')
<div class="ui grid">
		<div class="sixteen wide column main-content">
		    <div class="ui segments">
				<div class="ui segment">
					<form class="ui filter form">
		  				<div class="fields">
		  					<div class="field" style="width: 15%;">
								<select name="region_id" class="ui fluid selection dropdown">
									{!! \Lotte\Models\Master\Region::options('area', 'id',[], '-- Pilih Region --') !!}
								</select>
							</div>
							<div class="field" style="width: 28%;">
								<select name="lsi_id" class="ui fluid search selection dropdown lsi disabled" id="lsi_id">
								<option value="0">-- Pilih LSI --</option>
								</select>
							</div>
							<div class="field" style="width: 15%;">
								<select name="filter[tmuk_kode]" class="ui fluid search dropdown tmuk disabled" id="tmuk">
									<option value="0">-- Pilih TMUK --</option>
								</select>
							</div>

							<button type="button" class="ui teal icon filter button" id="filter_tmuk" data-content="Cari Data">
								<i class="search icon"></i>
							</button>
							<button type="reset" class="ui icon reset button" id="reset_tmuk" data-content="Bersihkan Pencarian">
								<i class="refresh icon"></i>
							</button>
						</div>
					</form>

					<div class="ui segment" id="alert">
				      <p align="center">Filter terlebih dahulu</p>
				    </div>
					<div class="ui clearing segment hidden" id="menu">
						<div class="ui inverted loading dimmer">
							<div class="ui text loader">Loading</div>
						</div>
						<div class="ui divided grid">
							<div class="four wide column" style="position: relative">
								@include('modules.laporan.laporan.menu')
							</div>
							<div class="twelve wide column">
								{{-- @include('modules.utama.finance.coa.lists.divisi') --}}
								{{-- @include('modules.utama.finance.coa.lists.coa1') --}}
							</div>
						</div>
					</div>
		        </div>
		    </div>
		</div>
	</div>
@endsection