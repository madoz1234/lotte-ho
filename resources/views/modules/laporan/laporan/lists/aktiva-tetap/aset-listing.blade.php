@if(isset($aset))
    @if($aset->count() != 0)
    <div class="ui form" style="float:right">
    <a class="ui default right labeled icon save button" target="_blank"  onclick="printLaporan('print-laporan-aktiva-tetap','aset-listing');">
            Print
            <i class="print icon"></i>
        </a>
        <div class="ui green export buttons" style="margin-right: 5px">
            <div class="ui button" onclick="printLaporan('exel-laporan-aktiva-tetap','aset-listing');"><i class="file excel outline icon"></i>Export</div>
        </div>
    </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Asset Listing</div><br>

<div class="field">
    <div class="ui calendar labeled input pm_date_start">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal">
        </div>
    </div>
    {{-- &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input pm_date_end">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div> --}}
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('aset-listing','-aktiva-tetap');">
        <i class="search icon"></i>
    </button>
</div>
<br>

@if(isset($aset))  
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="10%">Tipe Aset</th>
            <th class="center aligned" width="25%">Nama Aset</th>
            <th class="center aligned" width="15%">Nomor Seri</th>
            <th class="center aligned" width="15%">Tanggal Pembelian</th>
            <th class="center aligned" width="10%">Kondisi</th>
            <th class="center aligned" width="12%">Nilai Pembelian</th>
            <th class="center aligned" width="13%">Nilai Saat Ini</th>
        </tr>
    </thead>
    <tbody>
        @if($aset->count() > 0)
            @foreach ($aset as $row)
            {{-- {{ dd($row) }} --}}
                <tr>
                    <td class="left aligned">{{ $row->tipeaset->tipe }}</td>
                    <td class="left aligned">{{ $row->nama }}</td>
                    <td class="left aligned">{{ $row->no_seri }}</td>
                    <td class="center aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tanggal_pembelian)->format('d F Y') }}</td>
                    <td class="center aligned">{{ $row->kondisi==1 ? 'Baru' : 'Bekas' }}</td>
                    <td class="right aligned">{{ rupiah($row->nilai_pembelian) }}</td>
                    <?php
                        $d1 = $row->getUmurEkonomis($row->tipe_id);
                        $penyusutan_bulan = (float) $row->nilai_pembelian / (float) ($d1*12);

                        $d11 = new DateTime(\Carbon\Carbon::createFromFormat('Y-m-d',$row->tanggal_pembelian)->format('Y-m-d'));
                        $d22 = new DateTime($start);

                        $d3 = $d11->diff($d22)->m + ($d11->diff($d22)->y*12);
                        
                    ?>
                    <td class="right aligned">{{ rupiah(($row->nilai_pembelian) - ($d3 * $penyusutan_bulan)) }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
        @endif
    </tbody>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    var date = new Date();
    $('.pm_date_start').calendar({
        type: 'date',
        endCalendar: $('.pm_date_end'),
        maxDate:date,
        formatter: {
         date: function (date, settings)
            {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day+'/'+month+'/'+year;
             }
         }
    })

    $('.pm_date_end').calendar({
        type: 'date',
        startCalendar: $('.pm_date_start'),
        formatter: {
         date: function (date, settings)
            {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day+'/'+month+'/'+year;
             }
         }
    })
    $(document).ready(function() {
        @if(isset($aset))
            @if($aset->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif
    });
</script>