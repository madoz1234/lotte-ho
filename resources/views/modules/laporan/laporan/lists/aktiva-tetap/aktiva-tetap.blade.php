@if(isset($aset))
@if($aset->count() != 0)
<div class="ui form" style="float:right">
    <a class="ui default right labeled icon save button" target="_blank"  onclick="printLaporan('print-laporan-aktiva-tetap','aktiva-tetap');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-aktiva-tetap','aktiva-tetap');"><i class="file excel outline icon"></i>Export</div>
    </div>
</div>
@endif
@endif


<div class="title" style="font-size: 17px;">Rekap Aktiva Tetap</div><br>

<div class="field">
    <div class="ui calendar labeled input pm_date_start">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('aktiva-tetap','-aktiva-tetap');">
        <i class="search icon"></i>
    </button>
</div> 
<br>

<script type="text/javascript">
    $(document).ready(function() {
        @if(isset($aset))
        @if($aset->count() != 0)
        $('#tableAktiva').DataTable({
            paging: false,
            filter: false,
            lengthChange: false,
            ordering:false,
            scrollX: true,
            scrollY: 500,
        });
        @endif
        @endif
    });
</script>

@if(isset($aset))   
<table class="ui celled compact red table" id="tableAktiva" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" rowspan="2" >#</th>
            <th class="center aligned" rowspan="2" >Keterangan</th>
            <th class="center aligned" colspan="2" >Tanggal Perolehan</th>
            <th class="center aligned" colspan="3" >Perolehan</th>
            <th class="center aligned" rowspan="2" >Umur Ekonomis (Tahun)</th>
            <th class="center aligned" rowspan="2" >Penyusutan Perbulan</th>
            <th class="center aligned" rowspan="2" >Penyusutan {{ \Carbon\Carbon::createFromFormat('Y-m-d',$start)->format('F Y') }}</th>
            <th class="center aligned" colspan="2" >Penyusutan Tahun {{ \Carbon\Carbon::createFromFormat('Y-m-d',$start)->format('Y') }}</th>
            <th class="center aligned" rowspan="2" >Total Akm. Penyusutan</th>
            <th class="center aligned" rowspan="2" >Nilai Buku</th>
        </tr>
        <tr>
            <th class="center aligned">Bulan</th>
            <th class="center aligned">Tahun</th>
            <th class="center aligned">Unit</th>
            <th class="center aligned">Harga</th>
            <th class="center aligned">Jumlah</th>
            <th class="center aligned">Bulan</th>
            <th class="center aligned">Tahun</th>
        </tr>
    </thead>
    <tbody>
        @if($aset->count() > 0)
        <?php $perolehan_harga=0;
        $penyusutan_jumlah=0;
        $penyusutan_jumlah_bulan=0;
        $penyusutan_jumlah_tahun=0;
        $jum_unit=0;
        ?>
        <tr>
            <td></td>
            <td><b>SUMMARY {{ \Carbon\Carbon::createFromFormat('Y-m-d',$start)->format('d/m/Y') }} (Active)</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @foreach($aset as $v)
        <tr>
            <td></td>
            <td style="width: 135px">{{ isset($v->tipeaset->tipe) ? $v->tipeaset->tipe : '-' }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="right aligned">{{ rupiah($v->jumlah) }}</td>
            <td></td>
            <td class="right aligned">{{ rupiah($v->jumlah/($v->getUmurEkonomis($v->tipe_id)*12)) }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @endforeach
        <tr> 
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"><b>DETAIL</b></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
        </tr>
        @foreach($aset as $v)
        <tr>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"><b>{{ isset($v->tipeaset->tipe) ? $v->tipeaset->tipe : '-' }}</b></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
        </tr>
        <?php
        $num=0; 
        $total=0;
        $tot_penyusutan=0;
        $tot_penyusutan_bulan=0;
        $tot_penyusutan_tahun=0;
        ?>

        @foreach($v->getItemAset($v->tmuk_kode,$v->tipe_id,$start) as $d)
        
        <?php $total+=$d->nilai_pembelian;
        $perolehan_harga+=$d->nilai_pembelian;
        $num+=1;
        ?>

        <tr>
            <td></td>
            <td>{{ $d->nama }}</td>
            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d',$d->tanggal_pembelian)->format('F') }}</td>
            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d',$d->tanggal_pembelian)->format('Y') }}</td>
            <td class="center aligned">1</td>
            <td class="right aligned">{{ rupiah($d->nilai_pembelian) }}</td>
            <td class="right aligned">{{ rupiah($d->nilai_pembelian) }}</td>
            <td class="center aligned">{{ $d->getUmurEkonomis($d->tipe_id) }}</td>

            <?php
            $d1 = $d->getUmurEkonomis($d->tipe_id);

            $penyusutan_bulan = (float) $d->nilai_pembelian / (float) ($d1*12);
            $tot_penyusutan+=$penyusutan_bulan;
            $penyusutan_jumlah+=$penyusutan_bulan;
            ?>

            <td class="right aligned">{{ rupiah($penyusutan_bulan) }}</td>
            
            <?php
            $d1 = new DateTime(\Carbon\Carbon::createFromFormat('Y-m-d',$d->tanggal_pembelian)->format('Y-m-d'));
            $d2 = new DateTime($start);

            $d3 = $d1->diff($d2)->m + ($d1->diff($d2)->y*12);
            $tot_penyusutan_bulan+=($d3 * $penyusutan_bulan);
            ?>
            
            <td class="right aligned">{{ rupiah($d3 * $penyusutan_bulan) }}</td>

            <?php
            $month = \Carbon\Carbon::createFromFormat('Y-m-d',$start)->format('m');
            $month = (int) $month;
            $tot_penyusutan_tahun+=($month * $penyusutan_bulan);
            ?>

            <td class="center aligned">{{ $month }}</td>
            <td class="right aligned">{{ rupiah($month * $penyusutan_bulan) }}</td>
            <td class="right aligned">{{ rupiah($d3 * $penyusutan_bulan) }}</td>
            <td class="right aligned">{{ rupiah(($d->nilai_pembelian) - ($d3 * $penyusutan_bulan)) }}</td>
        </tr>
        @endforeach
        <tr>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"><b>Sub Total</b></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;" class="center aligned">{{ $v->getItemAset($v->tmuk_kode,$v->tipe_id,$start)->count() }}</td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($total) }}</td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($tot_penyusutan) }}</td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($tot_penyusutan_bulan) }}</td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($tot_penyusutan_tahun) }}</td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($tot_penyusutan_bulan) }}</td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($total-$tot_penyusutan_bulan) }}</td>
        </tr>
        <?php
        $penyusutan_jumlah_bulan+=$tot_penyusutan_bulan;    
        $penyusutan_jumlah_tahun+=$tot_penyusutan_tahun;    
        $jum_unit+=$num;    
        ?>
        @endforeach
        <tr>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"><b>Total</b></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;" class="center aligned">{{ $jum_unit }}</td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($perolehan_harga) }}</td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($aset->sum('jumlah')) }}</td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($penyusutan_jumlah) }}</td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($penyusutan_jumlah_bulan) }}</td>
            <td style="border-top:2px solid black;"></td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($penyusutan_jumlah_tahun) }}</td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($penyusutan_jumlah_bulan) }}</td>
            <td style="border-top:2px solid black;" class="right aligned">{{ rupiah($aset->sum('jumlah')-$penyusutan_jumlah_bulan) }}</td>
        </tr>
        @else
        <tr>
            <td colspan="14">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>
@else
<div class="ui segment">
  <p align="center">Filter tanggal terlebih dahulu</p>
</div>
@endif

<script type="text/javascript">
    var date = new Date();
    var today   = new Date(date.getFullYear(), date.getMonth(), 0, 0, 0, 0);

    $('.pm_date_start').calendar({
        type: 'month',
        maxDate: today,
        endCalendar: $('.pm_date_end'),
        formatter: {
           date: function (date, settings)
           {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return month+'/'+year;
        }
    }
})

    $('.pm_date_end').calendar({
        type: 'month',
        maxDate: today,
        startCalendar: $('.pm_date_start'),
        formatter: {
           date: function (date, settings)
           {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return month+'/'+year;
        }
    }
})
    $(document).ready(function() {
        @if(isset($aset))
        @if($aset->count() != 0)
        $('#reportTable').DataTable({
            paging: 10,
            filter: false,
            lengthChange: false,
            ordering:false,
        });
        @endif
        @endif
    });
</script>