@if(isset($detail))
    @if($detail->count() != 0)
    <div class="ui form" style="float:right">
     <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-piutang-member','piutang-member');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-akun-piutang-member','piutang-member');"><i class="file excel outline icon"></i>Export</div>
    </div>
    </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Rekap Piutang Member</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('piutang-member','-akun-piutang-member');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
@if(isset($detail))
    <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="40px">No.</th>
                {{-- <th class="center aligned" width="40px">Pelanggan/ No. Member</th> --}}
                <th class="center aligned" width="40px">Tanggal</th>
                <th class="center aligned" width="40px">No Transaksi</th>
                {{-- <th class="center aligned" width="40px">Deskripsi</th> --}}
                <th class="center aligned" width="40px">Jumlah</th>
                <th class="center aligned" width="40px">Sisa Utang</th>
            </tr>
        </thead>
        <tbody>
            @if($detail->count() > 0)
            <?php $i = 1; ?>
            @foreach ($detail as $row)
            <tr>
                <td class="center aligned">{{ $i }}</td>
                {{-- <td class="left aligned">{{ $row->coa_kode }} - {{ $row->coa->nama }}</td> --}}
                <td class="center aligned">{{ $row->tanggal }}</td>
                <td class="center aligned">{{ $row->idtrans }}</td>
                {{-- <td class="">{{ $row->deskripsi }}</td> --}}
                <td class="right aligned">{{ rupiah($row->jml) }}</td>
                <td class="right aligned">0</td>
            </tr>
            <?php $i++; ?>
            @endforeach
            @else
            <tr>
                <td colspan="11">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif
        
        $('.ui.calendar').calendar({
            type: 'date',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        });
    });
</script>