@if(isset($detail))
    @if($detail->count() != 0)
    <div class="ui form" style="float:right">
       <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-piutang-member','umur-piutang-member');">
            Print
            <i class="print icon"></i>
        </a>
        <div class="ui green export buttons" style="margin-right: 5px">
            <div class="ui button" onclick="printLaporan('exel-laporan-akun-piutang-member','umur-piutang-member');"><i class="file excel outline icon"></i>Export</div>
        </div>
    </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Rekap Umur Piutang Member</div><br>
<div class="field">
    <div class="ui calendar labeled input umur_date_start" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input umur_date_end" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('umur-piutang-member','-akun-piutang-member');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div><br>

@if(isset($detail))
<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
         <tr>
                <th class="center aligned" width="40px">#</th>
                <th class="center aligned" width="40px">No. Transaksi</th>
                <th class="center aligned" width="40px">Tanggal Transaksi</th>
                <th class="center aligned" width="40px">Umur Piutang</th>
                <th class="center aligned" width="40px">Jumlah</th>
                <th class="center aligned" width="40px">30 hari [0-30]</th>
                <th class="center aligned" width="40px">60 hari [31-60]</th>
                <th class="center aligned" width="40px">90 hari [61-90]</th>
                <th class="center aligned" width="40px">>90 hari [91-dst]</th>
            </tr>
    </thead>
    <tbody>
         @if($detail->count() > 0)
        <?php $i = 1; ?>
        <?php $totaljumlah = 0 ?>
        @foreach ($detail as $row)
        {{-- @foreach ($row->detail as $data) --}}
        <tr>
            <td class="center aligned">{{ $i }}</td>
            <td class="center aligned">{{ $row->idtrans }}</td>
            <td class="center aligned">{{ $row->tanggal }}</td>
            <td class="right aligned">0</td>
            <td class="right aligned">{{ rupiah($row->jml) }}</td>
            <td class="right aligned">0</td>
            <td class="right aligned">0</td>
            <td class="right aligned">0</td>
            <td class="right aligned">0</td>
        </tr>
        <?php $i++; ?>
        <?php $totaljumlah +=$row->jml; ?>
        {{-- @endforeach --}}
        @endforeach
        <tfoot>
            <tr>
                <th colspan="3" class="right aligned" width="40px">Grand Total (Rp)</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
                <th class="right aligned" width="40px">{{ rupiah($totaljumlah) }}</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
            </tr>
        </tfoot>
        @else
        <tr>
            <td colspan="11">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $('.umur_date_start').calendar({
        type: 'date',
        endCalendar: $('.umur_date_end'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return year+'-'+month+'-'+day;
             }
        }
    })

    $('.umur_date_end').calendar({
        type: 'date',
        startCalendar: $('.umur_date_start'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return year+'-'+month+'-'+day;
             }
        }
    })
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif
        // $('.ui.calendar').calendar({
        //     type: 'date',
        //     formatter: {
        //         date: function (date, settings) {
        //             if (!date) return '';
        //             var day = date.getDate();
        //             var month = date.getMonth() + 1;
        //             var year = date.getFullYear();
        //             return day+'/'+month+'/'+year;
        //         }
        //     }
        // });
    });
</script>