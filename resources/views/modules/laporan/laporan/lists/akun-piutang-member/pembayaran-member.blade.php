@if(isset($detail))
    @if($detail->count() != 0)
    <div class="ui form" style="float:right">
       <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-piutang-member','pembayaran-member');">
            Print
            <i class="print icon"></i>
        </a>
        <div class="ui green export buttons" style="margin-right: 5px">
            <div class="ui button" onclick="printLaporan('exel-laporan-akun-piutang-member','pembayaran-member');"><i class="file excel outline icon"></i>Export</div>
        </div>
    </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Rekap Pembayaran Member</div><br>
<div class="field">
    <div class="ui calendar labeled input pembayaran_date_start" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input pembayaran_date_end" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('pembayaran-member','-akun-piutang-member');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div><br>

@if(isset($detail))
<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">Tanggal</th>
            <th class="center aligned" width="40px">No Member</th>
            <th class="center aligned" width="40px">Nama Member</th>
            {{-- <th class="center aligned" width="40px">Jenis Member</th> --}}
            <th class="center aligned" width="40px">Jumlah Pembayaran (Rp)</th>
            <th class="center aligned" width="40px">Sisa Piutang (Rp)</th>
        </tr>
    </thead>
    <tbody>
        @if($detail->count() > 0)
        <?php $i = 1; ?>
        @foreach ($detail as $row)
{{-- {{ dd($row) }} --}}
        <tr>
            <td class="center aligned">{{ $i }}</td>
            <td class="center aligned">{{ $row->created_at }}</td>
            <td class="center aligned">{{ $row->no_member }}</td>
            <td class="center aligned">{{ $row->nama_member }}</td>
            {{-- <td class="center aligned">{{ $row->hutang }}</td> --}}
            <td class="center aligned">0</td>
            <td class="center aligned">0</td>
        </tr>
        <?php $i++; ?>
        @endforeach
        <tfoot>
            <tr>
                <th colspan="4" class="right aligned" width="40px">Grand Total (Rp)</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
            </tr>
        </tfoot>
        @else
        <tr>
            <td colspan="11">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $('.pembayaran_date_start').calendar({
        type: 'date',
        endCalendar: $('.pembayaran_date_end'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return year+'-'+month+'-'+day;
             }
        }
    })

    $('.pembayaran_date_end').calendar({
        type: 'date',
        startCalendar: $('.pembayaran_date_start'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return year+'-'+month+'-'+day;
             }
        }
    })
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif
        // $('.ui.calendar').calendar({
        //     type: 'date',
        //     formatter: {
        //         date: function (date, settings) {
        //             if (!date) return '';
        //             var day = date.getDate();
        //             var month = date.getMonth() + 1;
        //             var year = date.getFullYear();
        //             return day+'/'+month+'/'+year;
        //         }
        //     }
        // });
    });
</script>