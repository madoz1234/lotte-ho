@if(isset($detail))
    @if($detail->count() != 0)
    <div class="ui form" style="float:right">
       <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-piutang-member','daftar-member');">
            Print
            <i class="print icon"></i>
        </a>
        <div class="ui green export buttons" style="margin-right: 5px">
            <div class="ui button" onclick="printLaporan('exel-laporan-akun-piutang-member','daftar-member');"><i class="file excel outline icon"></i>Export</div>
        </div>
    </div>

    @endif
@endif

<div class="title" style="font-size: 17px;">Daftar Member</div><br>
<div class="field">
    <div class="ui calendar labeled input daftar_date_start" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input daftar_date_end" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('daftar-member','-akun-piutang-member');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div><br>

@if(isset($detail))
<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">No Member</th>
            <th class="center aligned" width="40px">Nama Member</th>
            <th class="center aligned" width="40px">Alamat</th>
            <th class="center aligned" width="40px">Telepon</th>
            <th class="center aligned" width="40px">Email</th>
            <th class="center aligned" width="40px">Limit Kredit</th>
            <th class="center aligned" width="40px">Total Order</th>
            <th class="center aligned" width="40px">Total pembelian (Rp)</th>
            <th class="center aligned" width="40px">Last Visit</th>
            <th class="center aligned" width="40px">Ave Pembelian</th>
        </tr>
    </thead>
    <tbody>
        @if($detail->count() > 0)
        <?php $i = 1; ?>
        <?php $totallimitkredit = 0 ?>
        @foreach ($detail as $row)

        <tr>
            <td class="center aligned">{{ $i }}</td>
            <td class="center aligned">{{ $row->nomor }}</td>
            <td class="left aligned">{{ $row->nama }}</td>
            <td class="left aligned">{{ $row->alamat }}</td>
            <td class="left aligned">{{ $row->telepon }}</td>
            <td class="left aligned">{{ $row->email }}</td>
            <td class="right aligned">{{ $row->limit_kredit==null ? '-': rupiah($row->limit_kredit) }}</td>
            <td class="center aligned">0</td>
            <td class="center aligned">0</td>
            <td class="center aligned">0</td>
            <td class="center aligned">0</td>
        </tr>
        <?php $i++; ?>
        <?php $totallimitkredit +=$row->limit_kredit; ?>
        @endforeach
        <tfoot>
            <tr>
                <th colspan="6" class="right aligned" width="40px">Grand Total (Rp)</th>
                <th class="right aligned" width="40px">{{ $totallimitkredit==null ? '-': $row->limit_kredit }}</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
                <th class="right aligned" width="40px">{{ rupiah(0) }}</th>
            </tr>
        </tfoot>
        @else
        <tr>
            <td colspan="11">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $('.daftar_date_start').calendar({
        type: 'date',
        endCalendar: $('.daftar_date_end'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return year+'-'+month+'-'+day;
             }
        }
    })

    $('.daftar_date_end').calendar({
        type: 'date',
        startCalendar: $('.daftar_date_start'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return year+'-'+month+'-'+day;
             }
        }
    })
    $(document).ready(function() {

        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif
        // $('.ui.calendar').calendar({
        //     type: 'date',
        //     formatter: {
        //         date: function (date, settings) {
        //             if (!date) return '';
        //             var day = date.getDate();
        //             var month = date.getMonth() + 1;
        //             var year = date.getFullYear();
        //             return day+'/'+month+'/'+year;
        //         }
        //     }
        // });
    });
</script>