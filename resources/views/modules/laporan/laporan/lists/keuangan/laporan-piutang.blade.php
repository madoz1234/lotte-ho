@if(isset($detail))
    @if($detail->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-laporan-keuangan','laporan-piutang');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-laporan-keuangan','laporan-piutang');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">LAPORAN PIUTANG TMUK</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('laporan-piutang','-laporan-keuangan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div><br>

@if(isset($detail))
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="20px">#</th>
            <th class="center aligned" width="20px">Tanggal</th>
            <th class="center aligned" width="20px">Deskripsi</th>
            <th class="center aligned" width="20px">Debit</th>
            <th class="center aligned" width="20px">Kredit</th>
        </tr>
    </thead>
    <tbody>
        @if($detail->count() != 0)
            <?php $i = 1; $total_debit = 0; $total_kredit = 0;?>
            @foreach ($detail as $row)
            <tr>
                <td class="center aligned">{{ $i }}</td>
                <td class="center aligned">{{ \Carbon\Carbon::parse($row->tanggal)->format('d/m/Y') }}</td>
                <td class="left aligned">{{ ucfirst($row->deskripsi) }}</td>
                <td class="right aligned">{{ number_format($row->posisi=='D' ? $total_debit += $row->jumlah : '0') }}</td>
                <td class="right aligned">{{ number_format($row->posisi=='K' ? $total_kredit += $row->jumlah : '0') }}</td>
            </tr>
            <?php $i++; ?>
            @endforeach
            <tr>
                <td class="center aligned"></td>
                <td class="center aligned"></td>
                <td class="center aligned"><b>Total</b></td>
                <td class="right aligned">{{ number_format($total_debit) }}</td>
                <td class="right aligned">{{ number_format($total_kredit) }}</td>
            </tr>
        @else
        <tr>
            <td colspan="11">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>
@else
<div class="ui segment">
  <p align="center">Filter tanggal terlebih dahulu</p>
</div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                })
            @endif
        @endif

        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth() + 1, 0, 0, 0, 0);
        
        $('#from').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){ 
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 2;
                var year = date.getFullYear();
                    if (month > 12) {
                        month = month - 12;
                        year = year + 1;
                    }
                $('#to').calendar("set date", new Date(year,month));
                // $('#selesai_filter').val(month+'/'+year);
            }
        })

        $('#to').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() - 2;
                var year = date.getFullYear();
                    if (month < 0) {
                        month = month + 12;
                        year = year - 1;
                    }

                // $('#from').calendar("set date", new Date(year,month));
                $('#mulai_filter').val((month+1)+'/'+year);
            }
        })
    });
</script>