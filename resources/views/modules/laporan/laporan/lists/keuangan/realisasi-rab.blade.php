@if(isset($detail))
    <div class="ui form" style="float:right">
       <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-laporan-keuangan','realisasi-rab');">
            Print
            <i class="print icon"></i>
        </a>
        {{-- <div class="ui green export buttons" style="margin-right: 5px">
            <div class="ui button" onclick="printLaporan('exel-laporan-laporan-keuangan','realisasi-rab');"><i class="file excel outline icon"></i>Export</div>
        </div> --}}
    </div>
@endif


<div class="title" style="font-size: 17px;">REALISASI RAB</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('realisasi-rab','-laporan-keuangan');">
        <i class="search icon"></i>
    </button>
</div>

@if(isset($detail))
    @if($detail->count() != 0)
        @foreach ($detail as $data)
            <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="center aligned" width="40px">RAB</th>
                        <th class="center aligned" width="40px">TOTAL</th>
                        <th class="center aligned" width="40px">Realisasi</th>
                        <th class="center aligned" width="40px">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="left aligned">Perkiraan dana investasi</td>
                        <td class="center aligned"></td>
                        <td class="left aligned">Pemasukan</td>
                        <td class="right aligned"></td>
                    </tr>
                        <?php $sum_tot = 0 ?>
                        @foreach ($saldo as $jual)
                    <tr>
                        <td class="left aligned" style="padding-left: 50px;"></td>
                        <td class="right aligned"></td>
                        {{-- <td class="left aligned" style="padding-left: 50px;">Bulan ke- {{ \Carbon\Carbon::now()->format($jual->month) }} --}}
                        <td class="left aligned" style="padding-left: 50px;">{{ \Carbon\Carbon::createFromFormat('m-Y',$jual->month.'-'.$jual->year)->format('F  Y') }}</td>
                        </td>
                        <td class="right aligned">{{ rupiah($jual->total_penjualan) }}</td>
                    </tr>
                        <?php $sum_tot += $jual->total_penjualan ?>
                        @endforeach
                    <tr>
                        <td class="left aligned" style="padding-left: 50px;">DP</td>
                        <td class="right aligned">{{ rupiah($data->i_satu) }}</td>
                        <td class="left aligned" style="padding-left: 50px;"></td>
                        <td class="right aligned"><b>{{ rupiah($sum_tot) }}</b></td>
                    </tr>
                    <tr>
                        <td class="left aligned" style="padding-left: 50px;">Dana investasi</td>
                        <?php $invest = $data->i_dua + $data->i_tiga_a + $data->i_tiga_b + $data->i_tiga_c + $data->i_tiga_d + $data->i_tiga_e + $data->i_tiga_f + $data->i_empat_a + $data->i_empat_b + $data->i_empat_c + $data->i_lima_a + $data->i_lima_b + $data->i_bila_ada_biaya_tambahan + $data->i_sewa_ruko + $data->i_ppn; ?>
                        <td class="right aligned">{{ rupiah($invest) }}</td>
                        <td class="left aligned">Pengeluaran</td>
                        <?php $totalrealisasirab = $invest + $data->i_satu + $data->i_lima_a + $data->i_lima_b + $data->i_tiga_a + $data->i_tiga_b + $data->i_tiga_c + $data->i_tiga_d + $data->i_tiga_e + $data->i_tiga_f + $data->i_empat_a + $data->i_empat_c + $data->i_empat_b + $data->i_satu + $data->i_lima_b + $data->i_bila_ada_biaya_tambahan; ?>
                        <td class="center aligned"></td>
                    </tr>
                    @if($akuisisi)
                        <tr>
                            <td class="left aligned"></td>
                            <?php $totaldana = $data->i_satu + $invest; ?>
                            <td class="right aligned"><b>{{ rupiah($totaldana) }}</b></td>
                            <td class="left aligned" style="padding-left: 50px;">Start & Biaya Promosi Opening</td>
                            <?php $start = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 1,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($start) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned">Perkiraan Pengeluaran</td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Perijinan Usaha Toko</td>
                            <?php $perijinan = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 2,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($perijinan) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Stok awal</td>
                            <?php $stokawal = $data->i_lima_a + $data->i_lima_b; ?>
                            <td class="right aligned">{{ rupiah($stokawal)}}</td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Renovasi Sipil</td>
                            <?php $renovasi = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 3,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($renovasi) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Kusen Aluminium + Kaca Cermin</td>
                            <?php $kusen = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 4,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($kusen) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan sipil</td>
                            <td class="right aligned">{{ rupiah($data->i_tiga_a + $data->i_tiga_b + $data->i_tiga_c + $data->i_tiga_d + $data->i_tiga_e + $data->i_tiga_f) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Folding Gate + Polycarbonate = Teras</td>
                            <?php $folding = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 5,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($folding) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" ></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Instalasi Listrik (lampu, regrouping)</td>
                            <?php $instalasi = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 6,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($instalasi) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;"> Peralatan Elektonik</td>
                            <td class="right aligned">{{ rupiah($data->i_empat_a + $data->i_empat_c) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Instalasi Pemasangan AC</td>
                            <?php $pemasangan = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 7,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($pemasangan) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Signage (Lisping Papan Nama Toko) Repainting</td>
                            <?php $signage = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 8,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($signage) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan Non-Elektronik</td>
                            <td class="right aligned">{{ rupiah($data->i_empat_b) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan AC Split</td>
                            <?php $sipil = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 9,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($sipil) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan Non Elektronik (Rak, Furniture, Acrylic, dll)</td>
                            <?php $non_elektronik = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 10,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($non_elektronik) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Startup & Promosi opening</td>
                            <td class="right aligned">{{ rupiah($data->i_satu) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan Eletronika (Komputer, Cooler & Timbangan)</td>
                            <?php $elektronik = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 11,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($elektronik) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Stock Awal</td>
                            <?php 
                                $stock_awal_pyr = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 12,$akuisisi->tmuk_kode);
                                $stock_awal     = $stock_awal_pyr + $stock_awal_po; 
                            ?>
                            <td class="right aligned">{{ rupiah($stock_awal) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Modal Kerja</td>
                            <td class="right aligned">{{ rupiah($data->i_lima_b) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Modal Kerja 1 bulan (ditransfer ke rek Ops TMUK)</td>
                            <?php $modal_kerja = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 13,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($modal_kerja) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Lain-lain</td>
                            <td class="right aligned">{{ rupiah($data->i_bila_ada_biaya_tambahan) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Lain-lain</td>
                            <?php $lain_lain = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeterangan('OPENING', 14,$akuisisi->tmuk_kode); ?>
                            <td class="right aligned">{{ rupiah($lain_lain) }}</td>
                        </tr>
                    @else
                        <tr>
                            <td class="left aligned"></td>
                            <?php $totaldana = $data->i_satu + $invest; ?>
                            <td class="right aligned"><b>{{ rupiah($totaldana) }}</b></td>
                            <td class="left aligned" style="padding-left: 50px;">Start & Biaya Promosi Opening</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned">Perkiraan Pengeluaran</td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Perijinan Usaha Toko</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Stok awal</td>
                            <td class="right aligned">{{ rupiah($data->i_lima_a)}}</td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Renovasi Sipil</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Kusen Aluminium + Kaca Cermin</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan sipil</td>
                            <td class="right aligned">{{ rupiah($data->i_tiga_a + $data->i_tiga_b + $data->i_tiga_c + $data->i_tiga_d + $data->i_tiga_e + $data->i_tiga_f) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Folding Gate + Polycarbonate = Teras</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned" ></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Instalasi Listrik (lampu, regrouping)</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;"> Peralatan Elektonik</td>
                            <td class="right aligned">{{ rupiah($data->i_empat_a + $data->i_empat_c) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Instalasi Pemasangan AC</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Signage (Lisping Papan Nama Toko) Repainting</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan Non-Elektronik</td>
                            <td class="right aligned">{{ rupiah($data->i_empat_b) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan AC Split</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan Non Elektronik (Rak, Furniture, Acrylic, dll)</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Startup & Promosi opening</td>
                            <td class="right aligned">{{ rupiah($data->i_satu) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan Eletronika (Komputer, Cooler & Timbangan)</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Stock Awal</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Modal Kerja</td>
                            <td class="right aligned">{{ rupiah($data->i_lima_b) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Modal Kerja 1 bulan (ditransfer ke rek Ops TMUK)</td>
                            <td class="right aligned">0</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Lain-lain</td>
                            <td class="right aligned">{{ rupiah($data->i_bila_ada_biaya_tambahan) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Lain-lain</td>
                            <td class="right aligned">0</td>
                        </tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th class="center aligned"></th>
                        <?php $total_perkiraan = $data->i_lima_a + $data->i_tiga_a + $data->i_tiga_b + $data->i_tiga_c + $data->i_tiga_d + $data->i_tiga_e + $data->i_tiga_f + $data->i_empat_a + $data->i_empat_c + $data->i_empat_b + $data->i_satu + $data->i_lima_b + $data->i_bila_ada_biaya_tambahan; ?>
                        <th class="right aligned"><b>{{ rupiah($total_perkiraan) }}</b></th>
                        @if($akuisisi)
                        <th class="center aligned"></th>
                        <?php $total_pengeluaran = $start + $perijinan + $renovasi + $kusen + $folding + $instalasi + $pemasangan + $signage + $sipil + $non_elektronik + $elektronik + $stock_awal + $modal_kerja + $lain_lain; ?>
                        <th class="right aligned"><b>{{ rupiah($total_pengeluaran) }}</b></th>
                        @else
                        <th class="center aligned"></th>
                        <th class="right aligned"><b>{{ rupiah(0) }}</b></th>
                        @endif
                    </tr>
                    <tr>
                        <th class="center aligned"></th>
                        <?php $total_keseluruhan_perkiraan = $totaldana - $total_perkiraan; ?>
                        <th class="right aligned"><b>{{ rupiah($total_keseluruhan_perkiraan) }}</b></th>
                        <th class="center aligned"></th>
                        @if($akuisisi)
                        <?php $pengeluaran_total = $sum_tot - $total_pengeluaran; ?>
                        <th class="right aligned"><b>{{ rupiah($pengeluaran_total) }}</b></th>
                        @else
                        <?php $pengeluaran_total = $sum_tot; ?>
                        <th class="right aligned"><b>{{ rupiah($pengeluaran_total) }}</b></th>
                        @endif
                    </tr>
                    
                </tfoot>
            </table>
        @endforeach
    @else
        <div class="ui segment">
          <p align="center">Maaf tidak ada data</p>
        </div>
    @endif
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth() + 1, 0, 0, 0, 0);
        
        $('#from').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){ 
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 2;
                var year = date.getFullYear();
                    if (month > 12) {
                        month = month - 12;
                        year = year + 1;
                    }
                $('#to').calendar("set date", new Date(year,month));
                // $('#selesai_filter').val(month+'/'+year);
            }
        })

        $('#to').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() - 2;
                var year = date.getFullYear();
                    if (month < 0) {
                        month = month + 12;
                        year = year - 1;
                    }

                // $('#from').calendar("set date", new Date(year,month));
                $('#mulai_filter').val((month+1)+'/'+year);
            }
        })
    });
</script>