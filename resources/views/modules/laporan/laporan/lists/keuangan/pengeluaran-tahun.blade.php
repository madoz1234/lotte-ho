
@if(isset($records))
<div class="ui form" style="float:right">
   <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-laporan-keuangan','pengeluaran-tahun');">
    Print
    <i class="print icon"></i>
</a>
<div class="ui green export buttons" style="margin-right: 5px">
    <div class="ui button" onclick="printLaporan('exel-laporan-laporan-keuangan','pengeluaran-tahun');"><i class="file excel outline icon"></i>Export</div>
</div>
</div>
@endif

<div class="title" style="font-size: 17px;">PENGELUARAN TAHUN</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('pengeluaran-tahun','-laporan-keuangan');">
        <i class="search icon"></i>
    </button>
</div>
<br>

@if(isset($records))
@if(count($records) != 0)
            <table id="reportTable" class="ui celled compact red table"  cellspacing="0">
                <thead>
                    <tr>
                        <th class="center aligned">Account Name</th>
                        @foreach ($header as $row)
                            <th class="center aligned" >{{ $row }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($records as $key => $elem)
                            <?php 
                                $akun_name = $key;
                                $nil   = $nilai_temp; 
                                $nilai = $nilai_temp;
                            ?>
                            @foreach ($elem as $key => $nils)
                                <tr>
                                    <td>
                                       {{ $akun_name }}
                                    </td>
                                    @foreach ($nilai as $keys => $val)
                                        <td>{{ rupiah(isset($nils[$keys])?$nils[$keys]:$nil[$keys]) }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                    @endforeach
                </tbody>
            </table>
@else
<div class="ui segment">
  <p align="center">Maaf tidak ada data</p>
</div>
@endif
@else
<div class="ui segment">
  <p align="center">Filter tanggal terlebih dahulu</p>
</div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth() + 1, 0, 0, 0, 0);
        
        $('#from').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            }
        })

        $('#to').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            }
        })
    });

    @if(isset($records))
        @if(count($records) != 0)
            $('#reportTable').DataTable({
                paging: false,
                filter: false,
                lengthChange: false,
                ordering:false,
                scrollX: false,
                scrollY: 400,
            })
        @endif
    @endif
    
</script>