@if(isset($records))
<div class="ui form" style="float:right">
   <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-laporan-keuangan','fokus-keuangan');">
        Print
        <i class="print icon"></i>
    </a>
    {{-- <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-laporan-keuangan','fokus-keuangan');"><i class="file excel outline icon"></i>Export</div>
    </div> --}}
</div>
@endif


<div class="title" style="font-size: 17px;">LAPORAN PERFORMANCE TMUK</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('fokus-keuangan','-laporan-keuangan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>

@if(isset($records))
    @if($records->count() != 0)
        @foreach ($records as $row)
            {{-- @foreach ($penjualan as $data) --}}
            <div class="ui packed yellow segment" style="overflow-x: auto;">
            <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="center aligned" width="40px">ITEM</th>
                        <th class="center aligned" width="40px">KKI</th>
                        <th class="center aligned" width="40px">%</th>
                        @for($m = 0; $m <= $diff; $m++)
                            <th class="center aligned" width="40px">{{ $dayStart->copy()->addMonths($m)->format('F / Y') }}</th>
                            <th class="center aligned" width="40px">%</th>
                        @endfor
                        <th class="center aligned" width="40px">Akumulasi</th>
                        <th class="center aligned" width="40px">Average</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="left aligned">PENJUALAN</td>
                        <td class="center aligned">{{ rupiah($row->ii_penjualan_normal) }}</td>
                        <?php $persen_penjualan = 0; $hppp=0; ?>
                        <?php  $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  ($row->ii_penjualan_normal / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan) }}%</td>
                        <?php $akumulasi = 0; $jurnal_penjualan=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                                $akumulasi += $jurnal_penjualan;
                            ?>
                            <td class="center aligned">{{ rupiah($jurnal_penjualan) }}</td>
                            <td class="center aligned">{{ $jurnal_penjualan == 0 ? 0 : rupiah($jurnal_penjualan / $jurnal_penjualan * (100)) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">HPP</td>
                        <td class="center aligned">{{ rupiah($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2) }}</td>
                        <?php $persen_penjualan=0; ?>
                        <?php  $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  (($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2) / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan) }}%</td>
                        <?php $akumulasi = 0; $hppp=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');
                                $akumulasi += $hppp;
                            ?>
                            <td class="center aligned">{{ rupiah($hppp) }}</td>
                            <td class="center aligned">{{ $hppp == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $hppp / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Margin</td>
                        <td class="center aligned">{{ rupiah($row->ii_penjualan_normal - ($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2) ) }}</td>
                        <?php $persen_penjualan =0; ?>
                        <?php  $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  (($row->ii_penjualan_normal - ($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2)) / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan) }}%</td>
                        <?php $akumulasi = 0; $margin=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $margin = $jurnal_penjualan - $hppp;
                                $akumulasi += $margin;
                            ?>
                            <td class="center aligned">{{ rupiah($margin) }}</td>
                            <td class="center aligned">{{ $margin == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $margin / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $akumulasi += $total;
                            ?>
                            <td class="center aligned"></td>
                            <td class="center aligned"></td>
                        @endfor
                        <td class="center aligned" width="40px"></td>
                        <td class="center aligned" width="40px"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">SPD</td>
                        <td class="center aligned">{{ rupiah($row->ii_sales_normal) }}</td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; $jurnal_penjualan= 0; $spd_jual =0; $toko=0; $toko_buka=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                            
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                                $toko_buka = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);

                                $toko = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? count($toko_buka)-1 : count($toko_buka);

                                $spd_jual = $jurnal_penjualan == 0 ? 0 : $jurnal_penjualan / $toko;
                                $akumulasi += $spd_jual;
                            ?>
                            <td class="center aligned">{{ rupiah($spd_jual) }}</td>
                            {{-- <td class="center aligned">{{ $toko }}</td> --}}
                            <td class="center aligned">{{-- {{ $spd_jual == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $spd_jual / $jurnal_penjualan * (100) : 0 ) }}% --}}</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>

                    <tr>
                        <td class="left aligned">STD</td>
                        <td class="center aligned">{{ rupiah($row->ii_struk_normal) }}</td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; $struk = 0; $std_jual=0; $toko_buka=0; $toko=0; $kondisi_struk=0;  ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $struk = Lotte\Models\Trans\TransRekapPenjualanTmuk::getCountStruk($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $kondisi_struk = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? $struk-$struk_akhir : $struk ;

                                $toko_buka = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $toko = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? count($toko_buka)-1 : count($toko_buka);

                                $std_jual = $kondisi_struk == 0 ? 0 : $kondisi_struk / $toko;

                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                                $akumulasi += $std_jual;
                            ?>
                            <td class="center aligned">{{ rupiah($std_jual) }}</td>
                            <td class="center aligned">{{-- {{ $std_jual == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $std_jual / $jurnal_penjualan * (100) : 0 ) }}% --}}</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">APC</td>
                        <td class="center aligned">{{ $row->ii_sales_normal == 0 ?  '0' : rupiah($row->ii_sales_normal / $row->ii_struk_normal) }}</td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; $spd_jual = 0; $std_jual = 0;  $apc_jual = 0; $struk = 0; $toko_buka=0; $toko=0; $kondisi_struk=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 

                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode);

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                                $toko_buka = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $toko = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? count($toko_buka)-1 : count($toko_buka);

                                $spd_jual = $jurnal_penjualan == 0 ? 0 : $jurnal_penjualan / $toko ;
                                $struk = Lotte\Models\Trans\TransRekapPenjualanTmuk::getCountStruk($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $kondisi_struk = $dayStart->copy()->addMonths($m)->format('Ym') == date('Ym') ? $struk-$struk_akhir : $struk ;

                                $std_jual = $kondisi_struk == 0 ? 0 : $kondisi_struk / $toko;
                                $apc_jual =  $spd_jual == 0 ? 0 : $spd_jual / $std_jual;
                                $akumulasi += $apc_jual;
                            ?>
                            <td class="center aligned">{{ rupiah($apc_jual) }}</td>
                            <td class="center aligned">{{-- {{ $apc_jual == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $apc_jual / $jurnal_penjualan * (100) : 0 ) }}% --}}</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $akumulasi += $total;
                            ?>
                            <td class="center aligned"></td>
                            <td class="center aligned"></td>
                        @endfor
                        <td class="center aligned" width="40px"></td>
                        <td class="center aligned" width="40px"></td>
                    </tr>
                    <thead>
                         <tr>
                            <th class="left aligned">Biaya Operasional</th>
                            <th class="center aligned"></th>
                            <th class="center aligned"></th>
                            @for($m = 0; $m <= $diff; $m++)
                                <?php 
                                    $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                                ?>
                                <th class="center aligned"></th>
                                <th class="center aligned"></th>
                            @endfor
                            <th class="center aligned" width="40px"></th>
                            <th class="center aligned" width="40px"></th>
                        </tr>
                    </thead>

                    <tr>
                    <?php $utilities = ($row->iv_satu_a_estimasi2+$row->iv_satu_b_estimasi2) + ($row->iv_satu_c_estimasi2); ?>
                        <td class="left aligned">Biaya Air , Listrik & Telp/Internet</td>
                        <td class="center aligned">{{ rupiah($utilities) }}</td>
                        <?php $persen_penjualan_listrik=0; ?>
                        <?php  $persen_penjualan_listrik = $row->ii_penjualan_normal == 0 ? 0 :  ($utilities / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_listrik) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili =0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                    <?php $perlengapan_toko = 0; ?>
                        <td class="left aligned">Perlengkapan Toko (ATK)</td>
                        <td class="center aligned">{{ rupiah($perlengapan_toko) }}</td>
                        <?php $persen_penjualan_atk=0; ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_atk) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili =0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.12');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Gaji Karyawan</td>
                        <td class="center aligned">{{ rupiah($row->iv_dua_estimasi2) }}</td>
                        <?php $persen_penjualan_gaji=0; ?>
                        <?php  $persen_penjualan_gaji = $row->ii_penjualan_normal == 0 ? 0 :  ($row->iv_dua_estimasi2 / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_gaji) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Pembungkus</td>
                        <td class="center aligned">{{ rupiah($row->iv_satu_f_estimasi2) }}</td>
                        <?php $persen_penjualan_bungkus=0; ?>
                        <?php  $persen_penjualan_bungkus = $row->ii_penjualan_normal == 0 ? 0 :  ($row->iv_satu_f_estimasi2 / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah(ceil($persen_penjualan_bungkus)) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Sewa Toko</td>
                        <td class="center aligned">{{ rupiah($row->iv_empat_d_estimasi2) }}</td>
                        <?php $persen_penjualan_sewa=0; ?>
                        <?php  $persen_penjualan_sewa = $row->ii_penjualan_normal == 0 ? 0 :  ($row->iv_empat_d_estimasi2 / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_sewa) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode,' 4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Lain-lain</td>
                        <?php $biaya_lain = $row->iv_satu_e_estimasi2 + $row->iv_tambahan_satu_b + $row->iv_tambahan_dua_b + $row->iv_tambahan_tiga_b + $row->iv_tambahan_empat_b;?>
                        <td class="center aligned">{{ rupiah($biaya_lain) }}</td>
                        <?php $persen_penjualan_lain=0; ?>
                        <?php  $persen_penjualan_lain = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_lain / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_lain) }}%</td>
                        <?php $utili=0; ?>
                         @for($m = 0; $m <= $diff; $m++)
                        <?php 
                            $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                            $akumulasi += $total;
                            ;
                        ?>
                        <td class="center aligned">0</td>
                        <td class="center aligned">{{ $utili == 0 ? 0 : rupiah($utili / 0 * (100)) }}%</td>
                        @endfor
                        <td class="center aligned">0</td>
                        <td class="center aligned">0</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Keamanan</td>
                        <?php $biaya_keamanan = $row->iv_satu_d_estimasi2;?>
                        <td class="center aligned">{{ rupiah($biaya_keamanan) }}</td>
                        <?php $persen_penjualan_keamanan=0; ?>
                        <?php  $persen_penjualan_keamanan = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_keamanan / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_keamanan) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Pengiriman</td>
                        <?php $biaya_pengiriman = 0;?>
                        <td class="center aligned">{{ rupiah($biaya_pengiriman) }}</td>
                        <?php $persen_penjualan_pengiriman=0; ?>
                        <?php  $persen_penjualan_pengiriman = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_pengiriman / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_pengiriman) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.1');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Distribusi (Administrasi)</td>
                        <?php $biaya_adm = $row->iv_satu_i_estimasi2;?>
                        <td class="center aligned">{{ rupiah($biaya_adm) }}</td>
                        <?php $persen_penjualan_adm=0; ?>
                        <?php  $persen_penjualan_adm = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_adm / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_adm) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Bensin</td>
                        <?php $biaya_bensin = 0;?>
                        <td class="center aligned">{{ rupiah($biaya_bensin) }}</td>
                        <?php $persen_penjualan_bensin=0; ?>
                        <?php  $persen_penjualan_bensin = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_bensin / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_bensin) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.11');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Barang Rusak/Hilang</td>
                        <?php $barang_rusak = $row->iv_satu_g_estimasi2 + $row->iv_satu_h_estimasi2;?>
                        <td class="center aligned">{{ rupiah($barang_rusak) }}</td>
                        <?php $persen_penjualan_rusak=0; ?>
                        <?php  $persen_penjualan_rusak = $row->ii_penjualan_normal == 0 ? 0 :  ($barang_rusak / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_rusak) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Premi Asuransi</td>
                        <?php $biaya_premi = $row->iv_tiga_a_estimasi2 + $row->iv_tiga_b_estimasi2; ?>
                        <td class="center aligned">{{ rupiah($biaya_premi) }}</td>
                        <?php $persen_penjualan_premi=0; ?>
                        <?php  $persen_penjualan_premi = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_premi / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_premi) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Depresiasi & Amortiasi</td>
                        <?php $dep_amor = $row->iv_empat_a_estimasi2 + $row->iv_empat_b_estimasi2 + $row->iv_empat_c_estimasi2 ; ?>
                        <td class="center aligned">{{ rupiah($dep_amor) }}</td>
                        <?php $persen_penjualan_amor=0; ?>
                        <?php $persen_penjualan_amor = $row->ii_penjualan_normal == 0 ? 0 :  ($dep_amor / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_amor) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Lain-Lain Operasional</td>
                        <?php $biaya_lain_operasional = 0 ; ?>
                        <td class="center aligned">{{ rupiah($biaya_lain_operasional) }}</td>
                        <?php $persen_penjualan_operasional=0; ?>
                        <?php $persen_penjualan_operasional = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_lain_operasional / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_operasional) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.14');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Lain-Lain Non-Operasional</td>
                        <?php $biaya_lain_non_operasional = 0 ; ?>
                        <td class="center aligned">{{ rupiah($biaya_lain_non_operasional) }}</td>
                        <?php $persen_penjualan_non_operasional=0; ?>
                        <?php $persen_penjualan_non_operasional = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_lain_non_operasional / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_non_operasional) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.15');
                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Selisih Kas</td>
                        <td class="center aligned">0</td>
                        <td class="center aligned">0%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; $kas_kurang=0; $kas_lebih=0; $selisih_kas=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $kas_kurang = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '8.1.0.0');
                                $kas_lebih = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '7.1.0.0');

                                $selisih_kas = $kas_kurang - $kas_lebih ;


                            

                                $total_akumulasi += $selisih_kas;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($selisih_kas) }}</td>
                            <td class="center aligned">{{ $selisih_kas == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $selisih_kas / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        @for($m = 0; $m <= $diff; $m++)
                        <?php 
                            $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                            $akumulasi += $total;
                            ;
                        ?>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        @endfor
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Total Biaya</td>
                        <td class="center aligned">{{ rupiah($utilities + $biaya_premi + $biaya_lain + $perlengapan_toko + $row->iv_dua_estimasi2 + $row->iv_satu_f_estimasi2 + $barang_rusak + $dep_amor + $row->iv_empat_d_estimasi2 + $biaya_adm + $biaya_keamanan + $biaya_pengiriman + $biaya_bensin + $biaya_lain_operasional + $biaya_lain_non_operasional) }}</td>
                        <td class="center aligned">{{ rupiah($persen_penjualan_amor + $persen_penjualan_premi + $persen_penjualan_rusak + $persen_penjualan_lain + $persen_penjualan_sewa + $persen_penjualan_bungkus + $persen_penjualan_gaji + $persen_penjualan_listrik + $persen_penjualan_atk + $persen_penjualan_adm + $persen_penjualan_keamanan + $persen_penjualan_bensin + $persen_penjualan_pengiriman + $persen_penjualan_operasional + $persen_penjualan_non_operasional) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $total_biaya_jual=0; $kas_kurang=0; $$kas_lebih=0; $selisih_kas=0;  ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $kas_kurang = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '8.1.0.0');
                                $kas_lebih = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '7.1.0.0');

                                $selisih_kas = $kas_kurang - $kas_lebih ;

                                $total_biaya_jual = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4')+ Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1') +  Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10')  + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.1') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.12') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.11') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.14') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.15') + $selisih_kas;

                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = (Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.3.0')) - Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');


                                $total_akumulasi += $total_biaya_jual;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($total_biaya_jual) }}</td>
                            <td class="center aligned">{{ $total_biaya_jual == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $total_biaya_jual / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($total_akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        @for($m = 0; $m <= $diff; $m++)
                        <?php 
                            $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                            $akumulasi += $total;
                            ;
                        ?>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        @endfor
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>

                    <thead>
                         <tr>
                            <th class="center aligned">Laba Bersih</th>
                            <?php 
                                $laba_bersih = ($row->ii_penjualan_normal - ($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2)) - ($utilities + $biaya_premi + $biaya_lain + $perlengapan_toko + $row->iv_dua_estimasi2 + $row->iv_satu_f_estimasi2 + $barang_rusak + $dep_amor + $row->iv_empat_d_estimasi2 + $biaya_keamanan + $biaya_adm + $biaya_pengiriman + $biaya_lain_non_operasional + $biaya_lain_operasional)   ?>
                            <th class="center aligned">{{ rupiah($laba_bersih) }}</th>
                            <?php $persen_penjualan=0; ?>
                            <?php
                                $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  ($laba_bersih / $row->ii_penjualan_normal) * 100 ?>
                            <th class="center aligned">{{ rupiah($persen_penjualan) }}%</th>
                            <?php $akumulasi_laba_tmuk = 0; $laba_bersih_tmuk_b=0; $kas_kurang=0; $kas_lebih=0; $selisih_kas=0; ?>
                            @for($m = 0; $m <= $diff; $m++)
                            <?php 

                                $jurnal_penjualan_b_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan_b = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $margin = $jurnal_penjualan_b - $hppp;


                                $kas_kurang = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '8.1.0.0');
                                $kas_lebih = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '7.1.0.0');

                                $selisih_kas = $kas_kurang - $kas_lebih ;


                                $total_biaya_b = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4')+ Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1') +  Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10')  + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.1') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.12') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.11') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.14') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.15') + $selisih_kas;

                                $jurnal_penjualan_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');


                                $laba_bersih_tmuk_b = $margin - $total_biaya_b;
                                $akumulasi_laba_tmuk += $laba_bersih_tmuk_b;


                            ?>
                            <th class="center aligned">{{ rupiah($laba_bersih_tmuk_b) }}</th>
                            <th class="center aligned">{{ $laba_bersih_tmuk_b == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $laba_bersih_tmuk_b / $jurnal_penjualan * (100) : 0 ) }}%</th>
                            @endfor
                            <th class="center aligned" width="40px">{{ rupiah($akumulasi_laba_tmuk) }}</th>
                            <th class="center aligned" width="40px">{{ rupiah($akumulasi_laba_tmuk / ($diff + 1)) }}</th>
                        </tr>
                        <tr>
                            <th class="center aligned">Laba Cash</th>
                            <?php
                                $dep_amor = $row->iv_empat_a_estimasi2 + $row->iv_empat_b_estimasi2 + $row->iv_empat_c_estimasi2 ; 
                                $laba_cash =  $laba_bersih + $dep_amor;
                                ?>
                            <th class="center aligned">{{ rupiah($laba_cash) }}</th>
                            <?php $persen_penjualan=0; ?>
                            <?php 
                                $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  ($laba_cash / $row->ii_penjualan_normal) * 100 ?>
                            <th class="center aligned">{{ rupiah($persen_penjualan) }}%</th>
                            <?php $akumulasi_laba_tmuk = 0; $laba_bersih_tmuk_b=0; ?>
                            @for($m = 0; $m <= $diff; $m++)
                            <?php 

                                $jurnal_penjualan_b_backup = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $jurnal_penjualan_b = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $margin = $jurnal_penjualan_b - $hppp;


                                $kas_kurang = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '8.1.0.0');
                                $kas_lebih = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '7.1.0.0');

                                $selisih_kas = $kas_kurang - $kas_lebih ;


                                $total_biaya_b = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4')+ Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1') +  Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10')  + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.1') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.12') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.11') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.14') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.15') + $selisih_kas;


                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0');

                                $amor = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.2.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.3.3.0');


                                $laba_bersih_tmuk_b = ($margin - $total_biaya_b) + $amor;
                                $akumulasi_laba_tmuk += $laba_bersih_tmuk_b;


                            ?>
                            <th class="center aligned">{{ rupiah($laba_bersih_tmuk_b) }}</th>
                            <th class="center aligned">{{ $laba_bersih_tmuk_b == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $laba_bersih_tmuk_b / $jurnal_penjualan * (100) : 0 ) }}%</th>
                            @endfor
                            <th class="center aligned" width="40px">{{ rupiah($akumulasi_laba_tmuk) }}</th>
                            <th class="center aligned" width="40px">{{ rupiah($akumulasi_laba_tmuk / ($diff + 1)) }}</th>
                        </tr>
                    </thead>




                <thead>
                    
                </thead>
                    
                    
                </tbody>
                <tfoot>
                    
                </tfoot>
            </table>
            </div>
        @endforeach
    @else
        <div class="ui segment">
          <p align="center">Maaf tidak ada data</p>
        </div>
    @endif
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth() + 1, 0, 0, 0, 0);
        
        $('#from').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){ 
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 2;
                var year = date.getFullYear();
                    if (month > 12) {
                        month = month - 12;
                        year = year + 1;
                    }
                $('#to').calendar("set date", new Date(year,month));
                // $('#selesai_filter').val(month+'/'+year);
            }
        })

        $('#to').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() - 2;
                var year = date.getFullYear();
                    if (month < 0) {
                        month = month + 12;
                        year = year - 1;
                    }

                // $('#from').calendar("set date", new Date(year,month));
                $('#mulai_filter').val((month+1)+'/'+year);
            }
        })
    });
</script>