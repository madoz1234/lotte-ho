@if(isset($records))
<div class="ui form" style="float:right">
   <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-laporan-keuangan','fokus-keuangan');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-laporan-keuangan','fokus-keuangan');"><i class="file excel outline icon"></i>Export</div>
    </div>
</div>
@endif


<div class="title" style="font-size: 17px;">LAPORAN PERFORMANCE TMUK</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('fokus-keuangan','-laporan-keuangan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>

{{-- {{ dd($penjualan) }} --}}

@if(isset($records))
    @if($records->count() != 0)
        @foreach ($records as $row)
            {{-- @foreach ($penjualan as $data) --}}
            <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="center aligned" width="40px">ITEM</th>
                        <th class="center aligned" width="40px">KKI</th>
                        <?php
                            $start_1 = $start;
                            $end_1 = $end;
                        ?>
                        @while (strtotime($start_1) <= strtotime($end_1))
                            <th class="center aligned" width="40px">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$start_1)->format('F') }}</th>
                            <?php
                                $start_1 = date('Y-m-d', strtotime('+1 month', strtotime($start_1)));
                            ?>
                        @endwhile
                        <th class="center aligned" width="40px">Total</th>
                        <th class="center aligned" width="40px">Average</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="left aligned">PENJUALAN</td>
                        <td class="center aligned">{{ rupiah($row->ii_penjualan_normal) }}</td>
                        <?php $total_penjualan=0; 
                            $count = 3;
                            $average = 0;
                        ?>
                        <?php
                            $start_2 = $start;
                            $end_2 = $end;
                        ?>
                        @while (strtotime($start_2) <= strtotime($end_2))
                            <?php
                                $bulan = \Carbon\Carbon::createFromFormat('Y-m-d',$start_2)->format('n');
                            ?>
                            @if($penjualan->where('month',$bulan)->first())
                                <?php $r = $penjualan->where('month',$bulan)->first();?>
                                <td class="center aligned">{{ rupiah($r->total_penjualan) }}</td>
                                <?php 
                                    $total_penjualan += $r->total_penjualan;
                                ?>
                            @else
                                <td class="center aligned">{{ rupiah(0) }}</td>
                                <?php 
                                    $total_penjualan += 0;
                                ?>
                            @endif
                            <?php
                                $start_2 = date('Y-m-d', strtotime('+1 month', strtotime($start_2)));
                            ?>
                        @endwhile
                        <?php
                            $average = $total_penjualan / $count;
                        ?>
                        <td class="center aligned">{{ rupiah($total_penjualan) }}</td>
                        <td class="center aligned">{{ rupiah($average) }}</td>
                    </tr>
                    <tr>
                    <?php $hpp = $row->ii_penjualan_normal-$row->iii_labakotor_estimasi2 ; ?>
                        <td class="left aligned">HPP</td>
                        <td class="center aligned">{{ rupiah($hpp) }}</td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Margin</td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">SPD</td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">STD</td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">APC</td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Setoran Penjualan</td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Pembelian ke Lotte</td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                <thead>
                    <tr>
                        <th class="center aligned" width="40px">BIAYA Oprasional</th>
                        <th class="center aligned" width="40px"></th>
                        @foreach ($penjualan as $data)
                        <th class="center aligned" width="40px"></th>
                        @endforeach
                        <th class="center aligned" width="40px"></th>
                        <th class="center aligned" width="40px"></th>
                    </tr>
                </thead>
                    <tr>
                    <?php $utilities = ($row->iv_satu_a_estimasi1+$row->iv_satu_b_estimasi1) + ($row->iv_satu_c_estimasi1); ?>
                        <td class="left aligned">Biaya Utilities (Air , Listrik & Telp/Internet</td>
                        <td class="center aligned">{{ rupiah($utilities) }}</td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                    <?php $perlengapan_toko = ($row->i_empat_a+$row->i_empat_b) + ($row->i_empat_c); ?>
                        <td class="left aligned">Perlengkapan Toko</td>
                        <td class="center aligned">{{ rupiah($perlengapan_toko) }}</td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Gaji Karyawan</td>
                        <td class="center aligned">{{ rupiah($row->iv_dua_estimasi1) }}</td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Pembungkus</td>
                        <td class="center aligned">{{ rupiah($row->iv_satu_f_estimasi1) }}</td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Pengiriman Barang</td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Lain-lain</td>
                        <td class="center aligned">{{ rupiah($row->iv_satu_e_estimasi1) }}</td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Barang Rusak/Hilang</td>
                        <td class="center aligned">{{ rupiah($row->iv_satu_g_estimasi1) }}</td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Premi Asuransi</td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                    <?php $dep_amor = ($row->iv_empat_a_estimasi1+$row->iv_empat_b_estimasi1) + ($row->iv_empat_c_estimasi1+$row->iv_empat_d_estimasi1); ?>
                        <td class="left aligned">Biaya Depresiasi & Amortiasi</td>
                        <td class="center aligned">{{ rupiah($dep_amor) }}</td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                    <?php $total_biaya = $utilities+$perlengapan_toko+$row->iv_dua_estimasi1+$row->iv_satu_f_estimasi1+$row->iv_satu_e_estimasi1+$row->iv_satu_g_estimasi1+$dep_amor; ?>
                        <td class="left aligned">Total Biaya</td>
                        <td class="center aligned">{{ rupiah($total_biaya) }}</td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        @foreach ($penjualan as $data)
                        <td class="center aligned"></td>
                        @endforeach
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    
                </tbody>
                <tfoot>
                    <tr>
                        <th class="center aligned">Laba Bersih</th>
                        <th class="center aligned"></th>
                        @foreach ($penjualan as $data)
                        <th class="center aligned"></th>
                        @endforeach
                        <th class="center aligned"></th>
                        <th class="center aligned"></th>
                    </tr>
                    <tr>
                        <th class="center aligned">Laba Cash</th>
                        <th class="center aligned"></th>
                        @foreach ($penjualan as $data)
                        <th class="center aligned"></th>
                         @endforeach
                        <th class="center aligned"></th>
                        <th class="center aligned"></th>
                    </tr>
                </tfoot>
            </table>
        @endforeach
    @else
        <div class="ui segment">
          <p align="center">Maaf tidak ada data</p>
        </div>
    @endif
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('#from').calendar({
            type: 'month',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){ 
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 2;
                var year = date.getFullYear();
                    if (month > 12) {
                        month = month - 12;
                        year = year + 1;
                    }
                $('#to').calendar("set date", new Date(year,month));
                // $('#selesai_filter').val(month+'/'+year);
            }
        })

        $('#to').calendar({
            type: 'month',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() - 2;
                var year = date.getFullYear();
                    if (month < 0) {
                        month = month + 12;
                        year = year - 1;
                    }

                // $('#from').calendar("set date", new Date(year,month));
                $('#mulai_filter').val((month+1)+'/'+year);
            }
        })
    });
</script>