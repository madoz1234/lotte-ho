<div class="ui form" style="float:right">
   <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-laporan-keuangan','laba-rugi-perbandingan-anggaran');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-laporan-keuangan','laba-rugi-perbandingan-anggaran');"><i class="file excel outline icon"></i>Export</div>
    </div>
</div>


<div class="title" style="font-size: 17px;">LABA / RUGI (Perbandingan Anggaran)</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" {{-- value="{{ $start or '' }}" --}} placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" {{-- value="{{ $end or '' }}" --}} placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('laba-rugi-perbandingan-anggaran','-laporan-keuangan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>

<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">Deskripsi</th>
            <th class="center aligned" width="40px">Balance</th>
            <th class="center aligned" width="40px">Budget</th>
            <th class="center aligned" width="40px">Selisih</th>
            <th class="center aligned" width="40px">% Slsh</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="center aligned">1</td>
            <td class="left aligned"></td>
            <td class="left aligned"></td>
            <td class="center aligned"></td>
            <td class="center aligned"></td>
            <td class="center aligned"></td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
        $('#from').calendar({
            type: 'month',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){ 
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 2;
                var year = date.getFullYear();
                    if (month > 12) {
                        month = month - 12;
                        year = year + 1;
                    }
                $('#to').calendar("set date", new Date(year,month));
                // $('#selesai_filter').val(month+'/'+year);
            }
        })

        $('#to').calendar({
            type: 'month',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() - 2;
                var year = date.getFullYear();
                    if (month < 0) {
                        month = month + 12;
                        year = year - 1;
                    }

                // $('#from').calendar("set date", new Date(year,month));
                $('#mulai_filter').val((month+1)+'/'+year);
            }
        })
    });
</script>