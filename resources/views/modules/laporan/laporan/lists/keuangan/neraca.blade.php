@if(isset($record))
    <div class="ui form" style="float:right">
       <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-laporan-keuangan','neraca');">
            Print
            <i class="print icon"></i>
        </a>
        {{-- <div class="ui green export buttons" style="margin-right: 5px">
            <div class="ui button" onclick="printLaporan('exel-laporan-laporan-keuangan','neraca');"><i class="file excel outline icon"></i>Export</div>
        </div> --}}
    </div>
@endif

<div class="title" style="font-size: 17px;">NERACA</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('neraca','-laporan-keuangan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>

@if(isset($record))
    <table  class="ui celled compact red table" width="90%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="40px">Description</th>
                @foreach($record['header'] as $row)
                    <th class="center aligned" width="40px">{{ $row }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($record['content'] as $row)
                @if($row['type'] == 'label')
                    @if(isset($row['child']))
                        <tr>
                            <td class="left aligned" colspan="{{ count($record['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                        </tr>
                        @foreach($row['child'] as $rowchild)
                            <tr>
                                <td class="left aligned">&nbsp;&nbsp;&nbsp;&nbsp;{{ $rowchild['text'] }}</td>
                                @foreach($rowchild['data'] as $val)
                                    <td class="right aligned">{{ curencyformat($val) }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="left aligned" colspan="{{ count($record['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                        </tr>
                    @endif
                @else
                    <tr>
                        <td class="left aligned"><b>{{ $row['text'] }}</b></td>
                        @foreach($row['data'] as $val)
                            <td class="right aligned"><b>{{ curencyformat($val) }}</b></td>
                        @endforeach
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), 0, 0, 0, 0);

        $('#from').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){ 
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth();
                var year = date.getFullYear();
                    if (month > 12) {
                        month = month - 12;
                        year = year + 1;
                    }
                // $('#to').calendar("set date", new Date(year,month));
                var tm = new Date(year,month+2) ;
                if (tm > today) {
                    maxDate = today;
                }else{
                    maxDate = tm;
                }

                $('#to').calendar("set date", new Date(year,month)).calendar({
                    type: 'month',
                    minDate: new Date(year,month),
                    maxDate: maxDate,
                    formatter: {
                        date: function (date, settings) {
                            if (!date) return '';
                            var day = date.getDate();
                            var month = date.getMonth() + 1;
                            var year = date.getFullYear();
                            return month+'/'+year;
                        }
                    },
                }).calendar('refresh')
            }
        })

        $('#to').calendar({
            type: 'month',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
        })

    });
</script>