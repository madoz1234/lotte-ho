@if(isset($record))
    <div class="ui form" style="float:right">
     <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-buku-besar','ringkasan-buku-besar');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-akun-buku-besar','ringkasan-buku-besar');"><i class="file excel outline icon"></i>Export</div>
    </div>
    </div>
@endif

<div class="title" style="font-size: 17px;">Ringkasan Buku Besar</div><br>
<div class="field">
    <div class="ui calendar labeled input rbb_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input rbb_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('ringkasan-buku-besar','-akun-buku-besar');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
@if(isset($record))
    <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="5%">No</th>
                <th class="center aligned" width="10%">Tanggal</th>
                <th class="center aligned" width="25%">Nama Akun</th>
                <th class="center aligned" width="15%">Saldo Awal</th>
                <th class="center aligned" width="10%">Perubahan Debit</th>
                <th class="center aligned" width="10%">Perubahan Kredit</th>
                <th class="center aligned" width="10%">Perubahan Bersih</th>
                <th class="center aligned" width="15%">Saldo Akhir</th>
            </tr>
        </thead>
        <tbody>
            @if($record->count() > 0)
                <?php 
                    $i           = 1; 
                    $saldo_awal = [];
                    $saldo_akhir = [];
                    $nama = [];
                ?>
                @foreach ($record as $data)
                    <tr>
                        <td class="center aligned">{{ $i }}</td>
                        <td class="center aligned">{{ \Carbon\Carbon::parse($data->tanggal)->format('Y-m-d') }}</td>
                        <?php 
                            $nama[$i] = $data->coa->nama;
                        ?>
                        <td class="left aligned">{{ $data->coa_kode }} - {{ $data->coa->nama }}</td>
                        <td class="right aligned">
                            <?php 
                                $saldo_awal[$i] = 0;
                                if(isset($nama[$i - 1]) && $nama[$i - 1] == $nama[$i])
                                {
                                    if(isset($saldo_akhir[$i - 1]))
                                    {
                                        $saldo_awal[$i] = $saldo_akhir[$i - 1];
                                    }
                                }
                            ?>
                            {{ rupiah($saldo_awal[$i]) }}
                        </td>
                        <td class="right aligned">
                            <?php 
                                $jumlah_debit = $data->posisi=='D' ? $data->jumlah : '0';
                            ?>
                            {{ rupiah($jumlah_debit) }}
                        </td>
                        <td class="right aligned">
                            <?php 
                                $jumlah_kredit = $data->posisi=='K' ? $data->jumlah : '0';
                            ?>
                            {{ rupiah($jumlah_kredit) }}
                        </td>
                        <td class="right aligned">
                            <?php 
                                $posisi_normal = $data->coa->posisi;

                                if ($posisi_normal == 'D') {
                                    $bersih = $jumlah_debit - $jumlah_kredit;
                                }else{
                                    $bersih = $jumlah_kredit - $jumlah_debit;
                                }
                            ?>
                            
                            {{ rupiah($bersih) }}
                        </td>
                        <td class="right aligned">
                            <?php 
                                $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                            ?>
                            {{ rupiah($saldo_akhir[$i]) }}
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            @else
                <tr>
                    <td colspan="8">
                        <center><i>Maaf tidak ada data</i></center>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.rbb_date_from').calendar({
            type: 'date',
            endCalendar: $('.rbb_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.rbb_date_to').calendar({
            type: 'date',
            startCalendar: $('.rbb_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        @if(isset($record))
            @if($record->count() != 0)
                $('#reportTable').DataTable({
                    paging: false,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                    scrollX: false,
                    scrollY: 400,
                })
            @endif
        @endif
    });
</script>