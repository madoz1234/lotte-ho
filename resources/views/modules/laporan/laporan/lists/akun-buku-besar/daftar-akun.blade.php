@if(isset($record))
    @if($record->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-buku-besar','daftar-akun');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-akun-buku-besar','daftar-akun');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif


<div class="title" style="font-size: 17px;">Daftar COA</div><br>
<div class="field">
    <div class="ui calendar labeled input da_date">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $date or '' }}" placeholder="Tanggal">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('daftar-akun','-akun-buku-besar');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>

@if(isset($record))
    <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="40px">#</th>
                <th class="center aligned" width="40px">No Akun</th>
                <th class="center aligned" width="40px">Nama Akun</th>
                <th class="center aligned" width="40px">Status</th>
            </tr>
        </thead>
        <tbody>

            @if(count($record) != 0)
                <?php $i = 1; ?>
                @foreach ($record as $row)
                    <tr>
                        <td class="center aligned">{{ $i }}</td>
                        <td class="center aligned">{{ $row->kode }}</td>
                        <td class="left aligned">{{ $row->nama }}</td>
                        <td class="center aligned">{{ $row->status = 1 ? ' Aktif' : 'Non-Aktif' }}</td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            @else
                <tr>
                    <td colspan="4">
                        <center><i>Maaf tidak ada data</i></center>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.da_date').calendar({
            type: 'date',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })
    });
</script>