@if(isset($record))
    @if (!empty($record))
        <div class="ui form" style="float:right">
            <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-buku-besar','daftar-history-gl');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-akun-buku-besar','daftar-history-gl');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif
{{-- <div class="title" style="font-size: 17px;">Daftar History GL</div><br> --}}
<div class="title" style="font-size: 17px;">Jurnal per Akun</div><br>
<div class="field">
    <div class="ui calendar labeled input jpa_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input jpa_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <div class="ui labeled input" style="width: 300px;">
        <select name="filter[coa]" class="ui search selection dropdown nama_akun">
            {!! \Lotte\Models\Master\Coa::options(function($q){
                                return $q->kode.' - '.$q->nama;
                             }, 'kode',['selected' => isset($coa) ? $coa : '', 
                                        'filters' => [function($query){ $query->where('status', [1]); }],
                                        'orders' => ['kode']], '-- Akun COA --') !!}
        </select>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="filter('daftar-history-gl','-akun-buku-besar');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
@if(isset($record))
    @if (!empty($record))
        @foreach ($record as $row)
                <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="center aligned" rowspan="2" style="width: 26%;">Nama Akun</th>
                            <th class="center aligned" rowspan="2" style="width: 10%;">Tanggal</th>
                            <th class="center aligned" rowspan="2" style="width: 10%;">ID Transaksi</th>
                            <th class="center aligned" rowspan="2" style="width: 12%;">Saldo Awal</th>
                            <th class="center aligned" rowspan="2" style="width: 10%;">Perubahan Debit</th>
                            <th class="center aligned" rowspan="2" style="width: 10%;">Perubahan Kredit</th>
                            <th class="center aligned" rowspan="2" style="width: 10%;">Perubahan Bersih</th>
                            <th class="center aligned" rowspan="2" style="width: 12%;">Saldo Akhir</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($row->jurnal->count() != 0)
                            <?php 
                            $i           = 1; 
                            $saldo_awal = [];
                            $saldo_akhir = [];
                            ?>
                            @foreach ($row->jurnal->sortBy('tanggal') as $key => $rw)
                                <?php $tanggal = \Carbon\Carbon::parse($rw->tanggal)->format('Y-m-d')?>
                                    <tr>
                                        <td class="left aligned">{{ $rw->coa_kode }} - {{ $rw->coa->nama }}</td>
                                        <td class="center aligned">{{ $tanggal }}</td>
                                        <td class="center aligned">{{ $rw->idtrans }}</td>
                                        <td class="right aligned">
                                            <?php 
                                                if ($i == 1) {
                                                    $saldo_awal[$i] = $saldo_kemarin;
                                                }else{
                                                    $saldo_awal[$i] = $saldo_akhir[$i-1];
                                                }
                                            ?>
                                            {{ rupiah($saldo_awal[$i]) }}
                                        </td>
                                        <td class="right aligned">
                                            <?php 
                                                $jumlah_debit = $rw->posisi=='D' ? $rw->jumlah : '0';
                                            ?>
                                            {{ rupiah(abs($jumlah_debit)) }}
                                        </td>
                                        <td class="right aligned">
                                            <?php 
                                                $jumlah_kredit = $rw->posisi=='K' ? $rw->jumlah : '0';
                                            ?>
                                            {{ rupiah(abs($jumlah_kredit)) }}
                                        </td>
                                        <td class="right aligned">
                                            <?php 
                                                $posisi_normal = $row->posisi;

                                                if ($posisi_normal == 'D') {
                                                    $bersih = $jumlah_debit - $jumlah_kredit;
                                                }else{
                                                    $bersih = $jumlah_kredit - $jumlah_debit;                                        
                                                }

                                            ?>
                                            {{ rupiah($bersih) }}
                                        </td>
                                        <td class="right aligned">
                                            <?php 
                                                $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                                            ?>
                                            {{ rupiah($saldo_akhir[$i]) }}
                                        </td>
                                    </tr>
                                <?php $i++; ?>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <center><i>Maaf tidak ada data</i></center>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
        @endforeach
    @else
        <div class="ui segment">
          <p align="center">Filter Akun Coa terlebih dahulu</p>
        </div>
    @endif
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.jpa_date_from').calendar({
            type: 'date',
            endCalendar: $('.jpa_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.jpa_date_to').calendar({
            type: 'date',
            startCalendar: $('.jpa_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.nama_akun').select2()
    });
</script>