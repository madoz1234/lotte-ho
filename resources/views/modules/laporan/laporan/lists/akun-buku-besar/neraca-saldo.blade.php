@if(isset($record))
    <div class="ui form" style="float:right">
       <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-buku-besar','neraca-saldo');">
            Print
            <i class="print icon"></i>
        </a>
        <div class="ui green export buttons" style="margin-right: 5px">
            <div class="ui button" onclick="printLaporan('exel-laporan-akun-buku-besar','neraca-saldo');"><i class="file excel outline icon"></i>Export</div>
        </div>
    </div>
@endif

<div class="title" style="font-size: 17px;">Neraca Saldo</div><br>
<div class="field">
    <div class="ui calendar labeled input ns_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input ns_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('neraca-saldo','-akun-buku-besar');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
@if(isset($record))
    <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="5%">#</th>
                <th class="center aligned" width="30%">Nama Akun</th>
                <th class="center aligned" width="15%">Saldo Awal</th>
                <th class="center aligned" width="10%">Perubahan Debit</th>
                <th class="center aligned" width="10%">Perubahan Kredit</th>
                <th class="center aligned" width="15%">Perubahan Bersih</th>
                <th class="center aligned" width="15%">Saldo Akhir</th>
            </tr>
        </thead>
        <tbody>
            @if($record->count() != 0)
                <?php 
                    $i           = 1; 
                    $saldo_awal = [];
                    $saldo_akhir = [];
                ?>
                @foreach ($record as $data)
                    <tr>
                        <td class="center aligned">{{ $i }}</td>
                        <td class="left aligned">{{ $data->kode }} - {{ $data->nama }}</td>
                        <td class="right aligned">
                            {{ rupiah($saldo_tanggal_awal[$data->kode]) }}
                        </td>
                        <td class="right aligned">
                            <?php 
                                $jumlah_debit = $data->jurnal->where('posisi', 'D')->sum('jumlah');
                            ?>
                            {{ rupiah($jumlah_debit) }}
                        </td>
                        <td class="right aligned">
                            <?php 
                                $jumlah_kredit = $data->jurnal->where('posisi', 'K')->sum('jumlah');
                            ?>
                            {{ rupiah($jumlah_kredit) }}
                        </td>
                        <td class="right aligned">
                            <?php 
                                $posisi_normal = $data->posisi;

                                if ($posisi_normal == 'D') {
                                    $bersih = $jumlah_debit - $jumlah_kredit;
                                }else{
                                    $bersih = $jumlah_kredit - $jumlah_debit;                                        
                                }

                            ?>
                            {{ rupiah($bersih) }}
                        </td>
                        <td class="right aligned">
                            {{ rupiah($saldo_tanggal_awal[$data->kode] + $bersih) }}
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            @else
                <tr>
                    <td colspan="7">
                        <center><i>Maaf tidak ada data</i></center>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.ns_date_from').calendar({
            type: 'date',
            endCalendar: $('.ns_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.ns_date_to').calendar({
            type: 'date',
            startCalendar: $('.ns_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        @if(isset($record))
            @if($record->count() != 0)
                $('#reportTable').DataTable({
                    paging: false,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                    scrollX: false,
                    scrollY: 400,
                })
            @endif
        @endif
    });
</script>