@if(isset($record))
    @if($record->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-buku-besar','bukti-jurnal-umum');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-akun-buku-besar','bukti-jurnal-umum');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif


<div class="title" style="font-size: 17px;">Jurnal Umum</div><br>
<div class="field">
    <div class="ui calendar labeled input ju_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input ju_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('bukti-jurnal-umum','-akun-buku-besar');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
@if(isset($record))
    <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="10%">#</th>
                <th class="center aligned" width="10%">Tanggal</th>
                <th class="center aligned" width="10%">No Akun</th>
                <th class="center aligned" width="20%">Nama Akun</th>
                <th class="center aligned" width="15%">Debit</th>
                <th class="center aligned" width="15%">Kredit</th>
                <th class="center aligned" width="20%">ID Transaksi</th>
            </tr>
        </thead>
        <tbody>
            @if($record->count() != 0)
                <?php $i = 1; ?>
                @foreach ($record as $data)
                    <tr>
                        <td class="center aligned">{{ $i }}</td>
                        <td class="center aligned">{{ $data->date }}</td>
                        <td class="center aligned">{{ $data->coa_kode }}</td>
                        <td class="left aligned">{{ $data->coa->nama }}</td>
                        <td class="right aligned">{{ $data->posisi=='D'? rupiah($data->jml):'-' }}</td>
                        <td class="right aligned">{{ $data->posisi=='K'? rupiah($data->jml):'-' }}</td>
                        <td class="center aligned">{{ $data->idtrans }}</td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            @else
                <tr>
                    <td colspan="7">
                        <center><i>Maaf tidak ada data</i></center>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.ju_date_from').calendar({
            type: 'date',
            endCalendar: $('.ju_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.ju_date_to').calendar({
            type: 'date',
            startCalendar: $('.ju_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        @if(isset($record))
            @if($record->count() != 0)
                $('#reportTable').DataTable({
                    paging: false,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                    scrollX: false,
                    scrollY: 400,
                })
            @endif
        @endif
    });
</script>