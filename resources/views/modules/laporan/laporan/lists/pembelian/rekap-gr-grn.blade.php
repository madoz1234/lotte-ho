@if(isset($detail))
    @if($detail->count() != 0)
        <div class="ui form" style="float:right">
            <a href="#" class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-pembelian','rekap-gr-grn');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-pembelian','rekap-gr-grn');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif


<div class="title" style="font-size: 17px;">Rekap GR (GRN)</div><br>

<div class="field">
    <div class="row">
        {{-- <input name="filter[tmuk_kode]" id="tmuk_kode" type="hidden" value="{{ $tmuk_kode }}"> --}}
        <input name="filter[status]" id="status" type="hidden" value="2">

        <div class="ui calendar labeled input pm_date_start" >
            <div class="ui input left icon">
                <i class="calendar icon date"></i>
                <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
            </div>
        </div>
        &nbsp;s.d.&nbsp;
        <div class="ui calendar labeled input pm_date_end">
            <div class="ui input left icon">
                <i class="calendar icon date"></i>
                <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
            </div>
        </div>

        <button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="filter('rekap-gr-grn','-pembelian');">
            <i class="search icon"></i>
        </button>
        {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
            <i class="refresh icon"></i>
        </button> --}}
    </div>
</div>
<br>

@if(isset($detail))
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="20%">#</th>
            <th class="center aligned" width="20%">LSI</th>
            <th class="center aligned" width="20%">TMUK</th>
            <th class="center aligned" width="20%">Tanggal GR</th>
            <th class="center aligned" width="20%">Nomor GR</th>
        </tr>
    </thead>
    <tbody>
        @if($detail->count() > 0)
            <?php $i = 1; ?>
            @foreach ($detail as $row)
                <tr>
                    <td class="center aligned">{{ $i }}</td>
                    <td>{{ $row->tmuk->lsi->nama }}</td>
                    <td>{{ $row->tmuk->nama }}</td>
                    <td class="center aligned">{{ $row->tgl_gr }}</td>
                    <td class="center aligned">{{ $row->no_gr }}</td>
                </tr>
                <?php $i++; ?>
            @endforeach
        @else
        <tr>
            <td colspan="5">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif


<script type="text/javascript">
    $('.pm_date_start').calendar({
        type: 'date',
        endCalendar: $('.pm_date_end'),
        formatter: {
         date: function (date, settings)
            {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day+'/'+month+'/'+year;
             }
         }
    })

    $('.pm_date_end').calendar({
        type: 'date',
        startCalendar: $('.pm_date_start'),
        formatter: {
         date: function (date, settings)
            {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day+'/'+month+'/'+year;
             }
         }
    })
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif
    });
</script>