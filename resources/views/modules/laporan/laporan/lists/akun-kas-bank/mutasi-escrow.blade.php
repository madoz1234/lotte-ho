@if(isset($detail))
    @if($detail->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-kas-bank','mutasi-escrow');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-akun-kas-bank','mutasi-escrow');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif


<div class="title" style="font-size: 17px;">Mutasi Escrow</div><br>
<div class="field">
    <div class="ui calendar labeled input kasbank_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input kasbank_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('mutasi-escrow','-akun-kas-bank');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
@if(isset($detail))
    @if($detail->count() != 0)
    <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="10%">Tanggal</th>
                <th class="center aligned" width="30%">Keterangan</th>
                <th class="center aligned" width="20%">Debet</th>
                <th class="center aligned" width="20%">Credit</th>
                <th class="center aligned" width="20%">Ledger</th>
            </tr>
        </thead>
        <tbody>

            <?php 
                $kredit=0;
                $debit=0;
                $awal=$saldo;
                $i=0;
                $tanggal=[];
                $deskripsi=[];
                $debet=[];
                $credit=[];
                $ledger=[];
            ?>
            @foreach ($detail as $row)
                    <?php
                        array_push($tanggal,\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->tanggal)->format('d F Y'));
                        if($row->reduce){
                            array_push($deskripsi,$row->reduce->po_nomor);
                        }else{
                            $strings = '';
                            if($row->pyr){
                                if($row->pyr->vendor_lokal == 0){
                                    $tmuk = Lotte\Models\Master\Tmuk::with('lsi')->where('kode', $row->pyr->tmuk->kode)->first();
                                    $strings = ' - '.$tmuk->lsi->nama;
                                }else if($row->pyr->vendor_lokal == 100){
                                    $strings = ' - '.$row->pyr->tmuk->nama;
                                }else{
                                    $vendor = Lotte\Models\Master\VendorLokalTmuk::where('id', $row->pyr->vendor_lokal)->first();
                                    $strings = ' - '.isset($vendor) ? $vendor->nama : '-';
                                }
                            }
                            $desk = $row->deskripsi.' '.$strings;
                            array_push($deskripsi,$desk);
                        }
                        array_push($debet,$row->posisi=='D' ? rupiah($row->jumlah) : '');
                        array_push($credit,$row->posisi=='K' ? rupiah($row->jumlah) : '');

                        if($row->posisi=='D'){
                            $debit+=$row->jumlah;
                        }else{
                            $kredit+=$row->jumlah;
                        }

                        if($row->coa->posisi=='D'){
                                $saldo = ($debit - $kredit) + $awal;
                           
                        }else{
                                $saldo = ($kredit - $debit) + $awal;
                        }
                        $i++;
                        array_push($ledger,rupiah($saldo));
                    ?>
            @endforeach
            @if(count($tanggal)>0)
            <?php
                krsort($tanggal);
                krsort($deskripsi);
                krsort($debet);
                krsort($credit);
                krsort($ledger);
            ?>
                @foreach ($tanggal as $key => $row)
                    <tr>
                        <td class="center aligned">{{ $tanggal[$key] }}</td>
                        <td>
                            {{ $deskripsi[$key] }}
                        </td>
                        <td class="right aligned">{{ $debet[$key] }}</td>
                        <td class="right aligned">{{ $credit[$key] }}</td>
                        <td class="right aligned">{{ $ledger[$key] }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>
        {{-- <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th class="right aligned"><b>{{ rupiah($debit) }}</b></th>
                <th class="right aligned"><b>{{ rupiah($kredit) }}</b></th>
                <th></th>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th class="right aligned"><b>Initial Balance</b></th>
                <th class="right aligned"><b>{{ rupiah($saldo) }}</b></th>
            </tr>
        </tfoot> --}}
    </table>
    <div class="ui grid">
        <div class="six wide column">
        </div>
        <div class="ten wide column">
            <table class="ui celled compact red table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="center aligned">OPENING BALANCE</th>
                        <th class="center aligned">TOTAL DEBET</th>
                        <th class="center aligned">TOTAL CREDIT</th>
                        <th class="center aligned">CLOSING BALANCE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="right aligned"><b>{{ rupiah($awal) }}</td>
                        <td class="right aligned"><b>{{ rupiah($debit) }}</b></td>
                        <td class="right aligned"><b>{{ rupiah($kredit) }}</b></td>
                        <td class="right aligned"><b>{{ rupiah($saldo) }}</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    @else
        <div class="ui segment">
          <p align="center">Maaf Tidak Ada Data</p>
        </div>
    @endif
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.kasbank_date_from').calendar({
            type: 'date',
            endCalendar: $('.kasbank_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.kasbank_date_to').calendar({
            type: 'date',
            startCalendar: $('.kasbank_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: false,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                    scrollX: false,
                    scrollY: 400,
                })
            @endif
        @endif
    });
</script>