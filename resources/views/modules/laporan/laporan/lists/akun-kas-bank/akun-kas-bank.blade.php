@if(isset($detail))
    @if($detail->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-kas-bank','akun-kas-bank');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-akun-kas-bank','akun-kas-bank');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif


<div class="title" style="font-size: 17px;">Jurnal Kas & Bank</div><br>
<div class="field">
    <div class="ui calendar labeled input kasbank_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input kasbank_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('akun-kas-bank','-akun-kas-bank');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
@if(isset($detail))
    <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="5%">#</th>
                <th class="center aligned" width="10%">Tanggal</th>
                <th class="center aligned" width="20%">Nama Akun</th>
                <th class="center aligned" width="10%">Saldo Awal</th>
                <th class="center aligned" width="10%">Perubahan <br>Debit</th>
                <th class="center aligned" width="10%">Perubahan <br>Kredit</th>
                <th class="center aligned" width="10%">Perubahan <br>Bersih</th>
                <th class="center aligned" width="10%">Saldo Akhir</th>
                <th class="center aligned" width="15%">Keterangan</th>
            </tr>
        </thead>
        <tbody>
            @if($detail->count() != 0)
                <?php 
                    $i           = 1; 
                    $saldo_awal = [];
                    $saldo_akhir = [];
                    $nama = [];
                ?>
                @foreach ($detail as $row)
                <tr>
                    <td class="center aligned">{{ $i }}</td>
                    <td class="center aligned">{{ \Carbon\Carbon::parse($row->tanggal)->format('Y-m-d') }}</td>
                    <?php 
                        $nama[$i] = $row->coa->nama;
                    ?>
                    <td>{{ $row->coa_kode }} - {{ $row->coa->nama }}</td>
                    <td class="right aligned">
                        <?php 
                            $saldo_awal[$i] = 0;
                            if(isset($nama[$i - 1]) && $nama[$i - 1] == $nama[$i])
                            {
                                if(isset($saldo_akhir[$i - 1]))
                                {
                                    $saldo_awal[$i] = $saldo_akhir[$i - 1];
                                }
                            }
                        ?>
                        {{ rupiah($saldo_awal[$i]) }}
                    </td>
                    <td class="right aligned">
                        <?php 
                            $jumlah_debit = $row->posisi=='D' ? $row->jumlah : '0';
                        ?>
                        {{ rupiah($jumlah_debit) }}
                    </td>
                    <td class="right aligned">
                        <?php 
                            $jumlah_kredit = $row->posisi=='K' ? $row->jumlah : '0';
                        ?>
                        {{ rupiah($jumlah_kredit) }}
                    </td>
                    <td class="right aligned">
                        <?php 
                            $posisi_normal = $row->coa->posisi;

                            if ($posisi_normal == 'D') {
                                $bersih = $jumlah_debit - $jumlah_kredit;
                            }else{
                                $bersih = $jumlah_kredit - $jumlah_debit;                                        
                            }

                        ?>
                        {{ rupiah($bersih) }}
                    </td>
                    <td class="right aligned">
                        <?php 
                            $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                        ?>
                        {{ rupiah($saldo_akhir[$i]) }}
                    </td>
                    <td class="">{{ $row->deskripsi }}</td>
                </tr>
                <?php $i++; ?>
                @endforeach
            @else
            <tr>
                <td colspan="9">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.kasbank_date_from').calendar({
            type: 'date',
            endCalendar: $('.kasbank_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.kasbank_date_to').calendar({
            type: 'date',
            startCalendar: $('.kasbank_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: false,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                    scrollX: false,
                    scrollY: 400,
                })
            @endif
        @endif
    });
</script>