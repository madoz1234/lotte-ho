@if(isset($detail))
    <div class="ui form" style="float:right">
       <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-kas-bank','arus-kas-per-akun');">
            Print
            <i class="print icon"></i>
        </a>
        <div class="ui green export buttons" style="margin-right: 5px">
            <div class="ui button" onclick="printLaporan('exel-laporan-akun-kas-bank','arus-kas-per-akun');"><i class="file excel outline icon"></i>Export</div>
        </div>
    </div>
@endif


<div class="title" style="font-size: 17px;">Daftar Arus Kas per Akun </div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('arus-kas-per-akun','-akun-kas-bank');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
@if(isset($detail))
    <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned">#</th>
                <th class="center aligned">Nama Akun</th>
                <th class="center aligned">Saldo</th>
            </tr>
        </thead>
        <tbody>
            @if($detail->count() != 0)
                <?php $i = 1; ?>
                @foreach ($detail as $row)
                    <tr>
                        <td class="center aligned">{{ $i }}</td>
                        <td class="left aligned">{{ $row->kode }} - {{ $row->nama }}</td>
                        <td class="right aligned">{{ rupiah($row->jurnal->sum('jumlah')) }}</td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            @else
                <tr>
                    <td colspan="3">
                        <center><i>Maaf tidak ada data</i></center>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.ui.calendar').calendar({
            type: 'date',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        });
    });
</script>