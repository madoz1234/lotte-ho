
<div class="title" style="font-size: 17px;">Laporan Kinerja TMUK</div><br>
<div class="field">
        <div class="ui labeled input" id="from">
            <div class="ui input left icon">
                <i class="calendar icon date"></i>
                <input name="filter[fiscal_mulai]" type="text" placeholder="Tanggal Mulai">
            </div>
        </div>
    &nbsp;s.d.&nbsp;
        <div class="ui labeled input" id="from">
            <div class="ui input left icon">
                <i class="calendar icon date"></i>
                <input name="filter[fiscal_selesai]" type="text" placeholder="Tanggal Selesai">
            </div>
        </div>
            <button type="button" class="ui teal icon filter button" data-content="Cari Data">
        <i class="search icon"></i>
    </button>
    <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button>
</div>

<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">LSI</th>
            <th class="center aligned" width="40px">TMUK</th>
            <th class="center aligned" width="40px">Periode</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="center aligned">1</td>
            <td class="left aligned">Lotte Pasar Rebo</td>
            <td class="left aligned">Pragma Informatika</td>
            <td class="center aligned">Nov-17</td>
        </tr>
        <tr>
            <td class="center aligned">2</td>
            <td class="left aligned">Lotte Pasar Minggu</td>
            <td class="left aligned">Jernih</td>
            <td class="center aligned">Des-17</td>
        </tr>
        <tr>
            <td class="center aligned">3</td>
            <td class="left aligned">Lotte Pasar Senen</td>
            <td class="left aligned">Etokokue</td>
            <td class="center aligned">Jan-18</td>
        </tr>
    </tbody>
</table>