@if(isset($detail))
    @if($detail->count() != 0)
        <div class="ui form" style="float:right">
            <a class="ui default right labeled icon save button" target="_blank" onclick="printLaporan('print-laporan-penjualan','penjualan-per-barang');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-penjualan','penjualan-per-barang');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Rekap Penjualan per Barang</div><br>
<div class="field">
    <div class="ui calendar labeled input ppb_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input ppb_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('penjualan-per-barang','-penjualan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
@if(isset($detail))
    <table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="40px">#</th>
                <th class="center aligned">Tanggal</th>
                <th class="center aligned">Kode Produk</th>
                <th class="center aligned">Nama Produk</th>
                <th class="center aligned">Qty</th>
                <th class="center aligned">Harga Jual (Rp)</th>
                <th class="center aligned">Penjualan (Rp)</th>
                <th class="center aligned">Hpp (Rp)</th>
                <th class="center aligned">Profit (Rp)</th>
                <th class="center aligned">Profit Margin %</th>
            </tr>
        </thead>
        <tbody>
            @if($detail->count() > 0)
            <?php $i = 1; ?>
            @foreach ($detail as $row)
            <tr>
                <td class="center aligned">{{ $i }}</td>
                <td class="center aligned">{{ $row->tanggal }}</td>
                <td class="center aligned">{{ $row->produk }}</td>
                <td class="center aligned">{{ $row->nama }}</td>
                <td class="center aligned">{{ $row->qty }}</td>
                <td class="center aligned">{{ number_format($row->harga) }}</td>
                <td class="center aligned">{{ number_format($row->total) }}</td>
                <td class="center aligned">{{ number_format($row->hpp) }}</td>
                <?php $profit    = number_format(round($row->hpp));
                      $penjualan = $row->total;
                      $hasilprofit = $row->total - $row->hpp; 
                ?>
                <td class="center aligned">{{ number_format($hasilprofit) }}</td>
                <?php 
                      $penjualan = $row->total;
                      $profitmargin = $hasilprofit / $penjualan; 
                ?>
                <td class="center aligned">{{ round($profitmargin,2) }}</td>
            </tr>
            <?php $i++; ?>
            @endforeach
            @else
            <tr>
                <td colspan="11">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif

        $('.ppb_date_from').calendar({
            type: 'date',
            maxDate: today,
            endCalendar: $('.ppb_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.ppb_date_to').calendar({
            type: 'date',
            maxDate: today,
            startCalendar: $('.ppb_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })
    });
    $(document).on('click', '.reset.button', function(e){
        $('#mulai_filter').val('');
        $('#selesai_filter').val('');
        $('.filter.button').trigger('click');
    });
</script>