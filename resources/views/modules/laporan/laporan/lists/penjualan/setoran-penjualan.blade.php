@if(isset($detail))
    @if($detail->count() != 0)
    <div class="ui form" style="float:right">
     <a class="ui default right labeled icon save button" target="_blank" onclick="printLaporan('print-laporan-penjualan','setoran-penjualan');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-penjualan','setoran-penjualan');"><i class="file excel outline icon"></i>Export</div>
    </div>
    </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Setoran Penjualan</div><br>
<div class="field">
    <div class="ui calendar labeled input ph_date_start" id="start">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input ph_date_end" id="end">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('setoran-penjualan','-penjualan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>

@if(isset($detail))
<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">Tanggal</th>
            <th class="center aligned" width="40px">Penjualan</th>
            <th class="center aligned" width="40px">Setoran</th>
        </tr>
    </thead>
    <tbody>
        @if($detail->count() > 0)
        <?php $i = 1; ?>
        <?php $total_penjualan = 0 ?>
        <?php $total_setoran = 0 ?>
        @foreach ($detail->sortBy('date') as $row)
        <tr>
            <td class="center aligned">{{ $i }}</td>
            <td class="center aligned">{{ $row->date }}</td>
            <td class="right aligned">{{ rupiah($row->total_penjualan) }}</td>
             <td class="right aligned">
             	<?php $setoran=0; ?>
                @foreach($saldo as $tn)
	                @if($tn->date==$row->date)
                       <?php $setoran=$setoran+($tn->total); ?>
	                @endif
                @endforeach
                {{ rupiah($setoran) }}	
            </td>
        </tr>
        <?php $i++; ?>
        <?php $total_penjualan +=$row->total_penjualan; ?>
        <?php $total_setoran +=$setoran; ?>
        @endforeach
        <tfoot>
            <tr>
                <th colspan="2" class="right aligned" width="40px">Grand Total (Rp)</th>
                <th class="right aligned" width="40px">{{ rupiah($total_penjualan) }}</th>
                <th class="right aligned" width="40px">{{ rupiah($total_setoran) }}</th>
            </tr>
        </tfoot>
        @else
        <tr>
            <td colspan="11">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    var nowDate = new Date();
    var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0);

    $('.ph_date_start').calendar({
        type: 'date',
        maxDate: today,
        endCalendar: $('.ph_date_end'),
        formatter: {
         date: function (date, settings) {
             if (!date) return '';
             var day = date.getDate();
             var month = date.getMonth() + 1;
             var year = date.getFullYear();
             return year+'-'+month+'-'+day;
         }
     }
    })

    $('.ph_date_end').calendar({
        type: 'date',
        maxDate: today,
        startCalendar: $('.ph_date_start'),
        formatter: {
         date: function (date, settings) {
             if (!date) return '';
             var day = date.getDate();
             var month = date.getMonth() + 1;
             var year = date.getFullYear();
             return year+'-'+month+'-'+day;
         }
     }
    })
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif
        // $('.ui.calendar').calendar({
        //     type: 'date',
        //     formatter: {
        //         date: function (date, settings) {
        //             if (!date) return '';
        //             var day = date.getDate();
        //             var month = date.getMonth() + 1;
        //             var year = date.getFullYear();
        //             return year+'-'+month+'-'+day;
        //         }
        //     }
        // });
    });
    // $(document).on('click', '.reset.button', function(e){
    //     $('#mulai_filter').val('');
    //     $('#selesai_filter').val('');
    //     $('.filter.button').trigger('click');
    // });
</script>
