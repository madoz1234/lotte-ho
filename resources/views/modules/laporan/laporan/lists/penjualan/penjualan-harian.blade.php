@if(isset($detail))
    @if($detail->count() != 0)
    <div class="ui form" style="float:right">
     <a class="ui default right labeled icon save button" target="_blank" onclick="printLaporan('print-laporan-penjualan','penjualan-harian');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-penjualan','penjualan-harian');"><i class="file excel outline icon"></i>Export</div>
    </div>
    </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Rekap Penjualan Harian</div><br>
<div class="field">
    <div class="ui calendar labeled input ph_date_start" id="start">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input ph_date_end" id="end">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('penjualan-harian','-penjualan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>

@if(isset($detail))
<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">Tanggal</th>
            <th class="center aligned" width="40px">Total Penjualan (Rp)</th>
            <th class="center aligned" width="40px">Penjualan Tunai (Rp)</th>
            <th class="center aligned" width="40px">Piutang (Rp)</th>
            <th class="center aligned" width="40px">Poin (Rp)</th>
            <th class="center aligned" width="40px">Diskon (Rp)</th>
            <th class="center aligned" width="40px">Voucher (Rp)</th>
        </tr>
    </thead>
    <tbody>
        @if($detail->count() > 0)
        <?php $i = 1; ?>
        <?php $all_penjualan = 0 ?>
        <?php $all_tunai     = 0 ?>
        <?php $all_piutang   = 0 ?>
        <?php $all_points    = 0 ?>
        <?php $all_diskon    = 0 ?>
        <?php $all_voucher   = 0 ?>
        @foreach ($detail->sortBy('tanggal') as $row)

        <tr>
            <td class="center aligned">{{ $i }}</td>
            <td class="center aligned">{{ $row->tanggal }}</td>
            <td class="right aligned">{{ rupiah($row->total_penjualan) }}</td>
            <td class="right aligned">{{ rupiah($row->tunai) }}</td>
            <td class="right aligned">{{ rupiah($row->piutang) }}</td>
            <td class="right aligned">{{ rupiah($row->points) }}</td>
            <td class="right aligned">{{ rupiah($row->diskon) }}</td>
            <td class="right aligned">{{ rupiah($row->voucher) }}</td>
            
        </tr>
        <?php $i++; ?>
        <?php $all_penjualan += $row->total_penjualan; ?>
        <?php $all_tunai     += $row->tunai; ?>
        <?php $all_piutang   += $row->piutang; ?>
        <?php $all_points    += $row->points; ?>
        <?php $all_diskon    += $row->diskon; ?>
        <?php $all_voucher   += $row->voucher; ?>
        @endforeach  
    <tfoot>
        <tr>
            <th colspan="2" class="right aligned" width="40px">Grand Total (Rp)</th>
            <th class="right aligned" width="40px">{{ rupiah($all_penjualan) }}</th>
            <th class="right aligned" width="40px">{{ rupiah($all_tunai) }}</th>
            <th class="right aligned" width="40px">{{ rupiah($all_piutang) }}</th>
            <th class="right aligned" width="40px">{{ rupiah($all_points) }}</th>
            <th class="right aligned" width="40px">{{ rupiah($all_diskon) }}</th>
            <th class="right aligned" width="40px">{{ rupiah($all_voucher) }}</th>
        </tr>
    </tfoot>
        @else
        <tr>
            <td colspan="11">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">

    $('.ph_date_start').calendar({
        type: 'date',
        endCalendar: $('.ph_date_end'),
        formatter: {
         date: function (date, settings) {
            if (!date) return '';
            let day = date.getDate() + '', month = (date.getMonth() + 1) + '', year = date.getFullYear();
            if (day.length < 2) day = '0' + day;
            if (month.length < 2) month = '0' + month;
            return year + '-' + month + '-' + day;
         }
     }
    })

    $('.ph_date_end').calendar({
        type: 'date',
        startCalendar: $('.ph_date_start'),
        formatter: {
         date: function (date, settings) {
             if (!date) return '';
             let day = date.getDate() + '', month = (date.getMonth() + 1) + '', year = date.getFullYear();
             if (day.length < 2) day = '0' + day;
             if (month.length < 2) month = '0' + month;
             return year + '-' + month + '-' + day;
         }
     }
    })
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif
    });
    // $(document).on('click', '.reset.button', function(e){
    //     $('#mulai_filter').val('');
    //     $('#selesai_filter').val('');
    //     $('.filter.button').trigger('click');
    // });
</script>
