<div class="title" style="font-size: 17px;">Rekap Pergerakan Persediaan</div><br>
<div class="field">
    <div class="ui calendar labeled input pnilai_date_start">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" placeholder="Tanggal Mulai" value="{{ $start or '' }}">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input pnilai_date_end">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" placeholder="Tanggal Selesai" value="{{ $end or '' }}">
        </div>
    </div>
    &nbsp;&nbsp;
    <div class="ui labeled input" style="width: 30%;">
    	<select name="filter[produk]" class="ui search dropdown produk produk_kode" id="produk">
    		{{-- <option value="0">-- Pilih Produk --</option> --}}
            {!! \Lotte\Models\Master\ProdukTmuk::options(function($query)
                { return $query->produk->kode.' - '.$query->produk->nama; }, 'produk_kode', 
                [
                    'filters' => ['tmuk_kode' => isset($kode_tmuk) ? $kode_tmuk : '' ], 
                    'selected' => isset($kode_produk) ? $kode_produk : '',
                ], '-- Pilih Produk --') !!}
    	</select>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('pergerakan-persediaan','-persediaan');">
        <i class="search icon"></i>
    </button>
</div>
<br>
@if(isset($hitung))
	<table id="aa" width="40%">
        <tbody>
			<tr>
				<td class="center aligned"><b>Nama TMUK</b></td>
				<td class="center aligned"><b>:</b> {{ $tmuk[0]['nama'] }}</td>
			</tr>
			<tr>
				<td class="center aligned"><b>Kategori</b></td>
				<td class="center aligned"><b>:</b> {{ $tmuk[0]['kategori'] }}</td>
			</tr>
			<tr>
				<td class="center aligned"><b>Kode Produk</b></td>
				<td class="center aligned"><b>:</b> {{ $tmuk[0]['kode'] }}</td>
			</tr>
			<tr>
				<td class="center aligned"><b>Nama Produk</b></td>
				<td class="center aligned"><b>:</b> {{ $tmuk[0]['nama_produk'] }}</td>
			</tr>
			<tr>
				<td class="center aligned"><b>Tanggal</b></td>
				<td class="center aligned"><b>:</b> {{ $tmuk[0]['awal'] }} &nbsp;&nbsp;-&nbsp;&nbsp; {{ $tmuk[0]['akhir'] }}</td>
			</tr>
        </tbody>
    </table>
	@if($hitung != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-persediaan','pergerakan-persediaan');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-persediaan','pergerakan-persediaan');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
    <br>
    <br>
    <br>
    <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
        <thead>

            <tr>
                <th class="center aligned" width="40px">Date</th>
                <th class="center aligned" width="40px">Receiving</th>
                <th class="center aligned" width="40px">Sales</th>
                <th class="center aligned" width="40px">Returning</th>
                <th class="center aligned" width="40px">Adjustment</th>
                <th class="center aligned" width="40px">Stock Opname</th>
                <th class="center aligned" width="40px">Payment Request</th>
                <th class="center aligned" width="40px">Balance Qty</th>
                <th class="center aligned" width="40px">HPP</th>
                <th class="center aligned" width="40px">Balance Amount</th>
            </tr>
        </thead>
        <tbody>
        	@if($hitung !=0)
	        	<tr>
	        		<td class="center aligned" bgcolor="#FFFF00"><b>Begining Stock</b></td>
	        		<td class="center aligned" bgcolor="#FFFF00"></td>
	        		<td class="center aligned" bgcolor="#FFFF00"></td>
	        		<td class="center aligned" bgcolor="#FFFF00"></td>
	        		<td class="center aligned" bgcolor="#FFFF00"></td>
	        		<td class="center aligned" bgcolor="#FFFF00"></td>
	        		<td class="center aligned" bgcolor="#FFFF00"></td>
	        		<td class="center aligned" bgcolor="#FFFF00">{{ $begining[0]['qty'] }}</td>
	        		<td class="center aligned" bgcolor="#FFFF00">{{ number_format($begining[0]['hpp']) }}</td>
	        		<td class="center aligned" bgcolor="#FFFF00">{{ number_format($begining[0]['nilai']) }}</td>
	        	</tr>
	        	@for($i=0;$i<$hitung;$i++)
	        	<tr>
	        		<td class="center aligned">{{$hasil[$i]['date']}}</td>
	        		<td class="center aligned">{{$hasil[$i]['gr']}}</td>
	        		<td class="center aligned">{{$hasil[$i]['jual']}}</td>
	        		<td class="center aligned">{{$hasil[$i]['rr']}}</td>
	        		<td class="center aligned">{{$hasil[$i]['ad']}}</td>
	        		<td class="center aligned">{{$hasil[$i]['so']}}</td>
	        		<td class="center aligned">{{$hasil[$i]['pyr']}}</td>
	        		<td class="center aligned">{{$hasil[$i]['qty']}}</td>
	        		<td class="center aligned">{{number_format($hasil[$i]['hpp'])}}</td>
	        		<td class="center aligned">{{number_format($hasil[$i]['nilai'])}}</td>
	        	</tr>
	        	@endfor
        	@else
                <tr>
                    <td colspan="8">
                        <center><i>Maaf tidak ada data</i></center>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
	var nowDate = new Date();
    var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);
    $('.pnilai_date_start').calendar({
        type: 'date',
        maxDate: today,
        endCalendar: $('.pnilai_date_end'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return day+'/'+month+'/'+year;
             }
        }
    })

    $('.pnilai_date_end').calendar({
        type: 'date',
        maxDate: today,
        startCalendar: $('.pnilai_date_start'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return day+'/'+month+'/'+year;
             }
        }
    })
    $(document).ready(function() {
        @if(isset($hitung))
            @if($hitung != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: true,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif

        $('.produk_kode').select2()

    });
</script>