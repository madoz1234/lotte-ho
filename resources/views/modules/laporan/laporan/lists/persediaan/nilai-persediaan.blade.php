@if(isset($data))
    @if($data->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-persediaan','nilai-persediaan');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-persediaan','nilai-persediaan');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Rekap Nilai Persediaan</div><br>
<div class="field">
    <div class="ui calendar labeled input">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="filter('nilai-persediaan','-persediaan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian" id="reset_nilai_persediaan">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
@if(isset($data))
    <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="20px">No</th>
                <th class="center aligned" width="40px">Category 1</th>
                <th class="center aligned" width="40px">Category 2</th>
                <th class="center aligned" width="40px">Kode Produk</th>
                <th class="center aligned" width="60px">Nama Produk</th>
                <th class="center aligned" width="40px">UOM</th>
                <th class="center aligned" width="40px">Qty</th>
                <th class="center aligned" width="40px">Cost Price (MAP) (Rp)</th>
                <th class="center aligned" width="40px">Jumlah (Rp)</th>
            </tr>
        </thead>
        <tbody>
        	<?php $grand=0;$tot_qty=0;$tot_hpp=0;?>
            @if($data->count() != 0)
                <?php $i = 1;?>
                @foreach ($data as $row)
                <tr>
                    <td class="center aligned">{{ $i }}</td>
                    <td class="center aligned">{{ isset($row->produk->l1_nm) ? $row->produk->l1_nm : '-' }}</td>
                    <td class="center aligned">{{ isset($row->produk->l2_nm) ? $row->produk->l2_nm : '-' }}</td>
                    <td class="center aligned">{{ $row->produk->kode }}</td>
                    <td class="left aligned">{{ $row->produk->nama }}</td>
                    <td class="right aligned">{{ isset($row->produksetting->uom1_satuan) ? $row->produksetting->uom1_satuan : '-' }}</td>
                    <td class="right aligned">
	                    <?php
	                        $qty = isset($row->detail) ? $row->detail->qty : 0;
	                    ?>
                        {{ number_format($qty) }}
                    </td>
                    <td class="right aligned">
                        <?php
                            $map = isset($row->detail) ? $row->detail->hpp : 0;
                        ?>
                        {{ number_format($map) }}
                    </td>
                    <td class="right aligned">{{ number_format(isset($row->detail) ? $row->detail->nilai_persediaan : 0) }}</td>
                </tr>
                <?php $i++; 
                $tot_qty += $qty;
                $tot_hpp += $map;
                $grand += isset($row->detail) ? $row->detail->nilai_persediaan : 0;
                ?>
                @endforeach
            @else
                <tr>
                    <td colspan="9">
                        <center><i>Maaf tidak ada data</i></center>
                    </td>
                </tr>
            @endif
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5" class="ui center aligned"></th>
                <th colspan="1" class="left aligned">Total</th>
                <th colspan="1" class="right aligned">{{number_format($tot_qty)}}</th>
                <th colspan="1" class="right aligned">{{number_format($tot_hpp)}}</th>
                <th colspan="1" class="right aligned">{{number_format($grand)}}</th>
            </tr>
        </tfoot>
    </table>
@else
<div class="ui segment">
	<p align="center">Filter tanggal terlebih dahulu</p>
</div>
@endif

<script type="text/javascript">
    $(document).on('click', '#reset_nilai_persediaan', function(e){
        $('#mulai_filter').val('');
        $('#selesai_filter').val('');

        filter('nilai-persediaan','-persediaan');
    });

    var nowDate = new Date();
    var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

    $(document).ready(function() {

        @if(isset($data))
            @if($data->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: true,
                    lengthChange: false,
                    ordering:true,
                });
            @endif
        @endif

        $('.ui.calendar').calendar({
            type: 'date',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        });
    });
</script>