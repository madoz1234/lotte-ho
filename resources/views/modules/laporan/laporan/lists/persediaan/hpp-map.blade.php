@if(isset($data))
    @if($data->count() != 0)
    <div class="ui form" style="float:right">
       <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-persediaan','hpp-map');">
            Print
            <i class="print icon"></i>
        </a>
        <div class="ui green export buttons" style="margin-right: 5px">
            <div class="ui button" onclick="printLaporan('exel-laporan-persediaan','hpp-map');"><i class="file excel outline icon"></i>Export</div>
        </div>
    </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Rekap HPP (MAP)</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal">
        </div>
    </div>
    {{-- &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div> --}}
    <button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="filter('hpp-map','-persediaan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>
<?php $grand=0;?>
@if(isset($data))
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="10%">No</th>
            <th class="center aligned" width="20%">Kode Produk</th>
            <th class="center aligned" width="40%">Nama Produk</th>
            <th class="center aligned" width="15%">UOM</th>
            <th class="center aligned" width="15%">HPP (Rp)</th>
        </tr>
    </thead>
    <tbody>
        @if($data->count() != 0)
        <?php $i = 1; ?>
            @foreach ($data as $row)
            <tr>
                <td class="center aligned">{{ $i }}</td>
                <td class="center aligned">{{ $row->produk->kode }}</td>
                <td class="left aligned">{{ $row->produk->nama }}</td>
                <td class="right aligned">{{ isset($row->produksetting->uom1_satuan) ? $row->produksetting->uom1_satuan : '-' }}</td>
                <td class="right aligned">{{ number_format(isset($row->detail) ? $row->detail->hpp : 0) }}</td>
            </tr>
            <?php $i++; 
            $grand += isset($row->detail) ? $row->detail->hpp : 0;
            ?>
            @endforeach
        @else
            <tr>
                <td colspan="5">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
        @endif
    </tbody>
    <tfoot>
    	<tr>
    		<th colspan="3" class="ui center aligned"></th>
    		<th colspan="1" class="center aligned">Grand Total (Rp)</th>
    		<th colspan="1" class="right aligned">{{number_format($grand)}}</th>
    	</tr>
    </tfoot>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        var nowDate = new Date();
	    var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);
	    
        @if(isset($data))
            @if($data->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: true,
                    lengthChange: false,
                    ordering:true,
                });
            @endif
        @endif

        
        $('.ui.calendar').calendar({
            type: 'date',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        });
    });
</script>