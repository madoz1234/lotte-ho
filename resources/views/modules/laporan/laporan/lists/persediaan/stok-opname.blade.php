@if(isset($data))
    @if($data->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-persediaan','stok-opname');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-persediaan','stok-opname');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Rekap Stok Opname</div><br>
<div class="field">
    <div class="ui calendar labeled input so_date_start">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;
    <div class="ui labeled input">
    	<?php 
    		if(isset($tipe)){
    			$type = $tipe;
    		}
    		?>
			<select name="filter[tipe]" class="ui compact search selection dropdown">
				<option value="">Type SO</option>
				<option <?php if ($type == 'MONTH' ) echo 'selected' ; ?> value="MONTH">MONTH</option>
				<option <?php if ($type == 'DAY' ) echo 'selected' ; ?> value="DAY">DAY</option>
			</select>
		</div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('stok-opname','-persediaan');">
        <i class="search icon"></i>
    </button>
</div>
<br>

@if(isset($data))
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            	<th class="center aligned" width="20px">No</th>
                <th class="center aligned" width="40px">Category</th>
                <th class="center aligned" width="40px">Divisi</th>
                <th class="center aligned" width="40px">Kode Produk</th>
                <th class="center aligned" width="60px">Nama Produk</th>
                <th class="center aligned" width="40px">Barcode</th>
                <th class="center aligned" width="40px">HPP</th>
                <th class="center aligned" width="40px">Jumlah Fisik</th>
                <th class="center aligned" width="40px">Jumlah System</th>
                <th class="center aligned" width="40px">Variance Qty</th>
                <th class="center aligned" width="40px">Variance Value</th>
        </tr>
    </thead>
    <tbody>
        <?php $total=0;$hpp=0;$fisik=0;$system=0;$var_qty=0;?>
        @if($data->count() != 0)
            @foreach ($data as $row)
            <?php $i = 1; $selisih_stok=0;?>
	            @foreach ($row->detail as $val)
	            <?php
				if($val->qty_system > $val->qty_barcode){
				$selisih_stok = ($val->qty_system - $val->qty_barcode) * -1;
				}elseif($val->qty_system < 0 AND $val->qty_system < $val->qty_barcode){
				$selisih_stok = (($val->qty_system * -1) + $val->qty_barcode);
				}elseif($val->qty_system >= 0 AND $val->qty_system < $val->qty_barcode){
				$selisih_stok = $val->qty_barcode - $val->qty_system;
				}
				$variance_val = ($val->qty_barcode * $val->hpp);
	            $hpp += $val->hpp; 
	            $fisik += $val->qty_barcode; 
	            $system += $val->qty_system; 
	            $var_qty += $selisih_stok; 
	            $total += $variance_val; 
				?>
	            <tr>
	                <td class="center aligned">{{ $i }}</td>
	                <td class="left aligned">{{ isset($val->produk->l1_nm) ? $val->produk->l1_nm : '-' }}</td>
	                <td class="left aligned">{{ isset($val->produk->l2_nm) ? $val->produk->l2_nm : '-' }}</td>
	                <td class="left aligned">{{ $val->produk->kode }}</td>
	                <td class="left aligned">{{ $val->produk->nama }}</td>
	                <td class="left aligned">{{ isset($val->produksetting) ? $val->produksetting->first()->uom1_barcode : '-' }}</td>
	                <td class="right aligned">{{ number_format($val->hpp) }}</td>
	                <td class="right aligned">{{ $val->qty_barcode }}</td>
	                <td class="right aligned">{{ $val->qty_system }}</td>
	                <td class="right aligned">{{ $selisih_stok }}</td>
	                <td class="right aligned">{{ number_format($variance_val) }}</td>
	            </tr>
	            <?php $i++;
	            $selisih_stok = 0;
	            ?>
	            @endforeach
            @endforeach
        @else
            <tr>
                <td colspan="11">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
        @endif
    </tbody>
    <tfoot>
            <tr>
                <th colspan="5" class="ui center aligned"></th>
                <th colspan="1">Total</th>
                <th colspan="1" class="right aligned">{{number_format($hpp)}}</th>
                <th colspan="1" class="right aligned">{{number_format($fisik)}}</th>
                <th colspan="1" class="right aligned">{{number_format($system)}}</th>
                <th colspan="1" class="right aligned">{{number_format($var_qty)}}</th>
                <th colspan="1" class="right aligned">{{number_format($total)}}</th>
            </tr>
        </tfoot>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
	var nowDate = new Date();
    var today   = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1, 0, 0, 0);

    $('.so_date_start').calendar({
        type: 'date',
        maxDate: today,
        endCalendar: $('.so_date_end'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return day+'/'+month+'/'+year;
             }
        }
    })

    $('.so_date_end').calendar({
        type: 'date',
        maxDate: today,
        startCalendar: $('.so_date_start'),
        formatter: {
             date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return day+'/'+month+'/'+year;
             }
        }
    })
    $(document).ready(function() {
        @if(isset($data))
            @if($data->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: true,
                    lengthChange: false,
                    ordering:true,
                });
            @endif
        @endif
    });
</script>