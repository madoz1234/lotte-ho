<div class="ui form" style="float:right">
   <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-persediaan','umur-persediaan');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-persediaan','umur-persediaan');"><i class="file excel outline icon"></i>Export</div>
    </div>
</div>


<div class="title" style="font-size: 17px;">Rekap Umur Persediaan</div><br>
<!-- <div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('umur-persediaan','-persediaan');">
        <i class="search icon"></i>
    </button>
    <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button>
</div> -->
<div class="field">
    <div class="ui checked checkbox">
        <input type="checkbox" name="uom1_order_cek" value="1" disabled="" checked="1">
        <label><?php echo \Carbon\Carbon::yesterday()->toDateString(); ?></label>
    </div>
</div>
<br>
@if(isset($detail))
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">No</th>
            <th class="center aligned" width="40px">Kode Produk</th>
            <th class="center aligned" width="40px">Deskripsi Produk</th>
            <th class="center aligned" width="40px">Total Qty</th>
            <th class="center aligned" width="40px">Qty (1-30 hari)</th>
            <th class="center aligned" width="40px">Qty (31-60 hari)</th>
            <th class="center aligned" width="40px">Qty (61-90 hari)</th>
            <th class="center aligned" width="40px">Qty (91-120 hari)</th>
            <th class="center aligned" width="40px">Qty >120 hari</th>
        </tr>
    </thead>
    <tbody>
        @if($detail->count() != 0)
        @foreach ($detail as $key => $row)
        <tr>
            <td class="center aligned">{{ $key + 1 }}</td>
            <td class="center aligned">{{ $row->kode }}</td>
            <td class="left aligned">{{ $row->nama }}</td>
            <td class="center aligned">{{ $row->getTotalStock() }}</td>
            <td class="right aligned">{{ number_format($row->getStockByDay(1)) }}</td>
            <td class="right aligned">{{ number_format($row->getStockByDay(31)) }}</td>
            <td class="right aligned">{{ number_format($row->getStockByDay(61)) }}</td>
            <td class="right aligned">{{ number_format($row->getStockByDay(91)) }}</td>
            <td class="right aligned">{{ number_format($row->getStockByDay(120)) }}</td>
        </tr>
        @endforeach
        @else
            <tr>
                <td colspan="9">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
        @endif
    </tbody>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: true,
                    lengthChange: false,
                    ordering:true,
                });
            @endif
        @endif
        
        $('.ui.calendar').calendar({
            type: 'date',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return year+'-'+month+'-'+day;
                }
            }
        });
    });
</script>