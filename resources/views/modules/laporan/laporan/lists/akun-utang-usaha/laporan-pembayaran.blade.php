@if(isset($record))
    @if($record->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-utang-usaha','laporan-pembayaran');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-akun-utang-usaha','laporan-pembayaran');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif


<div class="title" style="font-size: 17px;">Laporan Pembayaran</div><br>
<div class="field">
    <div class="ui calendar labeled input kasbank_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input kasbank_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('laporan-pembayaran','-akun-utang-usaha');">
        <i class="search icon"></i>
    </button>
</div>
<br>
@if(isset($record))
    <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="left aligned" width="20%">Tipe Transaksi</th>
                <th class="center aligned" width="10%">#</th>
                <th class="left aligned" width="50%">Jatuh Tempo</th>
                <th class="right aligned" width="20%">Total</th>
            </tr>
        </thead>
        <tbody>
            @if($record->count() != 0)
                <?php
                    $i=0;
                    $vendor='';
                    $total=0;
                ?>
                @foreach($record as $row)
                    <?php
                        if($i==0){
                            $vendor = $row->vendor_lokal;
                        }else{
                            if($row->vendor_lokal!=$vendor){
                                echo '
                                <tr>
                                    <td colspan="3" class="left aligned" style="border-bottom:2px solid black;border-top:1px solid black;">Total</td>
                                    <td style="display: none;"></td>
                                     <td style="display: none;"></td>
                                    <td class="right aligned" style="border-bottom:2px solid black;border-top:1px solid black;">'.rupiah($total).'</td>
                                </tr>
                                ';
                                $i=0;
                                $total=0;
                                $vendor = $row->vendor_lokal;
                            }
                        }
                    ?>
                    @if($i==0)
                        <tr>
                            <td colspan="3" class="left aligned" >Lokal Vendor {{ $row->vendors($row->id) }} - Cash Only</td>
                             <td style="display: none;"></td>
                             <td style="display: none;"></td>
                            <td class="right aligned" >IDR</td>
                        </tr>
                        <tr>
                            <td class="left aligned">Supplier Invoice</td>
                            <td class="center aligned">{{ $row->urutan($row->id) }}</td>
                            <td class="left aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                            <td class="right aligned">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
                        </tr>
                    @else
                        <tr>
                            <td class="left aligned">Supplier Invoice</td>
                            <td class="center aligned">{{ $row->urutan($row->id) }}</td>
                            <td class="left aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                            <td class="right aligned">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
                        </tr>
                    @endif
                    <?php 
                        $i++;
                        $total+=isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0;
                    ?>
                @endforeach
                <tr>
                    <td colspan="3" class="left aligned" style="border-bottom:2px solid black;border-top:1px solid black;">Total</td>
                    <td style="display: none;"></td>
                     <td style="display: none;"></td>
                    <td class="right aligned" style="border-bottom:2px solid black;border-top:1px solid black;">{{ rupiah($total) }}</td>
                </tr>
            @else
            <tr>
                <td colspan="4">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.kasbank_date_from').calendar({
            type: 'date',
            endCalendar: $('.kasbank_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.kasbank_date_to').calendar({
            type: 'date',
            startCalendar: $('.kasbank_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        @if(isset($record))
            @if($record->count() != 0)
                $('#reportTable').DataTable({
                    paging: false,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                    scrollX: false,
                    scrollY: 400,
                })
            @endif
        @endif
    });
</script>