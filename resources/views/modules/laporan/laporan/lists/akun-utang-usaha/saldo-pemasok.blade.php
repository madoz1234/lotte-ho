@if(isset($record))
    @if($record->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-utang-usaha','saldo-pemasok');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-akun-utang-usaha','saldo-pemasok');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif

{{-- {{ dd($record) }} --}}

<div class="title" style="font-size: 17px;">Saldo Pemasok</div><br>
<div class="field">
    <div class="ui calendar labeled input kasbank_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input kasbank_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    &nbsp;&nbsp;
    <div class="ui labeled input" style="width: 300px;">
        <select name="filter[vendor]" class="ui search selection dropdown nama_akun">
            {!! \Lotte\Models\Master\VendorLokalTmuk::options(function($query)
                { return $query->kode.' - '.$query->nama; }, 'id', [], '-- Pilih Vendor Lokal --')
             !!}
        </select>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('saldo-pemasok','-akun-utang-usaha');">
        <i class="search icon"></i>
    </button>
</div>
<br>
@if(isset($record))
    <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="20%">Trans Type</th>
                <th class="center aligned" width="10%">#</th>
                <th class="center aligned" width="15%">Date</th>
                <th class="center aligned" width="15%">Due Date</th>
                <th class="center aligned" width="10%">Charges</th>
                <th class="center aligned" width="10%">Credits</th>
                <th class="center aligned" width="10%">Allocated</th>
                <th class="center aligned" width="10%">Outstanding</th>
            </tr>
        </thead>
        <tbody>
            @if($record->count() != 0)
                <?php
                    $i=0;
                    $vendor='';
                    $total=0;
                ?>
                @foreach($record as $row)
                    <?php
                        if($i==0){
                            $vendor = $row->vendor_lokal;
                        }else{
                            if($row->vendor_lokal!=$vendor){
                                
                                $i=0;
                                $total=0;
                                $vendor = $row->vendor_lokal;
                            }
                        }
                    ?>
                    @if($i==0)
                        <tr>
                            <td colspan="2" class="left aligned" >Lokal Vendor
                                @if($row->vendor_lokal == 100)
                                    {{$row->tmuk->nama}}
                                @elseif($row->vendor_lokal == 0)
                                    {{$row->tmuk->lsi->nama}} 
                                @else
                                    {{$row->vendorlokal->nama}}
                                @endif
                            </td>
                            <td style="display: none;"></td>
                            <td class="left aligned" >IDR</td>
                            <td class="left aligned" >Open Balance</td>
                            <td class="right aligned" >{{ rupiah(0) }}</td>
                            <td class="right aligned" >{{ rupiah(0) }}</td>
                            <td class="right aligned" >{{ rupiah(0) }}</td>
                            <td class="right aligned" >{{ rupiah(0) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned">{{ $row->verifikasi4_user == NULL ? 'Supplier Invoice': 'Pembayaran Ke Pemasok' }}</td>
                            <td class="center aligned">
                                @if($row->vendor_lokal == 100)
                                    {{$row->tmuk->kode}}
                                @elseif($row->vendor_lokal == 0)
                                    {{$row->tmuk->lsi->kode}} 
                                @else
                                    {{$row->vendorlokal->kode}}
                                @endif
                            </td>
                            <td class="left aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->tanggal)->format('d/m/Y') }}</td>
                            <td class="left aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->tanggal)->format('d/m/Y') }}</td>
                            <td class="right aligned">
                                @if($row->verifikasi4_user == NULL)
                                    {{ $row->posisi == 'K' ? '0': rupiah($row->charges_jurnal) }}
                                @else
                                    {{ rupiah(0) }}
                                @endif
                            </td>
                            <td class="right aligned" >
                                @if($row->verifikasi4_user == NULL)
                                    {{ rupiah(0) }}
                                @else
                                    {{ rupiah($row->charges_jurnal) }}
                                @endif
                            </td>
                            <td class="right aligned" >
                                {{ $row->verifikasi4_user == NULL ? '0': rupiah($row->charges_jurnal) }}
                            </td>
                            <td class="right aligned" >
                                {{ $row->verifikasi4_user == NULL ? rupiah($row->charges_jurnal) : '0' }}
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td class="left aligned">{{ $row->verifikasi4_user == NULL ? 'Supplier Invoice': 'Pembayaran Ke Pemasok' }}</td>
                            <td class="center aligned">
                                @if($row->vendor_lokal == 100)
                                    {{$row->tmuk->kode}}
                                @elseif($row->vendor_lokal == 0)
                                    {{$row->tmuk->lsi->kode}} 
                                @else
                                    {{$row->vendorlokal->kode}}
                                @endif
                            </td>
                            <td class="left aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->tanggal)->format('d/m/Y') }}</td>
                            <td class="left aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->tanggal)->format('d/m/Y') }}</td>
                            <td class="right aligned">
                                @if($row->verifikasi4_user == NULL)
                                    {{ $row->posisi == 'K' ? '0': rupiah($row->charges_jurnal) }}
                                @else
                                    {{ rupiah(0) }}
                                @endif
                            </td>
                            <td class="right aligned" >
                                @if($row->verifikasi4_user == NULL)
                                    {{ rupiah(0) }}
                                @else
                                    {{ rupiah($row->charges_jurnal) }}
                                @endif
                            </td>
                            <td class="right aligned" >
                                {{ $row->verifikasi4_user == NULL ? '0': rupiah($row->charges_jurnal) }}
                            </td>
                            <td class="right aligned" >
                                {{ $row->verifikasi4_user == NULL ? rupiah($row->charges_jurnal) : '0' }}
                            </td>
                        </tr>
                    @endif
                    <?php 
                        $i++;
                    ?>
                @endforeach   
            @else
            <tr>
                <td colspan="8">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.kasbank_date_from').calendar({
            type: 'date',
            endCalendar: $('.kasbank_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.kasbank_date_to').calendar({
            type: 'date',
            startCalendar: $('.kasbank_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        @if(isset($record))
            @if($record->count() != 0)
                $('#reportTable').DataTable({
                    paging: false,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                    scrollX: false,
                    scrollY: 400,
                })
            @endif
        @endif
    });
</script>