@if(isset($record))
    @if($record->count() != 0)
        <div class="ui form" style="float:right">
           <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-utang-usaha','umur-pemasok');">
                Print
                <i class="print icon"></i>
            </a>
            <div class="ui green export buttons" style="margin-right: 5px">
                <div class="ui button" onclick="printLaporan('exel-laporan-akun-utang-usaha','umur-pemasok');"><i class="file excel outline icon"></i>Export</div>
            </div>
        </div>
    @endif
@endif


<div class="title" style="font-size: 17px;">Analysis Umur Utang Usaha</div><br>
<div class="field">
    <div class="ui calendar labeled input kasbank_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input kasbank_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('umur-pemasok','-akun-utang-usaha');">
        <i class="search icon"></i>
    </button>
</div>
<br>
@if(isset($record))
    <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="20%">Supplier</th>
                <th class="center aligned" width="20%">Currency</th>
                <th class="center aligned" width="15%">1-30 Days</th>
                <th class="center aligned" width="15%">31-60 Days</th>
                <th class="center aligned" width="15%">Over 60 Days</th>
                <th class="center aligned" width="15%">Total Balance</th>
            </tr>
        </thead>
        <tbody>
            @if($record->count() != 0)
                <?php
                    $i=0;
                    $vendor='';
                ?>
                @foreach($record as $row)
                    <?php
                        if($i==0){
                            $vendor = $row->vendor_lokal;
                        }else{
                            if($row->vendor_lokal!=$vendor){
                                $i=0;
                                $vendor = $row->vendor_lokal;
                            }
                        }

                        $st  = date_create(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->created_at)->format('Y-m-d'));
                        $en    = date_create();
                        $diff   = date_diff( $st, $en );
                        $day = $diff->days;
                    ?>
                    @if($i==0)
                        <tr>
                            <td class="left aligned" style="border-bottom: 2px solid black;">Lokal Vendor {{ $row->vendors($row->id) }}</td>
                            <td class="center aligned" style="border-bottom: 2px solid black;" >IDR</td>
                            <td class="right aligned" style="border-bottom: 2px solid black;" >{{ rupiah(0) }}</td>
                            <td class="right aligned" style="border-bottom: 2px solid black;" >{{ rupiah(0) }}</td>
                            <td class="right aligned" style="border-bottom: 2px solid black;" >{{ rupiah(0) }}</td>
                            <td class="right aligned" style="border-bottom: 2px solid black;" >{{ rupiah(0) }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned">Supplier Invoice</td>
                            <td class="center aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                            <td class="right aligned" >
                                @if($day <= 30)
                                    {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                                @else
                                    {{ rupiah(0) }}
                                @endif
                            </td>
                            <td class="right aligned" >
                                @if($day > 30 && $day <= 60)
                                    {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                                @else
                                    {{ rupiah(0) }}
                                @endif
                            </td>
                            <td class="right aligned" >
                                @if($day > 60)
                                    {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                                @else
                                    {{ rupiah(0) }}
                                @endif
                            </td>
                            <td class="right aligned">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
                        </tr>
                    @else
                        <tr>
                            <td class="left aligned">Supplier Invoice</td>
                            <td class="center aligned">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl_jatuh_tempo)->format('d/m/Y') }}</td>
                            <td class="right aligned" >
                                @if($day <= 30)
                                    {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                                @else
                                    {{ rupiah(0) }}
                                @endif
                            </td>
                            <td class="right aligned" >
                                @if($day > 30 && $day <= 60)
                                    {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                                @else
                                    {{ rupiah(0) }}
                                @endif
                            </td>
                            <td class="right aligned" >
                                @if($day > 60)
                                    {{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}
                                @else
                                    {{ rupiah(0) }}
                                @endif
                            </td>
                            <td class="right aligned">{{ rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) }}</td>
                        </tr>
                    @endif
                    <?php 
                        $i++;
                    ?>
                @endforeach
            @else
            <tr>
                <td colspan="7">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('.kasbank_date_from').calendar({
            type: 'date',
            endCalendar: $('.kasbank_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        $('.kasbank_date_to').calendar({
            type: 'date',
            startCalendar: $('.kasbank_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        })

        @if(isset($record))
            @if($record->count() != 0)
                $('#reportTable').DataTable({
                    paging: false,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                    scrollX: false,
                    scrollY: 400,
                })
            @endif
        @endif
    });
</script>