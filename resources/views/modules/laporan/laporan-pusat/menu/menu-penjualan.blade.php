<div class="ui treeview accordion" style="margin-bottom: 50px">
	<div class="title active">
	<i class="edit icon"></i>
	<b>LAPORAN PENJUALAN</b> 
	</div>
	<div class="content active">
	<div class="ui accordion">
		<div class="title active" onclick="showTable('penjualan-harian', '-penjualan')" style="font-weight: bold;">
			<i class="circle outline icon"></i>
			Rekap Penjualan Harian
		</div>
		<div class="content">
		</div>
		<div class="title" onclick="showTable('penjualan-mingguan', '-penjualan')">
			<i class="circle outline icon"></i>
			Rekap Penjualan Mingguan
		</div>
		<div class="content">
		</div>
		<div class="title" onclick="showTable('penjualan-bulanan', '-penjualan')">
			<i class="circle outline icon"></i>
			Rekap Penjualan Bulanan
		</div>
		<div class="content">
		</div>
		<div class="title" onclick="showTable('penjualan-per-barang', '-penjualan')">
			<i class="circle outline icon"></i>
			Rekap Penjualan Perbarang
		</div>
		<div class="content">
		</div>
		<div class="title" onclick="showTable('setoran-penjualan-pusat', '-penjualan')">
			<i class="circle outline icon"></i>
			Setoran Penjualan
		</div>
		<div class="content">
		</div>
	</div>
	</div>
</div>
