<div class="ui treeview accordion" style="margin-bottom: 50px">
	<div class="title active">
		<i class="edit icon"></i>
		<b>AKTIVA TETAP</b> 
	</div>
	<div class="content active">
		<div class="ui accordion">
			<div class="title active" onclick="showTable('aktiva-tetap', '-aktiva-tetap')" style="font-weight: bold;">
				<i class="circle outline icon"></i>
				Daftar Aktiva Tetap
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('aktiva-tetap-per-tipe', '-aktiva-tetap')">
				<i class="circle outline icon"></i>
				Rekap Aktiva Tetap per Tipe
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('asset-listing', '-aktiva-tetap')">
				<i class="circle outline icon"></i>
				Asset Listing
			</div>
			<div class="content">
			</div>
		</div>
	</div>
</div>