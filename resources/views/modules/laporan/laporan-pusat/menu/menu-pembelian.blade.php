<div class="ui treeview accordion" style="margin-bottom: 50px">
	<div class="title active">
		<i class="edit icon"></i>
		<b>LAPORAN PEMBELIAN</b> 
	</div>
	<div class="content active">
		<div class="ui accordion">
			<div class="title active" onclick="showTable('rekap-pembelian-persediaan', '-pembelian')" style="font-weight: bold;">
				<i class="circle outline icon"></i>
				Rekap Pembelian Persediaan
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('laporan-pembelian-persediaan', '-pembelian')">
				<i class="circle outline icon"></i>
				Laporan Pembelian Persediaan
			</div>

			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-po-btdk', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Purchase Order (PO BTDK)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-po-stdk', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Purchase Order (PO STDK)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('po-barang-trade-ke-lsi', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap PO Barang Trade ke LSI
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('po-barang-non-trade-ke-lsi', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap PO Barang Non-Trade ke LSI
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('po-barang-trade-ke-vendor-lokal', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap PO Barang Trade ke Vendor Lokal
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-picking-result-pt', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Picking Result (PT)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-label-kontainer-cl', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Label Kontainer (CL)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-daftar-muatan-it', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Daftar Muatan (LT)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-surat-jalan-dn', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Surat Jalan (DN)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('rekap-delivery', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap Delivery
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('rekap-gr-grn', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap GR (GRN)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-konfirmasi-retur-rr', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Konfirmasi Retur (RR)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('rekap-service-level-sl', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap Service Level (SL)
			</div>
			<div class="content">
			</div>
		</div>
	</div>
</div>