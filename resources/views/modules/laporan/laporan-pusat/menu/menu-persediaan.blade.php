<div class="ui treeview accordion" style="margin-bottom: 50px">
	<div class="title">
		<i class="edit icon"></i>
		<b>PERSEDIAAN</b> 
	</div>
	<div class="content active">
		<div class="ui accordion active">
			<div class="title active" onclick="showTable('nilai-persediaan', '-persediaan', 'priode')" style="font-weight: bold;">
				<i class="circle outline icon"></i>
				Rekap Nilai Persediaan
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('pergerakan-persediaan', '-persediaan', 'priode')">
				<i class="circle outline icon"></i>
				Rekap Pergerakan Persediaan
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('stok-opname', '-persediaan', 'priode')">
				<i class="circle outline icon"></i>
				Rekap Stok Opname
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('hpp-map', '-persediaan', 'priode')">
				<i class="circle outline icon"></i>
				Rekap HPP (MAP)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('umur-persediaan', '-persediaan', 'priode')">
				<i class="circle outline icon"></i>
				Rekap Umur Persediaan
			</div>
			<div class="content">
			</div>
		</div>
	</div>
</div>