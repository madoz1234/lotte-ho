<div class="ui treeview accordion" style="margin-bottom: 50px">
	<div class="title">
		<i class="edit icon"></i>
		<b>LAPORAN KEUANGAN</b> 
	</div>
	<div class="content active">
		<div class="ui accordion">
			<div class="title active" onclick="showTable('neraca', '-laporan-keuangan', 'print-excel')" style="font-weight: bold;">
				<i class="circle outline icon"></i>
				Neraca
			</div>
			<div class="content">
			</div>

			<div class="title" onclick="showTable('laba-rugi', '-laporan-keuangan', 'print-excel')">
				<i class="circle outline icon"></i>
				Laba/Rugi
			</div>
			<div class="content">
			</div>

			<div class="title" onclick="showTable('laporan-laba-ditahan', '-laporan-keuangan', 'print-excel')">
				<i class="circle outline icon"></i>
				Laporan Laba Ditahan
			</div>
			<div class="content">
			</div>

			{{-- <div class="title" onclick="showTable('laporan-piutang', '-laporan-keuangan', 'print-excel')">
				<i class="circle outline icon"></i>
				Laporan Piutang TMUK
			</div>
			<div class="content">
			</div> --}}

			<div class="title" onclick="showTable('fokus-keuangan', '-laporan-keuangan', 'print-excel')">
				<i class="circle outline icon"></i>
				Laporan Performance TMUK
			</div>
			<div class="content">
			</div>

			<div class="title" onclick="showTable('realisasi-rab', '-laporan-keuangan', 'print-excel')">
				<i class="circle outline icon"></i>
				Laporan Realisasi RAB
			</div>
			<div class="content">
			</div>

			<div class="title" onclick="showTable('pengeluaran-tahun', '-laporan-keuangan', 'print-excel')">
				<i class="circle outline icon"></i>
				Laporan Pengeluaran Tahunan
			</div>
			<div class="content">
			</div>
		</div>
	</div>
</div>
