<div class="ui treeview accordion" style="margin-bottom: 50px">
	<div class="title">
		<i class="edit icon"></i>
		<b>AKUN UTANG USAHA</b> 
	</div>
	<div class="content active">
		<div class="ui accordion">
			<div class="title" onclick="showTable('laporan-pembayaran','-akun-utang-usaha')">
				<i class="circle outline icon"></i>
				Laporan Pembayaran
			</div>
			<div class="content">
			</div>

			<div class="title" onclick="showTable('saldo-pemasok','-akun-utang-usaha')">
				<i class="circle outline icon"></i>
				Saldo Pemasok
			</div>
			<div class="content">
			</div>

			<div class="title" onclick="showTable('analysis-umur-pemasok','-akun-utang-usaha')">
				<i class="circle outline icon"></i>
				Analysis Umur Utang Usaha
			</div>
			<div class="content">
			</div>

		</div>
	</div>
</div>
