
<div class="ui treeview accordion" style="margin-bottom: 50px">
	{{-- <style type="text/css">
		.active { font-weight: bold; }
	</style> --}}
	<div class="title">
		<i class="dropdown icon"></i>
		LAPORAN KEUANGAN 
	</div>
	<div class="content">
		<div class="ui accordion">
			<div class="title" onclick="showTable('neraca', '-laporan-keuangan')">
				<i class="circle outline icon"></i>
				Neraca
			</div>
			<div class="content">
			</div>
			{{-- <div class="title" onclick="showTable('neraca-perbandingan-anggaran', '-laporan-keuangan')">
				<i class="circle outline icon"></i>
				Neraca (Perbandingan Anggaran)
			</div>
			<div class="content">
			</div> --}}

			<div class="title" onclick="showTable('laba-rugi', '-laporan-keuangan')">
				<i class="circle outline icon"></i>
				Laba/Rugi
			</div>
			<div class="content">
			</div>

			{{-- <div class="title" onclick="showTable('laba-rugi-perbandingan-anggaran', '-laporan-keuangan')">
				<i class="circle outline icon"></i>
				Laba/Rugi (Perbandingan Anggaran)
			</div>
			<div class="content">
			</div> --}}

			<div class="title" onclick="showTable('laporan-laba-ditahan', '-laporan-keuangan')">
				<i class="circle outline icon"></i>
				Laporan Laba Ditahan
			</div>
			<div class="content">
			</div>

			<div class="title" onclick="showTable('fokus-keuangan', '-laporan-keuangan')">
				<i class="circle outline icon"></i>
				Laporan Performance TMUK
			</div>
			<div class="content">
			</div>


			<div class="title" onclick="showTable('realisasi-rab', '-laporan-keuangan')">
				<i class="circle outline icon"></i>
				Laporan Realisasi RAB
			</div>
			<div class="content">
			</div>

			{{-- <div class="title" onclick="showTable('keu07_asset_')">
				<i class="dropdown icon"></i>
				Buku Besar
			</div>
			<div class="content">
			</div>

			<div class="title" onclick="showTable('keu08_asset_')">
				<i class="dropdown icon"></i>
				Rekap Setoran Penjualan TMUK
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu09_asset_')">
				<i class="dropdown icon"></i>
				Rekap Reduce Escrow Balance TMUK
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu10_asset_')">
				<i class="dropdown icon"></i>
				Rekap Mutasi Escrow TMUK
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu11_asset_')">
				<i class="dropdown icon"></i>
				Rekap Invoice Vendor Lokal TMUK
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu12_asset_')">
				<i class="dropdown icon"></i>
				Rekap Pembayaran TMUK Ke Vendor Lokal
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu13_asset_')">
				<i class="dropdown icon"></i>
				Rekap Piutang Usaha TMUK Ke Vendor Lokal
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu15_asset_')">
				<i class="dropdown icon"></i>
				Daftar KKI
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu16_asset_')">
				<i class="dropdown icon"></i>
				Daftar Akun COA
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu17_asset_')">
				<i class="dropdown icon"></i>
				Daftar Akun Escrow
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu18_asset_')">
				<i class="dropdown icon"></i>
				Daftar Vendor Lokal TMUK
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('keu19_asset_')">
				<i class="dropdown icon"></i>
				Laporan Audit
			</div>
			<div class="content">
			</div> --}}
		</div>
	</div>

	<div class="title">
		<i class="dropdown icon"></i>
		AKUN BUKU BESAR 
	</div>
	<div class="content">
		<div class="ui accordion">
			<div class="title" onclick="showTable('daftar-history-gl', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Jurnal per Akun
			</div>
			<div class="content">
			</div>
			{{-- <div class="title" onclick="showTable('keseluruhan-jurnal', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Keseluruhan Jurnal
			</div>
			<div class="content">
			</div> --}}
			<div class="title" onclick="showTable('bukti-jurnal-umum', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Jurnal Umum
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('ringkasan-buku-besar', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Ringkasan Buku Besar
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('neraca-saldo', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Neraca Saldo
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('daftar-akun', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Daftar COA
			</div>
			<div class="content">
			</div>
		</div>
	</div>

	<div class="title">
		<i class="dropdown icon"></i>
		ARUS KAS & BANK 
	</div>
	<div class="content">
		<div class="ui accordion">
			<div class="title" onclick="showTable('akun-kas-bank', '-akun-kas-bank')">
				<i class="circle outline icon"></i>
				Jurnal Kas & Bank
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('arus-kas-per-akun', '-akun-kas-bank')">
				<i class="circle outline icon"></i>
				Arus Kas per Akun
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('mutasi-escrow', '-akun-kas-bank')">
				<i class="circle outline icon"></i>
				Mutasi Escrow
			</div>
			<div class="content">
			</div>
		</div>
	</div>


	<div class="title">
		<i class="dropdown icon"></i>
		AKUN PIUTANG & MEMBER 
	</div>
	<div class="content">
		<div class="ui accordion">
			<div class="title" onclick="showTable('transaksi-belum-lunas','-akun-piutang-member')">
				<i class="circle outline icon"></i>
				Rekap Piutang Member
			</div>
			<div class="content">
			</div>
			{{-- <div class="title" onclick="showTable('piutang-member','-akun-piutang-member')">
				<i class="circle outline icon"></i>
				Rekap Piutang Member
			</div> --}}
			<div class="content">
			</div>
			<div class="title" onclick="showTable('pembayaran-member','-akun-piutang-member')">
				<i class="circle outline icon"></i>
				Rekap Pembayaran Member
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('daftar-member','-akun-piutang-member')">
				<i class="circle outline icon"></i>
				Daftar Member
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('umur-piutang-member','-akun-piutang-member')">
				<i class="circle outline icon"></i>
				Rekap Umur Piutang Member
			</div>
			<div class="content">
			</div>
		</div>
	</div>

	<div class="title">
		<i class="dropdown icon"></i>
		LAPORAN PENJUALAN 
	</div>
	<div class="content">
		<div class="ui accordion">
			<div class="title" onclick="showTable('penjualan-harian', '-penjualan')">
				<i class="circle outline icon"></i>
				Rekap Penjualan Harian
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('penjualan-mingguan', '-penjualan')">
				<i class="circle outline icon"></i>
				Rekap Penjualan Mingguan
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('penjualan-bulanan', '-penjualan')">
				<i class="circle outline icon"></i>
				Rekap Penjualan Bulanan
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('penjualan-per-barang', '-penjualan')">
				<i class="circle outline icon"></i>
				Rekap Penjualan per Barang
			</div>
			<div class="content">
			</div>
		</div>
	</div>
	<div class="title">
		<i class="dropdown icon"></i>
		LAPORAN PEMBELIAN 
	</div>
	<div class="content">
		<div class="ui accordion">
			<div class="title" onclick="showTable('rekap-pembelian-persediaan', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap Pembelian Persediaan
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-po-btdk', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Purchase Order (PO BTDK)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-po-stdk', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Purchase Order (PO STDK)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('po-barang-trade-ke-lsi', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap PO Barang Trade ke LSI
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('po-barang-non-trade-ke-lsi', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap PO Barang Non-Trade ke LSI
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('po-barang-trade-ke-vendor-lokal', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap PO Barang Trade ke Vendor Lokal
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-picking-result-pt', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Picking Result (PT)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-label-kontainer-cl', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Label Kontainer (CL)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-daftar-muatan-it', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Daftar Muatan (LT)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-surat-jalan-dn', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Surat Jalan (DN)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('rekap-delivery', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap Delivery
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('rekap-gr-grn', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap GR (GRN)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('lembar-konfirmasi-retur-rr', '-pembelian')">
				<i class="circle outline icon"></i>
				Lembar Konfirmasi Retur (RR)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('rekap-service-level-sl', '-pembelian')">
				<i class="circle outline icon"></i>
				Rekap Service Level (SL)
			</div>
			<div class="content">
			</div>
		</div>
	</div>

	<div class="title">
		<i class="dropdown icon"></i>
		AKTIVA TETAP 
	</div>
	<div class="content">
		<div class="ui accordion">
			<div class="title" onclick="showTable('aktiva-tetap', '-aktiva-tetap')">
				<i class="circle outline icon"></i>
				Daftar Aktiva Tetap
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('aktiva-tetap-per-tipe', '-aktiva-tetap')">
				<i class="circle outline icon"></i>
				Rekap Aktiva Tetap per Tipe
			</div>
			<div class="content">
			</div>
		</div>
	</div>

	<div class="title">
		<i class="dropdown icon"></i>
		PERSEDIAAN 
	</div>
	<div class="content">
		<div class="ui accordion">
			<div class="title" onclick="showTable('nilai-persediaan', '-persediaan')">
				<i class="circle outline icon"></i>
				Rekap Nilai Persediaan
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('pergerakan-persediaan', '-persediaan')">
				<i class="circle outline icon"></i>
				Rekap Pergerakan Persediaan
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('stok-opname', '-persediaan')">
				<i class="circle outline icon"></i>
				Rekap Stok Opname
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('hpp-map', '-persediaan')">
				<i class="circle outline icon"></i>
				Rekap HPP (MAP)
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('umur-persediaan', '-persediaan')">
				<i class="circle outline icon"></i>
				Rekap Umur Persediaan
			</div>
			<div class="content">
			</div>
		</div>
	</div>



	
</div>

<script type="text/javascript">
	// function byId(id) {
	// 	return document.getElementById ? document.getElementById(id) : document.all[id];
	// }
	// var prevLink = "";
	// function .changeActiveStates (ele) {
	// 	if (prevLink) byId(prevLink).className = "";
	// 	ele.className = 'active';
	// 	prevLink = ele.id;
	// }
</script>