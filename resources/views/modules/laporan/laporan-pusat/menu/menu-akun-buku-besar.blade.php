<div class="ui treeview accordion" style="margin-bottom: 50px">
	<div class="title">
		<i class="edit icon"></i>
		<b>AKUN BUKU BESAR</b> 
	</div>
	<div class="content active">
		<div class="ui accordion">
			<div class="title active" onclick="showTable('daftar-history-gl', '-akun-buku-besar')" style="font-weight: bold;">
				<i class="circle outline icon"></i>
				Jurnal per Akun
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('bukti-jurnal-umum', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Jurnal Umum
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('ringkasan-buku-besar', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Ringkasan Buku Besar
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('neraca-saldo', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Neraca Saldo
			</div>
			<div class="content">
			</div>
			<div class="title" onclick="showTable('daftar-akun', '-akun-buku-besar')">
				<i class="circle outline icon"></i>
				Daftar COA
			</div>
			<div class="content">
			</div>
		</div>
	</div>
</div>
