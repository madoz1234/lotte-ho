<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },
            {
         
              "title": "Tanggal",
              "data": "tanggal"
            },
            {
         
              "title": "Nama Akun",
              "data": "coa"
            },
            {
         
              "title": "Saldo Awal",
              "data": "saldo_awal"
            },
            {
         
              "title": "Perubahan Debit",
              "data": "jumlah_debit"
            },
            {
         
              "title": "Perubahan Kredit",
              "data": "jumlah_kredit"
            },
            // {
         
            //   "title": "Perubahan Bersih",
            //   "data": "posisi_normal"
            // },
            {
         
              "title": "Perubahan Bersih",
              "data": "bersih"
            },
            {
         
              "title": "Saldo Akhir",
              "data": "saldo_akhir"
            }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Ringkasan Buku Besar</div><br>

    <table id="reportTable" class="ui celled  table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="5%">No</th>
                <th class="center aligned" width="10%">Tanggal</th>
                <th class="center aligned" width="10%">COA</th>
                <th class="center aligned" width="10%">Saldo Awal</th>
                <th class="center aligned" width="10%">Jumlah Debit</th>
                <th class="center aligned" width="10%">Jumlah Kredit</th>
                <th class="center aligned" width="10%">Bersih</th>
                <th class="center aligned" width="10%">Saldo Akhir</th>
            </tr>
        </thead>
        <tbody>
{{--             @/if($record->count() > 0)
                </?php 
                    $i           = 1; 
                    $saldo_awal = [];
                    $saldo_akhir = [];
                ?>
                @/foreach ($record as $data)
                    <tr>
                        <td class="center aligned">{{ $i }}</td>
                        <td class="center aligned">{{ \Carbon\Carbon::parse($data->tanggal)->format('Y-m-d') }}</td>
                        <td class="left aligned">{{ $data->coa_kode }} - {{ $data->coa->nama }}</td>
                        <td class="right aligned">
                            </?php 
                                if ($i == 1) {
                                    $saldo_awal[$i] = 0;
                                }else{
                                    $saldo_awal[$i] = $saldo_akhir[$i-1];
                                }
                            ?>
                            {{ rupiah($saldo_awal[$i]) }}
                        </td>
                        <td class="right aligned">
                            </?php 
                                $jumlah_debit = $data->posisi=='D' ? $data->jumlah : '0';
                            ?>
                            {{ rupiah($jumlah_debit) }}
                        </td>
                        <td class="right aligned">
                            </?php 
                                $jumlah_kredit = $data->posisi=='K' ? $data->jumlah : '0';
                            ?>
                            {{ rupiah($jumlah_kredit) }}
                        </td>
                        <td class="right aligned">
                            </?php 
                                $posisi_normal = $data->coa->posisi;

                                if ($posisi_normal == 'D') {
                                    $bersih = $jumlah_debit - $jumlah_kredit;
                                }else{
                                    $bersih = $jumlah_kredit - $jumlah_debit;                                        
                                }

                            ?>
                            {{ rupiah($bersih) }}
                        </td>
                        <td class="right aligned">
                            </?php 
                                $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                            ?>
                            {{ rupiah($saldo_akhir[$i]) }}
                        </td>
                    </tr>
                    </?php $i++; ?>
                @/endforeach
            @/else
                <tr>
                    <td colspan="8">
                        <center><i>Maaf tidak ada data</i></center>
                    </td>
                </tr>
            @/endif --}}
        </tbody>
    </table>
