<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
                d.coa           = $('[name="filter[coa]"]').val();

            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },
            {
              "title": "Nama Akun",
              "data": "nama_akun"
            },
            {
              "title": "Tanggal",
              "data": "tanggal"
            },
            {
              "title": "ID Transaksi",
              "data": "id_tsransaksi"
            },
            {
              "title": "Saldo Awal",
              "data": "saldo_awal"
            },
            {
              "title": "Perubahan Debit",
              "data": "jumlah_debit"
            },
            {
              "title": "Perubahan Kredit",
              "data": "jumlah_kredit"
            },
            {
              "title": "Perubahan Bersih",
              "data": "bersih"
            },
            {
              "title": "Saldo Akhir",
              "data": "saldo_akhir"
            }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Jurnal per Akun</div><br>
<div class="field">
    <div class="ui labeled input">
        <select name="filter[coa]" class="ui search selection dropdown" style="width: 100%;">
            {!! \Lotte\Models\Master\Coa::options(function($q){
                                return $q->kode.' - '.$q->nama;
                             }, 'kode',['selected' => isset($coa) ? $coa : '', 'filters' => [function($query){ $query->where('status', [1]); }]], '-- Akun COA --') !!}
        </select>
    </div>
    {{-- <button type="button" class="ui teal icon filter button" data-content="Cari Data" onclick="filter('daftar-history-gl','-akun-buku-besar');">
        <i class="search icon"></i>
    </button> --}}
</div>
<br>


<table id="reportTable" class="ui celled table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="6%">#</th>
            <th class="center aligned" width="40%">Nama Akun</th>
            <th class="center aligned" width="40%">Tanggal</th>
            <th class="center aligned" width="40%">ID Transaksi</th>
            <th class="center aligned" width="40%">Saldo Awal</th>
            <th class="center aligned" width="40%">Perubahan Debit</th>
            <th class="center aligned" width="40%">Perubahan Kredit</th>
            <th class="center aligned" width="40%">Perubahan Bersih</th>
            <th class="center aligned" width="40%">Saldo Akhir</th>
        </tr>
    </thead>
    <tbody>

{{--         @/if(count($record) != 0)
            </?php $i = 1; ?>
            @/foreach ($record as $row)
                <tr>
                    <td class="center aligned">{/{ $i }}</td>
                    <td class="center aligned">{/{ $row->kode }}</td>
                    <td class="left aligned">{/{ $row->nama }}</td>
                    <td class="center aligned">{/{ $row->status = 1 ? ' Aktif' : 'Non-Aktif' }}</td>
                </tr>
                </?php $i++; ?>
            @/endforeach
        @/else
            <tr>
                <td colspan="4">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
        @/endif --}}
    </tbody>
</table>