<div class="ui form" style="float:right">
   <a class="ui default right labeled icon save button" onclick="printLaporan('print-laporan-akun-buku-besar','keseluruhan-jurnal');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-akun-buku-besar','keseluruhan-jurnal');"><i class="file excel outline icon"></i>Export</div>
    </div>
</div>


<div class="title" style="font-size: 17px;">Keseluruhan Jurnal</div><br>
<div class="field">
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input" id="from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('keseluruhan-jurnal','-akun-buku-besar');">
        <i class="search icon"></i>
    </button>
    <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button>
</div>

<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">No Akun</th>
            <th class="center aligned" width="40px">Nama Akun</th>
            <th class="center aligned" width="40px">Debit</th>
            <th class="center aligned" width="40px">Kredit</th>
        </tr>
    </thead>
    <tbody>
        @if(count($record) > 0)
        <?php $i = 1; ?>
        @foreach ($record as $row)
        <tr>
            <td class="center aligned">{{ $i }}</td>
            <td class="center aligned">{{ $row->coa_kode }}</td>
            <td class="left aligned">{{ $row->coa->nama }}</td>
            <td class="right aligned">{{ $row->posisi=='D'? rupiah($row->jumlah):'-' }}</td>
            <td class="right aligned">{{ $row->posisi=='K'? rupiah($row->jumlah):'-' }}</td>
        </tr>
        <?php $i++; ?>
        @endforeach
        @else
        <tr>
            <td colspan="11">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
        $('.ui.calendar').calendar({
            type: 'date',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return day+'/'+month+'/'+year;
                }
            }
        });
    });
</script>