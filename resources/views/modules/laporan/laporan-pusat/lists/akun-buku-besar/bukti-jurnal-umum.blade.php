<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },
            {
         
              "title": "Tanggal",
              "data": "date"
            },
            {
              "title": "No Akun",
              "data": "coa_kode"
            },
            {
              "title": "Nama Akun",
              "data": "nama"
            },
            {
              "title": "Debit",
              "data": "debit"
            },
            {
              "title": "Kredit",
              "data": "kredit"
            },
            {
              "title": "ID Transaksi",
              "data": "idtrans"
            }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<table id="reportTable" class="ui celled  table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="6%">#</th>
            <th class="center aligned" width="10%">Tanggal</th>
            <th class="center aligned" width="10%">No Akun</th>
            <th class="center aligned" width="20%">Nama Akun</th>
            <th class="center aligned" width="15%">Debit</th>
            <th class="center aligned" width="15%">Kredit</th>
            <th class="center aligned" width="20%">ID Transaksi</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>