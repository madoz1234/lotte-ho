<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num",
              "className" : "dt-center"
            },
            {
              "title": "LSI",
              "data": "lsi"
            },
            {
         
              "title": "TMUK",
              "data": "tmuk"
            },
            {
              "title": "Tanggal PO",
              "data": "tgl_buat"
            },
            {
              "title": "Nomor PO",
              "data": "nomor"
            },
            {
              "title": "Nilai PO BTDK (Rp)",
              "data": "total",
              "className" : "dt-right"
            }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;"> Lembar Purchase Order (PO STDK) </div><br>
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="5%">#</th>
            <th class="center aligned" width="25%">LSI</th>
            <th class="center aligned" width="20%">TMUK</th>
            <th class="center aligned" width="10%">Tanggal PO</th>
            <th class="center aligned" width="20%">Nomor PO</th>
            <th class="center aligned" width="20%">Nilai PO STDK (Rp)</th>
        </tr>
    </thead>
    <tbody>
        {{-- @/if($detail->count() > 0)
            </?php $i = 1; ?>
            @/foreach ($detail as $row)
                <tr>
                    <td class="center aligned">{/{ $i }}</td>
                    <td>{/{ $row->tmuk->lsi->nama }}</td>
                    <td>{/{ $row->tmuk->nama }}</td>
                    <td class="center aligned">{/{ $row['tgl_buat'] }}</td>
                    <td class="center aligned">{/{ $row['nomor'] }}</td>
                    <td class="right aligned">{/{ rupiah($row['total']) }}</td>
                </tr>
                </?php $i++; ?>
            @/endforeach
        @/else
        <tr>
            <td colspan="6">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @/endif --}}
    </tbody>
</table>