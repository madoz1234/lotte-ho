
<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token  = "{{ csrf_token() }}";
                d.types     = $("[name=types]").val();
                d.status    = "{{ $lists_grid }}";
                d.region_id = $('[name="region_id"]').val();
                d.lsi_id    = $('[name="lsi_id"]').val();
                d.tmuk_kode = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start     = $('#mulai_filter').val();
                d.tanggal_end       = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num",
              "className" : "dt-center"
            },
            {
              "title": "LSI",
              "data": "lsi"
            },
            {
         
              "title": "TMUK",
              "data": "tmuk"
            },
            {
              "title": "Tanggal Penjualan",
              "data": "tanggal",
              "className" : "dt-center"
            },
            {
              "title": "Qty",
              "data": "qty",
              "className" : "dt-center"
            },
            {
              "title": "Penjualan (Rp)",
              "data": "penjualan",
              "className" : "dt-right"
            },
            {
              "title": "Cost Price (Rp)",
              "data": "cost_price",
              "className" : "dt-right"
            },
            {
              "title": "Profit (Rp)",
              "data": "profit",
              "className" : "dt-right"
            },
            {
              "title": "Profit Margin (%)",
              "data": "profit_margin",
              "className" : "dt-right"
            }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 

</script>
<div class="title" style="font-size: 17px;">Rekap Pembelian Persediaan </div><br>
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="5px">#</th>
            <th class="center aligned" width="70px">LSI</th>
            <th class="center aligned" width="20px">TMUK</th>
            <th class="center aligned" width="20px">Tanggal Penjualan</th>
            <th class="center aligned" width="20px">Qty</th>
            <th class="center aligned" width="20px">Penjualan (Rp)</th>
            <th class="center aligned" width="20px">Cost Price (Rp)</th>
            <th class="center aligned" width="40px">Profit (Rp)</th>
            <th class="center aligned" width="40px">Profit Margin (Rp)</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
