{{-- <div class="ui form" style="float:right">
    <div class="fields">
        <button type="button" class="ui blue print button">
        <i class="print icon"></i>
        Cetak
        </button>
        <div class="ui teal buttons" style="margin-right: 5px">
        <div class="ui button">Download</div>
        <div class="ui floating dropdown icon button">
            <i class="dropdown icon"></i>
            <div class="menu">
                <div class="item"><i class="file excel outline icon"></i> Excel</div>
                <div class="item"><i class="file pdf outline icon"></i> PDF</div>
            </div>
        </div>
    </div>
        <button type="button" class="ui blue add button"><i class="plus icon"></i>Tambah Data</button>
    </div>
</div> --}}
<h2 class="ui header">Laporan Rekap Penjualan Perbarang</h2>
<div class="field">
        <div class="ui labeled input" id="from">
            <div class="ui input left icon">
                <i class="calendar icon date"></i>
                <input name="filter[fiscal_mulai]" type="text" placeholder="Tanggal Mulai">
            </div>
        </div>
     
        <div class="ui labeled input" id="from">
            <div class="ui input left icon">
                <i class="calendar icon date"></i>
                <input name="filter[fiscal_selesai]" type="text" placeholder="Tanggal Selesai">
            </div>
        </div>
            <button type="button" class="ui teal icon filter button" data-content="Cari Data">
        <i class="search icon"></i>
    </button>
    <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button>
</div>
<h3 class="ui header" style="margin: 7px 0">
    Seluruh COA
</h3>

<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th>Kode COA</th>
            <th>Nama</th>
            <th>Dibuat Pada</th>
            <th>Dibuat Oleh</th>
            <th class="center aligned" width="150px">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="center aligned">1</td>
            <td>1</td>
            <td>ASSET</td>
            <td>2 jam yang lalu</td>
            <td>Admin</td>
            <td class="center aligned">
                <button type="button" class="ui orange mini icon edit button"><i class="edit icon"></i></button>
                <button type="button" class="ui red mini icon delete button"><i class="delete icon"></i></button>
            </td>
        </tr>
        <tr>
            <td class="center aligned">2</td>
            <td>2</td>
            <td>HUTANG</td>
            <td>2 jam yang lalu</td>
            <td>Admin</td>
            <td class="center aligned">
                <button type="button" class="ui orange mini icon edit button"><i class="edit icon"></i></button>
                <button type="button" class="ui red mini icon delete button"><i class="delete icon"></i></button>
            </td>
        </tr>
        <tr>
            <td class="center aligned">3</td>
            <td>3</td>
            <td>EKUITAS</td>
            <td>2 jam yang lalu</td>
            <td>Admin</td>
            <td class="center aligned">
                <button type="button" class="ui orange mini icon edit button"><i class="edit icon"></i></button>
                <button type="button" class="ui red mini icon delete button"><i class="delete icon"></i></button>
            </td>
        </tr>
        <tr>
            <td class="center aligned">4</td>
            <td>4</td>
            <td>PENDAPATAN</td>
            <td>2 jam yang lalu</td>
            <td>Admin</td>
            <td class="center aligned">
                <button type="button" class="ui orange mini icon edit button"><i class="edit icon"></i></button>
                <button type="button" class="ui red mini icon delete button"><i class="delete icon"></i></button>
            </td>
        </tr>
        <tr>
            <td class="center aligned">5</t0d>
            <td>5</td>
            <td>HPP</td>
            <td>2 jam yang lalu</td>
            <td>Admin</td>
            <td class="center aligned">
                <button type="button" class="ui orange mini icon edit button"><i class="edit icon"></i></button>
                <button type="button" class="ui red mini icon delete button"><i class="delete icon"></i></button>
            </td>
        </tr>
        <tr>
            <td class="center aligned">6</td>
            <td>6</td>
            <td>BIAYA</td>
            <td>2 jam yang lalu</td>
            <td>Admin</td>
            <td class="center aligned">
                <button type="button" class="ui orange mini icon edit button"><i class="edit icon"></i></button>
                <button type="button" class="ui red mini icon delete button"><i class="delete icon"></i></button>
            </td>
        </tr>
        <tr>
            <td class="center aligned">7</td>
            <td>7</td>
            <td>PENDAPATAN DAN BIAYA LAIN-LAIN</td>
            <td>2 jam yang lalu</td>
            <td>Admin</td>
            <td class="center aligned">
                <button type="button" class="ui orange mini icon edit button"><i class="edit icon"></i></button>
                <button type="button" class="ui red mini icon delete button"><i class="delete icon"></i></button>
            </td>
        </tr>
    </tbody>
</table>