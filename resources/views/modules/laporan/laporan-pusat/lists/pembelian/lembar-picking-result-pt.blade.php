<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num",
              "className" : "dt-center"
            },
            {
              "title": "LSI",
              "data": "lsi"
            },
            {
         
              "title": "TMUK",
              "data": "tmuk"
            },
            {
              "title": "Tanggal PyR",
              "data": "tgl_buat",
              "className" : "dt-center"
            },
            {
              "title": "Nomor PR",
              "data": "nomor"
            },
            {
              "title": "Nilai PR (Rp)",
              "data": "total",
              "className" : "dt-right"
            },
            {
              "title": "Tanggal Picking",
              "data": "tgl_buat",
              "className" : "dt-center"
            }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Lembar Picking Result (PT)  </div><br>
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="5%">#</th>
            <th class="center aligned" width="20%">LSI</th>
            <th class="center aligned" width="20%">TMUK</th>
            <th class="center aligned" width="15%">Tanggal PR</th>
            <th class="center aligned" width="20%">Nomor PR</th>
            <th class="center aligned" width="20%">Nilai PyR (Rp)</th>
            <th class="center aligned" width="20%">Tanggal Picking</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>