{{-- <div class="ui form" style="float:right">
    <div class="fields">
        
        <button type="button" class="ui blue print button">
        <i class="print icon"></i>
        Cetak
        </button>
        <div class="ui teal buttons" style="margin-right: 5px">
        <div class="ui button">Download</div>
        <div class="ui floating dropdown icon button">
            <i class="dropdown icon"></i>
            <div class="menu">
                <div class="item"><i class="file excel outline icon"></i> Excel</div>
                <div class="item"><i class="file pdf outline icon"></i> PDF</div>
            </div>
        </div>
    </div>
        <button type="button" class="ui blue add button"><i class="plus icon"></i>Tambah Data</button>
    </div>
</div> --}}
<h2 class="ui header">Laporan Rekap Penjualan Perbarang</h2>
<div class="field">
        <div class="ui labeled input" id="from">
            <div class="ui input left icon">
                <i class="calendar icon date"></i>
                <input name="filter[fiscal_mulai]" type="text" placeholder="Tanggal Mulai">
            </div>
        </div>
     
        <div class="ui labeled input" id="from">
            <div class="ui input left icon">
                <i class="calendar icon date"></i>
                <input name="filter[fiscal_selesai]" type="text" placeholder="Tanggal Selesai">
            </div>
        </div>
            <button type="button" class="ui teal icon filter button" data-content="Cari Data">
        <i class="search icon"></i>
    </button>
    <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button>
</div>
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th>Kode COA 3</th>
            <th>Nama COA 3</th>
            <th class="center aligned" width="150px">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="center aligned">1</td>
            <td><span class="kode_3"></span></td>
            <td>Cash and Bank</td>
            <td class="center aligned">
                <button type="button" class="ui orange mini icon edit button"><i class="edit icon"></i></button>
                <button type="button" class="ui red mini icon delete button"><i class="delete icon"></i></button>
            </td>
        </tr>
    </tbody>
</table>