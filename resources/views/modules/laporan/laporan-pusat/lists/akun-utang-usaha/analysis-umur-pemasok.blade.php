<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },            
            {
              "title": "Tipe Transaksi",
              "data": "deskripsi"
            },
            {
              "title": "Currency",
              "data": "tgl_jatuh_tempo",
              "className" : "dt-center"
            },
            {
              "title": "Current",
              "data": "current",
              "className" : "dt-right"
            },
            {
              "title": "1-30 Days",
              "data": "jumlah_satu",
              "className" : "dt-right"
            },
            {
              "title": "31-60 Days",
              "data": "jumlah_dua",
              "className" : "dt-right"
            },
            {
              "title": "Over 60 Days",
              "data": "jumlah_tiga",
              "className" : "dt-right"
            },
            {
              "title": "Total Balance",
              "data": "balance",
              "className" : "dt-right"
            },
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Analysis Umur Utang Usaha</div><br>
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Kode TMUK</th>
            {{-- <th class="center aligned">Tanggal</th>
            <th class="center aligned">Total Penjualan (Rp)</th>
            <th class="center aligned">Penjualan Tunai (Rp)</th>
            <th class="center aligned">Penjualan Piutang (Rp)</th>
            <th class="center aligned">Penjualan Poin (Rp)</th> --}}
        </tr>
    </thead>
{{--     </?php $i=1; ?>
    @/foreach ($record as $elm)
        <tr>
            <td class="center aligned" width="40px">{/{ $i }}</td>
            <td class="center aligned">{/{ $elm->tmuk_kode }}</td>
            <td class="center aligned">{/{ $elm->tanggal }}</td>
            <td class="center aligned">{/{ $elm->total_penjualan }}</td>
            <td class="center aligned">{/{ $elm->tunai }}</td>
            <td class="center aligned">{/{ $elm->piutang }}</td>
            <td class="center aligned">{/{ $elm->points }}</td>
        </tr>
        </?php $i++; ?>
    @/endforeach --}}
</table>