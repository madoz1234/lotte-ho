<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "No",
              "data": "num"
            },
            {
              "title": "Vendor Lokal",
              "data": "vendor_lokal"
            },
            {
              "title": "Tipe Transaksi",
              "data": "tipe"
            },
            {
              "title": "#",
              "data": "vendor",
              "className" : "dt-center"
            },           
            {
              "title": "Jatuh Tempo",
              "data": "tgl_jatuh_tempo",
              "className" : "dt-center"
            },
            {
              "title": "Total (Rp)",
              "data": "total",
              "className" : "dt-right"
            }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Laporan Pembayaran</div><br>
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Supplier</th>
            <th class="center aligned">Total (Rp)</th>
        </tr>
    </thead>
</table>