<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },
            {
              "title": "Category 1",
              "data": "category_1"
            },
            {
              "title": "Category 2",
              "data": "category_2"
            },
            {
              "title": "Kode Produk",
              "data": "kode_produk"
            },
            {
              "title": "Nama Produk",
              "data": "nama_produk"
            },
            {
              "title": "UOM",
              "data": "uom"
            },
            {
              "title": "Qty",
              "data": "qty"
            },
            {
              "title": "Cost Price (MAP) (Rp)",
              "data": "cost_price"
            },
            {
              "title": "Jumlah (Rp)",
              "data": "jumlah"
            },
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Rekap Nilai Persediaan</div><br>
<table id="reportTable" class="ui celled table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="20px">#</th>
            <th class="center aligned" width="40px">Category 1</th>
            <th class="center aligned" width="40px">Category 2</th>
            <th class="center aligned" width="40px">Kode Produk</th>
            <th class="center aligned" width="60px">Nama Produk</th>
            <th class="center aligned" width="40px">UOM</th>
            <th class="center aligned" width="40px">Qty</th>
            <th class="center aligned" width="40px">Cost Price (MAP) (Rp)</th>
            <th class="center aligned" width="40px">Jumlah (Rp)</th>
        </tr>
    </thead>
    <tbody>
       {{--  @/if($detail->count() != 0)
            </?php $i = 1;?>
            @/foreach ($detail as $row)
            <tr>
                <td class="center aligned">{/{ $i }}</td>
                <td class="center aligned">{/{ isset($row->produk->l1_nm) ? $row->produk->l1_nm : '-' }}</td>
                <td class="center aligned">{/{ isset($row->produk->l2_nm) ? $row->produk->l2_nm : '-' }}</td>
                <td class="center aligned">{/{ $row->produk->kode }}</td>
                <td class="left aligned">{/{ $row->produk->nama }}</td>
                <td class="right aligned">{/{ isset($row->produksetting->uom1_satuan) ? $row->produksetting->uom1_satuan : '-' }}</td>
                <td class="right aligned">
                    </?php
                        $qty = isset($row->stock_akhir) ? $row->stock_akhir : (isset($row->stock_awal) ? $row->stock_awal : 0);
                    ?>
                    {/{ number_format($qty) }}
                </td>
                <td class="right aligned">
                    </?php
                        $map = isset($row->hargaJual->map) ? $row->hargaJual->map : 0;
                    ?>
                    {/{ number_format($map) }}
                <td class="right aligned">{/{ number_format($qty*$map) }}</td>
            </tr>
            </?php $i++; ?>
            @/endforeach
        @/else
            <tr>
                <td colspan="9">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
        @/endif --}}
    </tbody>
</table>