<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },
            {
              "title": "LSI",
              "data": "lsi"
            },
            {
              "title": "TMUK",
              "data": "tmuk"
            },
            {
              "title": "Tanggal Stok Opname",
              "data": "tgl_so"
            },
            {
              "title": "Qty Aktual",
              "data": "qty_aktual"
            },
            {
              "title": "Qty System",
              "data": "qty_system"
            },
            {
              "title": "Selisih",
              "data": "selisih"
            },
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Rekap Stok Opname</div><br>

<table id="reportTable" class="ui celled table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th rowspan="2" class="center aligned" width="40px">#</th>
            <th rowspan="2" class="center aligned" width="40px">LSI</th>
            <th rowspan="2" class="center aligned" width="40px">TMUK</th>
            <th rowspan="2" class="center aligned" width="40px">Tanggal Stok Opname</th>
            <th colspan="3" class="center aligned" width="40px">Selisih Qty</th>
        </tr>
        <tr>
            <th class="center aligned" width="40px">Qty Aktual</th>
            <th class="center aligned" width="40px">Qty System</th>
            <th class="center aligned" width="40px">Selisih</th>
        </tr>
    </thead>
    <tbody>
        {{-- @/if($detail->count() != 0)
            </?php $i = 1; ?>
            @/foreach ($detail as $row)
            <tr>
                <td class="center aligned">{/{ $i }}</td>
                <td class="left aligned">{/{ $row->tmuk->lsi->nama }}</td>
                <td class="left aligned">{/{ $row->tmuk->nama }}</td>
                <td class="center aligned">{/{ \Carbon\Carbon::parse($row->tgl_so)->format('d/m/Y') }}</td>
                <td class="right aligned">{/{ number_format($row->qty_aktual) }}</td>
                <td class="right aligned">{/{ number_format($row->qty_system) }}</td>
                <td class="right aligned">{/{ number_format($row->selisih) }}</td>
            </tr>
            </?php $i++; ?>
            @/endforeach
        @/else
            <tr>
                <td colspan="8">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
        @/endif
    </tbody> --}}
</table>