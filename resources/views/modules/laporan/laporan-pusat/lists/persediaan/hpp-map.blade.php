<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },
            {
              "title": "Kode Produk",
              "data": "kode_produk"
            },
            {
              "title": "Nama Produk",
              "data": "nama_produk"
            },
            {
              "title": "UOM",
              "data": "uom2_satuan"
            },
            {
              "title": "HPP (Rp)",
              "data": "map"
            },
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Rekap HPP (MAP)</div><br>

<table id="reportTable" class="ui celled table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">Kode Produk</th>
            <th class="center aligned" width="40px">Nama Produk</th>
            <th class="center aligned" width="40px">UOM</th>
            <th class="center aligned" width="40px">HPP (Rp)</th>
        </tr>
    </thead>
    <tbody>
        {{-- @/if($detail->count() != 0)
        </?php $i = 1; ?>
            @/foreach ($detail as $row)
            <tr>
                <td class="center aligned">{/{ $i }}</td>
                <td class="center aligned">{/{ $row->produk_kode }}</td>
                <td class="center aligned">{/{ $row->produk->nama }}</td>
                <td class="right aligned">{/{ $row->produk->produksetting->uom2_satuan }}</td>
                <td class="right aligned">{/{ number_format(round($row->map)) }}</td>
            </tr>
            </?php $i++; ?>
            @/endforeach
        @/else
            <tr>
                <td colspan="9">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
        @/endif --}}
    </tbody>
</table>