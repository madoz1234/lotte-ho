<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },
            {
              "title": "Kode Produk",
              "data": "kode_produk"
            },
            {
              "title": "Deskripsi Produk",
              "data": "deskripsi_produk"
            },
            {
              "title": "Total Qty",
              "data": "total_qty"
            },
            {
              "title": "Qty (1-30 hari)",
              "data": "Qty1"
            },
            {
              "title": "Qty (31-60 hari)",
              "data": "qty31"
            },
            {
              "title": "Qty (61-90 hari)",
              "data": "qty61"
            },
            {
              "title": "Qty (91-120 hari)",
              "data": "qty91"
            },
            {
              "title": "Qty >120 hari",
              "data": "qty120"
            },
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Rekap Umur Persediaan</div><br>

<table id="reportTable" class="ui celled table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">Kode Produk</th>
            <th class="center aligned" width="40px">Deskripsi Produk</th>
            <th class="center aligned" width="40px">Total Qty</th>
            <th class="center aligned" width="40px">Qty (1-30 hari)</th>
            <th class="center aligned" width="40px">Qty (31-60 hari)</th>
            <th class="center aligned" width="40px">Qty (61-90 hari)</th>
            <th class="center aligned" width="40px">Qty (91-120 hari)</th>
            <th class="center aligned" width="40px">Qty >120 hari</th>
        </tr>
    </thead>
    <tbody>
{{--         @/if($detail->count() != 0)
        @/foreach ($detail as $key => $row)
        <tr>
            <td class="center aligned">{/{ $key + 1 }}</td>
            <td class="center aligned">{/{ $row->kode }}</td>
            <td class="center aligned">{/{ $row->nama }}</td>
            <td class="center aligned">{/{ $row->getTotalStock() }}</td>
            <td class="right aligned">{/{ number_format($row->getStockByDay(1)) }}</td>
            <td class="right aligned">{/{ number_format($row->getStockByDay(31)) }}</td>
            <td class="right aligned">{/{ number_format($row->getStockByDay(61)) }}</td>
            <td class="right aligned">{/{ number_format($row->getStockByDay(91)) }}</td>
            <td class="right aligned">{/{ number_format($row->getStockByDay(120)) }}</td>
        </tr>
        @/endforeach
        @/else
            <tr>
                <td colspan="9">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
        @/endif --}}
    </tbody>
</table>