<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/akun-piutang-member/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "className":"dt-body-center",
              "data": "num"
            },            
            {
              "title": "No Member",
              "data": "no_member"
            },       
            {
              "title": "Nama Member",
              "data": "nama_member"
            },       
            {
              "title": "Alamat",
              "data": "alamat"
            },       
            {
              "title": "Telepon",
              "data": "telepon"
            },       
            {
              "title": "Email",
              "data": "email"
            },       
            {
              "title": "Limit Kredit",
              "data": "limit_kredit",
              "className" : "dt-right"
            },       
            {
              "title": "Total Order",
              "data": "total_order",
              "className" : "dt-right"
            },       
            {
              "title": "Total pembelian (Rp)",
              "data": "total_pembelian",
              "className" : "dt-right"
            },       
            {
              "title": "Last Visit",
              "data": "last_visit",
              "className" : "dt-right"
            },       
            {
              "title": "Ave Pembelian",
              "data": "ave_pembelian",
              "className" : "dt-right"
            },
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>

<div class="title" style="font-size: 17px;">Daftar Member</div><br>

<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">No Member</th>
            <th class="center aligned" width="40px">Nama Member</th>
            <th class="center aligned" width="40px">Alamat</th>
            <th class="center aligned" width="40px">Telepon</th>
            <th class="center aligned" width="40px">Email</th>
            <th class="center aligned" width="40px">Limit Kredit</th>
            <th class="center aligned" width="40px">Total Order</th>
            <th class="center aligned" width="40px">Total pembelian (Rp)</th>
            <th class="center aligned" width="40px">Last Visit</th>
            <th class="center aligned" width="40px">Ave Pembelian</th>
        </tr>
    </thead>
    <tbody>
{{--         @/if($detail->count() > 0)
        </?php $i = 1; ?>
        @/foreach ($detail as $row)

        <tr>
            <td class="center aligned">{/{ $i }}</td>
            <td class="center aligned">{/{ $row->nomor }}</td>
            <td class="left aligned">{/{ $row->nama }}</td>
            <td class="left aligned">{/{ $row->alamat }}</td>
            <td class="left aligned">{/{ $row->telepon }}</td>
            <td class="left aligned">{/{ $row->email }}</td>
            <td class="right aligned">{/{ $row->limit_kredit==null ? '-': rupiah($row->limit_kredit) }}</td>
            <td class="center aligned">0</td>
            <td class="center aligned">0</td>
            <td class="center aligned">0</td>
            <td class="center aligned">0</td>
        </tr>
        </?php $i++; ?>
        @/endforeach
        @/else
        <tr>
            <td colspan="11">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @/endif --}}
    </tbody>
</table>