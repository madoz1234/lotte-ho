<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/akun-piutang-member/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "className":"dt-body-center",
              "data": "num"
            },            
            {
              "title": "No. Transaksi",
              "data": "idtrans"
            },
            {
         
              "title": "Tanggal Transaksi",
              "data": "tanggal",
              "className" : "dt-center"
            },
            {
              "title": "Umur Piutang",
              "data": "umur",
              "className" : "dt-right"
            },
            {
              "title": "Jumlah",
              "data": "jml",
              "className" : "dt-right"
            },
            {
              "title": "30 hari [0-30]",
              "data": "30_hari",
              "className" : "dt-right"
            },
            {
              "title": "60 hari [31-60]",
              "data": "60_hari",
              "className" : "dt-right"
            },
            {
              "title": "90 hari [61-90]",
              "data": "90_hari",
              "className" : "dt-right"
            },
            {
              "title": ">90 hari [91-dst]",
              "data": "lebih_90_hari",
              "className" : "dt-right"
            },
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Rekap Umur Piutang Member </div><br>

<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="5px">#</th>
            <th class="center aligned" width="40px">Tanggal Transaksi</th>
            <th class="center aligned" width="40px">Piutang Member (Rp)</th>
            <th class="center aligned" width="40px">Nama Member</th>
            <th class="center aligned" width="40px">Nama Member</th>
            <th class="center aligned" width="40px">Nama Member</th>
            <th class="center aligned" width="40px">Nama Member</th>
            <th class="center aligned" width="40px">Nama Member</th>
            <th class="center aligned" width="40px">Nama Member</th>
        </tr>
    </thead>
    <tbody>
{{--         @/foreach ($detail as $row)
            @/foreach ($row->detail as $data)
            <tr>
                <td class="center aligned">{/{ $i }}</td>
                <td class="center aligned">{/{ $data->tanggal }}</td>
                <td class="right aligned">{/{ rupiah($data->hutang) }}</td>
                <td class="center aligned">{/{ $data->piutang->no_member }} - {/{ $data->piutang->nama_member }}</td>
            </tr>
            </?php $i++; ?>
            @/endforeach
        @/endforeach --}}
    </tbody>
</table>