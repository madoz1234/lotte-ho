<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/akun-piutang-member/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "className":"dt-body-center",
              "data": "num"
            },            
            {
              "title": "Tanggal",
              "data": "tanggals",
              "className" : "dt-center"
            },      
            {
              "title": "No Member",
              "data": "no_member"
            },      
            {
              "title": "Nama Member",
              "data": "nama_member"
            },      
            {
              "title": "Jumlah Pembayaran (Rp)",
              "data": "jumlah_pembayaran",
              "className" : "dt-right"
            },      
            {
              "title": "Sisa Piutang (Rp)",
              "data": "sisa_piutang",
              "className" : "dt-right"
            },
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>

<div class="title" style="font-size: 17px;">Rekap Pembayaran Member</div><br>


<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">Tanggal</th>
            <th class="center aligned" width="40px">No Member</th>
            <th class="center aligned" width="40px">Nama Member</th>
            <th class="center aligned" width="40px">Jumlah Pembayaran (Rp)</th>
            <th class="center aligned" width="40px">Sisa Piutang (Rp)</th>
        </tr>
    </thead>
    <tbody>
        {{-- </?php $i = 1; ?>
        @/foreach ($detail as $row)
        <tr>
            <td class="center aligned">{{ $i }}</td>
            <td class="center aligned">{{ $row->created_at }}</td>
            <td class="center aligned">{{ $row->no_member }}</td>
            <td class="center aligned">{{ $row->nama_member }}</td>
            <td class="center aligned">0</td>
            <td class="center aligned">0</td>
        </tr>
        </?php $i++; ?>
        @/endforeach --}}
    </tbody>
</table>
