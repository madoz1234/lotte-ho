<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },            
            {
              "title": "Tanggal",
              "data": "tanggal"
            },            
            {
              "title": "Kode Produk",
              "data": "produk"
            },            
            {
              "title": "Nama Produk",
              "data": "nama"
            },            
            {
              "title": "Qty",
              "data": "qty"
            },            
            {
              "title": "Harga",
              "data": "harga"
            },            
            {
              "title": "Total",
              "data": "total"
            },            
            {
              "title": "Hpp",
              "data": "hpp"
            },            
            {
              "title": "Profit",
              "data": "profit"
            },            
            {
              "title": "Profit Margin",
              "data": "total_margin"
            }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Rekap Penjualan Perbarang</div><br>
<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned">Tanggal</th>
            <th class="center aligned">Kode Produk</th>
            <th class="center aligned">Nama Produk</th>
            <th class="center aligned">Qty</th>
            <th class="center aligned">Harga</th>
            <th class="center aligned">Total</th>
            <th class="center aligned">Hpp</th>
            <th class="center aligned">Profit</th>
            <th class="center aligned">Profit Margin</th>
        </tr>
    </thead>
</table>