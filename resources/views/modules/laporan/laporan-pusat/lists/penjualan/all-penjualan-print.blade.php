<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned">Tmuk Kode</th>
            <th class="center aligned">Tanggal</th>
            <th class="center aligned">Jml Struk</th>
            <th class="center aligned">Total Sales</th>
        </tr>
    </thead>
    <?php $i=1; ?>
    @foreach ($record as $elm)
        <tr>
            <td class="center aligned" width="40px">{{ $i }}</td>
            <td class="center aligned">{{ $elm->tmuk_kode }}</td>
            <td class="center aligned">{{ $elm->tanggal }}</td>
            <td class="center aligned">{{ $elm->jml_struk }}</td>
            <td class="center aligned">{{ $elm->total_sales }}</td>
        </tr>
        <?php $i++; ?>
    @endforeach
</table>