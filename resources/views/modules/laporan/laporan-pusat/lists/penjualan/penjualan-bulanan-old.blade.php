
<div class="title" style="font-size: 17px;">Rekap Penjualan Bulanan</div><br>
<div class="field">
    <div class="ui calendar labeled input pb_date_from">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input pb_date_to">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('penjualan-bulanan','-penjualan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>

<br>
@if(isset($detail))
    <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="center aligned" width="40px">#</th>
                <th class="center aligned" width="40px">Periode</th>
                <th class="center aligned" width="40px">Total Penjualan (Rp)</th>
                <th class="center aligned" width="40px">Penjualan Tunai (Rp)</th>
                <th class="center aligned" width="40px">Penjualan Piutang (Rp)</th>
                <th class="center aligned" width="40px">Penjualan Poin (Rp)</th>
            </tr>
        </thead>
        <tbody>
            @if($detail->count() != 0)
            <?php $i = 1; ?>
            @foreach ($detail->sortBy('month') as $row)
            <tr>
                <td class="center aligned">{{ $i }}</td>
                <td class="center aligned">{{ \Carbon\Carbon::createFromFormat('Y-m', $row->year.'-'.$row->month)->formatLocalized('%B %Y') }}</td>
                <td class="right aligned">{{ rupiah($row->total_penjualan) }}</td>
                <td class="right aligned">
                    <?php $cash=0; ?>
                    @foreach($tunai as $tn)
                        @if($tn->month==$row->month && $tn->year==$row->year)
                            <?php $cash=$cash+($tn->tunai-$tn->kembali);?>
                        @endif
                    @endforeach
                    {{ rupiah($cash) }}
                </td>
                <td class="right aligned">
                    <?php $utang=0; ?>
                    @foreach($piutang as $tn)
                        @if($tn->month==$row->month && $tn->year==$row->year)
                            <?php $utang=$utang+$tn->tunai;?>
                        @endif
                    @endforeach
                    {{ rupiah($utang) }}
                </td>
                <td class="right aligned">
                    <?php $poin=0; ?>
                    @foreach($point as $tn)
                        @if($tn->month==$row->month && $tn->year==$row->year)
                            <?php $poin=$poin+$tn->tunai;?>
                        @endif
                    @endforeach
                    {{ rupiah($poin) }}
                </td>
            </tr>
            <?php $i++; ?>
            @endforeach
            @else
            <tr>
                <td colspan="6">
                    <center><i>Maaf tidak ada data</i></center>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif

        $('.pb_date_from').calendar({
            type: 'month',
            endCalendar: $('.pb_date_to'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return year+'-'+month;
                }
            }
        })

        $('.pb_date_to').calendar({
            type: 'month',
            startCalendar: $('.pb_date_from'),
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return year+'-'+month;
                }
            }
        })
    });

    $(document).on('click', '.reset.button', function(e){
        $('#mulai_filter').val('');
        $('#selesai_filter').val('');
        $('.filter.button').trigger('click');
    });
</script>
