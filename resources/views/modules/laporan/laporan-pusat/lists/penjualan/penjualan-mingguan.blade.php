<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token  = "{{ csrf_token() }}";
                d.types     = $("[name=types]").val();
                d.status    = "{{ $lists_grid }}";
                d.region_id = $('[name="region_id"]').val();
                d.lsi_id    = $('[name="lsi_id"]').val();
                d.tmuk_kode = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start     = $('#mulai_filter').val();
                d.tanggal_end       = $('#selesai_filter').val();
            }
        }, 

        columns: [
            {
              "title": "#",
              "data": "num"
            },
            {
              "title": "TMUK",
              "data": "tmuk_kode"
            },
            {
              "title": "Priode",
              "data": "periode",
              "className" : "dt-center"
            },
            {
              "title": "Total Penjualan (Rp)",
              "data": "total_penjualan",
              "className" : "dt-right"
            },
            {
              "title": "Penjualan Tunai (Rp)",
              "data": "tunai",
              "className" : "dt-right"
            },
            {
              "title": "Piutang (Rp)",
              "data": "piutang",
              "className" : "dt-right"
            },
            {
              "title": "Poin (Rp)",
              "data": "points",
              "className" : "dt-right"
            },
            {
              "title": "Diskon (Rp)",
              "data": "diskon",
              "className" : "dt-right"
            },
            {
              "title": "Voucher (Rp)",
              "data": "voucher",
              "className" : "dt-right"
            }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Rekap Penjualan Mingguan  </div><br>
<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned">#</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Priode</th>
            <th class="center aligned">Total Penjualan (Rp)</th>
            <th class="center aligned">Penjualan Tunai (Rp)</th>
            <th class="center aligned">Piutang (Rp)</th>
            <th class="center aligned">Poin (Rp)</th>
            <th class="center aligned">Diskon (Rp)</th>
            <th class="center aligned">Voucher (Rp)</th>
        </tr>
    </thead>
</table>