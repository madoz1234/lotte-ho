<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },            
            {
              "title": "TMUK",
              "data": "tmuk_kode"
            },
            {
         
              "title": "Tanggal",
              "data": "tanggal",
              "className" : "dt-center"
            },
            {
              "title": "Total Penjualan (Rp)",
              "data": "total_penjualan",
              "className" : "dt-right"
            },
            {
              "title": "Total Setoran (Rp)",
              "data": "setoran",
              "className" : "dt-right"
            },
            // {
            //   "title": "Penjualan Piutang (Rp)",
            //   "data": "piutang"
            // },
            // {
            //   "title": "Penjualan Poin (Rp)",
            //   "data": "points"
            // }
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Setoran Penjualan</div><br>
<table id="reportTable" class="ui celled compact red striped table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned">Kode TMUK</th>
            <th class="center aligned">Tanggal</th>
            <th class="center aligned">Total Penjualan (Rp)</th>
            <th class="center aligned">Total Setoran (Rp)</th>
            {{-- <th class="center aligned">Penjualan Tunai (Rp)</th>
            <th class="center aligned">Penjualan Piutang (Rp)</th>
            <th class="center aligned">Penjualan Poin (Rp)</th> --}}
        </tr>
    </thead>
{{--     </?php $i=1; ?>
    @/foreach ($record as $elm)
        <tr>
            <td class="center aligned" width="40px">{/{ $i }}</td>
            <td class="center aligned">{/{ $elm->tmuk_kode }}</td>
            <td class="center aligned">{/{ $elm->tanggal }}</td>
            <td class="center aligned">{/{ $elm->total_penjualan }}</td>
            <td class="center aligned">{/{ $elm->tunai }}</td>
            <td class="center aligned">{/{ $elm->piutang }}</td>
            <td class="center aligned">{/{ $elm->points }}</td>
        </tr>
        </?php $i++; ?>
    @/endforeach --}}
</table>