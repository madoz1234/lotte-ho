@if(isset($detail))
    @if($detail->count() != 0)
    <div class="ui form" style="float:right">
     <a class="ui default right labeled icon save button" target="_blank" onclick="printLaporan('print-laporan-penjualan','penjualan-harian');">
        Print
        <i class="print icon"></i>
    </a>
    <div class="ui green export buttons" style="margin-right: 5px">
        <div class="ui button" onclick="printLaporan('exel-laporan-penjualan','penjualan-harian');"><i class="file excel outline icon"></i>Export</div>
    </div>
    </div>
    @endif
@endif

<div class="title" style="font-size: 17px;">Rekap Penjualan Harian</div><br>
<div class="field">
    <div class="ui calendar labeled input ph_date_start" id="start">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
        </div>
    </div>
    &nbsp;s.d.&nbsp;
    <div class="ui calendar labeled input ph_date_end" id="end">
        <div class="ui input left icon">
            <i class="calendar icon date"></i>
            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
        </div>
    </div>
    <button type="button" class="ui teal icon filter button" data-content="Cari Data"  onclick="filter('penjualan-harian','-penjualan');">
        <i class="search icon"></i>
    </button>
    {{-- <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
        <i class="refresh icon"></i>
    </button> --}}
</div>
<br>

@if(isset($detail))
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned" width="40px">Tanggal</th>
            <th class="center aligned" width="40px">Total Penjualan (Rp)</th>
            <th class="center aligned" width="40px">Penjualan Tunai (Rp)</th>
            <th class="center aligned" width="40px">Penjualan Piutang (Rp)</th>
            <th class="center aligned" width="40px">Penjualan Poin (Rp)</th>
            <th class="center aligned" width="40px">Potongan Penjualan (Rp)</th>
        </tr>
    </thead>
    <tbody>
        @if($detail->count() > 0)
        <?php $i = 1; ?>
        @foreach ($detail->sortByDesc('date') as $row)
        <tr>
            <td class="center aligned">{{ $i }}</td>
            <td class="center aligned">{{ $row->date }}</td>
            <td class="right aligned">{{ rupiah($row->total_penjualan) }}</td>
            <td class="right aligned">
                <?php $cash=0; ?>
                @foreach($tunai as $tn)
                    @if($tn->tanggal==$row->date)
                        <?php $cash=$cash+($tn->tunai-$tn->kembali);?>
                    @endif
                @endforeach
                {{ rupiah($cash) }}
            </td>
            <td class="right aligned">
                <?php $utang=0; ?>
                @foreach($piutang as $pt)
                    @if($pt->tanggal==$row->date)
                        <?php $utang=$utang+$pt->tunai;?>
                    @endif
                @endforeach
                {{ rupiah($utang) }}
            </td>
            <td class="right aligned">
                <?php $poin=0; ?>
                @foreach($point as $pt)
                    @if($pt->tanggal==$row->date)
                        <?php $poin=$poin+$pt->tunai;?>
                    @endif
                @endforeach
                {{ rupiah($poin) }}
            </td>
            <td class="right aligned">
                <?php $vc=0; ?>
                @foreach($voucher as $pt)
                    @if($pt->tanggal==$row->date)
                        <?php $vc=$vc+$pt->tunai;?>
                    @endif
                @endforeach
                {{ rupiah($vc) }}
            </td>
        </tr>
        <?php $i++; ?>
        @endforeach
        @else
        <tr>
            <td colspan="11">
                <center><i>Maaf tidak ada data</i></center>
            </td>
        </tr>
        @endif
    </tbody>
</table>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $('.ph_date_start').calendar({
        type: 'date',
        endCalendar: $('.ph_date_end'),
        formatter: {
         date: function (date, settings) {
             if (!date) return '';
             var day = date.getDate();
             var month = date.getMonth() + 1;
             var year = date.getFullYear();
             return year+'-'+month+'-'+day;
         }
     }
    })

    $('.ph_date_end').calendar({
        type: 'date',
        startCalendar: $('.ph_date_start'),
        formatter: {
         date: function (date, settings) {
             if (!date) return '';
             var day = date.getDate();
             var month = date.getMonth() + 1;
             var year = date.getFullYear();
             return year+'-'+month+'-'+day;
         }
     }
    })
    $(document).ready(function() {
        @if(isset($detail))
            @if($detail->count() != 0)
                $('#reportTable').DataTable({
                    paging: 10,
                    filter: false,
                    lengthChange: false,
                    ordering:false,
                });
            @endif
        @endif
        // $('.ui.calendar').calendar({
        //     type: 'date',
        //     formatter: {
        //         date: function (date, settings) {
        //             if (!date) return '';
        //             var day = date.getDate();
        //             var month = date.getMonth() + 1;
        //             var year = date.getFullYear();
        //             return year+'-'+month+'-'+day;
        //         }
        //     }
        // });
    });
    // $(document).on('click', '.reset.button', function(e){
    //     $('#mulai_filter').val('');
    //     $('#selesai_filter').val('');
    //     $('.filter.button').trigger('click');
    // });
</script>
