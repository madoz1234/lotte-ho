
<div class="title" style="font-size: 17px;">LAPORAN PERFORMANCE TMUK</div><br>

    @if($records->count() != 0)
        @foreach ($records as $row)
            <div class="ui packed yellow segment" style="overflow-x: auto;">
            <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="center aligned" width="40px">ITEM</th>
                        <th class="center aligned" width="40px">KKI</th>
                        <th class="center aligned" width="40px">%</th>
                        @for($m = 0; $m <= $diff; $m++)
                            <th class="center aligned" width="40px">{{ $dayStart->copy()->addMonths($m)->format('F / Y') }}</th>
                            <th class="center aligned" width="40px">%</th>
                        @endfor
                        <th class="center aligned" width="40px">Akumulasi</th>
                        <th class="center aligned" width="40px">Average</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="left aligned">PENJUALAN</td>
                        <td class="center aligned">{{ rupiah($row->ii_penjualan_normal) }}</td>
                        <?php $persen_penjualan = 0; $hppp=0; ?>
                        <?php  $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  ($row->ii_penjualan_normal / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan) }}%</td>
                        <?php $akumulasi = 0; $jurnal_penjualan=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $akumulasi += $jurnal_penjualan;
                            ?>
                            <td class="center aligned">{{ rupiah($jurnal_penjualan) }}</td>
                            <td class="center aligned">{{ $jurnal_penjualan == 0 ? 0 : rupiah($jurnal_penjualan / $jurnal_penjualan * (100)) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">HPP</td>
                        <td class="center aligned">{{ rupiah($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2) }}</td>
                        <?php $persen_penjualan=0; ?>
                        <?php  $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  (($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2) / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan) }}%</td>
                        <?php $akumulasi = 0; $hppp=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $akumulasi += $hppp;
                            ?>
                            <td class="center aligned">{{ rupiah($hppp) }}</td>
                            <td class="center aligned">{{ $hppp == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $hppp / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Margin</td>
                        <td class="center aligned">{{ rupiah($row->ii_penjualan_normal - ($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2) ) }}</td>
                        <?php $persen_penjualan =0; ?>
                        <?php  $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  (($row->ii_penjualan_normal - ($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2)) / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan) }}%</td>
                        <?php $akumulasi = 0; $margin=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $margin = $jurnal_penjualan - $hppp;
                                $akumulasi += $margin;
                            ?>
                            <td class="center aligned">{{ rupiah($margin) }}</td>
                            <td class="center aligned">{{ $margin == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $margin / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $akumulasi += $total;
                            ?>
                            <td class="center aligned"></td>
                            <td class="center aligned"></td>
                        @endfor
                        <td class="center aligned" width="40px"></td>
                        <td class="center aligned" width="40px"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">SPD</td>
                        <td class="center aligned">{{ rupiah($row->ii_sales_normal) }}</td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; $jurnal_penjualan= 0; $spd_jual =0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                            

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $toko_buka = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);


                                $spd_jual = $jurnal_penjualan == 0 ? 0 : $jurnal_penjualan / count($toko_buka) ;
                                $akumulasi += $spd_jual;
                            ?>
                            <td class="center aligned">{{ rupiah($spd_jual) }}</td>
                            <td class="center aligned">{{-- {{ $spd_jual == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $spd_jual / $jurnal_penjualan * (100) : 0 ) }}% --}}</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>

                    {{-- == --}}
                    <tr>
                        <td class="left aligned">STD</td>
                        <td class="center aligned">{{ rupiah($row->ii_struk_normal) }}</td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; $struk = 0; $std_jual=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $struk = Lotte\Models\Trans\TransRekapPenjualanTmuk::getCountStruk($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $toko = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $std_jual = $struk == 0 ? 0 : $struk / count($toko);
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $akumulasi += $std_jual;
                            ?>
                            <td class="center aligned">{{ rupiah($std_jual) }}</td>
                            <td class="center aligned">{{-- {{ $std_jual == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $std_jual / $jurnal_penjualan * (100) : 0 ) }}% --}}</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">APC</td>
                        <td class="center aligned">{{ $row->ii_sales_normal == 0 ?  '0' : rupiah($row->ii_sales_normal / $row->ii_struk_normal) }}</td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; $spd_jual = 0; $std_jual = 0;  $apc_jual = 0; $struk = 0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $toko_buka = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $spd_jual = $jurnal_penjualan == 0 ? 0 : $jurnal_penjualan / count($toko_buka) ;
                                $struk = Lotte\Models\Trans\TransRekapPenjualanTmuk::getCountStruk($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $toko = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTokoBuka($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $std_jual = $struk == 0 ? 0 : $struk / count($toko);
                                $apc_jual =  $spd_jual == 0 ? 0 : $spd_jual / $std_jual;
                                $akumulasi += $apc_jual;
                            ?>
                            <td class="center aligned">{{ rupiah($apc_jual) }}</td>
                            <td class="center aligned">{{-- {{ $apc_jual == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $apc_jual / $jurnal_penjualan * (100) : 0 ) }}% --}}</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        <?php $akumulasi = 0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $akumulasi += $total;
                            ?>
                            <td class="center aligned"></td>
                            <td class="center aligned"></td>
                        @endfor
                        <td class="center aligned" width="40px"></td>
                        <td class="center aligned" width="40px"></td>
                    </tr>
                    <thead>
                         <tr>
                            <th class="left aligned">Biaya Operasional</th>
                            <th class="center aligned"></th>
                            <th class="center aligned"></th>
                            @for($m = 0; $m <= $diff; $m++)
                                <?php 
                                    $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                                ?>
                                <th class="center aligned"></th>
                                <th class="center aligned"></th>
                            @endfor
                            <th class="center aligned" width="40px"></th>
                            <th class="center aligned" width="40px"></th>
                        </tr>
                    </thead>

                    <tr>
                    <?php $utilities = ($row->iv_satu_a_estimasi2+$row->iv_satu_b_estimasi2) + ($row->iv_satu_c_estimasi2); ?>
                        <td class="left aligned">Biaya Air , Listrik & Telp/Internet</td>
                        <td class="center aligned">{{ rupiah($utilities) }}</td>
                        <?php $persen_penjualan_listrik=0; ?>
                        <?php  $persen_penjualan_listrik = $row->ii_penjualan_normal == 0 ? 0 :  ($utilities / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_listrik) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili =0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                    <?php $perlengapan_toko = 0; ?>
                        <td class="left aligned">Perlengkapan Toko (ATK)</td>
                        <td class="center aligned">{{ rupiah($perlengapan_toko) }}</td>
                        <?php $persen_penjualan_atk=0; ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_atk) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '1.2.6.0');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">0</td>
                            <td class="center aligned">0%</td>
                        @endfor
                        <td class="center aligned">0</td>
                        <td class="center aligned">0</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Gaji Karyawan</td>
                        <td class="center aligned">{{ rupiah($row->iv_dua_estimasi2) }}</td>
                        <?php $persen_penjualan_gaji=0; ?>
                        <?php  $persen_penjualan_gaji = $row->ii_penjualan_normal == 0 ? 0 :  ($row->iv_dua_estimasi2 / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_gaji) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Pembungkus</td>
                        <td class="center aligned">{{ rupiah($row->iv_satu_f_estimasi2) }}</td>
                        <?php $persen_penjualan_bungkus=0; ?>
                        <?php  $persen_penjualan_bungkus = $row->ii_penjualan_normal == 0 ? 0 :  ($row->iv_satu_f_estimasi2 / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah(ceil($persen_penjualan_bungkus)) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Sewa Toko</td>
                        <td class="center aligned">{{ rupiah($row->iv_empat_d_estimasi2) }}</td>
                        <?php $persen_penjualan_sewa=0; ?>
                        <?php  $persen_penjualan_sewa = $row->ii_penjualan_normal == 0 ? 0 :  ($row->iv_empat_d_estimasi2 / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_sewa) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode,' 4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Lain-lain</td>
                        <?php $biaya_lain = $row->iv_satu_e_estimasi2 + $row->iv_tambahan_satu_b + $row->iv_tambahan_dua_b + $row->iv_tambahan_tiga_b + $row->iv_tambahan_empat_b;?>
                        <td class="center aligned">{{ rupiah($biaya_lain) }}</td>
                        <?php $persen_penjualan_lain=0; ?>
                        <?php  $persen_penjualan_lain = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_lain / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_lain) }}%</td>
                        <?php $utili=0; ?>
                         @for($m = 0; $m <= $diff; $m++)
                        <?php 
                            $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                            $akumulasi += $total;
                            ;
                        ?>
                        <td class="center aligned">0</td>
                        <td class="center aligned">{{ $utili == 0 ? 0 : rupiah($utili / 0 * (100)) }}%</td>
                        @endfor
                        <td class="center aligned">0</td>
                        <td class="center aligned">0</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Keamanan</td>
                        <?php $biaya_keamanan = $row->iv_satu_d_estimasi2;?>
                        <td class="center aligned">{{ rupiah($biaya_keamanan) }}</td>
                        <?php $persen_penjualan_keamanan=0; ?>
                        <?php  $persen_penjualan_keamanan = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_keamanan / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_keamanan) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Distribusi (Administrasi)</td>
                        <?php $biaya_adm = $row->iv_satu_i_estimasi2;?>
                        <td class="center aligned">{{ rupiah($biaya_adm) }}</td>
                        <?php $persen_penjualan_adm=0; ?>
                        <?php  $persen_penjualan_adm = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_adm / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_adm) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Barang Rusak/Hilang</td>
                        <?php $barang_rusak = $row->iv_satu_g_estimasi2 + $row->iv_satu_h_estimasi2;?>
                        <td class="center aligned">{{ rupiah($barang_rusak) }}</td>
                        <?php $persen_penjualan_rusak=0; ?>
                        <?php  $persen_penjualan_rusak = $row->ii_penjualan_normal == 0 ? 0 :  ($barang_rusak / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_rusak) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Premi Asuransi</td>
                        <?php $biaya_premi = $row->iv_tiga_a_estimasi2 + $row->iv_tiga_b_estimasi2; ?>
                        <td class="center aligned">{{ rupiah($biaya_premi) }}</td>
                        <?php $persen_penjualan_premi=0; ?>
                        <?php  $persen_penjualan_premi = $row->ii_penjualan_normal == 0 ? 0 :  ($biaya_premi / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_premi) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $utili=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $utili = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $total_akumulasi += $utili;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($utili) }}</td>
                            <td class="center aligned">{{ $utili == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $utili / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ $count_akumulasi == 0 ? 0 : rupiah($total_akumulasi/$count_akumulasi) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned">Biaya Depresiasi & Amortiasi</td>
                        <?php $dep_amor = $row->iv_empat_a_estimasi2 + $row->iv_empat_b_estimasi2 + $row->iv_empat_c_estimasi2 ; ?>
                        <td class="center aligned">{{ rupiah($dep_amor) }}</td>
                        <?php $persen_penjualan_amor=0; ?>
                        <?php $persen_penjualan_amor = $row->ii_penjualan_normal == 0 ? 0 :  ($dep_amor / $row->ii_penjualan_normal) * 100 ?>
                        <td class="center aligned">{{ rupiah($persen_penjualan_amor) }}%</td>
                        <?php $utili=0; ?>
                         @for($m = 0; $m <= $diff; $m++)
                        <?php 
                            $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                            $akumulasi += $total;
                        ?>
                        <td class="center aligned">0</td>
                        <td class="center aligned">0%</td>
                        @endfor
                        <td class="center aligned">0</td>
                        <td class="center aligned">0</td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        @for($m = 0; $m <= $diff; $m++)
                        <?php 
                            $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                            $akumulasi += $total;
                            ;
                        ?>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        @endfor
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Total Biaya</td>
                        <td class="center aligned">{{ rupiah($utilities + $biaya_premi + $biaya_lain + $perlengapan_toko + $row->iv_dua_estimasi2 + $row->iv_satu_f_estimasi2 + $barang_rusak + $dep_amor + $row->iv_empat_d_estimasi2 + $biaya_adm + $biaya_keamanan) }}</td>
                        <td class="center aligned">{{ rupiah($persen_penjualan_amor + $persen_penjualan_premi + $persen_penjualan_rusak + $persen_penjualan_lain + $persen_penjualan_sewa + $persen_penjualan_bungkus + $persen_penjualan_gaji + $persen_penjualan_listrik + $persen_penjualan_atk + $persen_penjualan_adm + $persen_penjualan_keamanan) }}%</td>
                        <?php $total_akumulasi = 0; $count_akumulasi = 0; $total_biaya_jual=0; ?>
                        @for($m = 0; $m <= $diff; $m++)
                            <?php 
                                $total_biaya_jual = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4')+ Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1') +  Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10');

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $total_akumulasi += $total_biaya_jual;
                                $count_akumulasi++;
                            ?>
                            <td class="center aligned">{{ rupiah($total_biaya_jual) }}</td>
                            <td class="center aligned">{{ $total_biaya_jual == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $total_biaya_jual / $jurnal_penjualan * (100) : 0 ) }}%</td>
                        @endfor
                        <td class="center aligned">{{ rupiah($total_akumulasi) }}</td>
                        <td class="center aligned">{{ rupiah($total_akumulasi / ($diff + 1)) }}</td>
                    </tr>
                    <tr>
                        <td class="left aligned"></td>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        @for($m = 0; $m <= $diff; $m++)
                        <?php 
                            $total = Lotte\Models\Trans\TransRekapPenjualanTmuk::getTotalMonths($dayStart->copy()->addMonths($m), $tmuk_kode);
                            $akumulasi += $total;
                            ;
                        ?>
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                        @endfor
                        <td class="center aligned"></td>
                        <td class="center aligned"></td>
                    </tr>

                    <thead>
                         <tr>
                            <th class="center aligned">Laba Bersih</th>
                            <?php 
                                $laba_bersih = ($row->ii_penjualan_normal - ($row->ii_penjualan_normal  - $row->iii_labakotor_estimasi2)) - ($utilities + $biaya_premi + $biaya_lain + $perlengapan_toko + $row->iv_dua_estimasi2 + $row->iv_satu_f_estimasi2 + $barang_rusak + $dep_amor + $row->iv_empat_d_estimasi2 + $biaya_keamanan + $biaya_adm )   ?>
                            <th class="center aligned">{{ rupiah($laba_bersih) }}</th>
                            <?php $persen_penjualan=0; ?>
                            <?php
                                $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  ($laba_bersih / $row->ii_penjualan_normal) * 100 ?>
                            <th class="center aligned">{{ rupiah($persen_penjualan) }}%</th>
                            <?php $akumulasi_laba_tmuk = 0; $laba_bersih_tmuk_b=0; ?>
                            @for($m = 0; $m <= $diff; $m++)
                            <?php 

                                $jurnal_penjualan_b = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $margin = $jurnal_penjualan_b - $hppp;

                                $total_biaya_b = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4')+ Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1') +  Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10');
                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');


                                $laba_bersih_tmuk_b = $margin - $total_biaya_b;
                                $akumulasi_laba_tmuk += $laba_bersih_tmuk_b;


                            ?>
                            <th class="center aligned">{{ rupiah($laba_bersih_tmuk_b) }}</th>
                            <th class="center aligned">{{ $laba_bersih_tmuk_b == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $laba_bersih_tmuk_b / $jurnal_penjualan * (100) : 0 ) }}%</th>
                            @endfor
                            <th class="center aligned" width="40px">{{ rupiah($akumulasi_laba_tmuk) }}</th>
                            <th class="center aligned" width="40px">{{ rupiah($akumulasi_laba_tmuk / ($diff + 1)) }}</th>
                        </tr>
                        <tr>
                            <th class="center aligned">Laba Cash</th>
                            <?php
                                $laba_cash =  $dep_amor +  $laba_bersih + $row->iv_empat_d_estimasi2?>
                            <th class="center aligned">{{ rupiah($laba_cash) }}</th>
                            <?php $persen_penjualan=0; ?>
                            <?php 
                                $persen_penjualan = $row->ii_penjualan_normal == 0 ? 0 :  ($laba_cash / $row->ii_penjualan_normal) * 100 ?>
                            <th class="center aligned">{{ rupiah($persen_penjualan) }}%</th>
                            <?php $akumulasi_laba_tmuk = 0; $laba_bersih_tmuk=0; ?>
                            @for($m = 0; $m <= $diff; $m++)
                            <?php 

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');
                                $hppp = Lotte\Models\Trans\TransJurnal::getHppPenjualan($dayStart->copy()->addMonths($m), $tmuk_kode);
                                $margin = $jurnal_penjualan - $hppp;
                                

                                $total_biaya_c = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.2') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.3') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.4')+ Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.1') +  Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.7') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.13') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.8') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.9') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.1.4') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.5') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '6.2.2.10');

                                $jurnal_penjualan = Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.1.1.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.2.0.0') + Lotte\Models\Trans\TransJurnal::getTotalJumlah($dayStart->copy()->addMonths($m), $tmuk_kode, '4.3.0.0');

                                $laba_bersih_tmuk = $margin - $total_biaya_c;
                                $akumulasi_laba_tmuk += $laba_bersih_tmuk;


                            ?>
                            <th class="center aligned">{{ rupiah($laba_bersih_tmuk) }}</th>
                            <th class="center aligned">{{ $laba_bersih_tmuk == 0 ? 0 : rupiah( $jurnal_penjualan != 0 ? $laba_bersih_tmuk / $jurnal_penjualan * (100) : 0 ) }}%</th>
                            @endfor
                            <th class="center aligned" width="40px">{{ rupiah($akumulasi_laba_tmuk) }}</th>
                            <th class="center aligned" width="40px">{{ rupiah($akumulasi_laba_tmuk / ($diff + 1)) }}</th>
                        </tr>
                    </thead>




                <thead>
                    
                </thead>
                    
                    
                </tbody>
                <tfoot>
                    
                </tfoot>
            </table>
            </div>
        @endforeach
    @else
        <div class="ui segment">
          <p align="center">Maaf tidak ada data</p>
        </div>
    @endif
