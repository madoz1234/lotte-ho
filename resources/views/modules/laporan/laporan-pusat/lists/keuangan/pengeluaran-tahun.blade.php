
<style type="text/css">

    .scroll_imp{
        width: 100% !important;
        border:1px solid #D2F2E6;
        min-height:150mm!important;
        font-size: 12px!important;
        max-height: 150mm !important;
    }
</style>

<div class="title" style="font-size: 17px;">LAPORAN PENGELUARAN PERTAHUN</div><br>
@if(isset($records))
{{-- {{ dd($records) }} --}}
<div id="scroll" class="scroll scroll_imp">
    <div class="scrollDiv" >
        <div class="scrollContent">
            <table id="reportTable" class="ui celled compact table"  cellspacing="0">
                <thead>
                    <tr>
                        <th class="center aligned" style="width:400mm;">TMUK</th>
                        @foreach ($header as $row)
                            <th class="center aligned" >{{ $row }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($records as $key => $elem)
                        <tr>
                            <td colspan="{{ count($header)+1 }}">
                               <center> {{ $key }}</center>
                            </td>
                        </tr>
                            <?php 
                                $nil   = $nilai_temp; 
                                $nilai = $nilai_temp;
                            ?>
                            @foreach ($elem as $key => $nils)
                                <tr>
                                    <td  style="width:400mm; font-weight: bold;">{{ $key }}</td>
                                    @foreach ($nilai as $keys => $val)
                                        <td>{{ isset($nils[$keys])?$nils[$keys]:$nil[$keys] }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@else
<div class="ui segment">
  <p align="center">Filter tanggal terlebih dahulu</p>
</div>
@endif
