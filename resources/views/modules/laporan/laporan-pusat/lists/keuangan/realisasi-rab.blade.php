<div class="title" style="font-size: 17px;">LAPORAN REALISASI RAB</div><br>

@if(isset($detail))
    @if($detail->count() != 0)
        @foreach ($detail as $data)
            <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="center aligned" width="40px">RAB</th>
                        <th class="center aligned" width="40px">TOTAL</th>
                        <th class="center aligned" width="40px">Realisasi</th>
                        <th class="center aligned" width="40px">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="left aligned" colspan="3"><b>{{ $data->tmuk->nama }}</b></td>
                    </tr>
                    <tr>
                        <td class="left aligned">Perkiraan dana investasi</td>
                        <td class="center aligned"></td>
                        <td class="left aligned">Pemasukan</td>
                        <td class="right aligned"></td>
                    </tr>
                        <?php $sum_tot = 0 ?>
                        @foreach ($saldo as $jual)
                        {{-- {{ dd($jual->tmuk_kode) }} --}}
                    <tr>
                        @if ($data->tmuk->kode == $jual->tmuk_kode)
                            <td class="left aligned" style="padding-left: 50px;"></td>
                            <td class="right aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">{{ \Carbon\Carbon::createFromFormat('m-Y',$jual->month.'-'.$jual->year)->format('F  Y') }}</td>
                            <td class="right aligned">{{ rupiah($jual->total_penjualan) }}</td>
                        <?php $sum_tot += $jual->total_penjualan ?>
                        @endif
                    </tr>
                        @endforeach
                    <tr>
                        <td class="left aligned" style="padding-left: 50px;">DP</td>
                        <td class="right aligned">{{ rupiah($data->i_satu) }}</td>
                        <td class="left aligned" style="padding-left: 50px;"></td>
                        <td class="right aligned"><b>{{ rupiah($sum_tot) }}</b></td>
                    </tr>
                    <tr>
                        <td class="left aligned" style="padding-left: 50px;">Dana investasi</td>
                        <?php $invest = $data->i_dua + $data->i_tiga_a + $data->i_tiga_b + $data->i_tiga_c + $data->i_tiga_d + $data->i_tiga_e + $data->i_tiga_f + $data->i_empat_a + $data->i_empat_b + $data->i_empat_c + $data->i_lima_a + $data->i_lima_b + $data->i_bila_ada_biaya_tambahan + $data->i_sewa_ruko + $data->i_ppn; ?>
                        <td class="right aligned">{{ rupiah($invest) }}</td>
                        <td class="left aligned">Pengeluaran</td>
                        <?php $totalrealisasirab = $invest + $data->i_satu + $data->i_lima_a + $data->i_lima_b + $data->i_tiga_a + $data->i_tiga_b + $data->i_tiga_c + $data->i_tiga_d + $data->i_tiga_e + $data->i_tiga_f + $data->i_empat_a + $data->i_empat_c + $data->i_empat_b + $data->i_satu + $data->i_lima_b + $data->i_bila_ada_biaya_tambahan; ?>
                        <td class="center aligned"></td>
                    </tr>                    
                          
                        <?php 
                            $totaldana      = $data->i_satu + $invest; 
                            $start          = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 1,$data->tmuk->kode); 
                            $perijinan      = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 2,$data->tmuk->kode);
                            $renovasi       = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 3,$data->tmuk->kode);
                            $kusen          = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 4,$data->tmuk->kode);
                            $folding        = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 5,$data->tmuk->kode);
                            $instalasi      = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 6,$data->tmuk->kode);
                            $pemasangan     = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 7,$data->tmuk->kode);
                            $signage        = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 8,$data->tmuk->kode);
                            $sipil          = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 9,$data->tmuk->kode);
                            $non_elektronik = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 10,$data->tmuk->kode);
                            $elektronik     = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 11,$data->tmuk->kode);
                            $stock_awal     = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 12,$data->tmuk->kode);
                            $modal_kerja    = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 13,$data->tmuk->kode);
                            $lain_lain      = Lotte\Models\Trans\TransPyrDetail::sumPriceByKeteranganPusat('OPENING', 14,$data->tmuk->kode);

                        ?>
                       
                        <tr>
                            <td class="left aligned"></td>
                            <td class="right aligned"><b>{{ rupiah($totaldana) }}</b></td>
                            <td class="left aligned" style="padding-left: 50px;">Start & Biaya Promosi Opening</td>
                            <td class="right aligned">{{ rupiah($start ? $start : '0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned">Perkiraan Pengeluaran</td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Perijinan Usaha Toko</td>
                            <td class="right aligned">{{ rupiah($perijinan?$perijinan:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Stok awal</td>
                            <?php $stokawal = $data->i_lima_a + $data->i_lima_b; ?>
                            <td class="right aligned">{{ rupiah($stokawal?$stokawal:'0')}}</td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Renovasi Sipil</td>
                            <td class="right aligned">{{ rupiah($renovasi?$renovasi:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Kusen Aluminium + Kaca Cermin</td>
                            <td class="right aligned">{{ rupiah($kusen?$kusen:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan sipil</td>
                            <td class="right aligned">{{ rupiah($data->i_tiga_a + $data->i_tiga_b + $data->i_tiga_c + $data->i_tiga_d + $data->i_tiga_e + $data->i_tiga_f) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Folding Gate + Polycarbonate = Teras</td>
                            <td class="right aligned">{{ rupiah($folding?$folding:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" ></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Instalasi Listrik (lampu, regrouping)</td>
                            <td class="right aligned">{{ rupiah($instalasi?$instalasi:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;"> Peralatan Elektonik</td>
                            <td class="right aligned">{{ rupiah($data->i_empat_a + $data->i_empat_c) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Instalasi Pemasangan AC</td>
                            <td class="right aligned">{{ rupiah($pemasangan?$pemasangan:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Pekerjaan Signage (Lisping Papan Nama Toko) Repainting</td>
                            <td class="right aligned">{{ rupiah($signage?$signage:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan Non-Elektronik</td>
                            <td class="right aligned">{{ rupiah($data->i_empat_b) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan AC Sipil</td>
                            <td class="right aligned">{{ rupiah($sipil?$sipil:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan Non Elektronik (Rak, Furniture, Acrylic, dll)</td>
                            <td class="right aligned">{{ rupiah($non_elektronik?$non_elektronik:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Startup & Promosi opening</td>
                            <td class="right aligned">{{ rupiah($data->i_satu) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Peralatan Eletronika (Komputer, Cooler & Timbangan)</td>
                            <td class="right aligned">{{ rupiah($elektronik?$elektronik:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned"></td>
                            <td class="center aligned"></td>
                            <td class="left aligned" style="padding-left: 50px;">Stock Awal</td>
                            <td class="right aligned">{{ rupiah($stock_awal?$stock_awal:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Modal Kerja</td>
                            <td class="right aligned">{{ rupiah($data->i_lima_b) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Modal Kerja 1 bulan (ditransfer ke rek Ops TMUK)</td>
                            <td class="right aligned">{{ rupiah($modal_kerja?$modal_kerja:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="left aligned" style="padding-left: 50px;">Lain-lain</td>
                            <td class="right aligned">{{ rupiah($data->i_bila_ada_biaya_tambahan) }}</td>
                            <td class="left aligned" style="padding-left: 50px;">Lain-lain</td>
                            <td class="right aligned">{{ rupiah($lain_lain?$lain_lain:'0') }}</td>
                        </tr>
                        <tr>
                            <td class="center aligned"></td>
                            <?php $total_perkiraan = $data->i_lima_a + $data->i_tiga_a + $data->i_tiga_b + $data->i_tiga_c + $data->i_tiga_d + $data->i_tiga_e + $data->i_tiga_f + $data->i_empat_a + $data->i_empat_c + $data->i_empat_b + $data->i_satu + $data->i_lima_b + $data->i_bila_ada_biaya_tambahan; ?>
                            <td class="right aligned"><b>{{ rupiah($total_perkiraan) }}</b></td>
                            <td class="center aligned"></td>
                            <?php $total_pengeluaran = $start + $perijinan + $renovasi + $kusen + $folding + $instalasi + $pemasangan + $signage + $sipil + $non_elektronik + $elektronik + $stock_awal + $modal_kerja + $lain_lain; ?>
                            <td class="right aligned"><b>{{ rupiah($total_pengeluaran) }}</b></td>
                        </tr>
                    
                </tbody>
                <tfoot>
                    <tr>
                        <th class="center aligned"></th>
                        <?php $total_rab = $totaldana - $total_perkiraan; ?>
                        <th class="right aligned"><b>{{ rupiah($total_rab) }}</b></th>
                        <th class="center aligned"></th>
                        <?php $total_realisasi = $sum_tot - $total_pengeluaran; ?>
                        <th class="right aligned"><b>{{ rupiah($total_realisasi) }}</b></th>
      
                    </tr>
                </tfoot>
            </table>
        @endforeach
    @else
        <div class="ui segment">
          <p align="center">Maaf tidak ada data</p>
        </div>
    @endif
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        var nowDate = new Date();
        var today   = new Date(nowDate.getFullYear(), nowDate.getMonth() + 1, 0, 0, 0, 0);
        
        $('#from').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){ 
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 2;
                var year = date.getFullYear();
                    if (month > 12) {
                        month = month - 12;
                        year = year + 1;
                    }
                $('#to').calendar("set date", new Date(year,month));
                // $('#selesai_filter').val(month+'/'+year);
            }
        })

        $('#to').calendar({
            type: 'month',
            maxDate: today,
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    return month+'/'+year;
                }
            },
            onChange(date, text, mode){
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() - 2;
                var year = date.getFullYear();
                    if (month < 0) {
                        month = month + 12;
                        year = year - 1;
                    }

                // $('#from').calendar("set date", new Date(year,month));
                $('#mulai_filter').val((month+1)+'/'+year);
            }
        })
    });
</script>