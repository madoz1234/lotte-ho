<div class="title" style="font-size: 17px;">NERACA</div><br>

@if(isset($records))
@foreach($records as $record)
    @if(!isset($record['header']))
        <div class="ui segment">
          <p align="center">Filter tanggal terlebih dahulu</p>
        </div>
    @else
        <table  class="ui celled compact red table" width="90%" cellspacing="0">
            <thead>
                <tr>
                    <th class="center aligned" width="40px">Description</th>
                    @foreach($record['header'] as $row)
                        <th class="center aligned" width="40px">{{ $row }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($record['content'] as $row)
                    @if($row['type'] == 'label')
                        @if(isset($row['child']))
                            <tr>
                                <td class="left aligned" colspan="{{ count($record['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                            </tr>
                            @foreach($row['child'] as $rowchild)
                                <tr>
                                    <td class="left aligned">&nbsp;&nbsp;&nbsp;&nbsp;{{ $rowchild['text'] }}</td>
                                    @foreach($rowchild['data'] as $val)
                                        <td class="right aligned">{{ curencyformat($val) }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="left aligned" colspan="{{ count($record['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                            </tr>
                        @endif
                    @else
                        <tr>
                            <td class="left aligned"><b>{{ $row['text'] }}</b></td>
                            @foreach($row['data'] as $val)
                                <td class="right aligned"><b>{{ curencyformat($val) }}</b></td>
                            @endforeach
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    @endif
@endforeach
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif
