<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        responsive: true,
        autoWidth: false,
        ordering: false,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">LAPORAN PIUTANG TMUK</div><br>

<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="20px">#</th>
            <th class="center aligned" width="20px">Tmuk</th>
            <th class="center aligned" width="20px">Tanggal</th>
            <th class="center aligned" width="20px">Deskripsi</th>
            <th class="center aligned" width="20px">Debit</th>
            <th class="center aligned" width="20px">Kredit</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; $total_debit = 0; $total_kredit = 0;?>
        @foreach ($records as $row)
        <tr>
            <td class="center aligned">{{ $i }}</td>
            <td class="center aligned">{{ $row->tmuk->nama }}</td>
            <td class="center aligned">{{ \Carbon\Carbon::parse($row->tanggal)->format('d/m/Y') }}</td>
            <td class="left aligned">{{ ucfirst($row->deskripsi) }}</td>
            <td class="right aligned">{{ number_format($row->posisi=='D' ? $row->jumlah : '0') }}</td>
            <td class="right aligned">{{ number_format($row->posisi=='K' ? $row->jumlah : '0') }}</td>
            {{-- <td class="right aligned">{{ number_format($row->posisi=='D' ? $total_debit += $row->jumlah : '0') }}</td>
            <td class="right aligned">{{ number_format($row->posisi=='K' ? $total_kredit += $row->jumlah : '0') }}</td> --}}
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>
</table>