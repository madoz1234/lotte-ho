<style type="text/css">

    .scroll_imp{
        width: 100% !important;
        border:1px solid #D2F2E6;
        min-height:150mm!important;
        font-size: 12px!important;
        max-height: 150mm !important;
    }
</style>
<div class="title" style="font-size: 17px;">LABA / RUGI (Multi Periode)</div><br>

@if(isset($records))
<div id="scroll" class="scroll scroll_imp">
    <div class="scrollDiv" >
        <div class="scrollContent">
            @foreach ($records as $record)
                <table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="center aligned" width="40px">Description</th>
                            @foreach($record['header'] as $row)
                                <th class="center aligned" width="40px">{{ $row }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($record['content'] as $row)
                            @if($row['type'] == 'label')
                                @if(isset($row['child']))
                                    <tr>
                                        <td class="left aligned" colspan="{{ count($record['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                                    </tr>
                                    @foreach($row['child'] as $rowchild)
                                        <tr>
                                            <td class="left aligned">&nbsp;&nbsp;&nbsp;&nbsp;{{ $rowchild['text'] }}</td>
                                            @foreach($rowchild['data'] as $val)
                                                <td class="right aligned">{{ curencyformat($val) }}</td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="left aligned" colspan="{{ count($record['header']) + 1 }}"><b>{{ $row['text'] }}</b></td>
                                    </tr>
                                @endif
                            @else
                                <tr>
                                    <td class="left aligned"><b>{{ $row['text'] }}</b></td>
                                    @foreach($row['data'] as $val)
                                        <td class="right aligned"><b>{{ curencyformat($val) }}</b></td>
                                    @endforeach
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            @endforeach
        </div>
    </div>
</div>
@else
    <div class="ui segment">
      <p align="center">Filter tanggal terlebih dahulu</p>
    </div>
@endif