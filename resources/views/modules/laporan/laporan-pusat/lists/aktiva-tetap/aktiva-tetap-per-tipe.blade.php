<script>
    var dt_table = $('#reportTable').DataTable({
        dom: 'rt<"bottom"ip><"clear">',
        destroy: true,
        // retrieve: true,
        // paging: false,
        responsive: true,
        autoWidth: false,
        ordering: false,
        processing: true,
        serverSide: true,
        lengthChange: true,
        pageLength: 10,
        filter: false,
        sorting: [],
        language: {
            url: "{{ asset('plugins/datatables/Indonesian.json') }}"
        },
        ajax:  {
            url: "{{ url($pageUrl) }}/grid-all",
            type: 'POST',
            data: function (d) {
                d._token        = "{{ csrf_token() }}";
                d.types         = $("[name=types]").val();
                d.status        = "{{ $lists_grid }}";
                d.region_id     = $('[name="region_id"]').val();
                d.lsi_id        = $('[name="lsi_id"]').val();
                d.tmuk_kode     = $('[name="filter[tmuk_kode]"]').val();
                d.tanggal_start = $('#mulai_filter').val();
                d.tanggal_end   = $('#selesai_filter').val();
            }
        }, 
        columns: [
            {
              "title": "#",
              "data": "num"
            },
            {
              "title": "Nama Aktiva",
              "data": "nama"
            },         
            {
              "title": "Harga Perolehan",
              "data": "jumlah",
              "className" : "dt-right"
            },
            {
         
              "title": "Akumulasi Depresiasi",
              "data": "akumulasi",
            },
            {
         
              "title": "Book Value",
              "data": "book",
              "className" : "dt-center"
            },
            {
         
              "title": "Depresiasi Tahun Ini",
              "data": "depresi",
              "className" : "dt-center"
            },
            {
         
              "title": "Tanggal Pembelian",
              "data": "tanggal",
              "className" : "dt-center"
            },
        ],
        drawCallback: function() {
            var api = this.api();
            api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              // console.log(cell.innerHTML);
                cell.innerHTML = (parseInt(cell.innerHTML)+(i+1));
            });
        }
    }); 
</script>
<div class="title" style="font-size: 17px;">Rekap Aktiva Tetap per Tipe </div><br>
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th class="center aligned">Trans Type</th>
            <th class="center aligned">Trans Type</th>
            <th class="center aligned">Date</th>
            <th class="center aligned">Due ate</th>
            <th class="center aligned">Charges</th>
            <th class="center aligned">Credits</th>
        </tr>
    </thead>
</table>
