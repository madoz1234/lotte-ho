@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/scroll-bar/css/rocketScroll.css') }}" />
<link rel="stylesheet" href="{{ asset('plugins/scroll-bar/css/demo.css') }}" />
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
<script src="{{ asset('plugins/scroll-bar/js/rocketScroll.js') }}"></script>
<script src="{{ asset('plugins/scroll-bar/js/demo.js') }}"></script>
@append

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		postNewTab = function(url, param)
		{
			var form = document.createElement("form");
			form.setAttribute("method", 'POST');
			form.setAttribute("action", url);
			form.setAttribute("target", "_blank");

			$.each(param, function(key, val) {
				var inputan = document.createElement("input");
				inputan.setAttribute("type", "hidden");
				inputan.setAttribute("name", key);
				inputan.setAttribute("value", val);
				form.appendChild(inputan);
			});

			document.body.appendChild(form);
			form.submit();

			document.body.removeChild(form);
		}

	loads_table = function(v=''){
		$.ajax({
			type: 'POST',
			url: '{{ url($pageUrl) }}/grid-temp',
			data: {
				_token: "{{ csrf_token() }}",
				types  	 	 : $("[name=types]").val(),
				status  	 : v,
				@if(isset($view_list))
					region_id 	 : $('[name="region_id"]').val(),
					lsi_id 	 	 : $('[name="lsi_id"]').val(),
					tmuk_kode 	 : $('[name="filter[tmuk_kode]"]').val(),
					tanggal_start: $('#mulai_filter').val(),
					tanggal_end  : $('#selesai_filter').val(),
					coa  : $('[name="filter[coa]"]').val(),
				@endif
			},
		})
		.done(function(response) {
		    $('.twelve.wide.column').html(response);
		})
		.fail(function(response) {
			console.log("error");
			// alert('tara');
		});
	}
	loads_table();

		//FILTER TABLE
	@if(isset($view_list))
		filter = function(v, menu=''){
			loads_table();
		};
	@endif

		printLaporan = function(){
	    	// alert('tara');
	    	var url = '{{ url($pageUrl) }}/print';

	    	postNewTab(url, {
	    		_token 		: "{{ csrf_token() }}",
				types  	     : $("[name=types]").val(),
				region_id 	 : $('[name="region_id"]').val(),
				lsi_id 	 	 : $('[name="lsi_id"]').val(),
				tmuk_kode 	 : $('[name="filter[tmuk_kode]"]').val(),
				tanggal_start: $('#mulai_filter').val(),
				tanggal_end  : $('#selesai_filter').val(),
				coa  : $('[name="filter[coa]"]').val(),
	    	});
	    }

		printExcel = function(){
	    	// alert('tara');
	    	var url = '{{ url($pageUrl) }}/print-excel';

	    	postNewTab(url, {
	    		_token 		 : "{{ csrf_token() }}",
				types  	     : $("[name=types]").val(),
				region_id 	 : $('[name="region_id"]').val(),
				lsi_id 	 	 : $('[name="lsi_id"]').val(),
				tmuk_kode 	 : $('[name="filter[tmuk_kode]"]').val(),
				tanggal_start: $('#mulai_filter').val(),
				tanggal_end  : $('#selesai_filter').val(),
				coa  : $('[name="filter[coa]"]').val(),
	    	});
	    }


		$('.treeview.accordion')
		.accordion({
			selector: {
				trigger: '.title'
			}
		});

		$(".treeview.accordion").click(function(event) {
			$('.title').removeAttr('style');
			$('.title.active').css("font-weight","Bold");
		});

		var dt = $('#reportTable').DataTable({
			dom: 'rt<"bottom"ip><"clear">',
		});

		$("#searchbox").keyup(function() {
			dt.search(this.value).draw();
		});

		$('.ui.calendars').calendar({
			type: 'text',
			formatDate: 'yyyy-mm-dd'
		});

		$(document).on('click', '#reset_tmuk', function(e){
			// $('#menu').addClass('hidden');
			$('.lsi').addClass('disabled');
			$('#lsi_id').html('<option value="0">-- Pilih LSI --</option>');
			$('.tmuk').addClass('disabled');
			$('#tmuk').html('<option value="0">-- Pilih TMUK --</option>');
			// $('#alert').removeClass('hidden');

		    $('.twelve.wide.column').html('');
		    $('.title').removeClass('active');
		    $('.content').removeClass('active');
		    $(".title").removeAttr("style");
		});
	});

	$(document).ready(function(e){
		$('[name=region_id]').change(function(e){
			$(".loading").addClass('active');
			$.ajax({
				url: "{{ url('ajax/option/region') }}",
				type: 'GET',
				data:{
					id_region : $(this).val(),
				},
			})
			.success(function(response) {
				$(".loading").removeClass('active');

				$('.lsi').removeClass('disabled');
				$('[name=lsi_id]').html(response);
			})
			.fail(function() {
				console.log("error");
				$(".loading").removeClass('active');
			});
		});


		$('[name=lsi_id]').change(function(e){
			//generate produk by lsi
			$(".loading").addClass('active');

			$.ajax({
				url: "{{ url('ajax/option/lsi') }}",
				type: 'GET',
				data:{
					id_lsi : $(this).val(),
				},
			})
			.success(function(response) {
				$(".loading").removeClass('active');

				$('.kopro').removeClass('disabled');
				$('.btn-delete').removeClass('disabled');
				$('.btn-add').removeClass('disabled');
				$('#trigger_kode_produk_x').html(response);
				$('#trigger_kode_produk_y').html(response);
				$('#trigger_purchase_kode_produk').html(response);
			})
			.fail(function() {
				console.log("error");
			});

			//generate tmuk by lsi
			$.ajax({
				url: "{{ url('ajax/option/lsi-tmuk') }}",
				type: 'GET',
				data:{
					id_lsi : $(this).val(),
				},
			})
			.success(function(response) {
				$('.tmuk').removeClass('disabled');
				$('#tmuk').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

	});

	showTable = function(v, menu='' , hidden = ''){
		$("[name=types]").val(v);
		loads_table();
		$('.pm_date_end').removeClass('hidden');
	    $('#print_pdf').removeClass('hidden');
	    $('#print_excel').removeClass('hidden');
		switch(hidden) {
		    case 'priode':
		        $('.pm_date_end').addClass('hidden');
		        break;
		    case 'print-excel':
		        $('#print_excel').addClass('hidden');
		        break;
		    case 'print-pdf':
		        $('#print_pdf').addClass('hidden');
		        break;

		} 
		
	}
@if(isset($months))
	$('#from').calendar({
        type: 'month',
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return month+'/'+year;
            }
        },
        onChange(date, text, mode){ 
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 2;
            var year = date.getFullYear();
                if (month > 12) {
                    month = month - 12;
                    year = year + 1;
                }
            $('#to').calendar("set date", new Date(year,month));
            // $('#selesai_filter').val(month+'/'+year);
        }
    })

    $('#to').calendar({
        type: 'month',
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return month+'/'+year;
            }
        },
        onChange(date, text, mode){
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() - 2;
            var year = date.getFullYear();
                if (month < 0) {
                    month = month + 12;
                    year = year - 1;
                }

            // $('#from').calendar("set date", new Date(year,month));
            $('#mulai_filter').val((month+1)+'/'+year);
        }
    })
@else
	$('.pm_date_start').calendar({
        type: 'date',
        endCalendar: $('.pm_date_end'),
        formatter: {
         date: function (date, settings)
            {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day+'/'+month+'/'+year;
             }
         }
    })

    $('.pm_date_end').calendar({
        type: 'date',
        startCalendar: $('.pm_date_start'),
        formatter: {
         date: function (date, settings)
            {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day+'/'+month+'/'+year;
             }
         }
    })
@endif
    filter = function(v, menu=''){
        // alert('tara');
        dt_table.draw();
    };

</script>
@append

@section('js-filters')
d.region_kode = $("select[name='filter[region_kode]']").val();
d.lsi_kode = $("select[name='filter[lsi_kode]']").val();
d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
@endsection

@section('content-body')
<div class="ui grid">
		<div class="sixteen wide column main-content">
		    <div class="ui segments">
				<input name="types" type="hidden" value="{{ isset($types)?$types:'' }}" placeholder="Tanggal Selesai">
				<div class="ui segment">
					<form class="ui filter form">
		  				<div class="fields">
		  					<div class="field" style="width: 15%;">
								<select name="region_id" class="ui fluid selection dropdown">
									{!! \Lotte\Models\Master\Region::options('area', 'id',[], '-- Pilih Region --') !!}
								</select>
							</div>
							<div class="field" style="width: 15%;">
								<select name="lsi_id" class="ui fluid selection dropdown lsi disabled" id="lsi_id">
								<option value="0">-- Pilih LSI --</option>
								</select>
							</div>
							<div class="field" style="width: 15%;">
								<select name="filter[tmuk_kode]" class="ui fluid dropdown tmuk disabled" id="tmuk">
									<option value="0">-- Pilih TMUK --</option>
								</select>
							</div>

							@if(isset($months))
								<div class="field inline" id="months">
								    <div class="ui calendar labeled input pm_month_start" id="from">
								        <div class="ui input left icon">
								            <i class="calendar icon date"></i>
								            <input name="filter[month_start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Bulan Mulai">
								        </div>
								    </div>
								    <div class="ui calendar labeled input pm_month_end {{ isset($priode)? 'hidden':'' }}" id="to">
								   		s.d.&nbsp;
								        <div class="ui input left icon">
								            <i class="calendar icon date"></i>
								            <input name="filter[month_end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Bulan Selesai">
								        </div>
								    </div>
								</div>
							@else
								<div class="field inline" id="tanggals">
								    <div class="ui calendar labeled input pm_date_start">
								        <div class="ui input left icon">
								            <i class="calendar icon date"></i>
								            <input name="filter[start]" id="mulai_filter" type="text" value="{{ $start or '' }}" placeholder="Tanggal Mulai">
								        </div>
								    </div>
								    <div class="ui calendar labeled input pm_date_end {{ isset($priode)? 'hidden':'' }}">
								   		s.d.&nbsp;
								        <div class="ui input left icon">
								            <i class="calendar icon date"></i>
								            <input name="filter[end]" id="selesai_filter" type="text" value="{{ $end or '' }}" placeholder="Tanggal Selesai">
								        </div>
								    </div>
								</div>
							@endif



							<button type="button" class="ui teal icon filter button" id="filter_tmuk" data-content="Cari Data" onclick="filter();">
								<i class="search icon"></i>
							</button>
							<button type="reset" class="ui icon reset button" id="reset_tmuk" data-content="Bersihkan Pencarian">
								<i class="refresh icon"></i>
							</button>
							<button type="button" onclick="printLaporan();" id="print_pdf" class="ui green icon button" id="print" data-content="Print PDF">
								<i class="file icon"></i>
							</button>
							<button type="button" onclick="printExcel();" id="print_excel" class="ui orange icon button" id="print" data-content="Print EXCEL">
								<i class="file icon"></i>
							</button>
						</div>
					</form>

					<div class="ui clearing segment" id="menu">
						<div class="ui divided grid">
							<div class="four wide column" style="position: relative">
								@include('modules.laporan.laporan-pusat.menu.menu-'.$menus)
							</div>
							<div class="twelve wide column">
							</div>
						</div>
					</div>
		        </div>
		    </div>
		</div>
	</div>
@endsection
