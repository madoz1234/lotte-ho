@extends('layouts.grid')

@section('filters')
<div class="field" style="width: 15%;">
	<select name="region_id" class="ui fluid selection dropdown">
		{!! \Lotte\Models\Master\Region::options('area', 'id',[], '-- Pilih Region --') !!}
	</select>
</div>
<div class="field" style="width: 28%;">
	<select name="lsi_id" class="ui fluid selection dropdown lsi disabled" id="lsi_id">
		<option value="">-- Pilih LSI --</option>
	</select>
</div>
<div class="field" style="width: 15%;">
	<select name="tmuk_kode" class="ui fluid dropdown tmuk disabled" id="tmuk">
		<option value="">-- Pilih TMUK --</option>
	</select>
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
d.region = $("select[name='region_id']").val();
d.lsi = $("select[name='lsi_id']").val();
d.tmuk = $("select[name='tmuk_kode']").val();
@endsection

@section('toolbars')
<button type="button" class="ui button export-pdf">
	<i class="print icon"></i>
	Export PDF
</button>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
		$('.demo.menu .item').tab({
			history:false,
		});
	};
</script>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(e){
		$('[name=region_id]').change(function(e){
			$(".loading").addClass('active');
			$.ajax({
				url: "{{ url('ajax/option/region') }}",
				type: 'GET',
				data:{
					id_region : $(this).val(),
				},
			})
			.success(function(response) {
				$(".loading").removeClass('active');

				$('.lsi').removeClass('disabled');
				$('[name=lsi_id]').html(response);
			})
			.fail(function() {
				console.log("error");
				$(".loading").removeClass('active');
			});
		});


		$('[name=lsi_id]').change(function(e){
					//generate produk by lsi
					$(".loading").addClass('active');

					$.ajax({
						url: "{{ url('ajax/option/lsi') }}",
						type: 'GET',
						data:{
							id_lsi : $(this).val(),
						},
					})
					.success(function(response) {
						$(".loading").removeClass('active');

						$('.kopro').removeClass('disabled');
						$('.btn-delete').removeClass('disabled');
						$('.btn-add').removeClass('disabled');
						$('#trigger_kode_produk_x').html(response);
						$('#trigger_kode_produk_y').html(response);
						$('#trigger_purchase_kode_produk').html(response);
					})
					.fail(function() {
						console.log("error");
					});

					//generate tmuk by lsi
					$.ajax({
						url: "{{ url('ajax/option/lsi-tmuk') }}",
						type: 'GET',
						data:{
							id_lsi : $(this).val(),
						},
					})
					.success(function(response) {
						$('.tmuk').removeClass('disabled');
						$('#tmuk').html(response);
					})
					.fail(function() {
						console.log("error");
					});
				});

	});
	postNewTab = function(url, param)
	{
		var form = document.createElement("form");
		form.setAttribute("method", 'POST');
		form.setAttribute("action", url);
		form.setAttribute("target", "_blank");

		$.each(param, function(key, val) {
			var inputan = document.createElement("input");
			inputan.setAttribute("type", "hidden");
			inputan.setAttribute("name", key);
			inputan.setAttribute("value", val);
			form.appendChild(inputan);
		});

		document.body.appendChild(form);
		form.submit();

		document.body.removeChild(form);
	}
	$(document).on('click', '.export-pdf', function(e){
		var regions = $("select[name='region_id']").val();
		var lsis = $("select[name='lsi_id']").val();
		var tmuks = $("select[name='tmuk_kode']").val();
		var url = '{{ url($pageUrl) }}/cetak';

		postNewTab(url, {
			_token 		: "{{ csrf_token() }}",
			region  	: regions,
			lsi  		: lsis,
			tmuk  		: tmuks,
		});
	});
</script>
@append

@section('subcontent')
@parent
@endsection