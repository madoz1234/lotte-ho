@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<div class="ui calendar">
			<div class="ui input left icon">
				<i class="calendar icon date"></i>
				<input name="filter[created_at]" type="text" placeholder="Tanggal">
			</div>
		</div>
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection
@section('toolbars')
@endsection

@section('js-filters')
    d.created_at = $("input[name='filter[created_at]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		kode: {
			identifier: 'kode',
			rules: [{
				type   : 'empty',
				prompt : 'Isian kode tidak boleh kosong'
			}]
		}
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$('.ui.calendar').calendar({
		  type: 'date'
		});
		$(document).ready(function() {
			//
		});
	</script>
@append