@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[nama]" placeholder="Nama Pengguna" type="text">
	</div>
	<div class="field" style="width: 25%">
		<select name="filter[lsi]" class="ui search dropdown">
			{!! \Lotte\Models\Master\Lsi::options('nama', 'id', [], '-- Pilih LSI --') !!}
		</select>
	</div>

	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

	@section('toolbars')
	<div class="ui blue buttons" style="margin-right: 5px">
		<button type="button" class="ui blue add button"><i class="plus icon icon"></i>Tambah Pengguna</button>
	</div>
	@endsection

@section('js-filters')
    d.name = $("input[name='filter[nama]']").val();
    d.lsi = $("select[name='filter[lsi]']").val();
@endsection

	@section('rules')
	<script type="text/javascript">
		formRules = {
		};
	</script>
	@endsection

	@section('init-modal')
	<script type="text/javascript">
		initModal = function(){
			//radio checkbox
			$('.ui.radio.checkbox').checkbox();

			$('.demo.menu .item').tab({
				history:false,
			});

			$('.hak-akses').dropdown({
				onChange: function(item){
					$.each( item, function( key, value ) {
			    		// 11 = cso , 12 = manager-cso, 13 = director-cso, 14 = finance
					    if (value == 11 || value == 12 || value == 13 | value == 14) {
					    	$('.ttd').removeClass('hidden');
					    }else{
					    	$('.ttd').addClass('hidden');
					    }
					});
				}
			})
		};
	</script>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>

</script>
@append