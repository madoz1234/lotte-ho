<script type="text/javascript">
	$(document).ready(function() {
		$('[name="role[]"]').val({!! json_encode($checked) !!});
	});
</script>

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Pengguna</div>
<div class="content">
	<form method="POST" class="ui data form" action="{{ url($pageUrl.$record->id) }}" id="dataForm">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui error message"></div>
		<div class="field">
			<label>Nama Lengkap</label>
			<input name="nama_lengkap" placeholder="Inputkan Nama Lengkap" type="text" value="{{ $record->nama_lengkap }}">
		</div>

		<div class="two fields">
			<div class="field">
				<label>Telepon</label>
				<input name="telepon" placeholder="Inputkan No.Telpon" type="text" value="{{ $record->telepon }}">
			</div>

			<div class="field">
				<label>Email</label>
				<input name="email" placeholder="Inputkan Alamat Email" type="email" value="{{ $record->email }}">
			</div>
		</div>

		<div class="field">
			<label>Lokasi</label>
			<select  name="lsi_code" class="ui search dropdown ts">
				<option class="item" value="">-- Pilih --</option>
				@foreach ($lsi_options as $elm)
				<option class="item" value="{{ $elm->id }}" {{ $elm->id==$record->lsi_id? 'selected':'' }}>{{ $elm->kode }} - {{ $elm->nama }}</option>
				@endforeach
			</select>
		</div>
		<br>
		<div class="field">
			<h4 class="ui dividing header">Data Untuk Login</h4>
			<div class="two fields">
				<div class="field">
					<label>Hak Akses</label>
					<select name="role[]" class="ui fluid search dropdown ts hak-akses" id="hak-akses" multiple="">
						@foreach ($hak_aksess_options as $elm)
						<option class="item" value="{{ $elm->id }}">{{ $elm->display_name }}</option>
						@endforeach
					</select>
				</div>
				<div class="field {{ ($has_role) ? '': 'hidden'}} ttd">
					<label>Tanda Tangan</label>
					<input type="file" name="ttd">
				</div>
			</div>

			{{-- <div class="field">
				<label>Password</label>
				<input name="password" placeholder="****" type="password">
			</div>

			<div class="field">
				<label>Konfirmasi Password</label>
				<input name="konf_password" placeholder="****" type="password">
			</div> --}}
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>