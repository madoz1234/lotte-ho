<script type="text/javascript">
	checks = function(){
		console.log(this);
	};
	$('.checkboxs').on('click', function(){
			var val = $(this).data('headers');
			// alert(data);
			var but_form = true;
			var datas = $.parseJSON($(this).data('button'));
			console.log(datas);
			
			if(datas == true){
				but_form = false;
				$(this).data('button', false);
			}else{
				but_form = true;
				$(this).data('button', true);
			}

			$('.child'+val).data('button', but_form);
			$('.child'+val).prop('checked', datas);
			$('.childern'+val).prop('checked', datas);
	});

</script>
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>

<div class="header">Tambah Data Hak Akses</div>
<div class="content">
	<form method="POST" class="ui data form" action="{{ url($pageUrl) }}" id="dataForm">
		{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>Nama Role</label>
				<input name="name" placeholder="Nama Role" type="text">
			</div>
			<div class="field">
				<label>Deskripsi</label>
				<input name="description" placeholder="Deskripsi" type="text">
			</div>
		</div>
		@foreach ($record as $elm)
		<div class="row" style="margin-bottom: 10px;">
			<div class="ui segments">
			  <div class="ui secondary segment">
			  	<div class="ui grid">
			  		<div class="eight wide column">
			  			<b>{{ $elm['display_name'] }}</b>
			  		</div>
			  		<div class="eight wide column">	  			
						<div class="ui checked checkbox" style=";float: right;">
							<input type="checkbox" name="role[]" class="checkboxs" data-button='true' data-headers="{{ $elm['id'] }}" value="{{ $elm['id'] }}">
							<label></label>
						</div>
			  		</div>
			  	</div>
			  </div>
			  <div class="ui segment">
			  	<div class="ui grid">
			    <div class="three column row">
					@foreach ($elm['child'] as $key => $element)
				    	<div class="column">
							<table class="ui compact celled table" style="margin-bottom: 10px;">
								<thead class="full-width">
									<tr>
										<th>{{ $element['display_name'] }}</th>
										<th class="collapsing">
										<div class="ui checked checkbox">
										<input type="checkbox" class="child{{ $elm['id'] }} checkboxs" data-button='true' data-headers="{{ $element['id'] }}" name="role[]" value="{{ $element['id'] }}">
										<label></label>
										</div>
										</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($element['child'] as $elements)
									<tr>
										<td>{{ $elements['display_name'] }}</td>
										<td>
										<div class="ui checked checkbox">
										<input type="checkbox" name="role[]" class="childern{{ $element['id'] }} child{{ $elm['id'] }}" value="{{ $elements['id'] }}">
										<label></label>
										</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					@endforeach
			    </div>
		    </div>
		  </div>
		</div>
		</div>
	@endforeach
	</form>
<!-- Hak Akses -->
</div>

<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>