@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[nama]" placeholder="Nama Role" type="text">
	</div>

	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

	@section('toolbars')
	<div class="ui blue buttons" style="margin-right: 5px">
		<button type="button" class="ui blue add button"><i class="plus icon icon"></i>Tambah Hak Akses</button>
	</div>
	@endsection

@section('js-filters')
    d.name = $("input[name='filter[nama]']").val();
@endsection

	@section('rules')
	<script type="text/javascript">
		formRules = {

		};
	</script>
	@endsection

	@section('init-modal')
	<script type="text/javascript">
		initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
		$('.demo.menu .item').tab({
			history:false,
		});
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
		$('.demo.menu .item').tab({
			history:false,
		});
	};
</script>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>

</script>
@append