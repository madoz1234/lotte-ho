@extends('layouts.scaffold')

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.demo.menu .item').tab({history:false});
	});
</script>
@append

@section('content')

@section('content-header')
<div class="ui breadcrumb">
	<?php $i=1; $last=count($breadcrumb);?>
	@foreach ($breadcrumb as $name => $link)
	@if($i++ != $last)
	<a href="{{ $link }}" class="section">{{ $name }}</a>
	<i class="right chevron icon divider"></i>
	@else
	<div class="active section">{{ $name }}</div>
	@endif
	@endforeach
</div>
<h2 class="ui header">
	<div class="content">
		{!! $title or '-' !!}
		<div class="sub header">{{ $subtitle or ' ' }}</div>
	</div>
</h2>
@show
@section('content-body')
	<div class="ui grid">
		<div class="sixteen wide column main-content">
		    <div class="ui segments">
				<div class="ui segment">
					<div class="ui centered grid">
	<div class="sixteen wide column">
		{{-- <div class="ui segments fluid"> --}}
			<div class="ui top attached tabular demo menu">
				<a class="active item" data-tab="first">Persetujuan 1</a>
				<a class="item" data-tab="second">Persetujuan 2</a>
				<a class="item" data-tab="third">Persetujuan 3</a>
				<a class="item" data-tab="four">Persetujuan 4</a>
			</div>

			{{-- pertama --}}
			<div class="ui active tab segment" data-tab="first">
				<div class="content">
					<table class="ui celled striped table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Email</th>
								<th>No Telpon</th>
								<th>Hak Akses</th>
								<th>Ttd</th>
							</tr>
						</thead>
						<tbody>
							@if($persetujuan1->count() > 0)
							<?php $i = 0; ?>
							@foreach($persetujuan1 as $row)
							<tr>
								<td>{{ $i+1 }}</td>
								<td>{{ $row->nama_lengkap}}</td>
								<td>{{ $row->email }}</td>
								<td>{{ $row->telepon }}</td>
								<td>CSO</td>
								<td><img src="{{ isset($row->ttd) ? url('storage/'.$row->ttd) : asset('img/no_image.png') }}" style="width: 20%;"></td>
							</tr>
							<?php $i++;	?>
							@endforeach
							@else
							<tr>
								<td colspan="6">
									<center><i>Maaf tidak ada data</i></center>
								</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
			{{-- pertama --}}

			{{-- kedua --}}
			<div class="ui tab segment" data-tab="second">
				<div class="content">
					<table class="ui celled striped table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Email</th>
								<th>No Telpon</th>
								<th>Hak Akses</th>
								<th>Ttd</th>
							</tr>
						</thead>
						<tbody>
							@if($persetujuan2->count() > 0)
							<?php $i = 0; ?>
							@foreach($persetujuan2 as $row)
							<tr>
								<td>{{ $i+1 }}</td>
								<td>{{ $row->nama_lengkap}}</td>
								<td>{{ $row->email }}</td>
								<td>{{ $row->telepon }}</td>
								<td>Manager CSO</td>
								<td><img src="{{ isset($row->ttd) ? url('storage/'.$row->ttd) : asset('img/no_image.png') }}" style="width: 20%;"></td>
							</tr>
							<?php $i++;	?>
							@endforeach
							@else
							<tr>
								<td colspan="6">
									<center><i>Maaf tidak ada data</i></center>
								</td>
							</tr>
							@endif
						</tbody>
						</tbody>
					</table>
				</div>
			</div>
			{{-- kedua --}}

			{{-- ketiga --}}
			<div class="ui tab segment" data-tab="third">
				<div class="content">
					<table class="ui celled striped table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Email</th>
								<th>No Telpon</th>
								<th>Hak Akses</th>
								<th>Ttd</th>
							</tr>
						</thead>
						<tbody>
							@if($persetujuan3->count() > 0)
							<?php $i = 0; ?>
							@foreach($persetujuan3 as $row)
							<tr>
								<td>{{ $i+1 }}</td>
								<td>{{ $row->nama_lengkap}}</td>
								<td>{{ $row->email}}</td>
								<td>{{ $row->telepon }}</td>
								<td>Director CSO</td>
								<td><img src="{{ isset($row->ttd) ? url('storage/'.$row->ttd) : asset('img/no_image.png') }}" style="width: 20%;"></td>
							</tr>
							<?php $i++;	?>
							@endforeach
							@else
							<tr>
								<td colspan="6">
									<center><i>Maaf tidak ada data</i></center>
								</td>
							</tr>
							@endif
						</tbody>
						</tbody>
					</table>
				</div>
			</div>
			{{-- ketiga --}}

			{{-- keempat --}}
			<div class="ui tab segment" data-tab="four">
				<div class="content">
					<table class="ui celled striped table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Email</th>
								<th>No Telpon</th>
								<th>Hak Akses</th>
								<th>Ttd</th>
							</tr>
						</thead>
						<tbody>
							@if($persetujuan4->count() > 0)
							<?php $i = 0; ?>
							@foreach($persetujuan4 as $row)
							<tr>
								<td>{{ $i+1 }}</td>
								<td>{{ $row->nama_lengkap}}</td>
								<td>{{ $row->email }}</td>
								<td>{{ $row->telepon }}</td>
								<td>Finance</td>
								<td><img src="{{ isset($row->ttd) ? url('storage/'.$row->ttd) : asset('img/no_image.png') }}" style="width: 20%;"></td>
							</tr>
							<?php $i++;	?>
							@endforeach
							@else
							<tr>
								<td colspan="6">
									<center><i>Maaf tidak ada data</i></center>
								</td>
							</tr>
							@endif
						</tbody>
						</tbody>
					</table>
				</div>
			</div>
			{{-- keempat --}}

		{{-- </div> --}}
	</div>
</div>
		        </div>
		    </div>
		</div>
	</div>
	@show

@endsection

