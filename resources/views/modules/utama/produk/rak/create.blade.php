<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Rak</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
	{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="two fields">
			<div class="field">
				<label>Tipe Rak</label>
				<input name="tipe_rak" placeholder="Tipe Rak" type="text" value="{{ $tipe_rak or "" }}">
			</div>

			<div class="field">
				<label>Shelving</label>
				<div class="ui right labeled input">
					<input class="length" name="shelving" placeholder="Shelving" type="text" value="{{ $shelving or "" }}">
					<div class="ui basic label">tingkat</div>
				</div>
			</div>
		</div>
		
		<div class="three fields">
			<div class="field">
				<label>Tinggi Rak</label>
				<div class="ui right labeled input">
				<input class="length" name="tinggi_rak" placeholder="Tinggi Rak" type="text" value="{{ $tinggi_rak or "" }}">
				<div class="ui basic label">cm</div>
				</div>
			</div>

			<div class="field">
				<label>Panjang Rak</label>
				<div class="ui right labeled input">
				<input class="length" name="panjang_rak" placeholder="Panjang Rak" type="text" value="{{ $panjang_rak or "" }}">
				<div class="ui basic label">cm</div>
				</div>
			</div>

			<div class="field">
				<label>Lebar Rak</label>
				<div class="ui right labeled input">
				<input class="length" name="lebar_rak" placeholder="Lebar Rak" type="text" value="{{ $lebar_rak or "" }}">
				<div class="ui basic label">cm</div>
				</div>
			</div>
		</div>
		{{-- <h2 class="ui dividing header">
		</h2> --}}
		<div class="three fields">
			<div class="field">
				<label>Hanger</label>
				<div class="inline fields">
					<div class="field">
				      <div class="ui radio checkbox" onclick="undisable()">
				        <input type="radio" name="hanger" tabindex="0" class="hidden" value="1" checked>
				        <label>Ya</label>
				      </div>
				    </div>
				    
				    <div class="field">
				      <div class="ui radio checkbox" onclick="disable()">
				        <input type="radio" name="hanger" tabindex="0" class="hidden" value="0">
				        <label>Tidak</label>
				      </div>
					</div>
			    </div>
			</div>
			
			<div class="field">
				<label>Tinggi Hanger</label>
				<div class="ui right labeled input">
				<input class="length" id="myRadio1" name="tinggi_hanger" placeholder="Tinggi Hanger" type="text" value="{{ old('tinggi_hanger')}}">
				<div class="ui basic label">cm</div>
				</div>
			</div>

			<div class="field">
				<label>Panjang Hanger</label>
				<div class="ui right labeled input">
				<input class="length" id="myRadio2" name="panjang_hanger" placeholder="Panjang Hanger" type="text" value="{{ old('panjang_hanger')}}">
				<div class="ui basic label">cm</div>
				</div>
			</div>
			{{-- <div class="field">
				<label>Tinggi Hanger</label>
				<div class="ui right labeled input">
				<input id="myRadio" name="tinggi_hanger" placeholder="Tinggi Hanger" type="number" value="{{ old('tinggi_hanger')}}">
				<div class="ui basic label">cm</div>
				</div>
			</div>

			<div class="field">
				<label>Panjang Hanger</label>
				<div class="ui right labeled input">
				<input id="myRadio" name="panjang_hanger" placeholder="Panjang Hanger" type="number" value="{{ old('panjang_hanger')}}">
				<div class="ui basic label">cm</div>
				</div>
			</div> --}}
		</div>

	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

{{-- <button onclick="disable()">Disable radio button</button>
<button onclick="undisable()">Undisable radio button</button> --}}


<script type="text/javascript">
	function disable() {
    document.getElementById("myRadio1").disabled = true;
    document.getElementById("myRadio2").disabled = true;
	}
	
	function undisable() {
	    document.getElementById("myRadio1").disabled = false;
	    document.getElementById("myRadio2").disabled = false;
	}
	
	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });
</script>