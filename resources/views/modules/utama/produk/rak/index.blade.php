@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[tipe_rak]" placeholder="Tipe Rak" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_rak();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.tipe_rak = $("input[name='filter[tipe_rak]']").val();
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	tipe_rak: {
		identifier: 'tipe_rak',
		rules: [{
			type   : 'empty',
			prompt : 'Tipe Rak Harus Terisi'
		},{
				type   : 'minLength[2]',
				prompt : 'Isian tipe rak harus berjumlah {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Isian tipe rak harus berjumlah {ruleValue} karakter'
			}]
	},

	shelving: {
		identifier: 'shelving',
		rules: [{
			type   : 'empty',
			prompt : 'Shelving Harus Terisi'
		},{
				type   : 'maxLength[7]',
				prompt : 'Isian shelving harus berjumlah {ruleValue} karakter'
			}]
	},

	tinggi_rak: {
		identifier: 'tinggi_rak',
		rules: [{
			type   : 'empty',
			prompt : 'Tinggi Rak Harus Terisi'
		},{
				type   : 'minLength[2]',
				prompt : 'Isian tinggi rak harus berjumlah {ruleValue} karakter'
			},{
				type   : 'maxLength[7]',
				prompt : 'Isian tinggi rak harus berjumlah {ruleValue} karakter'
			}]
	},

	panjang_rak: {
		identifier: 'panjang_rak',
		rules: [{
			type   : 'empty',
			prompt : 'Panjang Rak Harus Terisi'
		},{
				type   : 'minLength[2]',
				prompt : 'Isian panjang rak harus berjumlah {ruleValue} karakter'
			},{
				type   : 'maxLength[7]',
				prompt : 'Isian panjang rak harus berjumlah {ruleValue} karakter'
			}]
	},

	lebar_rak: {
		identifier: 'lebar_rak',
		rules: [{
			type   : 'empty',
			prompt : 'Lebar Rak Harus Terisi'
		},{
				type   : 'minLength[2]',
				prompt : 'Isian lebar rak harus berjumlah {ruleValue} karakter'
			},{
				type   : 'maxLength[7]',
				prompt : 'Isian lebar rak harus berjumlah {ruleValue} karakter'
			}]
	},

	tinggi_hanger: {
		identifier: 'tinggi_hanger',
		rules: [{
			type   : 'empty',
			prompt : 'Lebar Rak Harus Terisi'
		},{
				type   : 'maxLength[7]',
				prompt : 'Isian tinggi hanger harus berjumlah {ruleValue} karakter'
			}]
	},

	panjang_hanger: {
		identifier: 'panjang_hanger',
		rules: [{
			type   : 'empty',
			prompt : 'Lebar Rak Harus Terisi'
		},{
				type   : 'maxLength[7]',
				prompt : 'Isian panjang hanger harus berjumlah {ruleValue} karakter'
			}]
	},

	// hanger: {
	// 	identifier: 'hanger',
	// 	rules: [{
	// 		type   : 'empty',
	// 		prompt : 'hanger Harus Terisi'
	// 	}]
	// }
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
	};
</script>
@endsection


@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_rak = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-rak') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var tipe_rak = document.createElement("input");
		            tipe_rak.setAttribute("type", "hidden");
		            tipe_rak.setAttribute("name", 'tipe_rak');
		            tipe_rak.setAttribute("value", $('[name="filter[tipe_rak]"]').val());
		        form.appendChild(tipe_rak);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append