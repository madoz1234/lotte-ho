<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Cek GMD Price & Status</div>
<div class="content">
	<table id="tabelDetailPrice" class="ui celled compact red table" width="98%" cellspacing="0">
		<thead>
			<tr>
				<th style="text-align: center; width: 15%;">Kode LSI</th>
				<th style="text-align: center; width: 30%;">Nama LSI</th>
				<th style="text-align: center; width: 15%;">Tanggal</th>
				<th style="text-align: center; width: 20%;">Harga</th>
				<th style="text-align: center; width: 20%;">Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach($lsi as $detail_lsi)
				<?php 
					$price = $produklsi->where('lsi_kode', $detail_lsi->kode)->first();
				?>
				<tr>
					<td style="text-align: center;width: 15%;">{{ $detail_lsi->kode }}</td>
					<td style="text-align: left;width: 30%;">{{ $detail_lsi->nama }}</td>
					<td style="text-align: center;width: 15%;">{{ isset($price) ? (!is_null($price->updated_at) ? date_format($price->updated_at,"d-m-Y") : date_format($price->created_at,"d-m-Y")) : '-' }}</td>
					<td style="text-align: center;width: 20%;">{{ isset($price) ? $price->curr_sale_prc : '0.00' }}</td>
					<td style="text-align: center;width: 20%;">{{ isset($price) ? (!is_null($price->status) ? $price->status : '-') : '-' }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div> --}}
</div>

<script type="text/javascript">
	$('#tabelDetailPrice').DataTable({
		paging: false,
		filter: false,
		lengthChange: false,
		ordering:false,
		scrollX: true,
		scrollY: 500,
	});
</script>