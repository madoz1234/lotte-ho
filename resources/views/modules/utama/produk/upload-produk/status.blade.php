<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Cek GMD Status</div>
<div class="content">
	<table id="logsheet" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th style="text-align: center;">Daftar LSI</th>
				<th style="text-align: center;">Tanggal</th>
				<th style="text-align: center;">Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach($lsi as $detail_lsi)
				<?php 
					$price = $produklsi->where('lsi_kode', $detail_lsi->kode)->first();
				?>
				<tr>
					<td style="text-align: left;">{{ $detail_lsi->kode.'-'.$detail_lsi->nama }}</td>
					<td style="text-align: center;">{{ isset($price) ? (!is_null($price->updated_at) ? date_format($price->updated_at,"d-m-Y") : date_format($price->created_at,"d-m-Y")) : '-' }}</td>
					<td style="text-align: center;">{{ isset($price) ? (!is_null($price->status) ? $price->status : '-') : '-' }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div> --}}
</div>
