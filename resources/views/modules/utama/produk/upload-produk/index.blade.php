@extends('layouts.grid')

@section('filters')

	<div class="field">
		<input name="filter[kode]" placeholder="Kode" type="number">
	</div>

	<div class="field">
		<input name="filter[nama]" placeholder="Product" type="text">
	</div>

	<div class="field">
		<select name="filter[bumun_nm]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\DivisiProduk::options('nama', 'nama',[], '-- Pilih Division --') !!}
    	</select>
	</div>

	<div class="field">
		<select name="filter[l1_nm]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\Kategori1::options('nama', 'nama',[], '-- Pilih Kategori 1 --') !!}
    	</select>
	</div>

	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('js-filters')
    d.kode = $("input[name='filter[kode]']").val();
    d.nama = $("input[name='filter[nama]']").val();
    d.bumun_nm = $("select[name='filter[bumun_nm]']").val();
    d.l1_nm = $("select[name='filter[l1_nm]']").val();
@endsection

@section('toolbars')
            {{-- <button type="button" class="ui grey button" onclick="javascript:tamplate_gmd();"><i class="download icon icon"></i>Template</button> --}}
            {{-- <button type="button" class="ui blue add button">
            	<i class="plus icon"></i>
            	Tambah Produk Non GMD
            </button> --}}
			<a href="{{ asset('template\produk-gmd.xls') }}" class="ui grey button" target="_blank"><i class="download icon icon"></i>Template</a>
        	<button type="button" class="ui blue importexcel button" style="background-color: #3537ad;"><i class="upload icon icon"></i>Upload</button>
			<button type="button" class="ui green button" onclick="javascript:export_upload_produk();">
				<i class="file excel outline icon"></i>
				Export Excel
			</button>
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	jenis_assortment: {
		identifier: 'jenis_assortment',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Jenis Assortment tidak boleh kosong'
		}]
	},

	jumlah_rak_lorong: {
		identifier: 'jumlah_rak_lorong',
		rules: [{
			type   : 'empty',
			prompt : 'Pilihan Jumlah Rak Diding tidak boleh kosong'
		}]
	},

	jumlah_rak_dinding: {
		identifier: 'jumlah_rak_dinding',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Jumlah Rak Dinding tidak boleh kosong'
		}]
	},

	jumlah_rak_kasir: {
		identifier: 'jumlah_rak_kasir',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Jumlah Rak Kasir tidak boleh kosong'
		}]
	}
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	$(document).on('click', '.cekprice.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/cekprice/"+id;

		loadModal(url);
	});

	$(document).on('click', '.cekstatus.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/cekstatus/"+id;

		loadModal(url);
	});

	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
		$('.demo.menu .item').tab({
			history:false,
		});
	};

	// $('#tabelDetailPrice').DataTable({
	// 	paging: false,
	// 	filter: false,
	// 	lengthChange: false,
	// 	ordering:false,
	// 	scrollX: true,
	// 	scrollY: 300,
	// });
</script>
@endsection
@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_upload_produk = function(){
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-upload-produk') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var kode = document.createElement("input");
		            kode.setAttribute("type", "hidden");
		            kode.setAttribute("name", 'kode');
		            kode.setAttribute("value", $('[name="filter[kode]"]').val());
		        form.appendChild(kode);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        var bumun_nm = document.createElement("input");
		            bumun_nm.setAttribute("type", "hidden");
		            bumun_nm.setAttribute("name", 'bumun_nm');
		            bumun_nm.setAttribute("value", $('[name="filter[bumun_nm]"]').val());
		        form.appendChild(bumun_nm);

		        var l1_nm = document.createElement("input");
		            l1_nm.setAttribute("type", "hidden");
		            l1_nm.setAttribute("name", 'l1_nm');
		            l1_nm.setAttribute("value", $('[name="filter[l1_nm]"]').val());
		        form.appendChild(l1_nm);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}

		});

	</script>
@append