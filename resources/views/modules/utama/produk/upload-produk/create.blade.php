<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Produk Non GMD</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="field">
			<div class="fields">
				<div class="four wide field">
			<label>Produk Kode</label>
					<input type="text" name="kode" placeholder="Produk Kode">
				</div>

				<div class="twelve wide field">
					<label>Produk</label>
					<input type="text" name="nama" placeholder="Nama Produk">
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>
	