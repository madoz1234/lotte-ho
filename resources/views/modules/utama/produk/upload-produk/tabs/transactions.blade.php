<form class="ui data form" id="dataForm">

	<div class="ui centered grid">
		<div class="sixteen wide column">
			<div class="ui two cards">
				<!-- uom 1 -->
				<div class="card">
					<div class="content">
						<div class="header" style="text-align: center;">UOM 1</div>
						<br/>
						<div class="ui form">
							<div class="sixteen fields">
								<div class="ten wide field">
									<label>Product Description</label>
									<input type="text" name="produk_deskripsi">
								</div>

								<div class="six wide field">
									<label>Description</label>
									<div class="ui fluid search selection dropdown">
										<input type="hidden" name="deskripsi">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item" data-value="1">Box</div>
											<div class="item" data-value="2">Carton</div>
											<div class="item" data-value="3">Dus</div>
										</div>
									</div>
								</div>
							</div>

							<h4 class="ui blue dividing header">Headling Type UOM 1</h4>
							<div class="fields">
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checked checkbox">
												<label>Selling Unit</label><input type="checkbox"> 
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<label>Receiving Unit</label><input type="checkbox">
											</div>
										</div>
									</div>
								</div>
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checked checkbox">
												<label>Order Unit</label><input type="checkbox"> 
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<label>Retrun Unit</label><input type="checkbox">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<label>Shelfing Rack Type Unit</label><input type="checkbox">
								</div>
							</div>
							
							<div class="two fields">
								<div class="field">
									<label>Internal Barcode</label>
									<input type="number" name="internal_barcode" placeholder="Inputkan">
								</div>

								<div class="field">
									<label>Conversion</label>
									<input type="number" name="conversion" placeholder="Inputkan">
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Width UOM 1</label>
									<div class="ui right labeled input">
										<input type="number" name="width" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Length UOM 1</label>
									<div class="ui right labeled input">
										<input type="number" name="length" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Height UOM 1</label>
									<div class="ui right labeled input">
										<input type="number" name="height" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Weight UOM 1</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											gr
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<label>Harga</label>
									<input type="number" name="height" placeholder="Inputkan">
								</div>

								{{-- <div class="field">
									<label>Margin</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											%
										</div>
									</div>
								</div> --}}

								<div class="field">
									<label>Box Type</label>
									<div class="inline fields">
										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="1" checked>
												<label>Outerbox</label>
											</div>
										</div>

										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="0">
												<label>Inerbox</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="field">
								<label>File Gambar</label>
								<input type="file" name="akun_diskon">
							</div>
						</div>
					</div>
				</div>
				<!-- uom 1 -->

				<!-- uom 2 -->
				<div class="card">
					<div class="content">
						<div class="header" style="text-align: center;">UOM 2</div>
						<br/>
						<div class="ui form">
							<div class="sixteen fields">
								<div class="ten wide field">
									<label>Product Description</label>
									<input type="text" name="produk_deskripsi">
								</div>

								<div class="six wide field">
									<label>Description</label>
									<div class="ui fluid search selection dropdown">
										<input type="hidden" name="deskripsi">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item" data-value="1">Box</div>
											<div class="item" data-value="2">Carton</div>
											<div class="item" data-value="3">Dus</div>
										</div>
									</div>
								</div>
							</div>

							<h4 class="ui blue dividing header">Headling Type UOM 2</h4>
							<div class="fields">
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checked checkbox">
												<label>Selling Unit</label><input type="checkbox"> 
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<label>Receiving Unit</label><input type="checkbox">
											</div>
										</div>
									</div>
								</div>
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checked checkbox">
												<label>Order Unit</label><input type="checkbox"> 
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<label>Retrun Unit</label><input type="checkbox">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<label>Shelfing Rack Type Unit</label><input type="checkbox">
								</div>
							</div>
							
							<div class="two fields">
								<div class="field">
									<label>Internal Barcode</label>
									<input type="number" name="internal_barcode" placeholder="Inputkan">
								</div>

								<div class="field">
									<label>Conversion</label>
									<input type="number" name="conversion" placeholder="Inputkan">
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Width UOM 2</label>
									<div class="ui right labeled input">
										<input type="number" name="width" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Length UOM 2</label>
									<div class="ui right labeled input">
										<input type="number" name="length" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Height UOM 2</label>
									<div class="ui right labeled input">
										<input type="number" name="height" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Weight UOM 2</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											gr
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<label>Harga</label>
									<input type="number" name="height" placeholder="Inputkan">
								</div>

								{{-- <div class="field">
									<label>Margin</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											%
										</div>
									</div>
								</div> --}}

								<div class="field">
									<label>Box Type</label>
									<div class="inline fields">
										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="1" checked>
												<label>Outerbox</label>
											</div>
										</div>

										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="0">
												<label>Inerbox</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="field">
								<label>File Gambar</label>
								<input type="file" name="akun_diskon">
							</div>
						</div>
					</div>
				</div>
				<!-- uom 2 -->
			</div>

			<div class="ui two cards">
				<!-- uom 3 -->
				<div class="card">
					<div class="content">
						<div class="header" style="text-align: center;">UOM 3</div>
						<br/>
						<div class="ui form">
							<div class="sixteen fields">
								<div class="ten wide field">
									<label>Product Description</label>
									<input type="text" name="produk_deskripsi">
								</div>

								<div class="six wide field">
									<label>Description</label>
									<div class="ui fluid search selection dropdown">
										<input type="hidden" name="deskripsi">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item" data-value="1">Box</div>
											<div class="item" data-value="2">Carton</div>
											<div class="item" data-value="3">Dus</div>
										</div>
									</div>
								</div>
							</div>

							<h4 class="ui blue dividing header">Headling Type UOM 3</h4>
							<div class="fields">
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checked checkbox">
												<label>Selling Unit</label><input type="checkbox"> 
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<label>Receiving Unit</label><input type="checkbox">
											</div>
										</div>
									</div>
								</div>
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checked checkbox">
												<label>Order Unit</label><input type="checkbox"> 
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<label>Retrun Unit</label><input type="checkbox">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<label>Shelfing Rack Type Unit</label><input type="checkbox">
								</div>
							</div>
							
							<div class="two fields">
								<div class="field">
									<label>Internal Barcode</label>
									<input type="number" name="internal_barcode" placeholder="Inputkan">
								</div>

								<div class="field">
									<label>Conversion</label>
									<input type="number" name="conversion" placeholder="Inputkan">
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Width UOM 3</label>
									<div class="ui right labeled input">
										<input type="number" name="width" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Length UOM 3</label>
									<div class="ui right labeled input">
										<input type="number" name="length" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Height UOM 3</label>
									<div class="ui right labeled input">
										<input type="number" name="height" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Weight UOM 3</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											gr
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<label>Harga</label>
									<input type="number" name="height" placeholder="Inputkan">
								</div>

								{{-- <div class="field">
									<label>Margin</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											%
										</div>
									</div>
								</div> --}}

								<div class="field">
									<label>Box Type</label>
									<div class="inline fields">
										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="1" checked>
												<label>Outerbox</label>
											</div>
										</div>

										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="0">
												<label>Inerbox</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="field">
								<label>File Gambar</label>
								<input type="file" name="akun_diskon">
							</div>
						</div>
					</div>
				</div>
				<!-- uom 3 -->

				<!-- uom 4 -->
				<div class="card">
					<div class="content">
						<div class="header" style="text-align: center;">UOM 4</div>
						<br/>
						<div class="ui form">
							<div class="sixteen fields">
								<div class="ten wide field">
									<label>Product Description</label>
									<input type="text" name="produk_deskripsi">
								</div>

								<div class="six wide field">
									<label>Description</label>
									<div class="ui fluid search selection dropdown">
										<input type="hidden" name="deskripsi">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item" data-value="1">Box</div>
											<div class="item" data-value="2">Carton</div>
											<div class="item" data-value="3">Dus</div>
										</div>
									</div>
								</div>
							</div>

							<h4 class="ui blue dividing header">Headling Type UOM 4</h4>
							<div class="fields">
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checked checkbox">
												<label>Selling Unit</label><input type="checkbox"> 
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<label>Receiving Unit</label><input type="checkbox">
											</div>
										</div>
									</div>
								</div>
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checked checkbox">
												<label>Order Unit</label><input type="checkbox"> 
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<label>Retrun Unit</label><input type="checkbox">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<label>Shelfing Rack Type Unit</label><input type="checkbox">
								</div>
							</div>
							
							<div class="two fields">
								<div class="field">
									<label>Internal Barcode</label>
									<input type="number" name="internal_barcode" placeholder="Inputkan">
								</div>

								<div class="field">
									<label>Conversion</label>
									<input type="number" name="conversion" placeholder="Inputkan">
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Width UOM 4</label>
									<div class="ui right labeled input">
										<input type="number" name="width" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Length UOM 4</label>
									<div class="ui right labeled input">
										<input type="number" name="length" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Height UOM 4</label>
									<div class="ui right labeled input">
										<input type="number" name="height" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Weight UOM 4</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											gr
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<label>Harga</label>
									<input type="number" name="height" placeholder="Inputkan">
								</div>

								{{-- <div class="field">
									<label>Margin</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											%
										</div>
									</div>
								</div> --}}

								<div class="field">
									<label>Box Type</label>
									<div class="inline fields">
										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="1" checked>
												<label>Outerbox</label>
											</div>
										</div>

										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="0">
												<label>Inerbox</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="field">
								<label>File Gambar</label>
								<input type="file" name="akun_diskon">
							</div>
						</div>
					</div>
				</div>
				<!-- uom 4 -->
			</div>
		</div>

		<div class="sixteen wide center aligned column">
			<div class="actions">
				<button type="button" class="ui positive icon save button" data-content="Ubah Data" data-id="1"><i class="save icon"></i> Simpan</button>
				<button type="button" class="ui negative icon button" data-content="batal Data" data-id="1"> Batal</button>
			</div>
		</div>
		<div class="ui error message"></div>
	</div>
</form>
		<!-- Form -->




function addUomp(){
		$( ".ui.data.form" ).append("<div class='ui two cards add'>"+
			"<div class='card'>"+
				"<div class='content'>"+		
				"<div class='header' style='text-align: center;'>UOM 3</div>"+
			    "<br/>"+
			    "<div class='ui form'>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>UOM Description</label>"+
				    "<div class='eight wide field'>"+
				      "<input placeholder='Description' type='text'>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>Barcode UOM</label>"+
				    "<div class='eight wide field'>"+
				      "<input placeholder='Barcode UOM' type='text'>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>Product Description</label>"+
				    "<div class='eight wide field'>"+
				      "<input placeholder='Product Description' type='text'>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>Handling Type UOM</label>"+
				    "<div class='eight wide field'>"+
				    	"<div class='ui form'>"+
					    	"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox'>"+
							      "<label>Selling Unit</label>"+
							    "</div>"+
							"</div>"+
							"<br/>"+
							"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox' checked=''>"+
							      "<label>Order Unit</label>"+
							    "</div>"+
							"</div>"+
							"<br/>"+
							"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox'>"+
							      "<label>Receive Unit</label>"+
							    "</div>"+
							"</div>"+
							"<br/>"+
							"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox' checked=''>"+
							      "<label>Return Unit</label>"+
							    "</div>"+
							"</div>"+
							"<br/>"+
							"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox'>"+
							      "<label>Inventory Unit</label>"+
							    "</div>"+
							"</div>"+	
						"</div>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>GMD Price</label>"+
				    	"<input placeholder='Description' type='text'>"+
				    "<div class='eight wide field'>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>Cost Price UOM</label>"+
				    	"<input placeholder='Description' type='text'>"+
				    "<div class='eight wide field'>"+
				    "</div>"+
				  "</div>"+
				"</div>"+


				"</div>"+
				"</div>"+
				"<div class='card'>"+
				"<div class='content'>"+		
				"<div class='header' style='text-align: center;'>UOM 4</div>"+
			    "<br/>"+
			    {{-- "<div class='ui form'>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>UOM Description</label>"+
				    "<div class='eight wide field'>"+
				      "<input placeholder='Description' type='text'>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>Barcode UOM</label>"+
				    "<div class='eight wide field'>"+
				      "<input placeholder='Barcode UOM' type='text'>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>Product Description</label>"+
				    "<div class='eight wide field'>"+
				      "<input placeholder='Product Description' type='text'>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>Handling Type UOM</label>"+
				    "<div class='eight wide field'>"+
				    	"<div class='ui form'>"+
					    	"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox'>"+
							      "<label>Selling Unit</label>"+
							    "</div>"+
							"</div>"+
							"<br/>"+
							"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox' checked=''>"+
							      "<label>Order Unit</label>"+
							    "</div>"+
							"</div>"+
							"<br/>"+
							"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox'>"+
							      "<label>Receive Unit</label>"+
							    "</div>"+
							"</div>"+
							"<br/>"+
							"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox' checked=''>"+
							      "<label>Return Unit</label>"+
							    "</div>"+
							"</div>"+
							"<br/>"+
							"<div class='inline field'>"+
							    "<div class='ui toggle checkbox'>"+
							      "<input tabindex='0' class='hidden' type='checkbox'>"+
							      "<label>Inventory Unit</label>"+
							    "</div>"+
							"</div>"+	
						"</div>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>GMD Price</label>"+
				    	"<input placeholder='Description' type='number'>"+
				    "<div class='eight wide field'>"+
				    "</div>"+
				  "</div>"+
				  "<div class='inline fields'>"+
				    "<label class='six wide field'>Cost Price UOM</label>"+
				    	"<input placeholder='Description' type='text'>"+
				    "<div class='eight wide field'>"+
				    "</div>"+
				  "</div>"+
				"</div>"+ --}}
				"</div>"+
				"</div>"
			);