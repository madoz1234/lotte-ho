<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">
Tambah
<div style="float: right;">
	<button type="button" class="ui blue add button uomp">
		<i class="plus icon"></i>
		UOM
	</button>
	<button type="button" class="ui red remove button uomp hidden">
		<i class="minus icon"></i>
		UOM
	</button>
</div>
</div>
<br><br>
<div class="content">
	<form class="ui data form" id="dataForm">
	<!-- pertama -->{{-- 
		    	<div class="ui form">
				  <div class="inline fields">
				    <label class="two wide field">Kode Produk</label>
				    <div class="six wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan" type="number">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="two wide field">Kode Produk</label>
				    <div class="six wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan" type="text">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="two wide field">TMUK</label>
				    <div class="six wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan" type="text">
				    </div>
				  </div>
				</div> --}}
	<!-- pertama -->
		<div class="ui two cards">
				<!-- uom 1 -->
				<div class="card">
					<div class="content">
						<div class="header" style="text-align: center;">UOM 1</div>
						<br/>
						<div class="ui form">
							<div class="sixteen fields">
								<div class="ten wide field">
									<label>Product Description</label>
									<input type="text" name="produk_deskripsi">
								</div>

								<div class="six wide field">
									<label>Description</label>
									<div class="ui fluid search selection dropdown">
										<input type="hidden" name="deskripsi">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item" data-value="1">Box</div>
											<div class="item" data-value="2">Carton</div>
											<div class="item" data-value="3">Dus</div>
										</div>
									</div>
								</div>
							</div>

							<h4 class="ui blue dividing header">Headling Type UOM 1</h4>
							<div class="fields">
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checkbox">
												<input type="checkbox" checked=""> 
												<label>Selling Unit</label>
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<input type="checkbox" >
												<label>Order Unit</label>
											</div>
										</div>
									</div>
								</div>
								<div class="eight wide field">
									<div class="two fields">
										
										<div class="field">
											<div class="ui checked checkbox">
												<input type="checkbox" checked="">
												<label>Receiving Unit</label>
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<input type="checkbox">
												<label>Retrun Unit</label>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<input type="checkbox" checked>
									<label>Inventory Unit</label>
								</div>
							</div>
							
							<div class="two fields">
								<div class="field">
									<label>Internal Barcode</label>
									<input type="number" name="internal_barcode" placeholder="Inputkan">
								</div>

								<div class="field">
									<label>Conversion</label>
									<input type="number" name="conversion" placeholder="Inputkan">
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Width UOM 1</label>
									<div class="ui right labeled input">
										<input type="number" name="width" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Length UOM 1</label>
									<div class="ui right labeled input">
										<input type="number" name="length" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Height UOM 1</label>
									<div class="ui right labeled input">
										<input type="number" name="height" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Weight UOM 1</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											gr
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<label>Harga</label>
									<input type="number" name="height" placeholder="Inputkan">
								</div>

								{{-- <div class="field">
									<label>Margin</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											%
										</div>
									</div>
								</div> --}}

								<div class="field">
									<label>Box Type</label>
									<div class="inline fields">
										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="1">
												<label>Outerbox</label>
											</div>
										</div>

										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="0">
												<label>Inerbox</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="field">
								<label>File Gambar</label>
								<input type="file" name="akun_diskon">
							</div>
						</div>
					</div>
				</div>
				<!-- uom 1 -->

				<!-- uom 2 -->
				<div class="card">
					<div class="content">
						<div class="header" style="text-align: center;">UOM 2</div>
						<br/>
						<div class="ui form">
							<div class="sixteen fields">
								<div class="ten wide field">
									<label>Product Description</label>
									<input type="text" name="produk_deskripsi">
								</div>

								<div class="six wide field">
									<label>Description</label>
									<div class="ui fluid search selection dropdown">
										<input type="hidden" name="deskripsi">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item" data-value="1">Box</div>
											<div class="item" data-value="2">Carton</div>
											<div class="item" data-value="3">Dus</div>
										</div>
									</div>
								</div>
							</div>

							<h4 class="ui blue dividing header">Headling Type UOM 2</h4>
							<div class="fields">
								<div class="eight wide field">
									<div class="two fields">
										<div class="field">
											<div class="ui checked checkbox">
												<input type="checkbox">
												<label>Selling Unit</label>
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<input type="checkbox" checked>
												<label>Order Unit</label>
											</div>
										</div>
									</div>
								</div>
								<div class="eight wide field">
									<div class="two fields">
										

										<div class="field">
											<div class="ui checked checkbox">
												<input type="checkbox">
												<label>Receiving Unit</label>
											</div>
										</div>

										<div class="field">
											<div class="ui checked checkbox">
												<input type="checkbox" checked>
												<label>Retrun Unit</label>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<input type="checkbox">
									<label>Inventory Unit</label>
								</div>
							</div>
							
							<div class="two fields">
								<div class="field">
									<label>Internal Barcode</label>
									<input type="number" name="internal_barcode" placeholder="Inputkan">
								</div>

								<div class="field">
									<label>Conversion</label>
									<input type="number" name="conversion" placeholder="Inputkan">
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Width UOM 2</label>
									<div class="ui right labeled input">
										<input type="number" name="width" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Length UOM 2</label>
									<div class="ui right labeled input">
										<input type="number" name="length" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>
							</div>

							<div class="two fields">
								<div class="field">
									<label>Height UOM 2</label>
									<div class="ui right labeled input">
										<input type="number" name="height" placeholder="Inputkan">
										<div class="ui basic label">
											cm
										</div>
									</div>
								</div>

								<div class="field">
									<label>Weight UOM 2</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											gr
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<label>Harga</label>
									<input type="number" name="height" placeholder="Inputkan">
								</div>

								{{-- <div class="field">
									<label>Margin</label>
									<div class="ui right labeled input">
										<input type="number" name="weight" placeholder="Inputkan">
										<div class="ui basic label">
											%
										</div>
									</div>
								</div> --}}

								<div class="field">
									<label>Box Type</label>
									<div class="inline fields">
										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="1">
												<label>Outerbox</label>
											</div>
										</div>

										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="box_type" tabindex="0" class="hidden" value="0">
												<label>Inerbox</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="field">
								<label>File Gambar</label>
								<input type="file" name="akun_diskon">
							</div>
						</div>
					</div>
				</div>
				<!-- uom 2 -->
			</div>
		</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>