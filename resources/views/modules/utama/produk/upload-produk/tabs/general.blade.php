<form class="ui data form" id="dataForm">
	<div class="ui grid">
		<div class="eight wide column">
			<!-- Barang -->
			<h4 class="ui blue dividing header">Barang</h4>
			<div class="two fields">
				<div class="field">
					<label>Tipe Barang</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="text">Trade - Non Lotte</div>
						<div class="menu">
							<div class="item" data-value="1">Trade - Lotte</div>
							<div class="item" data-value="2" selected>Trade - Non Lotte</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Jenis Barang</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="text">Non Food</div>
						<div class="menu">
							<div class="item" data-value="1">All</div>
							<div class="item" data-value="2">Food</div>
							<div class="item" data-value="3">Non Food</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Devision</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="text">Dry Food</div>
						<div class="menu">
							<div class="item" data-value="1">Dry Food</div>
							<div class="item" data-value="2">Fresh Food</div>
							<div class="item" data-value="3">Non Food</div>
						</div>
					</div>
				</div>
			</div>

			<div class="two fields">
				<div class="field">
					<label>Category 1</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">Pilih</div>
						<div class="menu">
							<div class="item" data-value="1">Dairy & Frozen</div>
							<div class="item" data-value="2">Bakery</div>
							<div class="item" data-value="3">Biscuit/Snacks</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Category 2</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">Pilih</div>
						<div class="menu">
							<div class="item" data-value="1">Dairy & Frozen</div>
							<div class="item" data-value="2">Bakery</div>
							<div class="item" data-value="3">Biscuit/Snacks</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Category 3</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">Pilih</div>
						<div class="menu">
							<div class="item" data-value="1">Dairy & Frozen</div>
							<div class="item" data-value="2">Bakery</div>
							<div class="item" data-value="3">Biscuit/Snacks</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Category 4</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">Pilih</div>
						<div class="menu">
							<div class="item" data-value="1">Dairy & Frozen</div>
							<div class="item" data-value="2">Bakery</div>
							<div class="item" data-value="3">Biscuit/Snacks</div>
						</div>
					</div>
				</div>
			</div>

			<div class="two fields">
				<div class="four wide field">
					<label>Kode Barang</label>
					<input placeholder="Inputkan" name="" type="text" value="5000006020">
				</div>

				<div class="twelve wide field">
					<label>Nama Barang</label>
					<input placeholder="Inputkan" name="" type="text" value="AQUA 1500ML/KARTON (Multijaya)">
				</div>

				{{-- <div class="field">
					<label>Pajak</label>
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="hanger" tabindex="0" class="hidden" value="1">
								<label>PPN</label>
							</div>
						</div>

						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="hanger" tabindex="0" class="hidden" value="0" checked>
								<label>Non PPN</label>
							</div>
						</div>
					</div>
				</div> --}}
			</div>

			<div class="field">
				<label>Margin</label>
				<div class="ui right labeled input">
					<input placeholder="Inputkan" name="" type="number">
					<div class="ui basic label">
						%
					</div>
				</div>
			</div>

			{{-- <div class="field">
				<label>Deskripsi Barang</label>
				<input placeholder="Inputkan" name="" type="text">
			</div> --}}
			<!-- Barang -->

			<!-- Moving Type -->
			<h4 class="ui blue dividing header">Moving Type </h4>
			<div class="two fields">
				<div class="field">
					<label>Average Sales Per Day</label>
					<input placeholder="Inputkan" name="" type="text" value="6,50">
				</div>

				<div class="field">
					<label>Moving Type</label>
					<input placeholder="Slow" name="" type="text" value="Fast">
				</div>
			</div>
			<!-- Moving Type -->

			<!-- Moving Type -->
			<h4 class="ui blue dividing header">Assortment Type </h4>

			<div class="field">
				<label for="fruit">Assortment Type</label>
				<div class="ui fluid search selection dropdown">
					<input type="hidden" name="provinsi_id">
					<i class="dropdown icon"></i>
					<div class="default text">Pilih Assortment Type</div>
					<div class="menu">
						<div class="item" data-value="1">500</div>
						<div class="item" data-value="2">800</div>
						<div class="item" data-value="3">1500</div>
						<div class="item" data-value="4">2500</div>
					</div>
				</div>
			</div>

			{{-- <div class="inline fields">
				<label for="fruit">Assortment Type</label>
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="500" checked="" tabindex="0" class="hidden">
						<label>500</label>
					</div>
				</div>
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="800" tabindex="0" class="hidden">
						<label>800</label>
					</div>
				</div>
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="1500" tabindex="0" class="hidden">
						<label>1500</label>
					</div>
				</div>
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="2500" tabindex="0" class="hidden">
						<label>2500</label>
					</div>
				</div>
			</div> --}}

			<div class="field">
				<label>Dari Suplier</label>
				<div class="ui fluid search selection dropdown">
					<input type="hidden" name="provinsi_id">
					<i class="dropdown icon"></i>
					<div class="default text">All Type</div>
					<div class="menu">
						<div class="item" data-value="1">All Type</div>
						<div class="item" data-value="2">Non Lotte</div>
						<div class="item" data-value="3">Lotte</div>
					</div>
				</div>
			</div>

			{{-- <div class="two fields">
				<div class="field">
					<label>Expiry Product</label>
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="ya" tabindex="0" class="hidden" value="1">
								<label>Ya</label>
							</div>
						</div>

						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="tidak" tabindex="0" class="hidden" value="0" checked>
								<label>Tidak</label>
							</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Weight Product</label>
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="ya" tabindex="0" class="hidden" value="1">
								<label>Ya</label>
							</div>
						</div>

						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="tidak" tabindex="0" class="hidden" value="0" checked>
								<label>Tidak</label>
							</div>
						</div>
					</div>
				</div>
			</div> --}}

			{{-- <div class="two fields">
				<div class="field">
					<label>Batch Product</label>
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="hanger" tabindex="0" class="hidden" value="1">
								<label>Ya</label>
							</div>
						</div>

						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="hanger" tabindex="0" class="hidden" value="0" checked>
								<label>Tidak</label>
							</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Active Status</label>
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="hanger" tabindex="0" class="hidden" value="1">
								<label>Ya</label>
							</div>
						</div>

						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="hanger" tabindex="0" class="hidden" value="0" checked>
								<label>Tidak</label>
							</div>
						</div>
					</div>
				</div>
			</div> --}}
			<!-- Moving Type -->
		</div>

		<div class="eight wide column">
			{{-- <h4 class="ui blue dividing header">Akun GL</h4> --}}
			<!-- GL Akun -->
			{{-- <div class="two fields">
				<div class="field">
					<label>Akun Penjualan</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">Penjualan NON LOTTE 4010014</div>
						<div class="menu">
							<div class="item" data-value="1">Penjualan NON LOTTE 4010014</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Akun Inventaris</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">Persediaan 106001 NON LOTTE</div>
						<div class="menu">
							<div class="item" data-value="1">Persediaan 106001 NON LOTTE</div>
						</div>
					</div>
				</div>
			</div> --}}

			{{-- <div class="two fields">
				<div class="field">
					<label>Akun COGS</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">5010014 HPP Inventory NON LOTTE</div>
						<div class="menu">
							<div class="item" data-value="1">5010014 HPP Inventory NON LOTTE</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Akun Penyesuaian Inventaris</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">7030001 Persediaan Keuntungan / Kerugian</div>
						<div class="menu">
							<div class="item" data-value="1">7030001 Persediaan Keuntungan / Kerugian</div>
						</div>
					</div>
				</div>
			</div> --}}
			<!-- GL Akun-->
			{{-- <h4 class="ui blue dividing header">Nomor Bagian Skunder</h4> --}}
			<!-- Nomor Skunder -->
			{{-- <div class="field">
				<label>Nama Barcode</label>
				<input type="text" name="provinsi_id">
			</div> --}}
			{{-- <div class="two fields">
				<div class="field">
					<label>Kode Barcode</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">Penjualan NON LOTTE 4010014</div>
						<div class="menu">
							<div class="item" data-value="1">Penjualan NON LOTTE 4010014</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Kode Lain</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">Persediaan 106001 NON LOTTE</div>
						<div class="menu">
							<div class="item" data-value="1">Persediaan 106001 NON LOTTE</div>
						</div>
					</div>
				</div>
			</div> --}}
			<!-- Nomor Skunder-->
			<h4 class="ui blue dividing header">Lain-lain</h4>
			<!-- Lain-lain -->
			<div class="field">
				<label>Akun Diskon Penjualan</label>
				<input type="text" name="akun_diskon">
			</div>

			<div class="two fields">
				<div class="field">
					<label>Diskon Maksimal</label>
					<div class="ui fluid search selection dropdown">
						<input type="hidden" name="provinsi_id">
						<i class="dropdown icon"></i>
						<div class="default text">Pilih</div>
						<div class="menu">
							<div class="item" data-value="1">Penjualan NON LOTTE 4010014</div>
						</div>
					</div>
				</div>

				<div class="field">
					<label>Status</label>
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="status_barang" tabindex="0" class="hidden" value="1">
								<label>Aktif</label>
							</div>
						</div>

						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="status_barang" tabindex="0" class="hidden" value="0" checked>
								<label>Non Aktif</label>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="field">
				<label>File Gambar</label>
				<input type="file" name="akun_diskon">
			</div>
			<!-- Lain-lain-->
		</div>

		<div class="sixteen wide center aligned column">
			<div class="actions">
				<button type="button" class="ui positive icon checkmark button" data-content="Ubah Data" data-id="1"><i class="checkmark icon"></i> Perbarui</button>
				<button type="button" class="ui primary icon copy button" data-content="Ubah Data" data-id="1"><i class="copy icon"></i> Salin</button>
				<button type="button" class="ui orange icon remove button" data-content="Ubah Data" data-id="1"><i class="remove icon"></i> Hapus</button>
				<button type="button" class="ui negative icon button" data-content="batal Data" data-id="1"> Batal</button>
			</div>
		</div>

		<div class="ui error message"></div>
	</div>
</form>
		<!-- Form -->