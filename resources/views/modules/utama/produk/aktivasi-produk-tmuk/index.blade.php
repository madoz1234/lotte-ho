@extends('layouts.grid')

@section('filters')
	<div class="field">
		<select name="filter[tmuk_kode]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
    	</select>
	</div>
	<div class="field">
		<input name="filter[produk_kode]" placeholder="Kode Produk" type="text">
	</div>
	<div class="field">
		<input name="filter[nama]" placeholder="Nama Produk" type="text">
	</div>
	{{-- <div class="field">
		<select name="filter[produk_kode]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\Produk::options('nama', 'kode',[], '-- Pilih Nama Produk --') !!}
    	</select>
	</div> --}}
	<div class="field">
		<select name="filter[tipe_barang_kode]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\TipeBarang::options('nama', 'kode',[], '-- Pilih Tipe Barang --') !!}
    	</select>
	</div>
	<div class="field">
		<select name="filter[jenis_barang_kode]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\JenisBarang::options('jenis', 'kode',[], '-- Pilih Jenis Barang --') !!}
    	</select>
	</div>
	<div class="field">
		<select name="filter[flag]" class="ui fluid search selection dropdown">
			<option value="">-- Pilih Status --</option>
			<option value="0">Aktif</option>
			<option value="1">Non Aktif</option>
		</select>
	</div>

	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

	@section('toolbars')
	<div class="ui blue buttons" style="margin-right: 5px">
		<button type="button" class="ui blue aktivasi button"><i class="plus icon icon"></i>Aktivasi Produk</button>
	</div>
	<div class="ui grey buttons" style="margin-right: 5px">
		{{-- <button type="button" class="ui grey button" onclick="javascript:tamplate_produk_tmuk();"><i class="download icon icon"></i>Template</button> --}}
		<a href="{{ asset('template\produk-tmuk.xls') }}" class="ui grey button" target="_blank"><i class="download icon icon"></i>Template</a>
	</div>
	<div class="ui blue buttons" style="margin-right: 5px">
		<button type="button" class="ui blue importexcel button" style="background-color: #3537ad;"><i class="upload icon icon"></i>Upload</button>
	</div>
	<button type="button" class="ui green button" onclick="javascript:export_produk_tmuk();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
	@endsection

@section('js-filters')
	d.nama = $("input[name='filter[nama]']").val();
    d.produk_kode = $("input[name='filter[produk_kode]']").val();
    {{-- d.produk_kode = $("select[name='filter[produk_kode]']").val(); --}}
    d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
    d.tipe_barang_kode = $("select[name='filter[tipe_barang_kode]']").val();
    d.jenis_barang_kode = $("select[name='filter[jenis_barang_kode]']").val();
	d.flag = $("select[name='filter[flag]']").val();
@endsection

	@section('rules')
	<script type="text/javascript">
		formRules = {
			jenis_assortment: {
				identifier: 'jenis_assortment',
				rules: [{
					type   : 'empty',
					prompt : 'Isian Jenis Assortment tidak boleh kosong'
				}]
			},

			jumlah_rak_lorong: {
				identifier: 'jumlah_rak_lorong',
				rules: [{
					type   : 'empty',
					prompt : 'Pilihan Jumlah Rak Diding tidak boleh kosong'
				}]
			},

			jumlah_rak_dinding: {
				identifier: 'jumlah_rak_dinding',
				rules: [{
					type   : 'empty',
					prompt : 'Isian Jumlah Rak Dinding tidak boleh kosong'
				}]
			},

			jumlah_rak_kasir: {
				identifier: 'jumlah_rak_kasir',
				rules: [{
					type   : 'empty',
					prompt : 'Isian Jumlah Rak Kasir tidak boleh kosong'
				}]
			}
		};

		//form aktivasi harga
		$(document).ready(function() {
			
			$('.aktivasi').on('click', function(){
				loadModal('{{ url($pageUrl.'form-aktivasi') }}')
			})

		});
	</script>
	@endsection

	@section('init-modal')
	<script type="text/javascript">
		initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
		$('.demo.menu .item').tab({
			history:false,
		});
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
		$('.demo.menu .item').tab({
			history:false,
		});
	};
</script>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$(document).ready(function() {

		tamplate_produk_tmuk = function(){
				// alert('tara');
				// create form
				var form = document.createElement("form");
				form.setAttribute("method", 'POST');
				form.setAttribute("action", "{{ url('export/tamplate-produk-tmuk') }}");
				form.setAttribute("target", "_blank");

				var csrf = document.createElement("input");
				csrf.setAttribute("type", "hidden");
				csrf.setAttribute("name", '_token');
				csrf.setAttribute("value", '{{ csrf_token() }}');
				form.appendChild(csrf);

				document.body.appendChild(form);
				form.submit();

				document.body.removeChild(form);
			}

		export_produk_tmuk = function(){
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-produk-tmuk') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var produk_kode = document.createElement("input");
		            produk_kode.setAttribute("type", "hidden");
		            produk_kode.setAttribute("name", 'produk_kode');
		            produk_kode.setAttribute("value", $('[name="filter[produk_kode]"]').val());
		        form.appendChild(produk_kode);

		        var tmuk_kode = document.createElement("input");
		            tmuk_kode.setAttribute("type", "hidden");
		            tmuk_kode.setAttribute("name", 'tmuk_kode');
		            tmuk_kode.setAttribute("value", $('[name="filter[tmuk_kode]"]').val());
		        form.appendChild(tmuk_kode);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        var tipe_barang_kode = document.createElement("input");
		            tipe_barang_kode.setAttribute("type", "hidden");
		            tipe_barang_kode.setAttribute("name", 'tipe_barang_kode');
		            tipe_barang_kode.setAttribute("value", $("select[name='filter[tipe_barang_kode]']").val());
		        form.appendChild(tipe_barang_kode);

		        var jenis_barang_kode = document.createElement("input");
		            jenis_barang_kode.setAttribute("type", "hidden");
		            jenis_barang_kode.setAttribute("name", 'jenis_barang_kode');
		            jenis_barang_kode.setAttribute("value", $("select[name='filter[jenis_barang_kode]']").val());
		        form.appendChild(jenis_barang_kode);

		        var flag = document.createElement("input");
		            flag.setAttribute("type", "hidden");
		            flag.setAttribute("name", 'flag');
		            flag.setAttribute("value", $("select[name='filter[flag]']").val());
		        form.appendChild(flag);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}

		});
	</script>
	@append