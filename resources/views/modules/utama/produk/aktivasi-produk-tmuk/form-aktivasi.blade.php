<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">
	Aktivasi Produk TMUK
</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.'form-aktivasi') }}" method="post">
		{!! csrf_field() !!}
		
		<div class="ui error message"></div>
		<!-- pertama -->
		<div class="field required">		
			<label>TMUK</label>
			<select name="aktivasi_tmuk" class="ui full-width search selection dropdown">
				{!! \Lotte\Models\Master\Tmuk::options(function($q){ 
					return $q->kode . ' - ' . $q->nama; 
				}, 'id') !!}
			</select>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Proses
		<i class="checkmark icon"></i>
	</div>
</div>