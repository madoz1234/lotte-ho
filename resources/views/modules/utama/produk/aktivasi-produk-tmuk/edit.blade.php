<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">
	Edit Aktivasi Produk Assortment
</div>
<div class="content">
	{{-- <form class="ui data form" id="dataForm"> --}}
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<!-- pertama -->
		<div class="field">		
			<div class="fields">
				<div class="eight wide field">
					<label>Kode Produk</label>
					<input style="background-color: #edf1f6;" type="text" name="produk_kode" placeholder="Inputkan Kode Produk" value="{{ $record->produk_kode or "" }}" disabled="">
				</div>
				<div class="required eight wide field">
					<label>TMUK</label>
					<select class="ui full-width search selection dropdown" name="tmuk_kode">
						 {!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',['selected' => isset($record->tmuk_kode) ? $record->tmuk_kode : '',], '-- Pilih TMUK --') !!}
					</select>
				</div>
			</div>
		</div>
		<div class="field">		
			<div class="fields">
				<div class="eight wide field">
					<label>Tipe Produk</label>
					<input style="background-color: #edf1f6;" type="text" name="tipe_produk" placeholder="Inputkan Tipe Produk" value="{{ ($record->produk->produksetting->tipe_produk == 1) ? 'Produk Gmd' : 'Produk Non Gmd' }}" disabled="">
				</div>
				<div class="required eight wide field">
					<label>Status</label>
						<div class="inline fields">
						    <div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="flag" tabindex="0" class="" value="1" {{ ($record->flag == 1 ) ? 'checked' : '' }}>
									<label>Non Aktif</label>
								</div>
							</div>

							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="flag" tabindex="0" class="" value="0" {{ ($record->flag == 0 ) ? 'checked' : '' }}>
									<label>Aktif</label>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
		<div class="field">		
			<label>Nama Produk</label>
				<input style="background-color: #edf1f6;" type="text" name="produk_kode" placeholder="Inputkan Kode Produk" value="{{ $record->produk->nama or "" }}" disabled="">
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>