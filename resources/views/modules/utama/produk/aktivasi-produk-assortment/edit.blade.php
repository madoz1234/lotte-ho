<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">
	Edit Aktivasi Produk Assortment
</div>
<div class="content">
	{{-- <form class="ui data form" id="dataForm"> --}}
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<!-- pertama -->
		<div class="field">		
			<div class="required fields">
				<div class="eight wide field">
					<label>Kode Produk</label>
					<input style="background-color: #edf1f6;" type="text" name="produk_kode" placeholder="Inputkan Kode Produk" value="{{ $record->produk_kode or "" }}" disabled="">
				</div>
				<div class="eight wide field">
					<label>Assortment Type</label>
					<select class="ui full-width search selection dropdown" name="assortment_type_id">
						 {!! \Lotte\Models\Master\JenisAssortment::options('nama', 'id',['selected' => isset($record->assortment_type_id) ? $record->assortment_type_id : '',], '-- Pilih Assortment --') !!}
					</select>
				</div>
			</div>
		</div>
		<div class="field">	
			<input style="background-color: #edf1f6;" type="text" name="produk_kode" placeholder="Inputkan Kode Produk" value="{{ $record->produk->nama or "" }}" disabled="">
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>