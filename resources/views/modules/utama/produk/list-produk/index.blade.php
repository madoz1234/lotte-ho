@extends('layouts.grid')

@section('filters')

<div class="field">
	<input name="filter[produk_kode]" placeholder="Kode" type="text">
</div>

<div class="field">
	<input name="filter[nama]" placeholder="Nama Produk" type="text">
</div>

<div class="field">
	<select name="filter[tipe_produk]" class="ui fluid search selection dropdown">
		<option value="">-- Pilih Tipe Produk --</option>
		<option value="1">Produk GMD</option>
		<option value="0">Produk Non GMD</option>
	</select>
</div>

<div class="field">
	<select name="filter[tipe_barang_kode]" class="ui fluid search selection dropdown">
		{!! \Lotte\Models\Master\TipeBarang::options('nama', 'kode',[], '-- Pilih Tipe Barang --') !!}
	</select>
</div>

<div class="field">
		<select name="filter[jenis_barang_kode]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\JenisBarang::options('jenis', 'kode',[], '-- Pilih Jenis Barang --') !!}
    	</select>
	</div>

<div class="field">
	<select name="filter[bumun_nm]" class="ui fluid search selection dropdown">
		{!! \Lotte\Models\Master\DivisiProduk::options('nama', 'nama',[], '-- Pilih Division --') !!}
	</select>
</div>

<div class="field">
	<select name="filter[l1_nm]" class="ui fluid search selection dropdown">
		{!! \Lotte\Models\Master\Kategori1::options('nama', 'nama',[], '-- Pilih Kategori 1 --') !!}
	</select>
</div>

{{-- <div class="field">
	<select name="filter[l1_nm]" class="ui fluid search selection dropdown">
		{!! \Lotte\Models\Master\Kategori1::options('nama', 'nama',[], '-- Pilih tipe produk --') !!}
	</select>
</div> --}}



{{-- <div class="field">
	<input name="filter[bumun_nm]" placeholder="Division" type="text">
</div>

<div class="field">
	<input name="filter[l1_nm]" placeholder="Category 1" type="text">
</div> --}}

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
d.produk_kode = $("input[name='filter[produk_kode]']").val();
d.nama = $("input[name='filter[nama]']").val();
d.bumun_nm = $("select[name='filter[bumun_nm]']").val();
d.tipe_barang_kode = $("select[name='filter[tipe_barang_kode]']").val();
d.l1_nm = $("select[name='filter[l1_nm]']").val();
d.tipe_produk = $("select[name='filter[tipe_produk]']").val();
d.jenis_barang_kode = $("select[name='filter[jenis_barang_kode]']").val();
@endsection

@section('toolbars')
{{-- <button class="ui grey button" target="_blank"><i class="download icon icon"></i>Download</button>
<button type="button" class="ui blue button" target="_blank"><i class="upload icon icon"></i>Upload</button> --}}

{{-- <a href="{{ asset('template\produk.xls') }}" class="ui grey button" target="_blank"><i class="download icon icon"></i>Template</a>
<button type="button" class="ui blue importexcel button"><i class="upload icon icon"></i>Upload</button> --}}
<button type="button" class="ui green button" onclick="javascript:export_produk();">
	<i class="file excel outline icon"></i>
	Export Excel
</button>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		// tipe_barang_kode: {
		// 	identifier: 'tipe_barang_kode',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian tipe barang tidak boleh kosong'
		// 	}]
		// },
		// jenis_barang_kode: {
		// 	identifier: 'jenis_barang_kode',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian jenis barang tidak boleh kosong'
		// 	}]
		// },
		// uom1_satuan: {
		// 	identifier: 'uom1_satuan',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 satuan tidak boleh kosong'
		// 	}]
		// },
		// uom1_rak_id: {
		// 	identifier: 'uom1_rak_id',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 rak tidak boleh kosong'
		// 	}]
		// },
		// uom1_barcode: {
		// 	identifier: 'uom1_barcode',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 barcode tidak boleh kosong'
		// 	}]
		// },
		// uom1_conversion: {
		// 	identifier: 'uom1_conversion',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 conversion tidak boleh kosong'
		// 	}]
		// },
		// uom1_width: {
		// 	identifier: 'uom1_width',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 width tidak boleh kosong'
		// 	}]
		// },
		// uom1_height: {
		// 	identifier: 'uom1_height',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 height tidak boleh kosong'
		// 	}]
		// },
		// uom1_length: {
		// 	identifier: 'uom1_length',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 length tidak boleh kosong'
		// 	}]
		// },
		// uom1_weight: {
		// 	identifier: 'uom1_weight',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 weight tidak boleh kosong'
		// 	}]
		// },
		// uom1_boxtype: {
		// 	identifier: 'uom1_boxtype',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 boxtype tidak boleh kosong'
		// 	}]
		// },
		// uom1_file: {
		// 	identifier: 'uom1_file',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 1 file tidak boleh kosong'
		// 	}]
		// },
		// uom2_satuan: {
		// 	identifier: 'uom2_satuan',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 satuan tidak boleh kosong'
		// 	}]
		// },
		// uom2_rak_id: {
		// 	identifier: 'uom2_rak_id',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 rak tidak boleh kosong'
		// 	}]
		// },
		// uom2_barcode: {
		// 	identifier: 'uom2_barcode',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 barcode tidak boleh kosong'
		// 	}]
		// },
		// uom2_conversion: {
		// 	identifier: 'uom2_conversion',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 conversion tidak boleh kosong'
		// 	}]
		// },
		// uom2_width: {
		// 	identifier: 'uom2_width',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 width tidak boleh kosong'
		// 	}]
		// },
		// uom2_height: {
		// 	identifier: 'uom2_height',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 height tidak boleh kosong'
		// 	}]
		// },
		// uom2_length: {
		// 	identifier: 'uom2_length',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 length tidak boleh kosong'
		// 	}]
		// },
		// uom2_weight: {
		// 	identifier: 'uom2_weight',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 weight tidak boleh kosong'
		// 	}]
		// },
		// uom2_boxtype: {
		// 	identifier: 'uom2_boxtype',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 boxtype tidak boleh kosong'
		// 	}]
		// },
		// uom2_file: {
		// 	identifier: 'uom2_file',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian uom 2 file tidak boleh kosong'
		// 	}]
		// }
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
		$('.demo.menu .item').tab({
			history:false,
		});
	};
</script>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>
	$(document).ready(function() {
		tamplate_produk = function(){
				// alert('tara');
				// create form
				var form = document.createElement("form");
				form.setAttribute("method", 'POST');
				form.setAttribute("action", "{{ url('export/tamplate-produk') }}");
				form.setAttribute("target", "_blank");

				var csrf = document.createElement("input");
				csrf.setAttribute("type", "hidden");
				csrf.setAttribute("name", '_token');
				csrf.setAttribute("value", '{{ csrf_token() }}');
				form.appendChild(csrf);

				document.body.appendChild(form);
				form.submit();

				document.body.removeChild(form);
			}

			export_produk = function(){
				// alert('tara');
				// create form
				var form = document.createElement("form");
				form.setAttribute("method", 'POST');
				form.setAttribute("action", "{{ url('export/export-produk') }}");
				form.setAttribute("target", "_blank");

				var csrf = document.createElement("input");
				csrf.setAttribute("type", "hidden");
				csrf.setAttribute("name", '_token');
				csrf.setAttribute("value", '{{ csrf_token() }}');
				form.appendChild(csrf);

				var produk_kode = document.createElement("input");
				produk_kode.setAttribute("type", "hidden");
				produk_kode.setAttribute("name", 'produk_kode');
				produk_kode.setAttribute("value", $('[name="filter[produk_kode]"]').val());
				form.appendChild(produk_kode);

				var nama = document.createElement("input");
				nama.setAttribute("type", "hidden");
				nama.setAttribute("name", 'nama');
				nama.setAttribute("value", $('[name="filter[nama]"]').val());
				form.appendChild(nama);

				var bumun_nm = document.createElement("input");
				bumun_nm.setAttribute("type", "hidden");
				bumun_nm.setAttribute("name", 'bumun_nm');
				bumun_nm.setAttribute("value", $('[name="filter[bumun_nm]"]').val());
				form.appendChild(bumun_nm);

				var tipe_produk = document.createElement("input");
				tipe_produk.setAttribute("type", "hidden");
				tipe_produk.setAttribute("name", 'tipe_produk');
				tipe_produk.setAttribute("value", $('[name="filter[tipe_produk]"]').val());
				form.appendChild(tipe_produk);

				var tipe_barang_kode = document.createElement("input");
				tipe_barang_kode.setAttribute("type", "hidden");
				tipe_barang_kode.setAttribute("name", 'tipe_barang_kode');
				tipe_barang_kode.setAttribute("value", $("select[name='filter[tipe_barang_kode]']").val());
				form.appendChild(tipe_barang_kode);

				var jenis_barang_kode = document.createElement("input");
				jenis_barang_kode.setAttribute("type", "hidden");
				jenis_barang_kode.setAttribute("name", 'jenis_barang_kode');
				jenis_barang_kode.setAttribute("value", $("select[name='filter[jenis_barang_kode]']").val());
				form.appendChild(jenis_barang_kode);

				var l1_nm = document.createElement("input");
				l1_nm.setAttribute("type", "hidden");
				l1_nm.setAttribute("name", 'l1_nm');
				l1_nm.setAttribute("value", $("select[name='filter[l1_nm]']").val());
				form.appendChild(l1_nm);

				document.body.appendChild(form);
				form.submit();

				document.body.removeChild(form);
			}
		});

	$(document).on('click', '.edit-non-gmd', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/edit-non-gmd/" + id;

		loadModal(url);
	});
</script>
@append

@section('subcontent')
@parent
@endsection