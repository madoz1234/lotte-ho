<div class="ui grid">
	<div class="eight wide column">
		<!-- Barang -->
		<h4 class="ui blue dividing header">Barang</h4>
		<div class="two fields">
			<div class="field">
				<label>Tipe Barang</label>
				<select name="tipe_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\TipeBarang::options('nama', 'kode',['selected' => isset($record->tipe_barang_kode) ? $record->tipe_barang_kode : '0',], '-- Pilih --') !!}
				</select>
			</div>

			<div class="field">
				<label>Jenis Barang</label>
				<select name="jenis_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\JenisBarang::options('jenis', 'kode',['selected' => isset($record->jenis_barang_kode) ? $record->jenis_barang_kode : '0',], '-- Pilih --') !!}
				</select>
			</div>

			<div class="field">
				<label>Tipe Aset</label>
				@if ($record->jenis_barang_kode == 004)
					<select name="tipe_asset_id" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\TipeAset::options('tipe', 'id',['selected' => isset($record->tipe_asset_id) ? $record->tipe_asset_id : '0',], '-- Pilih --') !!}
					</select>
				@else
					<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Bukan Tipe Aset">
				@endif
			</div>

			<div class="field">
				<label>Devision</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Devision" value="{{ $record->produk->bumun_nm or '' }}">
			</div>
		</div>

		<div class="two fields">
			<div class="field">
				<label>Category 1</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Category 1" value="{{ $record->produk->l1_nm or '' }}">
			</div>

			<div class="field">
				<label>Category 2</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Category 2" value="{{ $record->produk->l2_nm or '' }}">
			</div>

			<div class="field">
				<label>Category 3</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Category 3" value="{{ $record->produk->l3_nm or '' }}">
			</div>

			<div class="field">
				<label>Category 4</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Category 4" value="{{ $record->produk->l4_nm or '' }}">
			</div>
		</div>

		<div class="two fields">
			<div class="four wide field">
				<label>Kode Barang</label>
				<input placeholder="Kode Barang" type="text" disabled="" style="background-color: #edf1f6;" value="{{ $record->produk->kode or '' }}">
			</div>

			<div class="twelve wide field">
				<label>Nama Barang</label>
				<input placeholder="Nama Barang" type="text" disabled="" style="background-color: #edf1f6;" value="{{ $record->produk->nama or '' }}">
			</div>
		</div>
	</div>

	<div class="eight wide column">
		<!-- Nomor Skunder-->
		<h4 class="ui blue dividing header">Barang Locked Price </h4>
		<div class="two fields">
			<div class="field">
				<label>Locked Price</label>
				<div class="ui radio checkbox">
					<input type="radio" name="locked_price" tabindex="0" class="hidden" value="1" {{ ($record->locked_price == 1 ) ? 'checked' : '' }}>
					<label>Ya</label>
				</div>&nbsp;&nbsp;&nbsp;&nbsp;
				<div class="ui radio checkbox">
					<input type="radio" name="locked_price" tabindex="0" class="hidden" value="0" {{ ($record->locked_price == 0 ) ? 'checked' : '' }}>
					<label>Tidak</label>
				</div>
			</div>
		</div>
		<!-- Moving Type -->
		<h4 class="ui blue dividing header">Set Margin </h4>
		<div class="two fields">
			<div class="field">
				<label>Fixed Margin (Suggested Selling Price)</label>
				<div class="ui right labeled input">
					<input type="text" name="margin" placeholder="Persentase Margin" value="{{ FormatNumber($record->margin) }}" onblur="tampil()">
					{{-- <input type="text" name="tipe_barang_kode" style="background-color: #edf1f6;" disabled="" placeholder="Category 1" value="{{ $record->tipe_barang_kode or '' }}"> --}}
					<div class="ui basic label">
						%
					</div>
					{{-- <p id="tampil" style="font-size: 13px; color: blue; margin-left: 20px; margin-top: 15px;"></p>
					<p id="tampil" style="font-size: 13px; color: blue; margin-left: 1px; margin-top: 15px;">%</p> --}}
				</div>
			</div>
		</div>
		<!-- Moving Type -->
	</div>

	{{-- <div class="ui error message"></div> --}}
</div>

<script type="text/javascript">
	$(document).ready(function(){

		// Rak uom 1
		$('select[name="uom1_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving1"]').val(response.shelving1)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		tampil = function(){
			var v = $('[name="margin"]').val();

			$('#tampil').html(v)
		}

		// Rak uom 2
		$('select[name="uom2_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving2"]').val(response.shelving2)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rak uom 3
		$('select[name="uom3_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving3"]').val(response.shelving3)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rak uom 4
		$('select[name="uom4_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving4"]').val(response.shelving4)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

})
</script>