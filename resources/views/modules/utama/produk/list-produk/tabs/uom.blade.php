<div class="header">
	<div style="float: right;">
		<button type="button" class="ui blue add button uomp">
			<i class="plus icon"></i>
			UOM
		</button>
		<button type="button" class="ui red remove button uomp hidden">
			<i class="minus icon"></i>
			UOM
		</button>
	</div><br>
</div>
<br><br>

<div class="ui two cards">
	<!-- uom 1 -->
	<div class="card">
		<div class="content">
			<div class="header" style="text-align: center;">UOM 1</div>
			<br/>
			<div class="ui form">
				<div class="sixteen fields">
					<div class="ten wide field">
						<label>Product Description</label>
						<input placeholder="Product Description" type="text" name="uom1_produk_description" value="{{ isset($record->uom1_produk_description) ? $record->uom1_produk_description : $record->produk->nama }}">
					</div>

					<div class="six wide field">
						<label>Description</label>
						<select name="uom1_satuan" class="ui fluid search selection dropdown" style="width: 100%;">
							{!! \Lotte\Models\Master\UnitUom::options('nama', 'nama',['selected' => isset($record->uom1_satuan) ? $record->uom1_satuan : '0',], '-- Pilih --') !!}
						</select>
					</div>
				</div>

				<h4 class="ui blue dividing header">Headling Type UOM 1</h4>
				<div class="fields">
					<div class="eight wide field">
						<div class="two fields">
							<div class="field">
								<div class="ui checkbox">
								{{-- <input type="checkbox" name="uom1_selling_cek" value="{{ isset($record->uom1_selling_cek) ? $record->uom1_selling_cek : '0' }}" {{ $record->uom1_selling_cek == '1' ? 'checked="1"' : '' }}> --}}
								{{-- <input type="checkbox" value="0" name="uom1_selling_cek" {{ old($record->uom1_selling_cek) === '1' ? 'checked="checked"' : '' }} > --}}
								{{-- <input type="checkbox" value="1" name="uom1_selling_cek" {{ old('uom1_selling_cek', $record->uom1_selling_cek) === '0' ? 'checked' : '' }} > --}}
								<input type="checkbox" name="uom1_selling_cek" value="1" {{ $record->uom1_selling_cek == '1' ? 'checked="1"' : '' }}>
									<label>Selling Unit</label>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<input type="checkbox" name="uom1_order_cek" value="1" {{ $record->uom1_order_cek == '1' ? 'checked="1"' : '' }} disabled="">
									<label>Order Unit</label>
								</div>
							</div>
						</div>
					</div>
					<div class="eight wide field">
						<div class="two fields">
							<div class="field">
								<div class="ui checked checkbox">
									<input type="checkbox" name="uom1_receiving_cek" value="1" {{ $record->uom1_receiving_cek == '1' ? 'checked="1"' : '' }} disabled="">
									<label>Receiving Unit</label>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<input type="checkbox" name="uom1_return_cek" value="1" {{ $record->uom1_return_cek == '1' ? 'checked="1"' : '' }} disabled="">
									<label>Retrun Unit</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="field">
					<div class="ui checked checkbox">
						<input type="checkbox" name="uom1_inventory_cek" value="1" {{ $record->uom1_inventory_cek == '1' ? 'checked="1"' : '' }}>
						<label>Inventory Unit</label>
					</div>
				</div>

				{{-- <div class="two fields">
					<div class="field">
						<label>Rak Type</label>
						<select name="uom1_rak_id" class="ui fluid search selection dropdown" style="width: 100%;">
							{!! \Lotte\Models\Master\Rak::options('tipe_rak', 'id',['selected' => isset($record->uom1_rak_id) ? $record->uom1_rak_id : '',], '-- Pilih --') !!}
						</select>
					</div>

					<div class="field">
						<label>Shelving Rak Type</label>
						<input placeholder="Shelving Rak Type" name="shelving1" type="text" style="background-color: #edf1f6;" readonly="" value="{{ $record->rak->shelving or '' }}">
					</div>
				</div> --}}

				{{-- <div class="two fields"> --}}
					<div class="field">
						<label>Internal Barcode</label>
						<input type="text" name="uom1_barcode" placeholder="Inputkan" value="{{ $record->uom1_barcode or '' }}" onkeypress="return isNumberKey(event)">
					</div>

					{{-- <div class="field">
						<label>Conversion</label>
						<input type="number" name="uom1_conversion" placeholder="Inputkan" value="{{ $record->uom1_conversion or '' }}">
					</div> --}}
				{{-- </div> --}}

				<div class="two fields">
					<div class="field">
						<label>Width UOM 1</label>
						<div class="ui right labeled input">
							<input type="text" name="uom1_width" placeholder="Inputkan" value="{{ $record->uom1_width or '0' }}" data-inputmask="'alias' : 'numeric'">
							<div class="ui basic label">
								mm
							</div>
						</div>
					</div>

					<div class="field">
						<label>Length UOM 1</label>
						<div class="ui right labeled input">
							<input type="text" name="uom1_length" placeholder="Inputkan" value="{{ $record->uom1_length or '0' }}" data-inputmask="'alias' : 'numeric'">
							<div class="ui basic label">
								mm
							</div>
						</div>
					</div>
				</div>

				<div class="two fields">
					<div class="field">
						<label>Height UOM 1</label>
						<div class="ui right labeled input">
							<input type="text" name="uom1_height" placeholder="Inputkan" value="{{ $record->uom1_height or '0' }}" data-inputmask="'alias' : 'numeric'">
							<div class="ui basic label">
								mm
							</div>
						</div>
					</div>

					<div class="field">
						<label>Weight UOM 1</label>
						<div class="ui right labeled input">
							<input type="text" name="uom1_weight" placeholder="Inputkan" value="{{ $record->uom1_weight or '0' }}" data-inputmask="'alias' : 'numeric'">
							<div class="ui basic label">
								gr
							</div>
						</div>
					</div>
				</div>

				<div class="two fields">
					<div class="field">
						<label>Box Type</label>
						<div class="inline fields">
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="uom1_boxtype" tabindex="0" class="hidden" value="1" {{ ($record->uom1_boxtype == 1 ) ? 'checked' : '' }}>
									<label>Outerbox</label>
								</div>
							</div>

							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="uom1_boxtype" tabindex="0" class="hidden" value="0" {{ ($record->uom1_boxtype == 0 ) ? 'checked' : '' }}>
									<label>Inerbox</label>
								</div>
							</div>
						</div>
					</div>
					<div class="field">
						<label>File Gambar</label>
						<input type="file" name="uom1_file">
						{{-- <img class="ui small image" src="{{ asset('img/profile.png') }}" style="width: 10px center"> --}}
						{{-- <img class="ui small image" src="{{ (!is_null($record->uom1_file))?url('storage/'.$record->uom1_file):asset('img/no_image.png') }}" height="auto" width="100%"> --}}
						<img class="ui small image" src="{{ ($record->uom1_file) ? url('storage/'.$record->uom1_file) : asset('img/no_image.png') }}" height="auto" width="100%">
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- uom 1 -->

	<!-- uom 2 -->
	<div class="card">
		<div class="content">
			<div class="header" style="text-align: center;">UOM 2</div>
			<br/>
			<div class="ui form">
				<div class="sixteen fields">
					<div class="ten wide field">
						<label>Product Description</label>
						<input placeholder="Product Description" readonly="" type="text" style="background-color: #edf1f6;" {{-- name="uom2_produk_description" --}} value="{{ isset($record->uom2_produk_description) ? $record->uom2_produk_description : $record->produk->nama }}">
					</div>

					<div class="six wide field">
						<label>Description</label>
						<input placeholder="Product Description" readonly="" type="text" style="background-color: #edf1f6;" {{-- name="uom2_produk_description" --}} value="{{ $record->uom2_satuan }}">

						{{-- <select name="uom2_satuan" class="ui fluid search selection dropdown" style="width: 100%;">
							{!! \Lotte\Models\Master\UnitUom::options('nama', 'nama',['selected' => isset($record->uom2_satuan) ? $record->uom2_satuan : '0',], '-- Pilih --') !!}
						</select> --}}
					</div>
				</div>

				<h4 class="ui blue dividing header">Headling Type UOM 2</h4>
				<div class="fields">
					<div class="eight wide field">
						<div class="two fields">
							<div class="field">
								<div class="ui checkbox">
									<input type="checkbox" {{-- name="uom2_selling_cek" --}} value="1" {{ $record->uom2_selling_cek == '1' ? 'checked="1"' : '' }} disabled=""> 
									<label>Selling Unit</label>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<input type="checkbox" {{-- name="uom2_order_cek" --}} value="1" {{ $record->uom2_order_cek == '1' ? 'checked="1"' : '' }} disabled="">
									<label>Order Unit</label>
								</div>
							</div>
						</div>
					</div>
					<div class="eight wide field">
						<div class="two fields">

							<div class="field">
								<div class="ui checked checkbox">
									<input type="checkbox" {{-- name="uom2_receiving_cek" --}} value="1" {{ $record->uom2_receiving_cek == '1' ? 'checked="1"' : '' }} disabled="">
									<label>Receiving Unit</label>
								</div>
							</div>

							<div class="field">
								<div class="ui checked checkbox">
									<input type="checkbox" {{-- name="uom2_return_cek" --}} value="1" {{ $record->uom2_return_cek == '1' ? 'checked="1"' : '' }} disabled="">
									<label>Retrun Unit</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="field">
					<div class="ui checked checkbox">
						<input type="checkbox" {{-- name="uom2_inventory_cek" --}} value="1" {{ $record->uom2_inventory_cek == '1' ? 'checked="1"' : '' }} disabled="">
						<label>Inventory Unit</label>
					</div>
				</div>

				{{-- <div class="two fields">
					<div class="field">
						<label>Rak Type</label>
						<select name="uom2_rak_id" class="ui fluid search selection dropdown" style="width: 100%;">
							{!! \Lotte\Models\Master\Rak::options('tipe_rak', 'id',['selected' => isset($record->uom2_rak_id) ? $record->uom2_rak_id : '',], '-- Pilih --') !!}
						</select>
					</div>

					<div class="field">
						<label>Shelving Rak Type</label>
						<input placeholder="Jumlah" name="shelving2" type="text" style="background-color: #edf1f6;" readonly="" value="{{ $record->rak->shelving or '' }}">
					</div>
				</div> --}}


				<div class="two fields">
					<div class="field">
						<label>Internal Barcode</label>
						<input type="text" {{-- name="uom2_barcode" --}} style="background-color: #edf1f6;" readonly="" placeholder="Inputkan" value="{{ $record->uom2_barcode or '' }}" onkeypress="return isNumberKey(event)">
					</div>

					<div class="field">
						<label>Conversion</label>
						<input type="number" name="uom2_conversion" {{-- name="uom2_conversion" --}} placeholder="Inputkan" value="{{ $record->uom2_conversion or '' }}">
					</div>
				</div>

				<div class="two fields">
					<div class="field">
						<label>Width UOM 2</label>
						<div class="ui right labeled input">
							<input type="text" style="background-color: #edf1f6;" {{-- name="uom2_width" --}} placeholder="Inputkan" readonly="" value="{{ $record->produk->width or '' }}" data-inputmask="'alias' : 'numeric'">
							<div class="ui basic label">
								mm
							</div>
						</div>
					</div>

					<div class="field">
						<label>Length UOM 2</label>
						<div class="ui right labeled input">
							<input type="text" style="background-color: #edf1f6;" {{-- name="uom2_length" --}} placeholder="Inputkan" readonly="" value="{{ $record->produk->length or '' }}" data-inputmask="'alias' : 'numeric'">
							<div class="ui basic label">
								mm
							</div>
						</div>
					</div>
				</div>

				<div class="two fields">
					<div class="field">
						<label>Height UOM 2</label>
						<div class="ui right labeled input">
							<input type="text" style="background-color: #edf1f6;" {{-- name="uom2_height" --}} placeholder="Inputkan" readonly="" value="{{ $record->produk->height or '' }}" data-inputmask="'alias' : 'numeric'">
							<div class="ui basic label">
								mm
							</div>
						</div>
					</div>

					<div class="field">
						<label>Weight UOM 2</label>
						<div class="ui right labeled input">
							<input type="text" style="background-color: #edf1f6;" {{-- name="uom2_weight" --}} placeholder="Inputkan" readonly="" value="{{ $record->produk->wg or '' }}" data-inputmask="'alias' : 'numeric'">
							<div class="ui basic label">
								gr
							</div>
						</div>
					</div>
				</div>

				<div class="two fields">
					<div class="field">
						<label>Box Type</label>
						<div class="inline fields">
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" {{-- name="uom2_boxtype" --}} tabindex="0" disabled="" class="hidden" value="1" {{ ($record->uom2_boxtype == 1 ) ? 'checked' : '' }}>
									<label>Outerbox</label>
								</div>
							</div>

							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" {{-- name="uom2_boxtype" --}} tabindex="0" disabled="" class="hidden" value="0" {{ ($record->uom2_boxtype == 0 ) ? 'checked' : '' }}>
									<label>Inerbox</label>
								</div>
							</div>
						</div>
					</div>
					{{-- <div class="field">
						<label>File Gambar</label>
						<input type="file" name="uom2_file">
						<img class="ui small image" src="{{ ($record->uom2_file != "") ? url('storage/'.$record->uom2_file) : asset('img/no_image.png') }}" height="auto" width="100%">
					</div> --}}
				</div>


			</div>
		</div>
	</div>
	<!-- uom 2 -->
</div>

<script type="text/javascript">
	$(document).ready(function(){		
		// Rak uom 1
		$('select[name="uom1_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving1"]').val(response.shelving1)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rak uom 2
		$('select[name="uom2_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving2"]').val(response.shelving2)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rak uom 3
		$('select[name="uom3_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving3"]').val(response.shelving3)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rak uom 4
		$('select[name="uom4_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving4"]').val(response.shelving4)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		$(document).on('click', '.add.button.uomp', function(e) {
			addUomp();
		});

		$(document).on('click', '.remove.button.uomp', function(e) {
			removeUomp();
		});

		function addUomp(){
		var html = `<div class='ui two cards add'>
		<div class='card'>
					<div class='content'>
						<div class='header' style='text-align: center;'>UOM 3</div>
						<br/>
						<div class='ui form'>
							<div class='sixteen fields'>
								<div class='ten wide field'>
									<label>Product Description</label>
									<input type='text' name='produk_deskripsi' style="background-color: #edf1f6;" value='{{ $record->produk->nama or '' }}'>
								</div>
								<div class='six wide field'>
									<label>Description</label>
									<select name='uom3_satuan' class='ui fluid search selection dropdown' style='width: 100%;'>
										{!! \Lotte\Models\Master\UnitUom::options('nama', 'nama',['selected' => isset($record->uom3_satuan) ? $record->uom3_satuan : '',], '-- Pilih --') !!}
									</select>
								</div>
							</div>
							<h4 class='ui blue dividing header'>Headling Type UOM 3</h4>
							<div class='fields'>
							<div class='eight wide field'>
								<div class='two fields'>
									<div class='field'>
										<div class='ui checkbox'>
											<input type='checkbox' name='uom3_selling_cek' value='1' {{ $record->uom3_selling_cek == '1' ? 'checked="1"' : '' }}> 
											<label>Selling Unit</label>
										</div>
									</div>

									<div class='field'>
										<div class='ui checked checkbox'>
											<input type='checkbox' name='uom3_order_cek' value='1' {{ $record->uom3_order_cek == '1' ? 'checked="1"' : '' }}>
											<label>Order Unit</label>
										</div>
									</div>
								</div>
							</div>
							<div class='eight wide field'>
								<div class='two fields'>

									<div class='field'>
										<div class='ui checked checkbox'>
											<input type='checkbox' name='uom3_receiving_cek' value='1' {{ $record->uom3_receiving_cek == '1' ? 'checked="1"' : '' }}>
											<label>Receiving Unit</label>
										</div>
									</div>

									<div class="field">
										<div class='ui checked checkbox'>
											<input type='checkbox' name='uom3_return_cek' value='1' {{ $record->uom3_return_cek == '1' ? 'checked="1"' : '' }}>
											<label>Retrun Unit</label>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class='field'>
							<div class='ui checked checkbox'>
								<input type='checkbox' name='uom3_inventory_cek' value='1' {{ $record->uom3_inventory_cek == '1' ? 'checked="1"' : '' }}>
								<label>Inventory Unit</label>
							</div>
						</div>
							<!--<div class='two fields'>
									<div class='field'>
										<label>Tipe Rak</label>
										<select name='uom3_rak_id' class='ui fluid search selection dropdown' style='width: 100%;'>
											{!! \Lotte\Models\Master\Rak::options('tipe_rak', 'id',['selected' => isset($record->uom3_rak_id) ? $record->uom3_rak_id : '',], '-- Pilih --') !!}
										</select>
									</div>
									<div class='field'>
										<label>Tipe Shelfing</label>
										<input placeholder="Jumlah" name="shelving3" type="text" style="background-color: #edf1f6;" readonly="" value="{{ $record->rak->shelving or '' }}">
									</div>
							</div>--!>
							<div class='two fields'>
								<div class='field'>
									<label>Internal Barcode</label>
									<input type='text' name='uom3_barcode' placeholder='Inputkan' value='{{ $record->uom3_barcode or '0' }}' onkeypress="return isNumberKey(event)">
								</div>
								<div class='field'>
									<label>Conversion</label>
									<input type='number' name='uom3_conversion' placeholder='Inputkan' value='{{ $record->uom3_conversion or '0' }}'>
								</div>
							</div>
							<div class='two fields'>
								<div class='field'>
									<label>Width UOM 3</label>
									<div class='ui right labeled input'>
										<input type='text' name='uom3_width' placeholder='Inputkan' value='{{ $record->uom3_width or '0' }}' data-inputmask="'alias' : 'numeric'">
										<div class='ui basic label'>
											mm
										</div>
									</div>
								</div>
								<div class='field'>
									<label>Length UOM 3</label>
									<div class='ui right labeled input'>
										<input type='text' name='uom3_length' placeholder='Inputkan' value='{{ $record->uom3_length or '0' }}' data-inputmask="'alias' : 'numeric'">
										<div class='ui basic label'>
											mm
										</div>
									</div>
								</div>
							</div>
							<div class='two fields'>
								<div class='field'>
									<label>Height UOM 3</label>
									<div class='ui right labeled input'>
										<input type='text' name='uom3_height' placeholder='Inputkan' value='{{ $record->uom3_height or '0' }}' data-inputmask="'alias' : 'numeric'">
										<div class='ui basic label'>
											mm
										</div>
									</div>
								</div>
								<div class='field'>
									<label>Weight UOM 3</label>
									<div class='ui right labeled input'>
										<input type='text' name='uom3_weight' placeholder='Inputkan' value='{{ $record->uom3_weight or '0' }}' data-inputmask="'alias' : 'numeric'">
										<div class='ui basic label'>
											gr
										</div>
									</div>
								</div>
							</div>
							<div class='two fields'>
								<div class='field'>
									<label>Box Type</label>
									<div class="inline fields">
										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="uom3_boxtype" tabindex="0" class="hidden" value="1" {{ ($record->uom3_boxtype == 1 ) ? 'checked' : '' }}>
												<label>Outerbox</label>
											</div>
										</div>

										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="uom3_boxtype" tabindex="0" class="hidden" value="0" {{ ($record->uom3_boxtype == 0 ) ? 'checked' : '' }}>
												<label>Inerbox</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class='card'>
					<div class='content'>
						<div class='header' style='text-align: center;'>UOM 4</div>
						<br/>
						<div class='ui form'>
							<div class='sixteen fields'>
								<div class='ten wide field'>
									<label>Product Description</label>
									<input type='text' name='produk_deskripsi' style="background-color: #edf1f6;" value='{{ $record->produk->nama or '' }}'>
								</div>
								<div class='six wide field'>
									<label>Description</label>
									<select name='uom4_satuan' class='ui fluid search selection dropdown' style='width: 100%;'>
										{!! \Lotte\Models\Master\UnitUom::options('nama', 'nama',['selected' => isset($record->uom4_satuan) ? $record->uom4_satuan : '',], '-- Pilih --') !!}
									</select>
								</div>
							</div>
							<h4 class='ui blue dividing header'>Headling Type UOM 4</h4>
							<div class='fields'>
								<div class='eight wide field'>
									<div class='two fields'>
										<div class='field'>
											<div class='ui checkbox'>
												<input type='checkbox' name='uom4_selling_cek' value='1' {{ $record->uom4_selling_cek == '1' ? 'checked="1"' : '' }}> 
												<label>Selling Unit</label>
											</div>
										</div>

										<div class='field'>
											<div class='ui checked checkbox'>
												<input type='checkbox' name='uom4_order_cek' value='1' {{ $record->uom4_order_cek == '1' ? 'checked="1"' : '' }}>
												<label>Order Unit</label>
											</div>
										</div>
									</div>
								</div>
								<div class='eight wide field'>
									<div class='two fields'>

										<div class='field'>
											<div class='ui checked checkbox'>
												<input type='checkbox' name='uom4_receiving_cek' value='1' {{ $record->uom4_receiving_cek == '1' ? 'checked="1"' : '' }}>
												<label>Receiving Unit</label>
											</div>
										</div>

										<div class='field'>
											<div class='ui checked checkbox'>
												<input type='checkbox' name='uom4_return_cek' value='1' {{ $record->uom4_return_cek == '1' ? 'checked="1"' : '' }}>
												<label>Retrun Unit</label>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class='field'>
								<div class='ui checked checkbox'>
									<input type='checkbox' name='uom4_inventory_cek' value='1' {{ $record->uom4_inventory_cek == '1' ? 'checked="1"' : '' }}>
									<label>Inventory Unit</label>
								</div>
							</div>
							<!--<div class='two fields'>
									<div class='field'>
										<label>Tipe Rak</label>
										<select name='uom4_rak_id' class='ui fluid search selection dropdown' style='width: 100%;'>
											{!! \Lotte\Models\Master\Rak::options('tipe_rak', 'id',['selected' => isset($record->uom4_rak_id) ? $record->uom4_rak_id : '',], '-- Pilih --') !!}
										</select>
									</div>
									<div class='field'>
										<label>Tipe Shelfing</label>
										<input placeholder="Jumlah" name="shelving4" type="text" style="background-color: #edf1f6;" readonly="" value="{{ $record->rak->shelving or '' }}">
									</div>
							</div>--!>
							<div class='two fields'>
								<div class='field'>
									<label>Internal Barcode</label>
									<input type='text' name='uom4_barcode' placeholder='Inputkan' value='{{ $record->uom4_barcode or '' }}' onkeypress="return isNumberKey(event)">
								</div>
								<div class='field'>
									<label>Conversion</label>
									<input type='number' name='uom4_conversion' placeholder='Inputkan' value='{{ $record->uom4_conversion or '' }}'>
								</div>
							</div>
							<div class='two fields'>
								<div class='field'>
									<label>Width UOM 4</label>
									<div class='ui right labeled input'>
										<input type='text' name='uom4_width' placeholder='Inputkan' value='{{ $record->uom4_width or '0' }}' data-inputmask="'alias' : 'numeric'">
										<div class='ui basic label'>
											mm
										</div>
									</div>
								</div>
								<div class='field'>
									<label>Length UOM 4</label>
									<div class='ui right labeled input'>
										<input type='text' name='uom4_length' placeholder='Inputkan' value='{{ $record->uom4_length or '0' }}' data-inputmask="'alias' : 'numeric'">
										<div class='ui basic label'>
											mm
										</div>
									</div>
								</div>
							</div>
							<div class='two fields'>
								<div class='field'>
									<label>Height UOM 4</label>
									<div class='ui right labeled input'>
										<input type='text' name='uom4_height' placeholder='Inputkan' value='{{ $record->uom4_height or '0' }}' data-inputmask="'alias' : 'numeric'">
										<div class='ui basic label'>
											mm
										</div>
									</div>
								</div>
								<div class='field'>
									<label>Weight UOM 4</label>
									<div class='ui right labeled input'>
										<input type='text' name='uom4_weight' placeholder='Inputkan' value='{{ $record->uom4_weight or '0' }}' data-inputmask="'alias' : 'numeric'">
										<div class='ui basic label'>
											gr
										</div>
									</div>
								</div>
							</div>
							<div class='two fields'>
								<div class='field'>
									<label>Box Type</label>
									<div class="inline fields">
										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="uom4_boxtype" tabindex="0" class="hidden" value="1" {{ ($record->uom4_boxtype == 1 ) ? 'checked' : '' }}>
												<label>Outerbox</label>
											</div>
										</div>

										<div class="field">
											<div class="ui radio checkbox">
												<input type="radio" name="uom4_boxtype" tabindex="0" class="hidden" value="0" {{ ($record->uom4_boxtype == 0 ) ? 'checked' : '' }}>
												<label>Inerbox</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				</div>`
				
		$( ".uom" ).append(html);
		$('.add.button.uomp').addClass('hidden');
		$('.remove.button.uomp').removeClass('hidden');
	}

	function removeUomp(){
    	$('.ui.two.cards.add').remove();
		$('.remove.button.uomp').addClass('hidden');
		$('.add.button.uomp').removeClass('hidden');
	}

})
</script>