<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Data Produk</div>
<div class="ui pointing secondary demo menu">
	<a class="active blue item" data-tab="general">General Settings</a>
	{{-- <a class="blue item" data-tab="reorder">Reorder Level</a> --}}
	<a class="blue item" data-tab="uom">UoM</a>
</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	<div class="ui error message"></div>
		{!! csrf_field() !!}
	    <input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">

			<div class="ui active tab" data-tab="general">
				@include('modules.utama.produk.list-produk.tabs.general')
			</div>

			{{-- <div class="ui tab" data-tab="reorder">
				@include('modules.utama.produk.list-produk.tabs.reorder')
			</div> --}}

			<div class="ui tab uom" data-tab="uom">
				@include('modules.utama.produk.list-produk.tabs.uom')
			</div>
	</form>
</div>

<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		// Rak uom 1
		$('select[name="uom1_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving1"]').val(response.shelving1)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rak uom 2
		$('select[name="uom2_rak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="shelving2"]').val(response.shelving2)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})
	})
</script>

@include('layouts.scripts.inputmask')
