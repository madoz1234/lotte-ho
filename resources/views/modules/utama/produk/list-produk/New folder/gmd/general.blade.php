<div class="ui grid">
	<div class="eight wide column">
		<!-- Barang -->
		<h4 class="ui blue dividing header">Barang</h4>
		<div class="two fields">
			<div class="field">
				<label>Tipe Barang</label>
				<select name="tipe_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
				</select>
			</div>

			<div class="field">
				<label>Jenis Barang</label>
				<select name="jenis_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
				</select>
			</div>

			<div class="field">
				<label>Devision</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Devision" value="">
			</div>
		</div>

		<div class="two fields">
			<div class="field">
				<label>Category 1</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Category 1" value="">
			</div>

			<div class="field">
				<label>Category 2</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Category 2" value="">
			</div>

			<div class="field">
				<label>Category 3</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Category 3" value="">
			</div>

			<div class="field">
				<label>Category 4</label>
				<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Category 4" value="">
			</div>
		</div>

		<div class="two fields">
			<div class="four wide field">
				<label>Kode Barang</label>
				<input placeholder="Kode Barang" type="text" disabled="" style="background-color: #edf1f6;" value="">
			</div>

			<div class="twelve wide field">
				<label>Nama Barang</label>
				<input placeholder="Nama Barang" type="text" disabled="" style="background-color: #edf1f6;" value="">
			</div>
		</div>
	</div>

	<div class="eight wide column">
		<!-- Nomor Skunder-->
		<h4 class="ui blue dividing header">Barang Locked Price </h4>
		<div class="two fields">
			<div class="field">
				<label>Locked Price</label>
				<div class="ui radio checkbox">
					<input type="radio" name="locked_price" tabindex="0" class="hidden" value="1" checked>
					<label>Ya</label>
				</div>&nbsp;&nbsp;&nbsp;&nbsp;
				<div class="ui radio checkbox">
					<input type="radio" name="locked_price" tabindex="0" class="hidden" value="0">
					<label>Tidak</label>
				</div>
			</div>
		</div>
		<!-- Moving Type -->
		<h4 class="ui blue dividing header">Set Margin </h4>
		<div class="two fields">
			<div class="field">
				<label>Margin</label>
				<div class="ui right labeled input">
					<input type="number" name="margin" placeholder="Persentase Margin" value="" onblur="tampil()">
					{{-- <input type="text" name="tipe_barang_kode" style="background-color: #edf1f6;" disabled="" placeholder="Category 1" value="{{ $record->tipe_barang_kode or '' }}"> --}}
					<div class="ui basic label">
						%
					</div>
					<p id="tampil" style="font-size: 13px; color: blue; margin-left: 20px; margin-top: 10px;"></p>
					<p id="" style="font-size: 13px; color: blue; margin-left: 1px; margin-top: 10px;">%</p>
				</div>
			</div>
		</div>
		<!-- Moving Type -->
	</div>

	<div class="ui error message"></div>
</div>

