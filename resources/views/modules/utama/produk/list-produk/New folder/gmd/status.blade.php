function addUomp(){
		$( ".ui.data.form" ).append("<div class='ui two cards add'>"+
		"<div class='card'>"+
					"<div class='content'>"+
						"<div class='header' style='text-align: center;'>UOM 3</div>"+
						"<br/>"+
						"<div class='ui form'>"+
							"<div class='inline fields'>"+
								"<div class='ten wide field'>"+
									"<label>Product Description</label>"+
									"<input type='text' name='produk_deskripsi'>"+
								"</div>"+
								"<div class='six wide field'>"+
									"<label>Description</label>"+
									"<div class='ui fluid search selection dropdown'>"+
										"<input type='hidden' name='deskripsi'>"+
										"<i class='dropdown icon'></i>"+
										"<div class='default text'>Pilih</div>"+
										"<div class='menu'>"+
											"<div class='item' data-value="1">Box</div>"+
											"<div class='item' data-value="2">Carton</div>"+
											"<div class='item' data-value="3">Dus</div>"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<h4 class='ui blue dividing header'>Headling Type UOM 3</h4>"+
							"<div class='fields'>"+
								"<div class='eight wide field'>"+
									"<div class='two fields'>"+
										"<div class='field'>"+
											"<div class='ui checked checkbox'>"+
												"<label>Selling Unit</label><input type='checkbox'>"+
											"</div>"+
										"</div>"+
										"<div class='field'>"+
											"<div class='ui checked checkbox'>"+
												"<label>Receiving Unit</label><input type='checkbox'>"+
											"</div>"+
										"</div>"+
									"</div>"+
								"</div>"+
								"<div class='eight wide field'>"+
									"<div class='two fields'>"+
										"<div class='field'>"+
											"<div class='ui checked checkbox'>"+
												"<label>Order Unit</label><input type='checkbox'>"+
											"</div>"+
										"</div>"+
										"<div class='field'>"+
											"<div class='ui checked checkbox'>"+
												"<label>Retrun Unit</label><input type='checkbox'>"+
											"</div>"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<div class='field'>"+
								"<div class='ui checked checkbox'>"+
									"<label>Shelfing Rack Type Unit</label><input type='checkbox'>"+
								"</div>"+
							"</div>"+
							"<div class='two fields'>"+
								"<div class='field'>"+
									"<label>Internal Barcode</label>"+
									"<input type='number' name='internal_barcode' placeholder='Inputkan'>"+
								"</div>"+
								"<div class='field'>"+
									"<label>Conversion</label>"+
									"<input type='number' name='conversion' placeholder='Inputkan'>"+
								"</div>"+
							"</div>"+
							"<div class='two fields'>"+
								"<div class='field'>"+
									"<label>Width UOM 3</label>"+
									"<div class='ui right labeled input'>"+
										"<input type='number' name='width' placeholder='Inputkan'>"+
										"<div class='ui basic label'>"+
											"cm"+
										"</div>"+
									"</div>"+
								"</div>"+
								"<div class='field'>"+
									"<label>Length UOM 3</label>"+
									"<div class='ui right labeled input'>"+
										"<input type='number' name='length' placeholder='Inputkan'>"+
										"<div class='ui basic label'>"+
											"cm"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<div class='two fields'>"+
								"<div class='field'>"+
									"<label>Height UOM 3</label>"+
									"<div class='ui right labeled input'>"+
										"<input type='number' name='height' placeholder='Inputkan'>"+
										"<div class='ui basic label'>"+
											"cm"+
										"</div>"+
									"</div>"+
								"</div>"+
								"<div class='field'>"+
									"<label>Weight UOM 3</label>"+
									"<div class='ui right labeled input'>"+
										"<input type='number' name='weight' placeholder='Inputkan'>"+
										"<div class='ui basic label'>"+
											"gr"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<div class='three fields'>"+
								"<div class='field'>"+
									"<label>Harga</label>"+
									"<input type='number' name='height' placeholder='Inputkan'>"+
								"</div>"+
								"<div class='field'>"+
									"<label>Box Type</label>"+
									"<div class='inline fields'>"+
										"<div class='field'>"+
											"<div class='ui radio checkbox'>"+
												"<input type='radio' name='box_type' tabindex="0" class='hidden' value="1" checked>"+
												"<label>Outerbox</label>"+
											"</div>"+
										"</div>"+
										"<div class='field'>"+
											"<div class='ui radio checkbox'>"+
												"<input type='radio' name='box_type' tabindex="0" class='hidden' value="0">"+
												"<label>Inerbox</label>"+
											"</div>"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<div class='field'>"+
								"<label>File Gambar</label>"+
								"<input type='file' name='akun_diskon'>"+
							"</div>"+
						"</div>"+


					"</div>"+
				"</div>"+
				"<div class='card'>"+
					"<div class='content'>"+
						"<div class='header' style='text-align: center;'>UOM 4</div>"+
						"<br/>"+
						"<div class='ui form'>"+
							"<div class='sixteen fields'>"+
								"<div class='ten wide field'>"+
									"<label>Product Description</label>"+
									"<input type='text' name='produk_deskripsi'>"+
								"</div>"+
								"<div class='six wide field'>"+
									"<label>Description</label>"+
									"<div class='ui fluid search selection dropdown'>"+
										"<input type='hidden' name='deskripsi'>"+
										"<i class='dropdown icon'></i>"+
										"<div class='default text'>Pilih</div>"+
										"<div class='menu'>"+
											"<div class='item' data-value="1">Box</div>"+
											"<div class='item' data-value="2">Carton</div>"+
											"<div class='item' data-value="3">Dus</div>"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<h4 class='ui blue dividing header'>Headling Type UOM 4</h4>"+
							"<div class='fields'>"+
								"<div class='eight wide field'>"+
									"<div class='two fields'>"+
										"<div class='field'>"+
											"<div class='ui checked checkbox'>"+
												"<label>Selling Unit</label><input type='checkbox'>"+
											"</div>"+
										"</div>"+
										"<div class='field'>"+
											"<div class='ui checked checkbox'>"+
												"<label>Receiving Unit</label><input type='checkbox'>"+
											"</div>"+
										"</div>"+
									"</div>"+
								"</div>"+
								"<div class='eight wide field'>"+
									"<div class='two fields'>"+
										"<div class='field'>"+
											"<div class='ui checked checkbox'>"+
												"<label>Order Unit</label><input type='checkbox'>"+
											"</div>"+
										"</div>"+
										"<div class='field'>"+
											"<div class='ui checked checkbox'>"+
												"<label>Retrun Unit</label><input type='checkbox'>"+
											"</div>"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<div class='field'>"+
								"<div class='ui checked checkbox'>"+
									"<label>Shelfing Rack Type Unit</label><input type='checkbox'>"+
								"</div>"+
							"</div>"+
							"<div class='two fields'>"+
								"<div class='field'>"+
									"<label>Internal Barcode</label>"+
									"<input type='number' name='internal_barcode' placeholder='Inputkan'>"+
								"</div>"+
								"<div class='field'>"+
									"<label>Conversion</label>"+
									"<input type='number' name='conversion' placeholder='Inputkan'>"+
								"</div>"+
							"</div>"+
							"<div class='two fields'>"+
								"<div class='field'>"+
									"<label>Width UOM 4</label>"+
									"<div class='ui right labeled input'>"+
										"<input type='number' name='width' placeholder='Inputkan'>"+
										"<div class='ui basic label'>"+
											"cm"+
										"</div>"+
									"</div>"+
								"</div>"+
								"<div class='field'>"+
									"<label>Length UOM 4</label>"+
									"<div class='ui right labeled input'>"+
										"<input type='number' name='length' placeholder='Inputkan'>"+
										"<div class='ui basic label'>"+
											"cm"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<div class='two fields'>"+
								"<div class='field'>"+
									"<label>Height UOM 4</label>"+
									"<div class='ui right labeled input'>"+
										"<input type='number' name='height' placeholder='Inputkan'>"+
										"<div class='ui basic label'>"+
											"cm"+
										"</div>"+
									"</div>"+
								"</div>"+
								"<div class='field'>"+
									"<label>Weight UOM 4</label>"+
									"<div class='ui right labeled input'>"+
										"<input type='number' name='weight' placeholder='Inputkan'>"+
										"<div class='ui basic label'>"+
											"gr"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<div class='three fields'>"+
								"<div class='field'>"+
									"<label>Harga</label>"+
									"<input type='number' name='height' placeholder='Inputkan'>"+
								"</div>"+
								"<div class='field'>"+
									"<label>Box Type</label>"+
									"<div class='inline fields'>"+
										"<div class='field'>"+
											"<div class='ui radio checkbox'>"+
												"<input type='radio' name='box_type' tabindex="0" class='hidden' value="1" checked>"+
												"<label>Outerbox</label>"+
											"</div>"+
										"</div>"+
										"<div class='field'>"+
											"<div class='ui radio checkbox'>"+
												"<input type='radio' name='box_type' tabindex="0" class='hidden' value="0">"+
												"<label>Inerbox</label>"+
											"</div>"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<div class='field'>"+
								"<label>File Gambar</label>"+
								"<input type='file' name='akun_diskon'>"+
							"</div>"+
						"</div>"+
					"</div>"+
				"</div>"+
				"</div>"
				);