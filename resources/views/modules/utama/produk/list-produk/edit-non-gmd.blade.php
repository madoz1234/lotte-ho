<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Produk Non GMD</div>
{{-- <div class="ui pointing secondary demo menu">
	<a class="active blue item" data-tab="general">General Settings</a>
</div> --}}
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ URL::to('utama/produk/upload-non-produk/'.$record->id) }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">

			{{-- <div class="ui active tab" data-tab="general">
				@include('modules.utama.produk.upload-non-produk.tabs.general')
			</div> --}}
			<div class="ui grid">
				<div class="sixteen wide column">
					<!-- Barang -->
					<h4 class="ui blue dividing header">Barang</h4>

					<div class="three fields">
						<div class="four wide field">
							<label>Kode Barang</label>
							<input placeholder="Kode Barang" type="text" readonly="" value="{{ $record->produk->kode or '' }}">
						</div>

						<div class="six wide field">
							<label>Nama Barang</label>
							<input placeholder="Nama Barang" name="uom1_produk_description" type="text" value="{{ $record->uom1_produk_description or $record->produk->nama }}">
						</div>

						<div class="four wide field">
							<label>Barcode</label>
							<input placeholder="Barcode" name="uom1_barcode" type="text" value="{{ $record->uom1_barcode or '-' }}">
						</div>

						<div class="four wide field">
							<label>Margin</label>
								<input placeholder="Margin" name="margin" type="text" value="{{ $record->margin ? $record->margin : '1' }}">
						</div>
					</div>

					{{-- <div class="two fields">
						<div class="field">
							<label>Tipe Barang</label>
							<select name="tipe_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\TipeBarang::options('nama', 'kode',['selected' => isset($record->tipe_barang_kode) ? $record->tipe_barang_kode : '0',], '-- Pilih --') !!}
							</select>
						</div>

						<div class="field">
							<label>Jenis Barang</label>
							<select name="jenis_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\JenisBarang::options('jenis', 'kode',['selected' => isset($record->jenis_barang_kode) ? $record->jenis_barang_kode : '0',], '-- Pilih --') !!}
							</select>
						</div>

						<div class="field">
							<label>Tipe Aset</label>
							@if ($record->jenis_barang_kode == 004)
								<select name="tipe_asset_id" class="ui fluid search selection dropdown" style="width: 100%;">
									{!! \Lotte\Models\Master\TipeAset::options('tipe', 'id',['selected' => isset($record->tipe_asset_id) ? $record->tipe_asset_id : '',], '-- Pilih --') !!}
								</select>
							@else
								<input type="text" style="background-color: #edf1f6;" disabled="" placeholder="Bukan Tipe Aset">
							@endif
						</div>
					</div> --}}
					{{-- test --}}
					@if ($record->jenis_barang_kode == 004)
						<div class="two fields">
							<div class="field">
								<label>Tipe Barang</label>
								<select name="tipe_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
									{!! \Lotte\Models\Master\TipeBarang::options('nama', 'kode',['selected' => isset($record->tipe_barang_kode) ? $record->tipe_barang_kode : '0',], '-- Pilih --') !!}
								</select>
							</div>

							<div class="field">
								<label>Jenis Barang</label>
								<select name="jenis_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
									{!! \Lotte\Models\Master\JenisBarang::options('jenis', 'kode',['selected' => isset($record->jenis_barang_kode) ? $record->jenis_barang_kode : '0',], '-- Pilih --') !!}
								</select>
							</div>

							<div class="field">
								<label>Tipe Aset</label>
									<select name="tipe_asset_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\TipeAset::options('tipe', 'id',['selected' => isset($record->tipe_asset_id) ? $record->tipe_asset_id : '2',], '-- Pilih --') !!}
									</select>
							</div>
						</div>
					@else
						<div class="two fields">
						<div class="field">
							<label>Tipe Barang</label>
							<select name="tipe_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\TipeBarang::options('nama', 'kode',['selected' => isset($record->tipe_barang_kode) ? $record->tipe_barang_kode : '0',], '-- Pilih --') !!}
							</select>
						</div>

						<div class="field">
							<label>Jenis Barang</label>
							<select name="jenis_barang_kode" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\JenisBarang::options('jenis', 'kode',['selected' => isset($record->jenis_barang_kode) ? $record->jenis_barang_kode : '0',], '-- Pilih --') !!}
							</select>
						</div>
					</div>
					@endif
					{{-- test --}}
				</div>

				<div class="ui error message"></div>
			</div>
		</form>
	</div>

	<div class="actions">
		<div class="ui negative button" style="background: grey;">
			Batal
		</div>
		<div class="ui positive right labeled icon save button">
			Simpan
			<i class="checkmark icon"></i>
		</div>
	</div>

@include('layouts.scripts.inputmask')

