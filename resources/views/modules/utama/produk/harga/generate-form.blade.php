<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">
	Generate Data Harga
</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.'generate-form') }}" method="post">
		{!! csrf_field() !!}
		<!-- pertama -->
		<div class="field required">		
			<label>TMUK</label>
			<select name="tmuk_kode" class="ui full-width search selection dropdown">
				{!! \Lotte\Models\Master\Tmuk::options(function($q){ return $q->kode . ' - ' . $q->nama; }, 'kode') !!}
			</select>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Proses
		<i class="checkmark icon"></i>
	</div>
</div>