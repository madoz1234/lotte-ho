<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">
	Tambah Data Harga
	<div style="float: right;">
		<button type="button" class="ui blue add button uom">
			<i class="plus icon"></i>
			UOM
		</button>
		<button type="button" class="ui red remove button uom hidden">
			<i class="minus icon"></i>
			UOM
		</button>
	</div>
</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<!-- pertama -->
		<div class="field">		
			<div class="required fields">
				<div class="seven wide field">
					<label>Kode Produk</label>
					<input type="number" name="kode" placeholder="Inputkan Kode Produk">
				</div>
				<div class="seven wide field">
					<label>Nama Produk</label>
					<input type="text" name="nama" placeholder="Inputkan Nama Produk">
				</div>
				<div class="seven wide field">
					<label>TMUK</label>
					<select class="ui full-width search selection dropdown">
						<option value=""> &nbsp;&nbsp;Pilih TMUK</option>
						<option value="2"> &nbsp;&nbsp;Jernih</option>
						<option value="1"> &nbsp;&nbsp;Pragma Informatika</option>
						<option value="0"> &nbsp;&nbsp;Etokokue</option>
					</select>
				</div>
			</div>
		</div>
		{{-- </div> --}}

		{{-- <div class="ui form">
			<div class="inline fields">
				<label class="two wide field">Kode Produk</label>
				<div class="six wide field">:&nbsp;&nbsp;
					<input placeholder="Inputkan" type="number">
				</div>
			</div>

			<div class="inline fields">
				<label class="two wide field">Nama Produk</label>
				<div class="six wide field">:&nbsp;&nbsp;
					<input placeholder="Inputkan" type="text">
				</div>
			</div>

			<div class="inline fields">
				<label class="two wide field">TMUK</label>
				<div class="six wide field">:&nbsp;&nbsp;
					<select class="ui full-width search selection dropdown">
						<option value=""> &nbsp;&nbsp;Pilih TMUK</option>
						<option value="2"> &nbsp;&nbsp;Jernih</option>
						<option value="1"> &nbsp;&nbsp;Pragma Informatika</option>
						<option value="0"> &nbsp;&nbsp;Etokokue</option>
					</select>
				</div>
			</div>
		</div> --}}

{{-- <select class="ui dropdown">
						  <option value="">Pilih TMUK</option>
						  <option value="2">Jernih</option>
						  <option value="1">Pragma Informatika</option>
						  <option value="0">Etokokue</option>
						</select> --}}
					{{-- <div class=" six wide field ui fluid search selection dropdown">:&nbsp;&nbsp;
						<input type="hidden" name="tes">
						<i class="dropdown icon"></i>
						<div class="default text">Pilih TMUK</div>
						<div class="menu">
							<div class="item" data-value="1">Jernih</div>
							<div class="item" data-value="2">Pragma Informatika</div>
							<div class="item" data-value="3">Etokokue</div>
						</div>
					</div> --}}


					<!-- pertama -->
					<div class="ui two cards">
						<div class="card">
							<div class="content">
								<div class="header" style="text-align: center;">UOM 1</div>
								<br/>
								<div class="ui form">
									<div class="inline fields">
										<label class="six wide field">UOM 1 Description</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Barcode UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Barcode UOM" type="text">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Product Description UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Product Description" type="text">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Handling Type UOM 1</label>
										<div class="eight wide field">
											<div class="ui form">
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox" checked="">
														<label>Selling Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox">
														<label>Order Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox" checked="">
														<label>Receive Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox">
														<label>Return Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox" checked="">
														<label>Inventory Unit</label>
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Cost Price UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Description" type="number">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Suggested Rounding Selling Price</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text">
										</div>
									</div>
									{{-- <div class="inline fields">
										<label class="six wide field">Rounding Selling Price UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text">
										</div>
									</div> --}}
									<div class="inline fields">
										<label class="six wide field">Changed Selling Price UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Margin Amount UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text">
										</div>
									</div>
									<hr>
									<div class="inline fields">
										<label class="six wide field">Locked price</label>
										<div class="ui radio checkbox">
											<input type="radio" name="hanger" tabindex="0" class="hidden" value="0" checked>
											<label>Ya</label>
										</div>&nbsp;&nbsp;&nbsp;&nbsp;
										<div class="ui radio checkbox">
											<input type="radio" name="hanger" tabindex="0" class="hidden" value="0">
											<label>Tidak</label>
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Last Locked Price</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text" value="21/01/18" disabled="">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="content">
								<div class="header" style="text-align: center;">UOM 2</div>
								<br/>
								<div class="ui form">
									<div class="inline fields">
										<label class="six wide field">UOM 2 Description</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Barcode UOM 2</label>
										<div class="eight wide field">
											<input placeholder="Barcode UOM" type="text">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Product Description UOM 2</label>
										<div class="eight wide field">
											<input placeholder="Product Description" type="text">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Handling Type UOM 2</label>
										<div class="eight wide field">
											<div class="ui form">
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox">
														<label>Selling Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox" checked="">
														<label>Order Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox">
														<label>Receive Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox" checked="">
														<label>Return Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input tabindex="0" class="hidden" type="checkbox">
														<label>Inventory Unit</label>
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">GMD Price</label>
										<div class="eight wide field">
											<input placeholder="Description" type="number">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Cost Price UOM 2</label>
										<div class="eight wide field">
											<input placeholder="Description" type="number">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					{{-- </div> --}}
					{{-- </div> --}}
				</form>
			</div>
			<div class="actions">
				<div class="ui negative button" style="background: grey;">
					Batal
				</div>
				<div class="ui positive right labeled icon save button">
					Simpan
					<i class="checkmark icon"></i>
				</div>
			</div>