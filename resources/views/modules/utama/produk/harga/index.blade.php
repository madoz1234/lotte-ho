@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[produk_kode]" placeholder="Kode Produk" type="text">
	</div>

	<div class="field">
		<input name="filter[nama]" placeholder="Nama Produk" type="text">
	</div>
	{{-- <div class="field">
		<select name="filter[produk_kode]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\Produk::options('nama', 'kode',[], '-- Pilih Produk --') !!}
    	</select>
	</div> --}}
	<div class="field">
		<select name="filter[tmuk_kode]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
    	</select>
	</div>

	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	{{-- <div class="ui blue buttons" style="margin-right: 5px">
		<button type="button" class="ui blue generate-harga button"><i class="plus icon icon"></i>Generate Harga</button>
	</div> --}}
	<a href="{{ asset('template\harga-produk.xls') }}" class="ui grey button" target="_blank"><i class="download icon icon"></i>Template</a>
	<div class="ui blue buttons" style="margin-right: 5px">
		<button type="button" class="ui blue uploadharga button"><i class="upload icon icon"></i>Upload</button>
	</div>
	<button type="button" class="ui green button" onclick="javascript:export_harga();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.produk_kode = $("input[name='filter[produk_kode]']").val();
	d.nama = $("input[name='filter[nama]']").val();
    {{-- d.produk_kode = $("select[name='filter[produk_kode]']").val(); --}}
    d.tmuk_kode = $("select[name='filter[tmuk_kode]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		produk_kode: {
			identifier: 'produk_kode',
			rules: [{
				type   : 'empty',
				prompt : 'Isian produk_kode tidak boleh kosong'
			}]
		},

		tmuk_kode: {
			identifier: 'tmuk_kode',
			rules: [{
				type   : 'empty',
				prompt : 'Isian tmuk_kode tidak boleh kosong'
			}]
		}
	};

	$(document).ready(function() {
		
		$('.generate-harga').on('click', function(){
			loadModal('{{ url($pageUrl.'generate-form') }}')
		})

	});

</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.checkbox').checkbox();
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
	};
</script>
@endsection

@section('subcontent')
<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
	<thead>
		<tr align="center">
			<th width="20px" rowspan="2">No</th>
			<th width="80px" rowspan="2">Kode Produk</th>
			<th width="80px" rowspan="2">Nama Produk</th>
			<th width="80px" rowspan="2">TMUK</th>
			<th width="80px" colspan="3">UOM 1</th>
			<th width="80px" colspan="1">UOM 2</th>
			<th width="50px" rowspan="2">Dibuat Pada</th>
			<th width="50px" rowspan="2">Aksi</th>
		</tr>
		<tr align="center">
			<th width="80px">Cost Price (Rp)</th>
			<th width="80px">Suggested Rounded <br> Selling Price (Rp)</th>
			<th width="80px">Changed Selling Price (Rp)</th>
			<th width="80px">Margin Value (Rp)</th>
		</tr>	
	</thead>
{{-- 	<tbody>
		@foreach ($record as $data)
				<tr>
                	<td>0</td>
                	<td>{{ $data['kode'] }}</td>
                	<td>{{ $data['nama'] }}</td>
                	<td class="ui center aligned"></td>
                </tr>
            @endforeach
	</tbody> --}}
</table>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {

			export_harga = function(){
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-harga') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var produk_kode = document.createElement("input");
		            produk_kode.setAttribute("type", "hidden");
		            produk_kode.setAttribute("name", 'produk_kode');
		            produk_kode.setAttribute("value", $('[name="filter[produk_kode]"]').val());
		        form.appendChild(produk_kode);
		        
		        var tmuk_kode = document.createElement("input");
		            tmuk_kode.setAttribute("type", "hidden");
		            tmuk_kode.setAttribute("name", 'tmuk_kode');
		            tmuk_kode.setAttribute("value", $('[name="filter[tmuk_kode]"]').val());
		        form.appendChild(tmuk_kode);
		        
		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append