<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">
	Edit Data Harga
	<div style="float: right;">
		<button type="button" class="ui blue add button uom">
			<i class="plus icon"></i>
			UOM
		</button>
		<button type="button" class="ui red remove button uom hidden">
			<i class="minus icon"></i>
			UOM
		</button>
	</div>
</div>
<div class="content">
	{{-- <form class="ui data form" id="dataForm"> --}}
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<!-- pertama -->
		<div class="field">		
			<div class="required fields">
				<div class="seven wide field">
					<label>Kode Produk</label>
					<input style="background-color: #edf1f6;" type="text" placeholder="Inputkan Kode Produk" value="{{ $record->produk_kode or "" }}" readonly="">
				</div>
				<div class="seven wide field">
					<label>Nama Produk</label>
					<input style="background-color: #edf1f6;" type="text" placeholder="Inputkan Kode Produk" value="{{ $record->produksetting->produk->nama or "" }}" readonly="">
				</div>
				<div class="seven wide field">
					<label>TMUK</label>
					<input style="background-color: #edf1f6;" type="text" placeholder="Inputkan Kode Produk" value="{{ $record->tmuk->nama or "" }}" readonly="">
				</div>
			</div>
		</div>
					<!-- pertama -->
					<div class="ui two cards">
						<div class="card">
							<div class="content">
								<div class="header" style="text-align: center;">UOM 1</div>
								<br/>
								<div class="ui form">
									<div class="inline fields">
										<label class="six wide field">UOM 1 Description</label>
										<div class="eight wide field">
											<input placeholder="Description" readonly="" name="uom1_satuan" type="text" value="{{ $record->produksetting->uom1_satuan or "" }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Barcode UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Barcode UOM" readonly="" name="uom1_barcode" type="text" value="{{ $record->produksetting->uom1_barcode or "" }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Product Description UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Product Description" readonly="" name="uom1_produk_description" type="text" value="{{ $record->produksetting->uom1_produk_description or "" }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Handling Type UOM 1</label>
										<div class="eight wide field">
											<div class="ui form">
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input type="checkbox" value="1" {{ $record->produksetting->uom1_selling_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Selling Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input class="hidden" type="checkbox" value="1" {{ $record->produksetting->uom1_order_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Order Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input class="hidden" type="checkbox" value="1" {{ $record->produksetting->uom1_receiving_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Receive Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input class="hidden" type="checkbox" value="1" {{ $record->produksetting->uom1_return_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Return Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input type="checkbox" value="1" {{ $record->produksetting->uom1_inventory_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Inventory Unit</label>
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">HPP (MAP)</label>
										<div class="eight wide field">
											<input placeholder="Description" readonly="" type="text" value="{{ FormatNumber($record->map) }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Suggested Rounded Selling Price</label>
										<div class="eight wide field">
												<input placeholder="Description" readonly="" type="text" value="{{ FormatNumber($record->suggest_price) }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Changed Selling Price UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Description" readonly="" type="text" value="{{ FormatNumber($record->change_price) }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Margin Amount UOM 1</label>
										<div class="eight wide field">
											<input placeholder="Description" readonly="" type="text" value="{{ FormatNumber($record->margin_amount) }}">
										</div>
									</div>
									<hr>
									<div class="inline field">
										<label class="six wide field">Locked Price</label>
										<div class="ui radio checkbox">
											<input type="radio" tabindex="0" readonly="" class="hidden" value="1" {{ ($record->locked_price == 1 ) ? 'checked' : '' }}>
											<label>Ya</label>
										</div>&nbsp;&nbsp;&nbsp;&nbsp;
										<div class="ui radio checkbox">
											<input type="radio" tabindex="0" readonly="" class="hidden" value="0" {{ ($record->locked_price == 0 ) ? 'checked' : '' }}>
											<label>Tidak</label>
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Last Locked Price</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text" readonly="" value="{{ $record->updated_at or $record->created_at }}">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="content">
								<div class="header" style="text-align: center;">UOM 2</div>
								<br/>
								<div class="ui form">
									<div class="inline fields">
										<label class="six wide field">UOM 2 Description</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text" readonly="" value="{{ $record->produksetting->uom2_satuan or "" }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Barcode UOM 2</label>
										<div class="eight wide field">
											<input placeholder="Barcode UOM" type="text" readonly="" value="{{ $record->produksetting->uom2_barcode or "" }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Product Description UOM 2</label>
										<div class="eight wide field">
											<input placeholder="Product Description" type="text" readonly="" value="{{ $record->produksetting->uom1_produk_description or $record->produk->nama }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Handling Type UOM 2</label>
										<div class="eight wide field">
											<div class="ui form">
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input type="checkbox" value="1" {{ $record->produksetting->uom2_selling_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Selling Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input class="hidden" type="checkbox" value="1" {{ $record->produksetting->uom2_order_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Order Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input class="hidden" type="checkbox" value="1" {{ $record->produksetting->uom2_receiving_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Receive Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input class="hidden" type="checkbox" value="1" {{ $record->produksetting->uom2_return_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Return Unit</label>
													</div>
												</div>
												<br/>
												<div class="inline field">
													<div class="ui toggle checkbox">
														<input type="checkbox" value="1" {{ $record->produksetting->uom2_inventory_cek == '1' ? 'checked="1"' : '' }} disabled="">
														<label>Inventory Unit</label>
													</div>
												</div>	
											</div>
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">GMD Price Per UOM 2</label>
										<div class="eight wide field">
											{{-- <input placeholder="Description" type="text" readonly="" value="{{ FormatNumber($record->produksetting->produk->produklsi->curr_sale_prc) }}"> --}}
											<input placeholder="Description" type="text" readonly="" value="{{ FormatNumber($record->produksetting->produk->produklsi ? $record->produksetting->produk->produklsi->curr_sale_prc : '0') }}">
										</div>
									</div>
									<div class="inline fields">
										<label class="six wide field">Cost Price UOM 2</label>
										<div class="eight wide field">
											<input placeholder="Description" type="text" readonly="" value="{{ FormatNumber($record->produksetting->produk->produklsi ? $record->produksetting->produk->produklsi->curr_sale_prc / $record->produksetting->uom2_conversion : '0') }}">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	{{-- <div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div> --}}
</div>