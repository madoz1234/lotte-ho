<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Jenis Barang</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
			<input style="background-color: #efefef;" name="kode" placeholder="Kode Jenis Barang" type="hidden" value="{{ $code }}" readonly>
		<div class="field">
			<label>Jenis Barang</label>
			<input name="jenis" placeholder="Jenis Barang" type="text" value="{{ $jenis or "" }}">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>