@extends('layouts.grid')

@section('filters')

	<div class="field">
		<input name="filter[produk_kode]" placeholder="Kode" type="number">
	</div>
	<div class="field">
		<select name="filter[produk_nama]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\Produk::options('nama', 'kode',[], '-- Pilih Nama Produk --') !!}
    	</select>
	</div>
	<div class="field">
		<select name="filter[tipe_barang_kode]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\TipeBarang::options('nama', 'kode',[], '-- Pilih Tipe Barang --') !!}
    	</select>
	</div>
	<div class="field">
		<select name="filter[jenis_barang_kode]" class="ui fluid search selection dropdown">
	    	{!! \Lotte\Models\Master\JenisBarang::options('jenis', 'kode',[], '-- Pilih Jenis Barang --') !!}
    	</select>
	</div>

	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('js-filters')
    d.produk_kode = $("input[name='filter[produk_kode]']").val();
    d.produk_nama = $("select[name='filter[produk_nama]']").val();
    d.tipe_barang_kode = $("select[name='filter[tipe_barang_kode]']").val();
    d.jenis_barang_kode = $("select[name='filter[jenis_barang_kode]']").val();
@endsection

@section('toolbars')
    {{-- <button type="button" class="ui grey button" onclick="javascript:tamplate_gmd();"><i class="download icon icon"></i>Template</button> --}}
    {{-- <button type="button" class="ui blue add button">
    	<i class="plus icon"></i>
    	Tambah Produk Non GMD
    </button> --}}
	<a href="{{ asset('template\produk-non-gmd.xls') }}" class="ui grey button" target="_blank"><i class="download icon icon"></i>Template</a>
	<button type="button" class="ui blue importexcel button" style="background-color: #3537ad;"><i class="upload icon icon"></i>Upload</button>
	<button type="button" class="ui green button" onclick="javascript:export_upload_non_produk();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	jenis_assortment: {
		identifier: 'jenis_assortment',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Jenis Assortment tidak boleh kosong'
		}]
	},

	jumlah_rak_lorong: {
		identifier: 'jumlah_rak_lorong',
		rules: [{
			type   : 'empty',
			prompt : 'Pilihan Jumlah Rak Diding tidak boleh kosong'
		}]
	},

	jumlah_rak_dinding: {
		identifier: 'jumlah_rak_dinding',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Jumlah Rak Dinding tidak boleh kosong'
		}]
	},

	jumlah_rak_kasir: {
		identifier: 'jumlah_rak_kasir',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Jumlah Rak Kasir tidak boleh kosong'
		}]
	}
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	$(document).on('click', '.cekprice.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/cekprice/"+id;

		loadModal(url);
	});

	$(document).on('click', '.cekstatus.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/cekstatus/"+id;

		loadModal(url);
	});

	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
		$('.demo.menu .item').tab({
			history:false,
		});
	};
</script>
@endsection
@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_upload_non_produk = function(){
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-upload-non-produk') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var produk_kode = document.createElement("input");
		            produk_kode.setAttribute("type", "hidden");
		            produk_kode.setAttribute("name", 'produk_kode');
		            produk_kode.setAttribute("value", $('[name="filter[produk_kode]"]').val());
		        form.appendChild(produk_kode);

		        var tipe_barang_kode = document.createElement("input");
		            tipe_barang_kode.setAttribute("type", "hidden");
		            tipe_barang_kode.setAttribute("name", 'tipe_barang_kode');
		            tipe_barang_kode.setAttribute("value", $('[name="filter[tipe_barang_kode]"]').val());
		        form.appendChild(tipe_barang_kode);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}

		});

	</script>
@append