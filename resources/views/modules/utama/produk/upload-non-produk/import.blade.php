{{-- <form action="{{ url('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input type="file" name="upload_data_produk">
	<button type="submit" name="submit" class="ui yellow button">
	Upload
	</button>
</form> --}}

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Import Data Produk Non GMD Excel</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ URL::to('utama/produk/upload-non-produk/postimportexcel') }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="field">
		    <div class="ui left action input">
				<label for="file" class="ui icon labeled button">
				  	<i class="file icon"></i>
				    Pilih File
				</label>
				<input type="file" name="upload_produk_non_gmd" id="file" style="display: none" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
				<input type="text" id="filename" value="" readonly="">
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon button">
		Upload
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$('input[type=file]').change(function () {
		$('#filename').val(this.files[0]['name']);
	})

	$('#filename').click(function () {
		$('#file').click();
	})
</script>