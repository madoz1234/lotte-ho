<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Produk Non GMD</div>
{{-- <div class="ui pointing secondary demo menu">
	<a class="active blue item" data-tab="general">General Settings</a>
</div> --}}
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">

			{{-- <div class="ui active tab" data-tab="general">
				@include('modules.utama.produk.upload-non-produk.tabs.general')
			</div> --}}
			<div class="ui grid">
				<div class="sixteen wide column">
					<!-- Barang -->
					<h4 class="ui blue dividing header">Barang</h4>

					<div class="two fields">
						<div class="four wide field">
							<label>Kode Barang</label>
							<input placeholder="Kode Barang" type="text" disabled="" style="background-color: #edf1f6;" value="{{ $record->produk->kode or '' }}">
						</div>

						<div class="twelve wide field">
							<label>Nama Barang</label>
							<input placeholder="Nama Barang" type="text" disabled="" style="background-color: #edf1f6;" value="{{ $record->produk->nama or '' }}">
						</div>
					</div>

					<div class="two fields">
						<div class="field">
							<label>Tipe Barang</label>
							<select name="tipe_barang_kode" readonly="" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\TipeBarang::options('nama', 'kode',['selected' => isset($record->tipe_barang_kode) ? $record->tipe_barang_kode : '0',], '-- Pilih --') !!}
							</select>
						</div>

						<div class="field">
							<label>Jenis Barang</label>
							<select name="jenis_barang_kode" readonly="" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\JenisBarang::options('jenis', 'kode',['selected' => isset($record->jenis_barang_kode) ? $record->jenis_barang_kode : '0',], '-- Pilih --') !!}
							</select>
						</div>
					</div>
				</div>

				<div class="ui error message"></div>
			</div>
		</form>
	</div>

	<div class="actions">
		<div class="ui negative button" style="background: grey;">
			Batal
		</div>
		{{-- <div class="ui positive right labeled icon save button">
			Simpan
			<i class="checkmark icon"></i>
		</div> --}}
	</div>

@include('layouts.scripts.inputmask')
