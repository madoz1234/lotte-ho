<a class="ui fluid teal button" href="#" style="margin-bottom: 10px" onclick="showTable('divisi')">
	<i class="list icon"></i> 
	SEMUA DIVISI
</a>
<div class="ui treeview accordion" style="margin-bottom: 50px">
	@foreach($data_div as $divisi)
		<div class="title" onclick="showTable('kat1')">
			<i class="dropdown icon"></i>
			{{ ($divisi->nama)}} <a class="ui red circular tiny label">{{$divisi->kategori1->count()}}</a>
		</div>
		<div class="content">
			@foreach($divisi->kategori1 as $kat1)
				<div class="ui accordion">
					<div class="title" onclick="showTable('kat2')">
						<i class="dropdown icon"></i>
						{{ ($kat1->nama) }}<a class="ui red circular tiny label">{{$kat1->kategori2->count()}}</a>
					</div>
					<div class="content">
						@foreach($kat1->kategori2 as $kat2)
							<div class="ui accordion">
								<div class="title" onclick="showTable('kat3')">
									<i class="dropdown icon"></i>
									{{ ($kat2->nama) }}<a class="ui red circular tiny label">{{$kat2->kategori3->count()}}</a>
								</div>
								<div class="content">
									@foreach($kat2->kategori3 as $kat3)
										<div class="ui accordion">
											<div class="title" onclick="showTable('kat4')">
												<i class="dropdown icon"></i>
												{{ ($kat3->nama) }}<a class="ui red circular tiny label">{{$kat3->kategori4->count()}}</a>
											</div>
											<div class="content">
												@foreach($kat3->kategori4 as $kat4)
													<div class="ui vertical text menu">
														<a class="item">
															<i class="right angle icon"></i>{{ ($kat4->nama) }}
														</a>
													</div>
												@endforeach
											</div>
										</div>
									@endforeach
								</div>
							</div>
						@endforeach
					</div>
				</div>
			@endforeach
		</div>
	@endforeach
</div>
<div class="ui mini blue message" style="position: absolute; bottom: 15px;">
	Klik icon <i class="caret right icon"></i> untuk membuka folder, klik <strong>judul</strong> untuk melihat list.
</div>