<div class="ui form" style="float:right">
    <div class="fields">
        {{-- <div class="inline field">
            <div class="ui icon input">
                <input placeholder="Cari Kategori..." type="text" id="searchbox">
                <i class="search icon"></i>
            </div>
        </div> --}}
        {{-- <button type="button" class="ui blue add button"><i class="plus icon"></i>Tambah Bulk Data</button>	 --}}
        {{-- <div class="ui blue buttons" style="margin-right: 5px">
            <button type="button" class="ui blue button"><i class="download icon icon"></i>Download</button>
        </div> --}}
        {{-- <div class="ui green buttons" style="margin-right: 5px">
            <button type="button" class="ui green button"><i class="upload icon icon"></i>Upload</button>
        </div> --}}
        <button type="button" class="ui green button" onclick="javascript:kat2();">
            <i class="file excel outline icon"></i>
            Export Excel
        </button>
    </div>
</div>
<h3 class="ui header" style="margin: 7px 0">
    Kategori 2
</h3>

<table id="kategori2Table" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            @foreach ($tableStruct as $struct)
                <th>{{ $struct['label'] or $struct['name'] }}</th>
            @endforeach
        </tr>
    </thead>
</table>