<div class="ui form" style="float:right">
    <div class="fields">
        <div class="inline field">
            <div class="ui icon input">
                <input placeholder="Cari Kategori..." type="text" id="searchbox">
                <i class="search icon"></i>
            </div>
        </div>
    </div>
</div>
<h3 class="ui header" style="margin: 7px 0">
    Divisi Produk
</h3>

<table id="divisiTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="center aligned" width="40px">#</th>
            <th>Kode</th>
            <th>Nama Divisi</th>
            <th>Dibuat pada</th>
        </tr>
    </thead>
    <tbody>
        {{-- <tr>
            @foreach ($tableStruct as $struct)
                <th>{{ $struct['label'] or $struct['name'] }}</th>
            @endforeach
        </tr> --}}
        @foreach ($data as $v)
            <th>{{ $v->kode }}</th>
            <th>{{ $v->nama }}</th>
            <th>{{ $v->created_at }}</th>
        @endforeach
    </tbody>
</table>