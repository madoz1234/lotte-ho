@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		$('.treeview.accordion')
		.accordion({
			selector: {
				trigger: '.title .icon'
			}
		})
		;

		kat1 = function(){
                // alert('tara');
                // create form
                var form = document.createElement("form");
                    form.setAttribute("method", 'POST');
                    form.setAttribute("action", "{{ url('export/kat1') }}");
                    form.setAttribute("target", "_blank");

                var csrf = document.createElement("input");
                    csrf.setAttribute("type", "hidden");
                    csrf.setAttribute("name", '_token');
                    csrf.setAttribute("value", '{{ csrf_token() }}');
                form.appendChild(csrf);

                document.body.appendChild(form);
                form.submit();

                document.body.removeChild(form);
            }

        kat2 = function(){
                // alert('tara');
                // create form
                var form = document.createElement("form");
                    form.setAttribute("method", 'POST');
                    form.setAttribute("action", "{{ url('export/kat2') }}");
                    form.setAttribute("target", "_blank");

                var csrf = document.createElement("input");
                    csrf.setAttribute("type", "hidden");
                    csrf.setAttribute("name", '_token');
                    csrf.setAttribute("value", '{{ csrf_token() }}');
                form.appendChild(csrf);

                document.body.appendChild(form);
                form.submit();

                document.body.removeChild(form);
            }

        kat3 = function(){
                // alert('tara');
                // create form
                var form = document.createElement("form");
                    form.setAttribute("method", 'POST');
                    form.setAttribute("action", "{{ url('export/kat3') }}");
                    form.setAttribute("target", "_blank");

                var csrf = document.createElement("input");
                    csrf.setAttribute("type", "hidden");
                    csrf.setAttribute("name", '_token');
                    csrf.setAttribute("value", '{{ csrf_token() }}');
                form.appendChild(csrf);

                document.body.appendChild(form);
                form.submit();

                document.body.removeChild(form);
            }

        kat4 = function(){
                // alert('tara');
                // create form
                var form = document.createElement("form");
                    form.setAttribute("method", 'POST');
                    form.setAttribute("action", "{{ url('export/kat4') }}");
                    form.setAttribute("target", "_blank");

                var csrf = document.createElement("input");
                    csrf.setAttribute("type", "hidden");
                    csrf.setAttribute("name", '_token');
                    csrf.setAttribute("value", '{{ csrf_token() }}');
                form.appendChild(csrf);

                document.body.appendChild(form);
                form.submit();

                document.body.removeChild(form);
            }

        divisi = function(){
                // alert('tara');
                // create form
                var form = document.createElement("form");
                    form.setAttribute("method", 'POST');
                    form.setAttribute("action", "{{ url('export/divisi') }}");
                    form.setAttribute("target", "_blank");

                var csrf = document.createElement("input");
                    csrf.setAttribute("type", "hidden");
                    csrf.setAttribute("name", '_token');
                    csrf.setAttribute("value", '{{ csrf_token() }}');
                form.appendChild(csrf);

                document.body.appendChild(form);
                form.submit();

                document.body.removeChild(form);
            }

        // var activeTable = $('#reportTable').DataTable({
        //     "processing": true,
        //     "serverSide": true,
        //     "paging":   true,
        //     "ordering": true,
        //     "info":     false,
        //     "searching": false,
        //     "lengthChange": false,
        //     "ajax":{
        //         "url":"{{ url('utama/produk/kategori/gridkat1') }}",
        //         "type": "POST",
        //         data: function (d) {
        //         	d._token = "{{ csrf_token() }}";
        //             d.jurusan = $('input[name=jurusan]').val();
        //         },
        //         "fnCreatedRow": function (row, data, index) {
        //             $('td', row).eq(0).html(index + 1);
        //         }
        //     },
        //     "columns": [
        //     { "data": "num", 'width': '32px', 'className': "center aligned", "bSortable": false },
        //     { "data": "divisi_kode", 'className': "left aligned", "bSortable": false },
        //     { "data": "kode", 'className': "left aligned", "bSortable": false },
        //     { "data": "nama", 'className': "left aligned", "bSortable": false },
        //     { "data": "created_at", 'width': '96px', 'className': "center aligned", "bSortable": false }
        //     ],
        //     });

		$("#searchbox").keyup(function() {
			dt.search(this.value).draw();
		});   

		$('.ui.calendar').calendar({
			type: 'date'
		});
	});

	showTable = function(v){
		$.ajax({
			url: '{{  url($pageUrl) }}/grid',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				type: v
			},
		})

		// untuk mengambil kategori 1
		.done(function(response) {
			var v_1 = v.split('_')
			if(v_1[0] == 'kat1'){
				$('.twelve.wide.column').html(response);

		        var kategori1Table = $('#kategori1Table').DataTable({
		            "processing": true,
		            "serverSide": true,
		            "paging":   true,
		            "ordering": true,
		            "info":     false,
		            "searching": false,
		            "lengthChange": false,
		            "ajax":{
				                "url":"{{ url('utama/produk/kategori/gridkat1') }}",
				                "type": "POST",
				                data: function (d) {
				                	d._token = "{{ csrf_token() }}";
				                },
				                "fnCreatedRow": function (row, data, index) {
				                    $('td', row).eq(0).html(index + 0);
				                }
				            },
		            "columns": [
					            { "data": "num", 'width': '32px', 'className': "center aligned", "bSortable": false },
					            { "data": "divisi_kode", 'className': "left aligned", "bSortable": false },
					            { "data": "kode", 'className': "left aligned", "bSortable": false },
					            { "data": "nama", 'className': "left aligned", "bSortable": false },
					            { "data": "created_at", 'className': "center aligned", "bSortable": false }
					           ],
					drawCallback: function() {
						var api = this.api();

						api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
							cell.innerHTML = parseInt(cell.innerHTML)+i+1;
						} );

						$('[data-content]').popup({
							hoverable: true,
							position : 'top center',
							delay: {
								show: 300,
								hide: 800
							}
						});
					}
		            });

				$("#searchbox").keyup(function() {
					dx.search(this.value).draw();
				});

		// untuk mengambil kategori 2
			}else if(v_1[0] == 'kat2'){
				$('.twelve.wide.column').html(response);

		        var kategori2Table = $('#kategori2Table').DataTable({
		            "processing": true,
		            "serverSide": true,
		            "paging":   true,
		            "ordering": true,
		            "info":     false,
		            "searching": false,
		            "lengthChange": false,
		            "ajax":{
				                "url":"{{ url('utama/produk/kategori/gridkat2') }}",
				                "type": "POST",
				                data: function (d) {
				                	d._token = "{{ csrf_token() }}";
				                },
				                "fnCreatedRow": function (row, data, index) {
				                    $('td', row).eq(0).html(index + 1);
				                }
				            },
		            "columns": [
					            { "data": "num", 'width': '32px', 'className': "center aligned", "bSortable": false },
					            { "data": "divisi_kode", 'className': "left aligned", "bSortable": false },
					            { "data": "kategori1_kode", 'className': "left aligned", "bSortable": false },
					            { "data": "kode", 'className': "left aligned", "bSortable": false },
					            { "data": "nama", 'className': "left aligned", "bSortable": false },
					            { "data": "created_at", 'className': "center aligned", "bSortable": false }
					           ],
					drawCallback: function() {
						var api = this.api();

						api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
							cell.innerHTML = parseInt(cell.innerHTML)+i+1;
						} );

						$('[data-content]').popup({
							hoverable: true,
							position : 'top center',
							delay: {
								show: 300,
								hide: 800
							}
						});
					}
		            });

				$("#searchbox").keyup(function() {
					dx.search(this.value).draw();
				});
		// untuk mengambil kategori 3	
			}else if(v_1[0] == 'kat3'){
				$('.twelve.wide.column').html(response);

		        var kategori3Table = $('#kategori3Table').DataTable({
		            "processing": true,
		            "serverSide": true,
		            "paging":   true,
		            "ordering": true,
		            "info":     false,
		            "searching": false,
		            "lengthChange": false,
		            "ajax":{
				                "url":"{{ url('utama/produk/kategori/gridkat3') }}",
				                "type": "POST",
				                data: function (d) {
				                	d._token = "{{ csrf_token() }}";
				                },
				                "fnCreatedRow": function (row, data, index) {
				                    $('td', row).eq(0).html(index + 1);
				                }
				            },
		            "columns": [
					            { "data": "num", 'width': '32px', 'className': "center aligned", "bSortable": false },
					            { "data": "divisi_kode", 'className': "left aligned", "bSortable": false },
					            { "data": "kategori2_kode", 'className': "left aligned", "bSortable": false },
					            { "data": "kode", 'className': "left aligned", "bSortable": false },
					            { "data": "nama", 'className': "left aligned", "bSortable": false },
					            { "data": "created_at", 'className': "center aligned", "bSortable": false }
					           ],
					drawCallback: function() {
						var api = this.api();

						api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
							cell.innerHTML = parseInt(cell.innerHTML)+i+1;
						} );

						$('[data-content]').popup({
							hoverable: true,
							position : 'top center',
							delay: {
								show: 300,
								hide: 800
							}
						});
					}
		            });

				$("#searchbox").keyup(function() {
					dx.search(this.value).draw();
				});
		// untuk mengambil kategori 4	
			}else if(v_1[0] == 'kat4'){
				$('.twelve.wide.column').html(response);

		        var kategori4Table = $('#kategori4Table').DataTable({
		            "processing": true,
		            "serverSide": true,
		            "paging":   true,
		            "ordering": true,
		            "info":     false,
		            "searching": false,
		            "lengthChange": false,
		            "ajax":{
				                "url":"{{ url('utama/produk/kategori/gridkat4') }}",
				                "type": "POST",
				                data: function (d) {
				                	d._token = "{{ csrf_token() }}";
				                },
				                "fnCreatedRow": function (row, data, index) {
				                    $('td', row).eq(0).html(index + 1);
				                }
				            },
		            "columns": [
					            { "data": "num", 'width': '32px', 'className': "center aligned", "bSortable": false },
					            { "data": "divisi_kode", 'className': "left aligned", "bSortable": false },
					            { "data": "kategori3_kode", 'className': "left aligned", "bSortable": false },
					            { "data": "kode", 'className': "left aligned", "bSortable": false },
					            { "data": "nama", 'className': "left aligned", "bSortable": false },
					            { "data": "created_at", 'className': "center aligned", "bSortable": false }
					           ],
					drawCallback: function() {
						var api = this.api();

						api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
							cell.innerHTML = parseInt(cell.innerHTML)+i+1;
						} );

						$('[data-content]').popup({
							hoverable: true,
							position : 'top center',
							delay: {
								show: 300,
								hide: 800
							}
						});
					}
		            });

				$("#searchbox").keyup(function() {
					dx.search(this.value).draw();
				});
		// untuk mengambil divisi	
			}else if(v_1[0] == 'divisi'){
				$('.twelve.wide.column').html(response);

		        var divisiTable = $('#divisiTable').DataTable({
		            "processing": true,
		            "serverSide": true,
		            "paging":   true,
		            "ordering": true,
		            "info":     false,
		            "searching": false,
		            "lengthChange": false,
		            "ajax":{
				                "url":"{{ url('utama/produk/kategori/griddivisi') }}",
				                "type": "POST",
				                data: function (d) {
				                	d._token = "{{ csrf_token() }}";
				                },
				                "fnCreatedRow": function (row, data, index) {
				                    $('td', row).eq(0).html(index + 1);
				                }
				            },
		            "columns": [
					            { "data": "num", 'width': '32px', 'className': "center aligned", "bSortable": false },
					            { "data": "kode", 'className': "left aligned", "bSortable": false },
					            { "data": "nama", 'className': "left aligned", "bSortable": false },
					            { "data": "created_at", 'className': "center aligned", "bSortable": false }
					           ],
					drawCallback: function() {
						var api = this.api();

						api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
							cell.innerHTML = parseInt(cell.innerHTML)+i+1;
						} );

						$('[data-content]').popup({
							hoverable: true,
							position : 'top center',
							delay: {
								show: 300,
								hide: 800
							}
						});
					}
		            });

				$("#searchbox").keyup(function() {
					dx.search(this.value).draw();
				});
				
			}
		})
		.fail(function(response) {
			console.log("error");
		});
		
	}
</script>
@append

@section('filters')
	{{-- <div class="field">
		<div class="ui labeled input">
			<input placeholder="View" type="text" id="searchbox">
		</div>
	</div> --}}
	{{-- <div class="field">
		<div class="ui labeled input">
			<select name="filter[lsi]" class="ui compact search selection dropdown">
				<option value="">Assortment Type</option>
				<option value="06001">500</option>
				<option value="06002">1000</option>
				<option value="06003">1500</option>
				<option value="06004">2500</option>
			</select>
		</div>
	</div>
	<div class="field">
		<input name="filter[nomor_pyr]" placeholder="TMUK" type="text">
	</div> --}}
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
	@endsection

	@section('toolbars')
	{{-- <div class="ui default right labeled icon save button">
		Print
		<i class="print icon"></i>
	</div>
	<div class="ui green export buttons" style="margin-right: 5px">
		<div class="ui button"><i class="file excel outline icon"></i>Export</div>
		<div class="ui floating dropdown icon button">
			<i class="dropdown icon"></i>
			<div class="menu">
				<div class="item"><i class="file excel outline icon"></i> Excel</div>
				<div class="item"><i class="file pdf outline icon"></i> PDF</div>
			</div>
		</div>
	</div> --}}
	@endsection

	@section('subcontent')
	<div class="ui clearing segment">
		<div class="ui divided grid">
			<div class="four wide column" style="position: relative">
				@include('modules.utama.produk.kategori.menu')
			</div>
			<div class="twelve wide column">
				@include('modules.utama.produk.kategori.lists.divisi')
			</div>
		</div>
	</div>

	@endsection