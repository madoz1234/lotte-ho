<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Divisi</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm">
		<div class="field">
			<label>Kode	Divisi</label>
			<input type="text" name="kode_divisi" placeholder="Kode Divisi">
		</div>
		<div class="field">
			<label>Nama Divisi</label>
			<input type="text" name="nama_divisi" placeholder="Nama Divisi">
		</div>

		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>