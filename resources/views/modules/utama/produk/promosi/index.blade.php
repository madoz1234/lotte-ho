@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input name="filter[kode_promosi]" placeholder="Kode Promosi" type="text">
	</div>
	<div class="field">
		<input name="filter[nama_promosi]" placeholder="Nama Promosi" type="text">
	</div>

	<div class="field">
		<select name="filter[tipe_promo]" class="ui fluid search selection dropdown">
			<option value="">-- Tipe Promo --</option>
			<option value="1">Diskon</option>
			<option value="2">Promo Paket</option>
			<option value="3">Buy X Get Y</option>
			<option value="4">Purchase with Purchase</option>
		</select>
	</div>

	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	{{-- <div class="ui buttons" style="margin-right: 5px">
		<div class="ui buttons" style="margin-right: 5px">
			<div class="ui button"><i class="add icon icon"></i>All TMUK</div>
			<div class="ui floating dropdown icon button">
				<i class="dropdown icon"></i>
				<div class="menu">
					<div class="item alldiskon button"><i class="edit outline icon"></i>Diskon</div>
					<div class="item allpromo button"><i class="edit outline icon"></i>Promo Paket</div>
				</div>
			</div>
		</div>
	</div> --}}
	<div class="ui floating labeled icon dropdown button">
	  <i class="plus icon icon"></i>
	  <span class="text">All TMUK</span>
	  <div class="menu">
	    <div class="item alldiskon button">
	      <i class="edit outline icon"></i>
	      Diskon
	    </div>
	    <div class="item allpromo button">
	      <i class="edit outline icon"></i>
	      Promo Paket
	    </div>
	  </div>
	</div>
	{{-- <div class="ui green buttons" style="margin-right: 5px">
		<button type="button" class="ui green button"><i class="upload icon"></i>Upload</button>
		<div class="ui floating dropdown icon button">
			<i class="dropdown icon"></i>
			<div class="menu">
				<div class="item"><i class="download icon"></i> Download</div>
				<div class="item"><i class="file excel outline icon"></i> Export Excel</div>
			</div>
		</div>
	</div> --}}
@endsection

@section('js-filters')
    d.kode = $("input[name='filter[kode_promosi]']").val();
    d.nama = $("input[name='filter[nama_promosi]']").val();
	d.tipe_promo = $("select[name='filter[tipe_promo]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		region_id: {
			identifier: 'region_id',
			rules: [{
				type   : 'empty',
				prompt : 'pilihan region tidak boleh kosong'
			}]
		},

		lsi_id: {
			identifier: 'lsi_id',
			rules: [{
				type   : 'empty',
				prompt : 'pilihan lsi tidak boleh kosong'
			}]
		},

		tgl_awal: {
			identifier: 'tgl_awal',
			rules: [{
				type   : 'empty',
				prompt : 'isian tanggal awal tidak boleh kosong'
			}]
		},

		tgl_akhir: {
			identifier: 'tgl_akhir',
			rules: [{
				type   : 'empty',
				prompt : 'isian tanggal akhir tidak boleh kosong'
			}]
		},

		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'isian nama tidak boleh kosong'
			}]
		},

		kode_promo: {
			identifier: 'kode_promo',
			rules: [{
				type   : 'empty',
				prompt : 'isian kode promo tidak boleh kosong'
			}]
		},

		tipe_promo: {
			identifier: 'tipe_promo',
			rules: [{
				type   : 'empty',
				prompt : 'pilihan tipe promo tidak boleh kosong'
			}]
		},

		tmuk_id: {
			identifier: 'tmuk_id',
			rules: [{
				type   : 'empty',
				prompt : 'pilihan TMUK tidak boleh kosong'
			}]
		}
	};
</script>
@endsection

@section('init-modal')
	<script type="text/javascript">
		initModal = function(){
			$('.ui.radio.checkbox').checkbox();
			var nowDate = new Date();
			var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
			$('#from').calendar({
				type: 'date',
				minDate: new Date(),
				startDate: today,
				endCalendar: $('#to')
			})

			$('#to').calendar({
				type: 'date',
				minDate: new Date(),
				startDate: today,
				startCalendar: $('#from')
			})

			$('#combo').dropdown({
				onChange: function(e){
					var val = $('#combo').dropdown('get value');

					if(val == 1){
						$('.purchase').addClass('hidden');
						$('.promo').addClass('hidden');
						$('.buy').addClass('hidden');
						$('.diskon').removeClass('hidden');
						$('.box-type').removeClass('hidden');
					}else if(val == 2){
						$('.purchase').addClass('hidden');
						$('.buy').addClass('hidden');
						$('.diskon').addClass('hidden');
						$('.promo').removeClass('hidden');
						$('.box-type').removeClass('hidden');
					}else if(val == 3){
						$('.purchase').addClass('hidden');
						$('.diskon').addClass('hidden');
						$('.promo').addClass('hidden');
						$('.buy').removeClass('hidden');
						$('.box-type').removeClass('hidden');
					}else if(val == 4){
						$('.diskon').addClass('hidden');
						$('.promo').addClass('hidden');
						$('.buy').addClass('hidden');
						$('.purchase').removeClass('hidden');
						$('.box-type').removeClass('hidden');
					}
				}
			})

			$(document).ready(function(e){
				$('[name=region_id]').change(function(e){
					$(".loading").addClass('active');
					$.ajax({
						url: "{{ url('ajax/option/region') }}",
						type: 'GET',
						data:{
							id_region : $(this).val(),
						},
					})
					.success(function(response) {
						$(".loading").removeClass('active');
						
						$('.lsi').removeClass('disabled');
						$('[name=lsi_id]').html(response);
						//clear select all
						$('.tmuk').addClass('disabled');
						$('.tmuk').dropdown('clear');
						$('#selectall').prop("checked", false);

					})
					.fail(function() {
						console.log("error");
						$(".loading").removeClass('active');
					});
				});


				$('[name=lsi_id]').change(function(e){
					//generate produk by lsi
					$(".loading").addClass('active');

					$.ajax({
						url: "{{ url('ajax/option/lsi') }}",
						type: 'GET',
						data:{
							id_lsi : $(this).val(),
						},
					})
					.success(function(response) {
						$(".loading").removeClass('active');

						$('.kopro').removeClass('disabled');
						$('.btn-delete').removeClass('disabled');
						$('.btn-add').removeClass('disabled');
						$('#trigger_kode_produk_x').html(response);
						$('#trigger_kode_produk_y').html(response);
						$('#trigger_purchase_kode_produk').html(response);
					})
					.fail(function() {
						console.log("error");
					});

					//generate tmuk by lsi
					$.ajax({
						url: "{{ url('ajax/option/lsi-tmuk') }}",
						type: 'GET',
						data:{
							id_lsi : $(this).val(),
						},
					})
					.success(function(response) {
						$('.tmuk').dropdown('clear');
						$('#selectall').prop("checked", false);

						$('.tmuk').removeClass('disabled');
						$('#tmuk').html(response);
					})
					.fail(function() {
						console.log("error");
					});
				});

			});
		};
	</script>
@endsection

{{-- @section('subcontent')
	<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th rowspan="2" class="ui center aligned">No</th>
				<th rowspan="2" class="ui center aligned">Kode Promosi</th>
				<th rowspan="2" class="ui center aligned">Nama Promosi</th>
				<th rowspan="2" class="ui center aligned">Tipe Promosi</th>
				<th colspan="2" class="ui center aligned">Periode Promosi</th>
				<th rowspan="2" class="ui center aligned">Dibuat Pada</th>
				<th rowspan="2" class="ui center aligned">Aksi</th>
			</tr>
			<tr>
				<th class="ui center aligned">Mulai</th>
				<th class="ui center aligned">Selesai</th>
			</tr>
		</thead>
	</table>
@endsection --}}

@section('subcontent')
<div class="ui top demo tabular menu">
  <div class="active item" data-tab="first">Promosi</div>
  <div class="item" data-tab="second">Histori</div>
</div>
<div class="ui bottom demo active tab segment" data-tab="first">
	<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th rowspan="2" class="ui center aligned">No</th>
				<th rowspan="2" class="ui center aligned">Kode Promosi</th>
				<th rowspan="2" class="ui center aligned">Nama Promosi</th>
				<th rowspan="2" class="ui center aligned">Tipe Promosi</th>
				<th colspan="2" class="ui center aligned">Periode Promosi</th>
				<th rowspan="2" class="ui center aligned">Dibuat Pada</th>
				<th rowspan="2" class="ui center aligned">Aksi</th>
			</tr>
			<tr>
				<th class="ui center aligned">Mulai</th>
				<th class="ui center aligned">Selesai</th>
			</tr>
		</thead>
	</table>
</div>
<div class="ui bottom demo tab segment" data-tab="second">
	<table id="listTable2" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th rowspan="2" class="ui center aligned">No</th>
				<th rowspan="2" class="ui center aligned">Kode Promosi</th>
				<th rowspan="2" class="ui center aligned">Nama Promosi</th>
				<th rowspan="2" class="ui center aligned">Tipe Promosi</th>
				<th colspan="2" class="ui center aligned">Periode Promosi</th>
				<th rowspan="2" class="ui center aligned">Dibuat Pada</th>
				<th rowspan="2" class="ui center aligned">Aksi</th>
			</tr>
			<tr>
				<th class="ui center aligned">Mulai</th>
				<th class="ui center aligned">Selesai</th>
			</tr>
		</thead>
	</table>
</div>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function(){
		    $('.demo.menu .item').tab();
		});
	</script>
@append