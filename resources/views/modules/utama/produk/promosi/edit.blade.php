<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Promosi</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$promosi->id) }}" method="POST">
		{!! csrf_field() !!}
	    <input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $promosi->id }}">
		<div class="ui error message"></div>
		<div class="ui grid">
			<div class="eight wide column">
				<!-- Barang -->
				<h4 class="ui blue dividing header">Promosi</h4>
				<div class="field">
					<label>Periode</label>
					<div class="two fields">
						<div class="field">		
							<div class="ui calendar" id="from">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tgl_awal" type="text" placeholder="Tanggal Mulai" value="{{ $promosi->tgl_awal or "" }}">
								</div>
							</div>
						</div>
						
						<div class="field">		
							<div class="ui calendar" id="from">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tgl_akhir" type="text" placeholder="Tanggal Selesai" value="{{ $promosi->tgl_akhir or "" }}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="two fields">
					<div class="field">
						<label>Nama Promosi</label>
						<div class="ui input">
							<input name="nama" placeholder="Inputkan Nama Promosi" type="text" value="{{ $promosi->nama or "" }}">
						</div>
					</div>

					<div class="field">
						<label>Kode Promosi</label>
						<div class="ui disabled input">
							<input name="kode_promo" placeholder="Inputkan Kode Promosi" type="text" value="{{ $promosi->kode_promo or "" }}">
						</div>
					</div>
				</div>
				
				<div class="field">
					<label>Tipe Promosi</label>
					<div class="ui disabled fluid search selection dropdown">
						<input type="hidden" name="tipe_promo" value="{{ isset($promosi->tipe_promo) ? $promosi->tipe_promo : '' }}">
						<i class="dropdown icon"></i>
						<div class="default text">Tipe Promosi</div>
						<div class="menu">
							<div class="item {{ ($promosi->tipe_promo == 1 ) ? 'active selected' : '' }}" data-value="1" >Diskon</div>
							<div class="item {{ ($promosi->tipe_promo == 2 ) ? 'active selected' : '' }}" data-value="2" >Promo Paket</div>
							<div class="item {{ ($promosi->tipe_promo == 3 ) ? 'active selected' : '' }}" data-value="3" >Buy X Get Y</div>
							<div class="item {{ ($promosi->tipe_promo == 4 ) ? 'active selected' : '' }}" data-value="4" >Purchase with Purchase</div>
						</div>
					</div>
				</div>
			</div>
			<!-- kategori Promosi -->
			<div class="ui form eight wide column">

				<h4 class="ui blue dividing header">Area Promosi</h4>
				<div class="two fields">
					<div class="field">
						<label>Region</label>
						<select name="region_id" class="ui fluid search selection dropdown disabled" style="width: 100%;">
							{!! \Lotte\Models\Master\Region::options('area', 'id',['selected' => isset($promosi->region_id) ? $promosi->region_id : ''], '-- Pilih Region --') !!}
						</select>
					</div>

					<div class="field">
						<label>LSI</label>
						<select name="lsi_id" class="ui fluid search selection dropdown lsi disabled" id="lsi_id" style="width: 100%;">
							{!! \Lotte\Models\Master\Lsi::options('nama', 'id',['selected' => isset($promosi->lsi_id) ? $promosi->lsi_id : ''], '-- Pilih LSI --') !!}
						</select>
					</div>
				</div>

				<div class="field">
					<label>TMUK</label>
					<select name="tmuk_id[]" class="ui fluid search dropdown disabled" id="tmuk" multiple>
						{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',[], '-- Pilih TMUK --') !!}
					</select>
				</div>
				<div id="dtBox"></div>
				<div class="ui toggle checkbox disabled">
					<input type="checkbox" id="selectall">
					<label>Select all</label>
				</div>
			</div>
			<!-- kategori Promosi -->
		</div>
		<div class="field box-type">
			@if($promosi->tipe_promo == 1)
				<h4 class="ui blue dividing header">Diskon</h4>
				@include('modules.utama.produk.promosi.tipe-promo.diskon')
			@elseif($promosi->tipe_promo == 2)			
				<h4 class="ui blue dividing header">Promo Paket</h4>
				@include('modules.utama.produk.promosi.tipe-promo.promo')
			@elseif($promosi->tipe_promo == 3)
				<h4 class="ui blue dividing header">Buy X Get Y</h4>
				@include('modules.utama.produk.promosi.tipe-promo.buy-x-get-y')
			@elseif($promosi->tipe_promo == 4)
				<h4 class="ui blue dividing header">Purchase With Purchase</h4>
				@include('modules.utama.produk.promosi.tipe-promo.purchase')
			@endif
		</div>
	</form>
</div>

<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	<?php 
		if (isset($promosi)) {
			$tmuk = [];
			$array = [];
			foreach ($promosi->detailTmuk as $key => $value) {
				$array[] = $value->id;
			}
			$tmuk = json_encode($array);
		}
	?>
	$(document).ready(function(e){
		$("[name='tmuk_id[]']").val({{ isset($tmuk) ? $tmuk : '' }});

		$('.ui.checkbox').checkbox({
			onChecked() {
				const options = $('#tmuk > option').toArray().map(
					(obj) => obj.value
					);
				$('#tmuk').dropdown('set exactly', options);
			},
			onUnchecked() {
				$('#tmuk').dropdown('clear');
			},
		});

		$(".hapus").on('click', function() {
			$('.case:checkbox:checked').parents("tr").remove();
			$('.check_all').prop("checked", false);
			check();

		});

		// untuk manipulasi tabel
		get_index = function(elm)
		{
			return $(elm).closest("tr").index();
		}

		delete_row = function(elm)
		{
			var idx = get_index(elm);

			if (idx != 0) {
				var tr = $(elm).closest("tr");
				$(tr).fadeOut(500, function(){
					$(this).remove();
				});
			}
		}

		$('#logsheet').DataTable({
			paging: false,
			filter: false,
			lengthChange: false,
			ordering:false,
			scrollX: true,
			scrollY: 300,
		});

		$('#promo').DataTable({
			paging: false,
			filter: false,
			lengthChange: false,
			ordering:false,
			scrollX: true,
			scrollY: 300,
		});
    });
</script>