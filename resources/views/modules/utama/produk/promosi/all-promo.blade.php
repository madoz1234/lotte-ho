<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Promo All TMUK</div>
<div class="content">
	{{-- <form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST"> --}}
	<form class="ui data form" id="dataForm" action="{{ URL::to('utama/produk/promosi/postallpromo') }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="ui grid">
			<div class="eight wide column">
				<!-- Barang -->
				<h4 class="ui blue dividing header">Promosi</h4>
				<div class="field">
					<label>Periode</label>
					<div class="two fields">
						<div class="field">		
							<div class="ui calendar" id="from">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tgl_awal" type="text" placeholder="Tanggal Mulai" value="{{ $tgl_awal or "" }}">
								</div>
							</div>
						</div>
						
						<div class="field">		
							<div class="ui calendar" id="to">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tgl_akhir" type="text" placeholder="Tanggal Selesai" value="{{ $tgl_akhir or "" }}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="two fields">
					<div class="field">
						<label>Nama Promosi</label>
						<input name="nama" placeholder="Inputkan Nama Promosi" type="text" value="{{ $nama or "" }}">
					</div>

					<div class="field">
						<label>Kode Promosi</label>
						<input name="kode_promo" placeholder="Inputkan Kode Promosi" type="text" value="{{ $kode_promo or "" }}">
					</div>
				</div>
			</div>
			<!-- kategori Promosi -->
			<div class="ui form eight wide column">

				<h4 class="ui blue dividing header">Area Promosi</h4>
				<div class="field">
					<label>TMUK</label>		
					<select name="tmuk_id[]" class="ui fluid search dropdown tmuk" id="tmuk" multiple>
						{{-- <option value="0">-- Pilih TMUK --</option> --}}
						{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',[], '-- Pilih TMUK --') !!}
					</select>
				</div>
				<div id="dtBox"></div>
				<div class="ui toggle checkbox tmuk">
					<input type="checkbox" id="selectall">
					<label>Select all</label>
				</div>
			</div>
			<div class="ui form eight wide column">

				<h4 class="ui blue dividing header">Upload File Excel</h4>
					<div class="field">
					    <div class="ui left action input">
					      {{-- <button type="button" class="ui blue button btn-template" onclick="javascript:download_template_opening();" style="margin-right: 20px">
							  	<i class="download icon icon"></i>
							  	Template
							  </button> --}}
						  <div class="ui blue buttons" style="margin-right: 20px">
							<a href="{{ asset('template\promo-all-tmuk.xls') }}" class="ui grey button" target="_blank"><i class="download icon icon"></i>Template</a>
						  </div>

						  <label for="file" class="ui icon labeled button">
						  	<i class="file icon"></i>
						    Pilih File
						  </label>
						  <input type="file" name="upload_data_all_promo_tmuk" id="file" style="display: none" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
						  <input type="text" id="filename" value="">
						</div>
					</div>
			</div>
			<!-- kategori Promosi -->
		</div>
	</form>
<script type="text/javascript">
	$(document).ready(function(e){
		$('.ui.checkbox').checkbox({
			onChecked() {
				const options = $('#tmuk > option').toArray().map(
					(obj) => obj.value
					);
				$('#tmuk').dropdown('set exactly', options);
			},
			onUnchecked() {
				$('#tmuk').dropdown('clear');
			},
		});

		$('input[type=file]').change(function () {
		$('#filename').val(this.files[0]['name']);
		})

		$('#filename').click(function () {
			$('#file').click();
		})
	
    });
</script>
</div>

<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button btn-submit">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>