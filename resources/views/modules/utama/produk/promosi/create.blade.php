<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Promosi</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="ui grid">
			<div class="eight wide column">
				<!-- Barang -->
				<h4 class="ui blue dividing header">Promosi</h4>
				<div class="field">
					<label>Periode</label>
					<div class="two fields">
						<div class="field">		
							<div class="ui calendar" id="from">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tgl_awal" type="text" placeholder="Tanggal Mulai" value="{{ $tgl_awal or "" }}">
								</div>
							</div>
						</div>
						
						<div class="field">		
							<div class="ui calendar" id="to">
								<div class="ui input left icon">
									<i class="calendar icon date"></i>
									<input name="tgl_akhir" type="text" placeholder="Tanggal Selesai" value="{{ $tgl_akhir or "" }}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="two fields">
					<div class="field">
						<label>Nama Promosi</label>
						<input name="nama" placeholder="Inputkan Nama Promosi" type="text" value="{{ $nama or "" }}">
					</div>

					<div class="field">
						<label>Kode Promosi</label>
						<input name="kode_promo" placeholder="Inputkan Kode Promosi" type="text" value="{{ $kode_promo or "" }}">
					</div>
				</div>

				<div class="field">
					<label>Tipe Promosi</label>
					<div class="ui fluid search selection dropdown" id="combo">
						<input type="hidden" name="tipe_promo">
						<i class="dropdown icon"></i>
						<div class="default text">Tipe Promosi</div>
						<div class="menu">
							<div class="item" data-value="1">Diskon</div>
							<div class="item" data-value="2">Promo Paket</div>
							<div class="item" data-value="3">Buy X Get Y</div>
							<div class="item" data-value="4">Purchase with Purchase</div>
						</div>
					</div>
				</div>
			</div>
			<!-- kategori Promosi -->
			<div class="ui form eight wide column">

				<h4 class="ui blue dividing header">Area Promosi</h4>
				<div class="two fields">
					<div class="field">
						<label>Region</label>
						<select name="region_id" class="ui fluid search selection dropdown" style="width: 100%;">
							{!! \Lotte\Models\Master\Region::options('area', 'id',[], '-- Pilih Region --') !!}
						</select>
					</div>

					<div class="field">
						<label>LSI</label>
						<select name="lsi_id" class="ui fluid search selection dropdown lsi disabled" id="lsi_id" style="width: 100%;">
							<option value="0">-- Pilih LSI --</option>
							{{-- {!! \Lotte\Models\Master\Lsi::options('nama', 'id',[], '-- Pilih LSI --') !!} --}}
						</select>
					</div>
				</div>

				<div class="field">
					<label>TMUK</label>		
					<select name="tmuk_id[]" class="ui fluid search dropdown tmuk disabled" id="tmuk" multiple>
						<option value="0">-- Pilih TMUK --</option>
						{{-- {!! \Lotte\Models\Master\Tmuk::options('nama', 'id',[], '-- Pilih TMUK --') !!} --}}
					</select>
				</div>
				<div id="dtBox"></div>
				<div class="ui toggle checkbox tmuk disabled">
					<input type="checkbox" id="selectall">
					<label>Select all</label>
				</div>
			</div>
			<!-- kategori Promosi -->
		</div>
		<div class="field box-type hidden">
			<h4 class="ui blue dividing header diskon hidden">Diskon</h4>
			@include('modules.utama.produk.promosi.tipe-promo.diskon')
			
			<h4 class="ui blue dividing header promo hidden">Promo Paket</h4>
			@include('modules.utama.produk.promosi.tipe-promo.promo')

			<h4 class="ui blue dividing header buy hidden">Buy X Get Y</h4>
			@include('modules.utama.produk.promosi.tipe-promo.buy-x-get-y')

			<h4 class="ui blue dividing header purchase hidden">Purchase With Purchase</h4>
			@include('modules.utama.produk.promosi.tipe-promo.purchase')
		</div>
	</form>
<script type="text/javascript">
	$(document).ready(function(e){
		$('.message .close').on('click', function() {
		    $(this).closest('.message').transition('fade');
		});
		
		$('.ui.checkbox').checkbox({
			onChecked() {
				const options = $('#tmuk > option').toArray().map(
					(obj) => obj.value
					);
				$('#tmuk').dropdown('set exactly', options);
			},
			onUnchecked() {
				$('#tmuk').dropdown('clear');
			},
		});

		$(".hapus").on('click', function() {
			$('.case:checkbox:checked').parents("tr").remove();
			$('.check_all').prop("checked", false);
			check();

		});

		// untuk manipulasi tabel
		get_index = function(elm)
		{
			return $(elm).closest("tr").index();
		}

		delete_row = function(elm)
		{
			var idx = get_index(elm);

			if (idx != 0) {
				var tr = $(elm).closest("tr");
				$(tr).fadeOut(500, function(){
					$(this).remove();
				});
			}
		}

    });
</script>

</div>

<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button btn-submit">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>