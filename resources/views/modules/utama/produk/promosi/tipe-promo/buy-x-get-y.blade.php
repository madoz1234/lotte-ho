<!-- Buy X Get Y -->
<div class="ui two cards buy {{ isset($promosi) ? '' : 'hidden' }}">
	<?php
		if (isset($promosi)) {
			$buy = $promosi->detailBuyxgety;
		}
	?>
	<div class="card">
		<div class="content">
			<div class="eight wide column">
				<div class="ui data form">
					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Kode Produk</label>
						<div class="twelve wide field">
							<input name="kode_produk_x" type="hidden" value="{{ isset($buy) ? $buy->kode_produk_x : '' }}">
							@if(isset($promosi))
								<input type="hidden" name="buy_id" value="{{ isset($buy) ? $buy->id : '' }}">
								<input placeholder="Inputkan Kode Produk" name="kode_produk_x" type="text" value="{{ isset($buy) ? $buy->kode_produk_x : '' }}" readonly="" style="background-color: #edf1f6;">
							@else
								<select name="trigger_kode_produk_x" id="trigger_kode_produk_x" class="ui fluid search selection select2 dropdown kopro disabled" style="width: 100%;">
									<option value="0">-- Pilih Kode Produk --</option>
									{{-- {!! \Lotte\Models\Master\ProdukLsi::options('produk_kode', 'id',[], '-- Pilih Kode Produk --') !!} --}}
								</select>
							@endif
						</div>
					</div><br>

					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Nama Produk</label>
						<div class="twelve wide field">
							<input placeholder="Inputkan Nama Produk" name="nama_produk_x" type="text" value="{{ isset($buy) ? $buy->nama_produk_x : '' }}" readonly="" style="background-color: #edf1f6;">
						</div>
					</div><br>

					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Cost Price</label>
						<div class="twelve wide field">
							<div class="ui right labeled input">
							    <div class="ui label">
							      Rp.
							    </div>
								<input placeholder="Inputkan Harga" name="harga_jual_x" id="harga_jual_x" type="text" value="{{ isset($buy) ? $buy->harga_jual_x : '' }}" data-inputmask="'alias' : 'numeric'" readonly="" style="background-color: #edf1f6;">
						    </div>
						</div>
					</div><br>

					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Harga Jual</label>
						<div class="twelve wide field">
							<div class="ui right labeled input harga-diskon-x {{ isset($buy) ? (($buy->produkx->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
							    <div class="ui label">
							      Rp.
							    </div>
								<input placeholder="Inputkan Harga Jual" name="harga_diskon_x" id="harga_diskon_x" type="text" value="{{ isset($buy) ? $buy->harga_diskon_x : '' }}" onkeyup="priceDiskonX()" data-inputmask="'alias' : 'numeric'">
						    </div>
						</div>
					</div><br>

					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Diskon</label>
						<div class="twelve wide field">
							<div class="ui right labeled input diskon-x {{ isset($buy) ? (($buy->produkx->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
							    <div class="ui label">Rp.</div>
								<input placeholder="Inputkan Diskon" name="diskon_x" id="diskon_x" type="text" value="{{ isset($buy) ? $buy->diskon_x : '' }}" data-inputmask="'alias' : 'numeric'" readonly="" style="background-color: #edf1f6;">
						    </div>
						</div>
					</div><br>

				</div>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="content">
			<div class="eight wide column">
				<div class="ui data form">
					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Kode Produk</label>
						<div class="twelve wide field">
							<input name="kode_produk_y" type="hidden" value="{{ isset($buy) ? $buy->kode_produk_y : '' }}">
							@if(isset($promosi))
								<input placeholder="Inputkan Kode Produk" name="kode_produk_y" type="text" value="{{ isset($buy) ? $buy->kode_produk_y : '' }}" readonly="" style="background-color: #edf1f6;">
							@else
								<select name="trigger_kode_produk_y" id="trigger_kode_produk_y" class="ui fluid search selection select2 dropdown kopro disabled" style="width: 100%;">
									<option value="0">-- Pilih Kode Produk --</option>
									{{-- {!! \Lotte\Models\Master\ProdukLsi::options('produk_kode', 'id',[], '-- Pilih Kode Produk --') !!} --}}
								</select>
							@endif
						</div>
					</div><br>

					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Nama Produk</label>
						<div class="twelve wide field">
							<input placeholder="Inputkan Nama Produk" name="nama_produk_y" type="text" value="{{ isset($buy) ? $buy->nama_produk_y : '' }}" readonly="" style="background-color: #edf1f6;">
						</div>
					</div><br>

					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Cost Price</label>
						<div class="twelve wide field">
							<div class="ui right labeled input">
							    <div class="ui label">
							      Rp.
							    </div>
								<input placeholder="Inputkan Harga" name="harga_jual_y" id="harga_jual_y" type="text" value="{{ isset($buy) ? $buy->harga_jual_y : '' }}" data-inputmask="'alias' : 'numeric'" readonly="" style="background-color: #edf1f6;">
						    </div>
						</div>
					</div><br>

					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Harga Jual</label>
						<div class="twelve wide field">
							<div class="ui right labeled input harga-diskon-y {{ isset($buy) ? (($buy->produky->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
							    <div class="ui label">
							      Rp.
							    </div>
								<input placeholder="Inputkan Harga Jual" name="harga_diskon_y" id="harga_diskon_y" type="text" value="{{ isset($buy) ? $buy->harga_diskon_y : '' }}" data-inputmask="'alias' : 'numeric'" onkeyup="priceDiskonY()">
						    </div>
						</div>
					</div><br>

					<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
						<label class="four wide field">Diskon</label>
						<div class="twelve wide field">
							<div class="ui right labeled input diskon-y {{ isset($buy) ? (($buy->produky->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
							    <div class="ui label">Rp.</div>
								<input placeholder="Inputkan Diskon" name="diskon_y" id="diskon_y" type="text" value="{{ isset($buy) ? $buy->diskon_y : '' }}" data-inputmask="'alias' : 'numeric'" readonly="" style="background-color: #edf1f6;">
						    </div>
						</div>
					</div><br>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui centered grid buy {{ isset($promosi) ? '' : 'hidden' }}">
	<div class="eight wide column">
	</div>
	<div class="eight wide column">
		<div class="ui data form">
			<div class="inline fields buy {{ isset($promosi) ? '' : 'hidden' }}">
				<label class="four wide field">Total Diskon</label>
				<div class="twelve wide field">
					<div class="ui labeled input">
						<div class="ui label">Rp</div>
						<input name="total_diskon" id="total_diskon" placeholder="Inputkan" type="text" value="{{ isset($buy->total_diskon) ? $buy->total_diskon : '0,00' }}" readonly="" style="background-color: #edf1f6;"  data-inputmask="'alias' : 'numeric'">
					</div>
				</div>
			</div><br>

		</div>
	</div>
</div>
<!-- Buy X Get Y -->
		
@include('layouts.scripts.inputmask')

<script type="text/javascript">
	$(document).ready(function(e) {
		// buy x get y 
		$('select[name="trigger_kode_produk_x"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-produk/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="kode_produk_x"]').val(response.kode)
				$('input[name="nama_produk_x"]').val(response.nama)
				$('input[name="harga_jual_x"]').val(response.harga)

				var lock_price = response.locked_price;
				if (lock_price == 1) {
					$('.diskon-x').addClass('disabled')
					$('input[name="diskon_x"]').val('0')
					
					$('.harga-diskon-x').addClass('disabled')
					$('input[name="harga_diskon_x"]').val('0')
				}else{
					$('.diskon-x').removeClass('disabled')
					
					$('.harga-diskon-x').removeClass('disabled')
				}
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		});

		$('select[name="trigger_kode_produk_y"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-produk/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="kode_produk_y"]').val(response.kode)
				$('input[name="nama_produk_y"]').val(response.nama)
				$('input[name="harga_jual_y"]').val(response.harga)

				var lock_price = response.locked_price;
				if (lock_price == 1) {
					$('.diskon-y').addClass('disabled')
					$('input[name="diskon_y"]').val('0')
					
					$('.harga-diskon-y').addClass('disabled')
					$('input[name="harga_diskon_y"]').val('0')
				}else{
					$('.diskon-y').removeClass('disabled')
					
					$('.harga-diskon-y').removeClass('disabled')
				}
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		});
	})
	function sum() {
		var txtFirstNumberValue = document.getElementById('diskon_x').value;
		var val1 = txtFirstNumberValue.replace(',00', "");
		var new_val1 = val1.replace(/[^0-9\,-]+/g, "");

		var txtSecondNumberValue = document.getElementById('diskon_y').value;
		var val2 = txtSecondNumberValue.replace(',00', "");
		var new_val2 = val2.replace(/[^0-9\,-]+/g, "");

		var result = parseFloat(new_val1) + parseFloat(new_val2);
		if (!isNaN(result)) {
			document.getElementById('total_diskon').value = result+',00';
		}else{
			document.getElementById('total_diskon').value = 0;
		}
	}

	function priceDiskonX() {
		var harga_diskon_x = document.getElementById('harga_diskon_x').value;
		var val1 = harga_diskon_x.replace(',00', "");
		var new_val1 = val1.replace(/[^0-9\,-]+/g, "");

		var harga_jual_x = document.getElementById('harga_jual_x').value;
		var val2 = harga_jual_x.replace(',00', "");
		var new_val2 = val2.replace(/[^0-9\,-]+/g, "");

		var result = (parseFloat(new_val2) - parseFloat(new_val1));
		console.log(new_val1);
		console.log(new_val2);
		console.log(result);
		if (result > 0) {
			document.getElementById('diskon_x').value = parseFloat(result).toFixed(2);
			sum();
		}else{
			document.getElementById('diskon_x').value = 0;
		}
	}

	function priceDiskonY() {
		var harga_diskon_y = document.getElementById('harga_diskon_y').value;
		var val1 = harga_diskon_y.replace(',00', "");
		var new_val1 = val1.replace(/[^0-9\,-]+/g, "");

		var harga_jual_y = document.getElementById('harga_jual_y').value;
		var val2 = harga_jual_y.replace(',00', "");
		var new_val2 = val2.replace(/[^0-9\,-]+/g, "");

		var result = (parseFloat(new_val2) - parseFloat(new_val1));

		if (result > 0) {
			document.getElementById('diskon_y').value = parseFloat(result).toFixed(2);
			sum();
		}else{
			document.getElementById('diskon_y').value = 0;
		}
	}
</script>