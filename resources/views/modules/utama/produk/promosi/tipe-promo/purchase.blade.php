<div class="ui two cards purchase {{ isset($promosi) ? '' : 'hidden' }}">
	<?php
		if (isset($promosi)) {
			$purchase = $promosi->detailPurchase;
		}
	?>
	<div class="card" style="width: 60%">
		<div class="ui centered grid">
			<div class="full wide column">
				<table id="purchase" class="ui celled compact red table">
					<thead>
						<tr>
							<th style="text-align: center;">#</th>
							<th style="text-align: center;" width="20%">Kode Produk</th>
							<th style="text-align: center;">Nama Produk</th>
							<th style="text-align: center;">Suggested Rounded Selling Price</th>
						</tr>
					</thead>
					<tbody>
						@if(isset($purchase))
							<?php 
								$detail = $purchase->detailList->where('status', 0);
							?>
							@foreach($detail as $list)
								<tr>
									<td><input type="checkbox" class="case" value="{{ $list->id }}"></td>
									<td>
										<div class="field">
											<div class="ui input">
												<input type="hidden" name="purchase_list_id[]" value="{{ $list->id }}">
												<input type="text" name="purchase_list_kode_produk[]" placeholder="Inputkan Kode" onkeypress="return isNumberKey(event)" value="{{ $list->kode_produk }}" readonly="" style="background-color: #edf1f6;">
											</div>
										</div>
									</td>
									<td>
										<div class="field">
											<div class="ui input">
												<input type="text" name="purchase_list_nama_produk[]" placeholder="Inputkan Nama" value="{{ $list->nama_produk }}" readonly="" style="background-color: #edf1f6;">
											</div>
										</div>
									</td>
									<td>
										<div class="field">
											<div class="ui right labeled input">
											    <div class="ui label">Rp.</div>
												<div class="ui input">
													<input type="text" name="purchase_list_selling_price[]" class="purchase_list_selling_price" placeholder="Inputkan Selling Price" value="{{ $list->selling_price }}" readonly="" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;">
												</div>
										    </div>
										</div>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
					<tfoot>
						<tr>
							<th colspan="3" class="ui right aligned">Total</th>
							<th colspan="1" class="ui center aligned">
								<div class="ui right labeled input">
								    <div class="ui label">Rp.</div>
								    <div class="ui input">
										<input type="text" id="purchase_list_total_selling_price" name="purchase_list_total_selling_price[]" readonly="" value="{{ isset($purchase) ? ($purchase->detailList ? FormatNumber($purchase->detailList->sum('selling_price')) : FormatNumber(0)) : FormatNumber(0) }}" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;">
									</div>
							    </div>
							</th>
						</tr>
					</tfoot>
				</table>
				<button type="button" style="margin-left:30px; margin-bottom: 10px" class="hapus ui mini red icon trash button btn-delete {{ isset($promosi) ? '' : 'disabled'}}"><i class="icon trash button"></i></button>
				<button type="button" onclick="add_row_purchase()"  style="margin-bottom: 10px" class="ui mini blue icon plus button btn-add {{ isset($promosi) ? '' : 'disabled'}}"><i class="icon plus button"></i></button>

				<template id="template-row-purchase">
					<tr>
						<td><input type="checkbox" class="case"/></td>
						<td>
							<div class="field">
								{{-- <input name="purchase_list_kode_produk[]" class="purchase_list_kode_produk" type="hidden" value="{{ isset($purchase) ? $purchase->kode_produk : '' }}">
								<select name="trigger_purchase_list_kode_produk[]" class="ui fluid search selection dropdown purchase-kopro" style="width: 100%;">
									{!! \Lotte\Models\Master\ProdukLsi::options('produk_kode', 'id',[], '-- Pilih Kode Produk --') !!}
								</select> --}}
								<div class="ui action input">
				                    <input type="text" name="purchase_list_kode_produk[]" class="form-control kopro purchase_list_kode_produk" onkeypress="return isNumberKey(event)">
				                    <button type="button" class="ui button btn-cek-kode"><i class="search icon"></i></button>
				                </div>
							</div>
						</td>
						<td>
							<div class="field">
								<input type="text" name="purchase_list_nama_produk[]" class="purchase_list_nama_produk" placeholder="Inputkan Nama" readonly="" style="background-color: #edf1f6;">
							</div>
						</td>
						<td>
							<div class="field">
								<div class="ui right labeled input">
									<div class="ui label">Rp.</div>
									<input type="text" name="purchase_list_selling_price[]" class="purchase_list_selling_price" placeholder="Inputkan Selling Price" readonly="" style="background-color: #edf1f6;" data-inputmask="'alias' : 'numeric'">
								</div>
							</div>
						</td>
					</tr>
				</template>
			</div>
		</div>
	</div>

	<div class="card" style="width:36%">
		<div class="content">
			<div class="full wide column">
				<div class="inline fields purchase {{ isset($promosi) ? '' : 'hidden' }}">
					<label class="four wide field">Kode Produk</label>
					<div class="twelve wide field">
						<input name="purchase_kode_produk" type="hidden" value="{{ isset($purchase) ? $purchase->kode_produk : '' }}">
						@if(isset($promosi))
							<input type="hidden" name="purchase_id" value="{{ isset($purchase) ? $purchase->id : '' }}">
							<input placeholder="Inputkan Kode Produk" name="purchase_kode_produk" type="text" value="{{ isset($purchase) ? $purchase->kode_produk : '' }}" readonly="" style="background-color: #edf1f6;">
						@else
							<select name="trigger_purchase_kode_produk" id="trigger_purchase_kode_produk" class="ui fluid search selection dropdown kopro disabled" style="width: 100%;">
								<option value="0">-- Pilih Kode Produk --</option>
								{{-- {!! \Lotte\Models\Master\ProdukLsi::options('produk_kode', 'id',[], '-- Pilih Kode Produk --') !!} --}}
							</select>
						@endif
					</div>
				</div><br>

				<div class="inline fields purchase {{ isset($promosi) ? '' : 'hidden' }}">
					<label class="four wide field">Nama Produk</label>
					<div class="twelve wide field">
						<input placeholder="Inputkan Nama Produk" type="text" name="purchase_nama_produk" value="{{ isset($purchase) ? $purchase->nama_produk : '' }}" readonly="" style="background-color: #edf1f6;">
					</div>
				</div><br>

				<div class="inline fields purchase {{ isset($promosi) ? '' : 'hidden' }}">
					<label class="four wide field">Suggested Rounded Selling Price</label>
					<div class="twelve wide field">
						<div class="ui labeled input">
							<div class="ui label">Rp.</div>
							<input placeholder="Inputkan Selling Price" type="text" name="purchase_selling_price" id="purchase_selling_price" value="{{ isset($purchase) ? $purchase->selling_price : '' }}" readonly="" style="background-color: #edf1f6;" data-inputmask="'alias' : 'numeric'">
						</div>
					</div>
				</div><br>


				<div class="inline fields purchase {{ isset($promosi) ? '' : 'hidden' }}">
					<label class="four wide field">Harga Jual</label>
					<div class="twelve wide field">
						<div class="ui labeled input purchase-harga-terdiskon {{ isset($purchase) ? (($purchase->produk->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
							<div class="ui label">Rp.</div>
							<input placeholder="Inputkan Harga Terdiskon" type="text" name="purchase_harga_terdiskon" id="purchase_harga_terdiskon" data-inputmask="'alias' : 'numeric'" value="{{ isset($purchase) ? $purchase->harga_terdiskon : '' }}" onkeyup="priceDiskonPwp()">
						</div>
					</div>
				</div><br>
				
				<div class="inline fields purchase {{ isset($promosi) ? '' : 'hidden' }}">
					<label class="four wide field">Diskon</label>
					<div class="twelve wide field">
						<div class="ui labeled input purchase-diskon {{ isset($purchase) ? (($purchase->produk->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
							<div class="ui label">Rp.</div>
							<input placeholder="Inputkan Diskon" type="text" name="purchase_diskon" id="purchase_diskon" data-inputmask="'alias' : 'numeric'" value="{{ isset($purchase) ? $purchase->diskon : '' }}" readonly="" style="background-color: #edf1f6;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('layouts.scripts.inputmask')

<script type="text/javascript">
	$(document).ready(function(e) {
		$(document).on('click', '.btn-cek-kode', function(e){
			var _this = $(this);
			var kode = _this.closest('tr').find('.purchase_list_kode_produk').val();
			var lsi = $("#lsi_id").val();

			$(".loading").addClass('active');

			$.ajax({
                url: "{{url($pageUrl)}}/on-change-pop-produk-by-kode/"+kode+'/'+lsi,
                type: 'GET',
                dataType: 'json',
            })
            .done(function(response) {
				$(".loading").removeClass('active');
                // console.log(response);
				_this.closest('tr').find('.purchase_list_nama_produk').val(response.nama)
				_this.closest('tr').find('.purchase_list_selling_price').val(response.harga)

				totalSellingPrice();
				$('.btn-submit').removeClass('disabled');
            })
            .error(function(resp) {
				$(".loading").removeClass('active');
				swal(
					'Oops!',
					'Data Tidak Ditemukan',
					'warning'
				).then((result) => {
					$('.btn-submit').addClass('disabled');
				})
            });
		});

		add_row_purchase = function()
		{
            var tr = $('#template-row-purchase').html();

            // append to table
            $('table#purchase > tbody').append(tr);

            //attribut
        	reloadMask();
            
        }

		//triger promoso jenis purchase
		$('select[name="trigger_purchase_kode_produk"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-produk/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="purchase_kode_produk"]').val(response.kode)
				$('input[name="purchase_nama_produk"]').val(response.nama)
				$('input[name="purchase_selling_price"]').val(response.harga)

				var lock_price = response.locked_price;
				if (lock_price == 1) {
					$('.purchase-diskon').addClass('disabled')
					$('input[name="purchase_diskon"]').val('0')
					
					$('.purchase-harga-terdiskon').addClass('disabled')
					$('input[name="purchase_harga_terdiskon"]').val('0')
				}else{
					$('.purchase-diskon').removeClass('disabled')
					
					$('.purchase-harga-terdiskon').removeClass('disabled')
				}
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		});

	})

	function totalSellingPrice() {
		var element = document.getElementsByClassName('purchase_list_selling_price');
    	var total = 0;
    	var price = 0;
    	var nilai = 0;

	    for (var i = 0; i < element.length; i++){
	    	nilai = element[i].value;
	    	if (nilai != undefined) {
	    		price =  nilai.replace(/[^0-9\,-]+/g, "");
	    		total += parseFloat(price);
	    	}
	    }

	    if (!isNaN(total)) {
	    	$('#purchase_list_total_selling_price').val(total+',00');
	    }else{
	    	$('#purchase_list_total_selling_price').val('0,00');
	    }
	}

	function priceDiskonPwp() {
		var purchase_selling_price = document.getElementById('purchase_selling_price').value;
		var val1 = purchase_selling_price.replace(',00', "");
		var new_val1 = val1.replace(/[^0-9\,-]+/g, "");

		var purchase_harga_terdiskon = document.getElementById('purchase_harga_terdiskon').value;
		var val2 = purchase_harga_terdiskon.replace(',00', "");
		var new_val2 = val2.replace(/[^0-9\,-]+/g, "");

		var result = (parseFloat(new_val1) - parseFloat(new_val2));

		if (result > 0) {
			document.getElementById('purchase_diskon').value = parseFloat(result).toFixed(2);
			sum();
		}else{
			document.getElementById('purchase_diskon').value = 0;
		}
	}
</script>