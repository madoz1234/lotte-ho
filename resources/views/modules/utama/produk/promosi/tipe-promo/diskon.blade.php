<!-- Diskon -->
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui centered grid diskon {{ isset($promosi) ? '' : 'hidden' }}">
	<div class="full wide column">
		<table id="logsheet" class="ui celled compact red table" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th style="text-align: center;">#</th>
					<th style="text-align: center;">Kode Produk</th>
					<th style="text-align: center;">Nama Produk</th>
					<th style="text-align: center;">Cost Price</th>
					<th style="text-align: center;">Harga Awal</th>
					<th style="text-align: center;">Harga Terdiskon</th>
					<th style="text-align: center;">Diskon</th>
					<th style="text-align: center;">Diskon %</th>
				</tr>
			</thead>
			<tbody>
				@if(isset($promosi))
					<?php 
						$detail = $promosi->detailDiskon->where('status', 0);
					?>
					@foreach($detail as $diskon)
						<tr>
							<td><input type="checkbox" name="diskon_id[]" class="case" value="{{ $diskon->id }}"></td>
							<td>
								<div class="field">
									<div class="ui input">
										<input type="hidden" name="diskon_id[]" value="{{ $diskon->id }}">
										<input type="text" name="diskon_kode_produk[]" placeholder="Inputkan Kode" onkeypress="return isNumberKey(event)" value="{{ $diskon->kode_produk }}" readonly="">
									</div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui input">
										<input type="text" name="diskon_nama_produk[]" placeholder="Inputkan Nama" value="{{ $diskon->nama_produk }}" readonly="" style="background-color: #edf1f6;" readonly="">
									</div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui right labeled input">
									    <div class="ui label">Rp.</div>
										<div class="ui input">
											<input type="text" name="diskon_cost_price[]" class="diskon_cost_price" placeholder="Inputkan Cost Price" value="{{ $diskon->cost_price }}" readonly="" style="background-color: #edf1f6;" readonly="" data-inputmask="'alias' : 'numeric'">
										</div>
								    </div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui right labeled input">
									    <div class="ui label">Rp.</div>
										<div class="ui input">
											<input type="text" name="harga_awal[]" class="harga-awal" placeholder="Inputkan Harga Awal" value="{{ $diskon->harga_awal }}" readonly="" style="background-color: #edf1f6;" readonly="" data-inputmask="'alias' : 'numeric'">
										</div>
								    </div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui right labeled input {{ isset($diskon) ? (($diskon->produk->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
									    <div class="ui label">Rp.</div>
										<div class="ui input">
											<input type="text" name="diskon_harga_terdiskon[]" class="harga-terdiskon" placeholder="Inputkan Harga Terdiskon" value="{{ $diskon->harga_terdiskon }}" data-inputmask="'alias' : 'numeric'" onkeyup="calculateDiskon(this)">
										</div>
								    </div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui right labeled input {{ isset($diskon) ? (($diskon->produk->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
									    <div class="ui label">Rp.</div>
										<div class="ui input">
											<input type="text" name="diskon_diskon[]" class="diskon" placeholder="Inputkan Diskon" value="{{ $diskon->diskon }}" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="">
										</div>
								    </div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui right labeled input">
										<div class="ui right labeled input diskon_persen">
											<input type="text" id="diskon_persen" class="diskon_persen" placeholder="Diskon %" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="">
											<div class="ui label">%</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
			<tfoot>
				<tr>
					<button type="button" style="margin-bottom: 10px" class="hapus ui mini red icon trash button btn-delete {{ isset($promosi) ? '' : 'disabled'}}"><i class="icon trash button"></i></button>
					<button type="button" onclick="add_row()" style="margin-bottom: 10px" class="ui mini blue icon plus button btn-add {{ isset($promosi) ? '' : 'disabled'}}"><i class="icon plus button"></i></button>
				</tr>
			</tfoot>
		</table>

		<template id="template-row">
			<tr>
				<td><input type="checkbox" class="case"/></td>
				<td>
					<div class="field">
						{{-- <input type="text" name="diskon_kode_produk[]" placeholder="Inputkan Kode" onkeypress="return isNumberKey(event)"> --}}
						{{-- <select name="diskon_kode_produk[]" class="ui fluid search selection dropdown kopro" style="width: 100%;" >
							{!! \Lotte\Models\Master\ProdukLsi::options('produk_kode', 'produk_kode',[], '-- Pilih Kode Produk --') !!}
						</select> --}}
						<div class="ui action input">
		                    <input type="text" name="diskon_kode_produk[]" class="form-control kopro diskon_kode_produk" onkeypress="return isNumberKey(event)">
		                    <button type="button" class="ui button btn-cek-kode-diskon"><i class="search icon"></i></button>
		                </div>
					</div>
				</td>
				<td>
					<div class="field">
						<input type="text" name="diskon_nama_produk[]" class="diskon_nama_produk" placeholder="Inputkan Nama" readonly="" style="background-color: #edf1f6;">
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui right labeled input">
						    <div class="ui label">Rp.</div>
							<input type="text" name="diskon_cost_price[]" class="diskon_cost_price" placeholder="Inputkan Cost Price" readonly="" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;">
					    </div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui right labeled input">
						    <div class="ui label">Rp.</div>
							<input type="text" name="harga_awal[]" class="harga-awal" placeholder="Inputkan Harga Awal" data-inputmask="'alias' : 'numeric'">
					    </div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui right labeled input diskon_harga_terdiskon">
						    <div class="ui label">Rp.</div>
							<input type="text" name="diskon_harga_terdiskon[]" class="harga-terdiskon" placeholder="Inputkan Harga Terdiskon" data-inputmask="'alias' : 'numeric'" onkeyup="calculateDiskon(this)">
					    </div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui right labeled input diskon_diskon">
						    <div class="ui label">Rp.</div>
							<input type="text" name="diskon_diskon[]" class="diskon" placeholder="Inputkan Diskon" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="">
					    </div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui right labeled input diskon_persen">
							<input type="text" id="diskon_persen" class="diskon_persen" placeholder="Diskon %" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="">
						    <div class="ui label">%</div>
					    </div>
					</div>
				</td>
			</tr>
		</template>
	</div>
</div>
<!-- Diskon -->

@include('layouts.scripts.inputmask')

<script type="text/javascript">
	$(document).ready(function(e) {
		$(document).on('click', '.btn-cek-kode-diskon', function(e){
			var _this = $(this);
			var kode = _this.closest('tr').find('.diskon_kode_produk').val();
			var lsi = $("#lsi_id").val();

			$(".loading").addClass('active');

			$.ajax({
                url: "{{url($pageUrl)}}/on-change-pop-produk-by-kode/"+kode+'/'+lsi,
                type: 'GET',
                dataType: 'json',
            })
            .success(function(response) {
				$(".loading").removeClass('active');
                // console.log(response);
				_this.closest('tr').find('.diskon_nama_produk').val(response.nama);
				_this.closest('tr').find('.diskon_cost_price').val(response.harga);

				var lock_price = response.locked_price;
				if (lock_price == 1) {
					_this.closest('tr').find('.diskon_harga_terdiskon').addClass('disabled');
					_this.closest('tr').find('.diskon_harga_terdiskon > input').val('0');

 					_this.closest('tr').find('.diskon').addClass('disabled');
					_this.closest('tr').find('.diskon').val('0');
				}else{
					_this.closest('tr').find('.diskon_harga_terdiskon').removeClass('disabled');
					_this.closest('tr').find('.diskon').removeClass('disabled');
				}
				$('.btn-submit').removeClass('disabled');
            })
            .error(function(resp) {
				$(".loading").removeClass('active');
				swal(
					'Oops!',
					'Data Tidak Ditemukan',
					'warning'
				).then((result) => {
					_this.closest('tr').find('.diskon_harga_terdiskon').addClass('disabled');
					$('.btn-submit').addClass('disabled');
				})
            });
		});
	});

	add_row = function()
	{
        // get from template
        var tr = $('#template-row').html();

        // append to table
        $('table#logsheet > tbody').append(tr);
        
        reloadMask();
    }

    calculateDiskon = function(elm)
	{
        var price = $(elm).closest('tr').find('.harga-awal').val();
        var validasi = price.replace(',00', "");
		var new_price = validasi.replace(/[^0-9\,-]+/g, "");

        var terdiskon = $(elm).closest('tr').find('.harga-terdiskon').val();
        var validasi = terdiskon.replace(',00', "");
		var new_terdiskon = validasi.replace(/[^0-9\,-]+/g, "");

        if(new_terdiskon == ''){
	        $(elm).closest('tr').find('.diskon').val(0);
        }
        //calculate
        var total = new_price - new_terdiskon;

		if (!isNaN(total)) {
			if(total > 0){
		    	$(elm).closest('tr').find('.diskon').val(total);

		        var diskon = $(elm).closest('tr').find('.diskon').val();
		        var validasi = diskon.replace(',00', "");
				var new_diskon = validasi.replace(/[^0-9\,-]+/g, "");
		    	var persen = (diskon/new_price) * 100;
		    	
		    	$(elm).closest('tr').find('.diskon_persen').val(Math.round(persen)+'%');
			}else{
		        $(elm).closest('tr').find('.diskon').val(0);
			}
        }else{
	        $(elm).closest('tr').find('.diskon').val(0);
        }

    }
</script>