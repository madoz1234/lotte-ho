<!-- Promo Paket -->
<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="ui centered grid promo {{ isset($promosi) ? '' : 'hidden' }}">
	<div class="full wide column">
		<table id="promo" class="ui celled compact red table" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th style="text-align: center;">#</th>
					<th style="text-align: center;">Kode Produk</th>
					<th style="text-align: center;">Nama Produk</th>
					<th style="text-align: center;">Suggested Rounded Selling Price</th>
					<th style="text-align: center;">Harga Jual</th>
					<th style="text-align: center;">Diskon</th>
				</tr>
			</thead>
			<tbody>
				@if(isset($promosi))
					<?php 
						$detail = $promosi->detailPromo->where('status', 0);
					?>
					@foreach($detail as $promo)
						<tr>
							<td><input type="checkbox" name="promo_id[]" class="case" value="{{ $promo->id }}"></td>
							<td>
								<div class="field">
									<div class="ui input">
										<input type="hidden" name="promo_id[]" value="{{ $promo->id }}">
										<input type="text" name="promo_kode_produk[]" placeholder="Inputkan Kode" onkeypress="return isNumberKey(event)" value="{{ $promo->kode_produk }}" readonly="">
									</div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui input">
										<input type="text" name="promo_nama_produk[]" placeholder="Inputkan Nama" value="{{ $promo->nama_produk }}" style="background-color: #edf1f6;" readonly="">
									</div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui right labeled input">
									    <div class="ui label">Rp.</div>
									    <div class="ui input">
											<input type="text" class="selling-price" name="promo_selling_price[]" placeholder="Inputkan Selling Price" value="{{ $promo->selling_price }}" style="background-color: #edf1f6;" readonly="" data-inputmask="'alias' : 'numeric'">
										</div>
								    </div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui right labeled input {{ isset($promo) ? (($promo->produk->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
									    <div class="ui label">Rp.</div>
									    <div class="ui input">
											<input type="text" class="harga-terdiskon" name="promo_harga_terdiskon[]" placeholder="Inputkan Harga Terdiskon" value="{{ $promo->harga_terdiskon }}" data-inputmask="'alias' : 'numeric'" onkeyup="calculate(this)">
										</div>
								    </div>
								</div>
							</td>
							<td>
								<div class="field">
									<div class="ui right labeled input {{ isset($promo) ? (($promo->produk->produksetting->locked_price == 1) ? 'disabled' : '') : '' }}">
									    <div class="ui label">Rp.</div>
									    <div class="ui input">
											<input type="text" class="diskon" name="promo_diskon[]" placeholder="Inputkan Harga Diskon" value="{{ $promo->diskon }}" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="">
										</div>
								    </div>
								</div>
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
			<tfoot>
				<tr>
					<button type="button" style="margin-bottom: 10px" class="hapus ui mini red icon trash button btn-delete {{ isset($promosi) ? '' : 'disabled'}}"><i class="icon trash button"></i></button>
					<button type="button" onclick="add_row_promo()" style="margin-bottom: 10px" class="ui mini blue icon plus button btn-add {{ isset($promosi) ? '' : 'disabled'}}"><i class="icon plus button"></i></button>
				</tr>
				<tr>
					<th colspan="5" class="ui right aligned">Total Diskon</th>
					<th colspan="1" class="ui center aligned">
						<div class="ui right labeled input">
						    <div class="ui label">Rp.</div>
						    <div class="ui input">
								<input type="text" id="promo_total_diskon" name="promo_total_diskon[]" readonly="" value="{{ isset($promo) ? FormatNumber($promo->sum('diskon')) : FormatNumber(0) }}" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;">
							</div>
					    </div>
					</th>
				</tr>
			</tfoot>
		</table>

		<template id="template-row-promo">
			<tr>
				<td><input type="checkbox" class="case"/></td>
				<td>
					<div class="field">
						{{-- <input type="hidden" name="promo_kode_produk[]" placeholder="Inputkan Kode" onkeypress="return isNumberKey(event)">
						<select name="promo_kode_produk[]" class="ui fluid search selection dropdown kopro" style="width: 100%;">
							{!! \Lotte\Models\Master\ProdukLsi::options('produk_kode', 'produk_kode',[], '-- Pilih Kode Produk --') !!}
						</select> --}}
						<div class="ui action input">
		                    <input type="text" name="promo_kode_produk[]" class="form-control kopro promo_kode_produk" onkeypress="return isNumberKey(event)">
		                    <button type="button" class="ui button btn-cek-kode-promo"><i class="search icon"></i></button>
		                </div>
					</div>
				</td>
				<td>
					<div class="field">
						<input type="text" name="promo_nama_produk[]" class="promo_nama_produk" placeholder="Inputkan Nama" readonly="" style="background-color: #edf1f6;">
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui right labeled input">
						    <div class="ui label">Rp.</div>
							<input type="text" name="promo_selling_price[]" class="selling-price" placeholder="Inputkan Selling Price" readonly="" style="background-color: #edf1f6;" data-inputmask="'alias' : 'numeric'">
					    </div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui right labeled input promo_harga_terdiskon">
						    <div class="ui label">Rp.</div>
							<input type="text" name="promo_harga_terdiskon[]" class="harga-terdiskon" placeholder="Inputkan Harga Terdiskon" onkeypress="return isNumberKey(event)" data-inputmask="'alias' : 'numeric'" onkeyup="calculate(this)">
					    </div>
					</div>
				</td>
				<td>
					<div class="field">
						<div class="ui right labeled input promo_diskon">
						    <div class="ui label">Rp.</div>
						    <div class="ui input">
								<input type="text" class="diskon" name="promo_diskon[]" placeholder="Inputkan Harga Diskon" data-inputmask="'alias' : 'numeric'" style="background-color: #edf1f6;" readonly="">
							</div>
					    </div>
					</div>
				</td>
			</tr>
		</template>
	</div>
</div>
<!-- Promo Paket -->

@include('layouts.scripts.inputmask')

<script type="text/javascript">
	$(document).ready(function(e) {
		$(document).on('click', '.btn-cek-kode-promo', function(e){
			var _this = $(this);
			var kode = _this.closest('tr').find('.promo_kode_produk').val();
			var lsi = $("#lsi_id").val();

			$(".loading").addClass('active');

			$.ajax({
                url: "{{url($pageUrl)}}/on-change-pop-produk-by-kode/"+kode+'/'+lsi,
                type: 'GET',
                dataType: 'json',
            })
            .success(function(response) {
				$(".loading").removeClass('active');
                // console.log(response);
				_this.closest('tr').find('.promo_nama_produk').val(response.nama)
				_this.closest('tr').find('.selling-price').val(response.harga)

				var lock_price = response.locked_price;
				if (lock_price == 1) {
					_this.closest('tr').find('.promo_harga_terdiskon').addClass('disabled')
					_this.closest('tr').find('.promo_harga_terdiskon > input').val('0')

 					_this.closest('tr').find('.diskon').addClass('disabled')
					_this.closest('tr').find('.diskon').val('0')
				}else{
					_this.closest('tr').find('.promo_harga_terdiskon').removeClass('disabled')
					_this.closest('tr').find('.diskon').removeClass('disabled')
				}
				$('.btn-submit').removeClass('disabled');
            })
            .error(function(resp) {
				$(".loading").removeClass('active');
				swal(
					'Oops!',
					'Data Tidak Ditemukan',
					'warning'
				).then((result) => {
					_this.closest('tr').find('.promo_harga_terdiskon').addClass('disabled');
					$('.btn-submit').addClass('disabled');
				})
            });
		});
	});

	add_row_promo = function()
	{
        // get from template
        var tr = $('#template-row-promo').html();

        // append to table
        $('table#promo > tbody').append(tr);
        
        // set component
        reloadMask();
    }

    calculate = function(elm)
	{
        var price = $(elm).closest('tr').find('.selling-price').val();
        var validasi = price.replace(',00', "");
		var new_price = validasi.replace(/[^0-9\,-]+/g, "");

        var terdiskon = $(elm).closest('tr').find('.harga-terdiskon').val();
        var validasi = terdiskon.replace(',00', "");
		var new_terdiskon = validasi.replace(/[^0-9\,-]+/g, "");

		 if(new_terdiskon == ''){
	        $(elm).closest('tr').find('.diskon').val(0);
        }

        var total = new_price - new_terdiskon;

		if (!isNaN(total)) {
			if(total > 0){
	        	$(elm).closest('tr').find('.diskon').val(total);
			}else{
		        $(elm).closest('tr').find('.diskon').val(0);
			}
        }else{
	        $(elm).closest('tr').find('.diskon').val(0);
        }

    	var element = document.getElementsByClassName('diskon');
    	var total = 0;
    	var diskon = 0;
    	var nilai = 0;

	    for (var i = 0; i < element.length; i++){
	    	nilai = element[i].value;
	    	if (nilai != undefined) {
	    		diskon =  nilai.replace(/[^0-9\,-]+/g, "");
	    		total += parseFloat(diskon);

	    	}
	    }

	    if (!isNaN(total)) {
	    	$('#promo_total_diskon').val(total+',00');
	    }else{
	    	$('#promo_total_diskon').val('0,00');
	    }
	    
    }
</script>