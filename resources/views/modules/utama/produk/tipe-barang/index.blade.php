@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[nama]" placeholder="Tipe Barang" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_tipe_barang();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection


@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	nama: {
		identifier: 'nama',
		rules: [{
			type   : 'empty',
			prompt : 'Tipe Barang Harus Terisi'
		},{
			type   : 'minLength[2]',
			prompt : 'Isian tipe barang setidaknya harus memiliki {ruleValue} karakter'
		},{
			type   : 'maxLength[30]',
			prompt : 'Isian tipe barang setidaknya harus memiliki {ruleValue} karakter'
		}]
	},
};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_tipe_barang = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-tipe-barang') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append