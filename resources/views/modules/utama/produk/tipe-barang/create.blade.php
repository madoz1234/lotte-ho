<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Tipe Barang</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
			<input style="background-color: #efefef;" name="kode" placeholder="Kode Tipe Barang" type="hidden" value="{{ $code }}" readonly>
		<div class="field">
			<label>Tipe Barang</label>
			<input name="nama" placeholder="Tipe Barang" type="text" value="{{ $nama or "" }}">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>