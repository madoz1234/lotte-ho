<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Data Tipe Barang</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
	    <input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
			<input style="background-color: #efefef;"{{--  name="kode" --}} placeholder="Kode Tipe Barang" type="hidden" value="{{ $record->kode or '' }}" readonly>
		<div class="field">
			<label>Tipe Barang</label>
			<input {{-- name="nama" --}} placeholder="Tipe Barang" type="text" value="{{ $record->nama or '' }}" disabled="">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	{{-- <div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div> --}}
</div>