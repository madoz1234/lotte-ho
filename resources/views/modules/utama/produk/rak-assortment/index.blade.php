@extends('layouts.grid')

@section('filters')
	<div class="field">
		<select name="filter[hanger]" id="" class="ui fluid dropdown">
			<option value="">Assortment Type</option>
			<option value="1">1500</option>
			<option value="2">800</option>
			<option value="3">500</option>
		</select>
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	tinggi: {
		identifier: 'tinggi',
		rules: [{
			type   : 'empty',
			prompt : 'Jumlah Rak Lorong Harus Terisi'
		}]
	},

	panjang: {
		identifier: 'panjang',
		rules: [{
			type   : 'empty',
			prompt : 'Jumlah Rak Dinding Harus Terisi'
		}]
	},

	lebar: {
		identifier: 'lebar',
		rules: [{
			type   : 'empty',
			prompt : 'Jumlah Rak Kasir Harus Terisi'
		}]
	}
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
	};
</script>
@endsection

@section('tableBody')
	<tr>
		<td>1</td>
		<td>1500</td>
		<td>5</td>
		<td>6</td>
		<td>7</td>
		<td>2 Jam yang lalu</td>
		<td>
        	<button type="button" class="ui mini orange icon edit button" data-content="Ubah Data" data-id="1"><i class="edit icon"></i></button>
        	<button type="button" class="ui mini red icon delete button" data-content="Hapus Data" data-id="1"><i class="trash icon"></i></button>
		</td>
	</tr>
@endsection