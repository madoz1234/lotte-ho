<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Rak by Assortment Type</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		
			<div class="field">
				<label>Assortment Type</label>
				<div class="ui fluid search selection dropdown" id="combo">
					<input type="hidden" name="tes">
					<i class="dropdown icon"></i>
					<div class="default text">Pilih Assortment Type</div>
					<div class="menu">
						<div class="item" data-value="1">1500</div>
						<div class="item" data-value="2">800</div>
						<div class="item" data-value="3">500</div>
					</div>
				</div>
			</div>
			<div class="field">
				<label>Jumlah Rak Lorong</label>
				<input name="tinggi" placeholder="Jumlah Rak Lorong" type="number">
			</div>
		
	
			

			<div class="field">
				<label>Jumlah Rak Dinding</label>
				<input name="panjang" placeholder="Jumlah Rak Dinding" type="number">
			</div>

			<div class="field">
				<label>Jumlah Rak Kasir</label>
				<input name="lebar" placeholder="Jumlah Rak Kasir" type="number">
			</div>
	

	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>