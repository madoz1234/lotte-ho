@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[tipe]" placeholder="Tipe Truk" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
  @if ($message = Session::get('success'))
<div class="alert alert-success" role="alert">
{{ Session::get('success') }}
</div>
@endif


@if ($message = Session::get('error'))
<div class="alert alert-danger" role="alert">
{{ Session::get('error') }}
</div>
@endif
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_truk();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.tipe = $("input[name='filter[tipe]']").val();
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	tipe: {
		identifier: 'tipe',
		rules: [{
			type   : 'empty',
			prompt : 'Tipe Truk Harus Terisi'
		}]
	},

	tinggi: {
		identifier: 'tinggi',
		rules: [{
			type   : 'empty',
			prompt : 'Tinggi Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian tinggi seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian panjang seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	panjang: {
		identifier: 'panjang',
		rules: [{
			type   : 'empty',
			prompt : 'Panjang Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian panjang seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian panjang seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	lebar: {
		identifier: 'lebar',
		rules: [{
			type   : 'empty',
			prompt : 'Lebar Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian panjang seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian panjang seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	kapasitas: {
		identifier: 'kapasitas',
		rules: [{
			type   : 'empty',
			prompt : 'Kapasitas Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian panjang seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian panjang seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	volume: {
		identifier: 'volume',
		rules: [{
			type   : 'empty',
			prompt : 'Volume Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian panjang seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian panjang seharusnya berjumlah {ruleValue} karakter'
		}]
	}
};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_truk = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-truk') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var tipe = document.createElement("input");
		            tipe.setAttribute("type", "hidden");
		            tipe.setAttribute("name", 'tipe');
		            tipe.setAttribute("value", $('[name="filter[tipe]"]').val());
		        form.appendChild(tipe);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append