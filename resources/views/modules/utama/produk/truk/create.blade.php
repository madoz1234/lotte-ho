<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Truk</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="field">
			<label>Tipe Truk</label>
			<input name="tipe" placeholder="Tipe Truk" type="text" value="{{ $tipe or "" }}">
		</div>
		
		<div class="three  fields">
			<div class="field">
				<label>Tinggi</label>
				<div class="ui right labeled input">
					<input class="length" name="tinggi" placeholder="Tinggi" type="numeric" value="{{ $tinggi or "" }}">
					<div class="ui basic label">cm</div>
				</div>
			</div>

			<div class="field">
				<label>Panjang</label>
				<div class="ui right labeled input">
					<input class="length" name="panjang" placeholder="Panjang" type="numeric" value="{{ $panjang or "" }}">
					<div class="ui basic label">cm</div>
				</div>
			</div>

			<div class="field">
				<label>Lebar</label>
				<div class="ui right labeled input">
					<input class="length" name="lebar" placeholder="Lebar" type="numeric" value="{{ $lebar or "" }}">
					<div class="ui basic label">cm</div>
				</div>
			</div>
		</div>

		<div class="two fields">
			<div class="field">
				<label>Kapasitas</label>
				<div class="ui right labeled input">
					<input class="length" name="kapasitas" placeholder="Kapasitas" type="numeric" value="{{ $kapasitas or "" }}">
					<div class="ui basic label">ton</div>
				</div>
			</div>

			<div class="field">
				<label>Volume</label>
				<div class="ui right labeled input">
					<input class="length" name="volume" placeholder="Volume" type="numeric" value="{{ $volume or "" }}">
					<div class="ui basic label">liter</div>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>


	<script type="text/javascript">
	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });
</script>