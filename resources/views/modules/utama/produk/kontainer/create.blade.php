<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Kontainer</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="field">
			<label>Tipe Kontainer</label>
			<input name="nama" placeholder="Tipe Kontainer" type="text" value="{{ $nama or "" }}">
		</div>
		
		<div class="two fields">
			<div class="field">
				<label>Tinggi Luar</label>
				<div class="ui right labeled input">
					<input class="length"  name="tinggi_luar" placeholder="Tinggi Luar" type="text" value="{{ $tinggi_luar or "" }}">
					<div class="ui basic label">cm</div>
				</div>
			</div>

				<div class="field">
					<label>Tinggi Dalam</label>
					<div class="ui right labeled input">
						<input class="length"  name="tinggi_dalam" placeholder="Tinggi Dalam" type="text" value="{{ $tinggi_dalam or "" }}">
						<div class="ui basic label">cm</div>
					</div>
				</div>
			</div>

			<div class="two fields">
				<div class="field">
					<label>Panjang Atas</label>
					<div class="ui right labeled input">
						<input class="length"  name="panjang_atas" placeholder="Panjang Atas" type="text" value="{{ $panjang_atas or "" }}">
						<div class="ui basic label">cm</div>
					</div>
				</div>

				<div class="field">
					<label>Panjang Bawah</label>
					<div class="ui right labeled input">
						<input class="length"  name="panjang_bawah" placeholder="Panjang Bawah" type="text" value="{{ $panjang_bawah or "" }}">
						<div class="ui basic label">cm</div>
					</div>
				</div>
			</div>

			<div class="two fields">
				<div class="field">
					<label>Lebar Atas</label>
					<div class="ui right labeled input">
						<input class="length"  name="lebar_atas" placeholder="Lebar Atas" type="text" value="{{ $lebar_atas or "" }}">
						<div class="ui basic label">cm</div>
					</div>
				</div>

				<div class="field">
					<label>Lebar Bawah</label>
					<div class="ui right labeled input">
						<input class="length"  name="lebar_bawah" placeholder="Lebar Bawah" type="text" value="{{ $lebar_bawah or "" }}">
						<div class="ui basic label">cm</div>
					</div>
				</div>
			</div>

			<div class="two fields">
				<div class="field">
					<label>Volume</label>
					<div class="ui right labeled input">
						<input class="length"  name="volume" placeholder="Volume" type="text" value="{{ $volume or "" }}">
						<div class="ui basic label">liter</div>
					</div>
				</div>

				<div class="field">
					<label>Tipe Box</label>
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="tipe_box" tabindex="0" class="hidden" value="1" checked="">
								<label>Innerbox</label>
							</div>
						</div>

						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="tipe_box" tabindex="0" class="hidden" value="0">
								<label>Outbox</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		<div class="ui error message"></div>
	</form>
	</div>
	<div class="actions">
		<div class="ui negative button" style="background: grey;">
			Batal
		</div>
		<div class="ui positive right labeled icon save button">
			Simpan
			<i class="checkmark icon"></i>
		</div>
	</div>


	<script type="text/javascript">
	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });
</script>