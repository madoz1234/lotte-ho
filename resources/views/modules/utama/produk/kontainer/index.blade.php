@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[nama]" placeholder="Tipe Kontainer" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_kontainer();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	nama: {
		identifier: 'nama',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Tipe Kontainer Harus Terisi'
		}]
	},

	tinggi_luar: {
		identifier: 'tinggi_luar',
		rules: [{
			type   : 'empty',
			prompt : 'Pilihan Tinggi Luar Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian tinggi luar seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian volume seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	tinggi_dalam: {
		identifier: 'tinggi_dalam',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Tinggi Dalam Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian tinggi dalam seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian volume seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	panjang_atas: {
		identifier: 'panjang_atas',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Panjang Atas Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian panjang atas seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian volume seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	panjang_bawah: {
		identifier: 'panjang_bawah',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Panjang Bawah Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian panjang bawah seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian volume seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	lebar_atas: {
		identifier: 'lebar_atas',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Lebar Atas Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian lebar atas seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian volume seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	lebar_bawah: {
		identifier: 'lebar_bawah',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Lebar Bawah Harus Terisi'
		},{
			type   : 'minLength[1]',
			Prompt : 'Isian lebar bawah seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian volume seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	volume: {
		identifier: 'volume',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Volume Harus Terisi'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian volume seharusnya berjumlah {ruleValue} karakter'
		},{
			type   : 'maxLength[10]',
			prompt : 'Isian volume seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	tipe_box: {
		identifier: 'tipe_box',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Tipe Box Harus Terisi'
		}]
	}
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_kontainer = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-kontainer') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append