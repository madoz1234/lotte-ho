

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Produk Assortment Type</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$assortment->id) }}" method="POST">
	{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $assortment->id }}">
		<div class="ui error message"></div>
		<div class="three fields">
			<div class="field">
				<label>Assortment Type :</label>
				<input name="" placeholder="Assortment Type" style="background-color: #edf1f6;" type="text" value="{{ $assortment->nama or "" }}" disabled="">
			</div>
		</div>
	</form>
	<table id="dttable" class="ui celled compact red table" width="100%" cellspacing="0">
		<thead>
		    <tr>
		    	<th class="ui center aligned">#</th>
		    	<th class="ui center aligned">Kode Produk</th>
		    	<th class="ui center aligned">Nama Produk</th>
		    	<th class="ui center aligned">Aksi</th>
		    </tr>
		</thead>
		<tbody>
			@foreach ($produk as $prod)
				<tr>
                	<td>0</td>
                	<td>{{ $prod['kode'] }}</td>
                	<td>{{ $prod['nama'] }}</td>
                	<td class="ui center aligned"><input name="" type="checkbox" value="'.$record->id.'" onchange="updateAssortmentProduk(this, '{{ $assortment->id }}', '{{ $prod->kode }}')" {{ isset($ceklis[$prod->kode]) ? 'checked' : '' }}></td>
                </tr>
            @endforeach
		</tbody>
	</table>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$('button[data-content]').popup({
			hoverable: true,
			position : 'top center',
			delay: {
				show: 300,
				hide: 800
			}
		});

		dt = $('#dttable').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
			responsive: true,
			autoWidth: false,
			processing: true,
			@if(!$mockup)
			serverSide: true,
			@endif
			lengthChange: false,
			pageLength: 10,
			filter: false,
			sorting: [],
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			@if(!$mockup)
			ajax:  {
				url: "{{ url($pageUrl) }}/detail",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
					@yield('js-filters')
				}
			}, 
			@endif
			columns: {!! json_encode($tableStruct) !!},
			drawCallback: function() {
				var api = this.api();

				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML) + i+1;
				} );

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});

				//Calender
				// $('.ui.calendar').calendar({
				// 	type: 'date'
				// });

				//Popup							
				$('.checked.checkbox')
				  .popup({
				    popup : $('.custom.popup'),
				    on    : 'click'
				  })
				;
			}
		});

		$('.filter.button').on('click', function(e) {
			dt.draw();
			e.preventDefault();
		});
		$('.reset.button').on('click', function(e) {
			$('.dropdown .delete').trigger('click');
			$('.dropdown').dropdown('clear');
			setTimeout(function(){
				dt.draw();
			}, 100);
		});
	});
</script>