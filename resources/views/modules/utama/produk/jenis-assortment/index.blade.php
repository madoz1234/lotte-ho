@extends('layouts.grid')

@section('filters')
<div class="field">
	<input type="text" name="filter[nama]" placeholder="Assortment Type">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_assortment_type();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection


@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Isian assortment tidak boleh kosong'
			},{
				type   : 'minLength[2]',
				prompt : 'Isian assortment harus berjumlah {ruleValue} karakter'
			},{
				type   : 'maxLength[10]',
				prompt : 'Isian assortment harus berjumlah {ruleValue} karakter'
			}]
		},
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {

			// $('#dttable').DataTable();

			// $('#dttable').DataTable({
		 //            "processing": true,
		 //            "serverSide": true,
		 //            "paging":   true,
		 //            "ordering": true,
		 //            "info":     false,
		 //            "searching": false,
		 //            "lengthChange": false,
		 //            "ajax":{
			// 	                "url":"{{ url($pageUrl.'detail') }}",
			// 	                "type": "POST",
			// 	                data: function (d) {
			// 	                	d._token = "{{ csrf_token() }}";
			// 	                },
			// 	                "fnCreatedRow": function (row, data, index) {
			// 	                    $('td', row).eq(0).html(index + 0);
			// 	                }
			// 	            },
		 //            "columns": [
			// 		            { "data": "num", 'width': '32px', 'className': "center aligned", "bSortable": false },
			// 		            { "data": "divisi_kode", 'className': "left aligned", "bSortable": false },
			// 		            { "data": "kode", 'className': "left aligned", "bSortable": false },
			// 		            { "data": "nama", 'className': "left aligned", "bSortable": false },
			// 		            { "data": "created_at", 'className': "center aligned", "bSortable": false }
			// 		           ],
		 //            });


			export_assortment_type = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-assortment-type') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}

			updateAssortmentProduk = function(elm, type_id, produk_kode){
				var ceklis = $(elm).prop('checked')

				$.ajax({
					url: '{{ url($pageUrl.'save-assortment-produk') }}',
					type: 'POST',
					dataType: 'json',
					data: {
						ceklis: ceklis,
						type_id: type_id,
						produk_kode: produk_kode,
					},
				})
				.done(function(response) {
					console.log(response);
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
				
			}
		});
	</script>
@append