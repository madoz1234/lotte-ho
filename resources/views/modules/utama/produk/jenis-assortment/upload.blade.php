<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Upload Data Assortment Type</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="field">
			<input type="text" name="nama" placeholder="Inputkan Assortment">
		</div>

	</form>

	<!-- Produk -->

	{{-- <tr align="center"><td colspan="6"><i>Jumlah Produk :</i></td></tr> --}}
	<hr>
	<table class="ui compact celled table">
		<thead class="full-width">
			<tr>
				<th>No</th>
				<th>Kode Produk</th>
				<th>Nama Produk</th>
				<th>Division</th>
				<th>Category 1</th>
				<th>Aksi</th>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th></th>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</tbody>
	</table>
<!-- Produk -->
</div>
<div class="actions">
<div class="ui green button">
		<i class="download icon"></i>
		Template
	</div>
	<div class="ui blue button">
		<i class="upload icon"></i>
		Upload
	</div>
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>