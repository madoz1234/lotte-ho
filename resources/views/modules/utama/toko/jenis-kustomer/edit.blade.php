<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Jenis Kustomer</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
	    <input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
			<input style="background-color: #efefef;" name="kode" placeholder="Kode Jenis Kustomer" type="hidden" value="{{ $record->kode or '' }}" readonly>
		<div class="field">
			<label>Jenis Kustomer</label>
			<input name="jenis" placeholder="Jenis Kustomer" type="text" value="{{ $record->jenis or '' }}">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>