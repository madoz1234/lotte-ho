
@extends('layouts.grid')

@section('filters')
{{-- <div class="field">
	<input type="text" name="filter[kode]" placeholder="Kode">
</div> --}}
<div class="field">
	<input type="text" name="filter[jenis]" placeholder="Nama Jenis">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
    {{-- d.kode = $("input[name='filter[kode]']").val(); --}}
    d.jenis = $("input[name='filter[jenis]']").val();
@endsection
@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_jenis_kustomer();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		kode: {
			identifier: 'kode',
			rules: [{
				type   : 'empty',
				prompt : 'Isian kode tidak boleh kosong'
			}]
		},
		jenis: {
			identifier: 'jenis',
			rules: [{
				type   : 'empty',
				prompt : 'Isian jenis kustomer tidak boleh kosong'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama jenis setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Nama jenis setidaknya harus memiliki {ruleValue} karakter'
			}]
		}
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_jenis_kustomer = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-jenis-kustomer') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var kode = document.createElement("input");
		            kode.setAttribute("type", "hidden");
		            kode.setAttribute("name", 'kode');
		            kode.setAttribute("value", $('[name="filter[kode]"]').val());
		        form.appendChild(kode);

		        var jenis = document.createElement("input");
		            jenis.setAttribute("type", "hidden");
		            jenis.setAttribute("name", 'jenis');
		            jenis.setAttribute("value", $('[name="filter[jenis]"]').val());
		        form.appendChild(jenis);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append