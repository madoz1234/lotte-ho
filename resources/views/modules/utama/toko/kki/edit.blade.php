@include('layouts.scripts.inputmask')

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail KKI</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui error message"></div>
		{{-- <div class="two fields">
			<div class="field">
				<label>TMUK :</label>
				<input name="" placeholder="Inputkan Tanggal PYR" type="text" value="{{ $record->tmuk->nama or "" }}">
			</div>
			<div class="field">
				<label>LSI :</label>
				<input name="" placeholder="Inputkan Vendor Non Lotte" type="text" value="{{ $record->lsi->nama or "" }}">
			</div>
			<div class="field">
				<label>Tanggal Submit KKI:</label>
				<input name="" placeholder="Inputkan Saldo Escrow" type="text" value="{{ $record->tanggal_submit or "" }}">
			</div>
		</div> --}}

		<div class="field">		
			<div class="fields">
				<div class="seven wide field">
					<label>TMUK</label>
					{{-- <input type="text" style="background-color: #edf1f6;" name="tmuk_id" placeholder="Nama LSI" value="{{ $record->tmuk->nama or "" }}" readonly=""> --}}

					<select style="background-color: #edf1f6;" name="tmuk_id" class="ui fluid search selection dropdown" readonly="" {{-- disabled="" --}}>
			    	    {!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->tmuk_id) ? $record->tmuk_id : '',], '-- Pilih Akun Escrow --') !!}
				    </select>
				</div>
				<div class="seven wide field">
					<label>Nama LSI</label>
					<input type="text" style="background-color: #edf1f6;" name="lsi_id" placeholder="Nama LSI" value="{{ $record->tmuk->lsi->nama or "" }}" readonly="" {{-- disabled="" --}}>
				</div>
				<div class="seven wide field">
					<label>Tanggal Submit KKI</label>		
					<div class="ui calendar">
						<div class="ui input left icon">
							<i class="calendar icon date"></i>
							<input name="tanggal_submit" type="text" placeholder="Inputkan Tanggal Submit KKI" value="{{ $record->tanggal_submit or "" }}" readonly="" {{-- disabled="" --}}>
						</div>
					</div>
				</div>

				<div class="seven wide field">
					<label>Nomor</label>
					<input style="background-color: #edf1f6;" name="nomor" placeholder="Nomor" type="text" value="{{ $record->nomor or '' }}" readonly="" {{-- disabled="" --}}>
				</div>
				<div class="seven wide field">
					<div class="field">
						<label>Status</label>
						<div class="inline fields">
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="status" tabindex="0" class="" value="1" {{ ($record->status == 1 ) ? 'checked' : '' }}
									{{ ($record->status == 0 ) ? 'disabled' : '' }}>
									<label>Berjalan</label>
								</div>
							</div>

							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="status" tabindex="0" class="" value="0" {{ ($record->status == 0 ) ? 'checked' : '' }}>
									<label>Selesai</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="scrolling content">
			<div class="ui equal width grid">
				<div class="column">
					<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th class="ui center aligned">I.</th>
								<th class="ui left aligned">Perkiraan Investasi</th>
								<th class="ui center aligned">Nilai (Rp.)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="center aligned" width="10%">1</td>
								<td class="left aligned" width="70%">Startup & Biaya Promosi Opening</td>
								<td class="right aligned" width="30%"><input maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_satu" name="i_satu" onkeyup="sum_a(this)" value="{{ $record->i_satu or "" }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">2</td>
								<td class="left aligned" width="70%">Perijinan Usaha toko</td>
								<td class="right aligned" width="30%"><input maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_dua" name="i_dua" onkeyup="sum_a(this)" value="{{ $record->i_dua or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">3</td>
								<td class="left aligned" width="70%">a. Pekerjaan Renovasi Sipil</td>
								<td class="right aligned" width="30%"><input maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_tiga_a" name="i_tiga_a" onkeyup="sum_a(this)" value="{{ $record->i_tiga_a or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">b. Pekerjaan Kusen Aluminium + Kaca Cermin</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_tiga_b" name="i_tiga_b" onkeyup="sum_a(this)" value="{{ $record->i_tiga_b or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">c. Pekerjaan Folding Gate + Polycarbonate + Teralis</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_tiga_c" name="i_tiga_c" onkeyup="sum_a(this)" value="{{ $record->i_tiga_c or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">d. Pekerjaan Instalasi Listrik (lampu, regrouping)</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_tiga_d" name="i_tiga_d" onkeyup="sum_a(this)" value="{{ $record->i_tiga_d or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">e. Instalasi Pemasangan AC</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_tiga_e" name="i_tiga_e" onkeyup="sum_a(this)" value="{{ $record->i_tiga_e or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">f. Pekerjaan Signage (Lisping Papan Nama Toko) Repainting</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_tiga_f" name="i_tiga_f" onkeyup="sum_a(this)" value="{{ $record->i_tiga_f or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">4</td>
								<td class="left aligned" width="70%">a. Peralatan AC Spil</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_empat_a" name="i_empat_a" onkeyup="sum_a(this)" value="{{ $record->i_empat_a or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">b. Peralatan Non Elektronik (Rak, Furniture, Acrylic, dll)</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_empat_b" name="i_empat_b" onkeyup="sum_a(this)" value="{{ $record->i_empat_b or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">c. Peralatan Elektronik (Komputer, Cooler & Timbangan)</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_empat_c" name="i_empat_c" onkeyup="sum_a(this)" value="{{ $record->i_empat_c or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">5</td>
								<td class="left aligned" width="70%">a. Stock Awal</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_lima_a" name="i_lima_a" onkeyup="sum_a(this)" value="{{ $record->i_lima_a or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">b. Modal Kerja 1 bulan (ditransfer ke rek Ops TMUK)</td>
								<td class="right aligned" width="30%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" name="i_lima_b" id="i_lima_b" onkeyup="sum_a(this)" value="{{ $record->i_lima_b or '' }}">
								</td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">6</td>
								<td class="left aligned" width="70%">Lain-lain</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_bila_ada_biaya_tambahan" name="i_bila_ada_biaya_tambahan" onkeyup="sum_a(this)" value="{{ $record->i_bila_ada_biaya_tambahan or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<th class="center aligned" width="70%">Sub Total Investasi (Sebelum PPN)</th>
								<td class="right aligned" width="30%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" name="i_sub_total" id='i_sub_total' onkeyup="sum_a(this)" value="{{ $record->i_sub_total or '' }}"  style="background-color: #edf1f6;" readonly="">
								</td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">Sewa Ruko selama 5 tahun</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_sewa_ruko" name="i_sewa_ruko" onkeyup="sum_c(this)" value="{{ $record->i_sewa_ruko or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<th class="center aligned" width="70%">Total Investasi (Sebelum PPN)</th>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" name="i_total_inves1_1" id="i_total_inves1_1" onkeyup="sum_c(this)" value="{{ $record->i_total_inves1_1 or '' }}" style="background-color: #edf1f6;" readonly=""></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<th class="center aligned" width="70%">PPN</th>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_ppn" name="i_ppn" onkeyup="sum_e(this)" value="{{ $record->i_ppn or '' }}" {{-- style="background-color: #edf1f6;" readonly="" --}}></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<th class="center aligned" width="70%">Total Investasi (Sesudah PPN)</th>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_total_inves1_2" name="i_total_inves1_2" onkeyup="sum_e(this)" value="{{ $record->i_total_inves1_2 or '' }}" style="background-color: #edf1f6;" readonly=""></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="70%">Perkiraan stok ideal</td>
								<td class="right aligned" width="30%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="i_perkiraan_stok_ideal" name="i_perkiraan_stok_ideal" value="{{ $record->i_perkiraan_stok_ideal or '' }}"></td>
							</tr>
						</tbody>
					</table>
					<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th class="ui center aligned">II.</th>
								<th class="ui left aligned">Perkiraan Penjualan per bulan</th>
								<th class="ui center aligned">Terburuk (BEP)</th>
								<th class="ui center aligned">Normal (Layak)</th>
								<th class="ui center aligned">Terbaik (Aman)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="center aligned" width="10%">1</td>
								<td class="left aligned" width="30%">Struk Per Day (STD)</td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="ii_struk_terburuk" onkeyup="sum_o(this)" name="ii_struk_terburuk" value="{{ $record->ii_struk_terburuk or '' }}"></td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="ii_struk_normal" onkeyup="sum_p(this)" name="ii_struk_normal" value="{{ $record->ii_struk_normal or '' }}"></td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="ii_struk_terbaik" onkeyup="sum_q(this)" name="ii_struk_terbaik" value="{{ $record->ii_struk_terbaik or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">2</td>
								<td class="left aligned" width="30%">Average Purchase per Customer (APC)</td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="ii_average_terburuk" onkeyup="sum_o(this)" name="ii_average_terburuk" value="{{ $record->ii_average_terburuk or '' }}"></td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="ii_average_normal" onkeyup="sum_p(this)" name="ii_average_normal" value="{{ $record->ii_average_normal or '' }}"></td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="ii_average_terbaik" onkeyup="sum_q(this)" name="ii_average_terbaik" value="{{ $record->ii_average_terbaik or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">3</td>
								<td class="left aligned" width="30%">Sales Per Day (SPD)</td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="ii_sales_terburuk" onkeyup="sum_o(this)" name="ii_sales_terburuk" value="{{ $record->ii_sales_terburuk or '' }}" style="background-color: #edf1f6;" readonly=""></td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="ii_sales_normal" onkeyup="sum_p(this)" name="ii_sales_normal" value="{{ $record->ii_sales_normal or '' }}" style="background-color: #edf1f6;" readonly=""></td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="ii_sales_terbaik" onkeyup="sum_q(this)" name="ii_sales_terbaik" value="{{ $record->ii_sales_terbaik or '' }}" style="background-color: #edf1f6;" readonly=""></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">4</td>
								<td class="left aligned" width="30%">Penjualan per Bulan (SPD x 30 hari)</td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" name="ii_penjualan_terburuk" value="{{ $record->ii_penjualan_terburuk or '' }}"></td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" name="ii_penjualan_normal" value="{{ $record->ii_penjualan_normal or '' }}"></td>
								<td class="right aligned" width="20%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" name="ii_penjualan_terbaik" value="{{ $record->ii_penjualan_terbaik or '' }}"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="column">
					<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th class="ui center aligned">III.</th>
								<th class="ui left aligned">Perkiraan Pendapatan per bulan</th>
								<th class="ui center aligned">Terburuk (BEP)</th>
								<th class="ui center aligned">Normal (Layak)</th>
								<th class="ui center aligned">Terbaik (Aman)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="center aligned" width="10%">1</td>
								<td class="left aligned" width="30%">Laba Kotor Penjualan </td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_labakotor_estimasi1" name="iii_labakotor_estimasi1" onkeyup="sum_f(this)" value="{{ $record->iii_labakotor_estimasi1 or '' }}">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_labakotor_estimasi2" name="iii_labakotor_estimasi2" onkeyup="sum_g(this)" value="{{ $record->iii_labakotor_estimasi2 or '' }}">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_labakotor_estimasi3" name="iii_labakotor_estimasi3" onkeyup="sum_h(this)" value="{{ $record->iii_labakotor_estimasi3 or '' }}">
								</td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">2</td>
								<td class="left aligned" width="30%">Pendapatan Lain-lain</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_pendapatanlain2_estimasi1" name="iii_pendapatanlain2_estimasi1" onkeyup="sum_f(this)" value="{{ $record->iii_pendapatanlain2_estimasi1 or '' }}">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_pendapatanlain2_estimasi2" name="iii_pendapatanlain2_estimasi2" onkeyup="sum_g(this)" value="{{ $record->iii_pendapatanlain2_estimasi2 or '' }}">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_pendapatanlain2_estimasi3" name="iii_pendapatanlain2_estimasi3" onkeyup="sum_h(this)" value="{{ $record->iii_pendapatanlain2_estimasi3 or '' }}">
								</td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">3</td>
								<td class="left aligned" width="30%">Pendapatan Sewa Tempat Pemanjangan</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_pendapatansewa_estimasi1" name="iii_pendapatansewa_estimasi1" onkeyup="sum_f(this)" value="{{ $record->iii_pendapatansewa_estimasi1 or '' }}">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_pendapatansewa_estimasi2" name="iii_pendapatansewa_estimasi2" onkeyup="sum_g(this)" value="{{ $record->iii_pendapatansewa_estimasi2 or '' }}">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_pendapatansewa_estimasi3" name="iii_pendapatansewa_estimasi3" onkeyup="sum_h(this)" value="{{ $record->iii_pendapatansewa_estimasi3 or '' }}">
								</td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">4</td>
								<td class="left aligned" width="30%">Pendapatan Lain-lain</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_tambahan_1" name="iii_tambahan_1" onkeyup="sum_f(this)" value="{{ $record->iii_tambahan_1 or '' }}">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_tambahan_2" name="iii_tambahan_2" onkeyup="sum_g(this)" value="{{ $record->iii_tambahan_2 or '' }}">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_tambahan_3" name="iii_tambahan_3" onkeyup="sum_h(this)" value="{{ $record->iii_tambahan_3 or '' }}">
								</td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="canter aligned" width="30%" style="font-weight: bold;">Total Perkiraan Pendapatan / bulan</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_total_estimasi1" name="iii_total_estimasi1" onkeyup="sum_f(this)" value="{{ $record->iii_total_estimasi1 or '' }}" style="background-color: #edf1f6;" readonly="">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_total_estimasi2" name="iii_total_estimasi2" onkeyup="sum_g(this)" value="{{ $record->iii_total_estimasi2 or '' }}" style="background-color: #edf1f6;" readonly="">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iii_total_estimasi3" name="iii_total_estimasi3" onkeyup="sum_h(this)" value="{{ $record->iii_total_estimasi3 or '' }}" style="background-color: #edf1f6;" readonly="">
								</td>
							</tr>
						</tbody>
					</table>
					<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th class="ui center aligned">IV.</th>
								<th class="ui left aligned">Perkiraan Pengeluaran per bulan</th>
								<th class="ui center aligned">&nbsp</th>
								<th class="ui center aligned">&nbsp</th>
								<th class="ui center aligned">&nbsp</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="center aligned" width="10%">1</td>
								<td class="left aligned" width="30%">Biaya Type A -a. Biaya Air</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_a_estimasi1" name="iv_satu_a_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_satu_a_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_a_estimasi2" name="iv_satu_a_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_satu_a_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_a_estimasi3" name="iv_satu_a_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_satu_a_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-b. Biaya Listrik</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_b_estimasi1" name="iv_satu_b_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_satu_b_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_b_estimasi2" name="iv_satu_b_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_satu_b_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_b_estimasi3" name="iv_satu_b_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_satu_b_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-c. Biaya Telp & Internet</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_c_estimasi1" name="iv_satu_c_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_satu_c_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_c_estimasi2" name="iv_satu_c_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_satu_c_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_c_estimasi3" name="iv_satu_c_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_satu_c_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-d. Biaya Keamanan</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_d_estimasi1" name="iv_satu_d_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_satu_d_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_d_estimasi2" name="iv_satu_d_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_satu_d_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_d_estimasi3" name="iv_satu_d_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_satu_d_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-e. Biaya Lainnya (Sumbangan, dll jika ada)</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_e_estimasi1" name="iv_satu_e_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_satu_e_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_e_estimasi2" name="iv_satu_e_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_satu_e_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_e_estimasi3" name="iv_satu_e_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_satu_e_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-f. Plastik Pembungkus (struk sales X days)</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_f_estimasi1" name="iv_satu_f_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_satu_f_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_f_estimasi2" name="iv_satu_f_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_satu_f_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_f_estimasi3" name="iv_satu_f_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_satu_f_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-g. Barang Rusak</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_g_estimasi1" name="iv_satu_g_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_satu_g_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_g_estimasi2" name="iv_satu_g_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_satu_g_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_g_estimasi3" name="iv_satu_g_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_satu_g_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-h. Barang Hilang</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_h_estimasi1" name="iv_satu_h_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_satu_h_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_h_estimasi2" name="iv_satu_h_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_satu_h_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_h_estimasi3" name="iv_satu_h_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_satu_h_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-i. Biaya Distribusi (Administrasi)</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_i_estimasi1" name="iv_satu_i_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_satu_i_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_i_estimasi2" name="iv_satu_i_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_satu_i_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_satu_i_estimasi3" name="iv_satu_i_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_satu_i_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">Bila ada Biaya Tambahan</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_satu" name="iv_tambahan_satu" onkeyup="sum_i(this)" value="{{ $record->iv_tambahan_satu or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_satu_b" name="iv_tambahan_satu_b" onkeyup="sum_j(this)" value="{{ $record->iv_tambahan_satu_b or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_satu_c" name="iv_tambahan_satu_c" onkeyup="sum_k(this)" value="{{ $record->iv_tambahan_satu_c or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">2</td>
								<td class="left aligned" width="30%">Biaya Type B :a. Karyawan Toko</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_dua_estimasi1" name="iv_dua_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_dua_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_dua_estimasi2" name="iv_dua_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_dua_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_dua_estimasi3" name="iv_dua_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_dua_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">Bila ada Biaya Tambahan</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_dua" name="iv_tambahan_dua" onkeyup="sum_i(this)" value="{{ $record->iv_tambahan_dua or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_dua_b" name="iv_tambahan_dua_b" onkeyup="sum_j(this)" value="{{ $record->iv_tambahan_dua_b or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_dua_c" name="iv_tambahan_dua_c" onkeyup="sum_k(this)" value="{{ $record->iv_tambahan_dua_c or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">3</td>
								<td class="left aligned" width="30%">Biaya Type C -a. Premi lainnya</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tiga_a_estimasi1" name="iv_tiga_a_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_tiga_a_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tiga_a_estimasi2" name="iv_tiga_a_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_tiga_a_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tiga_a_estimasi3" name="iv_tiga_a_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_tiga_a_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-b. PPh Final Sewa Tempat Pemajangan (10% Stp)</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tiga_b_estimasi1" name="iv_tiga_b_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_tiga_b_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tiga_b_estimasi2" name="iv_tiga_b_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_tiga_b_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tiga_b_estimasi3" name="iv_tiga_b_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_tiga_b_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">Bila ada Biaya Tambahan</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_tiga" name="iv_tambahan_tiga" onkeyup="sum_i(this)" value="{{ $record->iv_tambahan_tiga or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_tiga_b" name="iv_tambahan_tiga_b" onkeyup="sum_j(this)" value="{{ $record->iv_tambahan_tiga_b or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_tiga_c" name="iv_tambahan_tiga_c" onkeyup="sum_k(this)" value="{{ $record->iv_tambahan_tiga_c or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">4</td>
								<td class="left aligned" width="30%">Biaya Type D -a. Amortisasi Biaya Lainnya</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_a_estimasi1" name="iv_empat_a_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_empat_a_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_a_estimasi2" name="iv_empat_a_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_empat_a_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_a_estimasi3" name="iv_empat_a_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_empat_a_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-b. Amortisasi Perijinan, Renovasi, dll</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_b_estimasi1" name="iv_empat_b_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_empat_b_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_b_estimasi2" name="iv_empat_b_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_empat_b_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_b_estimasi3" name="iv_empat_b_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_empat_b_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-c. Depresiasi Investasi Peralatan, dll</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_c_estimasi1" name="iv_empat_c_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_empat_c_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_c_estimasi2" name="iv_empat_c_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_empat_c_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_c_estimasi3" name="iv_empat_c_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_empat_c_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" width="30%">-d. Amortisasi Sewa Bangunan</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_d_estimasi1" name="iv_empat_d_estimasi1" onkeyup="sum_i(this)" value="{{ $record->iv_empat_d_estimasi1 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_d_estimasi2" name="iv_empat_d_estimasi2" onkeyup="sum_j(this)" value="{{ $record->iv_empat_d_estimasi2 or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_empat_d_estimasi3" name="iv_empat_d_estimasi3" onkeyup="sum_k(this)" value="{{ $record->iv_empat_d_estimasi3 or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%">5</td>
								<td class="left aligned" width="30%">Biaya Lain-lain</td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_empat" name="iv_tambahan_empat" onkeyup="sum_i(this)" value="{{ $record->iv_tambahan_empat or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_empat_b" name="iv_tambahan_empat_b" onkeyup="sum_j(this)" value="{{ $record->iv_tambahan_empat_b or '' }}"></td>
								<td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_tambahan_empat_c" name="iv_tambahan_empat_c" onkeyup="sum_k(this)" value="{{ $record->iv_tambahan_empat_c or '' }}"></td>
							</tr>
							<tr>
								<td class="center aligned" width="10%"></td>
								<td class="left aligned" style="font-weight: bold;" width="30%">Total Pengeluaran / bulan</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_total_estimasi1" name="iv_total_estimasi1" onkeyup="sum_i(this)" style="background-color: #edf1f6;" value="{{ $record->iv_total_estimasi1 or '' }}" readonly="">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_total_estimasi2" name="iv_total_estimasi2" onkeyup="sum_j(this)" style="background-color: #edf1f6;" value="{{ $record->iv_total_estimasi2 or '' }}" readonly="">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="iv_total_estimasi3" name="iv_total_estimasi3" onkeyup="sum_k(this)" style="background-color: #edf1f6;" value="{{ $record->iv_total_estimasi3 or '' }}" readonly="">
								</td>
							</tr>
						</tbody>
					</table>
					<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th class="ui center aligned">V.</th>
								<th colspan="4" class="ui left aligned">Perkiraan Rata-rata Laba /(Rugi) perbulan selama 5 tahun (sebelum pajak penghasilan)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="center aligned" width="10%">1</td>
								<td class="left aligned" width="30%">Laba Bersih</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="v_satu_estimasi1" name="v_satu_estimasi1" onkeyup="sum_l(this)" style="background-color: #edf1f6;" value="{{ $record->v_satu_estimasi1 or '' }}" readonly="">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="v_satu_estimasi2" name="v_satu_estimasi2" onkeyup="sum_m(this)" style="background-color: #edf1f6;" value="{{ $record->v_satu_estimasi2 or '' }}" readonly="">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="v_satu_estimasi3" name="v_satu_estimasi3" onkeyup="sum_n(this)" style="background-color: #edf1f6;" value="{{ $record->v_satu_estimasi3 or '' }}" readonly="">
								</td>
							</tr> 
							<tr> 
								<td class="center aligned" width="10%">2</td>
								<td class="left aligned" width="30%">Laba Cash</td>
								{{-- <td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numeric'" name="v_dua_estimasi1" value="{{ $record->v_dua_estimasi1 or '' }}"></td> --}}
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="v_dua_estimasi1" name="v_dua_estimasi1" onkeyup="sum_j(this)" style="background-color: #edf1f6;" value="{{ $record->v_dua_estimasi1 or '' }}" readonly="">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="v_dua_estimasi2" name="v_dua_estimasi2" onkeyup="sum_j(this)" style="background-color: #edf1f6;" value="{{ $record->v_dua_estimasi2 or '' }}" readonly="">
								</td>
								<td class="right aligned" width="15%">
									<input  maxlength="25" data-inputmask="'alias' : 'numericMinus'" id="v_dua_estimasi3" name="v_dua_estimasi3" onkeyup="sum_j(this)" style="background-color: #edf1f6;" value="{{ $record->v_dua_estimasi3 or '' }}" readonly="">
								</td>
								{{-- <td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numeric'" name="v_dua_estimasi2" value="{{ $record->v_dua_estimasi2 or '' }}"></td> --}}
								{{-- <td class="right aligned" width="15%"><input  maxlength="25" data-inputmask="'alias' : 'numeric'" name="v_dua_estimasi3" value="{{ $record->v_dua_estimasi3 or '' }}"></td> --}}
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	function sum_a(elm) {
		var i_satu = $('#i_satu').val();
		var i_dua = $('#i_dua').val();
		var i_tiga_a = $('#i_tiga_a').val();
		var i_tiga_b = $('#i_tiga_b').val();
		var i_tiga_c = $('#i_tiga_c').val();
		var i_tiga_d = $('#i_tiga_d').val();
		var i_tiga_e = $('#i_tiga_e').val();
		var i_tiga_f = $('#i_tiga_f').val();
		var i_empat_a = $('#i_empat_a').val();
		var i_empat_b = $('#i_empat_b').val();
		var i_empat_c = $('#i_empat_c').val();
		var i_lima_a = $('#i_lima_a').val();
		var i_lima_b = $('#i_lima_b').val();
		var i_bila_ada_biaya_tambahan = $('#i_bila_ada_biaya_tambahan').val();
		
		
		var total = parseFloat(i_satu.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_dua.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_tiga_a.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_tiga_b.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_tiga_c.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_tiga_d.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_tiga_e.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_tiga_f.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_empat_a.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_empat_b.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_empat_c.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_lima_a.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_lima_b.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_bila_ada_biaya_tambahan.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('i_sub_total').value = total;

		sum_c()
	}

	function sum_c(elm) {
		var i_sub_total = $('#i_sub_total').val();
		var i_sewa_ruko = $('#i_sewa_ruko').val();

		var total = parseFloat(i_sub_total.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_sewa_ruko.replace(/[^0-9\,-]+/g,""));

		document.getElementById('i_total_inves1_1').value = total;

		sum_e()
	}

	// function sum_d(elm) {
	// 	var i_total_inves1_1 = $('#i_total_inves1_1').val();

	// 	var total = parseFloat(i_total_inves1_1) * (10 / 100) ;

	// 	document.getElementById('i_ppn').value = total;

	// 	sum_e()
	// }

	function sum_e(elm) {
		var i_total_inves1_1 	= $('#i_total_inves1_1').val();
		var i_ppn 				= $('#i_ppn').val();

		var total 				= parseFloat(i_total_inves1_1.replace(/[^0-9\,-]+/g,"")) + parseFloat(i_ppn.replace(/[^0-9\,-]+/g,""));

		document.getElementById('i_total_inves1_2').value = total;
	}


	function sum_f(elm) {
		var iii_labakotor_estimasi1 		= $('#iii_labakotor_estimasi1').val();
		var iii_pendapatanlain2_estimasi1 	= $('#iii_pendapatanlain2_estimasi1').val();
		var iii_pendapatansewa_estimasi1 	= $('#iii_pendapatansewa_estimasi1').val();
		var iii_tambahan_1 					= $('#iii_tambahan_1').val();

		var total 							= parseFloat(iii_labakotor_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iii_pendapatanlain2_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iii_pendapatansewa_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iii_tambahan_1.replace(/[^0-9\,-]+/g,""));

		document.getElementById('iii_total_estimasi1').value = total;

		sum_l()
	}

	function sum_g(elm) {
		var iii_labakotor_estimasi2 		= $('#iii_labakotor_estimasi2').val();
		var iii_pendapatanlain2_estimasi2 	= $('#iii_pendapatanlain2_estimasi2').val();
		var iii_pendapatansewa_estimasi2 	= $('#iii_pendapatansewa_estimasi2').val();
		var iii_tambahan_2 					= $('#iii_tambahan_2').val();

		var total 							= parseFloat(iii_labakotor_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iii_pendapatanlain2_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iii_pendapatansewa_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iii_tambahan_2.replace(/[^0-9\,-]+/g,""));

		document.getElementById('iii_total_estimasi2').value = total;

		sum_m()
	}

	function sum_h(elm) {
		var iii_labakotor_estimasi3 		= $('#iii_labakotor_estimasi3').val();
		var iii_pendapatanlain2_estimasi3 	= $('#iii_pendapatanlain2_estimasi3').val();
		var iii_pendapatansewa_estimasi3 	= $('#iii_pendapatansewa_estimasi3').val();
		var iii_tambahan_3 					= $('#iii_tambahan_3').val();

		var total 							= parseFloat(iii_labakotor_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iii_pendapatanlain2_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iii_pendapatansewa_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iii_tambahan_3.replace(/[^0-9\,-]+/g,""));

		document.getElementById('iii_total_estimasi3').value = total;

		sum_n()
	}

	function sum_i(elm) {
		var iv_satu_a_estimasi1 	= $('#iv_satu_a_estimasi1').val();
		var iv_satu_b_estimasi1 	= $('#iv_satu_b_estimasi1').val();
		var iv_satu_c_estimasi1 	= $('#iv_satu_c_estimasi1').val();
		var iv_satu_d_estimasi1 	= $('#iv_satu_d_estimasi1').val();
		var iv_satu_e_estimasi1 	= $('#iv_satu_e_estimasi1').val();
		var iv_satu_f_estimasi1 	= $('#iv_satu_f_estimasi1').val();
		var iv_satu_g_estimasi1 	= $('#iv_satu_g_estimasi1').val();
		var iv_satu_h_estimasi1 	= $('#iv_satu_h_estimasi1').val();
		var iv_satu_i_estimasi1 	= $('#iv_satu_i_estimasi1').val();
		var iv_dua_estimasi1 		= $('#iv_dua_estimasi1').val();
		var iv_tiga_a_estimasi1 	= $('#iv_tiga_a_estimasi1').val();
		var iv_tiga_b_estimasi1 	= $('#iv_tiga_b_estimasi1').val();
		var iv_empat_a_estimasi1 	= $('#iv_empat_a_estimasi1').val();
		var iv_empat_b_estimasi1 	= $('#iv_empat_b_estimasi1').val();
		var iv_empat_c_estimasi1 	= $('#iv_empat_c_estimasi1').val();
		var iv_empat_d_estimasi1 	= $('#iv_empat_d_estimasi1').val();
		var iv_tambahan_satu 		= $('#iv_tambahan_satu').val();
		var iv_tambahan_dua 		= $('#iv_tambahan_dua').val();
		var iv_tambahan_tiga 		= $('#iv_tambahan_tiga').val();
		var iv_tambahan_empat 		= $('#iv_tambahan_empat').val();

		var total 					= parseFloat(iv_satu_a_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_b_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_c_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_d_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_e_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_f_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_g_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_h_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_i_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_dua_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tiga_a_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tiga_b_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_a_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_b_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_c_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_d_estimasi1.replace(/[^0-9\,-]+/g,""))  + parseFloat(iv_tambahan_satu.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_dua.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_tiga.replace(/[^0-9\,-]+/g,""))  + parseFloat(iv_tambahan_empat.replace(/[^0-9\,-]+/g,""));

		document.getElementById('iv_total_estimasi1').value = total;

		var v_satu_estimasi1 		= $('#v_satu_estimasi1').val();
		// console.log(v_satu_estimasi1);


		var laba_cash				= parseFloat(v_satu_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_a_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_b_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_c_estimasi1.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_d_estimasi1.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('v_dua_estimasi1').value = laba_cash;

		sum_l()
	}

	function sum_j(elm) {
		var iv_satu_a_estimasi2 	= $('#iv_satu_a_estimasi2').val();
		var iv_satu_b_estimasi2 	= $('#iv_satu_b_estimasi2').val();
		var iv_satu_c_estimasi2 	= $('#iv_satu_c_estimasi2').val();
		var iv_satu_d_estimasi2 	= $('#iv_satu_d_estimasi2').val();
		var iv_satu_e_estimasi2 	= $('#iv_satu_e_estimasi2').val();
		var iv_satu_f_estimasi2 	= $('#iv_satu_f_estimasi2').val();
		var iv_satu_g_estimasi2 	= $('#iv_satu_g_estimasi2').val();
		var iv_satu_h_estimasi2 	= $('#iv_satu_h_estimasi2').val();
		var iv_satu_i_estimasi2 	= $('#iv_satu_i_estimasi2').val();
		var iv_dua_estimasi2 		= $('#iv_dua_estimasi2').val();
		var iv_tiga_a_estimasi2 	= $('#iv_tiga_a_estimasi2').val();
		var iv_tiga_b_estimasi2 	= $('#iv_tiga_b_estimasi2').val();
		var iv_empat_a_estimasi2 	= $('#iv_empat_a_estimasi2').val();
		var iv_empat_b_estimasi2 	= $('#iv_empat_b_estimasi2').val();
		var iv_empat_c_estimasi2 	= $('#iv_empat_c_estimasi2').val();
		var iv_empat_d_estimasi2 	= $('#iv_empat_d_estimasi2').val();
		var iv_tambahan_satu_b 		= $('#iv_tambahan_satu_b').val();
		var iv_tambahan_dua_b 		= $('#iv_tambahan_dua_b').val();
		var iv_tambahan_tiga_b 		= $('#iv_tambahan_tiga_b').val();
		var iv_tambahan_empat_b 	= $('#iv_tambahan_empat_b').val();

		var total 					= parseFloat(iv_satu_a_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_b_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_c_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_d_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_e_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_f_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_g_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_h_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_i_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_dua_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tiga_a_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tiga_b_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_a_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_b_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_c_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_d_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_satu_b.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_dua_b.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_tiga_b.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_empat_b.replace(/[^0-9\,-]+/g,""));

		document.getElementById('iv_total_estimasi2').value = total;

		var v_satu_estimasi2 		= $('#v_satu_estimasi2').val();
		// console.log(v_satu_estimasi1);


		var laba_cash				= parseFloat(v_satu_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_a_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_b_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_c_estimasi2.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_d_estimasi2.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('v_dua_estimasi2').value = laba_cash;

		sum_m()
	}

	function sum_k(elm) {
		var iv_satu_a_estimasi3 	= $('#iv_satu_a_estimasi3').val();
		var iv_satu_b_estimasi3 	= $('#iv_satu_b_estimasi3').val();
		var iv_satu_c_estimasi3 	= $('#iv_satu_c_estimasi3').val();
		var iv_satu_d_estimasi3 	= $('#iv_satu_d_estimasi3').val();
		var iv_satu_e_estimasi3 	= $('#iv_satu_e_estimasi3').val();
		var iv_satu_f_estimasi3 	= $('#iv_satu_f_estimasi3').val();
		var iv_satu_g_estimasi3 	= $('#iv_satu_g_estimasi3').val();
		var iv_satu_h_estimasi3 	= $('#iv_satu_h_estimasi3').val();
		var iv_satu_i_estimasi3 	= $('#iv_satu_i_estimasi3').val();
		var iv_dua_estimasi3 		= $('#iv_dua_estimasi3').val();
		var iv_tiga_a_estimasi3 	= $('#iv_tiga_a_estimasi3').val();
		var iv_tiga_b_estimasi3 	= $('#iv_tiga_b_estimasi3').val();
		var iv_empat_a_estimasi3 	= $('#iv_empat_a_estimasi3').val();
		var iv_empat_b_estimasi3 	= $('#iv_empat_b_estimasi3').val();
		var iv_empat_c_estimasi3 	= $('#iv_empat_c_estimasi3').val();
		var iv_empat_d_estimasi3 	= $('#iv_empat_d_estimasi3').val();
		var iv_tambahan_satu_c 		= $('#iv_tambahan_satu_c').val();
		var iv_tambahan_dua_c 		= $('#iv_tambahan_dua_c').val();
		var iv_tambahan_tiga_c 		= $('#iv_tambahan_tiga_c').val();
		var iv_tambahan_empat_c 	= $('#iv_tambahan_empat_c').val();

		var total 					= parseFloat(iv_satu_a_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_b_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_c_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_d_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_e_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_f_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_g_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_h_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_satu_i_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_dua_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tiga_a_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tiga_b_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_a_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_b_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_c_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_d_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_satu_c.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_dua_c.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_tiga_c.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_tambahan_empat_c.replace(/[^0-9\,-]+/g,""));

		document.getElementById('iv_total_estimasi3').value = total;

		var v_satu_estimasi3 		= $('#v_satu_estimasi3').val();
		// console.log(v_satu_estimasi1);


		var laba_cash				= parseFloat(v_satu_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_a_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_b_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_c_estimasi3.replace(/[^0-9\,-]+/g,"")) + parseFloat(iv_empat_d_estimasi3.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('v_dua_estimasi3').value = laba_cash;


		sum_n()
	}

	function sum_l(elm) {
		var iii_total_estimasi1 			= $('#iii_total_estimasi1').val();
		var iv_total_estimasi1 				= $('#iv_total_estimasi1').val();

		var total 							= parseFloat(iii_total_estimasi1.replace(/[^0-9\,-]+/g,"")) - parseFloat(iv_total_estimasi1.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('v_satu_estimasi1').value = total;

		// sum_i()
	}

	function sum_m(elm) {
		var iii_total_estimasi2 			= $('#iii_total_estimasi2').val();
		var iv_total_estimasi2 				= $('#iv_total_estimasi2').val();

		var total 							= parseFloat(iii_total_estimasi2.replace(/[^0-9\,-]+/g,"")) - parseFloat(iv_total_estimasi2.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('v_satu_estimasi2').value = total;
	}

	function sum_n(elm) {
		var iii_total_estimasi3 			= $('#iii_total_estimasi3').val();
		var iv_total_estimasi3 				= $('#iv_total_estimasi3').val();

		var total 							= parseFloat(iii_total_estimasi3.replace(/[^0-9\,-]+/g,"")) - parseFloat(iv_total_estimasi3.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('v_satu_estimasi3').value = total;
	}

	function sum_o(elm) {
		var ii_struk_terburuk 			= $('#ii_struk_terburuk').val();
		var ii_average_terburuk 				= $('#ii_average_terburuk').val();

		var total 							= parseFloat(ii_struk_terburuk.replace(/[^0-9\,-]+/g,"")) * parseFloat(ii_average_terburuk.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('ii_sales_terburuk').value = total;
	}

	function sum_p(elm) {
		var ii_average_normal 			= $('#ii_average_normal').val();
		var ii_struk_normal 				= $('#ii_struk_normal').val();

		var total 							= parseFloat(ii_average_normal.replace(/[^0-9\,-]+/g,"")) * parseFloat(ii_struk_normal.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('ii_sales_normal').value = total;
	}

	function sum_q(elm) {
		var ii_struk_terbaik 			= $('#ii_struk_terbaik').val();
		var ii_average_terbaik 				= $('#ii_average_terbaik').val();

		var total 							= parseFloat(ii_struk_terbaik.replace(/[^0-9\,-]+/g,"")) * parseFloat(ii_average_terbaik.replace(/[^0-9\,-]+/g,"")) ;

		document.getElementById('ii_sales_terbaik').value = total;
	}

	$(document).ready(function(){
		// lsi
		$('select[name="tmuk_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-lsi/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="lsi_id"]').val(response.lsi_id)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		});

	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    })

	})
</script>