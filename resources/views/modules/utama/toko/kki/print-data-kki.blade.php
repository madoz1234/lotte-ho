<style type="text/css">

.page_header {width: 50%; border: none; padding: 2mm; font-size: 13px;}

.tables {
    border-collapse: collapse;
}

.tables th{
    border: 1px solid black; font-size: 9px;
}

.tables td{    border: 1px solid black; font-size: 9px;
}
}

</style>
<page>
        {{-- <p id="" style="; width: 90px; height: 5px;text-align: right; padding-right: 40px; ">
            <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
        </p> --}}

         <table class="page_header" border="0" width="" align="center">
            <tr>
                <td style="width: 480px;">
                    <b>Perkiraan Hasil Toko Kemitraan</b> <br> <span>{{ $row->tmuk->lsi->nama  }} - {{ $row->tmuk->nama  }} ({{ $row->tmuk->kode  }})</span>
                </td>
                <td style="text-align: right; padding-left: 100px;" id="text_header">
                    {{-- <p class="title" style="font-size: 21px;">PICKING RESULT</p> --}}
                    {{-- <barcode dimension="1D" type="C128" value="343435" label="label" style="width:78mm; height:10mm;"></barcode>  --}}<img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px;">
                </td>
            </tr>
 
        </table>


        {{-- <table border="0" padding="0" cellpadding="-10" cellspacing="0 align="center" style="font-size: 10px; border-spacing: 0; border-collapse: collapse;">
        <tr>
            <td>LSI </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $row->tmuk->lsi->nama  }}</td>
        </tr>
        <tr>
            <td>TMUK</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $row->tmuk->nama  }}</td>
        </tr>
        </table> --}}

        <table class="tables" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <th style="width:5%; text-align: center;">I.</th>
                <th style="width:70%; text-align: center;">Perkiraan Investasi</th>
                <th style="width:15%; text-align: center;">Nilai</th>
            </tr>
            <tr>
                <td style="width:5%;text-align: center;">1</td>
                <td style="width:70%">Startup & Biaya Promosi Opening</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_satu) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">2</td>
                <td style="width:70%">Perijinan Usaha toko</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_dua) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">3</td>
                <td style="width:70%">a. Pekerjaan Renovasi Sipil</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_tiga_a) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">b. Pekerjaan Kusen Aluminium + Kaca Cermin</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_tiga_b) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">c. Pekerjaan Folding Gate + Polycarbonate + Teralis</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_tiga_c) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">d. Pekerjaan Instalasi Listrik (lampu, regrouping)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_tiga_d) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">e. Instalasi Pemasangan AC</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_tiga_e) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">f. Pekerjaan Signage (Lisping Papan Nama Toko) Repainting</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_tiga_f) }}</td>
            </tr>

            <tr>
                <td style="width:5%; text-align: center;">4</td>
                <td style="width:70%">a. Peralatan AC Spil</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_empat_a) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">b. Peralatan Non Elektronik (Rak, Furniture, Acrylic, dll)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_empat_b) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">c. Peralatan Elektronik (Komputer, Cooler & Timbangan)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_empat_c) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">5</td>
                <td style="width:70%">a. Stock Awal</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_lima_a) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">b. Modal Kerja 1 bulan (ditransfer ke rek Ops TMUK)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_lima_b) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">6</td>
                <td style="width:70%">Lain-lain</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_bila_ada_biaya_tambahan) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70% ; text-align: center;"><b>Sub Total Investasi (Sebelum PPN)</b></td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_sub_total) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">Sewa Ruko selama 5 tahun</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_sewa_ruko) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%; text-align: center;"><b>Total Investasi (Sebelum PPN)</b></td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_total_inves1_1) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%; text-align: center;"><b>PPN</b></td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_ppn) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%; text-align: center;"><b>Total Investasi (Sesudah PPN)</b></td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_total_inves1_2) }}</td>
            </tr>
            <tr>
                <td style="width:5%"></td>
                <td style="width:70%">Perkiraan stok ideal</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->i_perkiraan_stok_ideal) }}</td>
            </tr>
        </table>
        <br>

        <table class="tables" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <th style="width:5%; text-align: center;">II.</th>
                <th style="width:40%; text-align: center;">Perkiraan Penjualan per bulan</th>
                <th style="width:15%; text-align: center;">Terburuk (BEP)</th>
                <th style="width:15%; text-align: center;">Normal (Layak)</th>
                <th style="width:15%; text-align: center;">Terbaik (Aman)</th>
            </tr>
             <tr>
                <td style="width:5%; text-align: center;">1</td>
                <td style="width:40%; text-align: left;">Struk Per Day (STD)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_struk_terburuk) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_struk_normal) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_struk_terbaik) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">2</td>
                <td style="width:40%; text-align: left;">Average Purchase per Customer (APC)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_average_terburuk) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_average_normal) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_average_terbaik) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">3</td>
                <td style="width:40%; text-align: left;">Sales Per Day (SPD)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_sales_terburuk) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_sales_normal) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_sales_terbaik) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">4</td>
                <td style="width:40%; text-align: left;">Penjualan per Bulan (SPD x 30 hari)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_penjualan_terburuk) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_penjualan_normal) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->ii_penjualan_terbaik) }}</td>
            </tr>
        </table>
        <br>

        <table class="tables" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <th style="width:5%; text-align: center;">III.</th>
                <th style="width:40%; text-align: center;">Perkiraan Pendapatan per bulan</th>
                <th style="width:15%; text-align: center;">Terburuk (BEP)</th>
                <th style="width:15%; text-align: center;">Normal (Layak)</th>
                <th style="width:15%; text-align: center;">Terbaik (Aman)</th>
            </tr>
             <tr>
                <td style="width:5%; text-align: center;">1</td>
                <td style="width:40%; text-align: left;">Laba Kotor Penjualan </td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_labakotor_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_labakotor_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_labakotor_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">2</td>
                <td style="width:40%; text-align: left;">Pendapatan Lain-lain</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_pendapatanlain2_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_pendapatanlain2_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_pendapatanlain2_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">3</td>
                <td style="width:40%; text-align: left;">Pendapatan Sewa Tempat Pemanjangan</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_pendapatansewa_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_pendapatansewa_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_pendapatansewa_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">4</td>
                <td style="width:40%; text-align: left;">Pendapatan Lain-lain</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_tambahan_1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_tambahan_2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iii_tambahan_3) }}</td>
            </tr>
            <tr>
                <th style="width:5%; text-align: center;"></th>
                <th style="width:40%; text-align: left;">Total Perkiraan Pendapatan / bulan</th>
                <th style="width:15%; text-align: right;">{{ rupiah($row->iii_total_estimasi1) }}</th>
                <th style="width:15%; text-align: right;">{{ rupiah($row->iii_total_estimasi2) }}</th>
                <th style="width:15%; text-align: right;">{{ rupiah($row->iii_total_estimasi3) }}</th>
            </tr>
        </table>
        <br>

        <table class="tables" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <th style="width:5%; text-align: center;">IV.</th>
                <th style="width:40%; text-align: left;">Perkiraan Pengeluaran per bulan</th>
                <th style="width:15%; text-align: center;"></th>
                <th style="width:15%; text-align: center;"></th>
                <th style="width:15%; text-align: center;"></th>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">1</td>
                <td style="width:40%; text-align: left;">Biaya Type A -a. Biaya Air</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_a_estimasi3) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_a_estimasi3) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_a_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-b. Biaya Listrik</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_b_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_b_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_b_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-c. Biaya Telp & Internet</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_c_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_c_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_c_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-d. Biaya Keamanan</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_d_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_d_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_d_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-e. Biaya Lainnya (Sumbangan, dll jika ada)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_e_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_e_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_e_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-f. Plastik Pembungkus (struk sales X days)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_f_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_f_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_f_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-g. Barang Rusak</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_g_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_g_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_g_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-h. Barang Hilang</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_h_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_h_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_h_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-i. Biaya Distribusi (Administrasi)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_i_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_i_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_satu_i_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">Bila ada Biaya Tambahan</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_satu) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_satu_b) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_satu_c) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">2</td>
                <td style="width:40%; text-align: left;">Biaya Type B :a. Karyawan Toko</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_dua_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_dua_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_dua_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">Bila ada Biaya Tambahan</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_dua) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_dua_b) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_dua_c) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">3</td>
                <td style="width:40%; text-align: left;">Biaya Type C -a. Premi lainnya</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tiga_a_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tiga_a_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tiga_a_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-b. PPh Final Sewa Tempat Pemajangan (5% Stp)</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tiga_b_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tiga_b_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tiga_b_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">Bila ada Biaya Tambahan</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_tiga) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_tiga_b) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_tiga_c) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">4</td>
                <td style="width:40%; text-align: left;">Biaya Type D -a. Amortisasi Biaya Lainnya</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_a_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_a_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_a_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-b. Amortisasi Perijinan, Renovasi, dll</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_b_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_b_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_b_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-c. Depresiasi Investasi Peralatan, dll</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_c_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_c_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_c_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;"></td>
                <td style="width:40%; text-align: left;">-d. Amortisasi Sewa Bangunan</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_d_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_d_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_empat_d_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">5</td>
                <td style="width:40%; text-align: left;">Biaya Lain-lain</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_empat) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_empat_b) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->iv_tambahan_empat_c) }}</td>
            </tr>
            <tr>
                <th style="width:5%; text-align: center;"></th>
                <th style="width:40%; text-align: left;">Total Pengeluaran / bulan</th>
                <th style="width:15%; text-align: right;">{{ rupiah($row->iv_total_estimasi1) }}</th>
                <th style="width:15%; text-align: right;">{{ rupiah($row->iv_total_estimasi2) }}</th>
                <th style="width:15%; text-align: right;">{{ rupiah($row->iv_total_estimasi3) }}</th>
            </tr>
        </table><br>

        <table class="tables" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <th style="width:5%; text-align: center;">V.</th>
                <th colspan="4" style="width:20%; text-align: center;">Perkiraan Rata-rata Laba /(Rugi) perbulan selama 5 tahun (sebelum pajak penghasilan)</th>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">1</td>
                <td style="width:40%; text-align: left;">Laba Bersih</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->v_satu_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->v_satu_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->v_satu_estimasi3) }}</td>
            </tr>
            <tr>
                <td style="width:5%; text-align: center;">2</td>
                <td style="width:40%; text-align: left;">Laba Cash</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->v_dua_estimasi1) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->v_dua_estimasi2) }}</td>
                <td style="width:15%; text-align: right;">{{ rupiah($row->v_dua_estimasi3) }}</td>
            </tr>
        </table><br>



        <table class="" border="0" align="" style="padding-left: 35px;"><br><br>
            {{-- <tr>
                <td style="padding-left: 5px;">
                    <div style="font-size: 5px; text-align: right;">{{ \Carbon\Carbon::now()->format('d, M Y ') }}</div>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr> --}}
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">Diperiksa,</div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;"></div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">Disetujui,</div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;"></div>
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">CSO HO</div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">Project Designer</div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">SGM</div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="font-size: 10px;">Mitra TMUK</div>
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 120px;height:50px; text-align: center;">
                            {{-- <img src="{{ asset('storage/'.$record->verifikasiuser1->pengguna->ttd) }}" width="118" style="margin-top: 0px;"> --}}
                    </div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 120px;height:50px; text-align: center;">
                            {{-- <img src="{{ asset('storage/'.$record->verifikasiuser2->pengguna->ttd) }}" width="118" style="margin-top: 0px;"> --}}
                    </div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 120px;height:50px; text-align: center;">
                            {{-- <img src="{{ asset('storage/'.$record->verifikasiuser3->pengguna->ttd) }}" width="118" style="margin-top: 0px;"> --}}
                    </div>
                </td>
                <td style="width: 25%; padding-left: 10px;">
                    <div style="border: 1px solid #000; width: 120px;height:50px; text-align: center;">
                            {{-- <img src="{{ asset('storage/'.$record->verifikasiuser4->pengguna->ttd) }}" width="118" style="margin-top: 0px;"> --}}
                    </div>
                </td>
            </tr>
        </table>


</page>