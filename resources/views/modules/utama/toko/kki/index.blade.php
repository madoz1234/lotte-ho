@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input name="filter[nomor]" placeholder="Nomor KKI" type="text">
	</div>

	<div class="field">
		<select name="filter[tmuk_id]" class="ui fluid search selection dropdown" style="width: 100%;">
		    {!! \Lotte\Models\Master\Tmuk::options('nama', 'id',[], '-- Pilih Tmuk --') !!}
		</select>
	</div>

	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>

	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_kki();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
	d.nomor = $("input[name='filter[nomor]']").val();
    d.tmuk_id = $("select[name='filter[tmuk_id]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		status: {
			identifier: 'status',
			rules: [{
				type   : 'empty',
				prompt : 'Status Harus Terisi'
			}]
		},
		tanggal_submit: {
			identifier: 'tanggal_submit',
			rules: [{
				type   : 'empty',
				prompt : 'Tanggal submit Harus Terisi'
			}]
		},
		lsi_id: {
			identifier: 'lsi_id',
			rules: [{
				type   : 'empty',
				prompt : 'LSI Harus Terisi'
			}]
		},
		tmuk_id: {
			identifier: 'tmuk_id',
			rules: [{
				type   : 'empty',
				prompt : 'TMUK Harus Terisi'
			}]
		}
	};
</script>
@endsection


@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});

		$('.ui.dropdown').dropdown();
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_kki = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-kki') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

				var nomor = document.createElement("input");
				nomor.setAttribute("type", "hidden");
				nomor.setAttribute("name", 'nomor');
				nomor.setAttribute("value", $('[name="filter[nomor]"]').val());
				form.appendChild(nomor);

		        var tmuk_id = document.createElement("input");
				tmuk_id.setAttribute("type", "hidden");
				tmuk_id.setAttribute("name", 'tmuk_id');
				tmuk_id.setAttribute("value", $("select[name='filter[tmuk_id]']").val());
				form.appendChild(tmuk_id);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append