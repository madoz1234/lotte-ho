<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat KKI</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		
		<!-- Filter -->
		<div class="three fields">
			<div class="field">
				<label>LSI</label>
				<div class="ui labeled input">
					<select name="filter[region]" class="ui fluid search selection dropdown">
						<option value="">Pilih Lsi</option>
						<option value="00001" selected>Jabodetabek</option>
						<option value="00002">Surabaya</option>
						<option value="00003">Bekasi</option>
						<option value="00004">Bandung</option>
					</select>
				</div>
			</div>

			<div class="field">
				<label>TMUK</label>
				<div class="ui labeled input">
					<select name="filter[region]" class="ui fluid search selection dropdown">
						<option value="">Pilih TMUK</option>
						<option value="0600100010" selected>Etokokoe</option>
						<option value="0602600012">Berkah</option>
						<option value="0601800002">Jernih</option>
						<option value="0600700018">Pragma Informatika</option>
					</select>
				</div>
			</div>

			<div class="field">
				<label>Tanggal Submit</label>
				<div class="ui labeled input">
					<select name="filter[region]" class="ui fluid search selection dropdown">
						<option value="">Tahun Fiskal</option>
						<option value="00001" selected>01/01/2017 - 31/12/2017  Active</option>
						<option value="00002">01/01/2016 - 31/12/2016  Active</option>
						<option value="00003">01/01/2015 - 31/12/2015  Active</option>
						<option value="00004">01/01/2014 - 31/12/2014  Active</option>
					</select>
				</div>
			</div>
		</div>

		<div class="three fields">
			<div class="field">
				<label>Import Data Excel</label>
				<form class="ui data form" id="dataForm" action="{{ URL::to('utama/toko/kki/postimportexcel') }}" method="POST" enctype="multipart/form-data">
					{{-- {!! csrf_field() !!} --}}
					<div class="field">
						<input type="file" name="kki">
					</div>
					<div class="ui error message"></div>
				</form>
			</div>
		</div>

		<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>

{{-- <div class="header">Import Data Excel</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ URL::to('utama/toko/kki/postimportexcel') }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="field">
			<input type="file" name="kki">
		</div>
		<div class="ui error message"></div>
	</form>
</div> --}}
		<!-- Filter -->
		<br>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>