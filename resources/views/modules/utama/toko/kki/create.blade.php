<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data KKI</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		{{-- <div class="ui error message"></div> --}}

		<div class="field">
			<div class="fields">
				<div class="seven wide field">
					<label>TMUK :</label>
					<select name="tmuk_id" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',[], '-- Pilih TMUK --') !!}
					</select>
				</div>
				<div class="seven wide field">
					<label>LSI :</label>
					<input type="text" name="lsi_id" placeholder="LSI" value="{{ $lsi_id or "" }}" style="background-color: #edf1f6;"	readonly="">
					{{-- <select name="lsi_id" class="ui fluid search selection dropdown" style="width: 100%;" disabled="">
						{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',[], '-- Pilih TMUK --') !!}
					</select> --}}
				</div>
				{{-- <div class="seven wide field">
					<label>Tanggal Submit KKI:</label>
					<input name="tanggal_submit" placeholder="Inputkan Saldo Escrow" type="date" value="{{ $tanggal_submit or "" }}">
				</div> --}}
				<div class="seven wide field">
					<label>Tanggal Submit KKI</label>		
					<div class="ui calendar">
						<div class="ui input left icon">
							<i class="calendar icon date"></i>
							<input name="tanggal_submit" type="text" placeholder="Inputkan Tanggal Submit KKI" value="{{ $tanggal_submit or "" }}">
						</div>
					</div>
				</div>


				<div class="seven wide field">
					<label>Nomor KKI</label>
					<input name="nomor" placeholder="Nomor KKI" type="text" value="{{ $nomor or "" }}" style="background-color: #edf1f6;" readonly>
				</div>
				<div class="seven wide field">
					<div class="field">
						<label>Status</label>
						<div class="inline fields">
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="status" tabindex="0" class="" value="1">
									<label>Berjalan</label>
								</div>
							</div>

							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="status" tabindex="0" class="" value="0">
									<label>Selesai</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="ui error message"></div>
</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function(){

		// lsi
		$('select[name="tmuk_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-lsi/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="lsi_id"]').val(response.lsi_id)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		$('select[name="tmuk_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-nomor/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nomor"]').val(response.nomor)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

	})
</script>