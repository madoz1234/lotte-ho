<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Provinsi</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="field">
			<label>Nama Provinsi</label>
			<input name="nama" placeholder="Nama Provinsi" type="text" value="{{ old('nama') }}">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>