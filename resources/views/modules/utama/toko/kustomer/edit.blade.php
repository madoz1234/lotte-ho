@include('layouts.scripts.inputmask')

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Kostumer</div>
<div class="scrolling content">

	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	<div class="ui error message"></div>
		{!! csrf_field() !!}
	    <input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui grid">
			<div class="eight wide column">
				<h4 class="ui dividing header">Data Kostumer</h4>
				<div class="ui equal width grid">
					<div class="column">
						<div class="field">
							<label>TMUK Induk</label>	
							<select name="tmuk_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->tmuk_id) ? $record->tmuk_id : '',], '-- Pilih TMUK --') !!}
							</select>
						</div>
						<div class="two fields">
							<div class="field">
								<label>Kode Kostumer</label>
								<input name="kode" style="background-color: #edf1f6;" placeholder="Kode Region" type="text" value="{{ $record->kode }}" readonly>
							</div>
							<div class="field">
								<label>Nomor Member Card</label>
								<select name="membercard_id" class="ui fluid search selection dropdown" style="width: 100%;">
									{!! \Lotte\Models\Master\MemberCard::options('nomor', 'id',['selected' => isset($record->membercard_id) ? $record->membercard_id : '',], '-- Pilih Member Card --') !!}
								</select>
							</div>
						</div>

						<div class="two fields">
							
							{{-- <div class="field">
								<label for="fruit">Jenis Kostumer</label>
								<select name="jeniskustomer_id" class="ui fluid search selection dropdown" style="width: 100%;">
									{!! \Lotte\Models\Master\JenisKustomer::options('jenis', 'id',['selected' => isset($record->jeniskustomer_id) ? $record->jeniskustomer_id : '',], '-- Pilih Jenis Kustomer --') !!}
								</select>
							</div> --}}

							<input hidden="" style="background-color: #edf1f6;" type="text" name="jeniskustomer_id" placeholder="No Telepon" value="{{ $record->jeniskustomer_id or "" }}" readonly="">
							<div class="field">
								<label>Jenis Kostumer</label>
								<input style="background-color: #edf1f6;" type="text" name="jeniskustomer" placeholder="Jenis Kustomer" value="{{ $record->kustomer->jenis or "" }}" readonly="">
							</div>

							<div class="field">
								<label>Nama Pemilik</label>
								<input style="background-color: #edf1f6;" type="text" name="nama_pemilik" value="{{ $record->membercard->nama or "" }}" readonly="">
							</div>
						</div>
						
						<div class="field">
							<label>Alamat</label>
							<div class="fields">
								<div class="twelve wide field">
									<input type="text" name="alamat" placeholder="Alamat Jalan" value="{{ $record->alamat }}">
								</div>
								<div class="four wide field">
									<input class="length" maxlength="5" type="text" name="kode_pos" placeholder="Kode Pos" value="{{ $record->kode_pos }}">
								</div>
							</div>
							<div class="three fields">
								<div class="field">
									<label class="hidden">Provinsi</label>
									<select name="provinsi_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\Provinsi::options('nama', 'id',['selected' => isset($record->kota_id) ? $record->kota_id : '',], '-- Pilih Provinsi --') !!}
									</select>
								</div>
								<div class="field">
									<label class="hidden">Kota</label>
									<select name="kota_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\Kota::options('nama', 'id',['selected' => isset($record->kota_id) ? $record->kota_id : '',], '-- Pilih Kota --') !!}
									</select>
								</div>
								<div class="field">
									<label class="hidden">Kecamatan</label>
									<select name="kecamatan_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\Kecamatan::options('nama', 'id',['selected' => isset($record->kecamatan_id) ? $record->kecamatan_id : '',], '-- Pilih Kecamatan --') !!}
									</select>
								</div>
							</div>
						</div>

						<div class="two fields">
							<div class="field">
								<label>No Telpon</label>
								<input style="background-color: #edf1f6;" type="text" name="telepon" placeholder="No Telepon" value="{{ $record->membercard->telepon or "" }}" readonly="">
							</div>
							<div class="field">
								<label>Email</label>
								<input style="background-color: #edf1f6;" type="email" name="email" placeholder="Email" value="{{ $record->membercard->email or "" }}" readonly="">
							</div>
							
						</div>
						<div class="field gps_coordinates {{ (strtoupper($record->kustomer->jenis) == "MEMBER")? 'disabled' : '' }}">
							<label>GPS Coordinates</label>
							<div class="two fields">
								<div class="field">
									<input type="text" name="latitude" placeholder="Latitude" value="{{ $record->latitude or "" }}">
								</div>
								<div class="field">
									<input type="text" name="longitude" placeholder="Longitude" value="{{ $record->longitude or "" }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="eight wide column">
				<h4 class="ui dividing header">Keuangan &amp; Pajak</h4>
				<div class="two fields">
					<div class="field">
						<label>Nama Perusahaan</label>
						<select name="pajak_id" class="ui fluid search selection dropdown" style="width: 100%;">
							{!! \Lotte\Models\Master\Pajak::options('nama', 'id',['selected' => isset($record->pajak_id) ? $record->pajak_id : '',], '-- Pilih Nama Perusahaan --') !!}
							<option value="">Tidak Memiliki NPWP</option>
						</select>
					</div>
					<div class="field">
						<label>NPWP</label>
						<input style="background-color: #edf1f6;" type="text" name="nama_npwp" value="{{ $record->pajak->npwp or "" }}" placeholder="NPWP" readonly="">
					</div>
				</div>

				<div class="two fields">
					<div class="field">
						<label>Limit Kredit</label>
						<div class="ui labeled input">
							<div class="ui basic label">
								Rp
							</div>
							<input {{-- class="length" --}} type="text" name="limit_kredit" maxlength="20" data-inputmask="'alias' : 'numeric'" placeholder="Inputkan Limit" value="{{ $record->limit_kredit or "" }}">
						</div>
					</div>

					<div class="field">
						<label>Persen Diskon</label>
						<div class="ui right labeled input">
							<input class="length" maxlength="5" type="text" name="persen_diskon" placeholder="Inputkan Diskon" value="{{ $record->persen_diskon or "" }}">
							<div class="ui basic label">%</div>
						</div>
					</div>
				</div>
				
				<div class="two fields">
					<div class="field">
						<label>Cara Pembayaran</label>
						<select name="cara_pembayaran" class="ui fluid search selection dropdown">
	                        <option>-- Pilih --</option>
	                        <option class="item" {{ $record->cara_pembayaran or '' == 1 ? 'selected' : '' }} value="1">Tunai </option>
							<option class="item" {{ $record->cara_pembayaran or '' == 2 ? 'selected' : '' }} value="2">Jatuh Tempo Tgl 15  Bulan Berikutnya</option>
							<option class="item" {{ $record->cara_pembayaran or '' == 3 ? 'selected' : '' }} value="3">Jatuh Tempo Di Akhir Bulan Berikutnya</option>
							<option class="item" {{ $record->cara_pembayaran or '' == 4 ? 'selected' : '' }} value="4">Jatuh Tempo Dalam 10 hari</option>
	                    </select>
					</div>

					<div class="field">
						<label>Status Kredit</label>
						<select name="status_kredit" class="ui fluid search selection dropdown">
	                        <option>-- Pilih --</option>
	                        <option class="item" value="3" {{ $record->status_kredit or '' == 3 ? 'selected' : '' }}>Good History</option>
							<option class="item" value="2" {{ $record->status_kredit or '' == 2 ? 'selected' : '' }}>In LiQuidation</option>
							<option class="item" value="1" {{ $record->status_kredit or '' == 1 ? 'selected' : '' }}>No More Work Until Payment Recaived</option>
	                    </select>
					</div>
				</div>


			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//jenis kustomer
		$('[name=jeniskustomer_id]').change(function(e){
			var text = this.options[this.selectedIndex].text;

			if (text.toUpperCase() == "MEMBER") {
				$('.gps_coordinates').addClass('disabled');
				$('input[name="longitude"]').val('');
				$('input[name="latitude"]').val('');
			}else{
				$('.gps_coordinates').removeClass('disabled');
			}
		});
		// membercard
		$('select[name="membercard_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-membercard/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_pemilik"]').val(response.nama)
				$('input[name="telepon"]').val(response.telepon)
				$('input[name="email"]').val(response.email)
				$('input[name="jeniskustomer_id"]').val(response.jeniskustomer_id)
				$('input[name="jeniskustomer"]').val(response.jeniskustomer)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})
		
		
		// provinsi
		$('[name=provinsi_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kota') }}",
				type: 'GET',
				data:{
					id_provinsi : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kota_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// kota
		$('[name=kota_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kecamatan') }}",
				type: 'GET',
				data:{
					id_kota : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kecamatan_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// kecamatan
		$('[name=kecamatan_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kelurahan') }}",
				type: 'GET',
				data:{
					id_kecamatan : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kelurahan_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// No NPWP
		$('select[name="pajak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-pajak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_npwp"]').val(response.nama_npwp)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    })

	})
</script>