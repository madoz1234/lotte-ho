@extends('layouts.grid')

@section('filters')
<div class="field">
	{{-- <select name="filter[tmuk_id]" class="ui fluid search selection dropdown" style="width: 100%;">
	    {!! \Lotte\Models\Master\TMUK::options('nama', 'id',[], '-- Pilih TMUK --') !!}
    </select> --}}
</div>

<div class="field">
	<input type="text" name="filter[nama_member]" placeholder="Nama Kostumer">
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
	
	
    <button style="background-color: #3537ad;" type="button" class="ui blue importexcel button"><i class="upload icon icon"></i>Upload</button>
    <a href="{{ asset('template\kustomer.xls') }}" class="ui grey button" target="_blank"><i class="download icon icon"></i>Template</a>
	
    <button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_kustomer();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.tmuk_id = $("select[name='filter[tmuk_id]']").val();
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		tmuk_id: {
			identifier: 'tmuk_id',
			rules: [{
				type   : 'empty',
				prompt : 'TMUK Induk Harus Terisi'
			}]
		},
		kode: {
			identifier: 'kode',
			rules: [{
				type   : 'empty',
				prompt : 'Kode Kostumer Harus Terisi'
			}]
		},
		jeniskustomer_id: {
			identifier: 'jeniskustomer_id',
			rules: [{
				type   : 'empty',
				prompt : 'Jenis Kostumer Harus Terisi'
			}]
		},
		membercard_id: {
			identifier: 'membercard_id',
			rules: [{
				type   : 'empty',
				prompt : 'Nama Nomor Member Card Harus Terisi'
			}]
		},
		nama_pemilik: {
			identifier: 'nama_pemilik',
			rules: [{
				type   : 'empty',
				prompt : 'Nama Pemilik Harus Terisi'
			}]
		},
		alamat: {
			identifier: 'alamat',
			rules: [{
				type   : 'empty',
				prompt : 'Alamat Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Isian alamat setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[255]',
				prompt : 'Isian alamat seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		// provinsi_id: {
		// 	identifier: 'provinsi_id',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Provinsi Harus Terisi'
		// 	}]
		// },
		kota_id: {
			identifier: 'kota_id',
			rules: [{
				type   : 'empty',
				prompt : 'Kota Harus Terisi'
			}]
		},
		kecamatan_id: {
			identifier: 'kecamatan_id',
			rules: [{
				type   : 'empty',
				prompt : 'Kota Harus Terisi'
			}]
		},
		telepon: {
			identifier: 'telepon',
			rules: [{
				type   : 'empty',
				prompt : 'No Telpon Harus Terisi'
			}]
		},
		longitude: {
			identifier: 'longitude',
			rules: [{
				type   : 'maxLength[10]',
				prompt : 'Isian nama limit kredit seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		latitude: {
			identifier: 'latitude',
			rules: [{
				type   : 'maxLength[10]',
				prompt : 'Isian nama limit kredit seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		// pajak_id: {
		// 	identifier: 'pajak_id',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'No Nama Perusahaan Harus Terisi'
		// 	}]
		// },
		// nama_npwp: {
		// 	identifier: 'nama_npwp',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'No npwp Harus Terisi'
		// 	}]
		// },
		limit_kredit: {
			identifier: 'limit_kredit',
			rules: [{
				type   : 'empty',
				prompt : 'Limit Kredit Harus Terisi'
			},{
				type   : 'minLength[3]',
				prompt : 'Isian nama limit kredit setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[20]',
				prompt : 'Isian nama limit kredit seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		persen_diskon: {
			identifier: 'persen_diskon',
			rules: [{
				type   : 'empty',
				prompt : 'Persen Diskon Harus Terisi'
			},{
				type   : 'minLength[1]',
				prompt : 'Isian nama persen diskon setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[3]',
				prompt : 'Isian nama persen diskon seharusnya berjumlah {ruleValue} karakter'
			}]
		},

		cara_pembayaran: {
			identifier: 'cara_pembayaran',
			rules: [{
				type   : 'empty',
				prompt : 'Cara Pembayaran Harus Terisi'
			}]
		},
		status_kredit: {
			identifier: 'status_kredit',
			rules: [{
				type   : 'empty',
				prompt : 'Status Kredit Harus Terisi'
			}]
		},
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
	};
</script>
@endsection


@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_kustomer = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-kustomer') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var tmuk_id = document.createElement("input");
		            tmuk_id.setAttribute("type", "hidden");
		            tmuk_id.setAttribute("name", 'tmuk_id');
		            tmuk_id.setAttribute("value", $('[name="filter[tmuk_id]"]').val());
		        form.appendChild(tmuk_id);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append