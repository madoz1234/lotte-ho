@include('layouts.scripts.inputmask')

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Kostumer</div>
<div class="scrolling content">

	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
	<div class="ui error message"></div>
		{!! csrf_field() !!}
		<div class="ui grid">
			<div class="eight wide column">
				<h4 class="ui dividing header">Data Kostumer</h4>
				<div class="ui equal width grid">
					<div class="column">
						<div class="field">
							<label>TMUK Induk</label>	
							<select name="tmuk_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',[], '-- Pilih TMUK --') !!}
							</select>
						</div>
						<div class="two fields">
							<div class="field">
								<label>Kode Kostumer</label>
								<input style="background-color: #edf1f6;" name="kode" placeholder="Kode Region" type="text" value="{{ $code }}" readonly>
								{{-- <input style="background-color: #edf1f6;" type="text" name="kode" value="{{ $code }}" readonly=""> --}}
							</div>
							
							<div class="field">
								<label>Nomor Member Card</label>
								<select name="membercard_id" class="ui fluid search selection dropdown" style="width: 100%;">
									{!! \Lotte\Models\Master\MemberCard::options('nomor', 'id',[], '-- Pilih Member Card --') !!}
								</select>
							</div>
						</div>

						<div class="two fields">
							{{-- <div class="field">
								<label>Jenis Kostumer</label>
								<select name="jeniskustomer_id" class="ui fluid search selection dropdown" style="width: 100%;">
									{!! \Lotte\Models\Master\JenisKustomer::options('jenis', 'id',[
										'selected' => isset($record->jeniskustomer_id) ? $record->jeniskustomer_id : '', 
										'filters' => [function($query){ $query->whereIn('kode', ['00001','00002']); }]], '-- Pilih Type --') !!}
								</select>
							</div> --}}
							<input hidden="" style="background-color: #edf1f6;" type="text" name="jeniskustomer_id" placeholder="No Telepon" value="{{ $jeniskustomer_id or "" }}" readonly="">
							<div class="field">
								<label>Jenis Kostumer</label>
								<input style="background-color: #edf1f6;" type="text" name="jeniskustomer" placeholder="Jenis Kustomer" readonly="">
							</div>
							<div class="field">
								<label>Nama Pemilik</label>
								<input style="background-color: #edf1f6;" type="text" name="nama_pemilik" placeholder="Nama Pemilik" value="{{ $nama_pemilik or "" }}" readonly="">
							</div>
						</div>
						
						<div class="field">
							<label>Alamat</label>
							<div class="fields">
								<div class="twelve wide field">
									<input type="text" name="alamat" placeholder="Alamat Jalan">
								</div>
								<div class="four wide field">
									<input class="length" maxlength="5" type="text" name="kode_pos" placeholder="Kode Pos">
								</div>
							</div>
							<div class="three fields">
								<div class="field">
									<label class="hidden">Provinsi</label>
									<select name="provinsi_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\Provinsi::options('nama', 'id',[], '-- Pilih Provinsi --') !!}
									</select>
								</div>
								<div class="field">
									<label class="hidden">Kota</label>
									<select name="kota_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\Kota::options('nama', 'id',[], '-- Pilih Kota --') !!}
									</select>
								</div>
								<div class="field">
									<label class="hidden">Kecamatan</label>
									<select name="kecamatan_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\Kecamatan::options('nama', 'id',[], '-- Pilih Kecamatan --') !!}
									</select>
								</div>
							</div>
						</div>

						<div class="two fields">
							<div class="field">
								<label>No Telpon</label>
								<input style="background-color: #edf1f6;" type="text" name="telepon" placeholder="No Telepon" value="{{ $telepon or "" }}" readonly="">
							</div>
							<div class="field">
								<label>Email</label>
								<input style="background-color: #edf1f6;" type="email" name="email" placeholder="Email" value="{{ $email or "" }}" readonly="">
							</div>
							
						</div>
						<div class="field gps_coordinates">
							<label>GPS Coordinates</label>
							<div class="two fields">
								<div class="field">
									<input type="text" name="latitude" placeholder="Latitude" value="{{ $latitude or "" }}">
								</div>
								<div class="field">
									<input type="text" name="longitude" placeholder="Longitude" value="{{ $longitude or "" }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="eight wide column">
				<h4 class="ui dividing header">Keuangan &amp; Pajak</h4>
				<div class="two fields">
					<div class="field">
						<label>Nama Perusahaan</label>
						<select name="pajak_id" class="ui fluid search selection dropdown" style="width: 100%;">
						<option></option>
						<option value="0">Tidak Memiliki NPWP</option>
							{!! \Lotte\Models\Master\Pajak::options('nama', 'id',[], '-- Pilih Nama Perusahaan --') !!}
							
						</select>
					</div>
					<div class="field">
						<label>NPWP</label>
						<input style="background-color: #edf1f6;" type="text" name="nama_npwp" value="{{ $npwp or "" }}" placeholder="No NPWP" readonly="">
					</div>
				</div>

				<div class="two fields">
					<div class="field">
						<label>Limit Kredit</label>
						<div class="ui labeled input">
							<div class="ui basic label">
								Rp
							</div>
							<input type="text" name="limit_kredit" maxlength="20" data-inputmask="'alias' : 'numeric'" placeholder="Inputkan Limit" value="{{ $limit_kredit or "" }}">
						</div>
					</div>

					<div class="field">
						<label>Persen Diskon</label>
						<div class="ui right labeled input">
							<input class="length" maxlength="5" type="text" name="persen_diskon" placeholder="Inputkan Diskon" value="{{ $persen_diskon or "" }}">
							<div class="ui basic label">%</div>
						</div>
					</div>
				</div>
				
				<div class="two fields">
					<div class="field">
						<label>Cara Pembayaran</label>		
						<div class="ui fluid search selection dropdown">
							<input type="hidden" name="cara_pembayaran">
							<i class="dropdown icon"></i>
							<div class="default text">Pilih Cara Pembayaran</div>
							<div class="menu">
								<div class="item" data-value="1">Tunai </div>
								<div class="item" data-value="2">Jatuh Tempo Tgl 15  Bulan Berikutnya</div>
								<div class="item" data-value="3">Jatuh Tempo Di Akhir Bulan Berikutnya</div>
								<div class="item" data-value="4">Jatuh Tempo Dalam 10 hari</div>
							</div>
						</div>
					</div>

					<div class="field">
						<label>Status Kredit</label>		
						<div class="ui fluid search selection dropdown">
							<input type="hidden" name="status_kredit">
							<i class="dropdown icon"></i>
							<div class="default text">Pilih Status Kredit</div>
							<div class="menu">
								<div class="item" data-value="3">Good History</div>
								<div class="item" data-value="2">In LiQuidation</div>
								<div class="item" data-value="1">No More Work Until Payment Recaived</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//jenis kustomer
		$('[name=jeniskustomer_id]').change(function(e){
			var text = this.options[this.selectedIndex].text;

			if (text.toUpperCase() == "MEMBER") {
				$('.gps_coordinates').addClass('disabled');
				$('input[name="longitude"]').val('');
				$('input[name="latitude"]').val('');
			}else{
				$('.gps_coordinates').removeClass('disabled');
			}
			
		});
		// MemberCard
		$('select[name="membercard_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-membercard/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_pemilik"]').val(response.nama)
				$('input[name="telepon"]').val(response.telepon)
				$('input[name="email"]').val(response.email)
				$('input[name="jeniskustomer_id"]').val(response.jeniskustomer_id)
				$('input[name="jeniskustomer"]').val(response.jeniskustomer)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})
		
		
		// provinsi
		$('[name=provinsi_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kota') }}",
				type: 'GET',
				data:{
					id_provinsi : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kota_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// kota
		$('[name=kota_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kecamatan') }}",
				type: 'GET',
				data:{
					id_kota : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kecamatan_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// kecamatan
		$('[name=kecamatan_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kelurahan') }}",
				type: 'GET',
				data:{
					id_kecamatan : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kelurahan_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// No NPWP
		$('select[name="pajak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-pajak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_npwp"]').val(response.nama_npwp)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    })

	})
</script>