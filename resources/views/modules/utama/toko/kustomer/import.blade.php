{{-- <form action="{{ url('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input type="file" name="upload_data_produk">
	<button type="submit" name="submit" class="ui yellow button">
	Upload
	</button>
</form> --}}

<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Import Data Excel</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ URL::to('utama/toko/kustomer/postimportexcel') }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="field">
			<input type="file" name="kustomer">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon button">
		Upload
		<i class="checkmark icon"></i>
	</div>
</div>