<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Kota</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
	    <input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
	    <div class="field">
			<label>Provinsi</label>
			<select name="provinsi_id" class="ui fluid search selection dropdown" style="width: 100%;">
			   	{!! \Lotte\Models\Master\Provinsi::options('nama', 'id',['selected' => isset($record->provinsi_id) ? $record->provinsi_id : '',], '-- Pilih Provinsi --') !!}
			</select>
		</div>
		<div class="field">
			<label>Nama Kota</label>
			<input name="nama" placeholder="Kota" type="text" value="{{ $record->nama or '' }}">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>