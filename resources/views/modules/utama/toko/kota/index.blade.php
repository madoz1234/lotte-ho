@extends('layouts.grid')

@section('filters')
	<div class="field" style="width: 250px;">
		<select name="filter[provinsi_id]" class="ui fluid search selection dropdown" style="width: 100%;">
		    {!! \Lotte\Models\Master\Provinsi::options('nama', 'id',[], '-- Pilih Provinsi --') !!}
	    </select>
    </div>
	<div class="field">
		<input name="filter[nama]" placeholder="Nama Kota" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection
@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_kota();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
	d.provinsi_id = $("select[name='filter[provinsi_id]']").val();
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		provinsi_id: {
			identifier: 'provinsi_id',
			rules: [{
				type   : 'empty',
				prompt : 'Isian provinsi tidak boleh kosong'
			}]
		},
		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nama tidak boleh kosong'
			}]
		}
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_kota = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-kota') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var provinsi_id = document.createElement("input");
		            provinsi_id.setAttribute("type", "hidden");
		            provinsi_id.setAttribute("name", 'provinsi_id');
		            provinsi_id.setAttribute("value", $('[name="filter[provinsi_id]"]').val());
		        form.appendChild(provinsi_id);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append