
@extends('layouts.grid')

@section('filters')
<div class="field">
	<select name="filter[region_id]" class="ui fluid search selection dropdown" style="width: 100%;">
	    {!! \Lotte\Models\Master\Region::options('area', 'id',[], '-- Pilih Region --') !!}
    </select>
	{{-- <div class="ui fluid search selection dropdown">
		<input type="hidden" name="filter[region]">
		<i class="dropdown icon"></i>
		<div class="default text">Region</div>
		<div class="menu">
			<div class="item" data-value="1">Jabodetabek</div>
			<div class="item" data-value="2">Bandung</div>
			<div class="item" data-value="3">Surabaya</div>
			<div class="item" data-value="4">Banten</div>
		</div>
	</div> --}}
</div>
<div class="field">
	<input type="text" name="filter[kode]" placeholder="Kode LSI">
</div>
<div class="field">
	<input type="text" name="filter[nama]" placeholder="Nama LSI">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

{{-- @section('js-filters')
    d.region_id = $("select[name='filter[region_id]']").val();
    d.nama = $("input[name='filter[nama]']").val();
    d.jenis = $("input[name='filter[jenis]']").val();
@endsection --}}

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_lsi();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.region_id = $("select[name='filter[region_id]']").val();
    d.kode = $("input[name='filter[kode]']").val();
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		region_id: {
			identifier: 'region_id',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Region tidak boleh kosong'
			}]
		},
		kode: {
			identifier: 'kode',
			rules: [{
				type   : 'empty',
				prompt : 'Isian kode LSI tidak boleh kosong'
			},{
			type   : 'minLength[5]',
			prompt : 'Isian kode LSI setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[10]',
				prompt : 'Isian kode LSI seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nama LSI tidak boleh kosong'
			},{
			type   : 'minLength[3]',
			prompt : 'Isian nama LSI setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[50]',
				prompt : 'Isian nama LSI seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		alamat: {
			identifier: 'alamat',
			rules: [{
				type   : 'empty',
				prompt : 'Isian alamat tidak boleh kosong'
			},{
			type   : 'minLength[5]',
			prompt : 'Isian alamat setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[255]',
				prompt : 'Isian alamat seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		provinsi_id: {
			identifier: 'provinsi_id',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Provinsi tidak boleh kosong'
			}]
		},
		kota_id: {
			identifier: 'kota_id',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Kota tidak boleh kosong'
			}]
		},
		// kode_pos: {
		// 	identifier: 'kode_pos',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian kode Pos tidak boleh kosong'
		// 	}]
		// },
		telepon: {
			identifier: 'telepon',
			rules: [{
				type   : 'empty',
				prompt : 'Isian telepon tidak boleh kosong'
			},{
			type   : 'minLength[2]',
			prompt : 'Isian telepon setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Isian telepon seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		// email: {
		// 	identifier: 'email',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian Email tidak boleh kosong'
		// 	},{
		// 	type   : 'minLength[5]',
		// 	prompt : 'Isian email setidaknya harus memiliki {ruleValue} karakter'
		// 	},{
		// 		type   : 'maxLength[50]',
		// 		prompt : 'Isian email seharusnya berjumlah {ruleValue} karakter'
		// 	}]
		// },
		longitude: {
			identifier: 'longitude',
			rules: [{
				type   : 'empty',
				prompt : 'Isian longitude tidak boleh kosong'
			},{
			type   : 'minLength[5]',
			prompt : 'Isian longitude setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[10]',
				prompt : 'Isian longitude seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		latitude: {
			identifier: 'latitude',
			rules: [{
				type   : 'empty',
				prompt : 'Isian latitude tidak boleh kosong'
			},{
			type   : 'minLength[5]',
			prompt : 'Isian latitude setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[10]',
				prompt : 'Isian latitude seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		pajak_id: {
			identifier: 'pajak_id',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nama perusahaan tidak boleh kosong'
			}]
		},
		nama_npwp: {
			identifier: 'nama_npwp',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nomor npwp tidak boleh kosong'
			},{
			type   : 'minLength[5]',
			prompt : 'Isian nomor npwp setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[50]',
				prompt : 'Isian nomor npwp seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		alamat_perusahaan: {
			identifier: 'alamat_perusahaan',
			rules: [{
				type   : 'empty',
				prompt : 'Isian alamat perusahaan tidak boleh kosong'
			},{
			type   : 'minLength[5]',
			prompt : 'Isian alamat perusahaan setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[255]',
				prompt : 'Isian alamat perusahaan seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		rekening_escrow_id: {
			identifier: 'rekening_escrow_id',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan akun bank tidak boleh kosong'
			}]
		},
		bank_escrow_id: {
			identifier: 'bank_escrow_id',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan id tidak boleh kosong'
			}]
		},
		kode_bank: {
			identifier: 'kode_bank',
			rules: [{
				type   : 'empty',
				prompt : 'Isian kode bank tidak boleh kosong'
			}]
		},
		nama_bank: {
			identifier: 'nama_bank',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nama bank tidak boleh kosong'
			}]
		},
		nomor_rekening: {
			identifier: 'nomor_rekening',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nomor rekening tidak boleh kosong'
			},{
			type   : 'minLength[5]',
			prompt : 'Isian nomor rekening setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[20]',
				prompt : 'Isian nomor rekening seharusnya berjumlah {ruleValue} karakter'
			}]
		}
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_lsi = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-lsi') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var kode = document.createElement("input");
		            kode.setAttribute("type", "hidden");
		            kode.setAttribute("name", 'kode');
		            kode.setAttribute("value", $('[name="filter[kode]"]').val());
		        form.appendChild(kode);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append

@section('init-modal')
	<script type="text/javascript">
		initModal = function(){
			$('[name=provinsi_id]').change(function(e){
				$.ajax({
					url: "{{ url('ajax/option/kota') }}",
					type: 'GET',
					data:{
						id_provinsi : $(this).val(),
					},
				})
				.success(function(response) {
					$('.kota').removeClass('disabled');
					$('[name=kota_id]').html(response);
				})
				.fail(function() {
					console.log("error");
				});
			});
		};
	</script>
@endsection