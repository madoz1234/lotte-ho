<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data LSI</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
	    <input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui equal width grid">
			<div class="column">
				<h4 class="ui dividing header">Data LSI</h4>
				<div class="field">
					<label>Region</label>
					<select name="region_id" class="ui fluid search selection dropdown" style="width: 100%;">
			    	    {!! \Lotte\Models\Master\Region::options('area', 'id',['selected' => isset($record->region_id) ? $record->region_id : '',], '-- Pilih Region --') !!}
				    </select>
				</div>
				<div class="field">		
					<div class="fields">
						<div class="four wide field">
							<label>Kode LSI</label>
							<input type="text" name="kode" placeholder="Kode LSI" value="{{ $record->kode or "" }}">
							<div class="ui error message"></div>
						</div>
						<div class="twelve wide field">
							<label>Nama LSI</label>
							<input type="text" name="nama" placeholder="Nama LSI" value="{{ $record->nama or "" }}">
						</div>
					</div>
				</div>
				<div class="field">
					<label>Alamat</label>
					<div class="field">
						<div class="field">
							<input type="text" name="alamat" placeholder="Alamat Jalan" value="{{ $record->alamat or "" }}">
						</div>
					</div>
					<div class="three fields">
						<div class="field">
							<label class="hidden">Provinsi</label>
							<select name="provinsi_id" class="ui fluid search selection dropdown" style="width: 100%;">
					    	    {!! \Lotte\Models\Master\Provinsi::options('nama', 'id',['selected' => isset($record->kota_id) ? $record->kota_id : '',], '-- Pilih Provinsi --') !!}
						    </select>
						</div>
						<div class="field">
							<label class="hidden">Kota</label>
							<select name="kota_id" class="ui fluid search selection dropdown" style="width: 100%;">
					    	    {!! \Lotte\Models\Master\Kota::options('nama', 'id',['selected' => isset($record->kota_id) ? $record->kota_id : '',], '-- Pilih Kota --') !!}
						    </select>
						</div>
						<div class="field">
							<label class="hidden">Kode Pos</label>
							<div class="">
								<input type="text" name="kode_pos" placeholder="Kode Pos" value="{{ $record->kode_pos or "" }}">
							</div>
						</div>
					</div>
				</div>
				<div class="two fields">
					<div class="field">
						<label>Telepon</label>
						<input type="text" name="telepon" placeholder="Telepon" value="{{ $record->telepon or "" }}">
					</div>
					<div class="field">
						<label>Email</label>
						<input type="text" name="email" placeholder="Email" value="{{ $record->email or "" }}">
					</div>
				</div>
				<div class="field">
					<label>GPS Coordinates</label>
					<div class="two fields">
						<div class="field">
							<input type="text" name="latitude" placeholder="Latitude" value="{{ $record->latitude or "" }}">
						</div>
						<div class="field">
							<input type="text" name="longitude" placeholder="Longitude" value="{{ $record->longitude or "" }}">
						</div>
					</div>
				</div>
			</div>
			<div class="column">
				<h4 class="ui dividing header">Keuangan &amp; Pajak</h4>
				<div class="field">
					{{-- <div class="two fields">
						<div class="field">
							<label>NPWP</label>
							<select name="pajak_id" class="ui fluid search selection dropdown" style="width: 100%;">
					    	    {!! \Lotte\Models\Master\Pajak::options('npwp', 'id',['selected' => isset($record->pajak_id) ? $record->pajak_id : '',], '-- Pilih No NPWP --') !!}
						    </select>
						</div>
						<div class="field">
							<label>Nama Perusahaan</label>
								<input type="text" style="background-color: #edf1f6;" name="nama_perusahaan" placeholder="Nama Pemilik" value="{{ $record->pajak->nama or "" }}"	readonly="">
						</div>
					</div> --}}

					<div class="field">
						<div class="two fields">
							<div class="field">
								<label>Nama Perusahaan</label>
								<select name="pajak_id" class="ui fluid search selection dropdown" style="width: 100%;">
									{{-- {!! \Lotte\Models\Master\Pajak::options('nama', 'id',[], '-- Pilih Nama Perusahaan --') !!} --}}
									{!! \Lotte\Models\Master\Pajak::options('nama', 'id',['selected' => isset($record->pajak_id) ? $record->pajak_id : '',], '-- Pilih Nama Perusahaan --') !!}
								</select>
							</div>
							<div class="field">
								<label>NPWP</label>
								<input type="text" style="background-color: #edf1f6;" name="nama_npwp" placeholder="Nomor Npwp" value="{{ $record->pajak->npwp or "" }}"	readonly="">
							</div>
						</div>
					</div>


					{{-- <div class="field">
						<label>Alamat</label>
						<input type="text" name="alamat_perusahaan" placeholder="Alamat Perusahaan (Sesuai NPWP)" value="{{ $alamat_perusahaan or "" }}">
					</div> --}}
				</div>
				<div class="field">
					<label>Akun Bank</label>
					<select name="rekening_escrow_id" class="ui fluid search selection dropdown" style="width: 100%;">
			    	    {!! \Lotte\Models\Master\RekeningEscrow::options('nama_pemilik', 'id',['selected' => isset($record->rekening_escrow_id) ? $record->rekening_escrow_id : '',], '-- Pilih Akun Escrow --') !!}
				    </select>
				</div>
				<div class="field">
					<div class="two fields">
							<input type="hidden" style="background-color: #edf1f6;" name="bank_escrow_id" placeholder="Id" value="{{ $record->bank_escrow_id or "" }}" readonly="">
						<div class="field">
							<label>Kode Bank</label>
							<input type="text" style="background-color: #edf1f6;" name="kode_bank" placeholder="Kode Bank" value="{{ $record->bankescrow->kode_bank or "" }}" readonly="">
						</div>
						<div class="field">
							<label>Nama Bank</label>
							<input type="text" style="background-color: #edf1f6;" name="nama_bank" placeholder="Nama Bank" value="{{ $record->bankescrow->nama or "" }}" readonly="">
						</div>
					</div>
				</div>


				<div class="field">
					<label>Nomor Rekening</label>
					<input type="text" style="background-color: #edf1f6;" name="nomor_rekening" placeholder="Nomor Rekening Bank" value="{{ $record->rekeningescrow->nomor_rekening or "" }}" readonly="">
				</div>
			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('[name=provinsi_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kota') }}",
				type: 'GET',
				data:{
					id_provinsi : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kota_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// No NPWP
		$('select[name="pajak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-pajak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_npwp"]').val(response.nama_npwp)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nomor_rekening"]').val(response.nomor_rekening)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="bank_escrow_id"]').val(response.bank_escrow_id)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_bank"]').val(response.nama_bank)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="kode_bank"]').val(response.kode_bank)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Bank Escrow
		$('select[name="bank_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-bank/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="kode_bank"]').val(response.kode_bank)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})
	})
</script>