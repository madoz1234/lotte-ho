<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data TMUK</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="ui grid">
			<div class="eleven wide column">
				<h4 class="ui dividing header">Data TMUK</h4>
				<div class="ui equal width grid">
					<div class="column">
						<div class="field">
							<label>LSI Induk</label>
							<select name="lsi_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Lsi::options('nama', 'id',[], '-- Pilih LSI --') !!}
							</select>
						</div>
						<div class="field">		
							<div class="fields">
								<div class="four wide field">
									<label>Kode TMUK</label>
									<input type="text" name="kode" placeholder="Kode TMUK" value="{{ $kode or "" }}">
								</div>
								<div class="twelve wide field">
									<label>Nama TMUK</label>
									<input type="text" name="nama" placeholder="Nama TMUK" value="{{ $nama or "" }}">
								</div>
							</div>
						</div>
						<div class="field">
							<label>Alamat</label>
							<div class="field">
								<div class="field">
									<input type="text" name="alamat" placeholder="Alamat Jalan" value="{{ $alamat or "" }}">
								</div>
							</div>
							<div class="four fields">
								<div class="field">
									<label class="hidden">Provinsi</label>
									<select name="provinsi_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\Provinsi::options('nama', 'id',[], 'Provinsi') !!}
									</select>
								</div>
								<div class="field">
									<label class="hidden">Kota</label>
									<select name="kota_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\Kota::options('nama', 'id',[], 'Kota') !!}
									</select>
								</div>
								<div class="field">
									<label class="hidden">Kecamatan</label>
									<select name="kecamatan_id" class="ui fluid search selection dropdown" style="width: 100%;">
										{!! \Lotte\Models\Master\Kecamatan::options('nama', 'id',[], 'Kecamatan') !!}
									</select>
								</div>
								<div class="four wide field">
									<input class="length" maxlength="5" type="text" name="kode_pos" placeholder="Kode Pos" value="{{ $kode_pos or "" }}">
								</div>
							</div>
						</div>
						<div class="two fields">
							<div class="field">
								<label>Telepon</label>
								<input type="text" name="telepon" placeholder="Telepon" value="{{ $telepon or "" }}">
							</div>
							<div class="field">
								<label>Email</label>
								<input type="text" name="email" placeholder="Email" value="{{ $email or "" }}">
							</div>
						</div>
						<div class="two fields">
							<div class="field">
								<label>ASN</label>
								<div class="inline fields">
									<div class="field">
										<div class="ui radio checkbox">
											<input name="asn" tabindex="0" class="" type="radio" value="1">
											<label>Ya</label>
										</div>
									</div>
									<div class="field">
										<div class="ui radio checkbox">
											<input name="asn" tabindex="0" class="" type="radio" value="0">
											<label>Tidak</label>
										</div>
									</div>
								</div>
							</div>
							<div class="field">
								<label>Auto Approve</label>
								<div class="inline fields">
									<div class="field">
										<div class="ui radio checkbox">
											<input name="auto_approve" tabindex="0" class="" type="radio" value="1">
											<label>Ya</label>
										</div>
									</div>
									<div class="field">
										<div class="ui radio checkbox">
											<input name="auto_approve" tabindex="0" class="" type="radio" value="0">
											<label>Tidak</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="field">
							<label>GPS Coordinates</label>
							<div class="two fields">
								<div class="field">
									<input type="text" name="latitude" placeholder="Latitude" maxlength="10" value="{{ $latitude or "" }}">
								</div>
								<div class="field">
									<input type="text" name="longitude" placeholder="Longitude" maxlength="10" value="{{ $longitude or "" }}">
								</div>
							</div>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<label>No Member Card</label>
							<select name="membercard_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{{-- {!! \Lotte\Models\Master\MemberCard::options('nomor', 'id',[], 'No MemberCard') !!} --}}

								{!! \Lotte\Models\Master\MemberCard::options('nomor', 'id',[
									'selected' => isset($record->membercard_id) ? $record->membercard_id : '', 
									'filters' => [function($query){ $query->whereIn('jeniskustomer_id', ['1']); }]], '-- Pilih Type --') !!}

								</select>
							</div>
							<div class="two fields">
								<div class="field">
									<label>Nama Pemilik</label>
									<input type="text" style="background-color: #edf1f6;" name="nama_pemilik" placeholder="Nama Pemilik" value="{{ $nama_pemilik or "" }}"	readonly="">
								</div>
								<div class="field">
									<label>Jenis Kustomer</label>
									<input type="text" style="background-color: #edf1f6;" name="jenis_kustomer" placeholder="Jenis Kostumer" value="{{ $jenis_kustomer or "" }}" readonly="">
								</div>
							</div>
							<div class="field">
								<label for="jenis_assortment_id">Assortment Type</label>
								<select name="jenis_assortment_id" class="ui fluid search selection dropdown" style="width: 100%;">
									{!! \Lotte\Models\Master\JenisAssortment::options('nama', 'id',[], 'Assortment Type') !!}
								</select>
							</div>
							<div class="two fields">
								<div class="field">
									<label>Gross Area</label>
									<div class="ui right labeled input">
										<input placeholder="Gross Area" name="gross_area" type="text" value="{{ $gross_area or "" }}">
										<div class="ui basic label">
											m²
										</div>
									</div>
								</div>
								<div class="field">
									<label>Selling Area</label>
									<div class="ui right labeled input">
										<input placeholder="Selling Area" name="selling_area" type="text" value="{{ $selling_area or "" }}">
										<div class="ui basic label">
											m²
										</div>
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field">
									<label>Nama CDE</label>
									<input type="text" name="nama_cde" placeholder="Nama CDE" value="{{ $nama_cde or "" }}">
								</div>
								<div class="field">
									<label>Email CDE</label>
									<input type="email" name="email_cde" placeholder="Email CDE" value="{{ $email_cde or "" }}">
								</div>
							</div>
							<div class="two fields">
								<div class="field">
									<label>Rencana Pembukaan Toko</label>		
									<div class="ui calendar" id="from">
										<div class="ui input left icon">
											<i class="calendar icon date"></i>
											<input name="rencana_pembukaan" type="text" placeholder="Rencana Pembukaan Toko" value="{{ $rencana_pembukaan or "" }}">
										</div>
									</div>
								</div>
								<div class="field">
									<label>Aktual Pembukaan Toko</label>		
									<div class="ui calendar" id="from">
										<div class="ui input left icon">
											<i class="calendar icon date"></i>
											<input name="aktual_pembukaan" type="text" placeholder="Aktual Pembukaan Toko" value="{{ $aktual_pembukaan or "" }}">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="five wide column">
					<h4 class="ui dividing header">Keuangan &amp; Pajak</h4>
				{{-- <div class="field">
					<div class="two fields">
						<div class="field">
							<label>NPWP</label>
							<select name="pajak_id" class="ui fluid search selection dropdown" style="width: 100%;">
					    	    {!! \Lotte\Models\Master\Pajak::options('npwp', 'id',[], '-- Pilih No NPWP --') !!}
						    </select>
						</div>
						<div class="field">
							<label>Nama Perusahaan</label>
								<input type="text" style="background-color: #edf1f6;" name="nama_perusahaan" placeholder="Nama Pemilik" value="{{ $nama_perusahaan or "" }}"	readonly="">
						</div>
					</div>
				</div> --}}

				<div class="field">
					<div class="two fields">
						<div class="field">
							<label>Nama Perusahaan</label>
							<select name="pajak_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Pajak::options('nama', 'id',[], '-- Pilih Nama Perusahaan --') !!}
							</select>
						</div>
						<div class="field">
							<label>NPWP</label>
							<input type="text" style="background-color: #edf1f6;" name="nama_npwp" placeholder="Nomor Npwp" value="{{ $npwp or "" }}"	readonly="">
						</div>
					</div>
				</div>

				<div class="field">
					<div class="field">
						<label>Alamat</label>
						<input type="text" style="background-color: #edf1f6;" name="nama_alamat" placeholder="Alamat Npwp" value="{{ $alamat or "" }}"	readonly="">
					</div>
				</div>

				<h4 class="ui dividing header"></h4>

				{{-- == --}}
				<div class="field">
					<div class="two fields">
						<div class="field">
							<label>Akun Escrow Bank</label>
							<select name="rekening_escrow_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\RekeningEscrow::options('nama_pemilik', 'id',['filters' => ['status_rekening' => 0]], '-- Pilih Akun Bank --') !!}
							</select>
						</div>
						<input type="hidden" style="background-color: #edf1f6;" name="bank_escrow_id" placeholder="Id" value="{{ $bank_escrow_id or "" }}" readonly="">
						<div class="field">
							<label>Kode Bank</label>
							<input type="text" style="background-color: #edf1f6;" name="kode_bank" placeholder="Kode Bank" value="{{ $kode_bank or "" }}" readonly="">
						</div>
					</div>
				</div>
				<div class="field">
					<div class="two fields">
						<div class="field">
							<label>Nama Bank</label>
							<input type="text" style="background-color: #edf1f6;" name="nama_bank" placeholder="Nama Bank" value="{{ $bank_escrow_id or "" }}" readonly="">
						</div>
						<div class="field">
							<label>Nomor Rekening</label>
							<input type="text" style="background-color: #edf1f6;" name="nomor_rekening" placeholder="Nomor Rekening Bank" value="{{ $nomor_rekening or "" }}" readonly="">
						</div>
					</div>
				</div>
				{{-- == --}}
			{{-- 	<div class="field">
					<label>Akun Escrow Bank</label>
					<select name="rekening_escrow_id" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\RekeningEscrow::options('nama_pemilik', 'id',['filters' => ['status_rekening' => 0]], '-- Pilih Akun Bank --') !!}
					</select>
				</div>
				<div class="field">
					<div class="two fields">
						<input type="hidden" style="background-color: #edf1f6;" name="bank_escrow_id" placeholder="Id" value="{{ $bank_escrow_id or "" }}" readonly="">
						<div class="field">
							<label>Kode Bank</label>
							<input type="text" style="background-color: #edf1f6;" name="kode_bank" placeholder="Kode Bank" value="{{ $kode_bank or "" }}" readonly="">
						</div>
						<div class="field">
							<label>Nama Bank</label>
							<input type="text" style="background-color: #edf1f6;" name="nama_bank" placeholder="Nama Bank" value="{{ $bank_escrow_id or "" }}" readonly="">
						</div>
					</div>
				</div>
				<div class="field">
					<label>Nomor Rekening</label>
					<input type="text" style="background-color: #edf1f6;" name="nomor_rekening" placeholder="Nomor Rekening Bank" value="{{ $nomor_rekening or "" }}" readonly="">
				</div> --}}
				{{-- == --}}
				<h4 class="ui dividing header"></h4>
				<div class="field">
					<div class="two fields">
						<input type="hidden" style="background-color: #edf1f6;" name="bank_pemilik_id" placeholder="Id" value="{{ $bank_pemilik_id or "" }}" readonly="">
						<div class="field">
							<label>Akun Pribadi Pemilik</label>
							<select name="rekening_pemilik_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\RekeningEscrow::options('nama_pemilik', 'id',[], '-- Pilih Akun Pemilik --') !!}
							</select>
						</div>
						<div class="field">
							<label>Kode Bank</label>
							<input type="text" style="background-color: #edf1f6;" name="kode_bank_pemilik" placeholder="Kode Bank" value="{{ $kode_bank_pemilik or "" }}" readonly="">
						</div>
					</div>
				</div>
				<div class="field">
					<div class="two fields">
						<input type="hidden" style="background-color: #edf1f6;" name="bank_pemilik_id" placeholder="Id" value="{{ $bank_pemilik_id or "" }}" readonly="">
						<div class="field">
							<label>Nama Bank</label>
							<input type="text" style="background-color: #edf1f6;" name="nama_bank_pemilik" placeholder="Kode Bank" value="{{ $bank_pemilik_id or "" }}" readonly="">
						</div>
						<div class="field">
							<label>Nomor Rekening</label>
							<input type="text" style="background-color: #edf1f6;" name="nomor_rekening_pemilik" placeholder="Nomor Rekening Bank" value="{{ $nomor_rekening_pemilik or "" }}" readonly="">
						</div>
					</div>
				</div>
				{{-- == --}}
			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('[name=provinsi_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kota') }}",
				type: 'GET',
				data:{
					id_provinsi : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kota_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// membercard
		$('select[name="membercard_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_pemilik"]').val(response.nama),
				$('input[name="jenis_kustomer"]').val(response.jeniskustomer)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// No NPWP
		$('select[name="pajak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-pajak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_npwp"]').val(response.nama_npwp)
				$('input[name="nama_alamat"]').val(response.nama_alamat)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// provinsi
		$('[name=provinsi_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kota') }}",
				type: 'GET',
				data:{
					id_provinsi : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kota_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// kota
		$('[name=kota_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kecamatan') }}",
				type: 'GET',
				data:{
					id_kota : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kecamatan_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nomor_rekening"]').val(response.nomor_rekening)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="bank_escrow_id"]').val(response.bank_escrow_id)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_bank"]').val(response.nama_bank)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="kode_bank"]').val(response.kode_bank)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Bank Escrow
		// $('select[name="bank_escrow_id"]').on('change', function(){
		// 	$.ajax({
		// 		url: "{{url($pageUrl)}}/on-change-pop-bank/"+this.value,
		// 		type: 'GET',
		// 		dataType: 'json',
		// 	})
		// 	.done(function(response) {
		// 		// console.log(response);
		// 		$('input[name="kode_bank"]').val(response.kode_bank)
		// 	})
		// 	.fail(function() {
		// 		console.log("error");
		// 	})
		// 	.always(function() {
		// 		console.log("complete");
		// 	});
		// })

		// ==
		// Rekening Pemilik
		$('select[name="rekening_pemilik_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening-pemilik/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nomor_rekening_pemilik"]').val(response.nomor_rekening_pemilik)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Pemilik
		$('select[name="rekening_pemilik_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening-pemilik/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="bank_pemilik_id"]').val(response.bank_pemilik_id)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Pemilik
		$('select[name="rekening_pemilik_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening-pemilik/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_bank_pemilik"]').val(response.nama_bank_pemilik)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Pemilik
		$('select[name="rekening_pemilik_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening-pemilik/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="kode_bank_pemilik"]').val(response.kode_bank_pemilik)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		var inputQuantity = [];
		$(function() {
			$(".length").each(function(i) {
				inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
     });
			$(".length").on("keyup", function (e) {
				var $field = $(this),
				val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            	this.value = inputQuantity[$thisIndex];
            	return;
            } 
            if (val.length > Number($field.attr("maxlength"))) {
            	val=val.slice(0, 5);
            	$field.val(val);
            }
            inputQuantity[$thisIndex]=val;
        });      
		});
		
	})
</script>


