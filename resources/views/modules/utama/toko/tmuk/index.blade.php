@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field">
	{{-- <select name="lsi_id" class="ui fluid search selection dropdown" style="width: 100%;">
		{!! \Lotte\Models\Master\Lsi::options('nama', 'id',[], '-- Pilih Lsi --') !!}
	</select> --}}
	{{-- <div class="ui fluid search selection dropdown">
		<input type="hidden" name="filter[lsi]">
		<i class="dropdown icon"></i>
		<div class="default text">Pilih LSI Induk  </div>
		<div class="menu">
			<div class="item" data-value="1">LOTTE GROSIR PASAR REBO</div>
			<div class="item" data-value="2">LOTTE GROSIR ALAM SUTERA</div>
			<div class="item" data-value="3">LOTTE GROSIR BEKASI</div>
		</div>
	</div> --}}
</div>
<div class="field">
	<input type="text" name="filter[kode]" placeholder="Kode TMUK">
</div>
<div class="field">
	<input type="text" name="filter[nama]" placeholder="Nama TMUK">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
    d.kode = $("input[name='filter[kode]']").val();
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_tmuk();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		lsi_id: {
			identifier: 'lsi_id',
			rules: [{
				type   : 'empty',
				prompt : 'LSI Induk Harus Terisi'
			}]
		},

		kode: {
			identifier: 'kode',
			rules: [{
				type   : 'empty',
				prompt : 'Kode TMUK Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama kode setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[10]',
				prompt : 'Nama kode setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Nama TMUK Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama nama setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Nama nama setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		alamat: {
			identifier: 'alamat',
			rules: [{
				type   : 'empty',
				prompt : 'Alamat Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama alamat setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[255]',
				prompt : 'Nama alamat setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		provinsi_id: {
			identifier: 'provinsi_id',
			rules: [{
				type   : 'empty',
				prompt : 'Provinsi Harus Terisi'
			}]
		},
		kota_id: {
			identifier: 'kota_id',
			rules: [{
				type   : 'empty',
				prompt : 'Kota Harus Terisi'
			}]
		},
		kecamatan_id: {
			identifier: 'kecamatan_id',
			rules: [{
				type   : 'empty',
				prompt : 'Kecamatan Harus Terisi'
			}]
		},
		// kode_pos: {
		// 	identifier: 'kode_pos',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Kode Pos Harus Terisi'
		// 	}]
		// },
		telepon: {
			identifier: 'telepon',
			rules: [{
				type   : 'empty',
				prompt : 'Telepon Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama telepon setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Nama telepon setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		// email: {
		// 	identifier: 'email',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Email Harus Terisi'
		// 	}]
		// },
		// asn: {
		// 	identifier: 'email',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Asn Harus Terisi'
		// 	}]
		// },
		// auto_approve: {
		// 	identifier: 'email',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Auto Approve Harus Terisi'
		// 	}]
		// },
		longitude: {
			identifier: 'longitude',
			rules: [{
				type   : 'empty',
				prompt : 'Longitude Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama longitude setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[10]',
				prompt : 'Nama longitude setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		latitude: {
			identifier: 'latitude',
			rules: [{
				type   : 'empty',
				prompt : 'Latitude Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama latitude setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[10]',
				prompt : 'Nama latitude setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		// nomor: {
		// 	identifier: 'nomor',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'No Member Card No. Harus Terisi'
		// 	},{
		// 		type   : 'minLength[2]',
		// 		prompt : 'Nama nomor setidaknya harus memiliki {ruleValue} karakter'
		// 	},{
		// 		type   : 'maxLength[30]',
		// 		prompt : 'Nama nomor setidaknya harus memiliki {ruleValue} karakter'
		// 	}]
		// },
		asn: {
			identifier: 'asn',
			rules: [{
				type   : 'empty',
				prompt : 'ASN Harus Terisi'
			}]
		},
		auto_approve: {
			identifier: 'auto_approve',
			rules: [{
				type   : 'empty',
				prompt : 'Auto Approve Harus Terisi'
			}]
		},
		membercard_id: {
			identifier: 'membercard_id',
			rules: [{
				type   : 'empty',
				prompt : 'membercard Harus Terisi'
			}]
		},
		nama_pemilik: {
			identifier: 'nama_pemilik',
			rules: [{
				type   : 'empty',
				prompt : 'Nama Pemilik Harus Terisi'
			}]
		},
		jenis_kustomer: {
			identifier: 'jenis_kustomer',
			rules: [{
				type   : 'empty',
				prompt : 'Jenis Kustomer Harus Terisi'
			}]
		},
		jenis_assortment_id: {
			identifier: 'jenis_assortment_id',
			rules: [{
				type   : 'empty',
				prompt : 'Assortment Type Harus Terisi'
			}]
		},
		gross_area: {
			identifier: 'gross_area',
			rules: [{
				type   : 'empty',
				prompt : 'Gross Area Harus Terisi'
			},{
				type   : 'minLength[1]',
				prompt : 'Nama gross area setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[7]',
				prompt : 'Nama gross area setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		selling_area: {
			identifier: 'selling_area',
			rules: [{
				type   : 'empty',
				prompt : 'Selling Area Harus Terisi'
			},{
				type   : 'minLength[1]',
				prompt : 'Nama selling area setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[7]',
				prompt : 'Nama selling area setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		nama_cde: {
			identifier: 'nama_cde',
			rules: [{
				type   : 'empty',
				prompt : 'Nama CDE Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama nama cde setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Nama nama cde setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		email_cde: {
			identifier: 'email_cde',
			rules: [{
				type   : 'empty',
				prompt : 'Email CDE Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama email cde setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Nama email cde setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		rencana_pembukaan: {
			identifier: 'rencana_pembukaan',
			rules: [{
				type   : 'empty',
				prompt : 'Rencana Pembukaan Toko Harus Terisi'
			}]
		},
		// aktual_pembukaan: {
		// 	identifier: 'aktual_pembukaan',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Aktual Pembukaan Toko Harus Terisi'
		// 	}]
		// },
		pajak_id: {
			identifier: 'pajak_id',
			rules: [{
				type   : 'empty',
				prompt : 'NPWP Harus Terisi'
			}]
		},
		nama_npwp: {
			identifier: 'nama_npwp',
			rules: [{
				type   : 'empty',
				prompt : 'Nama npwp Harus Terisi'
			}]
		},
		nama_bank: {
			identifier: 'nama_bank',
			rules: [{
				type   : 'empty',
				prompt : 'Nama bank Harus Terisi'
			}]
		},
		nama_perusahaan: {
			identifier: 'nama_perusahaan',
			rules: [{
				type   : 'empty',
				prompt : 'Nama Perusahaan Harus Terisi'
			}]
		},
		rekening_escrow_id: {
			identifier: 'rekening_escrow_id',
			rules: [{
				type   : 'empty',
				prompt : 'Nama Rekening Harus Terisi'
			}
			// ,{
			// 	type   : 'unique',
			// 	prompt : 'Nama Rekening Tidak Boleh Sama!'
			// }
			]
		},
		bank_escrow_id: {
			identifier: 'bank_escrow_id',
			rules: [{
				type   : 'empty',
				prompt : 'Nama Bank Harus Terisi'
			}]
		},
		nomor_rekening: {
			identifier: 'nomor_rekening',
			rules: [{
				type   : 'empty',
				prompt : 'Nomor Rekening Harus Terisi'
			}]
		},

		rekening_pemilik_id: {
			identifier: 'rekening_pemilik_id',
			rules: [{
				type   : 'empty',
				prompt : 'Nomor Rekening Pemilik Harus Terisi'
			}]
		},
		kode_bank_pemilik: {
			identifier: 'kode_bank_pemilik',
			rules: [{
				type   : 'empty',
				prompt : 'Kode bank pemilik Pemilik Harus Terisi'
			}]
		},
		nama_bank_pemilik: {
			identifier: 'nama_bank_pemilik',
			rules: [{
				type   : 'empty',
				prompt : 'Nama bank pemilik Pemilik Harus Terisi'
			}]
		},
		nomor_rekening_pemilik: {
			identifier: 'nomor_rekening_pemilik',
			rules: [{
				type   : 'empty',
				prompt : 'Nomor rekening pemilik Pemilik Harus Terisi'
			}]
		},
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_tmuk = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-tmuk') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var kode = document.createElement("input");
		            kode.setAttribute("type", "hidden");
		            kode.setAttribute("name", 'kode');
		            kode.setAttribute("value", $('[name="filter[kode]"]').val());
		        form.appendChild(kode);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append