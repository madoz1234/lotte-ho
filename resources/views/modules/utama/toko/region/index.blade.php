@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[kode]" placeholder="Kode Region" type="text">
	</div>
	<div class="field">
		<input name="filter[area]" placeholder="Region" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
	{{-- <button type="button" class="ui green button btn-export" onclick="javascript:export_region();"> Export</button> --}}
@endsection
@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_region();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.kode = $("input[name='filter[kode]']").val();
    d.area = $("input[name='filter[area]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		kode: {
			identifier: 'kode',
			rules: [{
				type   : 'empty',
				prompt : 'Isian kode tidak boleh kosong'
			}]
		},
		area: {
			identifier: 'area',
			rules: [{
				type   : 'empty',
				prompt : 'Isian area tidak boleh kosong'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			}]
		}
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date'
		});
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_region = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-region') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var kode = document.createElement("input");
		            kode.setAttribute("type", "hidden");
		            kode.setAttribute("name", 'kode');
		            kode.setAttribute("value", $('[name="filter[kode]"]').val());
		        form.appendChild(kode);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[area]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append