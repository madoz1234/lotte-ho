<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Region</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
			<input style="background-color: #efefef;" name="kode" placeholder="Kode Region" type="hidden" value="{{ $code }}" readonly>
		<div class="field">
			<label>Nama Region</label>
			<input name="area" placeholder="Region" type="text" value="{{ old('area') }}">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>