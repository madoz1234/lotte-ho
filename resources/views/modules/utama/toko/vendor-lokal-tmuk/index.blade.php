@extends('layouts.grid')

@section('filters')
<div class="field">
	<input type="text" name="filter[kode]" placeholder="Kode Vendor Lokal">
</div>
<div class="field">
	<input type="text" name="filter[nama]" placeholder="Nama Vendor Lokal">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
    d.kode = $("input[name='filter[kode]']").val();
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_vendor_tmuk();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		kode: {
			identifier: 'kode',
			rules: [{
				type   : 'empty',
				prompt : 'Kode Vendor Lokal TMUK Harus Terisi'
			},{
			type   : 'minLength[2]',
			prompt : 'Isian kode Vendor Lokal TMUK setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[5]',
				prompt : 'Isian kode Vendor Lokal TMUK seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		contact_person: {
			identifier: 'contact_person',
			rules: [{
				type   : 'maxLength[30]',
				prompt : 'Contact person setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Nama Vendor Lokal TMUK Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		alamat: {
			identifier: 'alamat',
			rules: [{
				type   : 'empty',
				prompt : 'Alamat Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[255]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		provinsi_id: {
			identifier: 'provinsi_id',
			rules: [{
				type   : 'empty',
				prompt : 'Provinsi Harus Terisi'
			}]
		},
		kota_id: {
			identifier: 'kota_id',
			rules: [{
				type   : 'empty',
				prompt : 'Kota Harus Terisi'
			}]
		},
		kecamatan_id: {
			identifier: 'kecamatan_id',
			rules: [{
				type   : 'empty',
				prompt : 'Kecamatan Harus Terisi'
			}]
		},
		// kode_pos: {
		// 	identifier: 'kode_pos',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Kode Pos Harus Terisi'
		// 	}]
		// },
		telepon: {
			identifier: 'telepon',
			rules: [{
				type   : 'empty',
				prompt : 'Telepon Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[15]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		// email: {
		// 	identifier: 'email',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Email Harus Terisi'
		// 	},{
		// 		type   : 'minLength[2]',
		// 		prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
		// 	},{
		// 		type   : 'maxLength[30]',
		// 		prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
		// 	}]
		// },
		top: {
			identifier: 'top',
			rules: [{
				type   : 'empty',
				prompt : 'TOP Harus Terisi'
			},{
				type   : 'minLength[1]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[3]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		lead_time: {
			identifier: 'lead_time',
			rules: [{
				type   : 'empty',
				prompt : 'Lead Time Harus Terisi'
			},{
				type   : 'minLength[1]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[3]',
				prompt : 'Nama area setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		jadwal_order: {
			identifier: 'jadwal_order[]',
			rules: [{
				type   : 'empty',
				prompt : 'Jadwal Order Harus Terisi'
			}]
		},
		// pajak_id: {
		// 	identifier: 'pajak_id',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Nama Perusahaan Harus Terisi'
		// 	}]
		// },
		// nama_perusahaan: {
		// 	identifier: 'nama_perusahaan',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Nama Perusahaan Harus Terisi'
		// 	}]
		// },
		// rekening_escrow_id: {
		// 	identifier: 'rekening_escrow_id',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Rekening Escrow Harus Terisi'
		// 	}]
		// },
		// bank_escrow_id: {
		// 	identifier: 'bank_escrow_id',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Bank Escrow Harus Terisi'
		// 	}]
		// },
		// kode_bank: {
		// 	identifier: 'kode_bank',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Kode Bank Harus Terisi'
		// 	}]
		// },
		// nama_npwp: {
		// 	identifier: 'nama_npwp',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Nama npwp Harus Terisi'
		// 	}]
		// },
		// nomor_rekening: {
		// 	identifier: 'nomor_rekening',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Nomor Rekening Harus Terisi'
		// 	}]
		// },
		// nama_bank: {
		// 	identifier: 'nama_bank',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Nama bank Harus Terisi'
		// 	}]
		// },
		tmuk_id: {
			identifier: 'tmuk_id[]',
			rules: [{
				type   : 'empty',
				prompt : 'TMUK Pelanggan Harus Terisi'
			}]
		}
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_vendor_tmuk = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-vendor-tmuk') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var kode = document.createElement("input");
		            kode.setAttribute("type", "hidden");
		            kode.setAttribute("name", 'kode');
		            kode.setAttribute("value", $('[name="filter[kode]"]').val());
		        form.appendChild(kode);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append