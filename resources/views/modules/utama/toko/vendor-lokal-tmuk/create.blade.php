<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Vendor Lokal TMUK</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui equal width grid">
			<div class="column">
				<h4 class="ui dividing header">Data Vendor Lokal TMUK</h4>
				<div class="field">		
					<div class="fields">
						<div class="eight wide field">
							<label>Kode Vendor Lokal TMUK</label>
							<input type="text" name="kode" placeholder="Kode Supplier" value="{{ $kode or "" }}" maxlength="5" onkeypress="return isNumberKey(event)">
							{{-- <input style="background-color: #efefef;" name="kode" placeholder="Kode Supplier" type="text" value="{{ $code }}" readonly> --}}
						</div>
						{{-- <div class="eight wide field">
							<label>Nama Vendor Lokal TMUK</label>
							<input type="text" name="nama" placeholder="Nama Vendor Lokal" value="VendorLokal-{{ $nama or "" }}">
						</div> --}}
						<div class="eight wide field">
							<label>Nama Vendor Lokal TMUK</label>
							<div class="ui left labeled input">
								<div class="ui basic label">
									Vendor Lokal -
								</div>
								<input type="text" name="nama" placeholder="Nama Vendor Lokal" value="{{ $nama or "" }}">
							</div>
						</div>
					</div>
				</div>
				<div class="field">
					<label>Alamat</label>
					<div class="field">
						<div class="field">
							<input type="text" name="alamat" placeholder="Alamat Jalan" value="{{ $alamat or "" }}">
						</div>
					</div>
					<div class="four fields">
						<div class="field">
							<label class="hidden">Provinsi</label>
							<select name="provinsi_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Provinsi::options('nama', 'id',[], '-- Pilih Provinsi --') !!}
							</select>
						</div>
						<div class="field">
							<label class="hidden">Kota</label>
							<select name="kota_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Kota::options('nama', 'id',[], '-- Pilih Kota --') !!}
							</select>
						</div>
						<div class="field">
							<label class="hidden">Kecamatan</label>
							<select name="kecamatan_id" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Kecamatan::options('nama', 'id',[], '-- Pilih Kecamatan --') !!}
							</select>
						</div>
						<div class="field">
							<input class="length" maxlength="5" type="text" name="kode_pos" placeholder="Kode Pos" value="{{ $kode_pos or "" }}">
						</div>
					</div>
				</div>
				<div class="two fields">
					{{-- <div class="field">
						<label>Nama Kontak</label>
						<input type="text" name="kontak" placeholder="Nama Kontak" value="{{ $kontak or "" }}">
					</div> --}}
					<div class="field">
						<label>Telepon</label>
						<input type="text" name="telepon" placeholder="Telepon" value="{{ $telepon or "" }}">
					</div>
					<div class="field">
						<label>Email</label>
						<input type="text" name="email" placeholder="Email" value="{{ $email or "" }}">
					</div>
				</div>
				<div class="ui divider"></div>

				<div class="field">		
					<div class="fields">
						<div class="four wide field">
							<label>TOP</label>
							<div class="ui right labeled input">
								<input class="length" maxlength="3" placeholder="TOP" name="top" type="number" value="{{ $top or "" }}">
								<div class="ui basic label">
									hari
								</div>
							</div>
						</div>
						<div class="four wide field">
							<label>Lead Time</label>
							<div class="ui right labeled input">
								<input class="length" maxlength="3" placeholder="Lead Time" name="lead_time" type="number" value="{{ $lead_time or "" }}">
								<div class="ui basic label">
									hari
								</div>
							</div>
						</div>
						<div class="eight wide field">
							<label>Contact Person</label>
							<input type="text" name="contact_person" placeholder="Contact Person" value="{{ $contact_person or "" }}">
						</div>
					</div>
				</div>
				
				<div class="required field">
					<label>Jadwal Order</label>
					<select name="jadwal_order[]" required="" class="ui fluid dropdown" multiple>
						<option value="">Jadwal Order</option>
						<option value="1">Senin</option>
						<option value="2">Selasa</option>
						<option value="3">Rabu</option>
						<option value="4">Kamis</option>
						<option value="5">Jumat</option>
						<option value="6">Sabtu</option>
						<option value="7">Minggu</option>
					</select>
				</div>
				
			</div>
			<div class="column">
				<h4 class="ui dividing header">Keuangan &amp; Pajak</h4>
				<div class="two fields">
					<div class="field">
						<label>Nama Perusahaan</label>
						<select name="pajak_id" class="ui fluid search selection dropdown" style="width: 100%;">
							<option></option>
							<option value="0">Tidak Memiliki NPWP</option>
							{!! \Lotte\Models\Master\Pajak::options('nama', 'id',[], '-- Pilih Nama Perusahaan --') !!}
						</select>
					</div>
					<div class="field">
						<label>NPWP</label>
						<input type="text" style="background-color: #edf1f6;" name="nama_npwp" placeholder="Nomor Npwp" value="{{ $npwp or "" }}"	readonly="">
					</div>
				</div>
				<div class="field">
					<label>Akun Bank</label>
					<select name="rekening_escrow_id" class="ui fluid search selection dropdown" style="width: 100%;">
						<option></option>
						<option value="0">Tidak Memiliki Rekening Bank</option>
						{!! \Lotte\Models\Master\RekeningEscrow::options('nama_pemilik', 'id',[], '-- Pilih Akun Bank --') !!}
						
					</select>
				</div>

				<div class="two fields">
					<input type="hidden" style="background-color: #edf1f6;" name="bank_escrow_id" placeholder="Id" value="{{ $bank_escrow_id or "" }}" readonly="">
					<div class="field">
						<label>Kode Bank</label>
						<input type="text" style="background-color: #edf1f6;" name="kode_bank" placeholder="Kode Bank" value="{{ $kode_bank or "" }}" readonly="">
					</div>
					<div class="field">
						<label>Nama Bank</label>
						<input type="text" style="background-color: #edf1f6;" name="nama_bank" placeholder="Nama Bank" value="{{ $bank_escrow_id or "" }}" readonly="">
					</div>
				</div>

				<div class="field">
					<label>Nomor Rekening</label>
					<input type="text" style="background-color: #edf1f6;" name="nomor_rekening" placeholder="Nomor Rekening Bank" value="{{ $nomor_rekening or "" }}" readonly="">
				</div>
				<h4 class="ui dividing header">TMUK Pelanggan</h4>
				<div class="required field">
					<label>TMUK - TMUK Pelanggan</label>
					<select name="tmuk_id[]" class="ui fluid search dropdown" id="tmuk" multiple>
						{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',[], '-- Pilih TMUK --') !!}
					</select>	
				</div>
				<div id="dtBox"></div>
				<div class="ui toggle checkbox">
					<input type="checkbox" id="selectall">
					<label>Select all</label>
				</div>
			</div>
		</div>

		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(e){
		$('.ui.checkbox').checkbox({
			onChecked() {
				const options = $('#tmuk > option').toArray().map(
					(obj) => obj.value
					);
				$('#tmuk').dropdown('set exactly', options);
			},
			onUnchecked() {
				$('#tmuk').dropdown('clear');
			},
		});

		$('[name=provinsi_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kota') }}",
				type: 'GET',
				data:{
					id_provinsi : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kota_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// kota
		$('[name=kota_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kecamatan') }}",
				type: 'GET',
				data:{
					id_kota : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kecamatan_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		// No NPWP
		$('select[name="pajak_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-pajak/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_npwp"]').val(response.nama_npwp)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nomor_rekening"]').val(response.nomor_rekening)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="nama_bank"]').val(response.nama_bank)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Bank Escrow
		$('select[name="bank_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-bank/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="kode_bank"]').val(response.kode_bank)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="bank_escrow_id"]').val(response.bank_escrow_id)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		// Rekening Escrow
		$('select[name="rekening_escrow_id"]').on('change', function(){
			$.ajax({
				url: "{{url($pageUrl)}}/on-change-pop-rekening/"+this.value,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				// console.log(response);
				$('input[name="kode_bank"]').val(response.kode_bank)
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		})

		var inputQuantity = [];
		$(function() {
		$(".length").each(function(i) {
		inputQuantity[i]=this.defaultValue;
		$(this).data("idx",i); // save this field's index to access later
		});
		$(".length").on("keyup", function (e) {
		var $field = $(this),
		val=this.value,
		$thisIndex=parseInt($field.data("idx"),10); // retrieve the 
		if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
		this.value = inputQuantity[$thisIndex];
		return;
		} 
		if (val.length > Number($field.attr("maxlength"))) {
		val=val.slice(0, 3);
		$field.val(val);
		}
		inputQuantity[$thisIndex]=val;
		});      
		})

	});
</script>