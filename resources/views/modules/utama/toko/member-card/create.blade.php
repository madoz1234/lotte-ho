<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Kartu Anggota</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui error message"></div>

		{{-- <div class="two fields">
			<div class="field">
				<label for="fruit">TMUK</label>
				<select name="tmuk_kode" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\Tmuk::options('nama', 'kode',[], '-- Pilih TMUK --') !!}
				</select>
			</div>
		</div> --}}

		<div class="two fields">
			
			{{-- <div class="field">
				<label for="fruit">Jenis Kostumer</label>
				<select name="jeniskustomer_id" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\JenisKustomer::options('jenis', 'id',[], '-- Pilih Jenis Kustomer --') !!}
				</select>
			</div> --}}
			<div class="field">
				<label>Jenis Kostumer</label>
				<select name="jeniskustomer_id" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\JenisKustomer::options('jenis', 'id',['selected' => isset($record->jeniskustomer_id) ? $record->jeniskustomer_id : '-',], '-- Pilih --') !!}
					</select>
				{{-- <select name="jeniskustomer_id" class="ui fluid search selection dropdown" style="width: 100%;">
					{!! \Lotte\Models\Master\JenisKustomer::options('jenis', 'id',[
						'selected' => isset($record->jeniskustomer_id) ? $record->jeniskustomer_id : '', 
						'filters' => [function($query){ $query->whereIn('kode', ['00001','00002']); }]], '-- Pilih Type --') !!}
					</select> --}}
				</div>
			<div class="field">
				<label>No. Kartu Anggota</label>
				<input class="length" maxlength="20" name="nomor" placeholder="No. Kartu Anggota" type="text" value="{{ $nomor or "" }}">
			</div>
			
		</div>

		<div class="two fields">
			<div class="field">
				<label>Nama Pemilik</label>
				<input name="nama" placeholder="Nama" type="text" value="{{ $nama or ""  }}">
			</div>
			<div class="field">
				<label>Alamat</label>
				<input name="alamat" placeholder="Alamat" type="text" value="{{ $alamat or "" }}">
			</div>
		</div>

		<div class="two fields">
			<div class="field">
				<label>Telepon</label>
				<input name="telepon" placeholder="Telepon" type="text" value="{{ $telepon or ""  }}">
			</div>
			<div class="field">
				<label>Email</label>
				<input name="email" placeholder="Email" type="text" value="{{ $email or ""  }}">
			</div>
		</div>

			<div class="field">
				<label>Catatan</label>
				{{-- <input name="notes" placeholder="Notes" type="text" value="{{ $notes or "" }}"> --}}
				<textarea name="notes" placeholder="Catatan..." rows="2"></textarea>
			</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>


<script type="text/javascript">
	var inputQuantity = [];
	$(function() {
		$(".length").each(function(i) {
			inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
     });
		$(".length").on("keyup", function (e) {
			var $field = $(this),
			val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            	this.value = inputQuantity[$thisIndex];
            	return;
            } 
            if (val.length > Number($field.attr("maxlength"))) {
            	val=val.slice(0, 5);
            	$field.val(val);
            }
            inputQuantity[$thisIndex]=val;
        });      
	});
</script>