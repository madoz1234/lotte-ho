@extends('layouts.grid')

@section('filters')
<div class="field">
	<input type="text" name="filter[nama]" placeholder="Nama Pemilik">
</div>
<div class="field">
	<select name="filter[jeniskustomer_id]" class="ui fluid search selection dropdown" style="width: 100%;">
	    {!! \Lotte\Models\Master\JenisKustomer::options('jenis', 'id',[], '-- Pilih Jenis --') !!}
    </select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
    d.jeniskustomer_id = $("select[name='filter[jeniskustomer_id]']").val();
@endsection
@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_membercard();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nomor: {
			identifier: 'nomor',
			rules: [{
				type   : 'empty',
				prompt : 'No Member Card Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama jenis setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[20]',
				prompt : 'Nama jenis setidaknya harus memiliki {ruleValue} karakter'
			}]
		},
		// limit_kredit: {
		// 	identifier: 'limit_kredit',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'limit_kredit Harus Terisi'
		// 	},{
		// 		type   : 'minLength[2]',
		// 		prompt : 'Limit kredit setidaknya harus memiliki {ruleValue} karakter'
		// 	},{
		// 		type   : 'maxLength[20]',
		// 		prompt : 'Limit kredit setidaknya harus memiliki {ruleValue} karakter'
		// 	}]
		// },

		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Nama Pemilik Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Nama jenis setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Nama jenis setidaknya harus memiliki {ruleValue} karakter'
			}]
		},

		alamat: {
			identifier: 'alamat',
			rules: [{
				type   : 'empty',
				prompt : 'Alamat Harus Terisi'
			},{
				type   : 'minLength[2]',
				prompt : 'Alamat setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[100]',
				prompt : 'Alamat setidaknya harus memiliki {ruleValue} karakter'
			}]
		},


		jeniskustomer_id: {
			identifier: 'jeniskustomer_id',
			rules: [{
				type   : 'empty',
				prompt : 'Jenis Kostumer Harus Terisi'
			}]
		},

		tmuk_id: {
			identifier: 'tmuk_id',
			rules: [{
				type   : 'empty',
				prompt : 'TMUK Harus Terisi'
			}]
		},

		telepon: {
			identifier: 'telepon',
			rules: [{
				type   : 'maxLength[15]',
				prompt : 'Telepon seharusnya berjumlah {ruleValue} karakter'
			}]
		},

		// email: {
		// 	identifier: 'email',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Email Harus Terisi'
		// 	}]
		// },
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_membercard = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-membercard') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        var jeniskustomer_id = document.createElement("input");
					jeniskustomer_id.setAttribute("type", "hidden");
					jeniskustomer_id.setAttribute("name", 'jeniskustomer_id');
					jeniskustomer_id.setAttribute("value", $("select[name='filter[jeniskustomer_id]']").val());
				form.appendChild(jeniskustomer_id);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append