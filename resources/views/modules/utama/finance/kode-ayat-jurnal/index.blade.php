@extends('layouts.grid')

@section('content')

@section('content-header')
<div class="ui breadcrumb">
	<?php $i=1; $last=count($breadcrumb);?>
	@foreach ($breadcrumb as $name => $link)
	@if($i++ != $last)
	<a href="{{ $link }}" class="section">{{ $name }}</a>
	<i class="right chevron icon divider"></i>
	@else
	<div class="active section">{{ $name }}</div>
	@endif
	@endforeach
</div>
<h2 class="ui header">
	<div class="content">
		{!! $title or '-' !!}
		<div class="sub header">{{ $subtitle or ' ' }}</div>
	</div>
</h2>
@show

{{-- <div class="ui segment" style="background: #F9F9F9;">
	<div class="content">
		<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
			{!! csrf_field() !!}
			<input type="hidden" name="_method" value="PUT">
			<input type="hidden" name="id" value="{{ $record->id }}">
			<div class="ui grid">
				<div class="eight wide column">
					<h4 class="ui blue dividing header">General GL</h4>
					<div class="two fields">
						<div class="field">
							<label>Interval Jatuh Tempo</label>
							<div class="ui right labeled input">
								<input placeholder="Interval jatuh tempo" name="interval_jatuh_tempo" type="number" value="{{ $record->interval_jatuh_tempo or '' }}">
								<div class="ui basic label">
									hari
								</div>
							</div>
						</div>
						<div class="field">
							<label>Dimensi Yang Diperlukan</label>
							<div class="ui right labeled input">
								<input placeholder="Dimensi yang diperlukan" name="dimensi_yang_diperlukan" type="number" value="{{ $record->dimensi_yang_diperlukan or '' }}">
								<div class="ui basic label">
									hari
								</div>
							</div>
						</div>
					</div>

					<div class="two fields">
						<div class="field">
							<label>Pendapatan Yang Disimpan</label>
							<input placeholder="Pendapatan yang disimpan" name="pendapatan_yang_disimpan" type="number" value="{{ $record->pendapatan_yang_disimpan or '' }}">
						</div>
						<div class="field">
							<div class="field">
								<label >Laba/Tahun</label>
								<select name="laba_tahun" class="ui fluid search selection dropdown" style="width: 100%;">
									{!! \Lotte\Models\Master\Coa::options('nama', 'id',['selected' => isset($record->laba_tahun) ? $record->laba_tahun : ''], 'Tmuk') !!}
								</select>
							</div>
						</div>
					</div>

					<div class="two fields">
						<div class="field">
							<label>Pertukaran Varian Akun</label>
							<select name="pertukaran_varian_akun" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->pertukaran_varian_akun) ? $record->pertukaran_varian_akun : ''], 'Tmuk') !!}
							</select>
						</div>
						<div class="field">
							<label>Rekening Biaya Bank</label>
							<input placeholder="Rekening biaya bank" name="rekening_biaya_bank" type="number" value="{{ $record->rekening_biaya_bank or '' }}">
						</div>
					</div>

					<h4 class="ui blue dividing header">Pelangan &amp; Penjualan </h4>
					<div class="there fields">
						<div class="field">
							<label>Batas Kredit</label>
							<input placeholder="Batas kredit" name="batas_kredit" type="number" value="{{ $record->batas_kredit or '' }}">
						</div>
						<div class="field">
							<label>Hari Yang Berlaku</label>
							<div class="ui right labeled input">
								<input placeholder="Hari yang berlaku" name="hari_yang_berlaku" type="number" value="{{ $record->hari_yang_berlaku or '' }}">
								<div class="ui basic label">
									hari
								</div>
							</div>
						</div>
						<div class="field">
							<label>Jumlah Hari Pengiriman</label>
							<div class="ui right labeled input">
								<input placeholder="Jumlah hari pengiriman" name="jumlah_hari_pengiriman" type="number" value="{{ $record->jumlah_hari_pengiriman or '' }}">
								<div class="ui basic label">
									hari
								</div>
							</div>
						</div>
					</div>

					<div class="field">
						<label>Dibebankan Kepengirim Akun</label>
						<select name="dibebankan_kepengirim_akun" class="ui fluid search selection dropdown" style="width: 100%;">
							{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->dibebankan_kepengirim_akun) ? $record->dibebankan_kepengirim_akun : ''], 'Tmuk') !!}
						</select>
					</div>

					<div class="two fields">
						<div class="field">
							<label>Rekening Piutang</label>
							<select name="rekening_piutang" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->rekening_piutang) ? $record->rekening_piutang : ''], 'Tmuk') !!}
							</select>
						</div>
						<div class="field">
							<label>Akun Penjualan</label>
							<select name="akun_penjualan1" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->akun_penjualan1) ? $record->akun_penjualan1 : ''], 'Tmuk') !!}
							</select>
						</div>
					</div>

					<div class="two fields">
						<div class="field">
							<label>Akun Diskon Penjualan</label>
							<select name="akun_diskon_penjualan" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->akun_diskon_penjualan) ? $record->akun_diskon_penjualan : ''], 'Tmuk') !!}
							</select>
						</div>
						<div class="field">
							<label>Rekening Diskon Penjualan</label>
							<select name="rekening_diskon_penjualan" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->rekening_diskon_penjualan) ? $record->rekening_diskon_penjualan : ''], 'Tmuk') !!}
							</select>
						</div>
					</div>

					<div class="field">
						<label>Legal Text on Invoice</label>
						<textarea placeholder="legal_text_invoice" name="legal_text_invoice" type="text">{{ $record->legal_text_invoice or '' }}</textarea>
					</div>
				</div>

				<div class="eight wide column">
					<h4 class="ui blue dividing header">Pemasok &amp; Pembelian</h4>
					<div class="two fields">
						<div class="field">
							<label>Pemasok over-Receive</label>
							<div class="ui right labeled input">
								<input placeholder="Pemasok over receive" name="pemasok_over_receive" type="number" value="{{ $record->pemasok_over_receive or '' }}">
								<div class="ui basic label">
									%
								</div>
							</div>
						</div>
						<div class="field">
							<label>Faktur Over-Charge Allowance</label>
							<div class="ui right labeled input">
								<input placeholder="Faktur over charge" name="faktur_over_charge" type="number" value="{{ $record->faktur_over_charge or '' }}">
								<div class="ui basic label">
									%
								</div>
							</div>
						</div>
					</div>

					<div class="two fields">
						<div class="field">
							<label>Rekening Akun</label>
							<select name="rekening_akun" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->rekening_akun) ? $record->rekening_akun : ''], 'Tmuk') !!}
							</select>
						</div>

						<div class="field">
							<label>Pembelian Rekening Diskon</label>
							<select name="pembelian_rekening_diskon" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->pembelian_rekening_diskon) ? $record->pembelian_rekening_diskon : ''], 'Tmuk') !!}
							</select>
						</div>

						<div class="field">
							<label>GRN Clearing Account</label>
							<select name="grn_clearing_account" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->grn_clearing_account) ? $record->grn_clearing_account : ''], 'Tmuk') !!}
							</select>
						</div>
					</div>

					<h4 class="ui blue dividing header">Barang Tetap</h4>
					<div class="two fields">
						<div class="field">
							<label>Akun Penjualan</label>
							<select name="akun_penjualan2" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->akun_penjualan2) ? $record->akun_penjualan2 : ''], 'Tmuk') !!}
							</select>
						</div>

						<div class="field">
							<label>Akun Inventaris</label>
							<select name="akun_inventaris1" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->akun_inventaris1) ? $record->akun_inventaris1 : ''], 'Tmuk') !!}
							</select>
						</div>

						<div class="field">
							<label>Akun COGS</label>
							<select name="akun_cogs" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->akun_cogs) ? $record->akun_cogs : ''], 'Tmuk') !!}
							</select>
						</div>
					</div>
					<div class="two fields">
						<div class="field">
							<label>Akun Penjualan</label>
							<select name="akun_penjualan3" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->akun_penjualan3) ? $record->akun_penjualan3 : ''], 'Tmuk') !!}
							</select>
						</div>

						<div class="field">
							<label>Akun Inventaris</label>
							<select name="akun_inventaris2" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->akun_inventaris2) ? $record->akun_inventaris2 : ''], 'Tmuk') !!}
							</select>
						</div>
					</div>

					<h4 class="ui blue dividing header">Lain</h4>
					<div class="two fields">
						<div class="field">
							<label>Work Order Required By After</label>
							<div class="ui right labeled input">
								<input placeholder="Work order" name="work_order" type="number" value="{{ $record->work_order or '' }}">
								<div class="ui basic label">
									hari
								</div>
							</div>
						</div>

						<div class="field">
							<label>Pembayaran Kembali TMUK</label>
							<input placeholder="pembayaran_kembali_tmuk" name="pembayaran_kembali_tmuk" type="number" value="{{ $record->pembayaran_kembali_tmuk or '' }}">
						</div>

						<div class="field">
							<label>Stock Opname Account</label>
							<select name="stock_opname" class="ui fluid search selection dropdown" style="width: 100%;">
								{!! \Lotte\Models\Master\Tmuk::options('nama', 'id',['selected' => isset($record->stock_opname) ? $record->stock_opname : ''], 'Tmuk') !!}
							</select>
						</div>
					</div>
					<BR><BR>
						<div class="fiveteen wide column main-content">
							<div class="fields">
								<div class="actions" style="margin: auto">
									<div class="ui positive right labeled icon save button" onclick="update()">
										Ubah
										<i class="checkmark icon"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ui error message"></div>
			</form>
		</div>
	</div> --}}

	<div class="ui segment" style="background: #F9F9F9;">
		<div class="content">
			<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
				{!! csrf_field() !!}
				<input type="hidden" name="_method" value="PUT">
				<input type="hidden" name="id" value="{{ $record->id }}">
				<div class="ui compact centered grid">
					<div style="font-weight: bold" class="column row">
						<div style="padding-left:200px;" class="five wide column">AYAT JURNAL</div>
						<div style="text-align: center;" class="four wide column">COA</div>
					</div>
					<div class="column row">
						<div class="five wide column">Aktiva</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Aktiva Lancar</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Kas</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Kas Hasil Penjualan</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Kas Kecil</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>

					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Bank</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Deposit Lotte Grosir</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Rekening SCN</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Rekening Escrow</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>

					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Piutang Usaha</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Piutang Member</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Persediaan</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Persediaan Dry Food</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Persediaan Fresh Food</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Persediaan Non Food</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Persediaan Non Lotte</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Pajak dibayar dimuka</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">PPN Masukan</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">PPH 22</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">PPH 23</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">PPH 25</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Aktiva Lancar Lainnya</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Aktiva Tetap</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Tanah</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Bangunan</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Akumulasi Penyusutan Bangunan</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Peralatan Toko</div>
						
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						
						<div style="padding-left:150px;" class="five wide column">Biaya Perolehan Elektronik</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						
						<div style="padding-left:150px;" class="five wide column">Biaya Perolehan Non Elektronik</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						
						<div style="padding-left:150px;" class="five wide column">Biaya Perolehan Fitting Out</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Akumulasi Penyusutan Peralatan Toko</div>
						
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						
						<div style="padding-left:150px;" class="five wide column">Penyusutan Elektronik</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						
						<div style="padding-left:150px;" class="five wide column">Penyusutan Non Elektronik</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						
						<div style="padding-left:150px;" class="five wide column">Penyusutan Fitting Out</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					{{-- ==2== --}}

					<div class="column row">
						<div class="five wide column">Liabilitas</div>
						
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Utang Jangka Pendek</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Utang Usaha</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Utang Usaha Vendor Lokal</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Utang Lain-lain</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Utang Lain-lain</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Pendapatan Dibayar Dimuka</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Sewa Diterima Dimuka</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Utang Pajak</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Utang PPH 21</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Utang PPH 23</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Utang PPH 25</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Utang PPH 29</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Biaya Masih Harus Dibayar</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Biaya Listrik , air , Telepon</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Utang Jangka Panjang</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Utang Jangka Panjang - Bank</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Utang Jangka Panjang - Lembaga Kredit</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Utang Jangka Panjang - Pihak Ketiga</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					{{-- ==3 --}}
					<div class="column row">
						<div class="five wide column">Ekuitas</div>
						
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Modal</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Modal Disetor</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Tambahan Modal Disetor</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Saldo Laba</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Saldo Laba Tahun Lalu</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Koreksi Saldo Laba</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Saldo Laba Tahun Berjalan</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					{{-- ==4 --}}
					<div class="column row">
						<div class="five wide column">Pendapatan Usaha</div>
						
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">penjualan Barang Dagang</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Penjulan Barang Dagangan - Dry Food</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Diskon Penjulan Barang Dagang</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					{{-- 5== --}}
					<div class="column row">
						<div class="five wide column">Harga Pokok Penjualan</div>
						
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Persediaan Awal</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Persediaan Awal - Dry Food</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">pembelian</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Diskon Pembelian</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Return Pembelian</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Persediaan Akhir</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					{{-- ==6 --}}
					<div class="column row">
						<div class="five wide column">Beban Usaha</div>
						
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Beban Administrasi dan Umum</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Beban Gaji dan Upah</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Gaji dan Upah Pokok</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">THR</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">Komisi dan Bonus</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:150px;" class="five wide column">PPH 21</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Iklan dan Promosi</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Sewa</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Telepon</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Listrik</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Asuransi</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Penyusutan</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:100px;" class="five wide column">Perbaikan dan pemeliharaan</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					{{-- 7== --}}
					<div class="column row">
						<div class="five wide column">Pendapatan Lain-lain</div>
						
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Penghasilan Deviden</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Laba Penjualan Aktiva Tetap</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Penghasilan Sewa</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Penghasilan lainnya</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					{{-- 8== --}}
					<div class="column row">
						<div class="five wide column">Beban Lain-lain</div>
						
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Rugi Penjualan Aktiva Tetap</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Biaya Admin Bank</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
					<div class="column row">
						<div style="padding-left:50px;" class="five wide column">Beban Lainnya</div>
						<div class="four wide column"><select name="" class="ui fluid search selection dropdown" style="width: 100%;">
						{!! \Lotte\Models\Master\Coa::options(function($q){
							return $q->kode.' - '.$q->nama;
							}, 'id',[], '-- Pilih  --') !!}
						</select></div>
					</div>
				</div>
				<div class="ui error message"></div>
			</form>
		</div>
	</div>
	@endsection

	<script type="text/javascript">
		//ajax submit
		update = function($id)
		{            
            // submit form
            $('#dataForm').ajaxSubmit({
            	dataType: 'json',
            	success: function(response){
            		swal(
            			'Tersimpan!',
            			'Data berhasil diubah.',
            			'success'
            			).then(function(e){
            				dt.draw();
            			});
            		},


            		error: function(response){
                    //console.log(response);
                    if (response.status == '422') { // data tidak valid

                    	populateError('#dataForm', response.responseJSON);
                    } else if (response.status == '500') {
                    	console.log('internal server error');
                    }
                }
            });
        }
    </script>