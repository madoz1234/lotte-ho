@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[tipe]" placeholder="Nama Tipe Aset" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_tipe_aset();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.tipe = $("input[name='filter[tipe]']").val();
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	tipe: {
		identifier: 'tipe',
		rules: [{
			type   : 'empty',
			prompt : 'Isian nama tipe aset tidak boleh kosong'
		},{
			type   : 'minLength[5]',
			prompt : 'Isian nama tipe aset setidaknya harus memiliki {ruleValue} karakter'
		},{
			type   : 'maxLength[30]',
			prompt : 'Isian nama tipe aset seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	tingkat_depresiasi: {
		identifier: 'tingkat_depresiasi',
		rules: [{
			type   : 'empty',
			prompt : 'Isian tingkat depresiasi tidak boleh kosong'
		},{
			type   : 'minLength[1]',
			prompt : 'Isian tingkat depresiasi setidaknya harus memiliki {ruleValue} karakter'
		},{
			type   : 'maxLength[5]',
			prompt : 'Isian tingkat depresiasi seharusnya berjumlah {ruleValue} karakter'
		}]
	}
};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_tipe_aset = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-tipe-aset') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var tipe = document.createElement("input");
		            tipe.setAttribute("type", "hidden");
		            tipe.setAttribute("name", 'tipe');
		            tipe.setAttribute("value", $('[name="filter[tipe]"]').val());
		        form.appendChild(tipe);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append