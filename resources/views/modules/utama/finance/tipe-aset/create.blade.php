<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Tipe Aset Terdepresiasi</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="field">
			<label>Tipe Aset Terdepresiasi</label>
			<input name="tipe" placeholder="Tipe Aset Terdepresiasi" type="text" value="{{ $tipe or ""  }}">
		</div>

		<div class="field">
			<label>Tingkat Depresiasi</label>
			<div class="ui right labeled input">
				<input name="tingkat_depresiasi" placeholder="Tingkat Depresiasi" type="number" value="{{ $tingkat_depresiasi or ""  }}">
				<div class="ui label">Bulan</div>
			</div>
		</div>
		<input type="hidden" name="status" value="0">

		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>