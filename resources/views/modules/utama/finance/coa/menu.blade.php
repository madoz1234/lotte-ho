<div class="ui icon fluid input" style="margin-bottom: 15px">
	<input placeholder="Nama Akun" type="text" id="searchbox">
	<i class="circular search link icon searchbtn"></i>
	{{-- <i class="search icon"></i> --}}
</div>
{{-- @foreach ($coa as $val) --}}
<a class="ui fluid teal button" href="#" onclick="showTable('coak_asset_')" style="margin-bottom: 10px">
	<i class="list icon"></i> 
	COA
</a>
{{-- @endforeach --}}
<div class="ui treeview accordion" style="margin-bottom: 50px">
@foreach ($coa as $tipe)
{{-- {{ dd($tipe['tipe_coa_id']) }} --}}
<div class="title" onclick="showTable('tipe_asset', '', {{ $tipe['tipe_coa_id'] }})">
		<i class="dropdown icon"></i> 
		{{ $tipe['nama'] }} <a class="ui red circular tiny label">{{ $tipe['count'] }}</a>
	</div>
	<div class="content">
		<div class="ui accordion">
			@foreach ($tipe['child'] as $val)
			<div class="title" onclick="showTable('parent_asset', '{{ $val['kode'] }}')">
				<i class="dropdown icon"></i>
				{{ $val['nama'] }} <a class="ui red circular tiny label">{{ $val['count'] }}</a>
			</div>
			<div class="content">
				<div class="ui accordion">
					@foreach ($val['child'] as $val1)
					<div class="title" onclick="showTable('child_asset_cash', '{{ $val1['kode'] }}')">
						<i class="dropdown icon"></i>
						{{ $val1['nama'] }} <a class="ui red circular tiny label">{{ $val1['count'] }}</a>
					</div>
					<div class="content">
						<div class="ui vertical text accordion">
							@foreach ($val1['child'] as $val2)
							<div class="title" onclick="showTable('asset_cash_daily', '{{ $val2['kode'] }}')">
								{{-- <i class="dropdown icon"></i> --}}
								{{ $val2['nama'] }}
							</div>
							@endforeach
						</div>
					</div>
					@endforeach
				</div>
			</div>
			@endforeach
		</div>
	</div>
	@endforeach
</div>
{{-- <div class="ui mini blue message" style="position: absolute; bottom: 15px;"> --}}
<div class="ui mini blue message">
	Klik icon <i class="caret right icon"></i> untuk membuka folder, klik <strong>judul</strong> untuk melihat list.
</div>

