<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data COA 4</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="POST">
	<input type="hidden" name="id" value="{{ $record->id or "" }}">
		@if(isset($record))
		<div class="field">
			<label>Kode	COA 3</label>
			<input type="text" name="parent_kode" value="{{ $record->parent_kode or $parent_kode }}" style="background-color: #edf1f6;" readonly>
		</div>
		<div class="field">
			<label>Nama	COA 3</label>
			<input type="text" name="" value="{{ $record->parent->nama or $record->nama }}" style="background-color: #edf1f6;" readonly>
		</div>
		<div class="field">
			<label>Kode	COA 4</label>
			<input type="text" name="kode" placeholder="Kode COA 4" value="{{ $record->kode or "" }}">
		</div>
		<div class="field">
			<label>Nama COA 4</label>
			<input type="text" name="nama" placeholder="Nama COA 4" value="{{ $record->nama or "" }}">
		</div>

		<div class="field">
			<label>Status COA 4</label>
			<div class="inline fields">
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="1" {{ isset($record->status) ? (($record->status == 1) ? 'checked' : '' ): '' }}>
						<label>Aktif</label>
					</div>
				</div>

				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="0" {{ isset($record->status) ? (($record->status == 0) ? 'checked' : '' ): '' }}>
						<label>Non Aktif</label>
					</div>
				</div>
			</div>
		</div>

		@else
			<div class="field">
				<label>Kode	COA 1</label>
				<input type="text" name="parent_kode" value="{{ $parent_kode }}" style="background-color: #edf1f6;" readonly>
			</div>
			<div class="field">
				<label>Nama	COA 1</label>
				<input type="text" name="" value="{{ $parent_nama }}" style="background-color: #edf1f6;" readonly>
			</div>
			<div class="field">
				<label>Kode	COA 2</label>
				<input type="text" name="kode" placeholder="Kode COA 2" value="">
			</div>
			<div class="field">
				<label>Nama COA 2</label>
				<input type="text" name="nama" placeholder="Nama COA 2" value="">
			</div>

			<div class="field">
			<label>Status COA 4</label>
			<div class="inline fields">
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="1" {{ isset($record->status) ? (($record->status == 1) ? 'checked' : '' ): '' }}>
						<label>Aktif</label>
					</div>
				</div>

				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="0" {{ isset($record->status) ? (($record->status == 0) ? 'checked' : '' ): '' }}>
						<label>Non Aktif</label>
					</div>
				</div>
			</div>
		</div>
				
		@endif
		
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>