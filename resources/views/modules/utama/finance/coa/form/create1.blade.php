<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data COA 2</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm">
		<div class="field">
			<label>Tipe Kelas</label>
			<div class="ui fluid search selection dropdown">
				<input type="hidden" name="tipe">
				<i class="dropdown icon"></i>
				<div class="default text">-- Pilih --</div>
				<div class="menu">
					<div class="item" data-value="Assets">Assets</div>
					<div class="item" data-value="Liabilities">Liabilities</div>
					<div class="item" data-value="Equity">Equity</div>
					<div class="item" data-value="Income">Income</div>
					<div class="item" data-value="Cos of Goods Sold">Cos of Goods Sold</div>
					<div class="item" data-value="Espense">Espense</div>
				</div>
			</div>
		</div>
		<div class="field">
			<label>Kode	COA 2</label>
			<input type="number" name="kode_divisi" placeholder="Kode COA 2">
		</div>
		<div class="field">
			<label>Nama COA 2</label>
			<input type="text" name="nama_divisi" placeholder="Nama COA 2">
		</div>
		
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>