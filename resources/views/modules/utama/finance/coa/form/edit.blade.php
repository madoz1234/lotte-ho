<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data COA</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="field">
			<label>Kode	Parent</label>
			<input type="text" name="parent_kode" value="{{ $parent_kode }}" style="background-color: #edf1f6;" readonly>
		</div>
		<div class="field">
			<label>Kode	COA</label>
			<input type="text" name="kode" placeholder="Kode COA" value="{{ $kode or "" }}">
		</div>
		<div class="field">
			<label>Nama COA</label>
			<input type="text" name="nama" placeholder="Nama COA" value="{{ $nama or "" }}">
		</div>
		
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>