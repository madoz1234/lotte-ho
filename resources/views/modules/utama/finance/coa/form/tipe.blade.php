<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data COA 1</div>
<div class="scrolling content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="POST">
		<input type="hidden" name="id" value="{{ $record->id or "" }}">
		@if(isset($record))
		<div class="field">
			<label>Kode	COA 2</label>
			<input type="text" name="tipe_coa_id" value="{{ $record->tipe_coa_id or "" }}" style="background-color: #edf1f6;" readonly>
		</div>
		<div class="field">
			<label>Nama	COA 2</label>
			{{-- <input type="text" name="" value="{{ $record->parent->nama or $record->nama }}" style="background-color: #edf1f6;" readonly> --}}
		</div>
		<div class="field">
			<label>Kode	COA 1</label>
			<input type="text" name="kode" placeholder="Kode COA 1" value="{{ $record->kode or "" }}">
		</div>
		<div class="field">
			<label>Nama COA 1</label>
			<input type="text" name="nama" placeholder="Nama COA 1" value="{{ $record->nama or "" }}">
		</div>

		<div class="field">
			<label>Status COA 3</label>
			<div class="inline fields">
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="1" {{ isset($record->status) ? (($record->status == 1) ? 'checked' : '' ): '' }}>
						<label>Aktif</label>
					</div>
				</div>

				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="0" {{ isset($record->status) ? (($record->status == 0) ? 'checked' : '' ): '' }}>
						<label>Non Aktif</label>
					</div>
				</div>
			</div>
		</div>

		@else
		<div class="field">
			<label>Type</label>
			<select name="tipe_coa_id" class="ui fluid search selection dropdown" style="width: 100%;">
				{!! \Lotte\Models\Master\TipeCoa::options('nama', 'id',['selected' => isset($record->tipe_coa_id) ? $record->tipe_coa_id : '', 'orders' => ['id']], '-- Pilih Type --') !!}
			</select>
		</div>
		{{-- <div class="field">
			<label>Kode	COA 1</label>
			<input type="text" name="tipe_coa_id" value="{{ $tipe_coa_id }}" style="background-color: #edf1f6;" readonly>
		</div> --}}
		<div class="field">
			<label>Kode	COA 1</label>
			<input type="text" name="kode" placeholder="Kode COA 1" value="">
		</div>
		<div class="field">
			<label>Nama COA 1</label>
			<input type="text" name="nama" placeholder="Nama COA 1" value="">
		</div>

		<div class="field">
			<label>Status COA 3</label>
			<div class="inline fields">
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="1" {{ isset($record->status) ? (($record->status == 1) ? 'checked' : '' ): '' }}>
						<label>Aktif</label>
					</div>
				</div>

				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="0" {{ isset($record->status) ? (($record->status == 0) ? 'checked' : '' ): '' }}>
						<label>Non Aktif</label>
					</div>
				</div>
			</div>
		</div>

		@endif

		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>