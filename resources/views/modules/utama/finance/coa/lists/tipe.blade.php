@include('modules.utama.finance.coa.lists.table')

<div class="ui form" style="float:right">
    <div class="fields">
        <input type="hidden" name="kode" value="{{ $kode }}">
        <input type="hidden" name="type_form" value="{{ $type_form }}">
        <input type="hidden" name="tipe_id" value="{{ $tipe_id }}">
        {{-- <input type="hidden" name="tipe_coa_id" value="{{ $tipe_coa_id }}"> --}}
        <button type="button" class="ui blue add button" onclick="showForm(5)"><i class="plus icon"></i>Tambah Data</button>
        {{-- <button type="button" class="ui green button" onclick="javascript:tipe();">
            <i class="file excel outline icon"></i>
            Export Excel
        </button> --}}
        
    </div>
</div>
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            @foreach ($tableStruct as $struct)
            <th>{{ $struct['label'] or $struct['name'] }}</th>
            @endforeach
        </tr>
    </thead>
</table>