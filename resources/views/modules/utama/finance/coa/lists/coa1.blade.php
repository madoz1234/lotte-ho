@include('modules.utama.finance.coa.lists.table')
<div class="ui form" style="float:right">
    <div class="fields">
        <input type="hidden" name="kode" value="{{ $kode }}">
        <input type="hidden" name="type_form" value="{{ $type_form }}">
        <button type="button" class="ui blue add button" onclick="showForm(2)"><i class="plus icon"></i>Tambah Data</button>
        <div class="ui green buttons" style="margin-right: 5px">
        <div class="ui button">Download</div>
        <div class="ui floating dropdown icon button">
            <i class="dropdown icon"></i>
            <div class="menu">
                <div class="item"><i class="file excel outline icon"></i> Excel</div>
                <div class="item"><i class="file pdf outline icon"></i> PDF</div>
            </div>
        </div>
    </div>
        
    </div>
</div>
<table id="reportTable" class="ui celled compact red table" width="100%" cellspacing="0">
    <thead>
        <tr>
            @foreach ($tableStruct as $struct)
               <th>{{ $struct['label'] or $struct['name'] }}</th>
            @endforeach
        </tr>
    </thead>
</table>