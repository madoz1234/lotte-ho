<script type="text/javascript">
// alert('tara');
	var dt = $('#reportTable').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
			responsive: true,
			destroy: true,
			autoWidth: false,
			processing: true,
			serverSide: true,
			lengthChange: false,
			pageLength: 10,
			filter: false,
			sorting: [],
			columns: {!! json_encode($tableStruct) !!},
			ajax:  {
				url: "{{ url($pageUrl) }}/grid-coa",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
                    d.kode = $('[name=kode]').val();
					d.tipe_id = $('[name=tipe_id]').val();
                    d.searchbox = $('#searchbox').val();
				}
			}, 
			drawCallback: function() {
				var api = this.api();
				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				} );
			}
		});

    $('.searchbtn').on('click', function(e) {
        dt.draw();
        e.preventDefault();
    });

    $('#searchbox').keypress(function(e) {
        if(e.which == 13) {
            dt.draw();
            e.preventDefault();
        }
    });

    type_coa = function(){
        // alert('tara');
        // create form
        var form = document.createElement("form");
            form.setAttribute("method", 'POST');
            form.setAttribute("action", "{{ url('export/type-coa') }}");
            form.setAttribute("target", "_blank");

        var csrf = document.createElement("input");
            csrf.setAttribute("type", "hidden");
            csrf.setAttribute("name", '_token');
            csrf.setAttribute("value", '{{ csrf_token() }}');
        form.appendChild(csrf);

        document.body.appendChild(form);
        form.submit();

        document.body.removeChild(form);
    }

    tipe = function(){
        // alert('tara');
        // create form
        var form = document.createElement("form");
            form.setAttribute("method", 'POST');
            form.setAttribute("action", "{{ url('export/tipe') }}");
            form.setAttribute("target", "_blank");

        var csrf = document.createElement("input");
            csrf.setAttribute("type", "hidden");
            csrf.setAttribute("name", '_token');
            csrf.setAttribute("value", '{{ csrf_token() }}');
        form.appendChild(csrf);

        document.body.appendChild(form);
        form.submit();

        document.body.removeChild(form);
    }

    coa1 = function(){
        // alert('tara');
        // create form
        var form = document.createElement("form");
            form.setAttribute("method", 'POST');
            form.setAttribute("action", "{{ url('export/coa1') }}");
            form.setAttribute("target", "_blank");

        var csrf = document.createElement("input");
            csrf.setAttribute("type", "hidden");
            csrf.setAttribute("name", '_token');
            csrf.setAttribute("value", '{{ csrf_token() }}');
        form.appendChild(csrf);

        document.body.appendChild(form);
        form.submit();

        document.body.removeChild(form);
    }
</script>