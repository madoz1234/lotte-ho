@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

{{-- @section('rules')
<script type="text/javascript">
	formRules = {
		npwp: {
			identifier: 'npwp',
			rules: [{
				type   : 'empty',
				prompt : 'Isian npwp tidak boleh kosong'
			},{
				type   : 'minLength[15]',
				prompt : 'Isian npwp harus berjumlah {ruleValue} karakter'
			},{
				type   : 'maxLength[15]',
				prompt : 'Isian npwp harus berjumlah {ruleValue} karakter'
			}]
		},
		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nama tidak boleh kosong'
			},{
				type   : 'minLength[2]',
				prompt : 'Isian nama setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Isian npwp seharusnya berjumlah {ruleValue} karakter'
			}]
		}
	};
</script>
@endsection --}}

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		$('.treeview.accordion')
		  .accordion({
		    selector: {
		      trigger: '.title .icon'
		    }
		  })
		;

		$(".treeview.accordion").click(function(event) {
			$('.title').removeAttr('style');
			$('.title.active').css("font-weight","Bold");
		});

		// var dt = $('#reportTable').DataTable({
	 //        dom: 'rt<"bottom"ip><"clear">',
		// });

		// $("#searchbox").keyup(function() {
		//    dt.search(this.value).draw();
		// });   

		$('.ui.calendar').calendar({
			type: 'date'
		});
	});

	showTable = function(v,kode, tipe_id =''){
        $('#searchbox').val('');
		$.ajax({
			url: '{{  url($pageUrl) }}/grid',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				type: v,
				kode: kode,
				tipe_id: tipe_id,
			},
		})
		.done(function(response) {
			

		var v_1 = v.split('_')

		if(v_1[0] == 'tipe'){
			$('[name=form_type]').val('tipe');
			var str1 = v_1[1].toLowerCase().replace(/\b[a-z]/g, function(letter) {
				    return letter.toUpperCase();
				});
			$('.twelve.wide.column').html(response);
			$('.parent').html(str1);
		}else if(v_1[0] == 'parent'){
			$('[name=form_type]').val('coa1');
			var str1 = v_1[1].toLowerCase().replace(/\b[a-z]/g, function(letter) {
				    return letter.toUpperCase();
				});
			$('.twelve.wide.column').html(response);
			$('.parent').html(str1);
		}else if(v_1[0] == 'child'){
			$('[name=form_type]').val('coa2');
			var str1 = v_1[2].toLowerCase().replace(/\b[a-z]/g, function(letter) {
				    return letter.toUpperCase();
				});
			$('.twelve.wide.column').html(response);
			$('.child').html(str1);
		}else{				
			var str1 = v_1[1].toLowerCase().replace(/\b[a-z]/g, function(letter) {
				    return letter.toUpperCase();
				});
			var str2 = v_1[2].toLowerCase().replace(/\b[a-z]/g, function(letter) {
				    return letter.toUpperCase();
				});
			$('.twelve.wide.column').html(response);
			$('.kode_3').html(str1 + ' ' + str2);
		}
			
		})
		.fail(function(response) {
			console.log("error");
		});
		
	}

	
	showForm =  function(type){
		var parent_kode = $('[name=kode]').val();
		if(parent_kode){
			parent_kode = parent_kode;
		}else{
			parent_kode = 0;
		}
		// alert (parent_kode);
		var url = "{{ url($pageUrl) }}/create/"+type+"/"+parent_kode;
		loadModal(url);
	}

	showFormEdit =  function(id){
		var type = $('[name=type_form]').val();
		var url = "{{ url($pageUrl) }}/edit/"+type+"/"+id;
		loadModal(url);
	}
</script>
@append

@section('content-body')
<div class="ui clearing segment">
	<div class="ui divided grid">
		<div class="four wide column" style="position: relative">
			@include('modules.utama.finance.coa.menu')
		</div>
		<div class="twelve wide column">
		{{-- 	@include('modules.utama.finance.coa.lists.divisi') --}}
			{{-- @include('modules.utama.finance.coa.lists.coa1') --}}
		</div>
	</div>
</div>

@endsection