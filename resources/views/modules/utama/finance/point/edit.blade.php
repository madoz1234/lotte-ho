<div class="ui inverted loading dimmer">
  <div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Point</div>
<div class="content">
  <form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
    {!! csrf_field() !!}
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="field">
      <label>Tanggal Berlaku</label>   
      <div class="ui calendar" id="from">
        <div class="ui input left icon">
          <i class="calendar icon date"></i>
          <input name="tgl_berlaku" type="text" placeholder="Tanggal Berlaku" value="{{ $record->tgl_berlaku or '' }}">
        </div>
      </div>
    </div>

    <div class="field">
      <label>Kelipatan (Rp)</label>   
      <input name="konversi" class="length" placeholder="Inputkan Konversi" maxlength="20" type="text" value="{{ $record->konversi or '' }}">
    </div>

    <div class="field">
      <label>Faktor Konversi (%)</label>   
      <input name="faktor_konversi" class="length" placeholder="Inputkan Faktor Konversi (%)" maxlength="20" type="text" value="{{ $record->faktor_konversi or '' }}">
    </div>

    <div class="field">
      <label>Faktor Reedem (Point/Rp)</label>   
      <input name="faktor_reedem" class="length" placeholder="Inputkan Faktor Reedem (Point/Rp)" maxlength="20" type="text" value="{{ $record->faktor_reedem or '' }}">
    </div>

    <div class="two fields">
      <div class="field">
        <label>Status</label>
        <div class="inline fields">
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" name="status" tabindex="0" class="" value="1" {{ ($record->status == 1 ) ? 'checked' : '' }}
              {{ ($record->status == 0 ) ? 'disabled' : '' }} >
              <label>Aktif</label>
            </div>
          </div>

          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" name="status" tabindex="0" class="" value="0" {{ ($record->status == 0 ) ? 'checked' : '' }}>
              <label>Non Aktif</label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ui error message"></div>
  </form>
</div>
<div class="actions">
  <div class="ui negative button" style="background: grey;">
    Tutup
  </div>
  <div class="ui positive right labeled icon save button">
    Simpan
    <i class="checkmark icon"></i>
  </div>
</div>


<script type="text/javascript">
  var inputQuantity = [];
  $(function() {
    $(".length").each(function(i) {
      inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
       });
    $(".length").on("keyup", function (e) {
      var $field = $(this),
      val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
              this.value = inputQuantity[$thisIndex];
              return;
            } 
            if (val.length > Number($field.attr("maxlength"))) {
              val=val.slice(0, 5);
              $field.val(val);
            }
            inputQuantity[$thisIndex]=val;
          });      
  });
</script>