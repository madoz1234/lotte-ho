<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Bank</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="fields">
			<div class="twelve wide field">
				<label>Nama Bank</label>
				<input type="text" name="nama" placeholder="Nama Bank" value="{{ $nama or ""  }}">
			</div>
			<div class="four wide field">
				<label>Kode Swift</label>
				<input type="text" name="kode_swift" placeholder="Kode Swift" value="{{ $kode_swift or ""  }}">
			</div>
			<div class="four wide field">
				<label>Kode Bank</label>
				<input class="length" maxlength="3" type="text" name="kode_bank" placeholder="Kode Bank" value="{{ $kode_bank or ""  }}">
			</div>
		</div>
		<div class="fields">
			<div class="twelve wide field">
				<label>Alamat</label>
				<input type="text" name="alamat" placeholder="Alamat Jalan" value="{{ $alamat or ""  }}">
			</div>
			<div class="four wide field">
				<label>Kode Pos</label>
				<input class="length" maxlength="5" type="text" name="kode_pos" placeholder="Kode Pos" value="{{ $kode_pos or ""  }}">
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label class="hidden">Provinsi</label>
				<select name="provinsi_id" class="ui fluid search selection dropdown" style="width: 100%;">
		    	    {!! \Lotte\Models\Master\Provinsi::options('nama', 'id',[], '-- Pilih Provinsi --') !!}
			    </select>
			</div>
			<div class="field">
				<label class="hidden">Kota</label>
				<select name="kota_id" class="ui fluid search selection dropdown" style="width: 100%;">
		    	    {!! \Lotte\Models\Master\Kota::options('nama', 'id',[], '-- Pilih Kota --') !!}
			    </select>
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label>Telepon</label>
				<input type="text" name="telepon" placeholder="Telepon" value="{{ $telepon or ""  }}">
			</div>
			<div class="field">
				<label>Email</label>
				<input type="text" name="email" placeholder="Email" value="{{ $email or ""  }}">
			</div>
		</div>
		<div class="field">
			<label>Tanggal Mulai Kerja Sama</label>
			<div class="ui calendar" id="tanggal_kerjasama">
				<div class="ui input left icon">
					<i class="calendar icon date"></i>
					<input name="tgl_mulai" type="text" placeholder="Tanggal Mulai Kerjasama" value="{{ $tgl_mulai or ""  }}">
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>

</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('[name=provinsi_id]').change(function(e){
			$.ajax({
				url: "{{ url('ajax/option/kota') }}",
				type: 'GET',
				data:{
					id_provinsi : $(this).val(),
				},
			})
			.success(function(response) {
				$('[name=kota_id]').html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });
	})
</script>