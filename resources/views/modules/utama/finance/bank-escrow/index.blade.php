@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input type="text" name="filter[nama]" placeholder="Bank">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_bank_escrow();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
			type: 'date'
		});
	};
</script>
@endsection

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nama bank tidak boleh kosong'
			},{
				type   : 'minLength[3]',
				prompt : 'Isian nama bank setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[50]',
				prompt : 'Isian nama bank seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		kode_swift: {
			identifier: 'kode_swift',
			rules: [{
				type   : 'empty',
				prompt : 'Isian kode swift tidak boleh kosong'
			},{
				type   : 'minLength[3]',
				prompt : 'Isian kode swift setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[8]',
				prompt : 'Isian kode swift seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		kode_bank: {
			identifier: 'kode_bank',
			rules: [{
				type   : 'empty',
				prompt : 'Isian kode bank tidak boleh kosong'
			},{
				type   : 'minLength[3]',
				prompt : 'Isian kode bank setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[3]',
				prompt : 'Isian kode bank seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		alamat: {
			identifier: 'alamat',
			rules: [{
				type   : 'empty',
				prompt : 'Isian alamat tidak boleh kosong'
			},{
				type   : 'minLength[5]',
				prompt : 'Isian alamat setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[200]',
				prompt : 'Isian alamat seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		provinsi_id: {
			identifier: 'provinsi_id',
			rules: [{
				type   : 'empty',
				prompt : 'Isian provinsi tidak boleh kosong'
			}]
		},
		kota_id: {
			identifier: 'kota_id',
			rules: [{
				type   : 'empty',
				prompt : 'Isian kota tidak boleh kosong'
			}]
		},
		telepon: {
			identifier: 'telepon',
			rules: [{
				type   : 'empty',
				prompt : 'Isian telepon tidak boleh kosong'
			},{
				type   : 'minLength[4]',
				prompt : 'Isian telepon setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[15]',
				prompt : 'Isian telepon seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		tgl_mulai: {
			identifier: 'tgl_mulai',
			rules: [{
				type   : 'empty',
				prompt : 'Isian tanggal mulai kerja Sama tidak boleh kosong'
			},{
				type   : 'minLength[4]',
				prompt : 'Isian tanggal mulai kerja Sama setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[20]',
				prompt : 'Isian tanggal mulai kerja seharusnya berjumlah {ruleValue} karakter'
			}]
		}
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_bank_escrow = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-bank-escrow') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append