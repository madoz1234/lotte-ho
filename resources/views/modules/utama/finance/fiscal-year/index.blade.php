@extends('layouts.grid')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@if (session('success'))
    <div class="flash-message">
    <div class="alert alert-success">

    </div>
    </div>
@endif

@section('filters')
	<div class="field">		
		<div class="ui calendar" id="from">
			<div class="ui input left icon">
				<i class="calendar icon date"></i>
				<input name="filter[tgl_awal]" type="text" placeholder="Tanggal Mulai">
			</div>
		</div>
	</div>

	<div class="field">		
		<div class="ui calendar" id="from">
			<div class="ui input left icon">
				<i class="calendar icon date"></i>
				<input name="filter[tgl_akhir]" type="text" placeholder="Tanggal Selesai">
			</div>
		</div>
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_tahun_fiskal();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.tgl_awal = $("input[name='filter[tgl_awal]']").val();
    d.tgl_akhir = $("input[name='filter[tgl_akhir]']").val();
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	tgl_awal: {
		identifier: 'tgl_awal',
		rules: [{
			type   : 'empty',
			prompt : 'Isian tahun angaran mulai tidak boleh kosong'
		},{
			type   : 'minLength[5]',
			prompt : 'Isian tahun angaran mulai setidaknya harus memiliki {ruleValue} karakter'
		},{
			type   : 'maxLength[20]',
			prompt : 'Isian tahun angaran mulai seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	tgl_akhir: {
		identifier: 'tgl_akhir',
		rules: [{
			type   : 'empty',
			prompt : 'Isian tahun angaran akhir tidak boleh kosong'
		},{
			type   : 'minLength[5]',
			prompt : 'Isian tahun angaran akhir setidaknya harus memiliki {ruleValue} karakter'
		},{
			type   : 'maxLength[20]',
			prompt : 'Isian tahun angaran akhir seharusnya berjumlah {ruleValue} karakter'
		}]
	},

	status: {
		identifier: 'status',
		rules: [{
			type   : 'empty',
			prompt : 'Pilihan status tidak boleh kosong'
		},{
			type   : 'maxLength[2]',
			prompt : 'Isian status seharusnya berjumlah {ruleValue} karakter'
		}]
	}
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//Calender
		$('.ui.calendar').calendar({
		  type: 'date',
		  startMode: 'year'
		});
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>

		$(document).ready(function() {
			export_tahun_fiskal = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-tahun-fiskal') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var tgl_awal = document.createElement("input");
		            tgl_awal.setAttribute("type", "hidden");
		            tgl_awal.setAttribute("name", 'tgl_awal');
		            tgl_awal.setAttribute("value", $('[name="filter[tgl_awal]"]').val());
		        form.appendChild(tgl_awal);

		        var tgl_akhir = document.createElement("input");
		            tgl_akhir.setAttribute("type", "hidden");
		            tgl_akhir.setAttribute("name", 'tgl_akhir');
		            tgl_akhir.setAttribute("value", $('[name="filter[tgl_akhir]"]').val());
		        form.appendChild(tgl_akhir);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append