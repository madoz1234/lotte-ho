<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Tahun Fiskal</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui error message"></div>
			<div class="field">
			<label>Mulai Tahun Fiskal</label>		
				<div class="ui calendar" id="from">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tgl_awal" type="text" placeholder="Tanggal Mulai" value="{{ $tgl_awal or "" }}">
					</div>
				</div>
			</div>

			<div class="field">
			<label>Selesai Tahun Fiskal</label>		
				<div class="ui calendar" id="to">
					<div class="ui input left icon">
						<i class="calendar icon date"></i>
						<input name="tgl_akhir" type="text" placeholder="Tanggal Selesai" value="{{ $tgl_akhir or "" }}">
					</div>
				</div>
			</div>

			<div class="two fields">
				<div class="field">
					<label>Status</label>
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="status" tabindex="0" class="" value="1">
								<label>Berjalan</label>
							</div>
						</div>

						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="status" tabindex="0" class="" value="0">
								<label>Selesai</label>
							</div>
						</div>
					</div>
				</div>
			</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>