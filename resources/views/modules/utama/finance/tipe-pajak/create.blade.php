<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Tipe Pajak</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="field">
			<label>Nama</label>
			<input name="nama" placeholder="Inputkan Nama" type="text">
		</div>

		<div class="field">
			<label>Bebas Pajak</label>
			<div class="ui fluid search selection dropdown">
				<input type="hidden" name="bebas_pajak">
				<i class="dropdown icon"></i>
				<div class="default text">-- Pilih --</div>
				<div class="menu">
					<div class="item" data-value="Iya">Iya</div>
					<div class="item" data-value="Tidak">Tidak</div>
				</div>
			</div>
		</div>

	</form>
</div>
<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>