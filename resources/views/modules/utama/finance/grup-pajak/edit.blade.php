<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Grup Pajak</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="field">
			<label>Deskripsi</label>
			<input name="deskripsi" placeholder="Inputkan Deskripsi" type="text" value="Bebas Pajak">
		</div>

		<div class="field">
			<label>Pajak Pengiriman</label>
			<div class="ui fluid search selection dropdown">
				<input type="hidden" name="pajak_pengiriman">
				<i class="dropdown icon"></i>
				<div class="text">Tidak</div>
				<div class="menu">
					<div class="item" data-value="Iya">Iya</div>
					<div class="item" data-value="Tidak">Tidak</div>
				</div>
			</div>
		</div>
		
		<div class="field">
			<label>Tipe Pajak</label>
			<div class="ui fluid search selection dropdown">
				<input type="hidden" name="tipe_pajak">
				<i class="dropdown icon"></i>
				<div class="text">PPN Masukan (10%)</div>
				<div class="menu">
					<div class="item" data-value="PPN Masukan">PPN Masukan (10%)</div>
					<div class="item" data-value="PPN Keluaran">PPN Keluaran (10%)</div>
				</div>
			</div>
		</div>

	</form>
</div>
<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>