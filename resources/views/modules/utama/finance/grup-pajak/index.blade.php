@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[deskripsi]" placeholder="Deskripsi" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
	deskripsi: {
		identifier: 'deskripsi',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Deskripsi tidak boleh kosong'
		}]
	},

	pajak_pengiriman: {
		identifier: 'pajak_pengiriman',
		rules: [{
			type   : 'empty',
			prompt : 'Isian Pajak Pengiriman tidak boleh kosong'
		}]
	},

	tipe_pajak: {
		identifier: 'tipe_pajak',
		rules: [{
			type   : 'empty',
			prompt : 'Pilihan Tipe Pajak tidak boleh kosong'
		}]
	}
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//
	};
</script>
@endsection

@section('tableBody')
	<tr>
		<td>1</td>
		<td>Bebas Pajak</td>
		<td>Tidak</td>
		<td>PPN Masukan (10%)</td>
		<td>2 Jam yang lalu</td>
		<td>Admin</td>
		<td>
        	<button type="button" class="ui mini orange icon edit button" data-content="Ubah Data" data-id="1"><i class="edit icon"></i></button>
        	<button type="button" class="ui mini red icon delete button" data-content="Hapus Data" data-id="1"><i class="trash icon"></i></button>
		</td>
	</tr>
@endsection