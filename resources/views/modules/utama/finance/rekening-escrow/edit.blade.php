<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Rekening Bank</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="field">
			<label>Bank</label>
			<select name="bank_escrow_id" class="ui fluid search selection dropdown" style="width: 100%;">
	    	    {!! \Lotte\Models\Master\BankEscrow::options('nama', 'id',['selected' => isset($record->bank_escrow_id) ? $record->bank_escrow_id : '',], '-- Pilih Bank Escrow --') !!}
		    </select>
		</div>

		<div class="field">
			<label>Nama Pemilik Rekening</label>
			<input name="nama_pemilik" placeholder="Nama Pemilik Rekening" type="text" value="{{ $record->nama_pemilik or "" }}">
		</div>
		<div class="field">
			<label>Nomor Rekening</label>
			<input class="length" maxlength="20"  name="nomor_rekening" placeholder="Nomor Rekening" type="text" value="{{ $record->nomor_rekening or "" }}">
		</div>
		<div class="field">
			<label>Status</label>
			<div class="inline fields">
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="1" {{ ($record->status == 1 ) ? 'checked' : '' }} >
						<label>Aktif</label>
					</div>
				</div>

				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="0" {{ ($record->status == 0 ) ? 'checked' : '' }}>
						<label>Non-Aktif</label>
					</div>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>


<script type="text/javascript">
	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });
</script>