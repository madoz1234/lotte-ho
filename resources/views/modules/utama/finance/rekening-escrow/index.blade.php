@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[nama_pemilik]" placeholder="Nama Rekening" type="text">
	</div>
	<div class="field">
		<input name="filter[nomor_rekening]" placeholder="No Rekening" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_rekening_escrow();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.nama_pemilik = $("input[name='filter[nama_pemilik]']").val();
    d.nomor_rekening = $("input[name='filter[nomor_rekening]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		bank_escrow_id: {
			identifier: 'bank_escrow_id',
			rules: [{
				type   : 'empty',
				prompt : 'Isian bank escrow tidak boleh kosong'
			},{
				type   : 'maxLength[2]',
				prompt : 'Isian bank escrow seharusnya berjumlah {ruleValue} karakter'
			}]
		},

		nama_pemilik: {
			identifier: 'nama_pemilik',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nama pemilik rekening tidak boleh kosong'
			},{
				type   : 'minLength[2]',
				prompt : 'Isian nama pemilik rekening setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[50]',
				prompt : 'Isian nama pemilik rekening seharusnya berjumlah {ruleValue} karakter'
			}]
		},

		nomor_rekening: {
			identifier: 'nomor_rekening',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nomor rekening tidak boleh kosong'
			},{
				type   : 'minLength[5]',
				prompt : 'Isian nomor rekening setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[20]',
				prompt : 'Isian nomor rekening seharusnya berjumlah {ruleValue} karakter'
			}]
		},

		status: {
			identifier: 'status',
			rules: [{
				type   : 'empty',
				prompt : 'Isian status tidak boleh kosong'
			},{
				type   : 'maxLength[2]',
				prompt : 'Isian status seharusnya berjumlah {ruleValue} karakter'
			}]
		}
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_rekening_escrow = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-rekening-escrow') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var nama_pemilik = document.createElement("input");
		            nama_pemilik.setAttribute("type", "hidden");
		            nama_pemilik.setAttribute("name", 'nama_pemilik');
		            nama_pemilik.setAttribute("value", $('[name="filter[nama_pemilik]"]').val());
		        form.appendChild(nama_pemilik);

		        var nomor_rekening = document.createElement("input");
		            nomor_rekening.setAttribute("type", "hidden");
		            nomor_rekening.setAttribute("name", 'nomor_rekening');
		            nomor_rekening.setAttribute("value", $('[name="filter[nomor_rekening]"]').val());
		        form.appendChild(nomor_rekening);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append