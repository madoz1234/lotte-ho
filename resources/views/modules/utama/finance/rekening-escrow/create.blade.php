<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Rekening Bank</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="field">
			<label>Bank</label>
			<select name="bank_escrow_id" class="ui fluid search selection dropdown" style="width: 100%;">
	    	    {!! \Lotte\Models\Master\BankEscrow::options('nama', 'id',[], '-- Pilih Bank  --') !!}
		    </select>
		</div>

		<div class="field">
			<label>Nama Pemilik Rekening</label>
			<input name="nama_pemilik" placeholder="Nama Pemilik Rekening" type="text" value="{{ $nama or ""  }}">
		</div>
		<div class="field">
			<label>Nomor Rekening</label>
			<input class="length" maxlength="20" name="nomor_rekening" placeholder="Nomor Rekening" type="text" value="{{ $nomor_rekening or ""  }}">
		</div>
		<div class="field">
			<label>Status</label>
			<div class="inline fields">
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="1">
						<label>Aktif</label>
					</div>
				</div>

				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="status" tabindex="0" class="" value="0">
						<label>Non-Aktif</label>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>


<script type="text/javascript">
	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });
</script>