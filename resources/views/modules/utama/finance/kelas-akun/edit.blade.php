<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Kelas Akun</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="field">
			<label>Kelas Id</label>
			<input name="kelas_id" placeholder="Inputkan ID Kelas" type="text" value="1">
		</div>

		<div class="field">
			<label>Nama Kelas</label>
			<input name="nama_kelas" placeholder="Inputkan Nama Kelas" type="text" value="Aset">
		</div>

		<div class="field">
			<label>Kelas Tipe </label>
			<div class="ui fluid search selection dropdown">
				<input type="hidden" name="kelas_tipe">
				<i class="dropdown icon"></i>
				<div class="text">Assets</div>
				<div class="menu">
					<div class="item" data-value="Assets">Assets</div>
					<div class="item" data-value="Liabilities">Liabilities</div>
					<div class="item" data-value="Equity">Equity</div>
					<div class="item" data-value="Income">Income</div>
					<div class="item" data-value="Cos of Goods Sold">Cos of Goods Sold</div>
					<div class="item" data-value="Espense">Espense</div>
				</div>
			</div>
		</div>
	</form>
	
</div>
<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>