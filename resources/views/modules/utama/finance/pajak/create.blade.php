<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Pajak</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		{{-- <input type="hidden" name="_method" value="PUT"> --}}
	    {{-- <input type="hidden" name="pajak_id" value="{{ $record->pajak_id }}"> --}}
		<div class="field">
			<label>NPWP</label>
			<input name="npwp" class="length" placeholder="Inputkan No NPWP" maxlength="15" type="text" value="{{ old('npwp') }}">
		</div>
		<div class="field">
			<label>Nama</label>
			<input name="nama" placeholder="Inputkan Nama" type="text" value="{{ old('nama') }}">
		</div>
		<div class="field">
			<label>Alamat</label>
			<input name="alamat_npwp" placeholder="Isian Alamat..." type="text" value="{{ old('alamat_npwp') }}">
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>


<script type="text/javascript">
	var inputQuantity = [];
    $(function() {
      $(".length").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".length").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 5);
          $field.val(val);
        }
        inputQuantity[$thisIndex]=val;
      });      
    });
</script>
