@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[npwp]" placeholder="NPWP" type="text">
	</div>
	<div class="field">
		<input name="filter[nama]" placeholder="Nama" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_pajak();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

@section('js-filters')
    d.npwp = $("input[name='filter[npwp]']").val();
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		npwp: {
			identifier: 'npwp',
			rules: [{
				type   : 'empty',
				prompt : 'Isian npwp tidak boleh kosong'
			},{
				type   : 'minLength[15]',
				prompt : 'Isian npwp harus berjumlah {ruleValue} karakter'
			},{
				type   : 'maxLength[15]',
				prompt : 'Isian npwp harus berjumlah {ruleValue} karakter'
			}]
		},
		nama: {
			identifier: 'nama',
			rules: [{
				type   : 'empty',
				prompt : 'Isian nama tidak boleh kosong'
			},{
				type   : 'minLength[2]',
				prompt : 'Isian nama setidaknya harus memiliki {ruleValue} karakter'
			},{
				type   : 'maxLength[30]',
				prompt : 'Isian npwp seharusnya berjumlah {ruleValue} karakter'
			}]
		},
		// alamat_npwp: {
		// 	identifier: 'alamat_npwp',
		// 	rules: [{
		// 		type   : 'empty',
		// 		prompt : 'Isian alamat npwp tidak boleh kosong'
		// 	},{
		// 		type   : 'minLength[2]',
		// 		prompt : 'Isian alamat npwp setidaknya harus memiliki {ruleValue} karakter'
		// 	},{
		// 		type   : 'maxLength[200]',
		// 		prompt : 'Isian alamat npwp seharusnya berjumlah {ruleValue} karakter'
		// 	}]
		// }
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_pajak = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-pajak') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        var npwp = document.createElement("input");
		            npwp.setAttribute("type", "hidden");
		            npwp.setAttribute("name", 'npwp');
		            npwp.setAttribute("value", $('[name="filter[npwp]"]').val());
		        form.appendChild(npwp);

		        var nama = document.createElement("input");
		            nama.setAttribute("type", "hidden");
		            nama.setAttribute("name", 'nama');
		            nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append