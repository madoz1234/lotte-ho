@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[grup_id]" placeholder="Grup ID" type="text">
	</div>

	<div class="field">
		<input name="filter[nama_grup]" placeholder="Nama Grup" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
		grup_id: {
			identifier: 'grup_id',
			rules: [{
				type   : 'empty',
				prompt : 'Isian ID Grup tidak boleh kosong'
			}]
		},
		nama_grup: {
			identifier: 'nama_grup',
			rules: [{
				type   : 'empty',
				prompt : 'Isian Nama Grup tidak boleh kosong'
			}]
		},

		sub_grup: {
			identifier: 'sub_grup',
			rules: [{
				type   : 'empty',
				prompt : 'Isian Sub Grup Tipe tidak boleh kosong'
			}]
		},

		tipe: {
			identifier: 'tipe',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Grup Tipe tidak boleh kosong'
			}]
		}
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//
	};
</script>
@endsection

@section('tableBody')
	<tr>
		<td>1</td>
		<td>1010000</td>
		<td>Cash and Bank</td>
		<td> </td>
		<td>Aset</td>
		<td>2 Jam yang lalu</td>
		<td>
        	<button type="button" class="ui mini orange icon edit button" data-content="Ubah Data" data-id="1"><i class="edit icon"></i></button>
        	<button type="button" class="ui mini red icon delete button" data-content="Hapus Data" data-id="1"><i class="trash icon"></i></button>
		</td>
	</tr>
@endsection