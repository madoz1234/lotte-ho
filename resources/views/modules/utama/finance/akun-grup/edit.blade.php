<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Kelas Akun</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="field">
			<label>Grup Id</label>
			<input name="kelas_id" placeholder="Inputkan ID Grup" type="text" value="1010000">
		</div>

		<div class="field">
			<label>Nama Grup</label>
			<input name="nama_grup" placeholder="Inputkan Nama Grup" type="text" value="Cash and Bank">
		</div>

		<div class="field">
			<label>Sub Grup</label>
			<input name="sub_grup" placeholder="Inputkan Sub Grup" type="text" value="">
		</div>

		<div class="field">
			<label>Tipe </label>
			<div class="ui fluid search selection dropdown">
				<input type="hidden" name="tipe">
				<i class="dropdown icon"></i>
				<div class="text">Aset</div>
				<div class="menu">
					<div class="item" data-value="Assets">Assets</div>
					<div class="item" data-value="Liabilities">Liabilities</div>
					<div class="item" data-value="Equity">Equity</div>
					<div class="item" data-value="Income">Income</div>
					<div class="item" data-value="Cos of Goods Sold">Cos of Goods Sold</div>
					<div class="item" data-value="Espense">Espense</div>
				</div>
			</div>
		</div>
	</form>
	
</div>
<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>