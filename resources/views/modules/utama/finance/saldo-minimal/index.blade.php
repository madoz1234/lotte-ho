@extends('layouts.grid')

@section('filters')
	{{-- <div class="field">
		<input name="filter[npwp]" placeholder="NPWP" type="text">
	</div>
	<div class="field">
		<input name="filter[nama]" placeholder="Nama" type="text">
	</div> --}}
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button">
		<i class="plus icon"></i>
		Tambah Data
	</button>
	<button type="button" class="ui green button" onclick="javascript:export_saldo_minimal();">
		<i class="file excel outline icon"></i>
		Export Excel
	</button>
@endsection

{{-- @section('js-filters')
    d.npwp = $("input[name='filter[npwp]']").val();
    d.nama = $("input[name='filter[nama]']").val();
@endsection --}}

@section('rules')
<script type="text/javascript">
	formRules = {
		saldo_minimal_mengendap: {
			identifier: 'saldo_minimal_mengendap',
			rules: [{
				type   : 'empty',
				prompt : 'Saldo minimal mengendap tidak boleh kosong'
			},{
				type   : 'minLength[2]',
				prompt : 'Saldo minimal mengendap harus berjumlah {ruleValue} karakter'
			},{
				type   : 'maxLength[15]',
				prompt : 'Saldo minimal mengendap harus berjumlah {ruleValue} karakter'
			}]
		},
		tahun_fiskal_id: {
			identifier: 'tahun_fiskal_id',
			rules: [{
				type   : 'empty',
				prompt : 'Tahun fiskal  tidak boleh kosong'
			}]
		}
	};
</script>
@endsection

@section('scripts')
	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function() {
			export_saldo_minimal = function(){
				// alert('tara');
				// create form
		        var form = document.createElement("form");
		            form.setAttribute("method", 'POST');
		            form.setAttribute("action", "{{ url('export/export-saldo-minimal') }}");
		            form.setAttribute("target", "_blank");

		        var csrf = document.createElement("input");
		            csrf.setAttribute("type", "hidden");
		            csrf.setAttribute("name", '_token');
		            csrf.setAttribute("value", '{{ csrf_token() }}');
		        form.appendChild(csrf);

		        // var npwp = document.createElement("input");
		        //     npwp.setAttribute("type", "hidden");
		        //     npwp.setAttribute("name", 'npwp');
		        //     npwp.setAttribute("value", $('[name="filter[npwp]"]').val());
		        // form.appendChild(npwp);

		        // var nama = document.createElement("input");
		        //     nama.setAttribute("type", "hidden");
		        //     nama.setAttribute("name", 'nama');
		        //     nama.setAttribute("value", $('[name="filter[nama]"]').val());
		        // form.appendChild(nama);

		        document.body.appendChild(form);
		        form.submit();

		        document.body.removeChild(form);
			}
		});
	</script>
@append