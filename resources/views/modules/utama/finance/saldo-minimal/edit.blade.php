<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Ubah Data Saldo Minimal Mengendap</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
   <input type="hidden" name="id" value="{{ $record->id }}">
   <div class="field">
    <label>Tahun Fiskal</label>
    <select name="tahun_fiskal_id" class="ui fluid search selection dropdown" style="width: 100%;">
      {!! \Lotte\Models\Master\TahunFiskal::options('tgl_awal', 'id',['selected' => isset($record->tahun_fiskal_id) ? $record->tahun_fiskal_id : '',], '-- Pilih Tahun Fiskal --') !!}
    </select> <div class="ui error message"></div>
  </div>
  <div class="field">
   <label>saldo minimal mengendap (Rp)</label>
   <input name="saldo_minimal_mengendap" class="length" placeholder="Inputkan Saldo Minimal Mengendap" maxlength="15" type="text" value="{{ $record->saldo_minimal_mengendap or '' }}">
 </div>
</form>
</div>
<div class="actions">
	<div class="ui negative button" style="background: grey;">
		Tutup
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>


<script type="text/javascript">
	var inputQuantity = [];
  $(function() {
    $(".length").each(function(i) {
      inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
       });
    $(".length").on("keyup", function (e) {
      var $field = $(this),
      val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the 
            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
              this.value = inputQuantity[$thisIndex];
              return;
            } 
            if (val.length > Number($field.attr("maxlength"))) {
              val=val.slice(0, 5);
              $field.val(val);
            }
            inputQuantity[$thisIndex]=val;
          });      
  });
</script>