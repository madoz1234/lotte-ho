<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Akun GL</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="field">
			<label>Akun Code</label>
			<input name="group_id" placeholder="Inputkan ID Group" type="text">
		</div>

		<div class="field">
			<label>Nama Akun</label>
			<input name="nama_group" placeholder="Inputkan Nama Group" type="text">
		</div>

		<div class="field">
			<label>Akun Group </label>
			<div class="ui fluid search selection dropdown">
				<input type="hidden" name="akun_group">
				<i class="dropdown icon"></i>
				<div class="default text">-- Pilih --</div>
				<div class="menu">
					<div class="item" data-value="1010000 Cash and Bank">1010000 Cash and Bank</div>
					<div class="item" data-value="1020000 Account Receivable">1020000 Account Receivable</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>