@extends('layouts.grid')

@section('filters')
	<div class="field">
		<input name="filter[akun_code]" placeholder="Akun Kode" type="text">
	</div>
	<div class="field">
		<input name="filter[nama_akun]" placeholder="Nama Akun" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('rules')
<script type="text/javascript">
formRules = {
		group_id: {
			identifier: 'group_id',
			rules: [{
				type   : 'empty',
				prompt : 'Isian Group ID tidak boleh kosong'
			}]
		},

		nama_group: {
			identifier: 'nama_group',
			rules: [{
				type   : 'empty',
				prompt : 'Isian Tipe Akun tidak boleh kosong'
			}]
		},

		akun_group: {
			identifier: 'akun_group',
			rules: [{
				type   : 'empty',
				prompt : 'Pilihan Akun Group tidak boleh kosong'
			}]
		}
};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		//radio checkbox
		$('.ui.radio.checkbox').checkbox();
	};
</script>
@endsection

@section('tableBody')
	<tr>
		<td>1</td>
		<td>1011001</td>
		<td>Escrow Account - Puskopau</td>
		<td>1010000 Cash and Bank</td>
		<td>2 Jam yang lalu</td>
		<td>
        	<button type="button" class="ui mini orange icon edit button" data-content="Ubah Data" data-id="1"><i class="edit icon"></i></button>
        	<button type="button" class="ui mini red icon delete button" data-content="Hapus Data" data-id="1"><i class="trash icon"></i></button>
		</td>
	</tr>
@endsection