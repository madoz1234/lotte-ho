<script type="text/javascript">
    var ratio = 0;
    var edited = false;

    $(document).ready(function($) {
        $('[data-content]').popup({
            hoverable: true,
            position : 'top center',
            delay: {
                show: 300,
                hide: 800
            }
        });
        countRatio({{ $rak->first()->id }});
        // $('.menu .item').tab({
        //     onLoad: function(e, param){
        //         countRatio(e);
        //     }
        // });

        initModal = function(){
            dt = $('#listTable').DataTable({
                dom: 'rt<"bottom"ip><"clear">',
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                lengthChange: true,
                pageLength: 10,
                filter: false,
                sorting: [],
                language: {
                    url: "{{ asset('plugins/datatables/Indonesian.json') }}"
                },
                ajax:  {
                    url: "{{ url($pageUrl) }}/products",
                    type: 'POST',
                    data: function (d) {
                        d._token = "{{ csrf_token() }}";
                        d.assortment = "{{ $record->jenis_assortment_id }}";
                        d.produk_kode = $("input[name='filter[produk_kode]']").val();
                        d.nama = $("input[name='filter[nama]']").val();
                        d.bumun_nm = $("input[name='filter[bumun_nm]']").val();
                        d.l1_nm = $("select[name='filter[l1_nm]']").val();
                    }
                }, 
                columns: [{
                    "data":"num",
                    "name":"num",
                    "label":"No",
                    "orderable":false,
                    "searchable":false,
                    "className":"center aligned",
                    "width":"40px"
                },{
                    "data":"kode",
                    "name":"kode",
                    "label":"Kode",
                    "searchable":false,
                    "sortable":true,
                    "className":"center aligned"
                },{
                    "data":"nama",
                    "name":"nama",
                    "label":"Nama Produk",
                    "searchable":false,
                    "sortable":true
                },{
                    "data":"bumun_nm",
                    "name":"bumun_nm",
                    "label":"Division",
                    "searchable":false,
                    "sortable":false
                },{
                    "data":"l1_nm",
                    "name":"l1_nm",
                    "label":"Category 1",
                    "searchable":false,
                    "sortable":false
                },{
                    "data":"l2_nm",
                    "name":"l2_nm",
                    "label":"Category 2",
                    "searchable":false,
                    "sortable":false
                },{
                    "data":"l3_nm",
                    "name":"l3_nm",
                    "label":"Category 3",
                    "searchable":false,
                    "sortable":false
                },{
                    "data":"action",
                    "name":"action",
                    "label":"Aksi",
                    "searchable":false,
                    "sortable":false,
                    "className":"center aligned",
                    "width":"50px"
                }],
                drawCallback: function() {
                    var api = this.api();

                    api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                    } );

                    $('[data-content]').popup({
                        hoverable: true,
                        position : 'top center',
                        delay: {
                            show: 300,
                            hide: 800
                        }
                    });

                    modalRefresh();

                    //Popup							
                    $('.checked.checkbox').popup({
                        popup : $('.custom.popup'),
                        on    : 'click'
                    });
                }
            });

            $('.filter.button').on('click', function(e) {
                e.preventDefault();
                var length = $("input[name='filter[entri]']").val();
                length = (length != '') ? length : 10;
                dt.page.len(length).draw();
            });
            $('.reset.button').on('click', function(e) {
                $('.dropdown .delete').trigger('click');
                $('.dropdown').dropdown('clear');
                setTimeout(function(){
                    var length = $("input[name='filter[entri]']").val();
                    length = (length != '') ? length : 10;
                    dt.page.len(length).draw();
                }, 100);
            });
        };
    });
    
    $(document).on('click', '.menu .item', function(e){
        if(edited){
            swal({
                title: 'Pindah Rak',
                text: "Data yang belum anda simpan mungkin akan hilang jika belum disimpan apakah anda yakin?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result) {      
                    changeTab($(this));
                }else{
                }
            });
        }else{
            changeTab($(this));
        }
    })
    $(document).on('click', '.add-rack', function(e){
        var rack = $(this).data('rack');
        var plano = $(this).data('plano');

        if(edited){
            swal({
                title: 'Tambah Rak',
                text: "Data yang belum anda simpan akan terhapus apakah anda yakin?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result) {
                    $.redirect('{{ url($pageUrl.'add') }}', {_token: '{{ csrf_token() }}', planogram_id:plano, rak_id:rack});
                }
            });
        }else{
            $.redirect('{{ url($pageUrl.'add') }}', {_token: '{{ csrf_token() }}', planogram_id:plano, rak_id:rack});
        }
    });
    $(document).on('click', '.append.product', function(e){
        e.preventDefault();
        var shelving = $(this).data('row');
        var rack = $(this).data('rack');

        var url = "{{ url($pageUrl) }}/products";
        $('#formModal').removeClass('small');
        $('#formModal').addClass('fullscreen');
        $('#formModal').data('row', shelving);
        $('#formModal').data('rack', rack);

        loadModal(url);
    })
    $(document).on('click', '.stack.product', function(e){
        var shelf = $(this).closest('.shelving');
        var prod = $(this).closest('.prod');
        var img = prod.find('img:first').clone();
        var popup = prod.find('.popup:first').clone();
        popup.removeClass('visible').hide();

        var height = (parseFloat(img.data('height') / 10)) * ratio;
        if(height + (height * prod.find('img').length) > Math.round(shelf.height())){
            swal(
                'Gagal!',
                'Rak sudah penuh.',
                'error'
            ).then(function(e){
                dt.draw();
            });
        }else{
            edited = true;
            prod.prepend(popup).prepend(img);
            
            $('.shelving img').popup({
                popup : $('.following.popup'),
                hoverable: true,
            });
        }
        generateAll(shelf.closest('.racks').attr('id'));
    });
    $(document).on('click', '.remove.product', function(e){
        var shelf = $(this).closest('.shelving');
        var prod = $(this).closest('.prod');

        if(prod.find('img').length > 1){
            var img = $(this).closest('.popup').prev('img');
            img.transition({
                animation  : 'scale',
                onComplete : function(dom) {
                  img.remove();
                  $(this).closest('.popup').remove();
                  generateAll(shelf.closest('.racks').attr('id'));
                }
            });
        }else{
            $(this).closest('.prod').transition({
                animation  : 'scale',
                onComplete : function(dom) {
                  $(this).closest('.prod').remove();
                  generateAll(shelf.closest('.racks').attr('id'));
                }
            });
        }

        edited = true;
    });
    $(document).on('click', '.insert.product', function(e){
        var id = $(this).data('id');
        var rack = $('#formModal').data('rack');
        var row = $('#formModal').data('row');
        var shelf = $('#'+rack).find('.shelving[data-row='+row+']');
        var imgs = shelf.find('.prod');
        var innerWidth = 0;

        imgs.each(function(index) {
            innerWidth += parseFloat($(this).width());
        });

        $('#formModal').find('.loading.dimmer').addClass('active');
        $.get('{{ url('utama/produk/list-produk') }}/'+id, function(data) {
            var style = data.produksetting.uom1_file != 0
                      ? 'normal' : 'empty';

            var img = $(`<li class="prod" 
                             data-kode="`+data.kode+`" 
                             data-nama="`+data.nama+`" 
                             data-barcode="`+data.produksetting.uom1_barcode+`">
                            <img src="{{ url('storage') }}/`+data.produksetting.uom1_file+`"
                                 class="`+style+`"
                                 alt="`+data.nama+`" 
                                 data-width="`+(parseFloat(data.produksetting.uom1_width))+`"
                                 data-height="`+(parseFloat(data.produksetting.uom1_height))+`"/>
                             <div class="ui flowing popup top left transition hidden">
                                   <h4 class="ui header">`+data.kode+`</h4>
                                   <p style="white-space:nowrap">`+data.nama+`</p>
                                   <div style="position:absolute; top:5px; right:5px;">
                                       <div class="ui mini stack product blue icon button"><i class="upload icon"></i></div>
                                       <div class="ui mini remove product red icon button"><i class="remove icon"></i></div>
                                   </div>
                             </div>
                         </li>`);

            var width = (parseFloat(data.produksetting.uom1_width) / 10) * ratio;
            var height = (parseFloat(data.produksetting.uom1_height) / 10) * ratio;

            if(innerWidth + width > shelf.width()){
                swal(
                    'Gagal!',
                    'Rak sudah penuh.',
                    'error'
                ).then(function(e){
                    dt.draw();
                });
            }else{
                edited = true;
                img.find('img').width(width).height(height);
                shelf.append(img.transition('scale'));
                
                $('.shelving img').popup({
                    popup : $('.following.popup'),
                    hoverable: true,
                });

                $('.shelving').sortable({
                    group: 'shelving',
                    onDrop: function ($item, container, _super, event) {
                        $item.removeClass(container.group.options.draggedClass).removeAttr("style")
                        $("body").removeClass(container.group.options.bodyClass)
                        
                        $('.shelving img').popup({
                            popup : $('.following.popup'),
                            hoverable: true,
                        });
                        generateAll($item.closest('.racks').attr('id'));
                    }
                });
            }

            generateAll(rack);
            $('#formModal').find('.loading.dimmer').removeClass('active');
        });
    });

    $(document).on('click', '.save.button', function(e){
        var id = $(this).data('id');
        var racks = $(this).closest('.racks');
        var num = racks.find('.rack.tab.active').data('nomor');
        var segment = $(this).closest('.tab');
        
        swal({
            title: 'Apakah anda yakin?',
            text: "Pastikan data anda sesuai sebelum menyimpan!",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Simpan',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result) {
                if(segment.find('.prod').length == 0){
                    swal(
                        'Gagal!',
                        'Rak tidak boleh kosong saat disimpan.',
                        'error'
                    )
                }else{
                    segment.addClass('loading');
					$('#form-rack-'+id+'-'+num).ajaxSubmit({
						success: function(resp){
                            swal(
                                'Tersimpan!',
                                'Data berhasil disimpan.',
                                'success'
                            ).then((result) => {
                                edited = false;
                                return true;
                            });
                            segment.removeClass('loading');
						},
						error: function(resp){
                            swal(
                                'Gagal!',
                                'Data gagal disimpan.'+resp.responseJSON,
                                'error'
                            );
                            segment.removeClass('loading');
						}
					});	
                }
			}
		})
    });

    $(document).on('change', '.unit.input', function(e){
        var rack = $(this).data('rack');
        var row = $(this).data('row');
        var kode = $(this).data('code');
        var value = $(this).val();

        var json = $.parseJSON($(".jsonHolder[data-rack='"+rack+"']").html());
        var find = json[parseInt(row)-1].find(function( obj ) {
          return obj.kode == kode;
        });
        find.unit = value;
        edited = true;

        $(".jsonHolder[data-rack='"+rack+"']").html(JSON.stringify(json));
    });

    function changeTab(el) {
        $.tab('change tab', el.data('tab'));
        var menu = el.closest('.menu');
        menu.find('.item.active')
            .removeClass('active');
        el.addClass('active');

        var rack = el.data('rack');
        var nomor = el.data('nomor');
        if(typeof nomor == 'undefined'){
            nomor = 1;
        }
        if(typeof rack != 'undefined'){
            countRatio(rack, nomor);
            $('.nomor-'+rack).html(nomor);
            edited = false;
        }
    }

    function countRatio(id, nomor='1') {
        $.get('{{ url('utama/produk/rak') }}/'+id, function(data) {
            var rack = $('#rack-'+id+'-'+nomor);
            var setting = {
                width: data.panjang_rak, 
                height: data.tinggi_rak, 
                length: data.lebar_rak, 
            };

            self.ratio = rack.width() / setting.width;
            var height = setting.height * ratio;

            rack.find('.shelving').height(height / data.shelving);
            $.each(rack.find('img'), function(index, val) {
                var w = $(val).data('width') / 10;
                var h = $(val).data('height') / 10;
                var prod = $(val).closest('.prod');
                $(val).width(w * ratio)
                      .height(h * ratio); 

                if(prod.hasClass('hidden')){
                    prod.transition('scale');
                }
            });
                
            $('.shelving img').popup({
                popup : $('.following.popup'),
                hoverable: true,
            });

            $('.shelving').sortable({
                group: 'shelving',
                onDrop: function ($item, container, _super, event) {
                    $item.removeClass(container.group.options.draggedClass).removeAttr("style")
                    $("body").removeClass(container.group.options.bodyClass)
                    
                    $('.shelving img').popup({
                        popup : $('.following.popup'),
                        hoverable: true,
                    });
                    generateAll($item.closest('.racks').attr('id'));
                }
            });
        });
    }

    function generateAll(selector){
        generateJson(selector);
        generateTable(selector);
        edited = true;
    }

    function generateJson(selector){
        var rack = $("#"+selector);
        var output = [];
        
        rack.children('.shelving').each(function(){
            var shelf = $(this);
            var row = new Array;
            var position = 0;

            shelf.children('.prod').each(function(){
                var prod = $(this);
                var kode = prod.data('kode').toString();
                var nama = prod.data('nama').toString();
                var barcode = prod.data('barcode').toString();
                var find = row.find(function( obj ) {
                  return obj.kode == kode;
                });

                if(typeof find !== 'undefined'){
                    var produk = {
                        stack: prod.children('img').length,
                        urutan: position,
                    };

                    var facing = find.items.reduce( function( sum, key ){
                        return sum + parseFloat( key.stack );
                    }, 0 ) + prod.children('img').length;

                    find.facing = facing;
                    find.unit = 8 * facing;
                    find.items.push(produk);
                }else{
                    var produk = {
                        kode: kode,
                        nama: nama,
                        barcode: barcode,
                        unit: 8,
                        facing: prod.children('img').length,
                        items: [{
                            stack: prod.children('img').length,
                            urutan: position,
                        }]
                    };
                    row.push(produk);
                }

                position++;
            });
            output.push(row);
        });
        // console.log(output);
        
        var json = JSON.stringify(output);
        $(".jsonHolder[data-rack='"+selector+"']").html(json);
    }

    function generateTable(selector){
        var rack = $("#"+selector);
        var table = $("#table-"+selector);
        var json = $.parseJSON($(".jsonHolder[data-rack='"+selector+"']").html());

        for (var row in json){
            var i = 1;
            var tr = table.find('tbody[data-row='+(parseInt(row)+1)+']');

            tr.find('tr').not($('.negative')).remove();      
            if(json[row].length == 0){
                tr.append(`<tr class="empty">
                               <td colspan="6" class="center aligned empty" style="padding: 25px;"><i>Belum ada produk</i></td> 
                           </tr>`);
            }else{
                for (var prod in json[row]){
                    var object = json[row][prod];
                    var html = `<tr>
                                  <td>`+i+`</td>
                                  <td>`+object.kode+`</td>
                                  <td>`+object.barcode+`</td>
                                  <td>`+object.nama+`</td>
                                  <td>`+object.facing+`</td>
                                  <td><div class="ui mini input">
                                    <input type="number" class="unit input" data-rack="`+selector+`" data-row="`+(parseInt(row)+1)+`" data-code="`+object.kode+`" value="`+object.unit+`"/>
                                  </div></td>   
                               </tr>`;

                    tr.append(html);

                    i++;
                }
            }
        }
        
        // rack.children('.shelving').each(function(){
        //     var shelf = $(this);
        //     var i = 1;
        //     var tr = table.find('tbody[data-row='+shelf.data('row')+']');

        //     tr.find('tr').not($('.negative')).remove();      
        //     if(shelf.children('.prod').length == 0){
        //         tr.append(`<tr class="empty">
							 //   <td colspan="6" class="center aligned empty" style="padding: 25px;"><i>Belum ada produk</i></td>	
						  //  </tr>`);
        //     }

        //     shelf.children('.prod').each(function(){
        //         var prod = $(this);
        //         var kode = prod.data('kode');
        //         var nama = prod.data('nama');
        //         var barcode = prod.data('barcode');
        //         var row = `<tr>
        //                       <td>`+i+`</td>
        //                       <td>`+kode+`</td>
        //                       <td>`+barcode+`</td>
        //                       <td>`+nama+`</td>
        //                       <td>`+prod.children('img').length+`</td>
        //                       <td><div class="ui mini input">
        //                         <input type="number" class="unit input" data-rack="`+selector+`" data-row="`+shelf.data('row')+`" value="8"/>
        //                       </div></td>	
        //                    </tr>`;

        //         tr.append(row);

        //         i++;
        //     });
        // });
    }
</script>