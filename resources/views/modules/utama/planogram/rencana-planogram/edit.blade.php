@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/sortable/jquery-sortable.css') }}">
@append
@section('js')
	<script type="text/javascript" src="{{ asset('plugins/jQuery/lodash.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/sortable/jquery-sortable.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/jQuery/jquery.redirect.js') }}"></script>
@append

@section('scripts')
	@include('modules.utama.planogram.scripts.detail')
@endsection

@section('content-body')
	<div class="ui top attached tabular menu">
		@foreach ($rak as $k => $row)
			<div class="@if($k==0) active @endif item" data-tab="{{ $row->id }}" data-rack="{{ $row->id }}">Rak {{ $row->tipe_rak }}</div>
		@endforeach
	</div>
	@foreach ($rak as $k => $row)
		<div class="ui bottom attached @if($k==0) active @endif tab segment" data-tab="{{ $row->id }}">
			@if($row->planogram->count() > 0)
				@include('modules.utama.planogram.rencana-planogram.racks', ['rack' => $row, 'planogram' => $record, 'data' => $row->planogram])
			@else
				@include('modules.utama.planogram.rencana-planogram.racks', ['rack' => $row, 'planogram' => $record])
			@endif
		</div>
	@endforeach
@endsection