@section('scripts')
	<script type="text/javascript">
		$(document).ready(function($) {
		});
	</script>
@append

<div class="tabs for racks">
	<div class="ui left floated tiny pagination menu">
		@if($rack->planogram->count() > 0)
			@foreach($rack->planogram as $plano)
				<a class="@if($plano->nomor_rak == 1)) active @endif item" data-tab="{{$rack->id}}-{{$plano->nomor_rak}}" data-rack="{{ $rack->id }}" data-nomor="{{$plano->nomor_rak}}">{{$plano->nomor_rak}}</a>
			@endforeach	
		@else
		<a class="active item" data-tab="{{$rack->id}}-1">
			1
		</a>
		@endif
		<!--a class="disabled item">
			<i class="chevron left fitted icon" style="margin:0"></i>
		</a-->
		<!--a class="item">
			<i class="chevron right fitted icon" style="margin:0"></i>
		</a-->
	</div>
	<a class="ui left floated blue add-rack button" data-rack="{{$rack->id}}" data-plano="{{$record->id}}">
		<i class="plus icon"></i>
		Tambah Rak
	</a>
	{{-- <a class="ui left floated red remove-rack icon button" data-rack="{{$rack->id}}" data-plano="{{$record->id}}" data-content="Hapus Rak">
		<i class="remove icon"></i>
	</a> --}}
	{{-- <button type="button" class="ui right floated green button">
		<i class="file excel outline icon"></i>
		Import Data
	</button> --}}
	<button type="button" class="ui right floated teal save button" data-id="{{$rack->id}}">
		<i class="save icon"></i>
		Simpan Data
	</button>
	<h2 class="ui center aligned header" style="margin-top: 0">
		Rak {{ $rack->tipe_rak }} #<span class="nomor-{{$rack->id}}">{{ $data->nomor_rak or '1' }}</span>
	</h2>
	<div class="ui clearing divider"></div>
	@if($rack->planogram->count() > 0)
		@foreach($rack->planogram as $i => $plano)
			<div class="ui rack tab basic @if($i==0) active @endif segment" data-tab="{{$rack->id}}-{{ $plano->nomor_rak }}" data-nomor="{{ $plano->nomor_rak }}">
				@include('modules.utama.planogram.rencana-planogram.rack', ['rack' => $rack, 'data' => $plano])
			</div>
		@endforeach	
	@else
		<div class="ui rack tab basic active segment" data-tab="{{$rack->id}}-1" data-nomor="1">
			@include('modules.utama.planogram.rencana-planogram.rack', ['rack' => $rack, 'data' => null])
		</div>
	@endif
</div>
