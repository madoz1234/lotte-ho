<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Produk</div>
<div class="content">
	<form class="ui data form">
		<div class="ui error message"></div>
		<form class="ui filter form">
			<div class="fields">
				<div class="field">
					<div class="ui fluid selection dropdown">
						<input type="hidden" name="filter[entri]">
						<i class="dropdown icon"></i>
						<div class="default text">Entries</div>
						<div class="menu">
							<div class="item" data-value="10">10</div>
							<div class="item" data-value="25">25</div>
							<div class="item" data-value="50">50</div>
							<div class="item" data-value="100">100</div>
						</div>
					</div>
				</div>
				<div class="field">
					<input name="filter[produk_kode]" placeholder="Kode" type="text">
				</div>
				<div class="field">
					<input name="filter[nama]" placeholder="Nama Produk" type="text">
				</div>
				<div class="field">
					<input name="filter[bumun_nm]" placeholder="Division" type="text">
				</div>
				<div class="field">
					{{-- <input name="filter[l1_nm]" placeholder="Category 1" type="text"> --}}
					<select name="filter[l1_nm]" class="ui fluid search selection dropdown">
						{!! \Lotte\Models\Master\Kategori1::options('nama', 'nama',[], 'Pilih Kategori 1') !!}
					</select>
				</div>
				<div class="field">
					<select name="filter[l2_nm]" class="ui fluid search selection dropdown">
						{!! \Lotte\Models\Master\Kategori2::options('nama', 'nama',[], 'Pilih Kategori 2') !!}
					</select>
				</div>
				<div class="field">
					<select name="filter[l3_nm]" class="ui fluid search selection dropdown">
						{!! \Lotte\Models\Master\Kategori3::options('nama', 'nama',[], 'Pilih Kategori 3') !!}
					</select>
				</div>
				<button type="button" class="ui teal icon filter button" data-content="Cari Data">
					<i class="search icon"></i>
				</button>
				<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
					<i class="refresh icon"></i>
				</button>
			</div>
		</form>
        <table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
            <thead>
                <tr>
				    <th>No</th>
				    <th>Kode</th>
				    <th>Nama Produk</th>
				    <th>Division</th>
				    <th>Category 1</th>
				    <th>Category 2</th>
				    <th>Category 3</th>
				    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
	</form>	
</div>

<div class="actions">
	<div class="ui negative button">
		Tutup
	</div>
</div>