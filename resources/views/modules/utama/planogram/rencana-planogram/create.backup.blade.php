<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">BUAT</div>
<div class="content">
	<form class="ui data form" id="dataForm">
		<div class="ui error message"></div>
		<div class="ui form">
				{{-- <div class="inline fields">
				        <div class="ui labeled input" id="from">
				            <div class="ui input left icon">
				                <input name="filter[fiscal_mulai]" type="text" placeholder="Nama Produk">
				            </div>
				        </div> &nbsp;&nbsp;
				    <button type="button" class="ui teal icon filter button" data-content="Cari Data">
				        <i class="search icon"></i>
				    </button>
				    <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
				        <i class="refresh icon"></i>
				    </button>
				</div> --}}

				  <div class="inline fields">
				    <label class="two wide field">Tipe Rak</label>
				    <div class="six wide field">:&nbsp;&nbsp;
				      <input placeholder="Tipe Rak" type="text" disabled="">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="two wide field">Kode Planogram</label>
				    <div class="six wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan Kode Planogram" type="text">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="two wide field">Nama Planogram</label>
				    <div class="six wide field">:&nbsp;&nbsp;
				      <input placeholder="Inputkan Nama Planogram" type="text">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="two wide field">Assortment Type</label>
				    <div class="six wide field">:&nbsp;&nbsp;
				      {{-- <input placeholder="Inputkan Jumlah Unit Rak" type="number" disabled=""> --}}
				      <div class="field">
				      	<div class="ui radio checkbox">
				      		<input type="radio" name="1" tabindex="0" class="hidden">
				      		<label>500</label>
				      	</div>
				      </div>
				      <div class="field">
				      	<div class="ui radio checkbox">
				      		<input type="radio" name="2" tabindex="0" class="hidden">
				      		<label>800</label>
				      	</div>
				      </div>
				      <div class="field">
				      	<div class="ui radio checkbox">
				      		<input type="radio" name="3" tabindex="0" class="hidden">
				      		<label>1500</label>
				      	</div>
				      </div>
				      <div class="field">
				      	<div class="ui radio checkbox">
				      		<input type="radio" name="4" tabindex="0" class="hidden">
				      		<label>2500</label>
				      	</div>
				      </div>
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="two wide field">Jumlah Unit Rak</label>
				    <div class="six wide field">:&nbsp;&nbsp;
				      <input placeholder="Jumlah Unit Rak" type="number" disabled="">
				    </div>
				  </div>

				  <div class="inline fields">
				    <label class="two wide field">TMUK</label>
				    <div class="six wide field">:&nbsp;&nbsp;
				      <select class="ui fluid search dropdown" id="tmuk" multiple>
				      	<option value="">Inputkan TMUK</option>
						<option value="1">0600100010 - Etokokoe</option>
						<option value="2">0602600012 - Berkah</option>
						<option value="3">0601800002 - Jernih</option>
						<option value="4">0600700018 - Pragma Informatika</option>
					</select>
				    </div>
				    <div id="dtBox"></div>
					<div class="ui toggle checkbox">
						<input type="checkbox" id="selectall">
						<label>Select all</label>
					</div>
				  </div>
				</div>
<script type="text/javascript">
	$(document).ready(function(e){
		$('.ui.checkbox').checkbox({
		    onChecked() {
		      const options = $('#tmuk > option').toArray().map(
		        (obj) => obj.value
		      );
		      $('#tmuk').dropdown('set exactly', options);
		    },
		    onUnchecked() {
		      $('#tmuk').dropdown('clear');
		    },
		});
	});
</script>
	</form>

	
</div>

{{-- <div class="inline fields">
  <div class="six wide field">
   <img class="ui big image" src="{{ asset('img/plano.png') }}" style="width: 100%; height: 1000px;">
  </div>
</div> --}}

<div class="ui equal width grid">
	<div class="column">
		<div class="ui segment">
			<form action="" class="ui form">
							<div class="two fields">
								<div class="field">		
									<div class="ui calendar" id="from">
										<div class="ui input left icon">
											<img src="{{ asset('img/plano.png') }}" style="width: 100%; height: 800px;">
										</div>
									</div>
								</div>
								<div class="field">
								<div class="inline fields">
				        			<div class="ui labeled input" id="from">
				        			    <div class="ui input left icon">
				        			        <input name="filter[fiscal_mulai]" type="text" placeholder="Nama Produk">
				        			    </div>
				        			</div> &nbsp;&nbsp;
				    					<button type="button" class="ui teal icon filter button" data-content="Cari Data">
				    					    <i class="search icon"></i>
				    					</button>
				    					<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
				    					    <i class="refresh icon"></i>
				    					</button>
								</div>	<br>
									<div class="ui calendar" id="to">
										<div class="ui input left icon">
											<table id="listTable" class="ui celled compact red table" width="100%" cellspacing="0">
			                					<thead>
			                    					<tr>
			                    						<th class="center aligned">No</th>
			                    						<th class="center aligned">Gambar</th>
			                    						<th class="center aligned">Kode Produk</th>
			                    						<th class="center aligned">Nama Produk</th>
			                    					</tr>
			                					</thead>
			                					<tbody>
			                						<tr>
														<td class="center aligned">1</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>
													<tr>
														<td class="center aligned">2</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>
													<tr>
														<td class="center aligned">3</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>
													<tr>
														<td class="center aligned">4</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>
													<tr>
														<td class="center aligned">5</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>
													<tr>
														<td class="center aligned">6</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>
													<tr>
														<td class="center aligned">7</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>
													<tr>
														<td class="center aligned">8</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>
													<tr>
														<td class="center aligned">9</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>
													<tr>
														<td class="center aligned">10</td>
														<td class="center aligned"><img src="{{ asset('img/plano.png') }}" style="width: 50px; height: 50px;"></td>
														<td class="right aligned">308</td>
														<td class="center aligned">ABC SUSU</td>
													</tr>

			                					</tbody>
			            					</table>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div class="ui buttons">
						  <button class="ui labeled icon button">
						    <i class="left chevron icon"></i>
						    Back
						  </button>
						
						  <button class="ui right labeled icon button">
						    Next
						    <i class="right chevron icon"></i>
						  </button>
						</div>
		</div>
	</div>
</div>
						
{{-- <div class="row">
    <div class="two wide column">
    	<img src="{{ asset('img/plano.png') }}" style="width: 100%; height: 1000px;">
    </div>
</div> --}}


<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>