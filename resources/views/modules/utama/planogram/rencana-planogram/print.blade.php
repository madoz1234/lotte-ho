<style type="text/css">
<!--

/*end_last_page div{
    border: solid 1mm;
    height: 27mm;
    margin: 0;
    padding: 0;
    text-align: center;
}*/

table.page_header {width: 100%; border: none; padding: 0; font-size: 12px;}
table.page_footer {width: 100%; border: none; border-top: solid 0.3mm #000000; padding: 0; font-size: 11px;}
table.page_content_header {width: 100%; border: none; padding: 0; font-size: 11px; background-color:#DDDDFF; border-top: solid 0.3mm #000000; border-bottom: solid 0.3mm #000000;}
table.page_content {width: 100%; border: none; padding: 2mm; font-size: 11px;}

.title{
    font-weight: bold;
}
.tittleMargin{
    padding: 10;
    margin: 2px;
    font-size: 24px;
}
.content-title{
	text-align:center; font-size: 24px; font-weight: bold;
}
.plano-table td, .plano-table th{
	border: 1px solid #555;
	padding: 5px;
}

#text_header{
    width: 100%;text-align: right; padding-bottom: 10px; padding-top: 10px; padding-right: 20px;
}
-->


.racks.segment{
  padding:0
  }/* Planogram Shelving */
.shelving {
  border-bottom: 5px solid rgba(34, 36, 38, 0.15);
  padding: 0;
  margin: 0;
  display: flex;
  position: relative;
  background-color: #fefefe;
  list-style: none;
}
.shelving:last-child {
  border-bottom: none;
}
.shelving > .prod{
  list-style: none;
  display: inline-block;
  align-self: flex-end;
}

.shelving > .prod > img{
  display: block;
}

.ui.compact.grid > .row{
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
}

img.empty{
  background: url('/img/empty.png');
  text-align: center;
  color: #fff;
  font-size: .8em;
  font-weight: bold;
  -moz-box-shadow:    inset 0 0 3px #ff0000;
  -webkit-box-shadow: inset 0 0 3px #ff0000;
  box-shadow:         inset 0 0 3px #ff0000;
}

</style>

@foreach ($rak as $k => $rack)
	@if($rack->planogram->count() > 0)
		@foreach($rack->planogram as $plano)
<page backtop="40mm" backbottom="3mm" backleft="-3mm" backright="0mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td id="" style="; width: 90px; height: 5px;">
                    <img src="{{ asset('img/logo_print.jpeg') }}" width="100" style="margin-top: 10px; margin-right: -3px;">
                </td>
                <td id="text_header" style=" width: 870px; height: 5px; text-align: center;">
                    <p class="title" style=""><h3>{{ $record->nama }}</h3>
                        <hr style="border: #F90606; margin-top: -6px;" >
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td style="width: 210px; padding: 10px"><b>Tanggal Cetak</b> : {{ \Carbon\Carbon::now()->format('d/m/Y h:i:s') }}</td>
                			<td style="width: 270px; padding: 10px"><b>Tahun Fiskal</b> : {{ $fiskal['tgl_awal'] }} - {{ $fiskal['tgl_akhir'] }} (Active)</td>
                			<td style="width: 180px; padding: 10px"><b>Kode Planogram</b> : {{ $record->kode }}</td>
                			<td style="width: 158px; padding: 10px"><b>Assortment Type</b> : {{ $record->assortment->nama }}</td>
						</tr>
					</table>
                </td>
            </tr>
        </table>
    </page_header>
    <table class="page_content" border="0" align="center">
        <tr>
        	<td style="width: 965px;" class="content-title" colspan="4">Rak {{ $rack->tipe_rak }} #{{ $plano->nomor_rak }}</td>
        </tr>
        <tr><td colspan="4"><br></td></tr>
        <tr>
        	<td style="width: 40px"></td>
        	<td style="width: 280px; text-align: left; vertical-align: top">
        		<?php
        			$ratio = 280 / $rack->panjang_rak;
        		?>
				<div class="ui racks segment" id="rack-{{$rack->id}}-{{ $plano->nomor_rak or '1' }}">
					@if(isset($plano))
						@foreach($plano->renderRack() as $i => $row)
							<div class="shelving nav" data-row="{{ $i+1 }}" style="height: {{ ($rack->tinggi_rak * $ratio) / $rack->shelving }}px; margin: 0 auto; vertical-align: bottom">
								<?php 
									$max = array_reduce($row, function ($a, $b) {
									    return @$a['stack'] > $b['stack'] ? $a : $b ;
									});
								?>
								@for($x=$max['stack']; $x>0; $x--)
									<div class="prod">
									@foreach($row as $prod)
										@if($prod['stack'] < $x)
							                <img src="{{ url('img/white.png') }}" 
							                	 width="{{ $prod['produk']['produksetting']['uom1_width'] / 10 * $ratio }}" 
							                	 height="{{ $prod['produk']['produksetting']['uom1_height'] / 10 * $ratio }}" 
							                	 alt="{{ $prod['produk']['nama'] }}">
										@else
											@if($prod['produk']['produksetting']['uom1_file'] == '0')
							                <img src="{{ url('img/empty.png') }}" 
							                	 width="{{ $prod['produk']['produksetting']['uom1_width'] / 10 * $ratio }}" 
							                	 height="{{ $prod['produk']['produksetting']['uom1_height'] / 10 * $ratio }}" 
							                	 alt="{{ $prod['produk']['nama'] }}">
						                	 @else
							                <img src="{{ url('storage/'.$prod['produk']['produksetting']['uom1_file']) }}" 
							                	 width="{{ $prod['produk']['produksetting']['uom1_width'] / 10 * $ratio }}" 
							                	 height="{{ $prod['produk']['produksetting']['uom1_height'] / 10 * $ratio }}" 
							                	 alt="{{ $prod['produk']['nama'] }}">
							                @endif
						                @endif
									@endforeach
						            </div>
					            @endfor
							</div>
						@endforeach
					@else
						@for($i=1; $i <= $rack->shelving; $i++)
							<div class="shelving nav" data-row="{{ $i }}">
							</div>
						@endfor
					@endif
				</div>
        	</td>
        	<td style="width: 80px"></td>
        	<td style="width: 540px; vertical-align: top">
				<table cellpadding="0" cellspacing="0" class="plano-table" data-rack="rack-{{$rack->id}}-{{ $plano->nomor_rak or '1' }}" id="table-rack-{{$rack->id}}-{{ $plano->nomor_rak or '1' }}">
					<thead>
						<tr>
							<th style="width: 5px">#</th>
							<th style="width: 70px">Kode Produk</th>
							<th style="width: 70px">Barcode</th>
							<th style="width: 250px">Nama Produk</th>
							<th>Facing</th>
							<th>Unit</th>
						</tr>
					</thead>
					@if(isset($plano))
						<?php $j=1; ?>
						@foreach($plano->planogram_array as $i => $row)
							<tbody data-row="{{$i+1}}">
								<tr class="negative">
									<th colspan="6" style="text-align:center">Tingkat #{{$i+1}}</th>
								</tr>
								@if(count($row) > 0)
								@foreach($row as $prod)
								<tr>
	                              <td>{{$j++}}</td>
	                              <td>{{ $prod->kode }}</td>
	                              <td>{{ $prod->barcode }}</td>
	                              <td style="width: 250px">{{ $prod->nama }}</td>
	                              <td>{{ $prod->facing }}</td>
	                              <td>{{ $prod->unit }}</td>   
	                           </tr>
	                           @endforeach
	                           @else
								<tr class="empty">
									<td colspan="6" style="text-align:center; padding: 10px"><i>Belum ada produk</i></td>	
								</tr>
	                           @endif
							</tbody>
						@endforeach
					@else
						@for($i=1; $i <= $row->shelving; $i++)
							<tbody data-row="{{$i}}">
								<tr class="negative">
									<th colspan="6" style="text-align: center">Tingkat #{{$i}}</th>
								</tr>
								<tr class="empty">
									<td colspan="6" style="text-align: center; padding: 10px"><i>Belum ada produk</i></td>	
								</tr>
							</tbody>
						@endfor
					@endif
				</table>
        	</td>
        </tr>
    </table>
</page>
        @endforeach
    @endif
@endforeach