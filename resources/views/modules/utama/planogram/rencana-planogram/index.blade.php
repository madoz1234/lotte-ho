@extends('layouts.grid')

@section('filters')
<div class="field">
	<input name="filter[kode]" placeholder="Kode Planogram" type="text">
</div>
<div class="field">
	<input name="filter[nama]" placeholder="Nama Planogram" type="text">
</div>
<div class="field">
	<select name="filter[id_assortment_type]" class="ui fluid selection dropdown">
		<option value="">Semua Assortment Type</option>
		@foreach ($assortment as $row)
			<option value="{{$row->id}}">{{$row->nama}}</option>
		@endforeach
	</select>
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
	<button type="button" class="ui blue add button"><i class="plus icon"></i>Tambah Data</button>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		kode: 'empty',
		nama: 'empty',
		jenis_assortment_id: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script type="text/javascript">
	initModal = function(){
		$('.ui.radio.checkbox').checkbox();
	};
	doneApproved = function(resp){
		var data = resp.data;
		window.location.href = "{{url($pageUrl)}}/"+data.id+"/edit";
	};
</script>
@endsection
{{-- 
@section('tableBody')
<tr>
	<td>1</td>
	<td>PLG001</td>
	<td>Planogram 2018 - Semester 1</td>
	<td>1500</td>
	<td><div class="ui small green label">Aktif</div></td>
	<td>2 Jam yang lalu</td>
	<td>
		<a href="{{url($pageUrl)}}/1/edit" class="ui mini orange icon view button" data-content="View Data" data-id="1"><i class="search icon"></i></a>
		<a href="{{url($pageUrl)}}/1" class="ui mini green icon analisis button" data-content="Analisis Data" data-id="1"><i class="file text icon"></i></a>
		<button type="button" class="ui mini red icon delete button" data-content="Hapus Data" data-id="1"><i class="trash icon"></i></button>
	</td>
</tr>
<tr>
	<td>2</td>
	<td>PLG002</td>
	<td>Planogram 2017 - Semester 2</td>
	<td>2500</td>
	<td><div class="ui small red label">Tidak Aktif</div></td>
	<td>3 Jam yang lalu</td>
	<td>
		<a href="{{url($pageUrl)}}/1/edit" class="ui mini orange icon view button" data-content="View Data" data-id="1"><i class="search icon"></i></a>
		<a href="{{url($pageUrl)}}/1" class="ui mini green icon analisis button" data-content="Analisis Data" data-id="1"><i class="file text icon"></i></a>
		<button type="button" class="ui mini red icon delete button" data-content="Hapus Data" data-id="1"><i class="trash icon"></i></button>
	</td>
</tr>
@endsection --}}