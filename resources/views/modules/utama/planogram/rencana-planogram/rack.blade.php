{{-- {!! dd($data->renderRack()) !!} --}}
<div class="ui grid">
	<div class="seven wide column">
		<div class="ui racks segment" id="rack-{{$rack->id}}-{{ $data->nomor_rak or '1' }}">
			@if(!is_null($data) && count($data->planogram_array) > 0)
				@foreach($data->renderRack() as $i => $row)
					<ol class="shelving nav" data-row="{{ $i+1 }}">
						<button type="button" class="ui append product green icon mini button" data-row="{{ $i+1 }}" data-rack="rack-{{$rack->id}}-{{ $data->nomor_rak or '1' }}" data-rack-number="{{ $data->nomor_rak or '1' }}"><i class="plus icon"></i></button>
						@foreach($row as $prod)
							<li class="prod transition hidden" data-kode="{{ $prod['produk']['kode'] }}" data-nama="{{ $prod['produk']['nama'] }}" data-barcode="{{ $prod['produk']['produksetting']['uom1_barcode'] }}" style="display: list-item !important;">
							@for($x=0; $x<$prod['stack']; $x++)
				                <img src="{{ url('storage/'.$prod['produk']['produksetting']['uom1_file']) }}" data-width="{{ $prod['produk']['produksetting']['uom1_width'] }}" data-height="{{ $prod['produk']['produksetting']['uom1_height'] }}" class="{{ $prod['produk']['produksetting']['uom1_file'] == '0' ? 'empty' : 'normal' }}" alt="{{ $prod['produk']['nama'] }}">
				                <div class="ui flowing popup top left transition hidden">
				                      <h4 class="ui header">{{$prod['produk']['kode']}}</h4>
				                      <p style="white-space:nowrap">{{$prod['produk']['nama']}}</p>
				                      <div style="position:absolute; top:5px; right:5px;">
				                          <div class="ui mini stack product blue icon button"><i class="upload icon"></i></div>
				                          <div class="ui mini remove product red icon button"><i class="remove icon"></i></div>
				                      </div>
				                </div>
				             @endfor
				             </li>
						@endforeach
					</ol>
				@endforeach
			@else
				@for($i=1; $i <= $rack->shelving; $i++)
					<ol class="shelving nav" data-row="{{ $i }}">
						<button type="button" class="ui append product green icon mini button" data-row="{{ $i }}" data-rack="rack-{{$rack->id}}-{{ $data->nomor_rak or '1' }}" data-rack-number="1"><i class="plus icon"></i></button>
					</ol>
				@endfor
			@endif
		</div>
	</div>
	<div class="nine wide column">
		<div class="ui segment">
			<h3 class="ui center aligned header">Detail Posisi Produk</h3>
			<table class="ui bordered small compact sorted_table table" data-rack="rack-{{$rack->id}}-{{ $data->nomor_rak or '1' }}" id="table-rack-{{$rack->id}}-{{ $data->nomor_rak or '1' }}">
				<thead>
					<tr>
						<th>#</th>
						<th>Kode Produk</th>
						<th>Barcode</th>
						<th>Nama Produk</th>
						<th>Facing</th>
						<th>Unit</th>
					</tr>
				</thead>
				@if(!is_null($data) && count($data->planogram_array) > 0)
					<?php $j=1; ?>
					@foreach($data->planogram_array as $i => $row)
						<tbody data-row="{{$i+1}}">
							<tr class="negative">
								<th colspan="6" class="center aligned">Tingkat #{{$i+1}}</th>
							</tr>
							@if(count($row) > 0)
							@foreach($row as $prod)
							<tr>
                              <td>{{$j++}}</td>
                              <td>{{ $prod->kode }}</td>
                              <td>{{ $prod->barcode }}</td>
                              <td>{{ $prod->nama }}</td>
                              <td>{{ $prod->facing }}</td>
                              <td><div class="ui mini input">
                                <input type="number" class="unit input" data-rack="rack-{{$rack->id}}-{{ $data->nomor_rak or '1' }}" data-row="{{$i+1}}" data-code="{{ $prod->kode }}" value="{{ $prod->unit }}"/>
                              </div></td>   
                           </tr>
                           @endforeach
                           @else
							<tr class="empty">
								<td colspan="6" class="center aligned empty" style="padding: 25px;"><i>Belum ada produk</i></td>	
							</tr>
                           @endif
						</tbody>
					@endforeach
				@else
					@for($i=1; $i <= $rack->shelving; $i++)
						<tbody data-row="{{$i}}">
							<tr class="negative">
								<th colspan="6" class="center aligned">Tingkat #{{$i}}</th>
							</tr>
							<tr class="empty">
								<td colspan="6" class="center aligned empty" style="padding: 25px;"><i>Belum ada produk</i></td>	
							</tr>
						</tbody>
					@endfor
				@endif
			</table>
		</div>
		<form class="ui rack form" id="form-rack-{{$rack->id}}-{{ $data->nomor_rak or '1' }}" method="POST" action="{{ url($pageUrl.'save') }}">
			{!! csrf_field() !!}
			<input type="hidden" name="detail[id]" value="{{ $data->id or '' }}">
			<input type="hidden" name="detail[planogram_id]" value="{{ $data->planogram_id or $planogram->id }}">
			<input type="hidden" name="detail[rak_id]" value="{{ $data->rak_id or $rack->id }}">
			<input type="hidden" name="detail[nomor_rak]" value="{{ $data->nomor_rak or '1' }}">
			<div class="field">
				<textarea class="ui input transition hidden jsonHolder" name="detail[planogram_json]" data-rack="rack-{{$rack->id}}-{{ $data->nomor_rak or '1' }}" placeholder="JSON HOLDER">{{ $data->planogram_json or '' }}</textarea>
			</div>
		</form>
	</div>
	<!--div class="sixteen wide center aligned column">
		<div class="ui pagination menu">
			<a class="disabled item">
				<i class="chevron left fitted icon" style="margin:0"></i>
			</a>
			<a class="active item">
				1
			</a>
			<a class="item">
				2
			</a>
			<a class="item">
				3
			</a>
			<a class="item">
				4
			</a>
			<a class="item">
				<i class="chevron right fitted icon" style="margin:0"></i>
			</a>
		</div>
	</div-->
</div>