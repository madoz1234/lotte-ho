<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Data Planogram</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui error message"></div>
		<div class="inline fields">
			<label class="four wide field">Kode Planogram</label>
			<span class="one wide field">
				:&nbsp;&nbsp;
			</span>
			<div class="eleven wide field">
				<input placeholder="Inputkan Kode Planogram" name="kode" type="text">
			</div>
		</div>

		<div class="inline fields">
			<label class="four wide field">Nama Planogram</label>
			<span class="one wide field">
				:&nbsp;&nbsp;
			</span>
			<div class="eleven wide field">
				<input placeholder="Inputkan Nama Planogram" name="nama" type="text">
			</div>
		</div>

		<div class="inline fields">
			<label class="four wide field">Assortment Type</label>
			<span class="one wide field">
				:&nbsp;&nbsp;
			</span>
			<div class="eleven wide field">
				@foreach ($assortment as $jenis)
					<div class="field">
						<div class="ui radio checkbox">
							<input type="radio" name="jenis_assortment_id" tabindex="0" class="hidden" value="{{ $jenis->id }}">
							<label>{{ $jenis->nama }}</label>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</form>	
</div>

<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>