@extends('layouts.grid')

@section('content-body')
	<div class="ui right floated" style="position: absolute; right: 28px; ">
		<label>Pilih TMUK : </label>
		<div class="ui small input">
			<select class="ui dropdown">
				<option value="">Pilih TMUK</option>
				<option value="1">TMUK Jernih</option>
			</select>
		</div>
	</div>
	<div class="ui top attached tabular menu">
		<div class="active item" data-tab="1">Rak Lorong</div>
		<div class="item" data-tab="2">Rak Dinding</div>
		<div class="item" data-tab="3">Rak Kasir</div>
	</div>
	<div class="ui bottom attached active tab segment" data-tab="1">
		<h2 class="ui center aligned header">
			Rak Lorong #1
		</h2>
		<div class="ui clearing divider"></div>
		<div class="ui grid">
			<div class="five wide column">
				<div class="ui segment">
					<img src="{{ asset('img/products/im_ab.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_ab.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_st.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_sg.png') }}" width="62px">
					<img src="{{ asset('img/products/sedap_kari.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_kari.jpg') }}" width="62px">
					<hr class="ui clearing divider">
					<img src="{{ asset('img/products/im_goreng.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_goreng.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_ip.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_goreng.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_goreng.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_soto.jfif') }}" width="62px">
					<hr class="ui clearing divider">
					<img src="{{ asset('img/products/im_ab.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_ab.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_st.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_sg.png') }}" width="62px">
					<img src="{{ asset('img/products/sedap_kari.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_kari.jpg') }}" width="62px">
					<hr class="ui clearing divider">
					<img src="{{ asset('img/products/im_goreng.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_goreng.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_ip.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_goreng.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_goreng.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_soto.jfif') }}" width="62px">
					<hr class="ui clearing divider">
					<img src="{{ asset('img/products/im_ab.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_ab.jpg') }}" width="62px">
					<img src="{{ asset('img/products/im_st.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_sg.png') }}" width="62px">
					<img src="{{ asset('img/products/sedap_kari.jpg') }}" width="62px">
					<img src="{{ asset('img/products/sedap_kari.jpg') }}" width="62px">
					<hr class="ui clearing divider">
				</div>
			</div>
			<div class="eleven wide column">
				<div class="ui segment">
					<h3 class="ui center aligned header">Detail Posisi Produk</h3>
					<table class="ui bordered small compact table">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Produk</th>
								<th>Barcode</th>
								<th>Nama Produk</th>
								<th>Facing</th>
								<th>Unit</th>
								<th>Selling Rate</th>
							</tr>
						</thead>
						<tbody>
							<tr class="negative">
								<th colspan="7" class="center aligned">Tingkat #1</th>
							</tr>
							<tr>
								<td>1</td>
								<td>1102928</td>
								<td>019231903182931</td>
								<td>INDOMIE AYAM BAWANG</td>
								<td>2</td>
								<td>16</td>	
								<td>0.61</td>
							</tr>
							<tr>
								<td>2</td>
								<td>1102929</td>
								<td>019231903182932</td>
								<td>INDOMIE REBUS SOTO</td>
								<td>1</td>
								<td>8</td>	
								<td>0.8</td>
							</tr>
							<tr>
								<td>3</td>
								<td>1103030</td>
								<td>019231903182029</td>
								<td>SUPERMI GORENG SAMBAL</td>
								<td>1</td>
								<td>8</td>	
								<td>0.5</td>
							</tr>
							<tr>
								<td>4</td>
								<td>1103032</td>
								<td>019231903182123</td>
								<td>SUPERMI AYAM GORENG</td>
								<td>1</td>
								<td>8</td>	
								<td>0.5</td>
							</tr>
							<tr class="negative">
								<th colspan="7" class="center aligned">Tingkat #2</th>
							</tr>
							<tr>
								<td>5</td>
								<td>1102933</td>
								<td>019231903182938</td>
								<td>INDOMIE GORENG</td>
								<td>2</td>
								<td>16</td>	
								<td>0.5</td>	
							</tr>
							<tr>
								<td>6</td>
								<td>1102934</td>
								<td>019231903182939</td>
								<td>INDOMIE GORENG CABE HIJAU</td>
								<td>1</td>
								<td>8</td>	
								<td>0.12</td>	
							</tr>
							<tr>
								<td>7</td>
								<td>1103035</td>
								<td>019231903182036</td>
								<td>MIE SEDAP GORENG</td>
								<td>2</td>
								<td>16</td>	
								<td>0.1</td>	
							</tr>
							<tr>
								<td>8</td>
								<td>1103037</td>
								<td>019231903182130</td>
								<td>MIE SEDAP SOTO</td>
								<td>1</td>
								<td>8</td>	
								<td>0.08</td>	
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="sixteen wide center aligned column">
				<div class="ui pagination menu">
					<a class="disabled item">
						<i class="chevron left fitted icon" style="margin:0"></i>
					</a>
					<a class="active item">
						1
					</a>
					<a class="item">
						2
					</a>
					<a class="item">
						3
					</a>
					<a class="item">
						4
					</a>
					<a class="item">
						<i class="chevron right fitted icon" style="margin:0"></i>
					</a>
				</div>
			</div>
		</div>	
	</div>
	<div class="ui bottom attached tab segment" data-tab="2">
		<p></p>
		<p></p>
	</div>
	<div class="ui bottom attached tab segment" data-tab="3">
		<p></p>
		<p></p>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function($) {
			$('.menu .item')
			  .tab()
			;	
		});
	</script>
@endsection