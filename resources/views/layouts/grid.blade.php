@extends('layouts.scaffold')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.semanticui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.css') }}">
@append

@section('js')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    {{-- <script src="{{ asset('plugins/datatables/extensions/FixedColumns/js/dataTables.fixedColumns.js') }}"></script> --}}
    {{-- <script src="{{ asset('plugins/datatables/dataTables.semanticui.min.js') }}"></script> --}}
    <script src="{{ asset('plugins/sweetalert/sweetalert2.js') }}"></script>
@append

@section('scripts')
	<script type="text/javascript">
		// global
	    var dt = "";
	    var formRules = [];
	    var initModal = function(){
	    	return false;
	    };
        var unModal = function(){
            return false;
        };
	    var doneApproved = function(){
	    	return false;
	    };

	    $.fn.form.settings.prompt = {
		    empty                : '{name} tidak boleh kosong',
		    checked              : '{name} harus dipilih',
		    email                : '{name} tidak valid',
		    url                  : '{name} must be a valid url',
		    regExp               : '{name} is not formatted correctly',
		    integer              : '{name} must be an integer',
		    decimal              : '{name} must be a decimal number',
		    number               : '{name} hanya boleh berisikan angka',
		    is                   : '{name} must be "{ruleValue}"',
		    isExactly            : '{name} must be exactly "{ruleValue}"',
		    not                  : '{name} cannot be set to "{ruleValue}"',
		    notExactly           : '{name} cannot be set to exactly "{ruleValue}"',
		    contain              : '{name} cannot contain "{ruleValue}"',
		    containExactly       : '{name} cannot contain exactly "{ruleValue}"',
		    doesntContain        : '{name} must contain  "{ruleValue}"',
		    doesntContainExactly : '{name} must contain exactly "{ruleValue}"',
		    minLength            : '{name} must be at least {ruleValue} characters',
		    length               : '{name} must be at least {ruleValue} characters',
		    exactLength          : '{name} must be exactly {ruleValue} characters',
		    maxLength            : '{name} cannot be longer than {ruleValue} characters',
		    match                : '{name} must match {ruleValue} field',
		    different            : '{name} must have a different value than {ruleValue} field',
		    creditCard           : '{name} must be a valid credit card number',
		    minCount             : '{name} must have at least {ruleValue} choices',
		    exactCount           : '{name} must have exactly {ruleValue} choices',
			maxCount 			 : '{name} must have {ruleValue} or less choices'
		};

	</script>
	@yield('rules')
	@yield('init-modal')
	
	@include('layouts.scripts.datatable')
	@include('layouts.scripts.actions')
@append
@section('content')

@if(config('app.env') == 'testing')

<div class="ui yellow message"><h4 style="text-align: center;">Testing & QC Purpose Only</h4></div><br>


@endif

@yield('breadcrumb-content')
	@section('content-header')
	<div class="ui breadcrumb">
		<?php $i=1; $last=count($breadcrumb);?>
		@foreach ($breadcrumb as $name => $link)
			@if($i++ != $last)
				<a href="{{ $link }}" class="section">{{ $name }}</a>
				<i class="right chevron icon divider"></i>
			@else
				<div class="active section">{{ $name }}</div>
			@endif
		@endforeach
	</div>
	<h2 class="ui header">
	  <div class="content">
	    {!! $title or '-' !!}
	    <div class="sub header">{!! $subtitle or ' ' !!}</div>
	  </div>
	</h2>
	@show

	@section('content-body')
	<div class="ui grid">
		<div class="sixteen wide column main-content">
		    <div class="ui segments">
				<div class="ui segment">
					<form class="ui filter form">
		  				<div class="fields">
		  					<div class="field">
								<div class="ui fluid selection dropdown">
								  <input type="hidden" name="filter[entri]">
								  <i class="dropdown icon"></i>
								  <div class="default text">Entries</div>
								  <div class="menu">
								    <div class="item" data-value="10">10</div>
								    <div class="item" data-value="25">25</div>
								    <div class="item" data-value="50">50</div>
								    <div class="item" data-value="100">100</div>
								  </div>
								</div>
							</div>
							@section('filters')
								<div class="field">
									<input name="filter[kode]" placeholder="Kode Area" type="text">
								</div>
								<div class="field">
									<input name="filter[area]" placeholder="Area" type="text">
								</div>
								<button type="button" class="ui teal icon filter button" data-content="Cari Data">
									<i class="search icon"></i>
								</button>
								<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
									<i class="refresh icon"></i>
								</button>
							@show
							<div style="margin-left: auto; margin-right: 1px;">
								@section('toolbars')
								 <button type="button" class="ui blue add button">
									<i class="plus icon"></i>
									Tambah Data
								</button>
								<button type="button" class="ui green button">
									<i class="file excel outline icon"></i>
									Export Excel
								</button>
								@show
							</div>
						</div>
					</form>

					@section('subcontent')
						@if(isset($tableStruct))
			            <table id="listTable" class="ui celled striped compact red table" width="100%" cellspacing="0">
			                <thead>
			                    <tr>
			                        @foreach ($tableStruct as $struct)
			                            <th>{{ $struct['label'] or $struct['name'] }}</th>
			                        @endforeach
			                    </tr>
			                </thead>
			                <tbody>
			                	@yield('tableBody')
			                </tbody>
			            </table>
			            @endif
		            @show
		        </div>
		    </div>
		</div>
	</div>
	@show
@endsection

@section('modals')
<div class="ui {{ $modalSize or 'mini' }} modal" id="formModal">
	<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
	</div>
</div>

<div class="ui {{ $modalSize or 'mini' }} modal" id="formModal2">
	<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
	</div>
</div>
@append