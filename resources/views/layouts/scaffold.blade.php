@extends('layouts.master')

@section('body')
{{-- <div class="ui sidebar inverted vertical menu">
</div> --}}


<div class="dimmed pusher content">
	<header>
		@include('partials.header')
	</header>

	<message></message>
	<div class="main ui fluid container" id="main-container">
		@yield('content')
	</div>
</div>

<footer class="ui vertical footer fixed segment" style="padding:15px">
	<div class="ui grid">
		<div class="ui eight wide column left aligned">
			<span>&copy; 2018 Lotte Grosir. All rights reserved.</span>
		</div>
		<div class="ui eight wide column right aligned">
			<span>{{ config('app.longname') }}</span>
		</div>
	</div>
</footer>
@endsection
