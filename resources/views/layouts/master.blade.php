<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>TMUK SYSTEM | LOTTE GROSIR</title>
    <link rel="shortcut icon" href="{{ asset('img/auth/fav_logo1.png') }}" sizes="16x13"/>


    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}"> --}}

    {{-- Style --}}

    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/semantic.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style type="text/css">
        .ui.file.input input[type="file"] {
            display: none;
        }
        .ui.button>.ui.floated.label {
            position: absolute;
            top: 15px;
            right: -10px;
        }
        /*.right {
          position:absolute;
          right:0;
          /*top:10px;*/
        } */
    </style>
    @yield('css')
    @yield('styles')
</head>

<!-- disabled inspect element -->
<!-- <body id="app" oncontextmenu="return false;"> -->
<!-- end disabled inspect element -->
<body id="app">

    @yield('body')

    <div v-cloak>
        @yield('additional')
    </div>

    @yield('modals')

    {{-- Script --}}
    <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!}
    </script>
    <script src="{{ asset('js/es6-promise.auto.min.js') }}"></script>
    
    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ asset('plugins/jQuery/jquery.form.min.js') }}"></script>
    <script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <script src="{{ asset('plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('semantic/semantic.min.js') }}"></script>
    <script src="{{ asset('plugins/input-mask-khp/jquery.inputmask.bundle.js') }}"></script>
    
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    @yield('js')
    <script type="text/javascript">
        $(document).ready(function() {
            // document.onkeydown = function(e) {
            //     if(event.keyCode == 123) {
            //         return false;
            //     }
            //     if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
            //         return false;
            //     }
            //     if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            //         return false;
            //     }
            //     if(e.ctrlKey && e.shiftKey && e.keyCode == '123'.charCodeAt(0)) {
            //         return false;
            //     }
            //     if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
            //         return false;
            //     }
            //     if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
            //         return false;
            //     }
            // }
            // initialize and add onChange event
            $('.ui.dropdown').dropdown({
                onChange: function(value) {
                    var target = $(this).dropdown();
                    if (value!="") {
                        target
                            .find('.dropdown.icon')
                            .removeClass('dropdown')
                            .addClass('delete')
                            .on('click', function() {
                                target.dropdown('clear');
                                $(this).removeClass('delete').addClass('dropdown');
                                return false;
                            });
                    }
                }
            });
            // force onChange  event to fire on initialization
            $('.ui.dropdown')
                .closest('.ui.selection')
                .find('.item.active').addClass('qwerty').end()
                .dropdown('clear')
                    .find('.qwerty').removeClass('qwerty')
                .trigger('click');

            $('.message .close').on('click', function() {
                $(this).closest('.message').transition('fade');
            });
            $('.ui.file.input').find('input:text, .ui.button')
              .on('click', function(e) {
                $(e.target).parent().find('input:file').click();
              })
            ;

            $('input:file', '.ui.file.input')
              .on('change', function(e) {
                var file = $(e.target);
                var name = '';

                for (var i=0; i<e.target.files.length; i++) {
                  name += e.target.files[i].name + ', ';
                }
                // remove trailing ","
                name = name.replace(/,\s*$/, '');

                    $('input:text', file.parent()).val(name);
              })
            ;
        });
    </script>
        
    @yield('scripts')
</body>
</html>
