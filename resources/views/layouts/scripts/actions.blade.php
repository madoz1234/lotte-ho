<script type="text/javascript">	
	$(document).on('submit', '#dataForm', function(e){
		return false;
	});

	$(document).on('keydown', '.field input', function(e){
		if(e.keyCode == 13){
			e.preventDefault();
			$( ".save.button" ).trigger( "click" );
		}
	});

	$(document).on('click', '.add.button', function(e){
		var url = "{{ url($pageUrl) }}/create";

		loadModal(url);
	});

	$(document).on('click', '.upload.button', function(e){
		var url = "{{ url($pageUrl) }}/upload";

		loadModal(url);
	});

	$(document).on('click', '.importexcel.button', function(e){
		var url = "{{ url($pageUrl) }}/importexcel";

		loadModal(url);
	});

	$(document).on('click', '.uploadharga.button', function(e){
		var url = "{{ url($pageUrl) }}/uploadharga";

		loadModal(url);
	});

	$(document).on('click', '.importexcel2.button', function(e){
		var url = "{{ url($pageUrl) }}/importexcel2";

		loadModal(url);
	});

	$(document).on('click', '.importexcelopening.button', function(e){
		var url = "{{ url($pageUrl) }}/importexcelopening";

		loadModal(url);
	});

	$(document).on('click', '.importpyrexcel.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/importpyrexcel/"+id;

		loadModal(url);
	});

	//Diskon
	$(document).on('click', '.alldiskon.button', function(e){
		var url = "{{ url($pageUrl) }}/alldiskon";

		loadModal(url);
	});

	//Promo
	$(document).on('click', '.allpromo.button', function(e){
		var url = "{{ url($pageUrl) }}/allpromo";

		loadModal(url);
	});


	$(document).on('click', '.add.button.uom', function(e) {
		addUom();
	});

	$(document).on('click', '.remove.button.uom', function(e) {
		removeUom();
	});

	// $(document).on('click', '.add.button.uomp', function(e) {
	// 	addUomp();
	// });

	// $(document).on('click', '.remove.button.uomp', function(e) {
	// 	removeUomp();
	// });

	$(document).on('click', '.edit.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/"+id+"/edit";

		loadModal(url);
	});

	$(document).on('click', '.detail.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/detail/" + id;

		loadModal(url);
	});

	$(document).on('click', '.detail-pyrs.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/detail-pyr/" + id;

		loadModal2(url);
	});

	$(document).on('click', '.delete.button', function(e){
		var id = $(this).data('id');

		swal({
			title: 'Apakah anda yakin?',
			text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Hapus',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/'+id,
				    type: 'POST',
				    data: {_token: "{{ csrf_token() }}", _method: "delete"},
				    success: function(resp){
						swal(
							'Terhapus!',
							'Data berhasil dihapus.',
							'success'
						).then(function(e){
							dt.draw(false);
						});
				    },
				    error : function(resp){
						swal(
							'Gagal!',
							'Data gagal dihapus.',
							'error'
						).then(function(e){
							dt.draw(false);
						});
				    }
				});
				
			}
		})
	});

	function loadModal(url) {
		$('#formModal').modal({
			inverted: true,
			observeChanges: true,
			closable: false,
			detachable: false, 
			autofocus: false,
			onApprove : function() {
				$("#dataForm").form('validate form');

				if($("#dataForm").form('is valid')){
					$('#formModal').find('.loading.dimmer').addClass('active');
					$("#dataForm").ajaxSubmit({
						success: function(response){
							$("#formModal").modal('hide');
							if(typeof(response.upload) != "undefined") {
							    var sukses = response.sukses;
							    var gagal = response.gagal;
							    var pesan = '';

							    if(typeof(response.log) != "undefined") {
							    	pesan = sukses+' data tersimpan,<br>'+gagal+' data gagal.<br>'+response.log;
							    }else{
							    	pesan = sukses+' data tersimpan,<br>'+gagal+' data gagal.';
							    }

							    if (sukses > gagal) {
								    swal(
										'Berhasil!',
										pesan,
										'success'
									).then((result) => {
										dt.draw(false);
										doneApproved(response);
										return true;
									})
							    }else{
							    	swal(
										'Gagal!',
										pesan,
										'error'
									).then((result) => {
										dt.draw(false);
										doneApproved(response);
										return true;
									})
							    }
							}else{
								swal(
									'Tersimpan!',
									'Data berhasil disimpan.',
									'success'
								).then((result) => {
									dt.draw(false);
									doneApproved(response);
									return true;
								})
							}

						},
						error: function(resp){
							$('#formModal').find('.loading.dimmer').removeClass('active');
							var error = $('<ul class="list"></ul>');
							$.each(resp.responseJSON, function(index, val) {
								// console.log(val)
								error.append('<li>'+val+'</li>');
							});

							$('#formModal').find('.ui.error.message').html(error).show();
						}
					});	
				}
				return false;
			},
			onShow: function(){
				$('#formModal').find('.loading.dimmer').addClass('active');

				$(this).draggable({
		            cancel: ".content" 
		        });

				$.get(url, { _token: "{{ csrf_token() }}" } )
				.done(function( response ) {
					$('#formModal').html(response);
					$('#dataForm').form({
						inline: true,
						fields: formRules
					});

					$('.ui.dropdown').dropdown();
					initModal();
				});
			},
			onHidden: function(){
				$('#formModal').html(`<div class="ui inverted loading dimmer">
										<div class="ui text loader">Loading</div>
									</div>`);
				unModal();
			}
		}).modal('show');
	}

	function loadModal2(url) {
		$('#formModal2').modal({
			inverted: true,
			observeChanges: true,
			closable: false,
			detachable: false, 
			autofocus: false,
			onApprove : function() {
				$("#dataForm").form('validate form');

				if($("#dataForm").form('is valid')){
					$('#formModal2').find('.loading.dimmer').addClass('active');
					$("#dataForm").ajaxSubmit({
						success: function(response){
							$("#formModal2").modal('hide');
							if(typeof(response.upload) != "undefined") {
							    var sukses = response.sukses;
							    var gagal = response.gagal;
							    var pesan = '';

							    if(typeof(response.log) != "undefined") {
							    	pesan = sukses+' data tersimpan,<br>'+gagal+' data gagal.<br>'+response.log;
							    }else{
							    	pesan = sukses+' data tersimpan,<br>'+gagal+' data gagal.';
							    }

							    if (sukses > gagal) {
								    swal(
										'Berhasil!',
										pesan,
										'success'
									).then((result) => {
										dt.draw(false);
										doneApproved(response);
										return true;
									})
							    }else{
							    	swal(
										'Gagal!',
										pesan,
										'error'
									).then((result) => {
										dt.draw(false);
										doneApproved(response);
										return true;
									})
							    }
							}else{
								swal(
									'Tersimpan!',
									'Data berhasil disimpan.',
									'success'
								).then((result) => {
									dt.draw(false);
									doneApproved(response);
									return true;
								})
							}

						},
						error: function(resp){
							$('#formModal2').find('.loading.dimmer').removeClass('active');
							var error = $('<ul class="list"></ul>');
							$.each(resp.responseJSON, function(index, val) {
								// console.log(val)
								error.append('<li>'+val+'</li>');
							});

							$('#formModal2').find('.ui.error.message').html(error).show();
						}
					});	
				}
				return false;
			},
			onShow: function(){
				$('#formModal2').find('.loading.dimmer').addClass('active');

				$(this).draggable({
		            cancel: ".content" 
		        });

				$.get(url, { _token: "{{ csrf_token() }}" } )
				.done(function( response ) {
					$('#formModal2').html(response);
					$('#dataForm').form({
						inline: true,
						fields: formRules
					});

					$('.ui.dropdown').dropdown();
					initModal();
				});
			},
			onHidden: function(){
				$('#formModal2').html(`<div class="ui inverted loading dimmer">
										<div class="ui text loader">Loading</div>
									</div>`);
				unModal();
			}
		}).modal('show');
	}

	function addUom(){
		var html =
		$( ".ui.data.form" ).append(`<div class='ui two cards add'>
			<div class='card'>
				<div class='content'>		
				<div class='header' style='text-align: center;'>UOM 3</div>
			    <br/>
			    <div class='ui form'>
				  <div class='inline fields'>
				    <label class='six wide field'>UOM 3 Description</label>
				    <div class='eight wide field'>
				      <input placeholder='Description' type='text' readonly=''>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>Barcode UOM 3</label>
				    <div class='eight wide field'>
				      <input placeholder='Barcode UOM 3' type='text' readonly=''>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>Product Description UOM 3</label>
				    <div class='eight wide field'>
				      <input placeholder='Product Description' type='text'>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>Handling Type UOM 3</label>
				    <div class='eight wide field'>
				    	<div class='ui form'>
					    	<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox' readonly=''>
							      <label>Selling Unit</label>
							    </div>
							</div>
							<br/>
							<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox' checked='' readonly=''>
							      <label>Order Unit</label>
							    </div>
							</div>
							<br/>
							<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox' readonly=''>
							      <label>Receive Unit</label>
							    </div>
							</div>
							<br/>
							<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox' checked='' readonly=''>
							      <label>Return Unit</label>
							    </div>
							</div>
							<br/>
							<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox' readonly=''>
							      <label>Inventory Unit</label>
							    </div>
							</div>	
						</div>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>GMD Price</label>
				    	<input placeholder='Description' type='text' readonly=''>
				    <div class='eight wide field'>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>Cost Price UOM 3</label>
				    	<input placeholder='Description' type='text' readonly=''>
				    <div class='eight wide field'>
				    </div>
				  </div>
				</div>
				</div>
				</div>
				<div class='card'>
				<div class='content'>		
				<div class='header' style='text-align: center;'>UOM 4</div>
			    <br/>
			    <div class='ui form'>
				  <div class='inline fields'>
				    <label class='six wide field'>UOM 4 Description</label>
				    <div class='eight wide field'>
				      <input placeholder='Description' type='text' readonly=''>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>Barcode UOM 4</label>
				    <div class='eight wide field'>
				      <input placeholder='Barcode UOM 4' type='text' readonly=''>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>Product Description UOM 4</label>
				    <div class='eight wide field'>
				      <input placeholder='Product Description' type='text' readonly=''>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>Handling Type UOM 4</label>
				    <div class='eight wide field'>
				    	<div class='ui form'>
					    	<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox'>
							      <label>Selling Unit</label>
							    </div>
							</div>
							<br/>
							<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox' checked=''>
							      <label>Order Unit</label>
							    </div>
							</div>
							<br/>
							<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox' readonly=''>
							      <label>Receive Unit</label>
							    </div>
							</div>
							<br/>
							<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox' checked='' readonly=''>
							      <label>Return Unit</label>
							    </div>
							</div>
							<br/>
							<div class='inline field'>
							    <div class='ui toggle checkbox'>
							      <input tabindex='0' class='hidden' type='checkbox' readonly=''>
							      <label>Inventory Unit</label>
							    </div>
							</div>	
						</div>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>GMD Price</label>
				    	<input placeholder='Description' type='number' readonly=''>
				    <div class='eight wide field'>
				    </div>
				  </div>
				  <div class='inline fields'>
				    <label class='six wide field'>Cost Price UOM 4</label>
				    	<input placeholder='Description' type='text' readonly=''>
				    <div class='eight wide field'>
				    </div>
				  </div>
				</div>
				</div>
				</div>`
			);
		
		$('.add.button.uom').addClass('hidden');
		$('.remove.button.uom').removeClass('hidden');
	}

	function removeUom(){
    	$('.ui.two.cards.add').remove();
		$('.remove.button.uom').addClass('hidden');
		$('.add.button.uom').removeClass('hidden');
	}

	function modalRefresh(){
		var modal = $("#formModal");
		modal.height(modal.find('.header').outerHeight() 
				   + modal.find('.content').outerHeight()
				   + modal.find('.actions').outerHeight());

	}
	function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>