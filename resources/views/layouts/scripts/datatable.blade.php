{{-- {{dd($type)}} --}}
{{-- {{ dd($pageUrl) }} --}}
<script type="text/javascript">
	$(document).ready(function() {
		$('button[data-content]').popup({
			hoverable: true,
			position : 'top center',
			delay: {
				show: 300,
				hide: 800
			}
		});

		dt = $('#listTable').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
			responsive: true,
			autoWidth: false,
			processing: true,
			@if(!$mockup)
			serverSide: true,
			@endif
			lengthChange: true,
			pageLength: 10,
			filter: false,
			sorting: [],
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			@if(!$mockup)

			ajax:  {
				url: "{{ url($pageUrl) }}/grid",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
					//d.length = $('input[name="filter[entri]"]').val();
					@yield('js-filters')
				}
			}, 
			@endif
			columns: {!! json_encode($tableStruct) !!},
			drawCallback: function() {
				var api = this.api();

				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML)+i+1;
				} );

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});
				//Calender
				// $('.ui.calendar').calendar({
				// 	type: 'date'
				// });

				//Ceboxx
				$('.ui.checkbox').checkbox();
				$('.pyr.button')
				  .popup({
				    popup : $('.pyr.popup'),
				    on    : 'click'
				  })
				;

				// //Popup							
				// $('.checked.checkbox')
				//   .popup({
				//     popup : $('.custom.popup'),
				//     on    : 'click'
				//   })
				// ;

				// modalRefresh();
			}
		});

		dt2 = $('#listTable2').DataTable({
	        dom: 'rt<"bottom"ip><"clear">',
			responsive: true,
			autoWidth: false,
			processing: true,
			@if(!$mockup)
			serverSide: true,
			@endif
			lengthChange: true,
			pageLength: 10,
			filter: false,
			sorting: [],
			language: {
				url: "{{ asset('plugins/datatables/Indonesian.json') }}"
			},
			@if(!$mockup)

			ajax:  {
				url: "{{ url($pageUrl) }}/grid-histori",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
					//d.length = $('input[name="filter[entri]"]').val();
					@yield('js-filters')
				}
			}, 
			@endif
			columns: {!! json_encode($tableStruct) !!},
			drawCallback: function() {
				var api = this.api();

				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML)+i+1;
				} );

				$('[data-content]').popup({
					hoverable: true,
					position : 'top center',
					delay: {
						show: 300,
						hide: 800
					}
				});
				//Calender
				// $('.ui.calendar').calendar({
				// 	type: 'date'
				// });

				//Ceboxx
				$('.ui.checkbox').checkbox();
				$('.pyr.button')
				  .popup({
				    popup : $('.pyr.popup'),
				    on    : 'click'
				  })
				;

				// //Popup							
				// $('.checked.checkbox')
				//   .popup({
				//     popup : $('.custom.popup'),
				//     on    : 'click'
				//   })
				// ;

				// modalRefresh();
			}
		});

		$('.filter.button').on('click', function(e) {
			var length = $("input[name='filter[entri]']").val();
			length = (length != '') ? length : 10;
			dt.page.len(length).draw();
			dt2.page.len(length).draw();
			e.preventDefault();
		});
		$('.reset.button').on('click', function(e) {
			$('.dropdown .delete').trigger('click');
			$('.dropdown').dropdown('clear');
			setTimeout(function(){
				var length = $("input[name='filter[entri]']").val();
				length = (length != '') ? length : 10;
				dt.page.len(length).draw();
				dt2.page.len(length).draw();
			}, 100);
		});
		@if(isset($type))
			@if($type=='autorefresh')
				// var refInterval = window.setTimeout('test()',3000); //30second
				setInterval(function(){ 
					var length = $("input[name='filter[entri]']").val();
					length = (length != '') ? length : 10;
					dt.page.len(length).draw(false);
					dt2.page.len(length).draw(false);
				}, 30000); // 30 second
			@endif
		@endif
	});
</script>
