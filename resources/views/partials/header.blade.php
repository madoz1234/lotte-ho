<div class="ui fixed light blue inverted menu">
	<a href="{{ url('dashboard') }}" class="header item">
		<img class="logo" src="{{ asset('img/logo.png')}}" style="width:4.5em">&nbsp;&nbsp;
		{{ config('app.sidename') }}
	</a>
    <div class="menu">
      @include('partials.menu', ['items' => $mainMenu->roots()])

      <?php $tahun = Lotte\Models\Master\TahunFiskal::where('status', 1 )->first(); ?>
      <a class="item" data-content="Tahun Fiskal Yang Berjalan"> Tahun Fiskal Berjalan : {{ is_null($tahun) ? '-' : \Carbon\Carbon::createFromFormat('Y-m-d', $tahun->tgl_awal)->format('d-m-Y') }}  </a>

  </div>

  <div class="right menu">
    @if(auth()->guest())
        @if(!Request::is('auth/login'))
        <a class="item" href="{{ url('/auth/login') }}">Login</a>
        @endif
            @if(!Request::is('auth/register'))
    <a class="item" href="{{ url('/auth/register') }}">Register</a>
    @endif
    @else
    <?php
    function indonesian_date ($timestamp = '', $date_format = 'l, j F Y  H:i', $suffix = '') {
        if (trim ($timestamp) == '')
        {
            $timestamp = time ();
        }
        elseif (!ctype_digit ($timestamp))
        {
            $timestamp = strtotime ($timestamp);
        }
# remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace ("/S/", "", $date_format);
        $pattern = array (
            '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
            '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
            '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
            '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
            '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
            '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
            '/April/','/June/','/July/','/August/','/September/','/October/',
            '/November/','/December/',
        );
        $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
            'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
            'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
            'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
            'Oktober','November','Desember',
        );
        $date = date ($date_format, $timestamp);
        $date = preg_replace ($pattern, $replace, $date);
        $date = "{$date} {$suffix}";
        return $date;
    } 
    ?>
    {{-- <a class="item" data-content="Tanggal Hari ini">{{ indonesian_date(\Carbon\Carbon::now()) }}  </a> --}}
    {{-- <a class="item" data-content="Tanggal Hari ini">{{ \Carbon\Carbon::now()->format('l, d F Y H:i') }}  </a> --}}
    {{-- <a class="item" data-content="Tanggal Hari ini">{{ \Carbon\Carbon::now()->addMonths(0)->format('d M Y, h:i') }}  </a> --}}

    <div class="ui pointing dropdown item" tabindex="0">
        {{ auth()->user()->name }} <i class="dropdown icon"></i>
        <div class="menu transition hidden" tabindex="-1">
            <a class="item" href="{{ url('/ganti-password') }}"><i class="key out icon"></i> Ganti Password</a>
            <a class="item" href="{{ url('/auth/logout') }}"><i class="sign out icon"></i> Logout</a>
        </div>
    </div>
    @endif

        {{-- <div class="ui pointing dropdown item" tabindex="0">
            <img src="{{ isset(auth()->user()->fotos->first()->path)?url(auth()->user()->fotos->first()->path):url('img/profile.png') }}" id="home-photo" style="border-radius: 50%; margin-right: 6px"> 
            {{ auth()->user()->name }} <i class="dropdown icon"></i>
            <div class="menu transition hidden" tabindex="-1">
            	<a href="{{ url('settings/profile') }}" class="item"><i class="user icon"></i> Profil</a>
                <a href="javascript: void(0)" onclick="event.preventDefault(); $('#logout-form').submit();" class="item"><i class="sign out icon"></i>Sign Out</a>
            </div>
        </div> --}}
    </div>
</div>

<form id="logout-form" action="{{ url('/logout') }}" method="POST">
    {{ csrf_field() }}
</form>