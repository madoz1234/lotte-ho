@foreach($items as $item)
	@if(!$item->hasChildren())
		@if(isset($item->data['permission'])?$item->data['permission']:'')
		    <a style="width: 210px; padding-left: 50px;" href="{!! $item->url() !!}" class="item" tabindex="{{ $item->id }}">
		    	<i class="{{ $item->icon }} icon" style="padding-right: 30px"></i>{!! $item->title !!}
		    </a>
	    @endif
	@else
		@if(isset($item->data['permission'])?$item->data['permission']:false)
	    <div class="ui pointing dropdown item accordion menu" tabindex="{{ $item->id }}" style="width: 210px;">
	    	<i class="{{ $item->icon }} icon"></i>
			<span style="width: 125px;">{!! $item->title !!}</span>
	        <i class="dropdown icon"></i>
	        <div class="menu transition hidden" tabindex="-1">
	        	@foreach ($item->children() as $child)
		        	@if(!$child->hasChildren())
						@if(isset($child->data['permission'])?$child->data['permission']:false)
		            		<a href="{!! $child->url() !!}" class="item">
		            			<i class="{{ $child->icon }} icon"></i>{!! $child->title !!}
		            		</a>
			            @endif
					@else
						@if(isset($child->data['permission'])?$child->data['permission']:false)
			            	<div class="ui dropdown item">
			            	    <i class="dropdown icon"></i>
			            	    <i class="{{ $child->icon }} icon"></i>{!! $child->title !!}
			            	    <div class="menu">
			            	    	@foreach ($child->children() as $grandChild)
										@if(isset($grandChild->data['permission'])?$grandChild->data['permission']:false)
					    	            <a href="{!! $grandChild->url() !!}" class="item">{!! $grandChild->title !!}</a>
			           					@endif
			            	    	@endforeach
			            	    </div>
			            	</div>
			            @endif
		            @endif
	        	@endforeach
	        </div>
		</div>
		@endif
	@endif
@endforeach