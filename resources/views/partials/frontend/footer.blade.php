<footer>
  <div class="container">
    <div class="row">
      {{-- <div class="d-none d-lg-block col-lg-6">
        <h3>Tentang Kami</h3>
        <div class="card">
          <div class="card-body">
            <p>Itaque ut minima, facilis totam! Ducimus officiis minima, quia assumenda harum deserunt possimus laborum, ipsa pariatur minus sint earum necessitatibus, dolor at. Optio, quibusdam beatae commodi mollitia delectus molestias non aliquid nam enim magni accusantium et quis, totam repudiandae explicabo omnis odit iste natus.</p>
          </div>
        </div>
      </div> --}}
      <div class="col-12 col-md-6">
        <h3>Detail Kontak</h3>
        <div class="card">
          <div class="card-body">
            <h3 class="card-title">APD Riau</h3>
            <span class="useful-texts"><i class="fa fa-building"></i> Eveniet et, odio cum quaerat 20123</span>
            <span class="useful-texts"><i class="fa fa-phone"></i> (0233) 234 1110</span>
            <span class="useful-texts"><i class="fa fa-envelope"></i> lipsum@gmail.com</span>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6">
        <h3>Link Terkait</h3>
        <div class="card">
          <div class="card-body">
            <ul>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
            </ul>
          </div>
        </div>
      </div>
      {{-- <div class="col-xs-12 col-md-4">
        <h3>Kontak Kami</h3> --}}

        {{-- <div class="card">
          <div class="card-body">
            <form class="form" id="contact" action="" method="post">
              <div class="form-group">
                <input class="form-control" placeholder="Your name" type="text" required autofocus>
              </div>
              <div class="form-group row">
                <div class="col-xs-12 col-sm-6">
                  <input class="form-control" placeholder="Your Email Address" type="email" required>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <input class="form-control" placeholder="Your Phone Number" type="tel" required>
                </div>
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Your Web Site starts with http://" type="url" required>
              </div>
              <div class="form-group">
                <textarea class="form-control" placeholder="Type your Message Here...." required></textarea>
              </div>
              <div class="form-group">
                <button name="submit" type="submit" class="btn btn-block btn-primary" id="contact-submit" data-submit="...Sending"><i class="fa fa-send"></i> Kirim</button>
              </div>
            </form>
          </div>
        </div> --}}
      {{-- </div> --}}
    </div>
  </div>
  <div class="real-footer">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          2018 &copy; PLN APD Riau 
        </div>
        <div class="col-xs-12 col-md-6 text-right">
          Franchisee | Web Design & Development by PLN APD RIAU
        </div>
      </div>
    </div>
  </div>
</footer>