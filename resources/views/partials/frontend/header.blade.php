<nav class="navbar navbar-expand-md fixed-top bg-light navbar-light">
  <a class="navbar-brand" href="{{ url('/') }}">
    <img src="{{ asset('img/logo_pln.png') }}" height="60">
    <span class="hidden-folded d-none d-xl-inline-block m-l-xs text-logo">PLN APD RIAU</span>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item {{ $title == 'Visi Misi' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/profil/visi-misi') }}">Visi Misi</a>
      </li>
      <li class="nav-item {{ $title == 'Struktur Organisasi' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/profil/struktur-organisasi') }}">Struktur Organisasi</a>
      </li>
      <li class="nav-item {{ $title == 'Proses Bisnis' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/profil/proses-bisnis') }}">Proses Bisnis</a>
      </li>
      <li class="nav-item {{ $title == 'Wilayah Kerja' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/profil/wilayah-kerja') }}">Wilayah Kerja</a>
      </li>
      <li class="nav-item {{ $title == 'Pengumuman' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/pengumuman') }}">Pengumuman</a>
      </li>
      <li class="nav-item {{ $title == 'Galeri' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/galeri') }}">Foto Kegiatan</a>
      </li>
      <li class="nav-item {{ $title == 'Kontak' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/profil/kontak') }}">Kontak</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/login') }}">Login</a>
      </li>
    </ul>
  </div>
</nav>