<div class="ui fixed light blue inverted menu">
	<a href="{{ url('/') }}" class="header item">
		<img class="logo" src="{{ asset('img/logo.png')}}" style="width:4.5em">&nbsp;&nbsp;
		{{ config('app.sidename') }}
	</a>
    <div class="menu">
      @include('partials.menu', ['items' => $mainMenu->roots()])
    </div>
    
    <div class="right menu">
        @if(auth()->guest())
            @if(!Request::is('auth/login'))
                <a class="item" href="{{ url('/auth/login') }}">Login</a>
            @endif
            @if(!Request::is('auth/register'))
                <a class="item" href="{{ url('/auth/register') }}">Register</a>
            @endif
        @else
            <div class="ui pointing dropdown item" tabindex="0">
                {{ auth()->user()->name }} <i class="dropdown icon"></i>
                <div class="menu transition hidden" tabindex="-1">
                    <a class="item" href="{{ url('/auth/logout') }}"><i class="sign out icon"></i> Logout</a>
                </div>
            </div>
        @endif
        {{-- <div class="ui pointing dropdown item" tabindex="0">
            <img src="{{ isset(auth()->user()->fotos->first()->path)?url(auth()->user()->fotos->first()->path):url('img/profile.png') }}" id="home-photo" style="border-radius: 50%; margin-right: 6px"> 
            {{ auth()->user()->name }} <i class="dropdown icon"></i>
            <div class="menu transition hidden" tabindex="-1">
            	<a href="{{ url('settings/profile') }}" class="item"><i class="user icon"></i> Profil</a>
                <a href="javascript: void(0)" onclick="event.preventDefault(); $('#logout-form').submit();" class="item"><i class="sign out icon"></i>Sign Out</a>
            </div>
        </div> --}}
    </div>
</div>

<form id="logout-form" action="{{ url('/logout') }}" method="POST">
    {{ csrf_field() }}
</form>