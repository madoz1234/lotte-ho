@foreach($items as $item)
	@if(!$item->hasChildren())
    <a href="{!! $item->url() !!}" class="item" tabindex="{{ $item->id }}">
    	<i class="{{ $item->icon }} icon"></i>{!! $item->title !!}
    </a>
	@else
    <div class="ui pointing dropdown item" tabindex="{{ $item->id }}">
    	<i class="{{ $item->icon }} icon"></i>
		{!! $item->title !!}
        <i class="dropdown icon"></i>
        <div class="menu transition hidden" tabindex="-1">
        	@foreach ($item->children() as $child)
	        	@if(!$child->hasChildren())
	            <a href="{!! $child->url() !!}" class="item">
	            	<i class="{{ $child->icon }} icon"></i>{!! $child->title !!}
	            </a>
				@else
	            <div class="ui dropdown item">
	                <i class="dropdown icon"></i>
	                <i class="{{ $child->icon }} icon"></i>{!! $child->title !!}
	                <div class="menu">
	                	@foreach ($child->children() as $grandChild)
			                <a href="{!! $grandChild->url() !!}" class="item">{!! $grandChild->title !!}</a>
	                	@endforeach
	                </div>
	            </div>
	            @endif
        	@endforeach
        </div>
	</div>
	@endif
@endforeach