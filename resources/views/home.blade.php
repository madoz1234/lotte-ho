@extends('layouts.scaffold')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.semanticui.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/highchart/highcharts.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
@append

@section('scripts')
@include('modules.dashboard.scripts.penjualan')
@include('modules.dashboard.scripts.pembelian')
{{-- @include('modules.dashboard.penjualan.scripts.finance') --}}
<script type="text/javascript">
	$(document).ready(function() {
		$('.listTables').DataTable();

		// $('.ui.accordion')
		// .accordion()
		// ;

		$('.ui.calendar').calendar({
			type: 'date'
		});

		$('.ui.month').calendar({
			type: 'month'
		});

		$('.ui.year').calendar({
			type: 'year'
		});
		
		$(document).ready(function(){
			$('.tabular.menu .item').tab({history:false});
		});

		$('#home-menu .item').tab({history:false});

		$('#home-menu .item').click(function() {
			$('#home-menu .item').removeClass('active');
			$(this).addClass('active');
		});

//I/O level
Highcharts.chart('io_level', {
	credits: false,
	chart: {
		type: 'column'
	},
	title: {
		text: 'Rasio I/O Level' + '<br>' + 'Region Timur / LSI Lotte Pasar Rebo / TMUK Jernih'
	},
	subtitle: {
		text: 'Periode 01/18 - 12/18'
	},
	xAxis: {
		type: 'category',
		labels: {
			rotation: -0,
			style: {
				fontSize: '13px',
				fontFamily: 'Verdana, sans-serif'
			}
		}
	},
	yAxis: {
		min: 0,
		title: {
			text: 'I/O Level (%)'
		}
	},
	legend: {
		enabled: false
	},
	series: [{
		name: 'I/O Level',
		data: [
		['Jan-18', 23.7],
		['Feb-18', 16.1],
		['Mar-18', 14.2],
		['Apr-18', 44.0],
		['Mei-18', 80.0],
		['Jun-18', 19.0],
		['Jul-18', 23.0]
		],
	}]
});
});

//Service level
Highcharts.chart('service_level', {
	credits: false,
	chart: {
		type: 'column'
	},
	title: {
		text: 'Rasio Service Level' + '<br>' + 'Region Timur / LSI Lotte Pasar Rebo / TMUK Jernih'
	},
	subtitle: {
		text: 'Periode 01/18 - 12/18'
	},
	xAxis: {
		type: 'category',
		labels: {
			rotation: -0,
			style: {
				fontSize: '13px',
				fontFamily: 'Verdana, sans-serif'
			}
		}
	},
	yAxis: {
		min: 0,
		title: {
			text: 'Service Level (%)'
		}
	},
	// plotOptions: {
 //        column: {
 //            dataLabels: {
 //                enabled: true
 //            },
 //            enableMouseTracking: false
 //        }
 //    },
 legend: {
 	enabled: false
 },
 series: [{
 	name: 'Service Level',
 	data: [
 	['Jan-18', 23.7],
 	['Feb-18', 16.1],
 	['Mar-18', 14.2],
 	['Apr-18', 44.0],
 	['Mei-18', 80.0],
 	['Jun-18', 19.0],
 	['Jul-18', 23.0]
 	],
 }]
});

show = function(){
	$.ajax({
		url: '{{ url('outstanding/grid') }}',
// type: 'default GET (Other values: POST)',
// data: {param1: 'value1'},
})
	.done(function(response) {
	// console.log(response);
	$('.thirteen.wide.column').html(response);
})
	.fail(function() {
		console.log("error");
	});

},

showMap = function(){
	$.ajax({
		url: '{{ url('lokasi-toko/grid') }}',
// type: 'default GET (Other values: POST)',
// data: {param1: 'value1'},
})
	.done(function(response) {
	// console.log(response);
	$('.thirteen.wide.column').html(response);
})
	.fail(function() {
		console.log("error");
	});	
}

finance = function(){
	$.ajax({
		url: '{{ url('finance/grid') }}',
// type: 'default GET (Other values: POST)',
// data: {param1: 'value1'},
})
	.done(function(response) {
// console.log(response);
$('.thirteen.wide.column').html(response);
})
	.fail(function() {
		console.log("error");
	});

},

chat = function(){
	$.ajax({
		url: '{{ url('chat/grid') }}',
// type: 'default GET (Other values: POST)',
// data: {param1: 'value1'},
})
	.done(function(response) {
// console.log(response);
$('.thirteen.wide.column').html(response);
})
	.fail(function() {
		console.log("error");
	});
},

monitoring = function(){
	$.ajax({
		url: '{{ url('monitoring/grid') }}',
// type: 'default GET (Other values: POST)',
// data: {param1: 'value1'},
})
	.done(function(response) {
// console.log(response);
$('.thirteen.wide.column').html(response);
})
	.fail(function() {
		console.log("error");
	});

}
</script>
@append

@section('content')
<div class="ui breadcrumb" style="float: right; padding:15px 0; ">
	<a href="/" class="section">Dashboard</a>
	<i class="right chevron icon divider"></i>
	<div class="active section">Monitoring</div>
</div>
<h2 class="ui header">
	<div class="content">
		Dashboard &amp; Statistik TMUK
		{{-- <div class="sub header">Statistik Penjualan &amp; Pembelian Seluruh TMUK</div> --}}
		<div class="sub header">&nbsp;</div>
	</div>
</h2>
<div class="ui grid">
	<div class="thirteen wide column" style="height: 100%">
		<div class="ui equal width grid">
			<div class="column">
				<div class="ui top attached tabular menu">
					{{-- <a class="active item" data-tab="penjualan-all">Semua Penjualan</a> --}}
					<a class="active item" data-tab="penjualan-mom">MoM</a>
					<a class="item" data-tab="penjualan-qoq">QoQ</a>
					<a class="item" data-tab="penjualan-ytd">YoY</a>
				</div>
					{{-- <div class="ui bottom attached active tab segment" data-tab="penjualan-all">
						<form action="" class="ui form">
							<div class="two fields">
								<div class="field">		
									<div class="ui calendar" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" value="02-01-2018">
										</div>
									</div>
								</div>
								<div class="field">		
									<div class="ui calendar" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" value="02-31-2018">
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div id="penjualan_all" style="width: 100%; height: 400px; margin: 0 auto"></div>
					</div> --}}
					<div class="ui bottom attached tab active segment" data-tab="penjualan-mom">
						<form action="" class="ui form">
							<div class="field">
								<div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field">		
									<div class="ui month" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" value="01-01-2018">
										</div>
									</div>
								</div> 
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui month" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" value="04-31-2018">
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>

						</form>
						<div id="penjualan_mom" style="width: 100%; height: 400px; margin: 0 auto"></div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="penjualan-qoq">
						<form action="" class="ui form">
							<div class="field">
								<div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field">		
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item active selected" data-value="0">I/17</div>
											<div class="item" data-value="1">II/17</div>
											<div class="item" data-value="2">III/17</div>
											<div class="item" data-value="3">IV/17</div>
											<div class="item" data-value="4">I/18</div>
										</div>
									</div>
								</div>
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item active selected" data-value="4">I/18</div>
											<div class="item" data-value="4">II/18</div>
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div id="penjualan_qoq" style="width: 100%; height: 400px; margin: 0 auto"></div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="penjualan-ytd">
						<form action="" class="ui form">
							<div class="field">
								<div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field">		
									<div class="ui year" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" value="2015">
										</div>
									</div>
								</div>
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui year" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Tahun" value="2018">
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div id="penjualan_ytd" style="width: 100%; height: 400px; margin: 0 auto"></div>
					</div>
				</div>

				<div class="column">
					<div class="ui top attached tabular menu">
						{{-- <a class="active item" data-tab="pembelian-all">Semua Pembelian</a> --}}
						<a class="item active" data-tab="pembelian-mom">MoM</a>
						<a class="item" data-tab="pembelian-qoq">QoQ</a>
						<a class="item" data-tab="pembelian-ytd">YoY</a>
					</div>
					{{-- <div class="ui bottom attached active tab segment" data-tab="pembelian-all">
						<form action="" class="ui form">
							<div class="two fields">
								<div class="field">		
									<div class="ui calendar" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" value="02-01-2018">
										</div>
									</div>
								</div>
								<div class="field">		
									<div class="ui calendar" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" value="02-31-2018">
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div id="pembelian_all" style="width: 100%; height: 400px; margin: 0 auto"></div>
					</div> --}}
					<div class="ui bottom attached active tab segment" data-tab="pembelian-mom">
						<form action="" class="ui form">
							<div class="field">
								<div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field">		
									<div class="ui month" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" value="02-01-2018">
										</div>
									</div>
								</div>
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui month" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" value="04-31-2018">
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div id="pembelian_mom" style="width: 100%; height: 400px; margin: 0 auto"></div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="pembelian-qoq">
						<form action="" class="ui form">
							<div class="field">
								<div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field">		
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item active selected" data-value="0">I/17</div>
											<div class="item" data-value="1">II/17</div>
											<div class="item" data-value="2">III/17</div>
											<div class="item" data-value="3">IV/17</div>
											<div class="item" data-value="4">I/18</div>
										</div>
									</div>
								</div>
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Pilih</div>
										<div class="menu">
											<div class="item active selected" data-value="4">I/18</div>
											<div class="item" data-value="4">II/18</div>
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div id="pembelian_qoq" style="width: 100%; height: 400px; margin: 0 auto"></div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="pembelian-ytd">
						<form action="" class="ui form">
							<div class="field">
								<div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field">		
									<div class="ui year" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Tahun" value="2015">
										</div>
									</div>
								</div>
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui year" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Tahun" value="2018">
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div id="pembelian_ytd" style="width: 100%; height: 400px; margin: 0 auto"></div>
					</div>
				</div>
			</div>
			<!-- Penjulan & Pembelian -->

			<!-- I/O & Service Level --> 
			<div class="ui equal width grid">

				<div class="column">
					<div class="ui segment">
						<form action="" class="ui form">
							<div class="field">
								<div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field">		
									<div class="ui month" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" value="01-2018">
										</div>
									</div>
								</div>
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui month" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" value="12-2018">
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div id="io_level" style="width: 100%; height: 400px; margin: 0 auto"></div>
						{{-- <table>
							<tr>
								<th rowspan="2" valign="top">Keterangan : </th>
								<td>I (Input) </td>
								<td> = </td>
							</tr>
							<tr>
								<td>O (Output) </td>
								<td> = </td>
							</tr>
						</table> --}}
						<table>
							<tr>
								<th rowspan="2" valign="top">Keterangan : </th>
								<td>I/O Level (%)&nbsp; = &nbsp;</td>
								<td> Ʃ Pembelian TMUK Ke LSI<hr></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>  Ʃ Penjualan TMUK Ke Kustomer</td>
							</tr>
						</table>
					</div>
				</div>

				<div class="column">
					<div class="ui segment">
						<form action="" class="ui form">
							<div class="field">
								<div class="fields">
									<div style="margin-left: auto; margin-right: 1px;">
										<button type="button" class="ui green button">
											<i class="download icon"></i> Download
										</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</div>
								</div>
							</div>
							<div class="two fields">
								<div class="field">		
									<div class="ui month" id="from">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Dari Bulan" value="01-2018">
										</div>
									</div>
								</div>
								<p style="text-align: center; margin: 10px; ">s.d.</p>
								<div class="field">		
									<div class="ui month" id="to">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" placeholder="Sampai Bulan" value="12-2018">
										</div>
									</div>
								</div>
							</div>

							<div class="three fields">
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">Region</div>
										<div class="menu">
											<div class="item active selected" data-value="0">Timur</div>
											<div class="item" data-value="1">Barat</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">LSI</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Lotte Pasar Rebo</div>
											<div class="item" data-value="0600700018">Lotte Grosir Bandung</div>
										</div>
									</div>
								</div>
								<div class="field">
									<div class="ui fluid search selection dropdown">
										<input name="country" type="hidden">
										<i class="dropdown icon"></i>
										<div class="default text">TMUK</div>
										<div class="menu">
											<div class="item active selected" data-value="0601800002">Jernih</div>
											<div class="item" data-value="0600700018">Pragma</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div id="service_level" style="width: 100%; height: 400px; margin: 0 auto"></div>
						<table>
							<tr>
								<th rowspan="2" valign="top">Keterangan : </th>
								<td>Service Level (%)&nbsp; = &nbsp;</td>
								<td> Ʃ Qty PO<hr></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>  Ʃ Qty PR</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<!-- I/O & Service Level -->

			<!-- Table -->
			<div class="ui equal width grid">
				<!-- Deviasi -->
				{{-- <div class="column">
					<!-- I/O Level -->
					<div class="ui segments">
						<h5 class="ui top attached header">
							<div class="ui accordion field">
								<div class="title">
									<i class="icon dropdown"></i>
									Deviasi Penjualan Harian
								</div>
								<div class="active content field">
									<div class="ui grid">
										<div class="three column row">
											<div class="left floated">&nbsp;
												<div class="ui icon input">
													<input type="text" placeholder="Search TMUK...">
													<i class="circular search link icon"></i>
												</div>
											</div>
											<div class="right floated">
												<button type="button" class="ui green button">
													<i class="download icon"></i> Download
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</h5>
						<div class="ui packed yellow segment">
							<span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;&nbsp;<b>Tahun 2018 YTD</b></span>
							<table class="ui very basic striped small compact table">
								<tbody align="center">
									<tr>
										<th>#</th>
										<th>TMUK</th>
										<th align="center">Penjualan Harian (KKI) (Rp.)</th>
										<th align="center">Penjualan Harian (Aktual) (Rp.)</th>
										<th>Selisih (Rp.)</th>
										<th align="center">% dari Target (Rp.)</th>

									</tr>
									<tr>
										<td>1</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
									<tr>
										<td>2</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
									<tr>
										<td>3</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
									<tr>
										<td>4</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
									<tr>
										<td>5</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
									<tr>
										<td>6</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
									<tr>
										<td>7</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
									<tr>
										<td>8</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
									<tr>
										<td>9</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
									<tr>
										<td>10</td>
										<td>Jernih</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
										<td>1.000.000,00</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="6">Menampilkan 1 Sampai 10 Dari 10 Entri
											<div class="ui right floated pagination menu">
												<a class="icon item">
													<i class="left chevron icon"></i>
												</a>
												<a class="item">1</a>
												<a class="item">2</a>
												<a class="item">3</a>
												<a class="icon item">
													<i class="right chevron icon"></i>
												</a>
											</div>
										</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- I/O Level -->
				</div> --}}
				
				<!-- Deviasi -->
			</div>
			<div class="ui equal width grid">
				<!-- Penjualan -->
				<div class="eight wide column">
					<!-- Statistik -->
					<div class="ui segments">
						<h5 class="ui top attached header">
							<div class="ui accordion field">
								<div class="title">
									{{-- <i class="icon dropdown"></i> --}}
									Penjualan Category 1 TMUK
								</div>
								<div class="active content field">
									<div class="ui grid">
										<div class="two column row">
											<div class="left floated eight wide column">&nbsp;
												<div class="ui icon input">
													<input type="text" placeholder="Search TMUK..." value="TMUK Pragma Informatika">
													<i class="circular search link icon"></i>
												</div>
											</div>
											<div class="fields">
												<div style="margin-left: auto; margin-right: 1px;">
													<button type="button" class="ui green button">
														<i class="download icon"></i> Download
													</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
										</div>
									</div>
								{{-- <div class="ui form">
									<div class="two fields">
										<div class="field">
											<input placeholder="Read Only" readonly="" type="text">
										</div>
										<div class="field">
											<button type="button" class="ui green button">
												<i class="download icon"></i> Download
											</button>
										</div>
									</div>
								</div> --}}
							</div>
						</div>
					</h5>
					<div class="ui packed yellow segment">
						{{-- <span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TMUK Pragma Informatika</b></span>
						<span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Tahun 2018 YTD</b></span> --}}
						<table class="ui table">
							<tr align="center">
								<td><b>TMUK Pragma Informatika</b><br>Tahun 2018 YTD
								</td>
							</tr>
						</table>

						<table class="ui very basic striped small compact table">
							<thead>
								<tr>
									<th style="text-align: center;">#</th>
									<th style="text-align: center;">Category 1 </th>
									<th style="text-align: center;">Penjualan (Rp.)</th>
									<th style="text-align: center;">Kontribusi dari Sales (%)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align: center;">1</td>
									<td style="text-align: left;">Biscuit/Snack</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
								<tr>
									<td style="text-align: center;">2</td>
									<td style="text-align: left;">Drink/Milk</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
								<tr>
									<td style="text-align: center;">3</td>
									<td style="text-align: left;">Detergent</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
								<tr>
									<td style="text-align: center;">4</td>
									<td style="text-align: left;">H&B</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
								<tr>
									<td style="text-align: center;">5</td>
									<td style="text-align: left;">Bakery</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
								<tr>
									<td style="text-align: center;">6</td>
									<td style="text-align: left;">TEXTILE</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
								<tr>
									<td style="text-align: center;">7</td>
									<td style="text-align: left;">HARDWARE</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
								<tr>
									<td style="text-align: center;">8</td>
									<td style="text-align: left;">Drink Milk</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
								<tr>
									<td style="text-align: center;">9</td>
									<td style="text-align: left;">Biscuit/Snack</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
								<tr>
									<td style="text-align: center;">10</td>
									<td style="text-align: left;">Detergent</td>
									<td style="text-align: right;">1.000.000</td>
									<td style="text-align: center;">75</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="6">Menampilkan 1 sampai 10 dari 10 Entri
										<div class="ui right floated pagination menu">
											<a class="icon item">
												<i class="left chevron icon"></i>
											</a>
											<a class="item">1</a>
											<a class="item">2</a>
											<a class="item">3</a>
											<a class="icon item">
												<i class="right chevron icon"></i>
											</a>
										</div>
									</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<!-- Statistik -->
			</div>
			<!-- Penjualan -->
			<div class="eight wide column">
				<!-- I/O Level -->
				<div class="ui segments">
					<h5 class="ui top attached header">
						<div class="ui accordion field">
							<div class="title">
								{{-- <i class="icon dropdown"></i> --}}
								Deviasi Penjualan Harian TMUK
							</div>
							<div class="active content field">
								<div class="ui grid">
									<div class="two column row">
										<div class="left floated eight wide column">&nbsp;
											<div class="ui icon input">
												<input type="text" placeholder="Search TMUK..." value="TMUK Pragma Informatika">
												<i class="circular search link icon"></i>
											</div>
										</div>
										<div class="fields">
											<div style="margin-left: auto; margin-right: 1px;">
												<button type="button" class="ui green button">
													<i class="download icon"></i> Download
												</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</h5>
					<div class="ui packed yellow segment">
						{{-- <span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;&nbsp;<b>Tahun 2018 YTD</b></span> --}}
						<table class="ui table">
							<tr align="center">
								<td><b>TMUK Pragma Informatika</b><br>Tahun 2018 YTD
								</td>
							</tr>
						</table>
						<table class="ui very basic striped small compact table">
							<thead>
								<tr>
									<th>#</th>
									<th>TMUK</th>
									<th align="center">Penjualan (KKI)</th>
									<th align="center">Penjualan (Aktual)</th>
									<th>Selisih (Rp.)</th>
									<th align="center">% dari Target</th>
								</tr>
							</thead>
							<tbody align="center">
								{{-- <tr>
									<th>#</th>
									<th>TMUK</th>
									<th align="center">Penjualan (KKI)</th>
									<th align="center">Penjualan (Aktual)</th>
									<th>Selisih (Rp.)</th>
									<th align="center">% dari Target</th>

										<th>#</th>
										<th>TMUK</th>
										<th align="center">Penjualan Harian (KKI) (Rp.)</th>
										<th align="center">Penjualan Harian (Aktual) (Rp.)</th>
										<th>Selisih (Rp.)</th>
										<th align="center">% dari Target (Rp.)</th>

									</tr> --}}
									<tr>
										<td style="text-align: center;">1</td>
										<td style="text-align: left;">Jernih</td>
										<td style="text-align: right;">10.000.000</td>
										<td style="text-align: right;">15.000.000</td>
										<td style="text-align: right;">5.000.000</td>
										<td style="text-align: center;">75</td>
									</tr>
									<tr>
										<td style="text-align: center;">2</td>
										<td style="text-align: left;">Pragma Informatika</td>
										<td style="text-align: right;">10.000.000</td>
										<td style="text-align: right;">12.000.000</td>
										<td style="text-align: right;">2.000.000</td>
										<td style="text-align: center;">75</td>
									</tr>
									<tr>
										<td style="text-align: center;">3</td>
										<td style="text-align: left;">Jernih</td>
										<td style="text-align: right;">1.000.000</td>
										<td style="text-align: right;">1.000.000</td>
										<td style="text-align: right;">0</td>
										<td style="text-align: center;">75</td>
									</tr>
									<tr>
										<td style="text-align: center;">4</td>
										<td style="text-align: left;">Pragma Informatika</td>
										<td style="text-align: right;">10.000.000</td>
										<td style="text-align: right;">14.000.000</td>
										<td style="text-align: right;">4.000.000</td>
										<td style="text-align: center;">75</td>
									</tr>
									<tr>
										<td style="text-align: center;">5</td>
										<td style="text-align: left;">Jernih</td>
										<td style="text-align: right;">10.000.000</td>
										<td style="text-align: right;">12.000.000</td>
										<td style="text-align: right;">2.000.000</td>
										<td style="text-align: center;">75</td>
									</tr>
									<tr>
										<td style="text-align: center;">6</td>
										<td style="text-align: left;">Pragma Informatika</td>
										<td style="text-align: right;">10.000.000</td>
										<td style="text-align: right;">15.000.000</td>
										<td style="text-align: right;">5.000.000</td>
										<td style="text-align: center;">75</td>
									</tr>
									<tr>
										<td style="text-align: center;">7</td>
										<td style="text-align: left;">Jernih</td>
										<td style="text-align: right;">10.000.000</td>
										<td style="text-align: right;">11.000.000</td>
										<td style="text-align: right;">1.000.000</td>
										<td style="text-align: center;">75</td>
									</tr>
									<tr>
										<td style="text-align: center;">8</td>
										<td style="text-align: left;">Pragma Informatika</td>
										<td style="text-align: right;">1.000.000</td>
										<td style="text-align: right;">1.000.000</td>
										<td style="text-align: right;">0</td>
										<td style="text-align: center;">75</td>
									</tr>
									<tr>
										<td style="text-align: center;">9</td>
										<td style="text-align: left;">Jernih</td>
										<td style="text-align: right;">10.000.000</td>
										<td style="text-align: right;">13.000.000</td>
										<td style="text-align: right;">3.000.000</td>
										<td style="text-align: center;">75</td>
									</tr>
									<tr>
										<td style="text-align: center;">10</td>
										<td style="text-align: left;">Pragma Informatika</td>
										<td style="text-align: right;">11.000.000</td>
										<td style="text-align: right;">12.000.000</td>
										<td style="text-align: right;">1.000.000</td>
										<td style="text-align: center;">75</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="6">Menampilkan 1 sampai 10 dari 10 Entri
											<div class="ui right floated pagination menu">
												<a class="icon item">
													<i class="left chevron icon"></i>
												</a>
												<a class="item">1</a>
												<a class="item">2</a>
												<a class="item">3</a>
												<a class="icon item">
													<i class="right chevron icon"></i>
												</a>
											</div>
										</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- I/O Level -->
				</div>
				<div class="eight wide  column">
					<div class="ui segments">
						<h5 class="ui top attached header">
							Statistik Periode 2018
						</h5>
						<div class="ui packed yellow segment">
							<table class="ui very basic striped small compact table">
								<tbody>
								{{-- <tr>
									<th width="50px"></th>
									<th width="230px"></th>
									<th></th>
								</tr> --}}
								<tr>
									<td width="50px">1</td>
									<td width="500px">Rata - rata Frekuensi PO/Bulan </td>
									<td>: 5 </td>
								</tr>
								<tr>
									<td width="50px">2</td>
									<td width="500px">Rata - rata Nilai PO/Bulan </td>
									<td>: Rp 1.000.000 /Bulan</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- Statistik -->
			{{-- <div class="three wide column">
				<div class="ui segments">
					<h5 class="ui top attached header">
						Statistik Periode 2018
					</h5>
					<div class="ui packed yellow segment">
						<table class="ui very basic striped small compact table">
							<tbody>
								<tr>
									<td>#</td>
									<td>Divisi </td>
									<td>Penjualan (Rp.)</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Rata - rata Frekuensi PO/Bulan </td>
									<td>: 5 </td>
								</tr>
								<tr>
									<td>2</td>
									<td>Rata - rata Nilai/PO </td>
									<td>: 1.000.000,00 Po</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div> --}}
			<!-- Statistik -->
		</div>
		<!-- Table -->
	</div>
	<div class="three wide column">
		<div id="home-menu" class="ui vertical fluid right tabular menu">
			<a class="active item" href="{{ url() }}">
				Penjualan
			</a>
			<a class="item" onclick="finance()">
				Finance
			</a>
			<a class="item" onclick="show()">
				Outstanding
			</a>
			<a class="item" onclick="showMap()">
				Peta Lokasi
			</a>
			<a class="item" onclick="chat()">
				Chat Box
			</a>
			<a class="item" onclick="monitoring()">
				Monitoring Online/Offline
			</a>
		</div>
		
		{{-- <div class="ui right">
			<h3 class="ui header">
				<div class="content">
					Monitoring
					<div class="sub header">Status Online/Offline</div>
				</div>
			</h3>
			<div class="ui segments vertical menu">
					<div class="item">
						<div class="menu">
							<a class="active item" data-tooltip="Status Online" data-position="top left"><i class="signal icon"></i>Etokokoe</a>
							<a class="item" data-tooltip="Aktif 1 jam yang lalu" data-position="top left">Berkah</a>
							<a class="active item"><i class="signal icon"></i>Coop Mart</a>
							<a class="active item"><i class="signal icon"></i>Zamzam</a>
							<a class="active item"><i class="signal icon"></i>Multijaya</a>
							<a class="item">Permata</a>
							<a class="item">Puskopau</a>
							<a class="item">Kalianda Lampung</a>
							<a class="item">Mercubuana</a>
							<a class="item">Taekwang</a>
							<a class="item">Intan</a>
							<a class="active item"><i class="signal icon"></i>Hikmah</a>
							<a class="item">Sultan Abadi</a>
							<a class="active item"><i class="signal icon"></i>Vitamart</a>
							<a class="item">3 SAUDARA</a>
							<a class="item">LJ Mart</a>
							<a class="active item"><i class="signal icon"></i>Jernih</a>
							<a class="active item"><i class="signal icon"></i>Abdi Bahari</a>
							<a class="item">Koperasi Srikandi</a>
							<a class="item">Jaya Mart</a>
						</div>
					</div>
			</div>
		</div> --}}
	</div>
</div>
@endsection