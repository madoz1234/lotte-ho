<?php

include "fungsi.php";

?>

<!DOCTYPE html>
<html>
<head>
	<title>GMD</title>
</head>
<body>
	<form action="cek_harga_gmd.php" method="post">
		<table>
			<tr>
				<th colspan="3">Cek Price GMD</th>
			</tr>
			<tr>
				<td>Kode LSI</td>
				<td>:</td>
				<td>
					<select name="kode_lsi">
						<?php
							foreach ($lsi as $key => $value) {
								echo '<option value="'.$key.'">'.$value.'</option>';
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Kode Produk</td>
				<td>:</td>
				<td>
					<input type="text" name="kode_produk" size="50">
					<br><i>contoh : 1000018000, 1000044000</i>
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<input type="submit" value="Submit">
				</td>
			</tr>
		</table>
	</form>
	<br><br>
	<br>
	<?php
		if (isset($_POST['kode_lsi'])) {
			//echo '<label style="color:blue;">Response: Kode LSI: '.$_POST['kode_lsi'].', Kode Produk: '.$_POST['kode_lsi'].'</label><br><br>';
			
			$respon = post("https://api.lottemart.co.id/api/tmuk/price", [
				'PROD_CD' => $_POST['kode_produk'],
				'STR_CD'  => $_POST['kode_lsi'],
			]);
			echo '<span style="color:#B3009C;">' . formatjson($respon) . '</span>';
		}
	?>
</body>
</html>

