<?php

$lsi = [
	'06007' => "LOTTE GROSIR ALAM SUTERA, TANGERANG",
	'06020' => "LOTTE GROSIR BALIKPAPAN",
	'06005' => "LOTTE GROSIR BANDUNG",
	'06017' => "LOTTE GROSIR BANJARMASIN",
	'06029' => "LOTTE GROSIR BATAM",
	'06018' => "LOTTE GROSIR BEKASI",
	'06026' => "LOTTE GROSIR BOGOR",
	'06023' => "LOTTE GROSIR CIKARANG",
	'06006' => "LOTTE GROSIR CIPUTAT",
	'06024' => "LOTTE GROSIR CIREBON",
	'06009' => "LOTTE GROSIR DENPASAR, BALI",
	'06021' => "LOTTE GROSIR JATAKE",
	'06003' => "LOTTE GROSIR KELAPA GADING",
	'06013' => "LOTTE GROSIR MAKASAR",
	'06012' => "LOTTE GROSIR MARGOMULYO, SURABAYA",
	'06010' => "LOTTE GROSIR MEDAN",
	'06004' => "LOTTE GROSIR MERUYA",
	'06008' => "LOTTE GROSIR MM2100, CIBITUNG",
	'06014' => "LOTTE GROSIR PALEMBANG",
	'06001' => "LOTTE GROSIR PASAR REBO",
	'06015' => "LOTTE GROSIR PEKANBARU",
	'06032' => "LOTTE GROSIR SAMARINDA",
	'06011' => "LOTTE GROSIR SEMARANG",
	'06022' => "LOTTE GROSIR SERANG",
	'06002' => "LOTTE GROSIR SIDOARJO, SURABAYA",
	'06019' => "LOTTE GROSIR SOLO",
	'06028' => "LOTTE GROSIR SURABAYA MASTRIP",
	'06027' => "LOTTE GROSIR TASIKMALAYA",
	'06016' => "LOTTE GROSIR YOGYAKARTA",
];

function post($url, $param)
{
	$apiKey = '3021fe5698c3c6aabb34d6432cb1f36d';
	
	$curl = curl_init($url);
	$headers = array(
		'X-API-KEY: '.$apiKey.''
	);
	
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
	$curl_response = curl_exec($curl);
	curl_close($curl);
	
	return $curl_response;
}

function get($url)
{
	$apiKey = '3021fe5698c3c6aabb34d6432cb1f36d';
	
	$curl = curl_init($url);
	$headers = array(
		'X-API-KEY: '.$apiKey.''
	);
	
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_POST, 0);
	//curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
	$curl_response = curl_exec($curl);
	curl_close($curl);
	
	return $curl_response;
}

function formatjson($json) {

    $result      = '';
    $pos         = 0;
    $strLen      = strlen($json);
    $indentStr   = '&nbsp;&nbsp;&nbsp;&nbsp;';
    $newLine     = "<br>";
    $prevChar    = '';
    $outOfQuotes = true;

    for ($i=0; $i<=$strLen; $i++) {

        // Grab the next character in the string.
        $char = substr($json, $i, 1);

        // Are we inside a quoted string?
        if ($char == '"' && $prevChar != '\\') {
            $outOfQuotes = !$outOfQuotes;

        // If this character is the end of an element,
        // output a new line and indent the next line.
        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
            $result .= $newLine;
            $pos --;
            for ($j=0; $j<$pos; $j++) {
                $result .= $indentStr;
            }
        }

        // Add the character to the result string.
        $result .= $char;

        // If the last character was the beginning of an element,
        // output a new line and indent the next line.
        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
            $result .= $newLine;
            if ($char == '{' || $char == '[') {
                $pos ++;
            }

            for ($j = 0; $j < $pos; $j++) {
                $result .= $indentStr;
            }
        }

        $prevChar = $char;
    }

    return $result;
}
