<?php

use Illuminate\Database\Seeder;

class KelurahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_kelurahan')->insert([
        	'id' => '1',
        	'kecamatan_id' => '1',
        	'nama' => 'Dauh Puri',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kelurahan')->insert([
            'id' => '2',
            'kecamatan_id' => '1',
            'nama' => 'Padang Sambian',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kelurahan')->insert([
            'id' => '3',
            'kecamatan_id' => '1',
            'nama' => 'Pemecutan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kelurahan')->insert([
            'id' => '4',
            'kecamatan_id' => '1',
            'nama' => 'Tegal Harum',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);
    }
}
