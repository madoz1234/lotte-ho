<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ProvinsiSeeder::class);
        $this->call(KotaSeeder::class);
        $this->call(KecamatanSeeder::class);
        $this->call(KelurahanSeeder::class);
        $this->call(DivisiSeeder::class);
        $this->call(Kategori1Seeder::class);
        $this->call(Kategori2Seeder::class);
        $this->call(Kategori3Seeder::class);
        $this->call(Kategori4Seeder::class);
        // $this->call(PajakSeeder::class);
        // $this->call(BankSeeder::class);
        // $this->call(RekeningSeeder::class);
        // $this->call(TahunFiskalSeeder::class);
        // $this->call(TipeAsetSeeder::class);
        $this->call(TipeCoaSeeder::class);
        
        Model::reguard();
    }

}
