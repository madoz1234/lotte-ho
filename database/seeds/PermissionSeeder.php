<?php

/**
 * RUN PERMISSION SEEDER
 */
use Illuminate\Database\Seeder;
use Lotte\Models\Entrust\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run database seeder
     */
    public function run(){
	    $permission = [
	    	// -------------------MENU UTAMA--------------------//
	    	//Menu utama//
	    	[
				'name'         => 'menu-utama',
				'display_name' => 'Menu Utama',
				'description'  => '',
				'action' => [''],
	    	],
	    	//Master data finance
	    	[
				'name'         => 'menu-utama-master-finance',
				'display_name' => 'Master Data Finance',
				'description'  => '',
				'action' => [''],
	    	],
	    	// Master data finance pajak
	    	[
				'name'         => 'menu-utama-master-finance-pajak',
				'display_name' => 'Pajak',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Master data finance bank
	    	[
				'name'         => 'menu-utama-master-finance-bank',
				'display_name' => 'Bank',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Master data finance rekening bank
	    	[
				'name'         => 'menu-utama-master-finance-rekening-bank',
				'display_name' => 'Rekening Bank',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Master data finance COA
	    	[
				'name'         => 'menu-utama-master-finance-coa',
				'display_name' => 'COA',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Master data finance Tahun Fiskal
	    	[
				'name'         => 'menu-utama-master-finance-tahun-fisikal',
				'display_name' => 'Tahun Fsikal',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Master data finance Tipe asset Terdepresiasi
	    	[
				'name'         => 'menu-utama-master-finance-tipe-asset-terdepresiasi',
				'display_name' => 'Tipe Asset Terdepresiasi',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Master data finance Saldo Minimal Mengendap
	    	[
				'name'         => 'menu-utama-master-finance-saldo-minimal-mengendap',
				'display_name' => 'Saldo Minimal Mengendap',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Master data finance Point
	    	[
				'name'         => 'menu-utama-master-finance-point',
				'display_name' => 'Point',
				'description'  => '',
				'action' => ['access'],
	    	],

	    	//Master data Produk
	    	[
				'name'         => 'menu-utama-master-produk',
				'display_name' => 'Master Data Produk',
				'description'  => '',
				'action' => [''],
	    	],   	
	    	//Master data Produk Rak
	    	[
				'name'         => 'menu-utama-master-produk-rak',
				'display_name' => 'Rak',
				'description'  => '',
				'action' => ['access'],
	    	],    	
	    	//Master data Produk Tipe Barang
	    	[
				'name'         => 'menu-utama-master-produk-tipe-barang',
				'display_name' => 'Tipe Barang',
				'description'  => '',
				'action' => ['access'],
	    	],    	
	    	//Master data Produk Tipe Barang
	    	[
				'name'         => 'menu-utama-master-produk-jenis-barang',
				'display_name' => 'Jenis Barang',
				'description'  => '',
				'action' => ['access'],
	    	],    	
	    	//Master data Produk unit UOM
	    	[
				'name'         => 'menu-utama-master-produk-unit-uom',
				'display_name' => 'Unit Uom',
				'description'  => '',
				'action' => ['access'],
	    	],    	
	    	//Master data Produk Upload produk GMD
	    	[
				'name'         => 'menu-utama-master-produk-upload-produk-gmd',
				'display_name' => 'Upload Produk GMD',
				'description'  => '',
				'action' => ['access'],
	    	],    	
	    	//Master data Produk Upload produk non GMD
	    	[
				'name'         => 'menu-utama-master-produk-upload-produk-non-gmd',
				'display_name' => 'Upload Produk non GMD',
				'description'  => '',
				'action' => ['access'],
	    	],    	
	    	//Master data Produk Produk
	    	[
				'name'         => 'menu-utama-master-produk-produk',
				'display_name' => 'Produk',
				'description'  => '',
				'action' => ['access'],
	    	],    	
	    	//Master data Produk aktivasi Produk assortment type
	    	[
				'name'         => 'menu-utama-master-produk-aktivasi-produk-assortment-type',
				'display_name' => 'Aktivasi Produk Assortment Type',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Produk aktivasi Produk TMUK
	    	[
				'name'         => 'menu-utama-master-produk-aktivasi-produk-tmuk',
				'display_name' => 'Aktivasi Produk TMUK',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Produk Harga
	    	[
				'name'         => 'menu-utama-master-produk-harga',
				'display_name' => 'Harga',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Produk Kategori Produk
	    	[
				'name'         => 'menu-utama-master-produk-kategori-produk',
				'display_name' => 'Kategori Produk',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Produk Kontainer
	    	[
				'name'         => 'menu-utama-master-produk-kontainer',
				'display_name' => 'Kontainer',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Produk Truk
	    	[
				'name'         => 'menu-utama-master-produk-truk',
				'display_name' => 'Truk',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Produk Promosi
	    	[
				'name'         => 'menu-utama-master-produk-promosi',
				'display_name' => 'Promosi',
				'description'  => '',
				'action' => ['access'],
	    	],

	    	//Master data Toko
	    	[
				'name'         => 'menu-utama-master-toko',
				'display_name' => 'Master Data Toko',
				'description'  => '',
				'action' => [''],
	    	],
	    	//Master data Toko Region
	    	[
				'name'         => 'menu-utama-master-toko-region',
				'display_name' => 'Region',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko Provinsi
	    	[
				'name'         => 'menu-utama-master-toko-provinsi',
				'display_name' => 'Provinsi',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko Kota
	    	[
				'name'         => 'menu-utama-master-toko-kota',
				'display_name' => 'Kota',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko Kecamatan
	    	[
				'name'         => 'menu-utama-master-toko-kecamatan',
				'display_name' => 'Kecamatan',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko LSI
	    	[
				'name'         => 'menu-utama-master-toko-lsi',
				'display_name' => 'LSI',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko TMUK
	    	[
				'name'         => 'menu-utama-master-toko-tmuk',
				'display_name' => 'TMUK',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko Vendor Lokal TMUK
	    	[
				'name'         => 'menu-utama-master-toko-vendor-lokal-tmuk',
				'display_name' => 'Vendor Lokal TMUK',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko KKI TMUK
	    	[
				'name'         => 'menu-utama-master-toko-kki-tmuk',
				'display_name' => 'KKI TMUK',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko Jenis Kustomer
	    	[
				'name'         => 'menu-utama-master-toko-jenis-kustomer',
				'display_name' => 'Jenis Kustomer',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko kartu Anggota
	    	[
				'name'         => 'menu-utama-master-toko-kartu-anggota',
				'display_name' => 'Kartu Anggota',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Toko Kustomer
	    	[
				'name'         => 'menu-utama-master-toko-kustomer',
				'display_name' => 'Kustomer',
				'description'  => '',
				'action' => ['access'],
	    	],

	    	//Master data Pengguna
	    	[
				'name'         => 'menu-utama-master-pengguna',
				'display_name' => 'Master Data Penguna',
				'description'  => '',
				'action' => [''],
	    	],
	    	//Master data Pengguna Hak Akses
	    	[
				'name'         => 'menu-utama-master-pengguna-hak-akses',
				'display_name' => 'Hak Akses',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Pengguna Pengguna
	    	[
				'name'         => 'menu-utama-master-pengguna-pengguna',
				'display_name' => 'Pengguna',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Master data Pengguna Persetujuan ESCROW
	    	[
				'name'         => 'menu-utama-master-pengguna-persetujuan-escrow',
				'display_name' => 'Persetujuan Escrow',
				'description'  => '',
				'action' => ['access'],
	    	],

	    	//Planogram
	    	[
				'name'         => 'menu-utama-planogram',
				'display_name' => 'Planogram',
				'description'  => '',
				'action' => [''],

	    	],    	

	    	//--------------TERANSAKSI---------------//
	    	//Transaksi
	    	[
				'name'         => 'transaksi',
				'display_name' => 'Teransaksi',
				'description'  => '',
				'action' => [''],
	    	],
	    	//Transaksi opening toko
	    	[
				'name'         => 'transaksi-opening-toko',
				'display_name' => 'Opening-toko',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi picking
	    	[
				'name'         => 'transaksi-picking',
				'display_name' => 'Picking',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Pembelian
	    	[
				'name'         => 'transaksi-pembelian',
				'display_name' => 'Pembelian',
				'description'  => '',
				'action' => [''],
	    	],
	    	//Transaksi Pembelian Purchase Order
	    	[
				'name'         => 'transaksi-pembelian-purchase-order',
				'display_name' => 'Purchase Order PO',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Pembelian Reduce Escrow Balance
	    	[
				'name'         => 'transaksi-pembelian-reduce-escrow-balance',
				'display_name' => 'Reduce Escrow Balance',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Pembelian payment Request
	    	[
				'name'         => 'transaksi-pembelian-payment-request',
				'display_name' => 'Payment Request',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Pembelian Pembayaran PYR
	    	[
				'name'         => 'transaksi-pembelian-pembayaran-pyr',
				'display_name' => 'Pembayaran PYR',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Kubikasi
	    	[
				'name'         => 'transaksi-kubikasi',
				'display_name' => 'Kubikasi',
				'description'  => '',
				'action' => [''],
	    	],
	    	//Transaksi Kubikasi Kontainer
	    	[
				'name'         => 'transaksi-kubikasi-kontainer',
				'display_name' => 'Kontainer',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Kubikasi Truk
	    	[
				'name'         => 'transaksi-kubikasi-truk',
				'display_name' => 'Truk',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Rute Pengiriman
	    	[
				'name'         => 'transaksi-rute-pengiriman',
				'display_name' => 'Rute Pengiriman',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Konfirmasi Retur
	    	[
				'name'         => 'transaksi-konfirmasi-retur',
				'display_name' => 'Konfirmasi Retur',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Member
	    	[
				'name'         => 'transaksi-member',
				'display_name' => 'Member',
				'description'  => '',
				'action' => [''],
	    	],
	    	//Transaksi Member Point
	    	[
				'name'         => 'transaksi-member-point',
				'display_name' => 'Point',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Member piutang
	    	[
				'name'         => 'transaksi-member-piutang',
				'display_name' => 'Piutang',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Finance
	    	[
				'name'         => 'transaksi-finance',
				'display_name' => 'Finance',
				'description'  => '',
				'action' => [''],
	    	],
	    	//Transaksi Finance top up escrow balance
	    	[
				'name'         => 'transaksi-finance-top-up-escrow-balance',
				'display_name' => 'Top up Escrow Balance',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Finance Direct Invoice
	    	[
				'name'         => 'transaksi-finance-direct-invoice',
				'display_name' => 'Direct Invoice',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Finance Jurnal Manual
	    	[
				'name'         => 'transaksi-finance-jurnal-manual',
				'display_name' => 'Jurnal Manual',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Finance pembatalan jurnal
	    	[
				'name'         => 'transaksi-finance-pembatalan-jurnal',
				'display_name' => 'Pembatalan Jurnal',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Finance Report Jurnal
	    	[
				'name'         => 'transaksi-finance-report-jurnal',
				'display_name' => 'Report Jurnal',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	//Transaksi Aset
	    	[
				'name'         => 'transaksi-aset',
				'display_name' => 'Aset',
				'description'  => '',
				'action' => [''],
	    	],
	    	
	    	//Transaksi Aset Akuisisi aset
	    	[
				'name'         => 'transaksi-aset-akuisisi-aset',
				'display_name' => 'Akuisisi Aset',
				'description'  => '',
				'action' => ['access'],
	    	],

	    	//--------Laporan Rincian--------//
	    	// Laporan & Rincian
	    	[
				'name'         => 'laporan-rincian',
				'display_name' => 'Laporan & Rincian',
				'description'  => '',
				'action' => [''],
	    	],
	    	// Laporan Rincian Rincian
	    	[
				'name'         => 'laporan-rincian-rincian',
				'display_name' => 'Rincian',
				'description'  => '',
				'action' => [''],
	    	],
	    	// Laporan Rincian Rincian Pergerakan Persediaan
	    	[
				'name'         => 'laporan-rincian-rincian-pergerakan-persediaan',
				'display_name' => 'Pergerakan Persediaan',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Laporan Rincian Rincian Status Persediaan
	    	[
				'name'         => 'laporan-rincian-rincian-status-persediaan',
				'display_name' => 'Status Persediaan',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Laporan Rincian Rincian Suplier Inquiry
	    	[
				'name'         => 'laporan-rincian-rincian-suplier-inquiry',
				'display_name' => 'Suplier Inquiry',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Laporan Rincian Rincian Rekening Bank
	    	[
				'name'         => 'laporan-rincian-rincian-rekening-bank',
				'display_name' => 'Rekening Bank',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Laporan Rincian Rincian Report Jurnal
	    	[
				'name'         => 'laporan-rincian-rincian-report-jurnal',
				'display_name' => 'Report Jurnal',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Laporan Rincian Rincian Rincian Neraca
	    	[
				'name'         => 'laporan-rincian-rincian-neraca',
				'display_name' => 'Rincian Neraca',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Laporan Rincian Rincian Rincian Laba Rugi
	    	[
				'name'         => 'laporan-rincian-rincian-rincian-laba-rugi',
				'display_name' => 'Rincian Laba Rugi',
				'description'  => '',
				'action' => ['access'],
	    	],
	    	// Laporan
	    	[
				'name'         => 'laporan-rincian-laporan',
				'display_name' => 'Laporan',
				'description'  => '',
				'action' => [''],
	    	],
	    	[
				'name'         => 'dashboard',
				'display_name' => 'Dashboard',
				'description'  => '',
				'action' => [''],
	    	]
	    ];
	    
    	foreach ($permission as $row) {
    		foreach ($row['action'] as $key => $val) {
    			$temp = [
					'name'         => $row['name'].'-'.$val,
					'display_name' => $row['display_name'].' '.ucfirst($val)
    			];
    			$cek = Permission::where('name', $temp['name'])->first();	
	    		if(!$cek){
    				Permission::create($temp);
	    		}
    		}
    	}
    }
}