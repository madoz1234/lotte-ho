<?php

use Illuminate\Database\Seeder;

class KotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_kota')->insert([
        	'id' => '1',
        	'provinsi_id' => '1',
        	'nama' => 'Serang',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '2',
        	'provinsi_id' => '2',
        	'nama' => 'Jakarta Pusat',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '3',
        	'provinsi_id' => '3',
        	'nama' => 'Bandung',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '4',
        	'provinsi_id' => '4',
        	'nama' => 'Semarang',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '5',
        	'provinsi_id' => '5',
        	'nama' => 'Surabaya',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '6',
        	'provinsi_id' => '6',
        	'nama' => 'Yogyakarta',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '7',
        	'provinsi_id' => '7',
        	'nama' => 'Banda Aceh',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '8',
        	'provinsi_id' => '8',
        	'nama' => 'Medan',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '9',
        	'provinsi_id' => '9',
        	'nama' => 'Palembang',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '10',
        	'provinsi_id' => '10',
        	'nama' => 'Padang',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '11',
        	'provinsi_id' => '11',
        	'nama' => 'Bengkulu',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '12',
        	'provinsi_id' => '12',
        	'nama' => 'Pekanbaru',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '13',
        	'provinsi_id' => '13',
        	'nama' => 'Tanjung Pinang',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '14',
        	'provinsi_id' => '14',
        	'nama' => 'Jambi',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '15',
        	'provinsi_id' => '15',
        	'nama' => 'Bandar Lampung',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '16',
        	'provinsi_id' => '16',
        	'nama' => 'Pangkal Pinang',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '17',
        	'provinsi_id' => '17',
        	'nama' => 'Denpasar',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '18',
        	'provinsi_id' => '18',
        	'nama' => 'Kupang',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '19',
        	'provinsi_id' => '19',
        	'nama' => 'Mataram',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '20',
        	'provinsi_id' => '20',
        	'nama' => 'Gorontalo',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '21',
        	'provinsi_id' => '21',
        	'nama' => 'Mamuju',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '22',
        	'provinsi_id' => '22',
        	'nama' => 'Palu',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '23',
        	'provinsi_id' => '23',
        	'nama' => 'Manado',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '24',
        	'provinsi_id' => '24',
        	'nama' => 'Kendari',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '25',
        	'provinsi_id' => '25',
        	'nama' => 'Makasar',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '26',
        	'provinsi_id' => '26',
        	'nama' => 'Pontianak',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '27',
        	'provinsi_id' => '27',
        	'nama' => 'Samarinda',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '28',
        	'provinsi_id' => '28',
        	'nama' => 'Banjarmasin',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '29',
        	'provinsi_id' => '29',
        	'nama' => 'Palangkaraya',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '30',
        	'provinsi_id' => '30',
        	'nama' => 'Tanjung Selor',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '31',
        	'provinsi_id' => '31',
        	'nama' => 'Ternate',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '32',
        	'provinsi_id' => '32',
        	'nama' => 'Ambon',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '33',
        	'provinsi_id' => '33',
        	'nama' => 'Manokwari',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
        	'id' => '34',
        	'provinsi_id' => '34',
        	'nama' => 'Jayapura',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '35',
            'provinsi_id' => '1',
            'nama' => 'Tangerang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '36',
            'provinsi_id' => '1',
            'nama' => 'Cilegon',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '37',
            'provinsi_id' => '1',
            'nama' => 'Rangkasbitung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '38',
            'provinsi_id' => '2',
            'nama' => 'Jakarta Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '39',
            'provinsi_id' => '2',
            'nama' => 'Jakarta Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '40',
            'provinsi_id' => '2',
            'nama' => 'Jakarta Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '41',
            'provinsi_id' => '2',
            'nama' => 'Jakarta Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '42',
            'provinsi_id' => '2',
            'nama' => 'Kepulauan Seribu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '43',
            'provinsi_id' => '3',
            'nama' => 'Sukabumi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '44',
            'provinsi_id' => '3',
            'nama' => 'Depok',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '45',
            'provinsi_id' => '3',
            'nama' => 'Cirebon',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '46',
            'provinsi_id' => '3',
            'nama' => 'Banjar',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '47',
            'provinsi_id' => '3',
            'nama' => 'Cimahi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '48',
            'provinsi_id' => '3',
            'nama' => 'Bekasi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '49',
            'provinsi_id' => '4',
            'nama' => 'Salatiga',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '50',
            'provinsi_id' => '4',
            'nama' => 'Pekalongan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '51',
            'provinsi_id' => '4',
            'nama' => 'Purwokerto',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '52',
            'provinsi_id' => '4',
            'nama' => 'Tegal',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '53',
            'provinsi_id' => '4',
            'nama' => 'Klaten',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '54',
            'provinsi_id' => '5',
            'nama' => 'Madiun',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '55',
            'provinsi_id' => '5',
            'nama' => 'Probolinggo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '56',
            'provinsi_id' => '5',
            'nama' => 'Pasuruan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '57',
            'provinsi_id' => '5',
            'nama' => 'Mojokerto',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '58',
            'provinsi_id' => '5',
            'nama' => 'Malang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '59',
            'provinsi_id' => '5',
            'nama' => 'Kediri',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '60',
            'provinsi_id' => '7',
            'nama' => 'Langsa',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '61',
            'provinsi_id' => '7',
            'nama' => 'Sabang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '62',
            'provinsi_id' => '7',
            'nama' => 'Lhoksumawe',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '63',
            'provinsi_id' => '8',
            'nama' => 'Binjay',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '64',
            'provinsi_id' => '8',
            'nama' => 'GunungSitoli',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '65',
            'provinsi_id' => '8',
            'nama' => 'TebingTinggi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '66',
            'provinsi_id' => '9',
            'nama' => 'Pagar Alam',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '67',
            'provinsi_id' => '9',
            'nama' => 'Lubuk Linggau',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '68',
            'provinsi_id' => '10',
            'nama' => 'Bukittinggi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '69',
            'provinsi_id' => '10',
            'nama' => 'Padang Panjang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '70',
            'provinsi_id' => '10',
            'nama' => 'Pariaman',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '71',
            'provinsi_id' => '10',
            'nama' => 'Payakumbu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '72',
            'provinsi_id' => '10',
            'nama' => 'Sawahlunto',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '73',
            'provinsi_id' => '10',
            'nama' => 'Solok',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '74',
            'provinsi_id' => '12',
            'nama' => 'Dumai',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '75',
            'provinsi_id' => '15',
            'nama' => 'Metro',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '76',
            'provinsi_id' => '19',
            'nama' => 'Bima',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '77',
            'provinsi_id' => '23',
            'nama' => 'Blitung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '78',
            'provinsi_id' => '23',
            'nama' => 'Tomohon',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '79',
            'provinsi_id' => '24',
            'nama' => 'Bau-bau',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '80',
            'provinsi_id' => '25',
            'nama' => 'Palopo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '81',
            'provinsi_id' => '25',
            'nama' => 'Parepare',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '82',
            'provinsi_id' => '26',
            'nama' => 'Singkawang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '83',
            'provinsi_id' => '27',
            'nama' => 'Bontang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '84',
            'provinsi_id' => '27',
            'nama' => 'Balikpapan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '85',
            'provinsi_id' => '28',
            'nama' => 'Banjar Baru',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '86',
            'provinsi_id' => '31',
            'nama' => 'Tidore',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kota')->insert([
            'id' => '87',
            'provinsi_id' => '32',
            'nama' => 'Tuai',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

         DB::table('ref_kota')->insert([
            'id' => '88',
            'provinsi_id' => '3',
            'nama' => 'Bogor',
            'created_by' => '1',
            'created_at' => '2018-05-25 17:08:35'
        ]);

    }
}
