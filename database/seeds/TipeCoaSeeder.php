<?php

use Illuminate\Database\Seeder;

class TipeCoaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_tipe_coa')->insert([
        	'id' => '1',
            'kode' => '1.0.0.0',
        	'nama' => 'Aktiva',
	        'created_by' => '1',
	        'created_at' => '2018-03-12 18:00:15'
        ]);

        DB::table('ref_tipe_coa')->insert([
        	'id' => '2',
            'kode' => '2.0.0.0',
        	'nama' => 'Liabilitas',
	        'created_by' => '1',
	        'created_at' => '2018-03-12 18:00:16'
        ]);

        DB::table('ref_tipe_coa')->insert([
            'id' => '3',
            'kode' => '3.0.0.0',
            'nama' => 'Ekuitas',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:17'
        ]);

        DB::table('ref_tipe_coa')->insert([
            'id' => '4',
            'kode' => '4.0.0.0',
            'nama' => 'Pendapatan Usaha',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:18'
        ]);

        DB::table('ref_tipe_coa')->insert([
            'id' => '5',
            'kode' => '5.0.0.0',
            'nama' => 'Harga Pokok Penjualan',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:19'
        ]);

        DB::table('ref_tipe_coa')->insert([
            'id' => '6',
            'kode' => '6.0.0.0',
            'nama' => 'Beban Usaha',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:20'
        ]);

        DB::table('ref_tipe_coa')->insert([
            'id' => '7',
            'kode' => '7.0.0.0',
            'nama' => 'Pendapatan Lain-lain',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:20'
        ]);

        DB::table('ref_tipe_coa')->insert([
            'id' => '8',
            'kode' => '8.0.0.0',
            'nama' => 'Beban Lain-lain',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:20'
        ]);
    }
}