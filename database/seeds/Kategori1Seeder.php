<?php

use Illuminate\Database\Seeder;

class Kategori1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_kategori1')->insert([
        	'id' => '1',
            'divisi_kode' => '0612100000',
            'kode' => '35',
        	'nama' => 'Dairy & Frozen',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '2',
            'divisi_kode' => '0612100000',
            'kode' => '80',
            'nama' => 'Bakery',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '3',
            'divisi_kode' => '0612200000',
            'kode' => '11',
            'nama' => 'Biscuit/Snacks',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '4',
            'divisi_kode' => '0612200000',
            'kode' => '12',
            'nama' => 'Drinks/Milk',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '5',
            'divisi_kode' => '0612200000',
            'kode' => '14',
            'nama' => 'Detergent',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '6',
            'divisi_kode' => '0612200000',
            'kode' => '17',
            'nama' => 'Bulk Product',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '7',
            'divisi_kode' => '0612200000',
            'kode' => '19',
            'nama' => 'H&B',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '8',
            'divisi_kode' => '0612200000',
            'kode' => '21',
            'nama' => 'Sauces&Spices',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '9',
            'divisi_kode' => '0612200000',
            'kode' => '99',
            'nama' => 'Non Lotte',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '10',
            'divisi_kode' => '0614300000',
            'kode' => '60',
            'nama' => 'Food Related',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '11',
            'divisi_kode' => '0614300000',
            'kode' => '61',
            'nama' => 'CLEANING & LUGGAGE',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '12',
            'divisi_kode' => '0614300000',
            'kode' => '62',
            'nama' => 'Textile',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '13',
            'divisi_kode' => '0614300000',
            'kode' => '64',
            'nama' => 'Appliance',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '14',
            'divisi_kode' => '0614300000',
            'kode' => '65',
            'nama' => 'Bussiness Supporting',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_kategori1')->insert([
            'id' => '15',
            'divisi_kode' => '0614300000',
            'kode' => '67',
            'nama' => 'Hardware',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15'
        ]);
    }
}
