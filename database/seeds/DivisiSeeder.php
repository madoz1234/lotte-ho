<?php

use Illuminate\Database\Seeder;

class DivisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_divisi')->insert([
        	'id' => '1',
            'kode' => '0612200000',
        	'nama' => 'DRY FOOD',
	        'created_by' => '1',
	        'created_at' => '2018-03-12 18:00:15'
        ]);


        DB::table('ref_divisi')->insert([
            'id' => '2',
            'kode' => '0612100000',
            'nama' => 'FRESH FOOD',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:15'
        ]);


        DB::table('ref_divisi')->insert([
            'id' => '3',
            'kode' => '0614300000',
            'nama' => 'NON FOOD',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:15'
        ]);

        DB::table('ref_divisi')->insert([
            'id' => '4',
            'kode' => '0614400000',
            'nama' => 'FOOD FOOD',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:15'
        ]);

        DB::table('ref_divisi')->insert([
            'id' => '5',
            'kode' => '0699900000',
            'nama' => 'NON LOTTE',
            'created_by' => '1',
            'created_at' => '2018-03-12 18:00:15'
        ]);
    }
}
