<?php

use Illuminate\Database\Seeder;

class ProvinsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_provinsi')->insert([
        	'id' => '1',
        	'nama' => 'Banten',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '2',
        	'nama' => 'DKI Jakarta',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '3',
        	'nama' => 'Jawa Barat',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '4',
        	'nama' => 'Jawa Tengah',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '5',
        	'nama' => 'Jawa Timur',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '6',
        	'nama' => 'DI Yogyakarta',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '7',
        	'nama' => 'Nanggro Aceh Darussalam',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '8',
        	'nama' => 'Sumatra Utara',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '9',
        	'nama' => 'Sumatra Selatan',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '10',
        	'nama' => 'Sumatra Barat',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '11',
        	'nama' => 'Bengkulu',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '12',
        	'nama' => 'Riau',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '13',
        	'nama' => 'Kepulauan Riau',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '14',
        	'nama' => 'Jambi',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '15',
        	'nama' => 'Lampung',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '16',
        	'nama' => 'Bangka Belitung',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '17',
        	'nama' => 'Bali',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '18',
        	'nama' => 'Nusa Tenggara Timur',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '19',
        	'nama' => 'Nusa Tenggara Barat',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '20',
        	'nama' => 'Gorontalo',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '21',
        	'nama' => 'Sulawesi Barat',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '22',
        	'nama' => 'Sulawesi Tengah',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '23',
        	'nama' => 'Sulawesi Utara',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '24',
        	'nama' => 'Sulawesi Tenggara',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '25',
        	'nama' => 'Sulawesi Selatan',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '26',
        	'nama' => 'Kalimantan Barat',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '27',
        	'nama' => 'Kalimantan Timur',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '28',
        	'nama' => 'Kalimantan Selatan',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '29',
        	'nama' => 'Kalimantan Tengah',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '30',
        	'nama' => 'Kalimantan Utara',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '31',
        	'nama' => 'Maluku Utara',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '32',
        	'nama' => 'Maluku',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '33',
        	'nama' => 'Papua Barat',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);

        DB::table('ref_provinsi')->insert([
        	'id' => '34',
        	'nama' => 'Papua',
	        'created_by' => '1',
	        'created_at' => '2018-02-12 18:00:15'
        ]);
    }
}
