<?php

use Illuminate\Database\Seeder;

class KecamatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_kecamatan')->insert([
            'id' => '1',
            'kota_id' => '1',
            'nama' => 'Cipocok Jaya',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '2',
            'kota_id' => '1',
            'nama' => 'Curug',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '3',
            'kota_id' => '1',
            'nama' => 'Kasemen',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '4',
            'kota_id' => '1',
            'nama' => 'Serang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '5',
            'kota_id' => '1',
            'nama' => 'Taktakan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '6',
            'kota_id' => '1',
            'nama' => 'Walantaka',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '7',
            'kota_id' => '2',
            'nama' => 'Gambir',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '8',
            'kota_id' => '2',
            'nama' => 'Tanah Abang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '9',
            'kota_id' => '2',
            'nama' => 'Menteng',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '10',
            'kota_id' => '2',
            'nama' => 'Senen',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '11',
            'kota_id' => '2',
            'nama' => 'Cempaka Putih',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '12',
            'kota_id' => '2',
            'nama' => 'Johar Baru',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '13',
            'kota_id' => '2',
            'nama' => 'Kemayoran',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '14',
            'kota_id' => '2',
            'nama' => 'Sawah Besar',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '15',
            'kota_id' => '3',
            'nama' => 'Andir',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '16',
            'kota_id' => '3',
            'nama' => 'Antapani',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '17',
            'kota_id' => '3',
            'nama' => 'Arcamanik',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '18',
            'kota_id' => '3',
            'nama' => 'Astana Anyar',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '19',
            'kota_id' => '3',
            'nama' => 'Babakan Ciparay',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '20',
            'kota_id' => '3',
            'nama' => 'Bandung Kidul',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '21',
            'kota_id' => '3',
            'nama' => 'Bandung Kulon',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '22',
            'kota_id' => '3',
            'nama' => 'Bandung Wetan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '23',
            'kota_id' => '3',
            'nama' => 'Batununggal',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '24',
            'kota_id' => '3',
            'nama' => 'Bojongloa Kaler',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '25',
            'kota_id' => '3',
            'nama' => 'Bojongloa Kidul',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '26',
            'kota_id' => '3',
            'nama' => 'Buahbatu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '27',
            'kota_id' => '3',
            'nama' => 'Cibeunying Kaler',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '28',
            'kota_id' => '3',
            'nama' => 'Cibeunying Kidul',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '29',
            'kota_id' => '3',
            'nama' => 'Cibiru',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '30',
            'kota_id' => '3',
            'nama' => 'Cicendo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '31',
            'kota_id' => '3',
            'nama' => 'Cidadap',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '32',
            'kota_id' => '3',
            'nama' => 'Cinambo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '33',
            'kota_id' => '3',
            'nama' => 'Coblong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '34',
            'kota_id' => '3',
            'nama' => 'Gedebage',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '35',
            'kota_id' => '3',
            'nama' => 'Kiara Condong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '36',
            'kota_id' => '3',
            'nama' => 'Lengkong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '37',
            'kota_id' => '3',
            'nama' => 'Mandalajati',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '38',
            'kota_id' => '3',
            'nama' => 'Panyileukan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '39',
            'kota_id' => '3',
            'nama' => 'Rancasari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '40',
            'kota_id' => '3',
            'nama' => 'Regol',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '41',
            'kota_id' => '3',
            'nama' => 'Sukajadi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '42',
            'kota_id' => '3',
            'nama' => 'Sukasari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '43',
            'kota_id' => '3',
            'nama' => 'Sumur Bandung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '44',
            'kota_id' => '3',
            'nama' => 'Ujungberung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '45',
            'kota_id' => '4',
            'nama' => 'Banyumanik',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '46',
            'kota_id' => '4',
            'nama' => 'Candisari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '47',
            'kota_id' => '4',
            'nama' => 'Gayamsari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '48',
            'kota_id' => '4',
            'nama' => 'Gunungpati',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '49',
            'kota_id' => '4',
            'nama' => 'Mijen',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '50',
            'kota_id' => '4',
            'nama' => 'Ngaliyan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '51',
            'kota_id' => '4',
            'nama' => 'Pedurungan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '52',
            'kota_id' => '4',
            'nama' => 'Semarang Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '53',
            'kota_id' => '4',
            'nama' => 'Semarang Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '54',
            'kota_id' => '4',
            'nama' => 'Semarang Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '55',
            'kota_id' => '4',
            'nama' => 'Semarang Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '56',
            'kota_id' => '4',
            'nama' => 'Semarang Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '57',
            'kota_id' => '4',
            'nama' => 'Tembalang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '58',
            'kota_id' => '4',
            'nama' => 'Tugu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '59',
            'kota_id' => '5',
            'nama' => 'Asemrowo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '60',
            'kota_id' => '5',
            'nama' => 'Benowo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '61',
            'kota_id' => '5',
            'nama' => 'Bubutan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '62',
            'kota_id' => '5',
            'nama' => ' Bulak',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '63',
            'kota_id' => '5',
            'nama' => 'Dukuh Pakis',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '64',
            'kota_id' => '5',
            'nama' => 'Gayungan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '65',
            'kota_id' => '5',
            'nama' => 'Genteng',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '66',
            'kota_id' => '5',
            'nama' => 'ubeng',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '67',
            'kota_id' => '5',
            'nama' => 'Gunung Anyar',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '68',
            'kota_id' => '6',
            'nama' => 'Danurejan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '6'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '69',
            'kota_id' => '6',
            'nama' => 'Gedong Tengen',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '6'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '70',
            'kota_id' => '6',
            'nama' => 'Gondokusuman',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '6'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '71',
            'kota_id' => '6',
            'nama' => 'Gondomanan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '6'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '72',
            'kota_id' => '6',
            'nama' => 'Jetis',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '6'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '73',
            'kota_id' => '6',
            'nama' => 'Kraton',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '6'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '74',
            'kota_id' => '6',
            'nama' => 'Kotagede',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '6'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '75',
            'kota_id' => '6',
            'nama' => 'Ngampilan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '6'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '76',
            'kota_id' => '7',
            'nama' => 'Baiturrahman',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '77',
            'kota_id' => '7',
            'nama' => 'Banda Raya',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '78',
            'kota_id' => '7',
            'nama' => 'Jaya Baru',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '79',
            'kota_id' => '7',
            'nama' => 'Kuta Alam',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '80',
            'kota_id' => '7',
            'nama' => 'Kuta Raja',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '81',
            'kota_id' => '7',
            'nama' => ' Lueng Bata',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '82',
            'kota_id' => '7',
            'nama' => 'Meuraxa / Meuraksa',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '83',
            'kota_id' => '7',
            'nama' => 'Syiah Kuala',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '84',
            'kota_id' => '8',
            'nama' => 'Medan Amplas',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '85',
            'kota_id' => '8',
            'nama' => 'Medan Area',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '86',
            'kota_id' => '8',
            'nama' => 'Medan Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '87',
            'kota_id' => '8',
            'nama' => ' Medan Baru',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '88',
            'kota_id' => '8',
            'nama' => 'Medan Belawan Kota',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '89',
            'kota_id' => '8',
            'nama' => 'Medan Deli',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '90',
            'kota_id' => '8',
            'nama' => 'Medan Denai',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '91',
            'kota_id' => '9',
            'nama' => 'Alang-Alang Lebar',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '92',
            'kota_id' => '9',
            'nama' => 'Bukit Kecil',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '93',
            'kota_id' => '9',
            'nama' => 'Gandus',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '94',
            'kota_id' => '9',
            'nama' => ' Ilir Barat I (Satu)',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '95',
            'kota_id' => '9',
            'nama' => ' Ilir Barat II (Dua)',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '96',
            'kota_id' => '9',
            'nama' => 'Ilir Timur I (Satu)',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '97',
            'kota_id' => '9',
            'nama' => 'Ilir Timur II (Dua)',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '98',
            'kota_id' => '9',
            'nama' => 'Kalidoni',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '99',
            'kota_id' => '9',
            'nama' => 'Bungus Teluk Kabung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '100',
            'kota_id' => '10',
            'nama' => 'Koto Tangah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '101',
            'kota_id' => '10',
            'nama' => 'Kuranji',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '102',
            'kota_id' => '10',
            'nama' => 'Lubuk Begalung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '103',
            'kota_id' => '10',
            'nama' => 'Lubuk Kilangan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '104',
            'kota_id' => '10',
            'nama' => 'Nanggalo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '105',
            'kota_id' => '11',
            'nama' => 'Gading Cempaka',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '11'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '106',
            'kota_id' => '11',
            'nama' => 'Nanggalo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '11'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '107',
            'kota_id' => '11',
            'nama' => 'Kampung Melayu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '11'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '108',
            'kota_id' => '11',
            'nama' => 'Muara Bangka Hulu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '11'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '109',
            'kota_id' => '11',
            'nama' => 'Ratu Agung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '11'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '110',
            'kota_id' => '11',
            'nama' => 'Ratu Samban',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '11'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '111',
            'kota_id' => '11',
            'nama' => 'Bukit Raya',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '11'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '112',
            'kota_id' => '12',
            'nama' => 'Lima Puluh',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '12'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '113',
            'kota_id' => '12',
            'nama' => 'Marpoyan Damai',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '12'
        ]);
        DB::table('ref_kecamatan')->insert([
            'id' => '114',
            'kota_id' => '12',
            'nama' => 'Payung Sekaki',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '12'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '115',
            'kota_id' => '12',
            'nama' => 'Pekanbaru Kota',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '12'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '116',
            'kota_id' => '12',
            'nama' => 'Rumbai',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '12'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '117',
            'kota_id' => '12',
            'nama' => 'Rumbai Pesisir',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '12'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '118',
            'kota_id' => '13',
            'nama' => 'Bukit Bestari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '13'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '119',
            'kota_id' => '13',
            'nama' => 'Tanjung Pinang Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '13'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '120',
            'kota_id' => '13',
            'nama' => 'Tanjung Pinang Kota',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '13'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '121',
            'kota_id' => '13',
            'nama' => 'Tanjung Pinang Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '13'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '122',
            'kota_id' => '14',
            'nama' => 'Alam Barajo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '14'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '123',
            'kota_id' => '14',
            'nama' => 'Danau Sipin',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '14'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '124',
            'kota_id' => '14',
            'nama' => 'Danau Teluk',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '14'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '125',
            'kota_id' => '14',
            'nama' => 'Jambi Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '14'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '126',
            'kota_id' => '14',
            'nama' => 'Jambi Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '14'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '127',
            'kota_id' => '15',
            'nama' => 'Bumi Waras',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '128',
            'kota_id' => '15',
            'nama' => 'Langkapura',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '129',
            'kota_id' => '15',
            'nama' => 'Tanjung Karang Pusat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '130',
            'kota_id' => '15',
            'nama' => 'Teluk Betung Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '131',
            'kota_id' => '15',
            'nama' => 'Kedaton',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '132',
            'kota_id' => '16',
            'nama' => ' Bukit Intan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '16'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '133',
            'kota_id' => '16',
            'nama' => 'Gerunggang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '16'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '134',
            'kota_id' => '16',
            'nama' => ' Pangkal Balam',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '16'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '135',
            'kota_id' => '16',
            'nama' => 'Rangkui',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '16'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '136',
            'kota_id' => '16',
            'nama' => 'Taman Sari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '16'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '137',
            'kota_id' => '17',
            'nama' => 'Denpasar Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '17'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '138',
            'kota_id' => '17',
            'nama' => 'Denpasar Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '17'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '139',
            'kota_id' => '17',
            'nama' => 'Denpasar Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '17'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '140',
            'kota_id' => '17',
            'nama' => 'Denpasar Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '17'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '141',
            'kota_id' => '18',
            'nama' => 'Alak',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '18'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '142',
            'kota_id' => '18',
            'nama' => 'Kelapa Lima',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '18'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '143',
            'kota_id' => '18',
            'nama' => 'Maulafa',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '18'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '144',
            'kota_id' => '18',
            'nama' => 'Oebobo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '18'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '145',
            'kota_id' => '18',
            'nama' => 'Amabi Oefeto',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '18'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '146',
            'kota_id' => '19',
            'nama' => 'Ampenan ',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '147',
            'kota_id' => '19',
            'nama' => 'Cakranegara ',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '148',
            'kota_id' => '19',
            'nama' => 'Mataram ',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '149',
            'kota_id' => '19',
            'nama' => 'Sandubaya / Sandujaya ',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '150',
            'kota_id' => '19',
            'nama' => 'Sekarbela ',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '151',
            'kota_id' => '19',
            'nama' => 'Selaparang / Selaprang ',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '152',
            'kota_id' => '20',
            'nama' => 'Dungingi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '20'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '153',
            'kota_id' => '20',
            'nama' => 'Kota Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '20'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '154',
            'kota_id' => '20',
            'nama' => ' Kota Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '20'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '155',
            'kota_id' => '20',
            'nama' => 'Kota Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '20'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '156',
            'kota_id' => '20',
            'nama' => ' Kota Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '20'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '157',
            'kota_id' => '21',
            'nama' => 'Bonehau',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '21'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '158',
            'kota_id' => '21',
            'nama' => 'Budong-Budong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '21'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '159',
            'kota_id' => '21',
            'nama' => 'Kalukku',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '21'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '160',
            'kota_id' => '21',
            'nama' => 'Kalumpang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '21'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '161',
            'kota_id' => '21',
            'nama' => 'Karossa',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '21'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '162',
            'kota_id' => '21',
            'nama' => 'Mamuju',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '21'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '163',
            'kota_id' => '22',
            'nama' => 'Palu Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '22'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '164',
            'kota_id' => '22',
            'nama' => 'Palu Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '22'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '165',
            'kota_id' => '22',
            'nama' => 'Palu Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '22'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '166',
            'kota_id' => '22',
            'nama' => 'Palu Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '22'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '167',
            'kota_id' => '22',
            'nama' => 'Mantikulore',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '22'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '168',
            'kota_id' => '22',
            'nama' => 'Ulujadi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '22'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '169',
            'kota_id' => '23',
            'nama' => 'Bunaken',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '170',
            'kota_id' => '23',
            'nama' => 'Malalayang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '171',
            'kota_id' => '23',
            'nama' => 'Sario',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '172',
            'kota_id' => '23',
            'nama' => 'Singkil',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '173',
            'kota_id' => '23',
            'nama' => 'Tikala',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '174',
            'kota_id' => '23',
            'nama' => 'Tuminting',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '175',
            'kota_id' => '24',
            'nama' => 'Abeli',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '176',
            'kota_id' => '24',
            'nama' => 'Baruga',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '177',
            'kota_id' => '24',
            'nama' => 'Kadia',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);
        DB::table('ref_kecamatan')->insert([
            'id' => '178',
            'kota_id' => '24',
            'nama' => 'Kambu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '179',
            'kota_id' => '24',
            'nama' => 'Kendari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '180',
            'kota_id' => '24',
            'nama' => 'Kendari Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '181',
            'kota_id' => '24',
            'nama' => 'Mandonga',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '182',
            'kota_id' => '24',
            'nama' => 'Poasia',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '183',
            'kota_id' => '24',
            'nama' => 'Puuwatu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '184',
            'kota_id' => '24',
            'nama' => 'Wua-Wua',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '185',
            'kota_id' => '25',
            'nama' => 'Wua',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '186',
            'kota_id' => '25',
            'nama' => 'Biringkanaya',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '187',
            'kota_id' => '25',
            'nama' => 'Bontoala',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '188',
            'kota_id' => '25',
            'nama' => 'Makassar',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '189',
            'kota_id' => '25',
            'nama' => 'Mamajang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '190',
            'kota_id' => '25',
            'nama' => 'Mariso',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '191',
            'kota_id' => '25',
            'nama' => 'Panakukkang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '192',
            'kota_id' => '25',
            'nama' => 'Rappocini',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '193',
            'kota_id' => '25',
            'nama' => 'Tallo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '194',
            'kota_id' => '26',
            'nama' => 'Pontianak Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '195',
            'kota_id' => '26',
            'nama' => 'Pontianak Kota',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '196',
            'kota_id' => '26',
            'nama' => 'Pontianak Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '197',
            'kota_id' => '26',
            'nama' => 'Pontianak Tenggara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '198',
            'kota_id' => '26',
            'nama' => 'Pontianak Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '199',
            'kota_id' => '26',
            'nama' => 'Pontianak Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '200',
            'kota_id' => '27',
            'nama' => 'Pontianak',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '201',
            'kota_id' => '27',
            'nama' => 'Sungai Kunjang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '202',
            'kota_id' => '27',
            'nama' => 'Samarinda Ulu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '203',
            'kota_id' => '27',
            'nama' => 'Samarinda Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '204',
            'kota_id' => '27',
            'nama' => 'Samarinda Ilir',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '205',
            'kota_id' => '27',
            'nama' => 'Samarinda Seberang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '206',
            'kota_id' => '27',
            'nama' => 'Palaran',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '207',
            'kota_id' => '28',
            'nama' => 'Banjarmasin Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '208',
            'kota_id' => '28',
            'nama' => 'Banjarmasin Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '209',
            'kota_id' => '28',
            'nama' => 'Banjarmasin Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '210',
            'kota_id' => '28',
            'nama' => 'Banjarmasin Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '211',
            'kota_id' => '28',
            'nama' => 'Banjarmasin Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '212',
            'kota_id' => '29',
            'nama' => 'Bukit Batu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '29'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '213',
            'kota_id' => '29',
            'nama' => 'Jekan Raya',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '29'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '214',
            'kota_id' => '29',
            'nama' => 'Pahandut',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '29'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '215',
            'kota_id' => '29',
            'nama' => 'Rakumpit',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '29'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '216',
            'kota_id' => '29',
            'nama' => 'Sebangau',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '29'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '217',
            'kota_id' => '30',
            'nama' => 'Peso',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '30'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '218',
            'kota_id' => '30',
            'nama' => 'Peso Hilir / Peso Ilir',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '30'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '219',
            'kota_id' => '30',
            'nama' => 'Pulau Bunyu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '30'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '220',
            'kota_id' => '30',
            'nama' => 'Sekatak',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '30'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '221',
            'kota_id' => '30',
            'nama' => 'Tanjung Palas',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '30'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '222',
            'kota_id' => '30',
            'nama' => 'Tanjung Palas Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '30'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '223',
            'kota_id' => '30',
            'nama' => 'Tanjung Palas Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '30'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '224',
            'kota_id' => '30',
            'nama' => 'Tanjung Palas Timurt',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '30'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '225',
            'kota_id' => '30',
            'nama' => 'Tanjung Palas Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '30'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '226',
            'kota_id' => '31',
            'nama' => 'Moti / Pulau Moti',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '227',
            'kota_id' => '31',
            'nama' => 'Pulau Batang Dua',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '228',
            'kota_id' => '31',
            'nama' => 'Pulau Ternate',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '229',
            'kota_id' => '31',
            'nama' => 'Ternate Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '230',
            'kota_id' => '31',
            'nama' => 'Ternate Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '231',
            'kota_id' => '31',
            'nama' => 'Ternate Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '232',
            'kota_id' => '32',
            'nama' => 'Baguala',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '32'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '233',
            'kota_id' => '32',
            'nama' => 'Leitimur Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '32'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '234',
            'kota_id' => '32',
            'nama' => 'Nusaniwe / Nusanive',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '32'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '235',
            'kota_id' => '32',
            'nama' => 'Sirimau',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '32'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '236',
            'kota_id' => '32',
            'nama' => 'Teluk Ambon',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '32'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '237',
            'kota_id' => '33',
            'nama' => 'Anggi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '33'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '238',
            'kota_id' => '33',
            'nama' => 'Anggi Gida',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '33'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '239',
            'kota_id' => '33',
            'nama' => 'Catubouw',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '33'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '240',
            'kota_id' => '33',
            'nama' => 'Didohu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '33'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '241',
            'kota_id' => '33',
            'nama' => 'Hingk',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '33'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '242',
            'kota_id' => '34',
            'nama' => 'Abepura',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '34'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '243',
            'kota_id' => '34',
            'nama' => 'Heram',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '34'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '244',
            'kota_id' => '34',
            'nama' => 'Jayapura Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '34'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '245',
            'kota_id' => '34',
            'nama' => 'Jayapura Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '34'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '246',
            'kota_id' => '34',
            'nama' => ' Muara Tami',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '34'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '247',
            'kota_id' => '35',
            'nama' => ' Batuceper',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '248',
            'kota_id' => '35',
            'nama' => ' Benda',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '249',
            'kota_id' => '35',
            'nama' => ' Cibodas',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '250',
            'kota_id' => '35',
            'nama' => ' Ciledug',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '251',
            'kota_id' => '35',
            'nama' => ' Cipondoh',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '252',
            'kota_id' => '35',
            'nama' => ' Jatiuwung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '253',
            'kota_id' => '36',
            'nama' => ' Cibeber',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '254',
            'kota_id' => '36',
            'nama' => ' Cilegon',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '255',
            'kota_id' => '36',
            'nama' => ' Citangkil',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '256',
            'kota_id' => '36',
            'nama' => ' Gerogol',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '257',
            'kota_id' => '36',
            'nama' => ' Pulomerak',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '258',
            'kota_id' => '36',
            'nama' => ' Jombang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '259',
            'kota_id' => '37',
            'nama' => ' Banjarsari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '260',
            'kota_id' => '37',
            'nama' => ' Bayah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '261',
            'kota_id' => '37',
            'nama' => ' Bojongmanik',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '262',
            'kota_id' => '37',
            'nama' => ' Cibadak',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '263',
            'kota_id' => '37',
            'nama' => ' Cibeber',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '264',
            'kota_id' => '37',
            'nama' => ' Cigemblong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '1'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '265',
            'kota_id' => '38',
            'nama' => ' Cilincing',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '266',
            'kota_id' => '38',
            'nama' => ' Kelapa Gading',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '267',
            'kota_id' => '38',
            'nama' => ' Koja',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '268',
            'kota_id' => '38',
            'nama' => ' Pademangan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '269',
            'kota_id' => '38',
            'nama' => ' Penjaringan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '270',
            'kota_id' => '38',
            'nama' => ' Tanjung Priok',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '271',
            'kota_id' => '39',
            'nama' => ' Cengkareng',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '272',
            'kota_id' => '39',
            'nama' => ' Grogol Petamburan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '273',
            'kota_id' => '39',
            'nama' => ' Kalideres',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '274',
            'kota_id' => '39',
            'nama' => '  Kebon Jeruk',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '275',
            'kota_id' => '39',
            'nama' => ' Kembangan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '276',
            'kota_id' => '39',
            'nama' => ' Palmerah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '277',
            'kota_id' => '39',
            'nama' => ' Taman Sari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '278',
            'kota_id' => '39',
            'nama' => ' Tambora',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '279',
            'kota_id' => '40',
            'nama' => ' Cilandak',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '280',
            'kota_id' => '40',
            'nama' => ' Jagakarsa',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '281',
            'kota_id' => '40',
            'nama' => ' Kebayoran Baru',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '282',
            'kota_id' => '40',
            'nama' => ' Kebayoran Lama',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '283',
            'kota_id' => '40',
            'nama' => ' Mampang Prapatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '284',
            'kota_id' => '40',
            'nama' => ' Pancoran',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '285',
            'kota_id' => '40',
            'nama' => ' Pasar Minggu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '286',
            'kota_id' => '40',
            'nama' => ' Pesanggrahan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '287',
            'kota_id' => '40',
            'nama' => ' Setia Budi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '288',
            'kota_id' => '40',
            'nama' => ' Tebet',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '289',
            'kota_id' => '41',
            'nama' => ' Cakung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '290',
            'kota_id' => '41',
            'nama' => ' Ciracas',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '291',
            'kota_id' => '41',
            'nama' => ' Duren Sawit',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '292',
            'kota_id' => '41',
            'nama' => ' Jatinegara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '293',
            'kota_id' => '41',
            'nama' => ' Kramat Jati',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '294',
            'kota_id' => '41',
            'nama' => ' Pasar Rebo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '295',
            'kota_id' => '41',
            'nama' => ' Pulo Gadung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '296',
            'kota_id' => '42',
            'nama' => ' Kepulauan Seribu Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '297',
            'kota_id' => '42',
            'nama' => ' Kepulauan Seribu Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '2'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '298',
            'kota_id' => '43',
            'nama' => ' Baros',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '299',
            'kota_id' => '43',
            'nama' => ' Cibeureum',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '300',
            'kota_id' => '43',
            'nama' => ' Cikole',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '301',
            'kota_id' => '43',
            'nama' => ' Citamiang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '302',
            'kota_id' => '43',
            'nama' => ' Gunung Puyuh',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '303',
            'kota_id' => '43',
            'nama' => ' Warudoyong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '304',
            'kota_id' => '44',
            'nama' => ' Warudoyong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '305',
            'kota_id' => '44',
            'nama' => ' Warudoyong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '306',
            'kota_id' => '44',
            'nama' => ' Warudoyong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '307',
            'kota_id' => '44',
            'nama' => ' Warudoyong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '308',
            'kota_id' => '44',
            'nama' => ' Warudoyong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '309',
            'kota_id' => '44',
            'nama' => ' Warudoyong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '310',
            'kota_id' => '45',
            'nama' => ' Harjamukti',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '311',
            'kota_id' => '45',
            'nama' => ' Kejaksan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);
        DB::table('ref_kecamatan')->insert([
            'id' => '312',
            'kota_id' => '45',
            'nama' => ' Kesambi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '313',
            'kota_id' => '45',
            'nama' => ' Lemahwungkuk',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '314',
            'kota_id' => '45',
            'nama' => ' Pekalipan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '315',
            'kota_id' => '46',
            'nama' => ' Banjar',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '316',
            'kota_id' => '46',
            'nama' => ' Langensari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '317',
            'kota_id' => '46',
            'nama' => ' Pataruman',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '318',
            'kota_id' => '46',
            'nama' => ' Purwaharja',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '319',
            'kota_id' => '47',
            'nama' => ' Cimahi Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '320',
            'kota_id' => '47',
            'nama' => ' Cimahi Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '321',
            'kota_id' => '47',
            'nama' => '  Cimahi Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '322',
            'kota_id' => '48',
            'nama' => ' Bantar Gebang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '323',
            'kota_id' => '48',
            'nama' => ' Bekasi Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);
        DB::table('ref_kecamatan')->insert([
            'id' => '324',
            'kota_id' => '48',
            'nama' => ' Bekasi Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '325',
            'kota_id' => '48',
            'nama' => ' Bekasi Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '326',
            'kota_id' => '48',
            'nama' => ' Bekasi Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '327',
            'kota_id' => '48',
            'nama' => ' Jati Sampurna',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '328',
            'kota_id' => '48',
            'nama' => ' Jatiasih',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '329',
            'kota_id' => '48',
            'nama' => ' Medan Satria',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '330',
            'kota_id' => '48',
            'nama' => ' Mustika Jaya',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '331',
            'kota_id' => '48',
            'nama' => ' Pondok Gede',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '332',
            'kota_id' => '48',
            'nama' => ' Pondok Melati',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '333',
            'kota_id' => '48',
            'nama' => ' Rawalumbu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '3'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '334',
            'kota_id' => '49',
            'nama' => ' Argomulyo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '335',
            'kota_id' => '49',
            'nama' => ' Sidomukti',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '336',
            'kota_id' => '49',
            'nama' => ' Sidorejo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '337',
            'kota_id' => '49',
            'nama' => ' Tingkir',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '338',
            'kota_id' => '50',
            'nama' => ' Pekalongan Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '339',
            'kota_id' => '50',
            'nama' => ' Pekalongan Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '340',
            'kota_id' => '50',
            'nama' => ' Pekalongan Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '341',
            'kota_id' => '50',
            'nama' => ' Pekalongan Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '342',
            'kota_id' => '51',
            'nama' => ' Ajibarang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '343',
            'kota_id' => '51',
            'nama' => ' Banyumas',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '344',
            'kota_id' => '51',
            'nama' => ' Baturaden',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '345',
            'kota_id' => '51',
            'nama' => ' Cilongok',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '346',
            'kota_id' => '51',
            'nama' => ' Gumelar',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '347',
            'kota_id' => '51',
            'nama' => ' Kalibagor',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '348',
            'kota_id' => '51',
            'nama' => ' Jatilawang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '349',
            'kota_id' => '51',
            'nama' => ' Karanglewas',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '350',
            'kota_id' => '51',
            'nama' => ' Kebasen',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '351',
            'kota_id' => '51',
            'nama' => ' Kedung Banteng',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '352',
            'kota_id' => '51',
            'nama' => ' Kembaran',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '353',
            'kota_id' => '52',
            'nama' => ' Margadana',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '354',
            'kota_id' => '52',
            'nama' => ' Tegal Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '355',
            'kota_id' => '52',
            'nama' => ' Tegal Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '356',
            'kota_id' => '52',
            'nama' => ' Tegal Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '357',
            'kota_id' => '53',
            'nama' => ' Bayat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '358',
            'kota_id' => '53',
            'nama' => ' Cawas',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '359',
            'kota_id' => '53',
            'nama' => ' Ceper',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '360',
            'kota_id' => '53',
            'nama' => ' Delanggu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '361',
            'kota_id' => '53',
            'nama' => ' Gantiwarno',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '4'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '362',
            'kota_id' => '54',
            'nama' => ' Kartoharjo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '363',
            'kota_id' => '54',
            'nama' => ' Manguharjo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '364',
            'kota_id' => '54',
            'nama' => ' Taman',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '365',
            'kota_id' => '55',
            'nama' => ' Kademangan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '366',
            'kota_id' => '55',
            'nama' => ' Mayangan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '367',
            'kota_id' => '55',
            'nama' => ' Wonoasih',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '368',
            'kota_id' => '56',
            'nama' => ' Bugul Kidul',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '369',
            'kota_id' => '56',
            'nama' => ' Gadingrejo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '370',
            'kota_id' => '56',
            'nama' => ' Purworejo',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '371',
            'kota_id' => '57',
            'nama' => ' Magersari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '372',
            'kota_id' => '57',
            'nama' => ' Prajurit Kulon',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '373',
            'kota_id' => '58',
            'nama' => ' Blimbing',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '374',
            'kota_id' => '58',
            'nama' => ' Kedungkandang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '375',
            'kota_id' => '58',
            'nama' => ' Klojen',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '376',
            'kota_id' => '58',
            'nama' => ' Lowokwaru',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '377',
            'kota_id' => '59',
            'nama' => ' Kediri Kota',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '378',
            'kota_id' => '59',
            'nama' => ' Mojoroto',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '379',
            'kota_id' => '59',
            'nama' => ' Pesantren',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '5'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '380',
            'kota_id' => '60',
            'nama' => ' Langsa Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '381',
            'kota_id' => '60',
            'nama' => ' Langsa Baro',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '382',
            'kota_id' => '60',
            'nama' => ' Langsa Kota',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '383',
            'kota_id' => '60',
            'nama' => ' Langsa Lama',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '384',
            'kota_id' => '61',
            'nama' => ' Sukajaya',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '385',
            'kota_id' => '61',
            'nama' => ' Sukakarya',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '386',
            'kota_id' => '62',
            'nama' => ' Banda Sakti',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '387',
            'kota_id' => '62',
            'nama' => ' Blang Mangat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '389',
            'kota_id' => '62',
            'nama' => ' Muara Dua',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '390',
            'kota_id' => '62',
            'nama' => ' Muara Satu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '7'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '391',
            'kota_id' => '63',
            'nama' => ' Binjai Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '392',
            'kota_id' => '63',
            'nama' => ' Binjai Kota',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '393',
            'kota_id' => '63',
            'nama' => ' Binjai Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '394',
            'kota_id' => '63',
            'nama' => ' Binjai Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '395',
            'kota_id' => '63',
            'nama' => ' Binjai Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '396',
            'kota_id' => '64',
            'nama' => ' Gunungsitoli',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '397',
            'kota_id' => '64',
            'nama' => ' Gunungsitoli Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '398',
            'kota_id' => '64',
            'nama' => ' Gunungsitoli Idanoi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '399',
            'kota_id' => '64',
            'nama' => ' Gunungsitoli Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '400',
            'kota_id' => '65',
            'nama' => ' Bajenis',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '401',
            'kota_id' => '65',
            'nama' => ' Padang Hilir',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '402',
            'kota_id' => '65',
            'nama' => ' Padang Hulu',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '403',
            'kota_id' => '65',
            'nama' => ' Rambutan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '404',
            'kota_id' => '65',
            'nama' => ' Tebing Tinggi Kota',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '8'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '405',
            'kota_id' => '66',
            'nama' => ' Dempo Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '406',
            'kota_id' => '66',
            'nama' => ' Dempo Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '407',
            'kota_id' => '66',
            'nama' => ' Dempo Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '408',
            'kota_id' => '66',
            'nama' => ' Pagar Alam Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '409',
            'kota_id' => '66',
            'nama' => ' Pagar Alam Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '410',
            'kota_id' => '67',
            'nama' => ' Lubuk Linggau Barat Dua (II)',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '411',
            'kota_id' => '67',
            'nama' => ' Lubuk Linggau Barat Satu (I)',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '412',
            'kota_id' => '67',
            'nama' => ' Lubuk Linggau Selatan Dua (II)',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '413',
            'kota_id' => '67',
            'nama' => 'Lubuk Linggau Selatan Satu (I)',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '9'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '414',
            'kota_id' => '68',
            'nama' => 'Aur Birugo Tigo Baleh',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '415',
            'kota_id' => '68',
            'nama' => 'Guguk Panjang / Guguak Panjang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '416',
            'kota_id' => '68',
            'nama' => 'Mandiangin Koto Selayan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '417',
            'kota_id' => '69',
            'nama' => 'Padang Panjang Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '418',
            'kota_id' => '69',
            'nama' => 'Padang Panjang Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '419',
            'kota_id' => '70',
            'nama' => 'Pariaman Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '420',
            'kota_id' => '70',
            'nama' => 'Pariaman Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '421',
            'kota_id' => '70',
            'nama' => 'Pariaman Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '422',
            'kota_id' => '71',
            'nama' => 'Lamposi Tigo Nagari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '423',
            'kota_id' => '71',
            'nama' => 'Payakumbuh Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '424',
            'kota_id' => '71',
            'nama' => 'Payakumbuh Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '425',
            'kota_id' => '71',
            'nama' => 'Payakumbuh Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '426',
            'kota_id' => '72',
            'nama' => 'Barangin',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '427',
            'kota_id' => '72',
            'nama' => 'Lembah Segar',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '428',
            'kota_id' => '72',
            'nama' => 'Silungkang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '429',
            'kota_id' => '72',
            'nama' => 'Talawi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '430',
            'kota_id' => '73',
            'nama' => 'Lubuk Sikarah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '431',
            'kota_id' => '73',
            'nama' => 'Tanjung Harapan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '10'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '432',
            'kota_id' => '74',
            'nama' => 'Bukit Kapur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '12'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '433',
            'kota_id' => '74',
            'nama' => 'Dumai Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '12'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '434',
            'kota_id' => '74',
            'nama' => 'Dumai Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '12'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '435',
            'kota_id' => '75',
            'nama' => 'Metro Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '436',
            'kota_id' => '75',
            'nama' => 'Metro Pusat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '437',
            'kota_id' => '75',
            'nama' => 'Metro Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '438',
            'kota_id' => '75',
            'nama' => 'Metro Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '439',
            'kota_id' => '75',
            'nama' => 'Metro Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '15'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '440',
            'kota_id' => '76',
            'nama' => 'Asakota',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '441',
            'kota_id' => '76',
            'nama' => 'Mpunda',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '442',
            'kota_id' => '76',
            'nama' => 'Raba',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '443',
            'kota_id' => '76',
            'nama' => 'Rasanae Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '444',
            'kota_id' => '76',
            'nama' => 'Rasanae Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '19'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '445',
            'kota_id' => '77',
            'nama' => 'Badau',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '446',
            'kota_id' => '77',
            'nama' => 'Membalong',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '447',
            'kota_id' => '77',
            'nama' => 'Selat Nasik',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '448',
            'kota_id' => '77',
            'nama' => 'Sijuk',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '449',
            'kota_id' => '78',
            'nama' => 'Tomohon Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '450',
            'kota_id' => '78',
            'nama' => 'Tomohon Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '451',
            'kota_id' => '78',
            'nama' => 'Tomohon Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '452',
            'kota_id' => '78',
            'nama' => 'Tomohon Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '453',
            'kota_id' => '78',
            'nama' => 'Tomohon Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '23'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '454',
            'kota_id' => '79',
            'nama' => 'Betoambari',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '455',
            'kota_id' => '79',
            'nama' => 'Bungi',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '456',
            'kota_id' => '79',
            'nama' => 'Sora Walio / Sorowalio',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '457',
            'kota_id' => '79',
            'nama' => 'Murhum',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '24'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '458',
            'kota_id' => '80',
            'nama' => 'Bara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '459',
            'kota_id' => '80',
            'nama' => 'Mungkajang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '460',
            'kota_id' => '80',
            'nama' => 'Sendana',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '461',
            'kota_id' => '80',
            'nama' => 'Telluwanua',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '462',
            'kota_id' => '81',
            'nama' => 'Bacukiki',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '463',
            'kota_id' => '81',
            'nama' => 'Bacukiki Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '464',
            'kota_id' => '81',
            'nama' => 'Soreang',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '465',
            'kota_id' => '81',
            'nama' => 'Ujung',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '25'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '466',
            'kota_id' => '82',
            'nama' => 'Singkawang Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '467',
            'kota_id' => '82',
            'nama' => 'Singkawang Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '468',
            'kota_id' => '82',
            'nama' => 'Singkawang Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '469',
            'kota_id' => '82',
            'nama' => 'Singkawang Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '470',
            'kota_id' => '82',
            'nama' => 'Singkawang Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '26'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '471',
            'kota_id' => '83',
            'nama' => 'Bontang Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '472',
            'kota_id' => '83',
            'nama' => 'Bontang Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '473',
            'kota_id' => '83',
            'nama' => 'Bontang Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '27'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '474',
            'kota_id' => '84',
            'nama' => 'Balikpapan Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '475',
            'kota_id' => '84',
            'nama' => 'Balikpapan Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '476',
            'kota_id' => '84',
            'nama' => 'Balikpapan Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '477',
            'kota_id' => '84',
            'nama' => 'Balikpapan Timur',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '478',
            'kota_id' => '84',
            'nama' => 'Balikpapan Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '479',
            'kota_id' => '85',
            'nama' => 'Banjar Baru Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '480',
            'kota_id' => '85',
            'nama' => 'Landasan Ulin',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '481',
            'kota_id' => '85',
            'nama' => 'Cempaka',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '482',
            'kota_id' => '85',
            'nama' => 'Liang Anggan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '28'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '483',
            'kota_id' => '86',
            'nama' => 'Oba',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '484',
            'kota_id' => '86',
            'nama' => 'Oba Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '485',
            'kota_id' => '86',
            'nama' => 'Oba Tengah',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '486',
            'kota_id' => '86',
            'nama' => 'Oba Utara',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '31'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '487',
            'kota_id' => '87',
            'nama' => 'Morotai Jaya',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '32'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '488',
            'kota_id' => '87',
            'nama' => 'Morotai Selatan',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '32'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '489',
            'kota_id' => '87',
            'nama' => 'Morotai Selatan Barat',
            'created_by' => '1',
            'created_at' => '2018-02-12 18:00:15',
            'provinsi_id' => '32'
        ]);

        DB::table('ref_kecamatan')->insert([
            'id' => '490',
            'kota_id' => '41',
            'nama' => 'Royal Residence',
            'created_by' => '1',
            'created_at' => '2018-05-25 16:56:49',
            'provinsi_id' => '2'
        ]);

    }
}
