<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRefLsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ref_lsi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lsi_id')->unsigned();
            $table->integer('region_id')->unsigned();
            $table->integer('kota_id')->unsigned();
            $table->integer('rekening_escrow_id')->unsigned();
            $table->integer('bank_escrow_id')->unsigned();
            $table->string('kode', 10);
            $table->string('nama', 50);
            $table->text('alamat');
            $table->string('kode_pos', 5);
            $table->string('telepon', 15);
            $table->string('email', 50);
            $table->string('longitude', 10);
            $table->string('latitude', 10);
            $table->string('npwp_perusahaan', 30);
            $table->string('nama_perusahaan', 50);
            $table->text('alamat_perusahaan');
            $table->string('kode_bank', 10);
            $table->string('nomor_rekening', 20);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_ref_lsi');
    }
}
