<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefTmukAddSaldo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_tmuk', function (Blueprint $table) {
            $table->decimal('saldo_scn', 20, 2)->nullable();
            $table->decimal('saldo_escrow', 20, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_tmuk', function (Blueprint $table) {
            $table->dropColumn('saldo_scn');
            $table->dropColumn('saldo_escrow');
        });
    }
}
