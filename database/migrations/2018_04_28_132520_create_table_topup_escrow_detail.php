<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTopupEscrowDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('trans_topup_escrow_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_topup_escrow')->unsigned();
            $table->string('week')->nullable();
            $table->decimal('total',20,2);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_topup_escrow')->references('id')->on('trans_topup_escrow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_topup_escrow_detail');
    }
}
