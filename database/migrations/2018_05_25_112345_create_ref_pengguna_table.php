<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefPenggunaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_pengguna', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('nama_lengkap', 50);
            $table->string('telepon', 18);
            $table->string('email');
            $table->integer('lsi_id')->unsigned();
            $table->date('deleted_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('sys_users');
            $table->foreign('lsi_id')->references('id')->on('ref_lsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_pengguna');

        // Schema::table('ref_pengguna', function (Blueprint $table) {
        //     $table->
        // });
    }
}
