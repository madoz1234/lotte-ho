<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReturDanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_retur', function (Blueprint $table) {
            $table->increments('id');

            $table->string('tmuk_kode', 10);
            $table->integer('nomor_po')->unsigned();
            $table->string('nomor_retur', 30);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
            $table->foreign('nomor_po')->references('id')->on('trans_po');
        });

        Schema::create('trans_retur_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('retur_id')->unsigned()->nullable();
            $table->string('produk_kode', 30);
            $table->integer('qty')->default(0);
            $table->string('catatan', 100);
            $table->integer('qty_retur')->default(0);
            $table->text('reason')->nullable();
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('retur_id')->references('id')->on('trans_retur');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_retur_detail');
        Schema::dropIfExists('trans_retur');
    }
}
