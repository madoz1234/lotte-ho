<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRefPajak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ref_pajak', function (Blueprint $table) {
            $table->integer('pajak_id')->unsigned();
            $table->increments('id');
            $table->string('npwp', 15);
            $table->string('nama', 30);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_ref_pajak');
    }
}
