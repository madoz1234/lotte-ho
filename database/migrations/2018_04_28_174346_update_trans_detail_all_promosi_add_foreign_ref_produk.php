<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransDetailAllPromosiAddForeignRefProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_detail_promosi_diskon', function (Blueprint $table) {
            $table->foreign('kode_produk')->references('kode')->on('ref_produk');
        });

        Schema::table('ref_detail_promosi_promo', function (Blueprint $table) {
            $table->foreign('kode_produk')->references('kode')->on('ref_produk');
        });

        Schema::table('ref_detail_promosi_purchase', function (Blueprint $table) {
            $table->foreign('kode_produk')->references('kode')->on('ref_produk');
        });

        Schema::table('ref_detail_promosi_purchase_list', function (Blueprint $table) {
            $table->foreign('kode_produk')->references('kode')->on('ref_produk');
        });

        Schema::table('ref_detail_promosi_buy_x_get_y', function (Blueprint $table) {
            $table->foreign('kode_produk_x')->references('kode')->on('ref_produk');
            $table->foreign('kode_produk_y')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_detail_promosi_diskon', function (Blueprint $table) {
            $table->dropForeign(['kode_produk']);
        });

        Schema::table('ref_detail_promosi_promo', function (Blueprint $table) {
            $table->dropForeign(['kode_produk']);
        });

        Schema::table('ref_detail_promosi_purchase', function (Blueprint $table) {
            $table->dropForeign(['kode_produk']);
        });

        Schema::table('ref_detail_promosi_purchase_list', function (Blueprint $table) {
            $table->dropForeign(['kode_produk']);
        }); 

        Schema::table('ref_detail_promosi_buy_x_get_y', function (Blueprint $table) {
            $table->dropForeign(['kode_produk_x']);
            $table->dropForeign(['kode_produk_y']);
        });
    }
}
