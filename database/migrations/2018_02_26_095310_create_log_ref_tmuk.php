<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRefTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ref_tmuk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tmuk_id')->unsigned();
            $table->integer('lsi_id')->unsigned();
            $table->integer('kota_id')->unsigned();
            $table->integer('kecamatan_id')->unsigned();
            $table->integer('membercard_id')->unsigned();
            $table->integer('jenis_assortment_id')->unsigned();
            $table->integer('rekening_escrow_id')->unsigned();
            $table->integer('bank_escrow_id')->unsigned();
            $table->string('kode', 10);
            $table->string('nama', 50);
            $table->text('alamat');
            $table->string('kode_pos', 5);
            $table->string('telepon', 15);
            $table->string('email', 50)->nullable();
            $table->integer('asn')->comment('1:Ya; 0:Tidak');
            $table->integer('auto_approve')->comment('1:Ya; 0:Tidak');
            $table->string('longitude', 10)->nullable();
            $table->string('latitude', 10)->nullable();
            $table->decimal('gross_area', 10, 2);
            $table->decimal('selling_area', 10, 2);
            $table->string('nama_cde', 50);
            $table->string('email_cde', 50);
            $table->date('rencana_pembukaan');
            $table->date('aktual_pembukaan');
            $table->string('npwp_perusahaan', 30);
            $table->string('nama_perusahaan', 50);
            $table->text('alamat_perusahaan');
            $table->string('nomor_rekening', 20);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_ref_tmuk');
    }
}
