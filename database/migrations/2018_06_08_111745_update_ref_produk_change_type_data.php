<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefProdukChangeTypeData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk', function (Blueprint $table) {
            $table->decimal('width', 10, 2)->nullable()->change();
            $table->decimal('length', 10, 2)->nullable()->change();
            $table->decimal('height', 10, 2)->nullable()->change();
            $table->decimal('wg', 10, 2)->nullable()->change();
            $table->decimal('inner_width', 10, 2)->nullable()->change();
            $table->decimal('inner_length', 10, 2)->nullable()->change();
            $table->decimal('inner_height', 10, 2)->nullable()->change();
            $table->decimal('inner_wg', 10, 2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk', function (Blueprint $table) {
            $table->float('width')->nullable()->change();
            $table->float('length')->nullable()->change();
            $table->float('height')->nullable()->change();
            $table->float('wg')->nullable()->change();
            $table->float('inner_width')->nullable()->change();
            $table->float('inner_length')->nullable()->change();
            $table->float('inner_height')->nullable()->change();
            $table->float('inner_wg')->nullable()->change();
        });
    }
}
