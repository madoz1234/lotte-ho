<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefProdukTmukAddStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk_tmuk', function (Blueprint $table) {
            $table->integer('stock_awal')->nullable();
            $table->integer('stock_akhir')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk_tmuk', function (Blueprint $table) {
            $table->dropColumn('stock_awal');
            $table->dropColumn('stock_akhir');
        });
    }
}
