<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHargaAwalRefDetailPromosiDiskon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_detail_promosi_diskon', function (Blueprint $table) {
            $table->decimal('harga_awal', 10, 2)->after('cost_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_detail_promosi_diskon', function (Blueprint $table) {
            $table->dropColumn('harga_awal');
        });
    }
}
