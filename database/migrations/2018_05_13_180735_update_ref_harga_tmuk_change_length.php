<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefHargaTmukChangeLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_harga_tmuk', function (Blueprint $table) {
            $table->decimal('cost_price', 25, 20)->nullable()->change();
            $table->decimal('suggest_price', 25, 20)->nullable()->change();
            $table->decimal('change_price', 25, 20)->nullable()->change();
            $table->decimal('margin_amount', 25, 20)->nullable()->change();
            $table->decimal('map', 25, 20)->nullable()->change();
            $table->decimal('gmd_price', 25, 20)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_harga_tmuk', function (Blueprint $table) {
            $table->decimal('cost_price', 10, 2)->nullable()->change();
            $table->decimal('suggest_price', 10, 2)->nullable()->change();
            $table->decimal('change_price', 10, 2)->nullable()->change();
            $table->decimal('margin_amount', 10, 2)->nullable()->change();
            $table->decimal('map', 10, 2)->nullable()->change();
            $table->decimal('gmd_price', 10, 2)->nullable()->change();
        });
    }
}
