<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalJualOnTopup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_topup_escrow', function (Blueprint $table) {
            $table->integer('total_jual')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_topup_escrow', function (Blueprint $table) {
            $table->dropColumn('total_jual');
        });
    }
}
