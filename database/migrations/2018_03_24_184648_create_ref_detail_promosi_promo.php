<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefDetailPromosiPromo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_detail_promosi_promo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('promosi_id')->unsigned();
            $table->string('kode_produk', 30);
            $table->string('nama_produk',255);
            $table->decimal('selling_price', 10, 2);
            $table->decimal('harga_terdiskon', 10, 2);
            $table->float('diskon');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('promosi_id')->references('id')->on('ref_promosi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_detail_promosi_promo');
    }
}
