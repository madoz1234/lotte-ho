<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefKategori3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kategori3', function (Blueprint $table) {
            $table->increments('id');
            $table->string('divisi_kode', 10)->unsigned();
            $table->string('kategori2_kode', 10)->unsigned();
            $table->string('kode', 10)->unique();
            $table->string('nama', 255);


            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('divisi_kode')->references('kode')->on('ref_divisi');
            $table->foreign('kategori2_kode')->references('kode')->on('ref_kategori2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_kategori3');
    }
}
