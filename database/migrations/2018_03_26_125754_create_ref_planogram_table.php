<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefPlanogramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_planogram', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 10);
            $table->string('nama', 50);
            $table->integer('jenis_assortment_id')->unsigned();
            $table->boolean('status')->default(1);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('jenis_assortment_id')->references('id')->on('ref_jenis_assortment');
        });
        Schema::create('log_ref_planogram', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('planogram_id')->unsigned();
            $table->string('kode', 10);
            $table->string('nama', 50);
            $table->integer('jenis_assortment_id')->unsigned();
            $table->boolean('status')->default(1);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ref_planogram');
        Schema::dropIfExists('ref_planogram');
    }
}
