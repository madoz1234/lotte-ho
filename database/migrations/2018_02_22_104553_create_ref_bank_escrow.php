<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefBankEscrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_bank_escrow', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kota_id')->unsigned();
            $table->string('nama', 30);
            $table->text('alamat');
            $table->string('kode_pos', 5);
            $table->string('telepon', 15);
            $table->string('email', 50);
            $table->date('tgl_mulai');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('kota_id')->references('id')->on('ref_kota');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_bank_escrow');
    }
}
