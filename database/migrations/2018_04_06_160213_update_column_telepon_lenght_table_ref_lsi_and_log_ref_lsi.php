<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnTeleponLenghtTableRefLsiAndLogRefLsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_lsi', function (Blueprint $table) {
            $table->string('telepon', 30)->change();
        });

        Schema::table('log_ref_lsi', function (Blueprint $table) {
            $table->string('telepon', 30)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_lsi', function (Blueprint $table) {
            $table->dropColumn('telepon', 15);
        });

        Schema::table('log_ref_lsi', function (Blueprint $table) {
            $table->dropColumn('telepon', 15);
        });
    }
}
