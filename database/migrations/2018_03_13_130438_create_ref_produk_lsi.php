<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefProdukLsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_produk_lsi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lsi_kode', 10)->unique()->unsigned();
            $table->string('produk_kode', 30)->unique()->unsigned();
            $table->integer('stok_gmd')->nullable();
            $table->decimal('curr_sale_prc', 10, 2)->nullable();
            $table->string('bblm_flag', 1)->nullable()->comment('1:ya; 0:tidak');
            $table->integer('con1')->nullable()->comment('1st Level Min Purchase');
            $table->decimal('dc1')->nullable()->comment('1st Level Disc Price');
            $table->integer('con2')->nullable()->comment('2nd Level Min Purchase');
            $table->decimal('dc2')->nullable()->comment('2nd Level Disc Price');
            $table->integer('con3')->nullable()->comment('3rd Level Min Purchase');
            $table->decimal('dc3')->nullable()->comment('3rd Level Disc Price');
            $table->integer('limit')->nullable()->comment('Quantity limit for BBLM');
            $table->integer('ea_qty')->nullable()->comment('Quantity of content');
            $table->integer('sale_tax_rt')->nullable()->comment('Tax');


            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('lsi_kode')->references('kode')->on('ref_lsi');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_produk_lsi');
    }
}
