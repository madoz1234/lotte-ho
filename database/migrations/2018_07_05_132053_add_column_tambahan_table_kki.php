<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTambahanTableKki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->decimal('iv_tambahan_satu_b', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_satu_c', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_dua_b', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_dua_c', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_tiga_b', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_tiga_c', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_empat_b', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_empat_c', 30, 2)->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->dropColumn('iv_tambahan_satu_b');
            $table->dropColumn('iv_tambahan_satu_c');
            $table->dropColumn('iv_tambahan_dua_b');
            $table->dropColumn('iv_tambahan_dua_c');
            $table->dropColumn('iv_tambahan_tiga_b');
            $table->dropColumn('iv_tambahan_tiga_c');
            $table->dropColumn('iv_tambahan_empat_b');
            $table->dropColumn('iv_tambahan_empat_c');
        });
    }
}
