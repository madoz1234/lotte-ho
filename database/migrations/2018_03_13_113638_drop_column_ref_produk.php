<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnRefProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk', function (Blueprint $table) {
            $table->dropColumn('nama');
            $table->dropColumn('barcode');
            $table->dropColumn('stok_gmd');
            $table->dropColumn('bblm_flag');
            $table->dropColumn('con1');
            $table->dropColumn('dc1');
            $table->dropColumn('con2');
            $table->dropColumn('dc2');
            $table->dropColumn('con3');
            $table->dropColumn('dc3');
            $table->dropColumn('limit');
            $table->dropColumn('ea_qty');
            $table->dropColumn('sale_tax_rt');
            $table->dropColumn('width');
            $table->dropColumn('length');
            $table->dropColumn('height');
            $table->dropColumn('wg');
            $table->dropColumn('inner_width');
            $table->dropColumn('inner_length');
            $table->dropColumn('inner_height');
            $table->dropColumn('inner_wg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk', function (Blueprint $table) {
            $table->string('nama');
            $table->string('barcode');
            $table->integer('stok_gmd');
            $table->string('bblm_flag');
            $table->integer('con1');
            $table->decimal('dc1');
            $table->integer('con2');
            $table->decimal('dc2');
            $table->integer('con3');
            $table->decimal('dc3');
            $table->integer('limit');
            $table->integer('ea_qty');
            $table->integer('sale_tax_rt');
            $table->float('width');
            $table->float('length');
            $table->float('height');
            $table->float('wg');
            $table->float('inner_width');
            $table->float('inner_length');
            $table->float('inner_height');
            $table->float('inner_wg');
        });
    }
}
