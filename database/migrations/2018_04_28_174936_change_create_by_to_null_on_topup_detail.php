<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCreateByToNullOnTopupDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_topup_escrow_detail', function (Blueprint $table) {
            $table->integer('created_by')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_topup_escrow_detail', function (Blueprint $table) {
            $table->integer('created_by')->unsigned()->change();
        });
    }
}
