<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPyrPembayaranNomorPyr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pyr_pembayaran', function (Blueprint $table) {
            $table->string('nomor_pyr', 50)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pyr_pembayaran', function (Blueprint $table) {
            $table->string('nomor_pyr', 30)->change();
        });
    }
}