<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHargaTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_harga_tmuk', function (Blueprint $table) {
            $table->increments('id');

            $table->string('tmuk_kode', 10)->unsigned();
            $table->string('produk_kode', 30)->unsigned();

            $table->decimal('cost_price', 10, 2)->nullable();
            $table->decimal('suggest_price', 10, 2)->nullable();
            $table->decimal('change_price', 10, 2)->nullable();
            $table->decimal('margin_amount', 10, 2)->nullable();
            $table->decimal('map', 10, 2)->nullable();
            $table->decimal('gmd_price', 10, 2)->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
