<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransRekapMemberTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rekap_member_tmuk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_member', 20)->nullable();
            $table->string('nama_member', 30)->nullable();
            $table->string('jenis_member', 30)->nullable();
            $table->text('alamat')->nullable();
            $table->integer('total_struk')->nullable();
            $table->decimal('total_pembelian',20,2)->nullable();
            $table->datetime('kunjungan_terakhir')->nullable();
            $table->decimal('rata_pembelian',20,2)->nullable();
            $table->integer('poin');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('trans_rekap_member_tmuk');
    }
}
