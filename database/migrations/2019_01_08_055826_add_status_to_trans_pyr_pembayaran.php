<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToTransPyrPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pyr_pembayaran', function (Blueprint $table) {
            $table->integer('statush2h')->default(0);
            $table->text('keterangan')->nullable();
            $table->string('nomorh2h', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pyr_pembayaran', function (Blueprint $table) {
            $table->dropColumn('statush2h');
            $table->dropColumn('keterangan');
            $table->dropColumn('nomorh2h');
        });
    }
}
