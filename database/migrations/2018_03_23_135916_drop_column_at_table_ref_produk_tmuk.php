<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnAtTableRefProdukTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk_tmuk', function($table) {
            $table->dropColumn('cost_price');
            $table->dropColumn('suggest_price');
            $table->dropColumn('change_price');
            $table->dropColumn('margin_amount');
            $table->dropColumn('map');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk_tmuk', function($table) {
            $table->decimal('cost_price', 10, 2)->nullable();
            $table->decimal('suggest_price', 10, 2)->nullable();
            $table->decimal('change_price', 10, 2)->nullable();
            $table->decimal('margin_amount', 10, 2)->nullable();
            $table->decimal('map', 10, 2)->nullable();
          });
    }
}
