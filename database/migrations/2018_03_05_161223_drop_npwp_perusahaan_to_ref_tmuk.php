<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropNpwpPerusahaanToRefTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_tmuk', function (Blueprint $table) {
            $table->dropColumn('npwp_perusahaan');
            $table->dropColumn('nama_perusahaan');
            $table->dropColumn('alamat_perusahaan');
            $table->dropColumn('nomor_rekening');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_tmuk', function (Blueprint $table) {
            $table->integer('npwp_perusahaan');
            $table->integer('nama_perusahaan');
            $table->integer('alamat_perusahaan');
            $table->integer('nomor_rekening');
        });
    }
}
