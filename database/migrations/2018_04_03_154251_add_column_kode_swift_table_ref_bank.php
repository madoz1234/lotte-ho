<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnKodeSwiftTableRefBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_bank', function (Blueprint $table) {
            $table->string('kode_swift', 8)->nullable()->after('nama');
        });

        Schema::table('log_ref_bank', function (Blueprint $table) {
            $table->string('kode_swift', 8)->nullable()->after('nama');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_bank', function (Blueprint $table) {
            $table->dropColumn('kode_swift');
        });

        Schema::table('log_ref_bank', function (Blueprint $table) {
            $table->dropColumn('kode_swift');
        });
    }
}
