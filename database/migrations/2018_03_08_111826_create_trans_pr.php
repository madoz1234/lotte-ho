<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('trans_pr', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_pr', 30)->unique();
            $table->string('lsi_kode', 10)->unsigned();
            $table->string('tmuk_kode', 10)->unsigned();
            $table->date('tgl_kirim');
            $table->decimal('total', 20,2);
            $table->text('keterangan')->nullable();


            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();


            $table->foreign('lsi_kode')->references('kode')->on('ref_lsi');
            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_pr');
    }
}
