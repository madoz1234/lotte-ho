<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRefTahunFiskal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ref_tahun_fiskal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tahun_fiskal_id')->unsigned();
            $table->date('tgl_awal');
            $table->date('tgl_akhir');
            $table->integer('status')->comment('1:Berjalan; 0:Selesai');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_ref_tahun_fiskal');
    }
}
