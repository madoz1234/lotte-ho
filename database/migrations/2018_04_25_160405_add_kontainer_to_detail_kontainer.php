<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKontainerToDetailKontainer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('trans_kontainer_detail', function (Blueprint $table) {
            $table->integer('no_kontainer')->after('produk_kode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_kontainer_detail', function (Blueprint $table) {
            $table->dropColumn('no_kontainer');
        });
    }
}
