<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBilaAdaBiayaTambahanTableKki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->decimal('i_bila_ada_biaya_tambahan', 30, 2)->default(0)->nullable();
            $table->decimal('i_perkiraan_stok_ideal', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_satu', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_dua', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_tiga', 30, 2)->default(0)->nullable();
            $table->decimal('iv_tambahan_empat', 30, 2)->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->dropColumn('i_bila_ada_biaya_tambahan');
            $table->dropColumn('i_perkiraan_stok_ideal');

            $table->dropColumn('iv_tambahan_satu');
            $table->dropColumn('iv_tambahan_dua');
            $table->dropColumn('iv_tambahan_tiga');
            $table->dropColumn('iv_tambahan_empat');
        });
    }
}
