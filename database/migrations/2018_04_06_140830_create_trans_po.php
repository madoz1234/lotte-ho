<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_po', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nomor_po', 30)->unique();
            $table->string('nomor_pr', 30)->unique();
            $table->string('tmuk_kode', 10);
            $table->date('tgl_buat');
            $table->decimal('total', 20, 2);
            $table->text('keterangan')->nullable();
            $table->integer('status')->default(0)->nullable();
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_po');
    }
}
