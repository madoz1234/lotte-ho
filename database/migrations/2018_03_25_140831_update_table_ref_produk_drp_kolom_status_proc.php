<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableRefProdukDrpKolomStatusProc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk', function ($table) {
            $table->dropColumn('status_prod');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk', function ($table) {
            $table->string('status_prod', 30)->nullable();
        });
    }
}
