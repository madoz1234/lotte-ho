<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefMembercard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_membercard', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor', 20);
            $table->string('nama', 30);
            $table->string('telepon', 15);
            $table->string('email', 30);
            $table->integer('jeniskustomer_id')->unsigned();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('jeniskustomer_id')->references('id')->on('ref_jeniskustomer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_membercard');
    }
}
