<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPyrPembayaranApproval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pyr_pembayaran', function (Blueprint $table) {
            $table->dateTime('verifikasi1_date')->nullable();
            $table->integer('verifikasi1_user')->unsigned()->nullable();
            $table->dateTime('verifikasi2_date')->nullable();
            $table->integer('verifikasi2_user')->unsigned()->nullable();
            $table->dateTime('verifikasi3_date')->nullable();
            $table->integer('verifikasi3_user')->unsigned()->nullable();
            $table->dateTime('verifikasi4_date')->nullable();
            $table->integer('verifikasi4_user')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pyr_pembayaran', function (Blueprint $table) {
            $table->dropColumn('verifikasi1_date');
            $table->dropColumn('verifikasi1_user');
            $table->dropColumn('verifikasi2_date');
            $table->dropColumn('verifikasi2_user');
            $table->dropColumn('verifikasi3_date');
            $table->dropColumn('verifikasi3_user');
            $table->dropColumn('verifikasi4_date');
            $table->dropColumn('verifikasi4_user');
        });
    }
}
