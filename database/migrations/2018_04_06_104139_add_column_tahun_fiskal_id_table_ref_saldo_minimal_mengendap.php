<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTahunFiskalIdTableRefSaldoMinimalMengendap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_saldo_minimal_mengendap', function (Blueprint $table) {
            $table->integer('tahun_fiskal_id')->unsigned()->after('saldo_minimal_mengendap');

            $table->foreign('tahun_fiskal_id')->references('id')->on('ref_tahun_fiskal');
        });

        Schema::table('log_ref_saldo_minimal_mengendap', function (Blueprint $table) {
            $table->integer('tahun_fiskal_id')->unsigned()->after('saldo_minimal_mengendap_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_saldo_minimal_mengendap', function (Blueprint $table) {
            $table->dropColumn('tahun_fiskal_id');
        });

        Schema::table('log_ref_saldo_minimal_mengendap', function (Blueprint $table) {
            $table->dropColumn('tahun_fiskal_id');
        });
    }
}
