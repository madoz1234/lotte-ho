<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitAdditionalTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE TABLE "public"."harga_gmd" (
            "lsi_code" character varying(2044) NOT NULL,
            "stock_id" character varying(2044) NOT NULL,
            "harga" double precision DEFAULT 0,
            CONSTRAINT "harga_gmd_pkey" PRIMARY KEY ("lsi_code", "stock_id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."t_po" (
            "po_no_immbo" integer NOT NULL,
            "po_no_ref" character varying(255),
            "reference" text NOT NULL,
            "supplier_id" integer DEFAULT 0 NOT NULL,
            "order_date" date,
            "delivery_date" date,
            "delivery_address" text NOT NULL,
            "comments" text,
            "pr_no_immpos" character varying(128),
            "pr_no_immbo" character varying(128),
            "gr_no_immpos" character varying(128),
            "total_pr" double precision DEFAULT 0 NOT NULL,
            "total_po" double precision DEFAULT 0 NOT NULL,
            "tax_included" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "t_po_pkey" PRIMARY KEY ("po_no_immbo")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."t_so" (
            "so_id" character varying(25) NOT NULL,
            "kode_verifikasi" character varying(255),
            "user_id" character varying(100),
            "so_date" date,
            "status" character varying(50),
            "so_type" character varying(25),
            "tmuk" character varying(255) NOT NULL,
            "u_gl" integer,
            CONSTRAINT "t_so_pkey" PRIMARY KEY ("so_id", "tmuk")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE t_so_detail_so_detail_item_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 99313 CACHE 1');

        DB::statement('CREATE TABLE "public"."t_so_detail" (
            "so_detail_item" integer DEFAULT nextval(\'t_so_detail_so_detail_item_seq\') NOT NULL,
            "so_id" character varying(25) NOT NULL,
            "item_code" character varying(20) DEFAULT \'\' NOT NULL,
            "description" text,
            "qty_tersedia" double precision DEFAULT (0) NOT NULL,
            "qty_barcode" double precision DEFAULT (0) NOT NULL,
            "unit_item" character varying(20) DEFAULT \'\' NOT NULL,
            "unit_price" double precision DEFAULT (0) NOT NULL,
            "total_price" double precision DEFAULT (0) NOT NULL,
            "flag_so_ok" boolean DEFAULT false,
            "barcode_code" character varying(32),
            CONSTRAINT "t_so_detail_pkey" PRIMARY KEY ("so_detail_item")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."temp_cat1" (
            "min" integer,
            "bumun_cd" character varying(255),
            "bumun_nm" character varying(255),
            "l1_cd" character varying(255),
            "l1_nm" character varying(255)
        ) WITH (oids = false)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE IF EXISTS "harga_gmd"');
        DB::statement('DROP TABLE IF EXISTS "t_po"');
        DB::statement('DROP TABLE IF EXISTS "t_so"');
        DB::statement('DROP TABLE IF EXISTS "t_so_detail"');
        DB::statement('DROP TABLE IF EXISTS "temp_cat1"');
        DB::statement('DROP SEQUENCE IF EXISTS "t_so_detail_so_detail_item_seq"');
    }
}
