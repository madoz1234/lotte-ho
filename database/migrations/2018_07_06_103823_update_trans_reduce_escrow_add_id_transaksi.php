<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransReduceEscrowAddIdTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->string('id_transaksi', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->dropColumn('id_transaksi');
        });
    }
}
