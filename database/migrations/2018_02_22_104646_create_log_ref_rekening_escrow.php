<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRefRekeningEscrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ref_rekening_escrow', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_escrow_id')->unsigned();
            $table->integer('rekening_escrow_id')->unsigned();
            $table->string('nama_pemilik', 50);
            $table->string('nomor_rekening',20);
            $table->integer('status')->comment('1:Aktif;2:Non-Aktif');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_ref_rekening_escrow');
    }
}
