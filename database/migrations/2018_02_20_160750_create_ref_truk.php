<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefTruk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_truk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipe', 30);
            $table->decimal('tinggi', 5,2);
            $table->decimal('panjang', 5,2);
            $table->decimal('lebar', 5,2);
            $table->decimal('kapasitas', 5,2);
            $table->decimal('volume', 5,2);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_truk');
    }
}
