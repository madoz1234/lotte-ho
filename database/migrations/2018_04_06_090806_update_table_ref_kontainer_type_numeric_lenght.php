<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableRefKontainerTypeNumericLenght extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kontainer', function (Blueprint $table) {
            $table->decimal('tinggi_luar', 20,2)->change();
            $table->decimal('tinggi_dalam', 20,2)->change();
            $table->decimal('panjang_atas', 20,2)->change();
            $table->decimal('panjang_bawah', 20,2)->change();
            $table->decimal('lebar_atas', 20,2)->change();
            $table->decimal('lebar_bawah', 20,2)->change();
            $table->decimal('volume', 20,2)->change();
        });

        Schema::table('log_ref_kontainer', function (Blueprint $table) {
            $table->decimal('tinggi_luar', 20,2)->change();
            $table->decimal('tinggi_dalam', 20,2)->change();
            $table->decimal('panjang_atas', 20,2)->change();
            $table->decimal('panjang_bawah', 20,2)->change();
            $table->decimal('lebar_atas', 20,2)->change();
            $table->decimal('lebar_bawah', 20,2)->change();
            $table->decimal('volume', 20,2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kontainer', function (Blueprint $table) {
            $table->dropColumn('tinggi_luar', 5,2);
            $table->dropColumn('tinggi_dalam', 5,2);
            $table->dropColumn('panjang_atas', 5,2);
            $table->dropColumn('panjang_bawah', 5,2);
            $table->dropColumn('lebar_atas', 5,2);
            $table->dropColumn('lebar_bawah', 5,2);
            $table->dropColumn('volume', 5,2);
        });

        Schema::table('log_ref_kontainer', function (Blueprint $table) {
            $table->dropColumn('tinggi_luar', 5,2);
            $table->dropColumn('tinggi_dalam', 5,2);
            $table->dropColumn('panjang_atas', 5,2);
            $table->dropColumn('panjang_bawah', 5,2);
            $table->dropColumn('lebar_atas', 5,2);
            $table->dropColumn('lebar_bawah', 5,2);
            $table->dropColumn('volume', 5,2);
        });
    }
}
