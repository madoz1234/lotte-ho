<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKontainerDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_kontainer_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_trans_kontainer')->unsigned();
            $table->integer('id_kontainer')->unsigned();
            $table->string('produk_kode', 30);
            

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_trans_kontainer')->references('id')->on('trans_kontainer');
            $table->foreign('id_kontainer')->references('id')->on('ref_kontainer');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');

        });
        
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_kontainer_detail');
    }
}
