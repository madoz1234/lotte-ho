<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProdukDeskripsiToTableRefProdukSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk_setting', function (Blueprint $table) {
            $table->string('uom1_produk_description',30)->nullable();
            $table->string('uom2_produk_description',30)->nullable();
            $table->string('uom3_produk_description',30)->nullable();
            $table->string('uom4_produk_description',30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk_setting', function (Blueprint $table) {
            $table->dropColumn('uom1_produk_description');
            $table->dropColumn('uom2_produk_description');
            $table->dropColumn('uom3_produk_description');
            $table->dropColumn('uom4_produk_description');
        });
    }
}