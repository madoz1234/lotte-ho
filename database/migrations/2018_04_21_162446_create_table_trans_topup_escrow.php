<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransTopupEscrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_topup_escrow', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tmuk_kode', 10);
            $table->date('tanggal');
            $table->integer('jenis');
            $table->decimal('jumlah',20,2);
            $table->string('lampiran',100)->nullable();
            $table->integer('status');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_topup_escrow');
    }
}
