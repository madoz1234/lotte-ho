<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateKodeToRefTipebarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_tipebarang', function (Blueprint $table) {
            $table->string('kode')->unique()->nullable()->change();
        });
        Schema::table('log_ref_tipebarang', function (Blueprint $table) {
            $table->string('kode')->unique()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_tipebarang', function (Blueprint $table) {
            Schema::dropColumn('kode');
        });
        Schema::table('log_ref_tipebarang', function (Blueprint $table) {
            Schema::dropColumn('kode');
        });
    }
}
