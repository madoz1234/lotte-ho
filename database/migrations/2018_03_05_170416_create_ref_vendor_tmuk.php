<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefVendorTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_vendor_tmuk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kota_id')->unsigned();
            $table->integer('kecamatan_id')->unsigned();
            $table->integer('rekening_escrow_id')->unsigned();
            $table->integer('bank_escrow_id')->unsigned();
            $table->integer('pajak_id')->unsigned();
            $table->string('kode', 10);
            $table->string('nama', 50);
            $table->text('alamat');
            $table->string('kode_pos', 5);
            $table->string('telepon', 15);
            $table->string('email', 50)->nullable();
            $table->string('top', 3)->nullable();
            $table->string('lead_time', 3)->nullable();
            $table->string('order_senin', 3)->nullable();
            $table->string('order_selasa', 3)->nullable();
            $table->string('order_rabu', 3)->nullable();
            $table->string('order_kamis', 3)->nullable();
            $table->string('order_jumat', 3)->nullable();
            $table->string('order_sabtu', 3)->nullable();
            $table->string('order_minggu', 3)->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('kota_id')->references('id')->on('ref_kota');
            $table->foreign('kecamatan_id')->references('id')->on('ref_kecamatan');
            $table->foreign('pajak_id')->references('id')->on('ref_pajak');
            $table->foreign('rekening_escrow_id')->references('id')->on('ref_rekening_escrow');
            $table->foreign('bank_escrow_id')->references('id')->on('ref_bank_escrow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_vendor_tmuk');
    }
}
