<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefJenisAssortment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_jenis_assortment', function (Blueprint $table) {
            $table->string('nama', 10)->unique()->change();
        });

        Schema::table('log_ref_jenis_assortment', function (Blueprint $table) {
            $table->string('nama', 10)->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_jenis_assortment', function (Blueprint $table) {
            $table->dropColumn('nama');
        });

        Schema::table('log_ref_jenis_assortment', function (Blueprint $table) {
            $table->dropColumn('nama');
        });
    }
}
