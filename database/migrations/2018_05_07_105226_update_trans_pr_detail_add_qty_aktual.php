<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPrDetailAddQtyAktual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pr_detail', function (Blueprint $table) {
            $table->integer('qty_aktual')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pr_detail', function (Blueprint $table) {
            $table->dropColumn('qty_aktual');
        });
    }
}
