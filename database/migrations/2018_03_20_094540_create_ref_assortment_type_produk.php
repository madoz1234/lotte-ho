<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefAssortmentTypeProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_assortment_type_produk', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('assortment_type_id')->unsigned();
            $table->string('produk_kode', 30)->unsigned();
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('assortment_type_id')->references('id')->on('ref_jenis_assortment');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_assortment_type_produk');
    }
}
