<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransSoDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_so_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trans_so_id')->unsigned();
            $table->string('produk_kode', 30);
            $table->integer('qty_barcode')->nullable();
            $table->integer('qty_system')->nullable();
            $table->decimal('hpp', 10, 2)->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
            $table->foreign('trans_so_id')->references('id')->on('trans_so');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_so_detail');
    }
}
