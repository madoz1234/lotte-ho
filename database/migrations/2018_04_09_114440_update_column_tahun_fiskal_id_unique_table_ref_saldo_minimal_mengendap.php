<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnTahunFiskalIdUniqueTableRefSaldoMinimalMengendap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_saldo_minimal_mengendap', function (Blueprint $table) {
            $table->integer('tahun_fiskal_id')->unsigned()->unique()->after('saldo_minimal_mengendap')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_saldo_minimal_mengendap', function (Blueprint $table) {
            $table->dropColumn('tahun_fiskal_id');
        });
    }
}
