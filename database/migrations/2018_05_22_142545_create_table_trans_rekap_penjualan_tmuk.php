<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransRekapPenjualanTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rekap_penjualan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jual_kode', 25)->unique()->nullable();
            $table->string('tmuk_kode',10)->unsigned()->nullable();
            $table->string('member_nomor', 20)->unsigned()->nullable();
            $table->string('kasir_id')->nullable();
            $table->integer('kassa_id')->nullable();
            $table->integer('shift_id')->nullable();
            $table->string('reference_id', 50)->nullable();
            $table->timestamp('tanggal')->nullable();
            $table->decimal('subtotal', 30,2);
            $table->decimal('tax', 30,2)->nullable();
            $table->decimal('discount', 30,2)->nullable();
            $table->decimal('total', 30,2)->nullable();
            $table->decimal('kembalian', 30,2)->nullable();
            $table->text('note')->nullable();
            $table->string('flag_sum', 1)->nullable();
            $table->string('flag_delete', 1)->nullable();
            $table->string('jam', 50)->nullable();
            $table->string('flag_sync', 1)->nullable();

            $table->foreign('member_nomor')->references('nomor')->on('ref_membercard');
            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
        });

        Schema::create('trans_rekap_penjualan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rekap_penjualan_kode', 25)->unsigned()->nullable();
            $table->string('item_code', 20)->nullable();
            $table->string('barcode', 255)->nullable();
            $table->text('description')->nullable();
            $table->decimal('harga', 30,2)->nullable();
            $table->decimal('harga_member', 30,2)->nullable();
            $table->integer('qty')->nullable();
            $table->string('satuan', 20)->nullable();
            $table->decimal('discount', 30,2)->nullable();
            $table->decimal('disc_percent', 30,2)->nullable();
            $table->decimal('disc_amount', 30,2)->nullable();
            $table->decimal('total', 30,2)->nullable();
            $table->string('promo', 25)->nullable();
            $table->string('tax_type', 25)->nullable();
            $table->decimal('harga_hpp3', 30,2)->nullable();

            $table->foreign('rekap_penjualan_kode')->references('jual_kode')->on('trans_rekap_penjualan');
        });

        Schema::create('trans_rekap_penjualan_bayar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rekap_penjualan_kode', 25)->unsigned()->nullable();
            $table->string('tipe', 20)->nullable();
            $table->string('bank_id', 25)->nullable();
            $table->text('bank_name')->nullable();
            $table->decimal('total', 30,2)->nullable();
            $table->text('no_kartu')->nullable();

            $table->foreign('rekap_penjualan_kode')->references('jual_kode')->on('trans_rekap_penjualan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //ini ditambahin biar pass rollback gk error kurang satu ini
        Schema::drop('trans_rekap_penjualan_bayar');
        //--------------------------------------------
        
        Schema::drop('trans_rekap_penjualan_detail');
        Schema::drop('trans_rekap_penjualan');

    }
}
