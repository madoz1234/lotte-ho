<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnVendorLokalTableTransDirectInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_direct_invoice', function (Blueprint $table) {
            $table->integer('vendor_lokal_id')->unsigned()->nullable();

            $table->foreign('vendor_lokal_id')->references('id')->on('ref_vendor_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_direct_invoice', function (Blueprint $table) {
            $table->dropColumn('vendor_lokal_id');
        });
    }
}
