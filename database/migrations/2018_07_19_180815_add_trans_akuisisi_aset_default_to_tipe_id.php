<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransAkuisisiAsetDefaultToTipeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_akuisisi_aset', function (Blueprint $table) {
            $table->integer('tipe_id')->default(0)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_akuisisi_aset', function (Blueprint $table) {
            $table->integer('tipe_id')->change();
        });
    }
}
