<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTmukKodeTableRefMembercard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_membercard', function (Blueprint $table) {
            $table->string('tmuk_kode', 10)->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
        });

        Schema::table('log_ref_membercard', function (Blueprint $table) {
            $table->string('tmuk_kode', 10)->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_membercard', function (Blueprint $table) {
            $table->dropColumn('tmuk_kode');
        });

        Schema::table('log_ref_membercard', function (Blueprint $table) {
            $table->dropColumn('tmuk_kode');
        });
    }
}
