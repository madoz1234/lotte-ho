<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefProdukSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_produk_setting', function (Blueprint $table) {
            $table->increments('id');

            $table->string('produk_kode', 30)->unsigned();
            $table->string('tipe_barang_kode', 30)->unsigned()->nullable();
            $table->string('jenis_barang_kode', 30)->unsigned()->nullable();
            $table->string('locked_price', 1)->nullable();
            $table->float('margin', 1)->nullable();
            $table->string('uom1_satuan', 15)->nullable();
            $table->tinyInteger('uom1_selling_cek')->nullable();
            $table->tinyInteger('uom1_order_cek')->nullable();
            $table->tinyInteger('uom1_receiving_cek')->nullable();
            $table->tinyInteger('uom1_return_cek')->nullable();
            $table->tinyInteger('uom1_inventory_cek')->nullable();
            $table->integer('uom1_rak_id')->unsigned()->nullable();
            $table->string('uom1_barcode', 30)->nullable();
            $table->integer('uom1_conversion')->nullable();
            $table->decimal('uom1_width', 10,2)->nullable();
            $table->decimal('uom1_height', 10,2)->nullable();
            $table->decimal('uom1_length', 10,2)->nullable();
            $table->decimal('uom1_weight', 10,2)->nullable();
            $table->string('uom1_boxtype', 15)->nullable();
            $table->string('uom1_file', 100)->nullable();


            $table->string('uom2_satuan', 15)->nullable();
            $table->tinyInteger('uom2_selling_cek')->nullable();
            $table->tinyInteger('uom2_order_cek')->nullable();
            $table->tinyInteger('uom2_receiving_cek')->nullable();
            $table->tinyInteger('uom2_return_cek')->nullable();
            $table->tinyInteger('uom2_inventory_cek')->nullable();
            $table->integer('uom2_rak_id')->unsigned()->nullable();
            $table->string('uom2_barcode', 30)->nullable();
            $table->integer('uom2_conversion')->nullable();
            $table->decimal('uom2_width', 10,2)->nullable();
            $table->decimal('uom2_height', 10,2)->nullable();
            $table->decimal('uom2_length', 10,2)->nullable();
            $table->decimal('uom2_weight', 10,2)->nullable();
            $table->string('uom2_boxtype', 15)->nullable();
            $table->string('uom2_file', 100)->nullable();


            $table->string('uom3_satuan', 15)->nullable();
            $table->tinyInteger('uom3_selling_cek')->nullable();
            $table->tinyInteger('uom3_order_cek')->nullable();
            $table->tinyInteger('uom3_receiving_cek')->nullable();
            $table->tinyInteger('uom3_return_cek')->nullable();
            $table->tinyInteger('uom3_inventory_cek')->nullable();
            $table->integer('uom3_rak_id')->unsigned()->nullable();
            $table->string('uom3_barcode', 30)->nullable();
            $table->integer('uom3_conversion')->nullable();
            $table->decimal('uom3_width', 10,2)->nullable();
            $table->decimal('uom3_height', 10,2)->nullable();
            $table->decimal('uom3_length', 10,2)->nullable();
            $table->decimal('uom3_weight', 10,2)->nullable();
            $table->string('uom3_boxtype', 15)->nullable();
            $table->string('uom3_file', 100)->nullable();


            $table->string('uom4_satuan', 15)->nullable();
            $table->tinyInteger('uom4_selling_cek')->nullable();
            $table->tinyInteger('uom4_order_cek')->nullable();
            $table->tinyInteger('uom4_receiving_cek')->nullable();
            $table->tinyInteger('uom4_return_cek')->nullable();
            $table->tinyInteger('uom4_inventory_cek')->nullable();
            $table->integer('uom4_rak_id')->unsigned()->nullable();
            $table->string('uom4_barcode', 30)->nullable();
            $table->integer('uom4_conversion')->nullable();
            $table->decimal('uom4_width', 10,2)->nullable();
            $table->decimal('uom4_height', 10,2)->nullable();
            $table->decimal('uom4_length', 10,2)->nullable();
            $table->decimal('uom4_weight', 10,2)->nullable();
            $table->string('uom4_boxtype', 15)->nullable();
            $table->string('uom4_file', 100)->nullable();
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
            $table->foreign('tipe_barang_kode')->references('kode')->on('ref_tipebarang');
            $table->foreign('jenis_barang_kode')->references('kode')->on('ref_jenis_barang');
            $table->foreign('uom1_rak_id')->references('id')->on('ref_rak');
            $table->foreign('uom2_rak_id')->references('id')->on('ref_rak');
            $table->foreign('uom3_rak_id')->references('id')->on('ref_rak');
            $table->foreign('uom4_rak_id')->references('id')->on('ref_rak');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_produk_setting');
    }
}
