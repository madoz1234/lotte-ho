<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPrDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pr_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pr_id')->unsigned();
            $table->string('produk_kode', 30)->unsigned();
            $table->string('deskrisi', 100);
            $table->integer('qty_order');
            $table->string('unit_order', 10);
            $table->decimal('unit_order_price', 10,2);
            $table->integer('qty_sell');
            $table->string('unit_sell', 10);
            $table->integer('qty_pr');
            $table->string('unit_pr', 10);
            $table->decimal('total_price', 10,2);



            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('pr_id')->references('id')->on('trans_pr');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_pr_detail');
    }
}