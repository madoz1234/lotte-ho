<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupirOnSuratJalan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_surat_jalan', function (Blueprint $table) {
            $table->string('nomor_kendaraan')->nullable();
            $table->string('supir')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_surat_jalan', function (Blueprint $table) {
            $table->dropColumn('nomor_kendaraan');
            $table->dropColumn('supir');
        });
    }
}
