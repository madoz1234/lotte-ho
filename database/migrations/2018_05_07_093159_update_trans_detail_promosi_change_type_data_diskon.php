<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransDetailPromosiChangeTypeDataDiskon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_detail_promosi_buy_x_get_y', function (Blueprint $table) {
            $table->decimal('diskon_x', 10, 2)->change();
            $table->decimal('diskon_y', 10, 2)->change();
        });

        Schema::table('ref_detail_promosi_diskon', function (Blueprint $table) {
            $table->decimal('diskon', 10, 2)->change();
        });

        Schema::table('ref_detail_promosi_promo', function (Blueprint $table) {
            $table->decimal('diskon', 10, 2)->change();
        });

        Schema::table('ref_detail_promosi_purchase', function (Blueprint $table) {
            $table->decimal('diskon', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_detail_promosi_buy_x_get_y', function (Blueprint $table) {
            $table->float('diskon_x')->change();
            $table->float('diskon_y')->change();
        });

        Schema::table('ref_detail_promosi_diskon', function (Blueprint $table) {
            $table->float('diskon')->change();
        });

        Schema::table('ref_detail_promosi_promo', function (Blueprint $table) {
            $table->float('diskon')->change();
        });

        Schema::table('ref_detail_promosi_purchase', function (Blueprint $table) {
            $table->float('diskon')->change();
        });
    }
}
