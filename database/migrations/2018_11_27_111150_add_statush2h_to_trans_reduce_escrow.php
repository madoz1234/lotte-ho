<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatush2hToTransReduceEscrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->integer('statush2h')->default(0);
            $table->text('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->dropColumn('statush2h');
            $table->dropColumn('keterangan');
        });
    }
}
