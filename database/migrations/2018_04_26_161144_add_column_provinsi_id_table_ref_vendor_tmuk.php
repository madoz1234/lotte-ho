<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnProvinsiIdTableRefVendorTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->integer('provinsi_id')->unsigned()->nullable();

            $table->foreign('provinsi_id')->references('id')->on('ref_provinsi');
        });

        Schema::table('log_ref_vendor_tmuk', function (Blueprint $table) {
            $table->integer('provinsi_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->dropColumn('provinsi_id');
        });

        Schema::table('log_ref_vendor_tmuk', function (Blueprint $table) {
            $table->dropColumn('provinsi_id');
        });
    }
}
