<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJumlahOnTransKontainerDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('trans_kontainer_detail', function (Blueprint $table) {
            $table->integer('jumlah_produk')->nullable()->after('produk_kode');
            $table->integer('id_kontainer')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_kontainer_detail', function (Blueprint $table) {
            $table->dropColumn('jumlah_produk');
            $table->integer('id_kontainer')->unsigned()->change();
        });
    }
}
