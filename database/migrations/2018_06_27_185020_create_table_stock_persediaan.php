<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStockPersediaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tmuk_kode', 10)->unsigned();
            $table->string('produk_kode', 30);
            $table->date('tgl_gr');
            $table->string('no_gr', 30);
            $table->integer('qty');
            $table->integer('sisa');
            $table->integer('total');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_stock');
    }
}
