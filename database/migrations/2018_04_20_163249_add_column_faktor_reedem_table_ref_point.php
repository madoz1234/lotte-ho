<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFaktorReedemTableRefPoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_point', function (Blueprint $table) {
            $table->decimal('faktor_reedem', 15, 2)->nullable();
        });

        Schema::table('log_ref_point', function (Blueprint $table) {
            $table->decimal('faktor_reedem', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_point', function (Blueprint $table) {
            $table->dropColumn('faktor_reedem');
        });

        Schema::table('log_ref_point', function (Blueprint $table) {
            $table->dropColumn('faktor_reedem');
        });
    }
}
