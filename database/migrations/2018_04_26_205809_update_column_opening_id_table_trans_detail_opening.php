<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnOpeningIdTableTransDetailOpening extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_detail_opening', function (Blueprint $table) {
            $table->dropForeign(['opening_id']);
        });

        Schema::table('trans_detail_opening', function (Blueprint $table) {
            $table->foreign('opening_id')->references('id')->on('trans_opening')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_detail_opening', function (Blueprint $table) {
            $table->dropForeign(['opening_id']);
        });
        
        Schema::table('trans_detail_opening', function (Blueprint $table) {
            $table->foreign('opening_id')->references('id')->on('trans_opening');
        });
    }
}
