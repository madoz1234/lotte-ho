<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHargaTransReturDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_retur_detail', function (Blueprint $table) {
            $table->decimal('harga_satuan', 30,2);
            $table->decimal('harga_total', 30,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_retur_detail', function (Blueprint $table) {
            $table->dropColumn('harga_satuan');
            $table->dropColumn('harga_total');
        });
    }
}