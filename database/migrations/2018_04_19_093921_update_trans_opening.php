<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransOpening extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_opening', function (Blueprint $table) {
            $table->date('tgl_kirim')->after('tgl_buat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_opening', function (Blueprint $table) {
            $table->dropColumn('tgl_kirim');
        });
    }
}
