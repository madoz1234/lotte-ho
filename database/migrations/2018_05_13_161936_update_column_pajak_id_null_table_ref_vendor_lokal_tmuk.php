<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnPajakIdNullTableRefVendorLokalTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->integer('pajak_id')->unsigned()->nullable()->change();
        });

        Schema::table('log_ref_vendor_tmuk', function (Blueprint $table) {
            $table->integer('pajak_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->dropColumn('pajak_id');
        });

        Schema::table('log_ref_vendor_tmuk', function (Blueprint $table) {
            $table->dropColumn('pajak_id');
        });
    }
}
