<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableRefTrukTypeNumericLenght extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_truk', function (Blueprint $table) {
            $table->decimal('tinggi', 20,2)->change();
            $table->decimal('panjang', 20,2)->change();
            $table->decimal('lebar', 20,2)->change();
            $table->decimal('kapasitas', 20,2)->change();
            $table->decimal('volume', 20,2)->change();
        });

        Schema::table('log_ref_truk', function (Blueprint $table) {
            $table->decimal('tinggi', 20,2)->change();
            $table->decimal('panjang', 20,2)->change();
            $table->decimal('lebar', 20,2)->change();
            $table->decimal('kapasitas', 20,2)->change();
            $table->decimal('volume', 20,2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_truk', function (Blueprint $table) {
            $table->dropColumn('tinggi', 5,2);
            $table->dropColumn('panjang', 5,2);
            $table->dropColumn('lebar', 5,2);
            $table->dropColumn('kapasitas', 5,2);
            $table->dropColumn('volume', 5,2);
        });

        Schema::table('log_ref_truk', function (Blueprint $table) {
            $table->dropColumn('tinggi', 5,2);
            $table->dropColumn('panjang', 5,2);
            $table->dropColumn('lebar', 5,2);
            $table->dropColumn('kapasitas', 5,2);
            $table->dropColumn('volume', 5,2);
        });
    }
}
