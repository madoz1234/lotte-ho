<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableRefTipebarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_tipebarang', function (Blueprint $table) {
            $table->dropUnique('log_ref_tipebarang_kode_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_tipebarang', function (Blueprint $table) {
            $table->dropColumn('kode');
        });
    }
}
