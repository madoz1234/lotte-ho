<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRefRak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ref_rak', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rak_id')->unsigned();
            $table->string('tipe_rak', 30);
            $table->integer('shelving');
            $table->decimal('tinggi_rak', 10, 2);
            $table->decimal('panjang_rak', 10, 2);
            $table->decimal('lebar_rak', 10, 2);
            $table->integer('hanger')->comment('1:ya;0:tidak');
            $table->decimal('tinggi_hanger', 10, 2);
            $table->decimal('panjang_hanger', 10, 2);
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_ref_rak');
    }
}
