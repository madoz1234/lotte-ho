<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMemberOnTopupDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_topup_escrow_detail', function (Blueprint $table) {
            $table->string('member')->nullable()->after('week');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_topup_escrow_detail', function (Blueprint $table) {
            $table->dropColumn('member');
        });
    }
}
