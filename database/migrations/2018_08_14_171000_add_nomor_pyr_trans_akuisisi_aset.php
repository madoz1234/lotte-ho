<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNomorPyrTransAkuisisiAset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_akuisisi_aset', function (Blueprint $table) {
            $table->string('nomor_pyr', 50)->nullable();
            $table->string('produk_kode', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_akuisisi_aset', function (Blueprint $table) {
            $table->dropColumn('nomor_pyr');
            $table->dropColumn('produk_kode');
        });
    }
}