<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersediaanRr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_persediaan_rr_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_tmuk_kode')->unsigned();
            $table->integer('qty')->nullable();
            $table->string('date')->nullable();
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->foreign('produk_tmuk_kode')->references('id')->on('ref_produk_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_persediaan_rr_detail');
    }
}
