<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePyrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pyr', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nomor_pyr', 30)->unique();
            $table->string('tmuk_kode', 10);
            $table->integer('vendor_lokal')->unsigned();
            $table->date('tgl_buat');
            $table->date('tgl_jatuh_tempo');
            $table->decimal('total', 10, 2)->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
            $table->foreign('vendor_lokal')->references('id')->on('ref_vendor_tmuk');

        });

        Schema::create('trans_pyr_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pyr_id')->unsigned();
            $table->string('produk_kode', 30)->unsigned();
            $table->integer('qty')->default(0);
            $table->string('unit', 10)->default(0);
            $table->decimal('price', 10, 2)->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('pyr_id')->references('id')->on('trans_pyr');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_pyr_detail');
        Schema::drop('trans_pyr');
    }
}
