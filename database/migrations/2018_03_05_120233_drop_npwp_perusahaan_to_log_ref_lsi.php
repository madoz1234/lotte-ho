<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropNpwpPerusahaanToLogRefLsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_lsi', function (Blueprint $table) {
            $table->dropColumn('npwp_perusahaan');
            $table->dropColumn('nama_perusahaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_lsi', function (Blueprint $table) {
            $table->integer('npwp_perusahaan');
            $table->integer('nama_perusahaan');
        });
    }
}
