<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefProdukSettingAddTipeAssetId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk_setting', function (Blueprint $table) {
            $table->integer('tipe_asset_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk_setting', function (Blueprint $table) {
            $table->dropColumn('tipe_asset_id');
        });
    }
}
