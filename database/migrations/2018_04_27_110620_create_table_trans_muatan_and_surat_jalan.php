<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransMuatanAndSuratJalan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_muatan', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nomor',30)->unique();
            $table->integer('status');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_muatan_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nomor_muatan',30);
            $table->string('po_nomor', 30);
            $table->string('tmuk_kode', 10);
            $table->integer('status');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('nomor_muatan')->references('nomor')->on('trans_muatan');
            $table->foreign('po_nomor')->references('nomor')->on('trans_po');
            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');

        });

        Schema::create('trans_surat_jalan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor', 30);
            $table->string('nomor_muatan', 30);
    
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('nomor_muatan')->references('nomor')->on('trans_muatan');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_muatan_detail');
        Schema::drop('trans_surat_jalan');
        Schema::drop('trans_muatan');
    }
}
