<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipeToTableTransPyr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pyr', function (Blueprint $table) {
            $table->string('tipe',30)->nullable();

            $table->foreign('tipe')->references('kode')->on('ref_tipebarang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pyr', function (Blueprint $table) {
            $table->dropColumn('tipe');
        });
    }
}
