<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// use DB;

class InitTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE SEQUENCE "0_amortisation_amortisation_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_amortisation" (
            "amortisation_id" integer DEFAULT nextval(\'"0_amortisation_amortisation_id_seq"\') NOT NULL,
            "asset_id" integer,
            "amortisation_year" integer,
            "asset_value" real,
            "amount" real,
            "posted" integer DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "details" text,
            "bulan" integer,
            "tmuk" character varying(150),
            CONSTRAINT "0_amortisation_pkey" PRIMARY KEY ("amortisation_id")
        ) WITH (oids = false)');

        DB::statement('CREATE INDEX "0_amortisation_asset_id" ON "public"."0_amortisation" USING btree ("asset_id")');


        DB::statement('CREATE SEQUENCE "0_approval_escrow_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_approval_escrow" (
            "id" integer DEFAULT nextval(\'"0_approval_escrow_id_seq"\') NOT NULL,
            "jenjang_persetujuan" character varying(50),
            "approver_1" character varying(100),
            "approver_2" character varying(100),
            CONSTRAINT "0_approval_escrow_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_areas_area_code_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_areas" (
            "area_code" integer DEFAULT nextval(\'"0_areas_area_code_seq"\') NOT NULL,
            "description" character varying(60) DEFAULT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_areas_description_key" UNIQUE ("description"),
            CONSTRAINT "0_areas_pkey" PRIMARY KEY ("area_code")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_asset_types_asset_type_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_asset_types" (
            "asset_type_id" integer DEFAULT nextval(\'"0_asset_types_asset_type_id_seq"\') NOT NULL,
            "asset_type_name" character varying(50) NOT NULL,
            "depreciation_type" integer DEFAULT 1 NOT NULL,
            "depreciation_rate" real DEFAULT 10 NOT NULL,
            "asset_account" character varying(15),
            "depreciation_account" character varying(15),
            "accumulated_account" character varying(15),
            "valuation_account" character varying(15),
            "disposal_account" character varying(15),
            "inactive" smallint DEFAULT 0 NOT NULL,
            "details" text,
            CONSTRAINT "0_asset_types_pkey" PRIMARY KEY ("asset_type_id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_asset_valuations_asset_valuation_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_asset_valuations" (
            "asset_valuation_id" integer DEFAULT nextval(\'"0_asset_valuations_asset_valuation_id_seq"\') NOT NULL,
            "asset_id" integer,
            "valuation_year" integer,
            "asset_value" real DEFAULT 0 NOT NULL,
            "value_change" real DEFAULT 0 NOT NULL,
            "posted" integer DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "details" text,
            CONSTRAINT "0_asset_valuations_asset_id_valuation_year_key" UNIQUE ("asset_id", "valuation_year"),
            CONSTRAINT "0_asset_valuations_pkey" PRIMARY KEY ("asset_valuation_id")
        ) WITH (oids = false)');

        DB::statement('CREATE INDEX "0_asset_valuations_asset_id" ON "public"."0_asset_valuations" USING btree ("asset_id")');


        DB::statement('CREATE SEQUENCE "0_assets_asset_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_assets" (
            "asset_id" integer DEFAULT nextval(\'"0_assets_asset_id_seq"\') NOT NULL,
            "asset_type_id" integer,
            "item_id" integer,
            "asset_name" character varying(150),
            "asset_serial" character varying(150),
            "purchase_date" date NOT NULL,
            "purchase_value" real NOT NULL,
            "disposal_amount" real,
            "disposal_date" date,
            "disposal_posting" integer DEFAULT 0,
            "tag_number" character varying(150),
            "asset_location" character varying(150),
            "asset_condition" character varying(150),
            "asset_acquisition" character varying(150),
            "inactive" smallint DEFAULT 0 NOT NULL,
            "details" text,
            CONSTRAINT "0_assets_pkey" PRIMARY KEY ("asset_id")
        ) WITH (oids = false)');

        DB::statement('CREATE INDEX "0_assets_asset_type_id" ON "public"."0_assets" USING btree ("asset_type_id")');

        DB::statement('CREATE INDEX "0_assets_item_id" ON "public"."0_assets" USING btree ("item_id")');


        DB::statement('CREATE SEQUENCE "0_assortment_type_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_assortment_type" (
            "id" integer DEFAULT nextval(\'"0_assortment_type_id_seq"\') NOT NULL,
            "ass_type" character varying(10),
            "jumlah_rack_lorong" integer,
            "jumlah_rack_dinding" integer,
            "jumlah_rack_kasir" integer
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_attachments_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_attachments" (
            "id" integer DEFAULT nextval(\'"0_attachments_id_seq"\') NOT NULL,
            "description" character varying(60) DEFAULT \'\' NOT NULL,
            "type_no" integer DEFAULT 0 NOT NULL,
            "trans_no" integer DEFAULT 0 NOT NULL,
            "unique_name" character varying(60) DEFAULT \'\' NOT NULL,
            "tran_date" date,
            "filename" character varying(60) DEFAULT \'\' NOT NULL,
            "filesize" integer DEFAULT 0 NOT NULL,
            "filetype" character varying DEFAULT \'\' NOT NULL,
            CONSTRAINT "0_attachments_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_audit_trail_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_audit_trail" (
            "id" integer DEFAULT nextval(\'"0_audit_trail_id_seq"\') NOT NULL,
            "type" smallint DEFAULT 0 NOT NULL,
            "trans_no" integer DEFAULT 0 NOT NULL,
            "user" smallint DEFAULT 0 NOT NULL,
            "stamp" timestamp(6) DEFAULT now(),
            "description" character varying(60) DEFAULT NULL,
            "fiscal_year" integer NOT NULL,
            "gl_date" date,
            "gl_seq" integer,
            CONSTRAINT "0_audit_trail_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_bank_bank_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_bank" (
            "bank_id" integer DEFAULT nextval(\'"0_bank_bank_id_seq"\') NOT NULL,
            "bank_name" text,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "imm_store_code" character varying,
            "account_code_credit" character varying(2044),
            "account_code_debit" character varying(2044),
            "account_code_voucher" character varying(2044),
            "account_code_piutang" character varying(2044),
            "account_code_point" character varying(2044),
            "cash" character varying(2044),
            CONSTRAINT "0_bank_pkey" PRIMARY KEY ("bank_id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_bank_account_topup" (
            "no" integer,
            "deskripsi" character varying(50),
            "account_code" character varying(50),
            "account_name" character varying(100)
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_bank_accounts_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_bank_accounts" (
            "account_code" character varying(15) DEFAULT \'\' NOT NULL,
            "account_type" smallint DEFAULT 0 NOT NULL,
            "bank_account_name" character varying(60) DEFAULT \'\' NOT NULL,
            "bank_account_number" character varying(100) DEFAULT \'\' NOT NULL,
            "bank_name" character varying(60) DEFAULT \'\' NOT NULL,
            "bank_address" text,
            "bank_curr_code" character(3) DEFAULT \'\' NOT NULL,
            "dflt_curr_act" smallint DEFAULT 0 NOT NULL,
            "id" integer DEFAULT nextval(\'"0_bank_accounts_id_seq"\') NOT NULL,
            "last_reconciled_date" timestamp(6),
            "ending_reconcile_balance" double precision DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_bank_accounts_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_bank_trans_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_bank_trans" (
            "id" integer DEFAULT nextval(\'"0_bank_trans_id_seq"\') NOT NULL,
            "type" smallint,
            "trans_no" integer,
            "bank_act" character varying(15) DEFAULT \'\' NOT NULL,
            "ref" character varying(40) DEFAULT NULL,
            "trans_date" date,
            "amount" double precision,
            "dimension_id" integer DEFAULT 0 NOT NULL,
            "dimension2_id" integer DEFAULT 0 NOT NULL,
            "person_type_id" integer DEFAULT 0 NOT NULL,
            "person_id" bytea,
            "reconciled" date,
            CONSTRAINT "0_bank_trans_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_bom_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_bom" (
            "id" integer DEFAULT nextval(\'"0_bom_id_seq"\') NOT NULL,
            "parent" character varying(20) DEFAULT \'\' NOT NULL,
            "component" character varying(20) DEFAULT \'\' NOT NULL,
            "workcentre_added" integer DEFAULT 0 NOT NULL,
            "loc_code" character varying(5) DEFAULT \'\' NOT NULL,
            "quantity" double precision DEFAULT 1 NOT NULL,
            CONSTRAINT "0_bom_pkey" PRIMARY KEY ("parent", "component", "workcentre_added", "loc_code")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_budget_trans_counter_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_budget_trans" (
            "counter" integer DEFAULT nextval(\'"0_budget_trans_counter_seq"\') NOT NULL,
            "type" smallint DEFAULT 0 NOT NULL,
            "type_no" bigint DEFAULT 1 NOT NULL,
            "tran_date" date,
            "account" character varying(15) DEFAULT \'\' NOT NULL,
            "memo_" text NOT NULL,
            "amount" double precision DEFAULT 0 NOT NULL,
            "dimension_id" integer DEFAULT 0,
            "dimension2_id" integer DEFAULT 0,
            "person_type_id" integer,
            "person_id" bytea,
            "status_lock" boolean,
            "kode_tmuk" character varying(255),
            CONSTRAINT "0_budget_trans_pkey" PRIMARY KEY ("counter")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_bumun_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_bumun" (
            "id" integer DEFAULT nextval(\'"0_bumun_id_seq"\') NOT NULL,
            "bumun_cd" character varying(255),
            "bumun_nm" character varying(255)
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_cat_1_ID_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_cat_1" (
            "id" integer DEFAULT nextval(\'"0_cat_1_ID_seq"\') NOT NULL,
            "bumun_cd" character varying(255),
            "bumun_nm" character varying(255),
            "l1_cd" character varying(255),
            "l1_nm" character varying(255),
            CONSTRAINT "0_cat_1_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_cat_2_ID_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_cat_2" (
            "id" integer DEFAULT nextval(\'"0_cat_2_ID_seq"\') NOT NULL,
            "bumun_cd" character varying(255),
            "l1_cd" character varying(255),
            "l2_cd" character varying(255),
            "l2_nm" character varying(255),
            CONSTRAINT "0_cat_2_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_cat_3_ID_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_cat_3" (
            "id" integer DEFAULT nextval(\'"0_cat_3_ID_seq"\') NOT NULL,
            "bumun_cd" character varying(255),
            "l2_cd" character varying(255),
            "l3_cd" character varying(255),
            "l3_nm" character varying(255),
            CONSTRAINT "0_cat_3_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_cat_4_ID_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_cat_4" (
            "id" integer DEFAULT nextval(\'"0_cat_4_ID_seq"\') NOT NULL,
            "bumun_cd" character varying(255),
            "l3_cd" character varying(255),
            "l4_cd" character varying(255),
            "l4_nm" character varying(255),
            CONSTRAINT "0_cat_4_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_chart_class" (
            "cid" character varying(3) NOT NULL,
            "class_name" character varying(60) DEFAULT \'\' NOT NULL,
            "ctype" smallint DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_chart_class_pkey" PRIMARY KEY ("cid")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_chart_master" (
            "account_code" character varying(15) DEFAULT \'\' NOT NULL,
            "account_code2" character varying(15) DEFAULT \'\' NOT NULL,
            "account_name" character varying(60) DEFAULT \'\' NOT NULL,
            "account_type" character varying(10) DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "curr" character(3),
            CONSTRAINT "0_chart_master_pkey" PRIMARY KEY ("account_code")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_chart_types" (
            "id" character varying(10) NOT NULL,
            "name" character varying(60) DEFAULT \'\' NOT NULL,
            "class_id" character varying(3) DEFAULT \'\' NOT NULL,
            "parent" character varying(10) DEFAULT -1 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_chart_types_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_comments" (
            "type" integer DEFAULT 0 NOT NULL,
            "id" integer DEFAULT 0 NOT NULL,
            "date_" date,
            "memo_" text
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_container_type_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_container_type" (
            "id" integer DEFAULT nextval(\'"0_container_type_id_seq"\') NOT NULL,
            "container_type" character varying(100),
            "tinggi_luar" double precision,
            "tinggi_dalam" double precision,
            "panjang_atas" double precision,
            "panjang_bawah" double precision,
            "lebar_atas" double precision,
            "lebar_bawah" double precision,
            "volume" double precision,
            "box_type" boolean
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_credit_status_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_credit_status" (
            "id" integer DEFAULT nextval(\'"0_credit_status_id_seq"\') NOT NULL,
            "reason_description" character(100) DEFAULT \'\' NOT NULL,
            "dissallow_invoices" smallint DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_credit_status_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_credit_status_reason_description_key" UNIQUE ("reason_description")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_crm_categories_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_crm_categories" (
            "id" integer DEFAULT nextval(\'"0_crm_categories_id_seq"\') NOT NULL,
            "type" character varying(20) NOT NULL,
            "action" character varying(20) NOT NULL,
            "name" character varying(30) NOT NULL,
            "description" text NOT NULL,
            "system" smallint DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_crm_categories_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_crm_categories_type_action_key" UNIQUE ("type", "action"),
            CONSTRAINT "0_crm_categories_type_name_key" UNIQUE ("type", "name")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_crm_contacts_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_crm_contacts" (
            "id" integer DEFAULT nextval(\'"0_crm_contacts_id_seq"\') NOT NULL,
            "person_id" integer DEFAULT 0 NOT NULL,
            "type" character varying(20) NOT NULL,
            "action" character varying(20) NOT NULL,
            "entity_id" character varying(11) DEFAULT NULL,
            CONSTRAINT "0_crm_contacts_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_crm_persons_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_crm_persons" (
            "id" integer DEFAULT nextval(\'"0_crm_persons_id_seq"\') NOT NULL,
            "ref" character varying(30) NOT NULL,
            "name" character varying(60) NOT NULL,
            "name2" character varying(60) DEFAULT NULL,
            "address" text,
            "phone" character varying(30) DEFAULT NULL,
            "phone2" character varying(30) DEFAULT NULL,
            "fax" character varying(30) DEFAULT NULL,
            "email" character varying(100) DEFAULT NULL,
            "lang" character(5) DEFAULT NULL,
            "notes" text NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_crm_persons_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_currencies" (
            "currency" character varying(60) DEFAULT \'\' NOT NULL,
            "curr_abrev" character(3) DEFAULT \'\' NOT NULL,
            "curr_symbol" character varying(10) DEFAULT \'\' NOT NULL,
            "country" character varying(100) DEFAULT \'\' NOT NULL,
            "hundreds_name" character varying(15) DEFAULT \'\' NOT NULL,
            "auto_update" smallint DEFAULT 1 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_currencies_pkey" PRIMARY KEY ("curr_abrev")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_cust_allocations_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_cust_allocations" (
            "id" integer DEFAULT nextval(\'"0_cust_allocations_id_seq"\') NOT NULL,
            "amt" double precision,
            "date_alloc" date,
            "trans_no_from" integer,
            "trans_type_from" integer,
            "trans_no_to" integer,
            "trans_type_to" integer,
            CONSTRAINT "0_cust_allocations_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_cust_branch_branch_code_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_cust_branch" (
            "branch_code" integer DEFAULT nextval(\'"0_cust_branch_branch_code_seq"\') NOT NULL,
            "debtor_no" integer DEFAULT 0 NOT NULL,
            "br_name" character varying(60) DEFAULT \'\' NOT NULL,
            "branch_ref" character varying(30) DEFAULT \'\' NOT NULL,
            "br_address" text NOT NULL,
            "area" integer DEFAULT 0,
            "salesman" integer DEFAULT 1 NOT NULL,
            "contact_name" character varying(60) DEFAULT \'\' NOT NULL,
            "default_location" character varying(5) DEFAULT \'\' NOT NULL,
            "tax_group_id" integer,
            "sales_account" character varying(15) DEFAULT \'\' NOT NULL,
            "sales_discount_account" character varying(15) DEFAULT \'\' NOT NULL,
            "receivables_account" character varying(15) DEFAULT \'\' NOT NULL,
            "payment_discount_account" character varying(15) DEFAULT \'\' NOT NULL,
            "default_ship_via" integer DEFAULT 1 NOT NULL,
            "disable_trans" smallint DEFAULT 0 NOT NULL,
            "br_post_address" text NOT NULL,
            "group_no" smallint DEFAULT 0 NOT NULL,
            "notes" text NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "nomor_member" character varying(255),
            CONSTRAINT "0_cust_branch_pkey" PRIMARY KEY ("branch_code", "debtor_no")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_debtor_trans" (
            "trans_no" integer DEFAULT 0 NOT NULL,
            "type" smallint DEFAULT 0 NOT NULL,
            "version" smallint DEFAULT 0 NOT NULL,
            "debtor_no" integer,
            "branch_code" integer DEFAULT -1 NOT NULL,
            "tran_date" date,
            "due_date" date,
            "reference" character varying(60) DEFAULT \'\' NOT NULL,
            "tpe" integer DEFAULT 0 NOT NULL,
            "order_" integer DEFAULT 0 NOT NULL,
            "ov_amount" double precision DEFAULT 0 NOT NULL,
            "ov_gst" double precision DEFAULT 0 NOT NULL,
            "ov_freight" double precision DEFAULT 0 NOT NULL,
            "ov_freight_tax" double precision DEFAULT 0 NOT NULL,
            "ov_discount" double precision DEFAULT 0 NOT NULL,
            "alloc" double precision DEFAULT 0 NOT NULL,
            "rate" double precision DEFAULT 1 NOT NULL,
            "ship_via" integer,
            "dimension_id" integer DEFAULT 0 NOT NULL,
            "dimension2_id" integer DEFAULT 0 NOT NULL,
            "payment_terms" integer,
            CONSTRAINT "0_debtor_trans_pkey" PRIMARY KEY ("type", "trans_no")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_debtor_trans_details_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_debtor_trans_details" (
            "id" integer DEFAULT nextval(\'"0_debtor_trans_details_id_seq"\') NOT NULL,
            "debtor_trans_no" integer,
            "debtor_trans_type" integer,
            "stock_id" character varying(20) DEFAULT \'\' NOT NULL,
            "description" text,
            "unit_price" double precision DEFAULT 0 NOT NULL,
            "unit_tax" double precision DEFAULT 0 NOT NULL,
            "quantity" double precision DEFAULT 0 NOT NULL,
            "discount_percent" double precision DEFAULT 0 NOT NULL,
            "standard_cost" double precision DEFAULT 0 NOT NULL,
            "qty_done" double precision DEFAULT 0 NOT NULL,
            "src_id" integer NOT NULL,
            CONSTRAINT "0_debtor_trans_details_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_debtors_master_debtor_no_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_debtors_master" (
            "debtor_no" integer DEFAULT nextval(\'"0_debtors_master_debtor_no_seq"\') NOT NULL,
            "name" character varying(100) DEFAULT \'\' NOT NULL,
            "debtor_ref" character varying(30) NOT NULL,
            "address" text,
            "tax_id" character varying(55) DEFAULT \'\' NOT NULL,
            "curr_code" character(3) DEFAULT \'\' NOT NULL,
            "sales_type" integer DEFAULT 1 NOT NULL,
            "dimension_id" integer DEFAULT 0 NOT NULL,
            "dimension2_id" integer DEFAULT 0 NOT NULL,
            "credit_status" integer DEFAULT 0 NOT NULL,
            "payment_terms" integer,
            "discount" double precision DEFAULT 0 NOT NULL,
            "pymt_discount" double precision DEFAULT 0 NOT NULL,
            "credit_limit" double precision DEFAULT 1000 NOT NULL,
            "notes" text NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "nomor_lotte" character varying(2044),
            CONSTRAINT "0_debtors_master_debtor_ref_key" UNIQUE ("debtor_ref"),
            CONSTRAINT "0_debtors_master_pkey" PRIMARY KEY ("debtor_no")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_dimensions_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_dimensions" (
            "id" integer DEFAULT nextval(\'"0_dimensions_id_seq"\') NOT NULL,
            "reference" character varying(60) DEFAULT \'\' NOT NULL,
            "name" character varying(60) DEFAULT \'\' NOT NULL,
            "type_" smallint DEFAULT 1 NOT NULL,
            "closed" smallint DEFAULT 0 NOT NULL,
            "date_" date,
            "due_date" date,
            CONSTRAINT "0_dimensions_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_dimensions_reference_key" UNIQUE ("reference")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_escrow_trans_counter_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_escrow_trans" (
            "counter" integer DEFAULT nextval(\'"0_escrow_trans_counter_seq"\') NOT NULL,
            "tmuk" character varying(255),
            "amount" double precision DEFAULT 0,
            "type" integer,
            "tran_date" date,
            "type_no" integer,
            "memo_" character varying(255),
            CONSTRAINT "0_escrow_trans_pkey" PRIMARY KEY ("counter")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_exchange_rates_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_exchange_rates" (
            "id" integer DEFAULT nextval(\'"0_exchange_rates_id_seq"\') NOT NULL,
            "curr_code" character(3) DEFAULT \'\' NOT NULL,
            "rate_buy" double precision DEFAULT 0 NOT NULL,
            "rate_sell" double precision DEFAULT 0 NOT NULL,
            "date_" date,
            CONSTRAINT "0_exchange_rates_curr_code_date__key" UNIQUE ("curr_code", "date_"),
            CONSTRAINT "0_exchange_rates_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_fiscal_year_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_fiscal_year" (
            "id" integer DEFAULT nextval(\'"0_fiscal_year_id_seq"\') NOT NULL,
            "begin" date,
            "end" date,
            "closed" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_fiscal_year_begin_key" UNIQUE ("begin"),
            CONSTRAINT "0_fiscal_year_end_key" UNIQUE ("end"),
            CONSTRAINT "0_fiscal_year_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_gl_trans_counter_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_gl_trans" (
            "counter" integer DEFAULT nextval(\'"0_gl_trans_counter_seq"\') NOT NULL,
            "type" smallint DEFAULT 0 NOT NULL,
            "type_no" bigint DEFAULT 1 NOT NULL,
            "tran_date" date,
            "account" character varying(15) DEFAULT \'\' NOT NULL,
            "memo_" text NOT NULL,
            "amount" double precision DEFAULT 0 NOT NULL,
            "dimension_id" integer DEFAULT 0 NOT NULL,
            "dimension2_id" integer DEFAULT 0 NOT NULL,
            "person_type_id" integer,
            "person_id" bytea,
            "tmuk_gl" character varying(2044),
            "amount_draft" double precision DEFAULT 0,
            CONSTRAINT "0_gl_trans_pkey" PRIMARY KEY ("counter")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_grn_batch_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_grn_batch" (
            "id" integer DEFAULT nextval(\'"0_grn_batch_id_seq"\') NOT NULL,
            "supplier_id" integer DEFAULT 0 NOT NULL,
            "purch_order_no" integer,
            "reference" character varying(60) DEFAULT \'\' NOT NULL,
            "delivery_date" date,
            "loc_code" character varying(5) DEFAULT NULL,
            "gr_no" character varying(255),
            CONSTRAINT "0_grn_batch_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_grn_items_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_grn_items" (
            "id" integer DEFAULT nextval(\'"0_grn_items_id_seq"\') NOT NULL,
            "grn_batch_id" integer,
            "po_detail_item" integer DEFAULT 0 NOT NULL,
            "item_code" character varying(20) DEFAULT \'\' NOT NULL,
            "description" text,
            "qty_recd" double precision DEFAULT 0 NOT NULL,
            "quantity_inv" double precision DEFAULT 0 NOT NULL,
            CONSTRAINT "0_grn_items_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_groups_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_groups" (
            "id" integer DEFAULT nextval(\'"0_groups_id_seq"\') NOT NULL,
            "description" character varying(60) DEFAULT \'\' NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_groups_description_key" UNIQUE ("description"),
            CONSTRAINT "0_groups_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_imm_store" (
            "imm_store_code" character varying(255) NOT NULL,
            "auto_approve_pr" boolean DEFAULT false,
            "imm_store_name" character varying(255),
            "ass_type" character varying(4),
            "address" character varying(255),
            "city" character varying(255),
            "zip_code" character varying(255),
            "kelurahan" character varying(255),
            "kecamatan" character varying(255),
            "propinsi" character varying(255),
            "gps_latitude" character varying(255),
            "gps_longitude" character varying(255),
            "member_card" character varying(255),
            "owner_name" character varying(255),
            "owner_email" character varying(255),
            "cde_name" character varying(255),
            "cde_email" character varying(255),
            "total_area" character varying(255),
            "selling_area" character varying(255),
            "planned_open" date,
            "actual_open" date,
            "company_name" character varying(255),
            "alamat_pajak" character varying(255),
            "npwp" character varying(255),
            "pkp" character varying(255),
            "escrow_account" character varying(255),
            "telephone" character varying(20),
            "lsi_induk_code" character varying(255),
            "jenis" character varying(2044),
            "last_sync" date,
            "nomor_debtor" integer,
            "asn" integer DEFAULT 0,
            CONSTRAINT "0_imm_store_pkey" PRIMARY KEY ("imm_store_code")
        ) WITH (oids = false)');

        DB::statement('CREATE INDEX "0_imm_store_imm_store_code_lsi_induk_code_idx" ON "public"."0_imm_store" USING btree ("imm_store_code", "lsi_induk_code")');


        DB::statement('CREATE SEQUENCE "0_imm_store_pos_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_imm_store_pos" (
            "imm_store_code" character varying,
            "machine_type" character varying,
            "machine_name" character varying,
            "id" integer DEFAULT nextval(\'"0_imm_store_pos_id_seq"\') NOT NULL,
            CONSTRAINT "0_imm_store_pos_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_item_codes_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_item_codes" (
            "id" integer DEFAULT nextval(\'"0_item_codes_id_seq"\') NOT NULL,
            "item_code" character varying(20) NOT NULL,
            "stock_id" character varying(20) NOT NULL,
            "description" character varying(200) DEFAULT \'\' NOT NULL,
            "category_id" smallint NOT NULL,
            "quantity" double precision DEFAULT 1 NOT NULL,
            "is_foreign" smallint DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_item_codes_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_item_codes_stock_id_item_code_key" UNIQUE ("stock_id", "item_code")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_item_tax_type_exemptions" (
            "item_tax_type_id" integer DEFAULT 0 NOT NULL,
            "tax_type_id" integer DEFAULT 0 NOT NULL,
            CONSTRAINT "0_item_tax_type_exemptions_pkey" PRIMARY KEY ("item_tax_type_id", "tax_type_id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_item_tax_types_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_item_tax_types" (
            "id" integer DEFAULT nextval(\'"0_item_tax_types_id_seq"\') NOT NULL,
            "name" character varying(60) DEFAULT \'\' NOT NULL,
            "exempt" smallint DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_item_tax_types_name_key" UNIQUE ("name"),
            CONSTRAINT "0_item_tax_types_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_item_units" (
            "abbr" character varying(20) NOT NULL,
            "name" character varying(40) NOT NULL,
            "decimals" smallint NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "id" integer,
            CONSTRAINT "0_item_units_name_key" UNIQUE ("name"),
            CONSTRAINT "0_item_units_pkey" PRIMARY KEY ("abbr")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE kubikasi_container_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_kubikasi_container" (
            "id" integer DEFAULT nextval(\'kubikasi_container_id_seq\') NOT NULL,
            "no_po" character varying(255),
            "jml_container" integer,
            "jenis_item" character varying(255),
            "quantity" integer,
            "container_type" character varying(255),
            "stock_id" character varying(255),
            "status_delivery" boolean,
            "no_retur" integer,
            "no_label_container" character varying(255),
            CONSTRAINT "0_kubikasi_container_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE kubikasi_truck_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_kubikasi_truck" (
            "id" integer DEFAULT nextval(\'kubikasi_truck_id_seq\') NOT NULL,
            "jenis_truck" character varying(255),
            "tmuk" character varying(255),
            "alamat" character varying(255),
            "no_po" character varying(255),
            "no_container_bronjong" character varying(255),
            "jumlah_container" integer,
            "jumlah_bronjong" integer DEFAULT 0,
            "posisi" character varying(255),
            "no_truck" character varying(255),
            "nama_supir" character varying(255),
            "no_surat_jalan" character varying(255),
            "expedition_number" integer,
            "loading_date" date,
            "no_delivery_note" character varying(255),
            CONSTRAINT "0_kubikasi_truck_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_loc_stock" (
            "loc_code" character(5) DEFAULT \'\' NOT NULL,
            "stock_id" character(20) DEFAULT \'\' NOT NULL,
            "reorder_level" bigint DEFAULT 0 NOT NULL,
            "block_order" boolean DEFAULT false,
            CONSTRAINT "0_loc_stock_pkey" PRIMARY KEY ("loc_code", "stock_id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_locations" (
            "loc_code" character varying(5) DEFAULT \'\' NOT NULL,
            "location_name" character varying(60) DEFAULT \'\' NOT NULL,
            "delivery_address" text NOT NULL,
            "phone" character varying(30) DEFAULT \'\' NOT NULL,
            "phone2" character varying(30) DEFAULT \'\' NOT NULL,
            "fax" character varying(30) DEFAULT \'\' NOT NULL,
            "email" character varying(100) DEFAULT \'\' NOT NULL,
            "contact" character varying(30) DEFAULT \'\' NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_locations_pkey" PRIMARY KEY ("loc_code")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_map_tmuk" (
            "stock_id" character varying(2044) NOT NULL,
            "tmuk" character varying(2044) NOT NULL,
            "map" double precision NOT NULL,
            "date" date NOT NULL,
            "inserted_at" timestamp(6) DEFAULT now() NOT NULL,
            CONSTRAINT "0_map_tmuk_pkey" PRIMARY KEY ("stock_id", "tmuk", "date")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_movement_types_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_movement_types" (
            "id" integer DEFAULT nextval(\'"0_movement_types_id_seq"\') NOT NULL,
            "name" character varying(60) DEFAULT \'\' NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_movement_types_name_key" UNIQUE ("name"),
            CONSTRAINT "0_movement_types_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_number_range_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_number_range" (
            "id" integer DEFAULT nextval(\'"0_number_range_id_seq"\') NOT NULL,
            "trans_type" character varying(2044) NOT NULL,
            "period" character varying NOT NULL,
            "acc_id" integer,
            "prefix" character varying,
            "from_" integer,
            "to_" integer,
            "current_no" integer,
            "rel_sec" character varying,
            CONSTRAINT "0_number_range_pkey" PRIMARY KEY ("trans_type", "period")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_payment_request_counter_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_payment_request" (
            "counter" integer DEFAULT nextval(\'"0_payment_request_counter_seq"\') NOT NULL,
            "trans_no" integer,
            "attachment" character varying(255),
            "attachment_po" character varying(255),
            "attachment_gr" character varying(255),
            "attachment_custom" character varying(255),
            "down_payment" double precision,
            "dp_date" date,
            "due_date" date,
            "payment_method" character varying(255),
            "pelunasan" double precision,
            "pyr_date" date,
            "pyr_no" character varying(255),
            "attachment_extra_name" character varying(255),
            "attachment_extra_no" character varying(255),
            "inv_no" character varying(255),
            "reference" date,
            "reference_2" character varying(255),
            "supp_name" integer,
            "bank_name" character varying(255),
            "po_no" character varying(255),
            "gr_no" character varying(255),
            "rs_name" character varying(255),
            "void_status" smallint,
            "approve_by" character varying(255),
            "print_status" smallint,
            "no_pmt" character varying(255),
            "print_count" smallint,
            CONSTRAINT "0_payment_request_pkey" PRIMARY KEY ("counter")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_payment_terms_terms_indicator_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_payment_terms" (
            "terms_indicator" integer DEFAULT nextval(\'"0_payment_terms_terms_indicator_seq"\') NOT NULL,
            "terms" character(80) DEFAULT \'\' NOT NULL,
            "days_before_due" smallint DEFAULT 0 NOT NULL,
            "day_in_following_month" smallint DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_payment_terms_pkey" PRIMARY KEY ("terms_indicator"),
            CONSTRAINT "0_payment_terms_terms_key" UNIQUE ("terms")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_picking_task_counter_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_picking_task" (
            "counter" integer DEFAULT nextval(\'"0_picking_task_counter_seq"\') NOT NULL,
            "request_detail_id" integer,
            "picking_quantity" integer,
            "request_no" integer,
            "no_picking_task" character varying(50),
            "tran_date" date,
            CONSTRAINT "0_picking_task_pkey" PRIMARY KEY ("counter")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_prices_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_prices" (
            "id" integer DEFAULT nextval(\'"0_prices_id_seq"\') NOT NULL,
            "stock_id" character varying(20) DEFAULT \'\' NOT NULL,
            "sales_type_id" integer DEFAULT 0 NOT NULL,
            "curr_abrev" character(3) DEFAULT \'\' NOT NULL,
            "price" double precision DEFAULT (0) NOT NULL,
            "margin" double precision DEFAULT 0,
            "average_cost" double precision DEFAULT 0,
            "uom1_suggested_price" double precision DEFAULT 0,
            "uom1_changed_price" double precision DEFAULT 0,
            "uom1_margin_amount" double precision DEFAULT 0,
            "uom2_suggested_price" double precision DEFAULT 0,
            "uom2_rounding_price" double precision DEFAULT 0,
            "uom2_changed_price" double precision DEFAULT 0,
            "uom2_margin_amount" double precision DEFAULT 0,
            "uom3_suggested_price" double precision DEFAULT 0,
            "uom3_rounding_price" double precision DEFAULT 0,
            "uom3_changed_price" double precision DEFAULT 0,
            "uom3_margin_amount" double precision DEFAULT 0,
            "uom1_cost_price" double precision DEFAULT 0,
            "uom2_cost_price" double precision DEFAULT 0,
            "uom3_cost_price" double precision DEFAULT 0,
            "uom4_suggested_price" double precision DEFAULT 0,
            "uom4_rounding_price" double precision DEFAULT 0,
            "uom4_changed_price" double precision DEFAULT 0,
            "uom4_margin_amount" double precision DEFAULT 0,
            "uom4_cost_price" double precision DEFAULT 0,
            "tmuk" character varying(255),
            "uom1_gmd" double precision DEFAULT 0,
            CONSTRAINT "0_prices_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_prices_stock_id_sales_type_id_curr_abrev_tmuk_key" UNIQUE ("stock_id", "sales_type_id", "curr_abrev", "tmuk")
        ) WITH (oids = false);');

        DB::statement('COMMENT ON COLUMN "public"."0_prices"."price" IS \'ini sama dengan uom1_rounding_price\'');


        DB::statement('CREATE SEQUENCE "0_print_profiles_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_print_profiles" (
            "id" integer DEFAULT nextval(\'"0_print_profiles_id_seq"\') NOT NULL,
            "profile" character varying(30) NOT NULL,
            "report" character varying(5) DEFAULT NULL,
            "printer" smallint,
            CONSTRAINT "0_print_profiles_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_print_profiles_profile_report_key" UNIQUE ("profile", "report")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_printers_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_printers" (
            "id" integer DEFAULT nextval(\'"0_printers_id_seq"\') NOT NULL,
            "name" character varying(20) NOT NULL,
            "description" character varying(60) NOT NULL,
            "queue" character varying(20) NOT NULL,
            "host" character varying(40) NOT NULL,
            "port" smallint NOT NULL,
            "timeout" smallint NOT NULL,
            CONSTRAINT "0_printers_name_key" UNIQUE ("name"),
            CONSTRAINT "0_printers_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_prod" (
            "prod_cd" character varying(255) NOT NULL,
            "prod_nm" character varying(255),
            "short_nm" character varying(255),
            "bumun_cd" character varying(255),
            "l1_cd" character varying(255),
            "l2_cd" character varying(255),
            "l3_cd" character varying(255),
            "l4_cd" character varying(255),
            "ass_type_1" boolean DEFAULT false NOT NULL,
            "ass_type_2" boolean DEFAULT false,
            "ass_type_3" boolean DEFAULT false,
            "uom1_nm" character varying(255),
            "uom1_srcmk_cd" character varying(255),
            "uom1_prod_nm" character varying(255),
            "uom1_htype_1" boolean DEFAULT false,
            "uom1_htype_2" boolean DEFAULT false,
            "uom1_htype_3" boolean DEFAULT false,
            "uom1_htype_4" boolean DEFAULT false,
            "uom1_htype_5" boolean DEFAULT false,
            "uom1_width" integer,
            "uom1_length" integer,
            "uom1_height" integer,
            "uom1_weight" numeric(7,2),
            "uom2_nm" character varying(255),
            "uom2_qty" numeric(7,2),
            "uom2_srcmk_cd" character varying(255),
            "uom2_prod_nm" character varying(255),
            "uom2_htype_1" boolean DEFAULT false,
            "uom2_htype_2" boolean DEFAULT false,
            "uom2_htype_3" boolean DEFAULT false,
            "uom2_htype_4" boolean DEFAULT false,
            "uom2_htype_5" boolean DEFAULT false,
            "uom2_width" integer,
            "uom2_length" integer,
            "uom2_height" integer,
            "uom2_weight" numeric(7,2),
            "uom3_nm" character varying(255),
            "uom3_qty" numeric(7,2),
            "uom3_srcmk_cd" character varying(255),
            "uom3_prod_nm" character varying(255),
            "uom3_htype_1" boolean DEFAULT false,
            "uom3_htype_2" boolean DEFAULT false,
            "uom3_htype_3" boolean DEFAULT false,
            "uom3_htype_4" boolean DEFAULT false,
            "uom3_htype_5" boolean DEFAULT false,
            "uom3_width" integer,
            "uom3_length" integer,
            "uom3_height" integer,
            "uom3_weight" numeric(7,2),
            "timestamp" timestamp(6),
            "expiry_product" boolean DEFAULT false,
            "batch_product" boolean DEFAULT false,
            "weight_product" boolean DEFAULT false,
            "active_status" boolean DEFAULT false,
            CONSTRAINT "0_prod_pkey" PRIMARY KEY ("prod_cd")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_promo_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_promosi" (
            "id" integer DEFAULT nextval(\'"0_promo_id_seq"\') NOT NULL,
            "kode_promosi" character varying(255),
            "nama_promosi" character varying(255),
            "from_" date,
            "to_" date,
            "loc_code" character varying(255),
            "jenis_promo" character varying(255),
            "amount" double precision,
            "created_at" date DEFAULT now(),
            CONSTRAINT "0_promosi_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_promo_barang_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_promosi_barang" (
            "id" integer DEFAULT nextval(\'"0_promo_barang_id_seq"\') NOT NULL,
            "kode_promosi" character varying(255) NOT NULL,
            "stock_id" character varying(255) NOT NULL,
            "qty" integer,
            "discount_max" integer,
            "discount_replace" integer,
            "price" double precision DEFAULT 0,
            "discount_amount" double precision DEFAULT 0,
            CONSTRAINT "0_promosi_barang_pkey" PRIMARY KEY ("id", "kode_promosi", "stock_id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_promo_hadiah_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_promosi_hadiah" (
            "id" integer DEFAULT nextval(\'"0_promo_hadiah_id_seq"\') NOT NULL,
            "kode_promosi" character varying NOT NULL,
            "stock_id" character varying NOT NULL,
            "qty" integer,
            CONSTRAINT "0_promosi_hadiah_pkey" PRIMARY KEY ("id", "kode_promosi", "stock_id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_promosi_location_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_promosi_location" (
            "kode_promosi" character varying,
            "location" character varying,
            "id" integer DEFAULT nextval(\'"0_promosi_location_id_seq"\') NOT NULL,
            CONSTRAINT "0_promosi_location_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_promo_pwp_detail_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_promosi_pwp_detail" (
            "id" integer DEFAULT nextval(\'"0_promo_pwp_detail_id_seq"\') NOT NULL,
            "kode_promosi" character varying,
            "stock_id" character varying,
            "price" double precision,
            CONSTRAINT "0_promosi_pwp_detail_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_purch_data" (
            "supplier_id" integer DEFAULT 0 NOT NULL,
            "stock_id" character(20) DEFAULT \'\' NOT NULL,
            "price" double precision DEFAULT 0 NOT NULL,
            "suppliers_uom" character(50) DEFAULT \'\' NOT NULL,
            "conversion_factor" double precision DEFAULT 1 NOT NULL,
            "supplier_description" character(50) DEFAULT \'\' NOT NULL,
            CONSTRAINT "0_purch_data_pkey" PRIMARY KEY ("supplier_id", "stock_id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_purch_order_details_po_detail_item_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_purch_order_details" (
            "po_detail_item" integer DEFAULT nextval(\'"0_purch_order_details_po_detail_item_seq"\') NOT NULL,
            "order_no" integer DEFAULT 0 NOT NULL,
            "item_code" character varying(20) DEFAULT \'\' NOT NULL,
            "description" text,
            "delivery_date" date,
            "qty_invoiced" double precision DEFAULT 0 NOT NULL,
            "unit_price" double precision DEFAULT 0 NOT NULL,
            "act_price" double precision DEFAULT 0 NOT NULL,
            "std_cost_unit" double precision DEFAULT 0 NOT NULL,
            "quantity_ordered" double precision DEFAULT 0 NOT NULL,
            "quantity_received" double precision DEFAULT 0 NOT NULL,
            "pr_immpos_id" integer DEFAULT 0,
            "act_qty" double precision DEFAULT 0,
            CONSTRAINT "0_purch_order_details_pkey" PRIMARY KEY ("po_detail_item")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_purch_orders_order_no_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_purch_orders" (
            "order_no" integer DEFAULT nextval(\'"0_purch_orders_order_no_seq"\') NOT NULL,
            "supplier_id" integer DEFAULT 0 NOT NULL,
            "comments" text,
            "ord_date" date,
            "reference" text NOT NULL,
            "requisition_no" text,
            "into_stock_location" character varying(5) DEFAULT \'\' NOT NULL,
            "delivery_address" text NOT NULL,
            "total" double precision DEFAULT 0 NOT NULL,
            "tax_included" smallint DEFAULT 0 NOT NULL,
            "nomorpr" character varying(100),
            "po_no" character varying(255),
            "old_total" double precision,
            CONSTRAINT "0_purch_orders_pkey" PRIMARY KEY ("order_no")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_purch_request_request_no_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_purch_request" (
            "request_no" integer DEFAULT nextval(\'"0_purch_request_request_no_seq"\') NOT NULL,
            "supplier_id" integer DEFAULT 0 NOT NULL,
            "comments" text,
            "ord_date" date,
            "reference" text NOT NULL,
            "requisition_no" text,
            "into_stock_location" character varying(5) DEFAULT \'\' NOT NULL,
            "delivery_address" text NOT NULL,
            "total" double precision DEFAULT 0 NOT NULL,
            "tax_included" smallint DEFAULT 0 NOT NULL,
            "tmuk" character varying(100),
            "create_date" date,
            CONSTRAINT "0_purch_request_pkey" PRIMARY KEY ("request_no")
        ) WITH (oids = false)');

        DB::statement('CREATE SEQUENCE "0_purch_request_details_po_detail_item_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_purch_request_details" (
            "po_detail_item" integer DEFAULT nextval(\'"0_purch_request_details_po_detail_item_seq"\') NOT NULL,
            "request_no" integer DEFAULT 0 NOT NULL,
            "item_code" character varying(20) DEFAULT \'\' NOT NULL,
            "description" text,
            "delivery_date" date,
            "qty_invoiced" double precision DEFAULT 0 NOT NULL,
            "unit_price" double precision DEFAULT 0 NOT NULL,
            "act_price" double precision DEFAULT 0 NOT NULL,
            "std_cost_unit" double precision DEFAULT 0 NOT NULL,
            "quantity_requested" double precision DEFAULT 0 NOT NULL,
            "quantity_received" double precision DEFAULT 0 NOT NULL,
            "pr_immpos_id" integer DEFAULT 0,
            CONSTRAINT "0_purch_request_details_pkey" PRIMARY KEY ("po_detail_item")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_quick_entries_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_quick_entries" (
            "id" integer DEFAULT nextval(\'"0_quick_entries_id_seq"\') NOT NULL,
            "type" smallint DEFAULT 0 NOT NULL,
            "description" character varying(60) NOT NULL,
            "base_amount" double precision DEFAULT 0 NOT NULL,
            "base_desc" character varying(60) DEFAULT NULL,
            "bal_type" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_quick_entries_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_quick_entry_lines_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_quick_entry_lines" (
            "id" integer DEFAULT nextval(\'"0_quick_entry_lines_id_seq"\') NOT NULL,
            "qid" smallint NOT NULL,
            "amount" double precision DEFAULT 0,
            "action" character varying(2) NOT NULL,
            "dest_id" character varying(15) DEFAULT \'\' NOT NULL,
            "dimension_id" smallint,
            "dimension2_id" smallint,
            CONSTRAINT "0_quick_entry_lines_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_rack_type_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_rack_type" (
            "id" integer DEFAULT nextval(\'"0_rack_type_id_seq"\') NOT NULL,
            "rack_type" character varying(30),
            "tinggi" double precision,
            "panjang" double precision,
            "lebar" double precision,
            "shelving" integer,
            "hunger" boolean,
            "tinggi_hunger" double precision,
            "panjang_hunger" double precision
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_recurrent_invoices_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_recurrent_invoices" (
            "id" integer DEFAULT nextval(\'"0_recurrent_invoices_id_seq"\') NOT NULL,
            "description" character varying(60) DEFAULT \'\' NOT NULL,
            "order_no" integer NOT NULL,
            "debtor_no" integer,
            "group_no" smallint,
            "days" integer DEFAULT 0 NOT NULL,
            "monthly" integer DEFAULT 0 NOT NULL,
            "begin" date,
            "end" date,
            "last_sent" date,
            CONSTRAINT "0_recurrent_invoices_description_key" UNIQUE ("description"),
            CONSTRAINT "0_recurrent_invoices_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_reduce_escrow_counter_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_reduce_escrow" (
            "counter" integer DEFAULT nextval(\'"0_reduce_escrow_counter_seq"\') NOT NULL,
            "nomor_po" integer,
            "tmuk" character varying,
            "amount" double precision,
            "status" integer,
            "id_gl" integer,
            "no_struk" character varying(255),
            "status_checkout" boolean,
            "approver_1" character varying(255),
            "approver_2" character varying(255),
            "approver_3" character varying(255),
            "approver_4" character varying(255),
            "memo_" character varying(255),
            "type_reduce" integer,
            "deduct_date" date,
            "selisih_qty" double precision DEFAULT 0,
            "selisih_amount" double precision DEFAULT 0,
            CONSTRAINT "0_reduce_escrow_pkey" PRIMARY KEY ("counter")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_refs" (
            "id" integer DEFAULT 0 NOT NULL,
            "type" integer DEFAULT 0 NOT NULL,
            "reference" character varying(100) DEFAULT \'\' NOT NULL,
            CONSTRAINT "0_refs_pkey" PRIMARY KEY ("id", "type")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE reorder_formula_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_reorder_formula" (
            "id" integer DEFAULT nextval(\'reorder_formula_id_seq\') NOT NULL,
            "delivery_days" integer DEFAULT 0,
            "safety_days" integer DEFAULT 0
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_retur_pembelian_trans_no_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_retur_pembelian" (
            "trans_no" integer DEFAULT nextval(\'"0_retur_pembelian_trans_no_seq"\') NOT NULL,
            "tmuk" character varying,
            "nomor_po" character varying,
            "no_return_confirmation" character varying(255),
            CONSTRAINT "0_retur_pembelian_pkey" PRIMARY KEY ("trans_no")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_retur_pembelian_detail_counter_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_retur_pembelian_detail" (
            "counter" integer DEFAULT nextval(\'"0_retur_pembelian_detail_counter_seq"\') NOT NULL,
            "trans_no" integer,
            "item_code" character varying,
            "quantity" integer,
            "accepted" smallint,
            "note" character varying(255),
            "quantity_return" integer,
            "reason" character varying(100),
            CONSTRAINT "0_retur_pembelian_detail_pkey" PRIMARY KEY ("counter")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_sales_order_details_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_sales_order_details" (
            "id" integer DEFAULT nextval(\'"0_sales_order_details_id_seq"\') NOT NULL,
            "order_no" integer DEFAULT 0 NOT NULL,
            "trans_type" smallint DEFAULT 30 NOT NULL,
            "stk_code" character varying(20) DEFAULT \'\' NOT NULL,
            "description" text,
            "qty_sent" double precision DEFAULT 0 NOT NULL,
            "unit_price" double precision DEFAULT 0 NOT NULL,
            "quantity" double precision DEFAULT 0 NOT NULL,
            "discount_percent" double precision DEFAULT 0 NOT NULL,
            CONSTRAINT "0_sales_order_details_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_sales_orders" (
            "order_no" integer NOT NULL,
            "trans_type" smallint DEFAULT 30 NOT NULL,
            "version" smallint DEFAULT 0 NOT NULL,
            "type" smallint DEFAULT 0 NOT NULL,
            "debtor_no" integer DEFAULT 0 NOT NULL,
            "branch_code" integer DEFAULT 0 NOT NULL,
            "reference" character varying(100) DEFAULT \'\' NOT NULL,
            "customer_ref" text NOT NULL,
            "comments" text,
            "ord_date" date,
            "order_type" integer DEFAULT 0 NOT NULL,
            "ship_via" integer DEFAULT 0 NOT NULL,
            "delivery_address" text NOT NULL,
            "contact_phone" character varying(30) DEFAULT NULL,
            "contact_email" character varying(100) DEFAULT NULL,
            "deliver_to" text NOT NULL,
            "freight_cost" double precision DEFAULT 0 NOT NULL,
            "from_stk_loc" character varying(5) DEFAULT \'\' NOT NULL,
            "delivery_date" date,
            "payment_terms" integer,
            "total" double precision DEFAULT 0 NOT NULL,
            CONSTRAINT "0_sales_orders_pkey" PRIMARY KEY ("trans_type", "order_no")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_sales_pos_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_sales_pos" (
            "id" integer DEFAULT nextval(\'"0_sales_pos_id_seq"\') NOT NULL,
            "pos_name" character varying(30) NOT NULL,
            "cash_sale" smallint NOT NULL,
            "credit_sale" smallint NOT NULL,
            "pos_location" character varying(5) NOT NULL,
            "pos_account" smallint NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_sales_pos_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_sales_pos_pos_name_key" UNIQUE ("pos_name")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_sales_types_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_sales_types" (
            "id" integer DEFAULT nextval(\'"0_sales_types_id_seq"\') NOT NULL,
            "sales_type" character(50) DEFAULT \'\' NOT NULL,
            "tax_included" integer DEFAULT 0 NOT NULL,
            "factor" double precision DEFAULT 1 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_sales_types_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_sales_types_sales_type_key" UNIQUE ("sales_type")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_salesman_salesman_code_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_salesman" (
            "salesman_code" integer DEFAULT nextval(\'"0_salesman_salesman_code_seq"\') NOT NULL,
            "salesman_name" character(60) DEFAULT \'\' NOT NULL,
            "salesman_phone" character(30) DEFAULT \'\' NOT NULL,
            "salesman_fax" character(30) DEFAULT \'\' NOT NULL,
            "salesman_email" character varying(100) DEFAULT \'\' NOT NULL,
            "provision" double precision DEFAULT 0 NOT NULL,
            "break_pt" double precision DEFAULT 0 NOT NULL,
            "provision2" double precision DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_salesman_pkey" PRIMARY KEY ("salesman_code"),
            CONSTRAINT "0_salesman_salesman_name_key" UNIQUE ("salesman_name")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_security_roles_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_security_roles" (
            "id" integer DEFAULT nextval(\'"0_security_roles_id_seq"\') NOT NULL,
            "role" character varying(30) NOT NULL,
            "description" character varying(50) DEFAULT NULL,
            "sections" text,
            "areas" text,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_security_roles_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_security_roles_role_key" UNIQUE ("role")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_shippers_shipper_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_shippers" (
            "shipper_id" integer DEFAULT nextval(\'"0_shippers_shipper_id_seq"\') NOT NULL,
            "shipper_name" character varying(60) DEFAULT \'\' NOT NULL,
            "phone" character varying(30) DEFAULT \'\' NOT NULL,
            "phone2" character varying(30) DEFAULT \'\' NOT NULL,
            "contact" text NOT NULL,
            "address" text NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_shippers_pkey" PRIMARY KEY ("shipper_id"),
            CONSTRAINT "0_shippers_shipper_name_key" UNIQUE ("shipper_name")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_som" (
            "code" character varying(15) NOT NULL,
            "area" character varying(100),
            CONSTRAINT "0_som_pkey" PRIMARY KEY ("code")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_sql_trail_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_sql_trail" (
            "id" integer DEFAULT nextval(\'"0_sql_trail_id_seq"\') NOT NULL,
            "sql" text NOT NULL,
            "result" smallint NOT NULL,
            "msg" character varying(255) NOT NULL,
            CONSTRAINT "0_sql_trail_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_stock_category_category_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_stock_category" (
            "category_id" integer DEFAULT nextval(\'"0_stock_category_category_id_seq"\') NOT NULL,
            "description" character varying(60) DEFAULT \'\' NOT NULL,
            "dflt_tax_type" integer DEFAULT 1 NOT NULL,
            "dflt_units" character varying(20) DEFAULT \'each\' NOT NULL,
            "dflt_mb_flag" character(1) DEFAULT \'B\' NOT NULL,
            "dflt_sales_act" character varying(15) DEFAULT \'\' NOT NULL,
            "dflt_cogs_act" character varying(15) DEFAULT \'\' NOT NULL,
            "dflt_inventory_act" character varying(15) DEFAULT \'\' NOT NULL,
            "dflt_adjustment_act" character varying(15) DEFAULT \'\' NOT NULL,
            "dflt_assembly_act" character varying(15) DEFAULT \'\' NOT NULL,
            "dflt_dim1" integer,
            "dflt_dim2" integer,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "dflt_no_sale" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_stock_category_description_key" UNIQUE ("description"),
            CONSTRAINT "0_stock_category_pkey" PRIMARY KEY ("category_id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_stock_master" (
            "stock_id" character varying(20) DEFAULT \'\' NOT NULL,
            "category_id" integer DEFAULT 0,
            "tax_type_id" integer DEFAULT 0 NOT NULL,
            "description" character varying(200) DEFAULT \'\' NOT NULL,
            "long_description" text NOT NULL,
            "units" character varying(20) DEFAULT \'each\' NOT NULL,
            "mb_flag" character(1) DEFAULT \'B\' NOT NULL,
            "sales_account" character varying(15) DEFAULT \'\' NOT NULL,
            "cogs_account" character varying(15) DEFAULT \'\' NOT NULL,
            "inventory_account" character varying(15) DEFAULT \'\' NOT NULL,
            "adjustment_account" character varying(15) DEFAULT \'\' NOT NULL,
            "assembly_account" character varying(15) DEFAULT \'\' NOT NULL,
            "dimension_id" integer DEFAULT 0,
            "dimension2_id" integer DEFAULT 0,
            "actual_cost" double precision DEFAULT 0 NOT NULL,
            "last_cost" double precision DEFAULT 0 NOT NULL,
            "material_cost" double precision DEFAULT 0 NOT NULL,
            "labour_cost" double precision DEFAULT 0 NOT NULL,
            "overhead_cost" double precision DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "no_sale" smallint DEFAULT 0 NOT NULL,
            "editable" smallint DEFAULT 0 NOT NULL,
            "jenis_item" integer DEFAULT 0,
            "bumun_cd" character varying(255),
            "l1_cd" character varying(255),
            "l2_cd" character varying(255),
            "l3_cd" character varying(255),
            "l4_cd" character varying(255),
            "margin" double precision DEFAULT 0,
            "moving_type" character varying(20),
            "ass_type_1" boolean DEFAULT false,
            "ass_type_2" boolean DEFAULT false,
            "ass_type_3" boolean DEFAULT false,
            "expiry_product" boolean DEFAULT false,
            "weight_product" boolean DEFAULT false,
            "batch_product" boolean DEFAULT false,
            "active_status" boolean DEFAULT false,
            "kode_1" character varying(255),
            "kode_2" character varying(100),
            "kode_3" character varying(100),
            "discount" double precision DEFAULT 0,
            "sales_discount_account" character varying(15),
            "supplied_by" character varying(100),
            "uom1_nm" character varying(255),
            "uom1_htype_1" boolean DEFAULT false,
            "uom1_htype_2" boolean DEFAULT false,
            "uom1_htype_3" boolean DEFAULT false,
            "uom1_htype_4" boolean DEFAULT false,
            "uom1_htype_5" boolean DEFAULT false,
            "uom1_rack_type" character varying(255),
            "uom1_shelfing_rack_type" character varying(255),
            "uom1_internal_barcode" character varying(255),
            "uom1_width" numeric(5,2) DEFAULT 0,
            "uom1_length" numeric(5,2) DEFAULT 0,
            "uom1_height" numeric(5,2) DEFAULT 0,
            "uom1_weight" numeric(7,2) DEFAULT 0,
            "uom1_margin" numeric(5,2) DEFAULT 0,
            "uom1_box_type" boolean DEFAULT false,
            "uom1_harga" double precision DEFAULT 0,
            "uom2_nm" character varying(255),
            "uom2_htype_1" boolean DEFAULT false,
            "uom2_htype_2" boolean DEFAULT false,
            "uom2_htype_3" boolean DEFAULT false,
            "uom2_htype_4" boolean DEFAULT false,
            "uom2_htype_5" boolean DEFAULT false,
            "uom2_rack_type" character varying(255),
            "uom2_shelfing_rack_type" character varying(255),
            "uom2_conversion" character varying(255),
            "uom2_internal_barcode" character varying(255),
            "uom2_width" numeric(5,2) DEFAULT 0,
            "uom2_length" numeric(5,2) DEFAULT 0,
            "uom2_height" numeric(5,2) DEFAULT 0,
            "uom2_weight" numeric(7,2) DEFAULT 0,
            "uom2_margin" numeric(5,2) DEFAULT 0,
            "uom2_box_type" boolean DEFAULT false,
            "uom2_harga" double precision DEFAULT 0,
            "uom3_nm" character varying(255),
            "uom3_htype_1" boolean DEFAULT false,
            "uom3_htype_2" boolean DEFAULT false,
            "uom3_htype_3" boolean DEFAULT false,
            "uom3_htype_4" boolean DEFAULT false,
            "uom3_htype_5" boolean DEFAULT false,
            "uom3_rack_type" character varying(255),
            "uom3_shelfing_rack_type" character varying(255),
            "uom3_conversion" character varying(255),
            "uom3_internal_barcode" character varying(255),
            "uom3_width" numeric(5,2) DEFAULT 0,
            "uom3_length" numeric(5,2) DEFAULT 0,
            "uom3_height" numeric(5,2) DEFAULT 0,
            "uom3_weight" numeric(7,2) DEFAULT 0,
            "uom3_margin" numeric(5,2) DEFAULT 0,
            "uom3_box_type" boolean DEFAULT false,
            "uom3_harga" double precision DEFAULT 0,
            "uom4_nm" character varying(255),
            "uom4_htype_1" boolean DEFAULT false,
            "uom4_htype_2" boolean DEFAULT false,
            "uom4_htype_3" boolean DEFAULT false,
            "uom4_htype_4" boolean DEFAULT false,
            "uom4_htype_5" boolean DEFAULT false,
            "uom4_rack_type" character varying(255),
            "uom4_shelfing_rack_type" character varying(255),
            "uom4_conversion" character varying(255),
            "uom4_internal_barcode" character varying(255),
            "uom4_width" numeric(5,2) DEFAULT 0,
            "uom4_length" numeric(5,2) DEFAULT 0,
            "uom4_height" numeric(5,2) DEFAULT 0,
            "uom4_weight" numeric(7,2) DEFAULT 0,
            "uom4_margin" numeric(5,2) DEFAULT 0,
            "uom4_box_type" boolean DEFAULT false,
            "uom4_harga" double precision DEFAULT 0,
            "uom1_prod_nm" character varying(255),
            "uom2_prod_nm" character varying(255),
            "uom3_prod_nm" character varying(255),
            "uom4_prod_nm" character varying(255),
            "asset_type_id_id" integer,
            "ppob" integer DEFAULT 0,
            CONSTRAINT "0_stock_master_pkey" PRIMARY KEY ("stock_id")
        ) WITH (oids = false)');

        DB::statement('CREATE INDEX "0_stock_master_stock_id_l1_cd_l2_cd_l3_cd_l4_cd_idx" ON "public"."0_stock_master" USING btree ("stock_id", "l1_cd", "l2_cd", "l3_cd", "l4_cd")');


        DB::statement('CREATE SEQUENCE "0_stock_master_barcode_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_stock_master_barcode" (
            "id" integer DEFAULT nextval(\'"0_stock_master_barcode_id_seq"\') NOT NULL,
            "stock_id" character varying(255),
            "barcode" character varying(255)
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_stock_master_detail_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_stock_master_detail" (
            "id" integer DEFAULT nextval(\'"0_stock_master_detail_id_seq"\') NOT NULL,
            "prod_cd" character varying(50),
            "ass_type" character varying(10)
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_stock_moves_trans_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_stock_moves" (
            "trans_id" integer DEFAULT nextval(\'"0_stock_moves_trans_id_seq"\') NOT NULL,
            "trans_no" integer DEFAULT 0 NOT NULL,
            "stock_id" character(20) DEFAULT \'\' NOT NULL,
            "type" smallint DEFAULT 0 NOT NULL,
            "loc_code" character(5) DEFAULT \'\' NOT NULL,
            "tran_date" date,
            "person_id" integer,
            "price" double precision DEFAULT 0 NOT NULL,
            "reference" character(40) DEFAULT \'\' NOT NULL,
            "qty" double precision DEFAULT 1 NOT NULL,
            "discount_percent" double precision DEFAULT 0 NOT NULL,
            "standard_cost" double precision DEFAULT 0 NOT NULL,
            "visible" smallint DEFAULT 1 NOT NULL,
            "tmuk" character varying(255),
            "keterangan" character varying(2044),
            "status_data" character varying(10),
            CONSTRAINT "0_stock_moves_pkey" PRIMARY KEY ("trans_id")
        ) WITH (oids = false)');

        DB::statement('CREATE INDEX "0_stock_moves_tran_date_stock_id_tmuk_idx" ON "public"."0_stock_moves" USING btree ("tran_date", "stock_id", "tmuk")');


        DB::statement('CREATE SEQUENCE "0_supp_allocations_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_supp_allocations" (
            "id" integer DEFAULT nextval(\'"0_supp_allocations_id_seq"\') NOT NULL,
            "amt" double precision,
            "date_alloc" date,
            "trans_no_from" integer,
            "trans_type_from" integer,
            "trans_no_to" integer,
            "trans_type_to" integer,
            CONSTRAINT "0_supp_allocations_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_supp_invoice_items_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_supp_invoice_items" (
            "id" integer DEFAULT nextval(\'"0_supp_invoice_items_id_seq"\') NOT NULL,
            "supp_trans_no" integer,
            "supp_trans_type" integer,
            "gl_code" character varying(15) DEFAULT \'\' NOT NULL,
            "grn_item_id" integer,
            "po_detail_item_id" integer,
            "stock_id" character varying(20) DEFAULT \'\' NOT NULL,
            "description" text,
            "quantity" double precision DEFAULT 0 NOT NULL,
            "unit_price" double precision DEFAULT 0 NOT NULL,
            "unit_tax" double precision DEFAULT 0 NOT NULL,
            "memo_" text,
            "inv_immpos_id" integer DEFAULT 0,
            "tmuk" character varying(2044),
            CONSTRAINT "0_supp_invoice_items_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_supp_trans" (
            "trans_no" integer DEFAULT 0 NOT NULL,
            "type" smallint DEFAULT 0 NOT NULL,
            "supplier_id" integer,
            "reference" text NOT NULL,
            "supp_reference" character varying(60) DEFAULT \'\' NOT NULL,
            "tran_date" date,
            "due_date" date,
            "ov_amount" double precision DEFAULT 0 NOT NULL,
            "ov_discount" double precision DEFAULT 0 NOT NULL,
            "ov_gst" double precision DEFAULT 0 NOT NULL,
            "rate" double precision DEFAULT 1 NOT NULL,
            "alloc" double precision DEFAULT 0 NOT NULL,
            "tax_included" smallint DEFAULT 0 NOT NULL,
            "tmuk" character varying(255),
            CONSTRAINT "0_supp_trans_pkey" PRIMARY KEY ("type", "trans_no")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_suppliers_supplier_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_suppliers" (
            "supplier_id" integer DEFAULT nextval(\'"0_suppliers_supplier_id_seq"\') NOT NULL,
            "supp_name" character varying(60) DEFAULT \'\' NOT NULL,
            "supp_ref" character varying(30) DEFAULT \'\' NOT NULL,
            "address" text NOT NULL,
            "supp_address" text NOT NULL,
            "gst_no" character varying(25) DEFAULT \'\' NOT NULL,
            "contact" character varying(60) DEFAULT \'\' NOT NULL,
            "supp_account_no" character varying(40) DEFAULT \'\' NOT NULL,
            "website" character varying(100) DEFAULT \'\' NOT NULL,
            "bank_account" character varying(60) DEFAULT \'\' NOT NULL,
            "curr_code" character(3) DEFAULT NULL,
            "payment_terms" integer,
            "tax_included" smallint DEFAULT (0) NOT NULL,
            "dimension_id" integer DEFAULT 0,
            "dimension2_id" integer DEFAULT 0,
            "tax_group_id" integer DEFAULT 0,
            "credit_limit" double precision DEFAULT (0) NOT NULL,
            "purchase_account" character varying(15) DEFAULT \'\' NOT NULL,
            "payable_account" character varying(15) DEFAULT \'\' NOT NULL,
            "payment_discount_account" character varying(15) DEFAULT \'\' NOT NULL,
            "notes" text DEFAULT \'\' NOT NULL,
            "inactive" smallint DEFAULT (0) NOT NULL,
            "supp_code" character varying(50),
            "supp_telp" character varying(15),
            "pic" character varying(100),
            "top_days" character varying(5),
            "order_mon" boolean,
            "order_tue" boolean,
            "order_wed" boolean,
            "order_thu" boolean,
            "order_fri" boolean,
            "order_sat" boolean,
            "order_sun" boolean,
            "lead_time" character varying(100),
            "npwp" character varying(20),
            "pkp" boolean DEFAULT false,
            "city" character varying(50),
            "id_som" character varying(30),
            "zip_code" character varying(20),
            "kelurahan" character varying(50),
            "provinsi" character varying(50),
            "gps_latitude" character varying(30),
            "gps_longitude" character varying(30),
            "kecamatan" character varying(50),
            "company_name" character varying(50),
            "company_address" character varying(50),
            "escrow_account" character varying(50),
            "tax_type_id" integer DEFAULT 1,
            "bank_tujuan" character varying(255),
            "for_tmuk" text DEFAULT NULL,
            CONSTRAINT "0_suppliers_pkey" PRIMARY KEY ("supplier_id")
        ) WITH (oids = false)');

        DB::statement('CREATE INDEX "0_suppliers_supplier_id_id_som_idx" ON "public"."0_suppliers" USING btree ("supplier_id", "id_som")');


        DB::statement('CREATE TABLE "public"."0_sys_prefs" (
            "name" character varying(35) DEFAULT \'\' NOT NULL,
            "category" character varying(30) DEFAULT NULL,
            "type" character varying(20) DEFAULT \'\' NOT NULL,
            "length" smallint,
            "value" text,
            CONSTRAINT "0_sys_prefs_pkey" PRIMARY KEY ("name")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_sys_types" (
            "type_id" smallint DEFAULT 0 NOT NULL,
            "type_no" integer DEFAULT 1 NOT NULL,
            "next_reference" character varying(100) DEFAULT \'\' NOT NULL,
            CONSTRAINT "0_sys_types_pkey" PRIMARY KEY ("type_id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_tag_associations" (
            "record_id" character varying(15) NOT NULL,
            "tag_id" integer NOT NULL,
            CONSTRAINT "0_tag_associations_record_id_tag_id_key" UNIQUE ("record_id", "tag_id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_tags_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_tags" (
            "id" integer DEFAULT nextval(\'"0_tags_id_seq"\') NOT NULL,
            "type" smallint NOT NULL,
            "name" character varying(30) NOT NULL,
            "description" character varying(60) DEFAULT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_tags_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_tags_type_name_key" UNIQUE ("type", "name")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_tax_group_items" (
            "tax_group_id" integer DEFAULT 0 NOT NULL,
            "tax_type_id" integer DEFAULT 0 NOT NULL,
            "rate" double precision DEFAULT 0 NOT NULL,
            CONSTRAINT "0_tax_group_items_pkey" PRIMARY KEY ("tax_group_id", "tax_type_id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_tax_groups_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_tax_groups" (
            "id" integer DEFAULT nextval(\'"0_tax_groups_id_seq"\') NOT NULL,
            "name" character varying(60) DEFAULT \'\' NOT NULL,
            "tax_shipping" smallint DEFAULT 0 NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_tax_groups_name_key" UNIQUE ("name"),
            CONSTRAINT "0_tax_groups_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_tax_types_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_tax_types" (
            "id" integer DEFAULT nextval(\'"0_tax_types_id_seq"\') NOT NULL,
            "rate" double precision DEFAULT 0 NOT NULL,
            "sales_gl_code" character varying(15) DEFAULT \'\' NOT NULL,
            "purchasing_gl_code" character varying(15) DEFAULT \'\' NOT NULL,
            "name" character varying(60) DEFAULT \'\' NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "is_ppn_masukan" smallint,
            "is_ppn_keluaran" smallint,
            CONSTRAINT "0_tax_types_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_trans_tax_details_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_trans_tax_details" (
            "id" integer DEFAULT nextval(\'"0_trans_tax_details_id_seq"\') NOT NULL,
            "trans_type" smallint,
            "trans_no" integer,
            "tran_date" date NOT NULL,
            "tax_type_id" integer DEFAULT 0 NOT NULL,
            "rate" double precision DEFAULT 0 NOT NULL,
            "ex_rate" double precision DEFAULT 1 NOT NULL,
            "included_in_price" smallint DEFAULT 0 NOT NULL,
            "net_amount" double precision DEFAULT 0 NOT NULL,
            "amount" double precision DEFAULT 0 NOT NULL,
            "memo" text,
            CONSTRAINT "0_trans_tax_details_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_truck_type_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_truck_type" (
            "id" integer DEFAULT nextval(\'"0_truck_type_id_seq"\') NOT NULL,
            "truck_type" character varying(100),
            "tinggi" double precision DEFAULT 0,
            "panjang" double precision DEFAULT 0,
            "lebar" double precision DEFAULT 0,
            "kapasitas" double precision DEFAULT 0,
            "volume" double precision DEFAULT 0
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_useronline_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_useronline" (
            "id" integer DEFAULT nextval(\'"0_useronline_id_seq"\') NOT NULL,
            "timestamp" integer DEFAULT 0 NOT NULL,
            "ip" character varying(40) DEFAULT \'\' NOT NULL,
            "file" character varying(100) DEFAULT \'\' NOT NULL,
            CONSTRAINT "0_useronline_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_users_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_users" (
            "id" integer DEFAULT nextval(\'"0_users_id_seq"\') NOT NULL,
            "user_id" character varying(60) DEFAULT \'\' NOT NULL,
            "password" character varying(100) DEFAULT \'\' NOT NULL,
            "real_name" character varying(100) DEFAULT \'\' NOT NULL,
            "role_id" integer DEFAULT 1 NOT NULL,
            "phone" character varying(30) DEFAULT \'\' NOT NULL,
            "email" character varying(100) DEFAULT NULL,
            "language" character varying(20) DEFAULT NULL,
            "date_format" smallint DEFAULT 0 NOT NULL,
            "date_sep" smallint DEFAULT 0 NOT NULL,
            "tho_sep" smallint DEFAULT 0 NOT NULL,
            "dec_sep" smallint DEFAULT 0 NOT NULL,
            "theme" character varying(20) DEFAULT \'default\' NOT NULL,
            "page_size" character varying(20) DEFAULT \'A4\' NOT NULL,
            "prices_dec" smallint DEFAULT 2 NOT NULL,
            "qty_dec" smallint DEFAULT 2 NOT NULL,
            "rates_dec" smallint DEFAULT 4 NOT NULL,
            "percent_dec" smallint DEFAULT 1 NOT NULL,
            "show_gl" smallint DEFAULT 1 NOT NULL,
            "show_codes" smallint DEFAULT 0 NOT NULL,
            "show_hints" smallint DEFAULT 0 NOT NULL,
            "last_visit_date" timestamp(6),
            "query_size" smallint DEFAULT 10 NOT NULL,
            "graphic_links" smallint DEFAULT 1,
            "pos" smallint DEFAULT 1,
            "print_profile" character varying(30) DEFAULT \'\' NOT NULL,
            "rep_popup" smallint DEFAULT 1,
            "sticky_doc_date" smallint DEFAULT 0,
            "startup_tab" character varying(20) DEFAULT \'\' NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            "location_user" character varying(2044),
            CONSTRAINT "0_users_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_users_user_id_key" UNIQUE ("user_id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_vendor_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_vendor" (
            "id" integer DEFAULT nextval(\'"0_vendor_id_seq"\') NOT NULL,
            "id_som" character varying(30),
            "store_code" character varying(50) NOT NULL,
            "store_name" character varying(100) NOT NULL,
            "address" character varying(100),
            "city" character varying(50),
            "kelurahan" character varying(50),
            "propinsi" character varying(50),
            "telepon" character varying(20),
            "zip_code" character varying(20),
            "kecamatan" character varying(50),
            "gps_latitude" character varying(30),
            "gps_longitude" character varying(30),
            "company_name" character varying(100),
            "tax_address" character varying(100),
            "npwp" character varying(20),
            "pkp" boolean DEFAULT false,
            "escrow_account" character varying(50),
            "accounts_payable_account" character varying(50),
            "purchase_account" character varying(50),
            "purchase_discount_account" character varying(50),
            "tax_included" smallint DEFAULT 0,
            CONSTRAINT "0_vendor_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE TABLE "public"."0_voided" (
            "type" integer DEFAULT 0 NOT NULL,
            "id" integer DEFAULT 0 NOT NULL,
            "date_" date,
            "memo_" text NOT NULL,
            CONSTRAINT "0_voided_type_id_key" UNIQUE ("type", "id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_wo_issue_items_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_wo_issue_items" (
            "id" integer DEFAULT nextval(\'"0_wo_issue_items_id_seq"\') NOT NULL,
            "stock_id" character varying(40) DEFAULT NULL,
            "issue_id" integer,
            "qty_issued" double precision,
            CONSTRAINT "0_wo_issue_items_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_wo_issues_issue_no_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_wo_issues" (
            "issue_no" integer DEFAULT nextval(\'"0_wo_issues_issue_no_seq"\') NOT NULL,
            "workorder_id" integer DEFAULT 0 NOT NULL,
            "reference" character varying(100) DEFAULT NULL,
            "issue_date" date,
            "loc_code" character varying(5) DEFAULT NULL,
            "workcentre_id" integer,
            CONSTRAINT "0_wo_issues_pkey" PRIMARY KEY ("issue_no")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_wo_manufacture_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_wo_manufacture" (
            "id" integer DEFAULT nextval(\'"0_wo_manufacture_id_seq"\') NOT NULL,
            "reference" character varying(100) DEFAULT NULL,
            "workorder_id" integer DEFAULT 0 NOT NULL,
            "quantity" double precision DEFAULT 0 NOT NULL,
            "date_" date,
            CONSTRAINT "0_wo_manufacture_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_wo_requirements_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_wo_requirements" (
            "id" integer DEFAULT nextval(\'"0_wo_requirements_id_seq"\') NOT NULL,
            "workorder_id" integer DEFAULT 0 NOT NULL,
            "stock_id" character(20) DEFAULT \'\' NOT NULL,
            "workcentre" integer DEFAULT 0 NOT NULL,
            "units_req" double precision DEFAULT 1 NOT NULL,
            "std_cost" double precision DEFAULT 0 NOT NULL,
            "loc_code" character(5) DEFAULT \'\' NOT NULL,
            "units_issued" double precision DEFAULT 0 NOT NULL,
            CONSTRAINT "0_wo_requirements_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_workcentres_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_workcentres" (
            "id" integer DEFAULT nextval(\'"0_workcentres_id_seq"\') NOT NULL,
            "name" character(40) DEFAULT \'\' NOT NULL,
            "description" character(50) DEFAULT \'\' NOT NULL,
            "inactive" smallint DEFAULT 0 NOT NULL,
            CONSTRAINT "0_workcentres_name_key" UNIQUE ("name"),
            CONSTRAINT "0_workcentres_pkey" PRIMARY KEY ("id")
        ) WITH (oids = false)');


        DB::statement('CREATE SEQUENCE "0_workorders_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1');

        DB::statement('CREATE TABLE "public"."0_workorders" (
            "id" integer DEFAULT nextval(\'"0_workorders_id_seq"\') NOT NULL,
            "wo_ref" character varying(60) DEFAULT \'\' NOT NULL,
            "loc_code" character varying(5) DEFAULT \'\' NOT NULL,
            "units_reqd" double precision DEFAULT 1 NOT NULL,
            "stock_id" character varying(20) DEFAULT \'\' NOT NULL,
            "date_" date,
            "type" smallint DEFAULT 0 NOT NULL,
            "required_by" date,
            "released_date" date,
            "units_issued" double precision DEFAULT 0 NOT NULL,
            "closed" smallint DEFAULT 0 NOT NULL,
            "released" smallint DEFAULT 0 NOT NULL,
            "additional_costs" double precision DEFAULT 0 NOT NULL,
            CONSTRAINT "0_workorders_pkey" PRIMARY KEY ("id"),
            CONSTRAINT "0_workorders_wo_ref_key" UNIQUE ("wo_ref")
        ) WITH (oids = false)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE IF EXISTS "0_amortisation"');
        DB::statement('DROP TABLE IF EXISTS "0_approval_escrow"');
        DB::statement('DROP TABLE IF EXISTS "0_areas"');
        DB::statement('DROP TABLE IF EXISTS "0_asset_types"');
        DB::statement('DROP TABLE IF EXISTS "0_asset_valuations"');
        DB::statement('DROP TABLE IF EXISTS "0_assets"');
        DB::statement('DROP TABLE IF EXISTS "0_assortment_type"');
        DB::statement('DROP TABLE IF EXISTS "0_attachments"');
        DB::statement('DROP TABLE IF EXISTS "0_audit_trail"');
        DB::statement('DROP TABLE IF EXISTS "0_bank"');
        DB::statement('DROP TABLE IF EXISTS "0_bank_account_topup"');
        DB::statement('DROP TABLE IF EXISTS "0_bank_accounts"');
        DB::statement('DROP TABLE IF EXISTS "0_bank_trans"');
        DB::statement('DROP TABLE IF EXISTS "0_bom"');
        DB::statement('DROP TABLE IF EXISTS "0_budget_trans"');
        DB::statement('DROP TABLE IF EXISTS "0_bumun"');
        DB::statement('DROP TABLE IF EXISTS "0_cat_1"');
        DB::statement('DROP TABLE IF EXISTS "0_cat_2"');
        DB::statement('DROP TABLE IF EXISTS "0_cat_3"');
        DB::statement('DROP TABLE IF EXISTS "0_cat_4"');
        DB::statement('DROP TABLE IF EXISTS "0_chart_class"');
        DB::statement('DROP TABLE IF EXISTS "0_chart_master"');
        DB::statement('DROP TABLE IF EXISTS "0_chart_types"');
        DB::statement('DROP TABLE IF EXISTS "0_comments"');
        DB::statement('DROP TABLE IF EXISTS "0_container_type"');
        DB::statement('DROP TABLE IF EXISTS "0_credit_status"');
        DB::statement('DROP TABLE IF EXISTS "0_crm_categories"');
        DB::statement('DROP TABLE IF EXISTS "0_crm_contacts"');
        DB::statement('DROP TABLE IF EXISTS "0_crm_persons"');
        DB::statement('DROP TABLE IF EXISTS "0_currencies"');
        DB::statement('DROP TABLE IF EXISTS "0_cust_allocations"');
        DB::statement('DROP TABLE IF EXISTS "0_cust_branch"');
        DB::statement('DROP TABLE IF EXISTS "0_debtor_trans"');
        DB::statement('DROP TABLE IF EXISTS "0_debtor_trans_details"');
        DB::statement('DROP TABLE IF EXISTS "0_debtors_master"');
        DB::statement('DROP TABLE IF EXISTS "0_dimensions"');
        DB::statement('DROP TABLE IF EXISTS "0_escrow_trans"');
        DB::statement('DROP TABLE IF EXISTS "0_exchange_rates"');
        DB::statement('DROP TABLE IF EXISTS "0_fiscal_year"');
        DB::statement('DROP TABLE IF EXISTS "0_gl_trans"');
        DB::statement('DROP TABLE IF EXISTS "0_grn_batch"');
        DB::statement('DROP TABLE IF EXISTS "0_grn_items"');
        DB::statement('DROP TABLE IF EXISTS "0_groups"');
        DB::statement('DROP TABLE IF EXISTS "0_imm_store"');
        DB::statement('DROP TABLE IF EXISTS "0_imm_store_pos"');
        DB::statement('DROP TABLE IF EXISTS "0_item_codes"');
        DB::statement('DROP TABLE IF EXISTS "0_item_tax_type_exemptions"');
        DB::statement('DROP TABLE IF EXISTS "0_item_tax_types"');
        DB::statement('DROP TABLE IF EXISTS "0_item_units"');
        DB::statement('DROP TABLE IF EXISTS "0_kubikasi_container"');
        DB::statement('DROP TABLE IF EXISTS "0_kubikasi_truck"');
        DB::statement('DROP TABLE IF EXISTS "0_loc_stock"');
        DB::statement('DROP TABLE IF EXISTS "0_locations"');
        DB::statement('DROP TABLE IF EXISTS "0_map_tmuk"');
        DB::statement('DROP TABLE IF EXISTS "0_movement_types"');
        DB::statement('DROP TABLE IF EXISTS "0_number_range"');
        DB::statement('DROP TABLE IF EXISTS "0_payment_request"');
        DB::statement('DROP TABLE IF EXISTS "0_payment_terms"');
        DB::statement('DROP TABLE IF EXISTS "0_picking_task"');
        DB::statement('DROP TABLE IF EXISTS "0_prices"');
        DB::statement('DROP TABLE IF EXISTS "0_print_profiles"');
        DB::statement('DROP TABLE IF EXISTS "0_printers"');
        DB::statement('DROP TABLE IF EXISTS "0_prod"');
        DB::statement('DROP TABLE IF EXISTS "0_promosi"');
        DB::statement('DROP TABLE IF EXISTS "0_promosi_barang"');
        DB::statement('DROP TABLE IF EXISTS "0_promosi_hadiah"');
        DB::statement('DROP TABLE IF EXISTS "0_promosi_location"');
        DB::statement('DROP TABLE IF EXISTS "0_promosi_pwp_detail"');
        DB::statement('DROP TABLE IF EXISTS "0_purch_data"');
        DB::statement('DROP TABLE IF EXISTS "0_purch_order_details"');
        DB::statement('DROP TABLE IF EXISTS "0_purch_orders"');
        DB::statement('DROP TABLE IF EXISTS "0_purch_request"');
        DB::statement('DROP TABLE IF EXISTS "0_purch_request_details"');
        DB::statement('DROP TABLE IF EXISTS "0_quick_entries"');
        DB::statement('DROP TABLE IF EXISTS "0_quick_entry_lines"');
        DB::statement('DROP TABLE IF EXISTS "0_rack_type"');
        DB::statement('DROP TABLE IF EXISTS "0_recurrent_invoices"');
        DB::statement('DROP TABLE IF EXISTS "0_reduce_escrow"');
        DB::statement('DROP TABLE IF EXISTS "0_refs"');
        DB::statement('DROP TABLE IF EXISTS "0_reorder_formula"');
        DB::statement('DROP TABLE IF EXISTS "0_retur_pembelian"');
        DB::statement('DROP TABLE IF EXISTS "0_retur_pembelian_detail"');
        DB::statement('DROP TABLE IF EXISTS "0_sales_order_details"');
        DB::statement('DROP TABLE IF EXISTS "0_sales_orders"');
        DB::statement('DROP TABLE IF EXISTS "0_sales_pos"');
        DB::statement('DROP TABLE IF EXISTS "0_sales_types"');
        DB::statement('DROP TABLE IF EXISTS "0_salesman"');
        DB::statement('DROP TABLE IF EXISTS "0_security_roles"');
        DB::statement('DROP TABLE IF EXISTS "0_shippers"');
        DB::statement('DROP TABLE IF EXISTS "0_som"');
        DB::statement('DROP TABLE IF EXISTS "0_sql_trail"');
        DB::statement('DROP TABLE IF EXISTS "0_stock_category"');
        DB::statement('DROP TABLE IF EXISTS "0_stock_master"');
        DB::statement('DROP TABLE IF EXISTS "0_stock_master_barcode"');
        DB::statement('DROP TABLE IF EXISTS "0_stock_master_detail"');
        DB::statement('DROP TABLE IF EXISTS "0_stock_moves"');
        DB::statement('DROP TABLE IF EXISTS "0_supp_allocations"');
        DB::statement('DROP TABLE IF EXISTS "0_supp_invoice_items"');
        DB::statement('DROP TABLE IF EXISTS "0_supp_trans"');
        DB::statement('DROP TABLE IF EXISTS "0_suppliers"');
        DB::statement('DROP TABLE IF EXISTS "0_sys_prefs"');
        DB::statement('DROP TABLE IF EXISTS "0_sys_types"');
        DB::statement('DROP TABLE IF EXISTS "0_tag_associations"');
        DB::statement('DROP TABLE IF EXISTS "0_tags"');
        DB::statement('DROP TABLE IF EXISTS "0_tax_group_items"');
        DB::statement('DROP TABLE IF EXISTS "0_tax_groups"');
        DB::statement('DROP TABLE IF EXISTS "0_tax_types"');
        DB::statement('DROP TABLE IF EXISTS "0_trans_tax_details"');
        DB::statement('DROP TABLE IF EXISTS "0_truck_type"');
        DB::statement('DROP TABLE IF EXISTS "0_useronline"');
        DB::statement('DROP TABLE IF EXISTS "0_users"');
        DB::statement('DROP TABLE IF EXISTS "0_vendor"');
        DB::statement('DROP TABLE IF EXISTS "0_voided"');
        DB::statement('DROP TABLE IF EXISTS "0_wo_issue_items"');
        DB::statement('DROP TABLE IF EXISTS "0_wo_issues"');
        DB::statement('DROP TABLE IF EXISTS "0_wo_manufacture"');
        DB::statement('DROP TABLE IF EXISTS "0_wo_requirements"');
        DB::statement('DROP TABLE IF EXISTS "0_workcentres"');
        DB::statement('DROP TABLE IF EXISTS "0_workorders"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_amortisation_amortisation_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_approval_escrow_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_areas_area_code_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_asset_types_asset_type_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_asset_valuations_asset_valuation_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_assets_asset_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_assortment_type_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_attachments_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_audit_trail_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_bank_bank_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_bank_accounts_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_bank_trans_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_bom_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_budget_trans_counter_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_bumun_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_cat_1_ID_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_cat_2_ID_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_cat_3_ID_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_cat_4_ID_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_container_type_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_credit_status_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_crm_categories_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_crm_contacts_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_crm_persons_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_cust_allocations_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_cust_branch_branch_code_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_debtor_trans_details_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_debtors_master_debtor_no_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_dimensions_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_escrow_trans_counter_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_exchange_rates_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_fiscal_year_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_gl_trans_counter_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_grn_batch_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_grn_items_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_groups_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_imm_store_pos_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_item_codes_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_item_tax_types_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "kubikasi_container_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "kubikasi_truck_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_movement_types_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_number_range_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_payment_request_counter_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_payment_terms_terms_indicator_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_picking_task_counter_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_prices_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_print_profiles_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_printers_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_promo_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_promo_barang_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_promo_hadiah_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_promosi_location_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_promo_pwp_detail_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_purch_order_details_po_detail_item_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_purch_orders_order_no_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_purch_request_request_no_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_purch_request_details_po_detail_item_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_quick_entries_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_quick_entry_lines_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_rack_type_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_recurrent_invoices_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_reduce_escrow_counter_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "reorder_formula_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_retur_pembelian_trans_no_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_retur_pembelian_detail_counter_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_sales_order_details_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_sales_pos_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_sales_types_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_salesman_salesman_code_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_security_roles_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_shippers_shipper_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_sql_trail_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_stock_category_category_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_stock_master_barcode_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_stock_master_detail_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_stock_moves_trans_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_supp_allocations_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_supp_invoice_items_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_suppliers_supplier_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_tags_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_tax_groups_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_tax_types_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_trans_tax_details_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_truck_type_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_useronline_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_users_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_vendor_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_wo_issue_items_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_wo_issues_issue_no_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_wo_manufacture_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_wo_requirements_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_workcentres_id_seq"');
        DB::statement('DROP SEQUENCE IF EXISTS "0_workorders_id_seq"');
    }
}
