<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableRefRekeningBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_rekening_bank', function (Blueprint $table) {
            $table->string('nomor_rekening', 20)->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_rekening_bank', function (Blueprint $table) {
            $table->dropColumn('nomor_rekening');
        });
    }
}
