<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatusRekeningToRefRekeningBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_rekening_bank', function (Blueprint $table) {
            $table->integer('status_rekening')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_rekening_bank', function (Blueprint $table) {
            $table->dropColumn('status_rekening');
        });
    }
}
