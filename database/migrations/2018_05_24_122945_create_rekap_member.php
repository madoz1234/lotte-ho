<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekapMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('trans_rekap_member_tmuk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('debtor_no', 10)->nullable();
            $table->string('br_name', 60)->nullable();
            $table->string('branch_ref', 30)->nullable();
            $table->text('br_address')->nullable();
            $table->string('contact_name',60)->nullable();
            $table->string('default_location',5)->nullable();
            $table->integer('tax_group_id')->nullable();
            $table->text('br_post_address')->nullable();
            $table->text('notes')->nullable();
            $table->integer('inactive')->nullable();
            $table->string('nomor_member')->nullable();
            $table->decimal('credit_limit',20,2)->nullable();
            $table->string('telepon')->nullable();
            $table->string('email')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('trans_rekap_member_tmuk');
        
    }
}
