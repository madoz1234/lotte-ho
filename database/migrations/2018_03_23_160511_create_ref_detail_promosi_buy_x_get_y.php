<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefDetailPromosiBuyXGetY extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_detail_promosi_buy_x_get_y', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('promosi_id')->unsigned();
            $table->string('kode_produk_x', 30);
            $table->string('nama_produk_x',255);
            $table->decimal('harga_jual_x', 10, 2);
            $table->float('diskon_x');
            $table->decimal('harga_diskon_x', 10, 2);
            $table->string('kode_produk_y', 30);
            $table->string('nama_produk_y', 255);
            $table->decimal('harga_jual_y', 10, 2);
            $table->float('diskon_y');
            $table->decimal('harga_diskon_y', 10, 2);
            $table->decimal('total_diskon', 10, 2);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('promosi_id')->references('id')->on('ref_promosi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_detail_promosi_buy_x_get_y');
    }
}
