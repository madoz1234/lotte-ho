<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableLogRefRakVersi2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_rak', function (Blueprint $table) {
            $table->string('tinggi_hanger', 10, 2)->nullable()->change();
            $table->string('panjang_hanger', 10, 2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_rak', function (Blueprint $table) {
            $table->decimal('tinggi_hanger', 10, 2)->change();
            $table->decimal('panjang_hanger', 10, 2)->change();
        });
    }
}
