<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefPointAndLogRefPoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_point', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tgl_berlaku');
            $table->decimal('konversi', 30, 2);
            $table->integer('status')->comment('1:aktif; 0:non-aktif');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('log_ref_point', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('point_id')->unsigned();
             $table->date('tgl_berlaku');
            $table->decimal('konversi', 30, 2);
            $table->integer('status')->comment('1:aktif; 0:non-aktif');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_point');

        Schema::drop('log_ref_point');
    }
}
