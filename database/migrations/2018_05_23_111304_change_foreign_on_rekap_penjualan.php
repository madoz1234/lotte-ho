<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignOnRekapPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rekap_penjualan', function (Blueprint $table) {
            $table->dropForeign('trans_rekap_penjualan_member_nomor_foreign');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::table('trans_rekap_penjualan_detail', function (Blueprint $table) {
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::table('trans_rekap_penjualan_bayar', function (Blueprint $table) {
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_rekap_penjualan', function (Blueprint $table) {
            $table->foreign('member_nomor')->references('nomor')->on('ref_membercard');
            $table->dropColumn('created_at');
            $table->dropColumn('created_by');
            $table->dropColumn('updated_by');
            $table->dropColumn('updated_at');
        });
        Schema::table('trans_rekap_penjualan_detail', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('created_by');
            $table->dropColumn('updated_by');
            $table->dropColumn('updated_at');
        });
        Schema::table('trans_rekap_penjualan_bayar', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('created_by');
            $table->dropColumn('updated_by');
            $table->dropColumn('updated_at');
        });
    }
}
