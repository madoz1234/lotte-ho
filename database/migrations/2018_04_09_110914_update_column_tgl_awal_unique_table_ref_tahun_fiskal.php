<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnTglAwalUniqueTableRefTahunFiskal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_tahun_fiskal', function (Blueprint $table) {
            $table->date('tgl_awal')->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_tahun_fiskal', function (Blueprint $table) {
            $table->dropColumn('tgl_awal');
        });
    }
}
