<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableLogKustomerColumnLimitKreditKesatu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_kustomer', function (Blueprint $table) {
            $table->decimal('limit_kredit', 20, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_kustomer', function (Blueprint $table) {
            $table->dropColumn('limit_kredit', 10, 2);
        });
    }
}
