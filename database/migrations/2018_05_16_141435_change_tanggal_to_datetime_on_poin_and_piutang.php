<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTanggalToDatetimeOnPoinAndPiutang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_member_point_detail', function (Blueprint $table) {
            $table->datetime('tanggal')->change();
        });
        Schema::table('trans_member_piutang_detail', function (Blueprint $table) {
            $table->datetime('tanggal')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_member_point_detail', function (Blueprint $table) {
            $table->date('tanggal')->change();
        });
        Schema::table('trans_member_piutang_detail', function (Blueprint $table) {
            $table->date('tanggal')->change();
        });
    }
}
