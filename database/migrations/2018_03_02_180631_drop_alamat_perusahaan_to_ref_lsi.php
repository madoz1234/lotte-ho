<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAlamatPerusahaanToRefLsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_lsi', function (Blueprint $table) {
            $table->dropColumn('alamat_perusahaan');
            $table->dropColumn('kode_bank');
            $table->dropColumn('nomor_rekening');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_lsi', function (Blueprint $table) {
            $table->integer('alamat_perusahaan');
            $table->integer('kode_bank');
            $table->integer('nomor_rekening');
        });
    }
}
