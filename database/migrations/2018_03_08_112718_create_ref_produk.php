<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_produk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 30)->unique();
            $table->string('nama', 255);
            $table->string('barcode', 255)->nullable();

            $table->integer('stok_gmd')->nullable();
            $table->string('bblm_flag', 1)->comment('1:ya; 0:tidak')->nullable();
            $table->integer('con1')->comment('1st Level Min Purchase')->nullable();
            $table->decimal('dc1')->comment('1st Level Disc Price')->nullable();
            $table->integer('con2')->comment('2nd Level Min Purchase')->nullable();
            $table->decimal('dc2')->comment('2nd Level Disc Price')->nullable();
            $table->integer('con3')->comment('3rd Level Min Purchase')->nullable();
            $table->decimal('dc3')->comment('3rd Level Disc Price')->nullable();
            $table->integer('limit')->comment('Quantity limit for BBLM')->nullable();
            $table->integer('ea_qty')->comment('Quantity of content')->nullable();
            $table->integer('sale_tax_rt')->comment('Tax')->nullable();
            $table->float('width')->comment('Width of Product')->nullable();
            $table->float('length')->comment('Length of Product')->nullable();
            $table->float('height')->comment('Height of Product')->nullable();
            $table->float('wg')->comment('Weight of Product')->nullable();
            $table->float('inner_width')->comment('Inner Width of Product')->nullable();
            $table->float('inner_length')->comment('Inner Length of Product')->nullable();
            $table->float('inner_height')->comment('Inner Height of Product')->nullable();
            $table->float('inner_wg')->comment('Inner Weight of Product')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_produk');
    }
}
