<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRefBankEscrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ref_bank_escrow', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kota_id')->unsigned();
            $table->integer('bank_escrow_id')->unsigned();
            $table->string('nama', 30);
            $table->text('alamat');
            $table->string('kode_pos', 5);
            $table->string('telepon', 15);
            $table->string('email', 50);
            $table->date('tgl_mulai');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_ref_bank_escrow');
    }
}
