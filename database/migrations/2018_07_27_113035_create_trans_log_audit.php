<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransLogAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_log_audit', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('tanggal');
            $table->integer('user_id')->unsigned();
            $table->date('tanggal_transaksi');
            $table->string('type', 150)->nullable();
            $table->string('ref', 150)->nullable();
            $table->string('aksi', 150)->nullable();
            $table->double('amount')->nullable();

            $table->foreign('user_id')->references('id')->on('sys_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_log_audit');
    }
}
