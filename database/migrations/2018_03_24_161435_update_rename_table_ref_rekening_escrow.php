<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRenameTableRefRekeningEscrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('ref_rekening_escrow', 'ref_rekening_bank');

        Schema::rename('log_ref_rekening_escrow', 'log_ref_rekening_bank');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('ref_rekening_bank', 'ref_rekening_escrow');

        Schema::rename('log_ref_rekening_bank', 'log_ref_rekening_escrow');
    }
}
