<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRefMembercard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ref_membercard', function (Blueprint $table) {
            $table->integer('membercard_id')->unsigned();
            $table->increments('id');
            $table->string('nomor', 20);
            $table->string('nama', 30);
            $table->string('telepon', 15);
            $table->string('email', 30);
            $table->integer('jeniskustomer_id')->unsigned();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_ref_membercard');
    }
}
