<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignTransOpening extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_opening', function (Blueprint $table) {
            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_opening', function (Blueprint $table) {
            $table->dropForeign(['tmuk_kode']);
        });
    }
}
