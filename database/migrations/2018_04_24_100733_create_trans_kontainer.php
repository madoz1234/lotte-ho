<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKontainer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_kontainer', function (Blueprint $table) {
            $table->increments('id');

            $table->string('po_nomor', 30);
            $table->string('tmuk_kode', 10);

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('po_nomor')->references('nomor')->on('trans_po');
            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');

        });

        Schema::create('trans_truk', function (Blueprint $table) {
            $table->increments('id');

            $table->string('po_nomor', 30);
            $table->string('tmuk_kode', 10);

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('po_nomor')->references('nomor')->on('trans_po');
            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');

        });
        
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_kontainer');
        Schema::drop('trans_truk');
    }
}
