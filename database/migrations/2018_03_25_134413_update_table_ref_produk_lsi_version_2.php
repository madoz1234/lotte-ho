<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableRefProdukLsiVersion2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk_lsi', function ($table) {
            $table->dropColumn('ea_qty');
            $table->dropColumn('sale_tax_rt');

            $table->string('status', 50)->after('limit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk_lsi', function ($table) {
            $table->integer('ea_qty')->nullable();
            $table->integer('sale_tax_rt')->nullable();

            $table->dropColumn('status', 50)->after('limit')->nullable();
        });
    }
}
