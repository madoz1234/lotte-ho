<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableRefKustomerDeleteColumnKelurahanId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kustomer', function (Blueprint $table) {
            $table->dropColumn('kelurahan_id');
        });

        Schema::table('log_ref_kustomer', function (Blueprint $table) {
            $table->dropColumn('kelurahan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kustomer', function (Blueprint $table) {
            $table->integer('kelurahan_id');
        });

        Schema::table('log_ref_kustomer', function (Blueprint $table) {
            $table->integer('kelurahan_id');
        });
    }
}
