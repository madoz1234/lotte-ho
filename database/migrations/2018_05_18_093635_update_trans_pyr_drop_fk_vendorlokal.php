<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPyrDropFkVendorlokal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pyr', function (Blueprint $table) {
            $table->dropForeign(['vendor_lokal']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('trans_pyr', function (Blueprint $table) {
            $table->foreign('vendor_lokal')->references('id')->on('ref_vendor_tmuk');
        });
    }
}
