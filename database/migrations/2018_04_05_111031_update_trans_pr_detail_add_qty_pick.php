<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPrDetailAddQtyPick extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pr_detail', function (Blueprint $table) {
            $table->integer('qty_pick')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pr_detail', function (Blueprint $table) {
            $table->dropColumn('qty_pick');
        });
    }
}
