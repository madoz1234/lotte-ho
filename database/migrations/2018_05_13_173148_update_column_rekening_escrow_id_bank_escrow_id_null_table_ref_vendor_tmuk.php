<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnRekeningEscrowIdBankEscrowIdNullTableRefVendorTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->integer('rekening_escrow_id')->unsigned()->nullable()->change();
            $table->integer('bank_escrow_id')->unsigned()->nullable()->change();
        });

        Schema::table('log_ref_vendor_tmuk', function (Blueprint $table) {
            $table->integer('rekening_escrow_id')->unsigned()->nullable()->change();
            $table->integer('bank_escrow_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->dropColumn('rekening_escrow_id');
            $table->dropColumn('bank_escrow_id');
        });

        Schema::table('log_ref_vendor_tmuk', function (Blueprint $table) {
            $table->dropColumn('rekening_escrow_id');
            $table->dropColumn('bank_escrow_id');
        });
    }
}
