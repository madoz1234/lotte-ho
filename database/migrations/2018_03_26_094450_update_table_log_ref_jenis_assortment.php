<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableLogRefJenisAssortment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_jenis_assortment', function (Blueprint $table) {
            $table->dropUnique('log_ref_jenis_assortment_nama_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_jenis_assortment', function (Blueprint $table) {
            $table->dropColumn('nama');
        });
    }
}
