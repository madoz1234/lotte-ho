<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypeFlagTableTransSo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_so', function (Blueprint $table) {
            $table->string('type', 25)->nullable();
            $table->string('flag', 1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_so', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('flag');
        });
    }
}
