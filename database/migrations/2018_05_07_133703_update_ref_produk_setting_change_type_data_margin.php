<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefProdukSettingChangeTypeDataMargin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk_setting', function (Blueprint $table) {
            $table->decimal('margin', 25,20)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk_setting', function (Blueprint $table) {
            $table->float('margin', 1)->nullable()->change();
        });
    }
}
