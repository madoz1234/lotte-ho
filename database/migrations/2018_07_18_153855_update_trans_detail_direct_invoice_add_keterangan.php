<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransDetailDirectInvoiceAddKeterangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_detail_direct_invoice', function (Blueprint $table) {
            $table->integer('keterangan')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_detail_direct_invoice', function (Blueprint $table) {
            $table->dropColumn('keterangan');
        });
    }
}