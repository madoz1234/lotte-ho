<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAlamatNpwpTableRefPajak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_pajak', function (Blueprint $table) {
            $table->text('alamat_npwp')->nullable()->after('nama');
        });

        Schema::table('log_ref_pajak', function (Blueprint $table) {
            $table->text('alamat_npwp')->nullable()->after('nama');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_pajak', function (Blueprint $table) {
            $table->dropColumn('alamat_npwp');
        });

        Schema::table('log_ref_pajak', function (Blueprint $table) {
            $table->dropColumn('alamat_npwp');
        });
    }
}
