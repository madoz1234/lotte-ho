<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullToKodePos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->string('kode_pos', 5)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->string('kode_pos', 5)->change();
        });
    }
}
