<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransPrDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pr_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nomor_pr', 30);
            $table->string('produk_kode', 30);
            $table->string('deskripsi', 100)->nullable();
            $table->integer('qty_order')->nullable();
            $table->string('unit_order', 10)->nullable();
            $table->decimal('unit_order_price', 20, 2)->nullable();
            $table->integer('qty_sell')->nullable();
            $table->string('unit_sell', 10)->nullable();
            $table->integer('qty_pr')->nullable();
            $table->string('unit_pr', 10)->nullable();
            $table->decimal('total_price', 20, 2)->nullable();
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('nomor_pr')->references('nomor_pr')->on('trans_pr');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_pr_detail');
    }
}
