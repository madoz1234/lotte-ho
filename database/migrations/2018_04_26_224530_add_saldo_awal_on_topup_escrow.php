<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSaldoAwalOnTopupEscrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_topup_escrow', function (Blueprint $table) {
            $table->decimal('saldo_awal', 20, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_topup_escrow', function (Blueprint $table) {
            $table->dropColumn('saldo_awal');
        });
    }
}
