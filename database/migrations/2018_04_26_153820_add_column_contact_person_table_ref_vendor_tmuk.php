<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnContactPersonTableRefVendorTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->string('contact_person', 30)->nullable();
        });

        Schema::table('log_ref_vendor_tmuk', function (Blueprint $table) {
            $table->string('contact_person', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_vendor_tmuk', function (Blueprint $table) {
            $table->dropColumn('contact_person');
        });

        Schema::table('log_ref_vendor_tmuk', function (Blueprint $table) {
            $table->dropColumn('contact_person');
        });
    }
}
