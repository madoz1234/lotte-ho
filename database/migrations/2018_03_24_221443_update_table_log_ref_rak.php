<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableLogRefRak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_rak', function (Blueprint $table) {
            $table->dropUnique('log_ref_rak_tipe_rak_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_rak', function (Blueprint $table) {
            $table->dropColumn('tipe_rak');
        });
    }
}
