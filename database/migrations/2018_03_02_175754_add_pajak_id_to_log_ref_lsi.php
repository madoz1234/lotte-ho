<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPajakIdToLogRefLsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_lsi', function (Blueprint $table) {
            $table->integer('pajak_id')->unsigned();
            $table->dropColumn('alamat_perusahaan');
            $table->dropColumn('kode_bank');
            $table->dropColumn('nomor_rekening');

            $table->foreign('pajak_id')->references('id')->on('ref_pajak');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_lsi', function (Blueprint $table) {
            $table->integer('alamat_perusahaan');
            $table->integer('kode_bank');
            $table->integer('nomor_rekening');
        });
    }
}
