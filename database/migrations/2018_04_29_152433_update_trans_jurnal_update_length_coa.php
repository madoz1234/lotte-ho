<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransJurnalUpdateLengthCoa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_jurnal', function (Blueprint $table) {
            $table->string('coa_kode', 30)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_jurnal', function (Blueprint $table) {
            $table->string('coa_kode', 10)->change();
        });
    }
}
