<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPajakIdToLogRefTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_tmuk', function (Blueprint $table) {
            $table->integer('pajak_id')->nullable()->unsigned();

            $table->foreign('pajak_id')->references('id')->on('ref_pajak');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_tmuk', function (Blueprint $table) {
            //
        });
    }
}
