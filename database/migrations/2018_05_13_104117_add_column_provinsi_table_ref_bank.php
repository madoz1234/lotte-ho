<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnProvinsiTableRefBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_bank', function (Blueprint $table) {
            $table->integer('provinsi_id')->unsigned()->nullable();

            $table->foreign('provinsi_id')->references('id')->on('ref_provinsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_bank', function (Blueprint $table) {
            $table->dropColumn('provinsi_id');
        });
    }
}
