<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransReduceEscrowChangeDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->dateTime('verifikasi1_date')->nullable()->change();
            $table->dateTime('verifikasi2_date')->nullable()->change();
            $table->dateTime('verifikasi3_date')->nullable()->change();
            $table->dateTime('verifikasi4_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->date('verifikasi1_date')->nullable()->change();
            $table->date('verifikasi2_date')->nullable()->change();
            $table->date('verifikasi3_date')->nullable()->change();
            $table->date('verifikasi4_date')->nullable()->change();
        });
    }
}
