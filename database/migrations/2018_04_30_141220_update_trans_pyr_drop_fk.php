<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPyrDropFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pyr', function (Blueprint $table) {
            $table->dropForeign(['tipe']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pyr', function (Blueprint $table) {
            $table->foreign('tipe')->references('kode')->on('ref_tipebarang');
        });
    }
}