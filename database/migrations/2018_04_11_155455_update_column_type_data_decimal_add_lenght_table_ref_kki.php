<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnTypeDataDecimalAddLenghtTableRefKki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kki', function (Blueprint $table) {

            $table->decimal('i_satu', 30, 2)->default(0)->change();
            $table->decimal('i_dua', 30, 2)->default(0)->change();
            $table->decimal('i_tiga_a', 30, 2)->default(0)->change();
            $table->decimal('i_tiga_b', 30, 2)->default(0)->change();
            $table->decimal('i_tiga_c', 30, 2)->default(0)->change();
            $table->decimal('i_tiga_d', 30, 2)->default(0)->change();
            $table->decimal('i_tiga_e', 30, 2)->default(0)->change();
            $table->decimal('i_tiga_f', 30, 2)->default(0)->change();
            $table->decimal('i_empat_a', 30, 2)->default(0)->change();
            $table->decimal('i_empat_b', 30, 2)->default(0)->change();
            $table->decimal('i_empat_c', 30, 2)->default(0)->change();
            $table->decimal('i_lima_a', 30, 2)->default(0)->change();
            $table->decimal('i_lima_b', 30, 2)->default(0)->change();
            $table->decimal('i_sub_total', 30, 2)->default(0)->change();
            $table->decimal('i_sewa_ruko', 30, 2)->default(0)->change();
            $table->decimal('i_total_inves1_1', 30, 2)->default(0)->change();
            $table->decimal('i_ppn', 30, 2)->default(0)->change();
            $table->decimal('i_total_inves1_2', 30, 2)->default(0)->change();

            $table->decimal('ii_struk_terburuk', 30, 2)->default(0)->change();
            $table->decimal('ii_average_terburuk', 30, 2)->default(0)->change();
            $table->decimal('ii_sales_terburuk', 30, 2)->default(0)->change();
            $table->decimal('ii_penjualan_terburuk', 30, 2)->default(0)->change();
            $table->decimal('ii_struk_normal', 30, 2)->default(0)->change();
            $table->decimal('ii_average_normal', 30, 2)->default(0)->change();
            $table->decimal('ii_sales_normal', 30, 2)->default(0)->change();
            $table->decimal('ii_penjualan_normal', 30, 2)->default(0)->change();
            $table->decimal('ii_struk_terbaik', 30, 2)->default(0)->change();
            $table->decimal('ii_average_terbaik', 30, 2)->default(0)->change();
            $table->decimal('ii_sales_terbaik', 30, 2)->default(0)->change();
            $table->decimal('ii_penjualan_terbaik', 30, 2)->default(0)->change();

            $table->decimal('iii_labakotor_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iii_pendapatanlain2_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iii_pendapatansewa_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iii_total_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iii_labakotor_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iii_pendapatanlain2_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iii_pendapatansewa_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iii_total_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iii_labakotor_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iii_pendapatanlain2_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iii_pendapatansewa_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iii_total_estimasi3', 30, 2)->default(0)->change();

            $table->decimal('iv_satu_a_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_b_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_c_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_d_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_e_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_f_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_g_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_h_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_i_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_dua_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_tiga_a_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_tiga_b_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_a_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_b_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_c_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_d_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_total_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_a_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_b_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_c_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_d_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_e_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_f_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_g_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_h_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_i_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_dua_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_tiga_a_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_tiga_b_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_a_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_b_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_c_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_d_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_total_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_a_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_b_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_c_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_d_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_e_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_f_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_g_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_h_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_satu_i_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_dua_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_tiga_a_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_tiga_b_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_a_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_b_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_c_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_empat_d_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('iv_total_estimasi3', 30, 2)->default(0)->change();

            $table->decimal('v_satu_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('v_dua_estimasi1', 30, 2)->default(0)->change();
            $table->decimal('v_satu_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('v_dua_estimasi2', 30, 2)->default(0)->change();
            $table->decimal('v_satu_estimasi3', 30, 2)->default(0)->change();
            $table->decimal('v_dua_estimasi3', 30, 2)->default(0)->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->dropColumn('i_satu');
            $table->dropColumn('i_dua');
            $table->dropColumn('i_tiga_a');
            $table->dropColumn('i_tiga_b');
            $table->dropColumn('i_tiga_c');
            $table->dropColumn('i_tiga_d');
            $table->dropColumn('i_tiga_e');
            $table->dropColumn('i_tiga_f');
            $table->dropColumn('i_empat_a');
            $table->dropColumn('i_empat_b');
            $table->dropColumn('i_empat_c');
            $table->dropColumn('i_lima_a');
            $table->dropColumn('i_lima_b');
            $table->dropColumn('i_sub_total');
            $table->dropColumn('i_sewa_ruko');
            $table->dropColumn('i_total_inves1_1');
            $table->dropColumn('i_ppn');
            $table->dropColumn('i_total_inves1_2');

            $table->dropColumn('ii_struk_terburuk');
            $table->dropColumn('ii_average_terburuk');
            $table->dropColumn('ii_sales_terburuk');
            $table->dropColumn('ii_penjualan_terburuk');
            $table->dropColumn('ii_struk_normal');
            $table->dropColumn('ii_average_normal');
            $table->dropColumn('ii_sales_normal');
            $table->dropColumn('ii_penjualan_normal');
            $table->dropColumn('ii_struk_terbaik');
            $table->dropColumn('ii_average_terbaik');
            $table->dropColumn('ii_sales_terbaik');
            $table->dropColumn('ii_penjualan_terbaik');

            $table->dropColumn('iii_labakotor_estimasi1');
            $table->dropColumn('iii_pendapatanlain2_estimasi1');
            $table->dropColumn('iii_pendapatansewa_estimasi1');
            $table->dropColumn('iii_total_estimasi1');
            $table->dropColumn('iii_labakotor_estimasi2');
            $table->dropColumn('iii_pendapatanlain2_estimasi2');
            $table->dropColumn('iii_pendapatansewa_estimasi2');
            $table->dropColumn('iii_total_estimasi2');
            $table->dropColumn('iii_labakotor_estimasi3');
            $table->dropColumn('iii_pendapatanlain2_estimasi3');
            $table->dropColumn('iii_pendapatansewa_estimasi3');
            $table->dropColumn('iii_total_estimasi3');

            $table->dropColumn('iv_satu_a_estimasi1');
            $table->dropColumn('iv_satu_b_estimasi1');
            $table->dropColumn('iv_satu_c_estimasi1');
            $table->dropColumn('iv_satu_d_estimasi1');
            $table->dropColumn('iv_satu_e_estimasi1');
            $table->dropColumn('iv_satu_f_estimasi1');
            $table->dropColumn('iv_satu_g_estimasi1');
            $table->dropColumn('iv_satu_h_estimasi1');
            $table->dropColumn('iv_satu_i_estimasi1');
            $table->dropColumn('iv_dua_estimasi1');
            $table->dropColumn('iv_tiga_a_estimasi1');
            $table->dropColumn('iv_tiga_b_estimasi1');
            $table->dropColumn('iv_empat_a_estimasi1');
            $table->dropColumn('iv_empat_b_estimasi1');
            $table->dropColumn('iv_empat_c_estimasi1');
            $table->dropColumn('iv_empat_d_estimasi1');
            $table->dropColumn('iv_total_estimasi1');
            $table->dropColumn('iv_satu_a_estimasi2');
            $table->dropColumn('iv_satu_b_estimasi2');
            $table->dropColumn('iv_satu_c_estimasi2');
            $table->dropColumn('iv_satu_d_estimasi2');
            $table->dropColumn('iv_satu_e_estimasi2');
            $table->dropColumn('iv_satu_f_estimasi2');
            $table->dropColumn('iv_satu_g_estimasi2');
            $table->dropColumn('iv_satu_h_estimasi2');
            $table->dropColumn('iv_satu_i_estimasi2');
            $table->dropColumn('iv_dua_estimasi2');
            $table->dropColumn('iv_tiga_a_estimasi2');
            $table->dropColumn('iv_tiga_b_estimasi2');
            $table->dropColumn('iv_empat_a_estimasi2');
            $table->dropColumn('iv_empat_b_estimasi2');
            $table->dropColumn('iv_empat_c_estimasi2');
            $table->dropColumn('iv_empat_d_estimasi2');
            $table->dropColumn('iv_total_estimasi2');
            $table->dropColumn('iv_satu_a_estimasi3');
            $table->dropColumn('iv_satu_b_estimasi3');
            $table->dropColumn('iv_satu_c_estimasi3');
            $table->dropColumn('iv_satu_d_estimasi3');
            $table->dropColumn('iv_satu_e_estimasi3');
            $table->dropColumn('iv_satu_f_estimasi3');
            $table->dropColumn('iv_satu_g_estimasi3');
            $table->dropColumn('iv_satu_h_estimasi3');
            $table->dropColumn('iv_satu_i_estimasi3');
            $table->dropColumn('iv_dua_estimasi3');
            $table->dropColumn('iv_tiga_a_estimasi3');
            $table->dropColumn('iv_tiga_b_estimasi3');
            $table->dropColumn('iv_empat_a_estimasi3');
            $table->dropColumn('iv_empat_b_estimasi3');
            $table->dropColumn('iv_empat_c_estimasi3');
            $table->dropColumn('iv_empat_d_estimasi3');
            $table->dropColumn('iv_total_estimasi3');

            $table->dropColumn('v_satu_estimasi1');
            $table->dropColumn('v_dua_estimasi1');
            $table->dropColumn('v_satu_estimasi2');
            $table->dropColumn('v_dua_estimasi2');
            $table->dropColumn('v_satu_estimasi3');
            $table->dropColumn('v_dua_estimasi3');
        });
    }
}
