<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransHppMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_hpp_map', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('tanggal');
            $table->string('po_nomor', 30);
            $table->string('produk_kode', 30);
            $table->string('tmuk_kode', 10);
            
            $table->integer('qty');
            $table->decimal('price',20,2);
            $table->decimal('map',20,2);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('po_nomor')->references('nomor')->on('trans_po');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_hpp_map');
    }
}
