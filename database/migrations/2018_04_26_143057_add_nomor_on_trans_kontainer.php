<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNomorOnTransKontainer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('trans_kontainer', function (Blueprint $table) {
            $table->string('nomor',30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_kontainer', function (Blueprint $table) {
            $table->dropColumn('nomor');
        });
    }
}
