<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransDetailOpening extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_detail_opening', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('opening_id')->unsigned();
            $table->string('produk_kode', 30)->unsigned();
            $table->integer('qty_po')->nullable();
            $table->string('unit_po', 5)->nullable();
            $table->string('unit_price', 5)->nullable();
            $table->decimal('price', 10, 2)->nullable();

            $table->foreign('opening_id')->references('id')->on('trans_opening');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_detail_opening');
    }
}
