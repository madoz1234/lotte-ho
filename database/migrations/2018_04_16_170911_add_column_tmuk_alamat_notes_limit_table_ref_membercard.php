<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTmukAlamatNotesLimitTableRefMembercard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_membercard', function (Blueprint $table) {
            $table->integer('tmuk_id')->unsigned()->nullable();
            $table->text('alamat')->nullable();
            $table->text('notes')->nullable();
            $table->decimal('limit_kredit', 30, 2)->nullable();

            $table->foreign('tmuk_id')->references('id')->on('ref_tmuk');
        });

        Schema::table('log_ref_membercard', function (Blueprint $table) {
            $table->integer('tmuk_id')->unsigned()->nullable();
            $table->text('alamat')->nullable();
            $table->text('notes')->nullable();
            $table->decimal('limit_kredit', 30, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_membercard', function (Blueprint $table) {
            $table->dropColumn('tmuk_id');
            $table->dropColumn('alamat');
            $table->dropColumn('notes');
            $table->dropColumn('limit_kredit');
        });

        Schema::table('log_ref_membercard', function (Blueprint $table) {
            $table->dropColumn('tmuk_id');
            $table->dropColumn('alamat');
            $table->dropColumn('notes');
            $table->dropColumn('limit_kredit');
        });
    }
}
