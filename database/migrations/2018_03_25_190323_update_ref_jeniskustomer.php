<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefJeniskustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_jeniskustomer', function (Blueprint $table) {
            $table->string('jenis', 30)->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_jeniskustomer', function (Blueprint $table) {
            $table->dropColumn('jenis');
        });
    }
}
