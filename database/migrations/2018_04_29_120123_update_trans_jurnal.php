<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransJurnal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_jurnal', function (Blueprint $table) {
            $table->dropColumn('debit');
            $table->dropColumn('kredit');

            $table->string('idtrans', 30)->nullable();
            $table->decimal('jumlah', 20, 2)->nullable();
            $table->string('posisi', 1)->nullable();
            $table->string('flag', 1)->nullable();
            $table->tinyInteger('delete')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_jurnal', function (Blueprint $table) {
            $table->decimal('debit', 20, 2)->nullable();
            $table->decimal('kredit', 20, 2)->nullable();

            $table->dropColumn('idtrans');
            $table->dropColumn('jumlah');
            $table->dropColumn('posisi');
            $table->dropColumn('flag');
            $table->dropColumn('delete');
        });
    }
}
