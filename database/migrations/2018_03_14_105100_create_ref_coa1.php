<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefCoa1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_coa1', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tipe_coa_id')->unsigned();
            $table->string('kode', 10)->unique();
            $table->string('nama', 255);


            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tipe_coa_id')->references('id')->on('ref_tipe_coa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_coa1');
    }
}
