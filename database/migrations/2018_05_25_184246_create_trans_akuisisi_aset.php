<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransAkuisisiAset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_akuisisi_aset', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tmuk_kode', 10)->unsigned()->nullable();
            $table->integer('tipe_id')->unsigned();
            $table->string('nama', 100)->nullable();
            $table->string('no_seri', 100)->nullable();
            $table->date('tanggal_pembelian')->nullable();
            $table->decimal('nilai_pembelian', 30,2)->nullable();
            $table->integer('kondisi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
            $table->foreign('tipe_id')->references('id')->on('ref_tipe_aset');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_akuisisi_aset');
    }
}
