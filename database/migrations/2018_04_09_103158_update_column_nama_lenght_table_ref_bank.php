<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnNamaLenghtTableRefBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_bank', function (Blueprint $table) {
            $table->string('nama', 50)->change();
        });

        Schema::table('log_ref_bank', function (Blueprint $table) {
            $table->string('nama', 50)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_bank', function (Blueprint $table) {
            $table->dropColumn('nama', 30);
        });

        Schema::table('log_ref_bank', function (Blueprint $table) {
            $table->dropColumn('nama', 30);
        });
    }
}
