<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnDroplsiAddstatusTableRefKki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //drop lsi
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->dropForeign(['lsi_id']);
            $table->dropColumn('lsi_id');
        });


        //add status
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->integer('status')->comment('1:Berjalan; 0:Selesai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //drop lsi
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->integer('lsi_id')->unsigned();

            $table->foreign('lsi_id')->references('id')->on('ref_lsi');
        });
        

        //add status
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
