<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTableRefProdukHarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('ref_produk_harga');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('ref_produk_harga', function (Blueprint $table) {
            $table->increments('id');

            $table->string('tmuk_kode', 10)->unsigned();
            $table->string('produk_kode', 30)->unsigned();

            $table->decimal('uom1_cost_price', 10, 2)->nullable();
            $table->decimal('uom1_suggest_price', 10, 2)->nullable();
            $table->decimal('uom1_change_price', 10, 2)->nullable();
            $table->decimal('uom1_margin_amount', 10, 2)->nullable();
            $table->decimal('uom1_map', 10, 2)->nullable();
            $table->decimal('uom2_cost_price', 10, 2)->nullable();
            $table->decimal('uom2_gmd_price', 10, 2)->nullable();
            $table->tinyInteger('bblm_fg')->nullable();
            $table->integer('con1')->nullable();
            $table->decimal('dc1', 10, 2)->nullable();
            $table->integer('con2')->nullable();
            $table->decimal('dc2', 10, 2)->nullable();
            $table->integer('con3')->nullable();
            $table->decimal('dc3', 10, 2)->nullable();
            $table->integer('limit')->nullable();
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }
}
