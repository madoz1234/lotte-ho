<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransJurnalAddTahunFiskal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_jurnal', function (Blueprint $table) {
            $table->integer('tahun_fiskal_id')->unsigned();

            $table->foreign('tahun_fiskal_id')->references('id')->on('ref_tahun_fiskal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_jurnal', function (Blueprint $table) {
            $table->dropForeign(['tahun_fiskal_id']);

            $table->dropColumn('tahun_fiskal_id');
        });
    }
}
