<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusRefDetailPromosiBuyXGetY extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_promosi', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });

        Schema::table('ref_detail_promosi_buy_x_get_y', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });
        
        Schema::table('ref_detail_promosi_diskon', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });
        
        Schema::table('ref_detail_promosi_promo', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });
        
        Schema::table('ref_detail_promosi_purchase', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_promosi', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('ref_detail_promosi_buy_x_get_y', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        
        Schema::table('ref_detail_promosi_diskon', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        
        Schema::table('ref_detail_promosi_promo', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        
        Schema::table('ref_detail_promosi_purchase', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
