<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefPromosi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_promosi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('region_id')->unsigned();
            $table->integer('lsi_id')->unsigned();
            $table->date('tgl_awal');
            $table->date('tgl_akhir');
            $table->string('nama', 255);
            $table->string('kode_promo', 50);
            $table->tinyInteger('tipe_promo');
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('region_id')->references('id')->on('ref_region');
            $table->foreign('lsi_id')->references('id')->on('ref_lsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_promosi');
    }
}
