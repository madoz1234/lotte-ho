<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransReduceEscrowAddNomorh2h extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->string('nomorh2h', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->dropColumn('nomorh2h');
        });
    }
}
