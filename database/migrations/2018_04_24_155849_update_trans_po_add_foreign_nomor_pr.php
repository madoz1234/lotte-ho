<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPoAddForeignNomorPr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_po', function (Blueprint $table) {
            $table->foreign('nomor_pr')->references('nomor')->on('trans_pr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_po', function (Blueprint $table) {
            $table->dropForeign(['nomor_pr']);
        });
    }
}
