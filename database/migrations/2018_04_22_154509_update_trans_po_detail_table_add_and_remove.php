<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPoDetailTableAddAndRemove extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_po_detail', function (Blueprint $table) {
            $table->dropColumn('unit_price');

            $table->integer('qty_pr')->nullable()->default(0);
            $table->string('unit_pr', 5)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_po_detail', function (Blueprint $table) {
            $table->dropColumn('qty_pr');
            $table->dropColumn('unit_pr');
            
            $table->string('unit_price', 5)->nullable()->default(0);
        });
    }
}
