<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateForeignRefDetailPromosiTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_detail_promosi_tmuk', function (Blueprint $table) {
            $table->dropForeign(['promosi_id']);
        });

        Schema::table('ref_detail_promosi_tmuk', function (Blueprint $table) {
            $table->foreign('promosi_id')->references('id')->on('ref_promosi')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_detail_promosi_tmuk', function (Blueprint $table) {
            $table->dropForeign(['promosi_id']);
        });
        
        Schema::table('ref_detail_promosi_tmuk', function (Blueprint $table) {
            $table->foreign('promosi_id')->references('id')->on('ref_promosi');
        });
    }
}
