<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableRefProdukLsiUpdateUniqueV1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk_lsi', function ($table) {
            $table->dropUnique('ref_produk_lsi_lsi_kode_unique');
            $table->dropUnique('ref_produk_lsi_produk_kode_unique');

            $table->unique(['lsi_kode', 'produk_kode']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
