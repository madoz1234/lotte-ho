<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHppMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_hpp_map', function (Blueprint $table) {
            $table->dropForeign(['po_nomor']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_hpp_map', function (Blueprint $table) {
            $table->foreign('po_nomor')->references('nomor')->on('trans_po');
        });
    }
}
