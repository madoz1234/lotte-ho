<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefKontainer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kontainer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 20);
            $table->decimal('tinggi_luar', 5,2);
            $table->decimal('tinggi_dalam', 5,2);
            $table->decimal('panjang_atas', 5,2);
            $table->decimal('panjang_bawah', 5,2);
            $table->decimal('lebar_atas', 5,2);
            $table->decimal('lebar_bawah', 5,2);
            $table->decimal('volume', 5,2);
            $table->integer('tipe_box')->comment('1:Innerbox; 0:Outbox');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_kontainer');
    }
}
