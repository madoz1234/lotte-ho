<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransMemberPiutangAndDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_member_piutang', function (Blueprint $table) {
            $table->increments('id');

            $table->string('tmuk_kode', 30);
            $table->string('no_member', 10);
            $table->string('nama_member',50);

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
        });
        Schema::create('trans_member_piutang_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_trans_member_piutang')->unsigned();
            $table->date('tanggal');
            $table->integer('hutang');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_trans_member_piutang')->references('id')->on('trans_member_piutang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_member_piutang_detail');
        Schema::drop('trans_member_piutang');
    }
}
