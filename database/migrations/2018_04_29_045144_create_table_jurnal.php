<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJurnal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_jurnal', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('tanggal');
            $table->string('tmuk_kode', 30);
            $table->string('coa_kode', 10);

            $table->string('referensi', 50)->nullable();
            $table->string('deskripsi', 200)->nullable();

            $table->decimal('debit', 20, 2)->nullable();
            $table->decimal('kredit', 20, 2)->nullable();
            
            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
            $table->foreign('coa_kode')->references('kode')->on('ref_coa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_jurnal');
    }
}
