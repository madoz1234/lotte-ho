<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnVendorLokalTableDirectInvoiceDeleteFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_direct_invoice', function (Blueprint $table) {
            $table->dropForeign(['vendor_lokal_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_direct_invoice', function (Blueprint $table) {
            $table->foreign('vendor_lokal_id')->references('id')->on('ref_vendor_tmuk');
        });
    }
}