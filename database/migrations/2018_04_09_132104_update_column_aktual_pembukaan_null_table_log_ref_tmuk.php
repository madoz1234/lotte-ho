<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnAktualPembukaanNullTableLogRefTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_tmuk', function (Blueprint $table) {
            $table->date('aktual_pembukaan')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_tmuk', function (Blueprint $table) {
            $table->dropColumn('aktual_pembukaan');
        });
    }
}
