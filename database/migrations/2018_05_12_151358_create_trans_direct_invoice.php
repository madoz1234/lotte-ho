<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransDirectInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_direct_invoice', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nomor', 30)->unique();
            $table->string('tmuk_kode', 10)->unsigned();
            $table->date('tgl_buat');
            $table->date('tgl_kirim');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
        });

        Schema::create('trans_detail_direct_invoice', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('direct_invoice_id')->unsigned();
            $table->string('produk_kode', 30)->unsigned();
            $table->integer('qty_po')->nullable();
            $table->string('unit_po', 5)->nullable();
            $table->string('unit_price', 5)->nullable();
            $table->decimal('price', 20, 2)->nullable();
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('direct_invoice_id')->references('id')->on('trans_direct_invoice');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_direct_invoice');

        Schema::drop('trans_detail_direct_invoice');
    }
}
