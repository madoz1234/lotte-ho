<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReduceEscrowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_reduce_escrow', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('tanggal');
            $table->string('po_nomor', 30);
            $table->string('tmuk_kode', 10);

            $table->decimal('saldo_dipotong', 20, 2)->nullable();
            $table->decimal('saldo_scn', 20, 2)->nullable();
            $table->decimal('saldo_escrow', 20, 2)->nullable();
            $table->date('verifikasi1_date')->nullable();
            $table->integer('verifikasi1_user')->unsigned()->nullable();
            $table->date('verifikasi2_date')->nullable();
            $table->integer('verifikasi2_user')->unsigned()->nullable();
            $table->date('verifikasi3_date')->nullable();
            $table->integer('verifikasi3_user')->unsigned()->nullable();
            $table->date('verifikasi4_date')->nullable();
            $table->integer('verifikasi4_user')->unsigned()->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('po_nomor')->references('nomor_po')->on('trans_po');
            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');

        });

        Schema::create('trans_reduce_escrow_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('reduce_escrow_id')->unsigned();
            $table->string('nomor_struk', 30);
            $table->decimal('total_belanja', 20, 2);

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('reduce_escrow_id')->references('id')->on('trans_reduce_escrow');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_reduce_escrow_detail');
        Schema::drop('trans_reduce_escrow');
    }
}
