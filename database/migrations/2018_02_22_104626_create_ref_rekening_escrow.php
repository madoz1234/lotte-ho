<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefRekeningEscrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_rekening_escrow', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_escrow_id')->unsigned();
            $table->string('nama_pemilik', 50);
            $table->string('nomor_rekening',20);
            $table->integer('status')->comment('1:Aktif;2:Non-Aktif');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('bank_escrow_id')->references('id')->on('ref_bank_escrow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_rekening_escrow');
    }
}
