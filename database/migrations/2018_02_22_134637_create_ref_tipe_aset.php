<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefTipeAset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_tipe_aset', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipe', 30);
            $table->integer('tingkat_depresiasi');
            $table->integer('status')->comment('1:Belum Selesai/Beli Aset; 0:Selesai/Jual Aset');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_tipe_aset');
    }
}
