<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPoPrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_po', function ($table) {
            $table->renameColumn('nomor_po', 'nomor');
        });

        Schema::table('trans_pr', function ($table) {
            $table->renameColumn('nomor_pr', 'nomor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_po', function ($table) {
            $table->renameColumn('nomor','nomor_po');
        });

        Schema::table('trans_pr', function ($table) {
            $table->renameColumn('nomor','nomor_pr');
        });
    }
}
