<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIii4pendapatanlainlainTableRefKki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->decimal('iii_tambahan_1', 30, 2)->default(0)->nullable();
            $table->decimal('iii_tambahan_2', 30, 2)->default(0)->nullable();
            $table->decimal('iii_tambahan_3', 30, 2)->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kki', function (Blueprint $table) {
            $table->dropColumn('iii_tambahan_1');
            $table->dropColumn('iii_tambahan_2');
            $table->dropColumn('iii_tambahan_3');
        });
    }
}
