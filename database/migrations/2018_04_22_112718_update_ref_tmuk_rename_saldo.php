<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefTmukRenameSaldo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_tmuk', function ($table) {
            $table->renameColumn('saldo', 'saldo_deposit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('ref_tmuk', function ($table) {
            $table->renameColumn('saldo_deposit', 'saldo');
        });
    }
}
