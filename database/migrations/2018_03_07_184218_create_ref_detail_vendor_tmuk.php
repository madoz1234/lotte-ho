<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefDetailVendorTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_detail_vendor_tmuk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_tmuk_id')->unsigned()->nullable();
            $table->integer('tmuk_id')->unsigned()->nullable();

            $table->foreign('vendor_tmuk_id')->references('id')->on('ref_vendor_tmuk');
            $table->foreign('tmuk_id')->references('id')->on('ref_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_detail_vendor_tmuk');
    }
}
