<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateForeignRefDetailVendorTmukV1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_detail_vendor_tmuk', function (Blueprint $table) {
            $table->dropForeign(['vendor_tmuk_id']);
        });

        Schema::table('ref_detail_vendor_tmuk', function (Blueprint $table) {
            $table->foreign('vendor_tmuk_id')->references('id')->on('ref_vendor_tmuk')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_detail_vendor_tmuk', function (Blueprint $table) {
            $table->dropForeign(['vendor_tmuk_id']);
        });
        
        Schema::table('ref_detail_vendor_tmuk', function (Blueprint $table) {
            $table->foreign('vendor_tmuk_id')->references('id')->on('ref_vendor_tmuk');
        });
    }
}
