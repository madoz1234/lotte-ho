<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPoDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_po_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nomor_po', 30);
            $table->string('produk_kode', 30);
            $table->integer('qty_po')->default(0);
            $table->integer('unit_po')->default(0);
            $table->decimal('unit_price', 10, 2)->default(0);
            $table->decimal('price', 10, 2)->default(0);
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('nomor_po')->references('nomor_po')->on('trans_po');
            $table->foreign('produk_kode')->references('kode')->on('ref_produk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_po_detail');
    }
}
