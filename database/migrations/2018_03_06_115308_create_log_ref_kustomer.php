<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRefKustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ref_kustomer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kustomer_id')->unsigned();
            $table->integer('tmuk_id')->unsigned();
            $table->integer('kota_id')->unsigned();
            $table->integer('kecamatan_id')->unsigned();
            $table->integer('kelurahan_id')->unsigned();
            $table->integer('pajak_id')->unsigned();
            $table->integer('jeniskustomer_id')->unsigned();
            $table->integer('membercard_id')->unsigned();
            $table->string('kode', 10);
            $table->text('alamat');
            $table->string('kode_pos', 5);
            $table->string('longitude', 10);
            $table->string('latitude', 10);
            $table->decimal('limit_kredit', 10, 2);
            $table->string('persen_diskon', 3);
            $table->integer('cara_pembayaran');
            $table->integer('status_kredit');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_ref_kustomer');
    }
}
