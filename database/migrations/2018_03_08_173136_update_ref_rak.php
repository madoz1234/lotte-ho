<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefRak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_rak', function (Blueprint $table) {
            $table->string('tipe_rak', 30)->unique()->change();
        });

        Schema::table('log_ref_rak', function (Blueprint $table) {
            $table->string('tipe_rak', 30)->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_rak', function (Blueprint $table) {
            $table->dropColumn('tipe_rak');
        });

        Schema::table('log_ref_rak', function (Blueprint $table) {
            $table->dropColumn('tipe_rak');
        });
    }
}