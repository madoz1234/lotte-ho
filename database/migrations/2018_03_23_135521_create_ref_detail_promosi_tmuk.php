<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefDetailPromosiTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_detail_promosi_tmuk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('promosi_id')->unsigned()->nullable();
            $table->integer('tmuk_id')->unsigned()->nullable();

            $table->foreign('promosi_id')->references('id')->on('ref_promosi');
            $table->foreign('tmuk_id')->references('id')->on('ref_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_detail_promosi_tmuk');
    }
}
