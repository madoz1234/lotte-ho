<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKodeAyatJurnal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kode_ayat_jurnal', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('interval_jatuh_tempo');
            $table->integer('dimensi_yang_diperlukan');
            $table->integer('pendapatan_yang_disimpan');
            $table->integer('laba_tahun')->unsigned();
            $table->integer('pertukaran_varian_akun')->unsigned();
            $table->integer('rekening_biaya_bank');

            $table->integer('batas_kredit');
            $table->integer('hari_yang_berlaku');
            $table->integer('jumlah_hari_pengiriman');
            $table->integer('dibebankan_kepengirim_akun')->unsigned();
            $table->integer('rekening_piutang')->unsigned();
            $table->integer('akun_penjualan1')->unsigned();
            $table->integer('akun_diskon_penjualan')->unsigned();
            $table->integer('rekening_diskon_penjualan')->unsigned();
            $table->text('legal_text_invoice');

            $table->integer('pemasok_over_receive');
            $table->integer('faktur_over_charge');
            $table->integer('rekening_akun')->unsigned();
            $table->integer('pembelian_rekening_diskon')->unsigned();
            $table->integer('grn_clearing_account')->unsigned();

            $table->integer('akun_penjualan2')->unsigned();
            $table->integer('akun_inventaris1')->unsigned();
            $table->integer('akun_cogs')->unsigned();
            $table->integer('akun_penjualan3')->unsigned();
            $table->integer('akun_inventaris2')->unsigned();

            $table->integer('work_order');
            $table->integer('pembayaran_kembali_tmuk');
            $table->integer('stock_opname')->unsigned();
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('laba_tahun')->references('id')->on('ref_tmuk');
            $table->foreign('pertukaran_varian_akun')->references('id')->on('ref_tmuk');

            $table->foreign('dibebankan_kepengirim_akun')->references('id')->on('ref_tmuk');
            $table->foreign('rekening_piutang')->references('id')->on('ref_tmuk');
            $table->foreign('akun_penjualan1')->references('id')->on('ref_tmuk');
            $table->foreign('akun_diskon_penjualan')->references('id')->on('ref_tmuk');
            $table->foreign('rekening_diskon_penjualan')->references('id')->on('ref_tmuk');

            $table->foreign('rekening_akun')->references('id')->on('ref_tmuk');
            $table->foreign('pembelian_rekening_diskon')->references('id')->on('ref_tmuk');
            $table->foreign('grn_clearing_account')->references('id')->on('ref_tmuk');

            $table->foreign('akun_penjualan2')->references('id')->on('ref_tmuk');
            $table->foreign('akun_inventaris1')->references('id')->on('ref_tmuk');
            $table->foreign('akun_cogs')->references('id')->on('ref_tmuk');
            $table->foreign('akun_penjualan3')->references('id')->on('ref_tmuk');
            $table->foreign('akun_inventaris2')->references('id')->on('ref_tmuk');

            $table->foreign('stock_opname')->references('id')->on('ref_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_kode_ayat_jurnal');
    }
}
