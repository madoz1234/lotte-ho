<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransPyrPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pyr_pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_pyr',30);
            $table->string('tmuk_kode',10);
            $table->decimal('saldo_deposit', 20, 2)->nullable();
            $table->decimal('saldo_scn', 20, 2)->nullable();
            $table->decimal('saldo_escrow', 20, 2)->nullable();
            $table->decimal('biaya_admin', 20, 2)->nullable();
            $table->integer('status')->default(0);
            
            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('nomor_pyr')->references('nomor_pyr')->on('trans_pyr');
            $table->foreign('tmuk_kode')->references('kode')->on('ref_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_pyr_pembayaran');
    }
}
