<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLogRefTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_ref_tmuk', function ($table) {
            $table->dropColumn('npwp_perusahaan');
            $table->dropColumn('nama_perusahaan');
            $table->dropColumn('alamat_perusahaan');
            $table->dropColumn('nomor_rekening');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_tmuk', function ($table) {
            $table->string('npwp_perusahaan', 30);
            $table->string('nama_perusahaan', 50);
            $table->text('alamat_perusahaan');
            $table->string('nomor_rekening', 20);
        });
    }
}