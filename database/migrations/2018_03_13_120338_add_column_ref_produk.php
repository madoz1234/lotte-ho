<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRefProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_produk', function (Blueprint $table) {
            $table->string('nama', 255)->after('kode');
            $table->string('bumun_cd', 10)->nullable()->after('nama');
            $table->string('bumun_nm', 50)->nullable()->after('bumun_cd');
            $table->string('l1_cd', 10)->nullable()->after('bumun_nm');
            $table->string('l1_nm', 50)->nullable()->after('l1_cd');
            $table->string('l2_cd', 10)->nullable()->after('l1_nm');
            $table->string('l2_nm', 50)->nullable()->after('l2_cd');
            $table->string('l3_cd', 10)->nullable()->after('l2_nm');
            $table->string('l3_nm', 50)->nullable()->after('l3_cd');
            $table->string('l4_cd', 10)->nullable()->after('l3_nm');
            $table->string('l4_nm', 50)->nullable()->after('l4_cd');
            $table->float('width')->nullable()->comment('Width of Product')->after('l4_nm');
            $table->float('length')->nullable()->comment('Length of Product')->after('width');
            $table->float('height')->nullable()->comment('Height of Product')->after('length');
            $table->float('wg')->nullable()->comment('Weight of Product')->after('height');
            $table->float('inner_width')->nullable()->comment('Inner Width of Product')->after('wg');
            $table->float('inner_length')->nullable()->comment('Inner Length of Product')->after('inner_width');
            $table->float('inner_height')->nullable()->comment('Inner Height of Product')->after('inner_length');
            $table->float('inner_wg')->nullable()->comment('Inner Weight of Product')->after('inner_height');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_produk', function (Blueprint $table) {
            //
        });
    }
}
