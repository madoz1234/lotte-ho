<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBankRekeningPemilikTableRefTmukLogRefTmuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_tmuk', function (Blueprint $table) {
            $table->integer('bank_pemilik_id')->unsigned()->nullable();
            $table->integer('rekening_pemilik_id')->unsigned()->nullable();

            $table->foreign('bank_pemilik_id')->references('id')->on('ref_bank');
            $table->foreign('rekening_pemilik_id')->references('id')->on('ref_rekening_bank');
        });

        Schema::table('log_ref_tmuk', function (Blueprint $table) {
            $table->decimal('saldo', 20, 2)->nullable();
            $table->integer('bank_pemilik_id')->unsigned()->nullable();
            $table->integer('rekening_pemilik_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_tmuk', function (Blueprint $table) {
            $table->dropColumn('bank_pemilik_id');
            $table->dropColumn('rekening_pemilik_id');
        });

        Schema::table('log_ref_tmuk', function (Blueprint $table) {
            $table->dropColumn('saldo');
            $table->dropColumn('bank_pemilik_id');
            $table->dropColumn('rekening_pemilik_id');
        });
    }
}
