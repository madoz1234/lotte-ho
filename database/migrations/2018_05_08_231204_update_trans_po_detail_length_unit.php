<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransPoDetailLengthUnit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_po_detail', function (Blueprint $table) {
            $table->string('unit_po', 15)->nullable()->change();
            $table->string('unit_pr', 15)->nullable()->default(0)->change();
            $table->decimal('price', 20, 2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_po_detail', function (Blueprint $table) {
            $table->string('unit_po', 5)->nullable()->change();
            $table->string('unit_pr', 5)->nullable()->default(0)->change();
            $table->decimal('price', 10, 2)->default(0)->change();
        });
    }
}
