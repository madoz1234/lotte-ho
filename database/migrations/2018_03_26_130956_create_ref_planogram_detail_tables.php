<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefPlanogramDetailTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_planogram_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('planogram_id')->unsigned();
            $table->integer('rak_id')->unsigned();
            $table->string('nomor_rak', 10);
            $table->text('planogram_json');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('planogram_id')->references('id')->on('ref_planogram');
            $table->foreign('rak_id')->references('id')->on('ref_rak');
        });
        Schema::create('log_ref_planogram_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('planogram_id')->unsigned();
            $table->integer('rak_id')->unsigned();
            $table->string('nomor_rak', 10);
            $table->text('planogram_json');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ref_planogram_detail');
        Schema::dropIfExists('ref_planogram_detail');
    }
}
