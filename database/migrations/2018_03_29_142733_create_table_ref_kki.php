<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefKki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kki', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lsi_id')->unsigned();
            $table->integer('tmuk_id')->unsigned();
            $table->date('tanggal_submit');

            $table->decimal('i_satu', 10, 2)->default(0);
            $table->decimal('i_dua', 10, 2)->default(0);
            $table->decimal('i_tiga_a', 10, 2)->default(0);
            $table->decimal('i_tiga_b', 10, 2)->default(0);
            $table->decimal('i_tiga_c', 10, 2)->default(0);
            $table->decimal('i_tiga_d', 10, 2)->default(0);
            $table->decimal('i_tiga_e', 10, 2)->default(0);
            $table->decimal('i_tiga_f', 10, 2)->default(0);
            $table->decimal('i_empat_a', 10, 2)->default(0);
            $table->decimal('i_empat_b', 10, 2)->default(0);
            $table->decimal('i_empat_c', 10, 2)->default(0);
            $table->decimal('i_lima_a', 10, 2)->default(0);
            $table->decimal('i_lima_b', 10, 2)->default(0);
            $table->decimal('i_sub_total', 10, 2)->default(0);
            $table->decimal('i_sewa_ruko', 10, 2)->default(0);
            $table->decimal('i_total_inves1_1', 10, 2)->default(0);
            $table->decimal('i_ppn', 10, 2)->default(0);
            $table->decimal('i_total_inves1_2', 10, 2)->default(0);

            $table->decimal('ii_struk_terburuk', 10, 2)->default(0);
            $table->decimal('ii_average_terburuk', 10, 2)->default(0);
            $table->decimal('ii_sales_terburuk', 10, 2)->default(0);
            $table->decimal('ii_penjualan_terburuk', 10, 2)->default(0);
            $table->decimal('ii_struk_normal', 10, 2)->default(0);
            $table->decimal('ii_average_normal', 10, 2)->default(0);
            $table->decimal('ii_sales_normal', 10, 2)->default(0);
            $table->decimal('ii_penjualan_normal', 10, 2)->default(0);
            $table->decimal('ii_struk_terbaik', 10, 2)->default(0);
            $table->decimal('ii_average_terbaik', 10, 2)->default(0);
            $table->decimal('ii_sales_terbaik', 10, 2)->default(0);
            $table->decimal('ii_penjualan_terbaik', 10, 2)->default(0);

            $table->decimal('iii_labakotor_estimasi1', 10, 2)->default(0);
            $table->decimal('iii_pendapatanlain2_estimasi1', 10, 2)->default(0);
            $table->decimal('iii_pendapatansewa_estimasi1', 10, 2)->default(0);
            $table->decimal('iii_total_estimasi1', 10, 2)->default(0);
            $table->decimal('iii_labakotor_estimasi2', 10, 2)->default(0);
            $table->decimal('iii_pendapatanlain2_estimasi2', 10, 2)->default(0);
            $table->decimal('iii_pendapatansewa_estimasi2', 10, 2)->default(0);
            $table->decimal('iii_total_estimasi2', 10, 2)->default(0);
            $table->decimal('iii_labakotor_estimasi3', 10, 2)->default(0);
            $table->decimal('iii_pendapatanlain2_estimasi3', 10, 2)->default(0);
            $table->decimal('iii_pendapatansewa_estimasi3', 10, 2)->default(0);
            $table->decimal('iii_total_estimasi3', 10, 2)->default(0);

            $table->decimal('iv_satu_a_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_satu_b_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_satu_c_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_satu_d_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_satu_e_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_satu_f_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_satu_g_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_satu_h_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_satu_i_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_dua_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_tiga_a_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_tiga_b_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_empat_a_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_empat_b_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_empat_c_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_empat_d_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_total_estimasi1', 10, 2)->default(0);
            $table->decimal('iv_satu_a_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_satu_b_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_satu_c_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_satu_d_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_satu_e_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_satu_f_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_satu_g_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_satu_h_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_satu_i_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_dua_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_tiga_a_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_tiga_b_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_empat_a_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_empat_b_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_empat_c_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_empat_d_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_total_estimasi2', 10, 2)->default(0);
            $table->decimal('iv_satu_a_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_satu_b_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_satu_c_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_satu_d_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_satu_e_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_satu_f_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_satu_g_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_satu_h_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_satu_i_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_dua_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_tiga_a_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_tiga_b_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_empat_a_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_empat_b_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_empat_c_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_empat_d_estimasi3', 10, 2)->default(0);
            $table->decimal('iv_total_estimasi3', 10, 2)->default(0);

            $table->decimal('v_satu_estimasi1', 10, 2)->default(0);
            $table->decimal('v_dua_estimasi1', 10, 2)->default(0);
            $table->decimal('v_satu_estimasi2', 10, 2)->default(0);
            $table->decimal('v_dua_estimasi2', 10, 2)->default(0);
            $table->decimal('v_satu_estimasi3', 10, 2)->default(0);
            $table->decimal('v_dua_estimasi3', 10, 2)->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('lsi_id')->references('id')->on('ref_lsi');
            $table->foreign('tmuk_id')->references('id')->on('ref_tmuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_kki');
    }
}
