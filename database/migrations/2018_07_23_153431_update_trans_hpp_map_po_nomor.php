<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransHppMapPoNomor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_hpp_map', function (Blueprint $table) {
            $table->string('po_nomor', 50)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_hpp_map', function (Blueprint $table) {
            $table->string('po_nomor', 30)->change();
        });
    }
}
