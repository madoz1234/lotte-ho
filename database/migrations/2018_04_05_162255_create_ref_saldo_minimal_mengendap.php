<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefSaldoMinimalMengendap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_saldo_minimal_mengendap', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('saldo_minimal_mengendap', 17, 2);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('log_ref_saldo_minimal_mengendap', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('saldo_minimal_mengendap_id')->unsigned();
            $table->decimal('saldo_minimal_mengendap', 17, 2);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_saldo_minimal_mengendap');

        Schema::drop('log_ref_saldo_minimal_mengendap');
    }
}
