<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransDetailPoPrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_po_detail', function ($table) {
            $table->renameColumn('nomor_po', 'po_nomor');
        });

        Schema::table('trans_pr_detail', function ($table) {
            $table->renameColumn('nomor_pr', 'pr_nomor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_po_detail', function ($table) {
            $table->renameColumn('po_nomor', 'nomor_po');
        });

        Schema::table('trans_pr_detail', function ($table) {
            $table->renameColumn('pr_nomor', 'nomor_pr');
        });
    }
}
