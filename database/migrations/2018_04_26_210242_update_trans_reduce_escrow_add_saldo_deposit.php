<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransReduceEscrowAddSaldoDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->decimal('saldo_deposit', 20, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_reduce_escrow', function (Blueprint $table) {
            $table->dropColumn('saldo_deposit');
        });
    }
}
