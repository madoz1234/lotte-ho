<?php

namespace Lotte\Models;

use Illuminate\Database\Eloquent\Model as Base;

class SecTransaksi extends Base
{
	public $table = 'sec_transaksi';
	public $timestamps = false;

	public static function generate($kode='GL')
	{
		$tanggal = date('Ymd');
		$kode    = strtoupper($kode);

		$rcd = SecTransaksi::where('tanggal', $tanggal)->where('kode', $kode)->first();
		if (!$rcd) {
			$rcd = new SecTransaksi();
			$rcd->kode    = $kode;
			$rcd->tanggal = $tanggal;
			$rcd->nomor   = 0;
		}

		$rcd->nomor = $rcd->nomor + 1;
		$rcd->save();

		return $kode . '-' . $tanggal . str_pad($rcd->nomor, 5, "0", STR_PAD_LEFT);
	}
}
