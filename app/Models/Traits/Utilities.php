<?php

namespace Lotte\Models\Traits;


use Carbon\Carbon;

///trait ModelUtilitiesTrait

trait Utilities

{
    public function lpad($field, $length = 2, $padder = ' ')
    {
        return str_pad($this->$field, $length, $padder, STR_PAD_LEFT);
    }

    public function readMoreRaw($value, $maxLength = 150)
    {
        $return = $value;
        if (strlen($value) > $maxLength) {
            $return   = substr($value, 0, $maxLength);
            $readmore = substr($value, $maxLength);

            $return .= '<a href="javascript: void(0)" class="read-more" onclick="$(this).parent().find(\'.read-more-cage\').show(); $(this).hide()">&nbsp;&nbsp;read more</a>';

            $readless = '<a href="javascript: void(0)" class="read-less" onclick="$(this).parent().parent().find(\'.read-more\').show(); $(this).parent().hide()">&nbsp;&nbsp;read less</a>';

            $return = "<span>{$return}<span style='display: none' class='read-more-cage'>{$readmore} {$readless}</span></span>";
        }

        return $return;
    }

    public function readMoreText($field, $maxLength = 150)
    {
        $value = $this->$field;
        return utf8_decode($this->readMoreRaw($value, $maxLength));
    }

    public static function options($display, $id = 'id', $params = [], $default=null)
    {
        $q = static::select('*');

        $params = array_merge([
            'valuePrefix' => '',
        ], $params);

        if (isset($params['filters'])) {
            foreach ($params['filters'] as $key => $value) {
                if (is_numeric($key) && is_callable($value)) {
                    $q = $q->where($value);
                } else {
                    $q = $q->where($key, $value);
                }
            }
        }

        if (isset($params['orders'])) {
            foreach ($params['orders'] as $key => $value) {
                if (is_numeric($key)) {
                    $key   = $value;
                    $value = 'asc';
                }

                $q = $q->orderBy($key, $value);
            }
        }

        // if (isset($params['group'])) {
        //      $q->distinct($params['group']);
        //      dd($params['group']);
        // }

        $r = [];

        $ret = '';
        if ($default !== false) {
            if($default === null){
                $default = '(Pilih Salah Satu)';
            }
            $ret = '<option value="">' . $default . '</option>';
        }

        if (is_string($display)) {
            $q = $q->orderBy($display, 'asc');
            $r = $q->lists($display, $id);

            foreach ($r as $i => $v) {
                $i = $params['valuePrefix'] . $i;
                if (isset($params['selected']) && $i == $params['selected']) {
                    $ret .= '<option value="' . $i . '" selected>' . $v . '</option>';
                } else {
                    $ret .= '<option value="' . $i . '">' . $v . '</option>';
                }
            }
        } elseif (is_callable($display)) {
            $r = $q->get();
            foreach ($r as $d) {
                $i = $params['valuePrefix'] . $d->$id;
                if (isset($params['selected']) && $d->$id == $params['selected']) {
                    $ret .= '<option value="' . $i . '" selected>' . $display($d) . '</option>';
                } else {
                    $ret .= '<option value="' . $i . '">' . $display($d) . '</option>';
                }
            }
        }
        return $ret;
    }


    // public static function generateDays($val)
    // {
    //     switch ($val) {
    //         case 1:
    //              $days = 'order_senin';  
    //             break;
    //         case 2:
    //              $days = 'order_selasa';  
                   
    //             break;
    //         case 3:
    //              $days = 'order_rabu';  
                   
    //             break;
    //         case 4:
    //              $days = 'order_kamis';  
                   
    //             break;
    //         case 5:
    //              $days = 'order_jumat';  
                   
    //             break;
    //         case 6:
    //              $days = 'order_sabtu';  
                   
    //             break;
    //         case 7:
    //              $days = 'order_minggu';  
                   
    //             break;
    //     }

    //     return $days;
        
    // }


    // public function save(Request $request){

    //     $request['order_days' ] = [1,2,3,4];
        
    //     $request['order_senin']  = null;
    //     $request['order_selasa'] = null;
    //     $request['order_rabu']   = null;
    //     $request['order_kamis']  = null;
    //     $request['order_jumat']  = null;
    //     $request['order_sabtu']  = null;
    //     $request['order_minggu'] = null;

    //     foreach ($request['order_days' ] as $val) {
    //         $request[generateDays($val)] = $value;
    //     }



    //     Table::addField($request);





    // }



}
