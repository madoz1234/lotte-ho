<?php

namespace Lotte\Models\Traits;

trait RaidModel
{
    public function scopeSort($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public static function prepare($request, $identifier = 'id')
    {
        $record = new static;

        if ($request->has($identifier) && $request->get($identifier) != null && $request->get($identifier) != 0) {
            $record = static::find($request->get($identifier));
        }

        return $record;
    }

    public function postSave($request, $identifier = 'id')
    {
        # code didieu
    }

    public static function saveData($request, $identifier = 'id')
    {
        $record = static::prepare($request, $identifier);
        $record->fillData($request);
        $record->save();

        $record->postSave($request, $identifier);

        return $record;
    }

    public function fillData($request)
    {
        $this->fill($request->all());
    }

    public static function getSorted()
    {
        return static::sort()->get();
    }
}
