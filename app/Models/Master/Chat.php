<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Chat extends Model
{	
    protected $table = 'ref_chat';
    protected $fillable = ['pesan','nama'];
}
