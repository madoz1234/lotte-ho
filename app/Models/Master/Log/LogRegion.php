<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;

class LogRegion extends Model
{	
    protected $table = 'log_ref_region';
    protected $fillable = ['region_id','kode', 'area'];

}
