<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\BankEscrow;

class LogLsi extends Model
{	
    protected $table = 'log_ref_lsi';
    protected $fillable = ['lsi_id','region_id','kota_id','rekening_escrow_id','bank_escrow_id','kode','nama','alamat','kode_pos','telepon','email','longitude','latitude'];

}
