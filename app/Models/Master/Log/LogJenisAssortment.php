<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\JenisAssortment;

class LogJenisAssortment extends Model
{	
    protected $table = 'log_ref_jenis_assortment';
    protected $fillable = ['jenis_assortment_id','nama'];

    // public function assortment(){
    // 	return $this->belongsTo(JenisAssortment::class, 'jenis_assortment_id', 'id');
    // }
}