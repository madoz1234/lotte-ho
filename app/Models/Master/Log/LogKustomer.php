<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\BankEscrow;

class LogKustomer extends Model
{	
    protected $table = 'log_ref_kustomer';
    protected $fillable = ['kustomer_id','tmuk_id','kota_id','kecamatan_id','pajak_id','jeniskustomer_id','membercard_id','kode','alamat','kode_pos','longitude','latitude','limit_kredit','persen_diskon','cara_pembayaran','status_kredit'];

}
