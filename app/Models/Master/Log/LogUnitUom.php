<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;

class LogUnitUom extends Model
{	
    protected $table = 'log_ref_unit_uom';
    protected $fillable = ['unit_uom_id','nama'];

}
