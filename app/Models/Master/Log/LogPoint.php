<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\Pajak;

class LogPoint extends Model
{	
    protected $table = 'log_ref_point';
    protected $fillable = ['point_id', 'saldo_minimal_mengendap', 'tgl_berlaku', 'konversi', 'status', 'faktor_konversi', 'faktor_reedem'];

}