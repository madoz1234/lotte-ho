<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\Pajak;

class LogPajak extends Model
{	
    protected $table = 'log_ref_pajak';
    protected $fillable = ['pajak_id', 'npwp', 'nama', 'alamat_npwp'];

    // public function pajak(){
    // 	return $this->belongsTo(Pajak::class, 'pajak_id', 'id');
    // }
}