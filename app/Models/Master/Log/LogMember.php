<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\BankEscrow;

class LogMember extends Model
{	
    protected $table = 'log_ref_membercard';
    protected $fillable = ['membercard_id','nomor','nama','telepon','email','jeniskustomer_id', 'tmuk_kode'];

}
