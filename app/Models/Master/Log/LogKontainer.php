<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;

class LogKontainer extends Model
{	
    protected $table = 'log_ref_kontainer';
    protected $fillable = ['kontainer_id','nama','tinggi_luar','tinggi_dalam','panjang_atas','panjang_bawah','lebar_atas','lebar_bawah','volume','tipe_box'];
}