<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\Log\RekeningEscrow;

class LogRekeningBank extends Model
{	
    protected $table = 'log_ref_rekening_bank';
    protected $fillable = ['bank_escrow_id','rekening_escrow_id','nama_pemilik','nomor_rekening','status'];

    public function bankescrow(){
        return $this->belongsTo(BankEscrow::class, 'bank_escrow_id', 'id');
    }
    public function rekening(){
        return $this->belongsTo(RekeningEscrow::class, 'rekening_escrow_id', 'id');
    }
}
