<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\Pajak;

class LogSaldoMengendap extends Model
{	
    protected $table = 'log_ref_saldo_minimal_mengendap';
    protected $fillable = ['saldo_minimal_mengendap_id', 'saldo_minimal_mengendap'];

}