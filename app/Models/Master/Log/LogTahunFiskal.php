<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\TahunFiskal;

class LogTahunFiskal extends Model
{	
    protected $table = 'log_ref_tahun_fiskal';
    protected $fillable = ['tahun_fiskal_id','tgl_awal', 'tgl_akhir', 'status'];

    // public function pajak(){
    // 	return $this->belongsTo(TahunFiskal::class, 'tahun_fiskal_id', 'id');
    // }
}
