<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\BankEscrow;

class LogTmuk extends Model
{	
    protected $table = 'log_ref_tmuk';
    protected $fillable = ['tmuk_id','lsi_id','kota_id','kecamatan_id','membercard_id','jenis_assortment_id','rekening_escrow_id','bank_escrow_id','kode','nama','alamat','kode_pos','telepon','email','asn','auto_approve','longitude','latitude','gross_area','selling_area','nama_cde','email_cde','rencana_pembukaan','aktual_pembukaan','npwp_perusahaan','nama_perusahaan','alamat_perusahaan','nomor_rekening','pajak_id'];

}
