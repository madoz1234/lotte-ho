<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\BankEscrow;

class LogVendorLokalTmuk extends Model
{	
    protected $table = 'log_ref_vendor_tmuk';
    protected $fillable = ['vendor_lokal_tmuk_id','kota_id','kecamatan_id','rekening_escrow_id','bank_escrow_id','pajak_id','kode','nama','alamat', 'kode_pos', 'telepon', 'email', 'top', 'lead_time', 'order_senin', 'order_selasa', 'order_rabu', 'order_kamis', 'order_jumat', 'order_sabtu', 'order_minggu', 'contact_person', 'provinsi_id'];

}
