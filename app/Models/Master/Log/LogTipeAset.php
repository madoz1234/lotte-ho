<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\TipeAset;

class LogTipeAset extends Model
{	
    protected $table = 'log_ref_tipe_aset';
    protected $fillable = ['tipe_aset_id','tipe', 'tingkat_depresiasi', 'status'];

    // public function tipe(){
    // 	return $this->belongsTo(TipeAset::class, 'tipe_aset_id', 'id');
    // }
}
