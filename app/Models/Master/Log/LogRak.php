<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\Rak;

class LogRak extends Model
{	
    protected $table = 'log_ref_rak';
    protected $fillable = ['rak_id','tipe_rak','shelving','tinggi_rak','panjang_rak','lebar_rak','hanger','tinggi_hanger','panjang_hanger'];

    // public function rak(){
    // 	return $this->belongsTo(Rak::class, 'rak_id', 'id');
    // }
}