<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\TipeBarang;

class LogTipeBarang extends Model
{	
    protected $table = 'log_ref_tipebarang';
    protected $fillable = ['tipebarang_id','kode','nama'];

    // public function tipe(){
    // 	return $this->belongsTo(TipeBarang::class, 'tipebarang_id', 'id');
    // }
}