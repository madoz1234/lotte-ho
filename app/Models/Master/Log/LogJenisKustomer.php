<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\BankEscrow;

class LogJenisKustomer extends Model
{	
    protected $table = 'log_ref_jeniskustomer';
    protected $fillable = ['jeniskustomer_id','kode','jenis'];

}
