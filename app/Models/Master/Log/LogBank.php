<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\BankEscrow;

class LogBank extends Model
{	
    protected $table = 'log_ref_bank';
    protected $fillable = ['kota_id','bank_escrow_id','nama','kode_bank','alamat','kode_pos','telepon','email','tgl_mulai', 'kode_swift'];

}
