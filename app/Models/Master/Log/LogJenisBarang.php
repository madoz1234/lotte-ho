<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;
// use Lotte\Models\Master\JenisBarang;

class LogJenisBarang extends Model
{	
    protected $table = 'log_ref_jenis_barang';
    protected $fillable = ['jenis_barang_id','kode','jenis'];

}
