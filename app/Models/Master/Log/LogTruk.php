<?php

namespace Lotte\Models\Master\Log;

use Lotte\Models\Model;

class LogTruk extends Model
{	
    protected $table = 'log_ref_truk';
    protected $fillable = ['truk_id','tipe','tinggi','panjang','lebar','kapasitas','volume'];
}