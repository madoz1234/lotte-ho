<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\TransAkuisisiAset;

class TipeAset extends Model
{	
    protected $table = 'ref_tipe_aset';
    protected $fillable = ['tipe', 'tingkat_depresiasi', 'status'];

    public function akuisisiaset(){
        return $this->hasMany(TransAkuisisiAset::class,'tipe_id','id');
    }


}
