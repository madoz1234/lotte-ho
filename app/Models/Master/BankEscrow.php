<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class BankEscrow extends Model
{	
    protected $table = 'ref_bank';
    protected $fillable = ['kota_id','nama','kode_bank','alamat','kode_pos','telepon','email','tgl_mulai', 'kode_swift', 'provinsi_id'];

    
    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id');
    }
    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'kota_id');
    }
}
