<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\Kota;

class Kecamatan extends Model
{	
    protected $table = 'ref_kecamatan';
    protected $fillable = ['nama', 'kota_id'];

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id', 'id');
    }
}
