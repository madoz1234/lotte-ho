<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\BankEscrow;

class RekeningEscrow extends Model
{	
    protected $table = 'ref_rekening_bank';
    protected $fillable = ['bank_escrow_id','nama_pemilik','nomor_rekening','status'];

    public function bankescrow(){
        return $this->belongsTo(BankEscrow::class, 'bank_escrow_id', 'id');
    }
}
