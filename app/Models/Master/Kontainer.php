<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Kontainer extends Model
{	
    protected $table = 'ref_kontainer';
    protected $fillable = ['kode','tipe_barang_id','nama','tinggi_luar','tinggi_dalam','panjang_atas','panjang_bawah','lebar_atas','lebar_bawah','volume','tipe_box'];
}