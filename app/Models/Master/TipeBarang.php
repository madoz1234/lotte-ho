<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class TipeBarang extends Model
{	
    protected $table = 'ref_tipebarang';
    protected $fillable = ['kode','nama'];

    public static function generateCode()
    {
        $last = static::orderBy('kode', 'desc')->first();
        $kode = (!is_null($last) && $k = $last->kode) ? intval($last->kode) : 0;

        return str_pad($kode + 1, 3, "0", STR_PAD_LEFT);
    }
}