<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Planogram\Detail;

class Rak extends Model
{	
    protected $table = 'ref_rak';
    protected $fillable = ['tipe_rak','shelving','tinggi_rak','panjang_rak','lebar_rak','hanger','tinggi_hanger','panjang_hanger'];

    public function planogram()
    {
    	return $this->hasMany(Detail::class, 'rak_id');
    }
}