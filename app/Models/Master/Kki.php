<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Trans\TransDirectInvoice;

class Kki extends Model
{	
    protected $table = 'ref_kki';
    protected $fillable = [
            'tmuk_id',
            'tanggal_submit',
            'nomor',
            'status',
            'i_satu',
            'i_dua',
            'i_tiga_a',
            'i_tiga_b',
            'i_tiga_c',
            'i_tiga_d',
            'i_tiga_e',
            'i_tiga_f',
            'i_empat_a',
            'i_empat_b',
            'i_empat_c',
            'i_lima_a',
            'i_lima_b',
            'i_sub_total',
            'i_sewa_ruko',
            'i_total_inves1_1',
            'i_ppn',
            'i_total_inves1_2',
            'i_bila_ada_biaya_tambahan',
            'i_perkiraan_stok_ideal',

            'ii_struk_terburuk',
            'ii_average_terburuk',
            'ii_sales_terburuk',
            'ii_penjualan_terburuk',
            'ii_struk_normal',
            'ii_average_normal',
            'ii_sales_normal',
            'ii_penjualan_normal',
            'ii_struk_terbaik',
            'ii_average_terbaik',
            'ii_sales_terbaik',
            'ii_penjualan_terbaik',

            'iii_labakotor_estimasi1',
            'iii_pendapatanlain2_estimasi1',
            'iii_pendapatansewa_estimasi1',
            'iii_total_estimasi1',
            'iii_labakotor_estimasi2',
            'iii_pendapatanlain2_estimasi2',
            'iii_pendapatansewa_estimasi2',
            'iii_total_estimasi2',
            'iii_labakotor_estimasi3',
            'iii_pendapatanlain2_estimasi3',
            'iii_pendapatansewa_estimasi3',
            'iii_total_estimasi3',

            'iv_satu_a_estimasi1',
            'iv_satu_b_estimasi1',
            'iv_satu_c_estimasi1',
            'iv_satu_d_estimasi1',
            'iv_satu_e_estimasi1',
            'iv_satu_f_estimasi1',
            'iv_satu_g_estimasi1',
            'iv_satu_h_estimasi1',
            'iv_satu_i_estimasi1',
            'iv_dua_estimasi1',
            'iv_tiga_a_estimasi1',
            'iv_tiga_b_estimasi1',
            'iv_empat_a_estimasi1',
            'iv_empat_b_estimasi1',
            'iv_empat_c_estimasi1',
            'iv_empat_d_estimasi1',
            'iv_total_estimasi1',
            'iv_satu_a_estimasi2',
            'iv_satu_b_estimasi2',
            'iv_satu_c_estimasi2',
            'iv_satu_d_estimasi2',
            'iv_satu_e_estimasi2',
            'iv_satu_f_estimasi2',
            'iv_satu_g_estimasi2',
            'iv_satu_h_estimasi2',
            'iv_satu_i_estimasi2',
            'iv_dua_estimasi2',
            'iv_tiga_a_estimasi2',
            'iv_tiga_b_estimasi2',
            'iv_empat_a_estimasi2',
            'iv_empat_b_estimasi2',
            'iv_empat_c_estimasi2',
            'iv_empat_d_estimasi2',
            'iv_total_estimasi2',
            'iv_satu_a_estimasi3',
            'iv_satu_b_estimasi3',
            'iv_satu_c_estimasi3',
            'iv_satu_d_estimasi3',
            'iv_satu_e_estimasi3',
            'iv_satu_f_estimasi3',
            'iv_satu_g_estimasi3',
            'iv_satu_h_estimasi3',
            'iv_satu_i_estimasi3',
            'iv_dua_estimasi3',
            'iv_tiga_a_estimasi3',
            'iv_tiga_b_estimasi3',
            'iv_empat_a_estimasi3',
            'iv_empat_b_estimasi3',
            'iv_empat_c_estimasi3',
            'iv_empat_d_estimasi3',
            'iv_total_estimasi3',
            'iv_tambahan_satu',
            'iv_tambahan_satu_b',
            'iv_tambahan_satu_c',
            'iv_tambahan_dua',
            'iv_tambahan_dua_b',
            'iv_tambahan_dua_c',
            'iv_tambahan_tiga',
            'iv_tambahan_tiga_b',
            'iv_tambahan_tiga_c',
            'iv_tambahan_empat',
            'iv_tambahan_empat_b',
            'iv_tambahan_empat_c',

            'v_satu_estimasi1',
            'v_dua_estimasi1',
            'v_satu_estimasi2',
            'v_dua_estimasi2',
            'v_satu_estimasi3',
            'v_dua_estimasi3'];


    public static function generateCode($getlast)
    {

        $last = static::orderBy('nomor', 'desc')->first();
        $nomor = (!is_null($last) && $k = $last->nomor) ? intval($last->nomor) : 0;

        return str_pad($getlast + 1, 5, "0", STR_PAD_LEFT);
    }

    // public static function generateCode($tmuk)
    // {
    //     $str = "PO-" . date('Ymd') . $tmuk;

    //     $rcd = static::where('nomor', 'like', $str . '%')->orderBy('nomor', 'desc')->first();

    //     $nomor = 1;
    //     if ($rcd) {
    //         $nomor = (int) substr($rcd->nomor, -5);
    //         $nomor++;
    //     }

    //     return $str . str_pad($nomor, 5, "0", STR_PAD_LEFT);
    // }

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_id', 'id');
    }

    public function directinvoice()
    {
        return $this->Has_manny(TransPyrDetail::class, 'tmuk_id', 'tmuk_kode');
    }


}
