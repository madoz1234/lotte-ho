<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class BuyXGetY extends Model
{	
    protected $table = 'ref_detail_promosi_buy_x_get_y';
    protected $fillable = ['promosi_id', 'kode_produk_x', 'nama_produk_x', 'harga_jual_x', 'diskon_x', 'harga_diskon_x', 'kode_produk_y', 'nama_produk_y', 'harga_jual_y', 'diskon_y', 'harga_diskon_y', 'promo_x_y', 'total_diskon', 'status'];

    public function produkx()
    {
        return $this->belongsTo(Produk::class, 'kode_produk_x', 'kode');
    }

    public function produky()
    {
        return $this->belongsTo(Produk::class, 'kode_produk_y', 'kode');
    }
}