<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class SaldoMinimal extends Model
{	
    protected $table = 'ref_saldo_minimal_mengendap';
    protected $fillable = ['saldo_minimal_mengendap', 'tahun_fiskal_id'];

    public function tahunfiskal()
    {
        return $this->belongsTo(TahunFiskal::class, 'tahun_fiskal_id', 'id');
    }

}