<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class PurchaseList extends Model
{	
    protected $table = 'ref_detail_promosi_purchase_list';
    protected $fillable = ['purchase_id', 'kode_produk', 'nama_produk', 'selling_price'];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'kode_produk', 'kode');
    }
}