<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Coa1 extends Model
{	
    protected $table = 'ref_coa1';
    protected $fillable = ['tipe_coa_id','kode','nama'];


    public function type(){
    	return $this->belongsTo(TipeCoa::class, 'tipe_coa_id', 'id');
    }

    public function coa2(){
    	return $this->hasMany(Coa2::class, 'coa1_kode', 'kode');
    }


}