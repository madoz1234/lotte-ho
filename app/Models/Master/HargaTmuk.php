<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Trans\TransPoDetail;
use Lotte\Models\Master\ProdukSetting;

class HargaTmuk extends Model
{	
    protected $table = 'ref_harga_tmuk';
    protected $fillable = ['tmuk_kode','produk_kode','cost_price','suggest_price','change_price','margin_amount','map','gmd_price','created_at','created_by','updated_at','updated_by'];

    public function produk(){
    	return $this->belongsTo(Produk::class, 'produk_kode', 'kode');
    }

    public function po_detail(){
    	return $this->belongsTo(TransPoDetail::class, 'produk_kode', 'produk_kode');
    }

    public function produk_setting_harga(){
    	return $this->belongsTo(ProdukSetting::class, 'produk_kode', 'produk_kode');
    }
}

