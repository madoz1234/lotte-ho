<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Trans\TransPoDetail;

use Carbon\Carbon;

class ProdukTmuk extends Model
{	
    protected $table = 'ref_produk_tmuk';
    protected $fillable = ['tmuk_kode','produk_kode','stock_awal','stock_akhir','flag'];
    
    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode', 'kode');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode', 'kode');
    }

    public function produksetting(){
        return $this->belongsTo(ProdukSetting::class, 'produk_kode','produk_kode' );
    }

    public function hargaJual()
    {
        return $this->hasOne(HargaTmuk::class, 'produk_kode', 'produk_kode');
    }

    public function hargaBeli()
    {
        return $this->hasOne(ProdukLsi::class, 'produk_kode', 'produk_kode');
    }

    public function poDetail()
    {
        return $this->hasMany(TransPoDetail::class, 'produk_kode', 'produk_kode');
    }

    public function getPoDetailSum()
    {
        return $this->poDetail->sum('qty_po');
    }

    public function detail()
    {
        return $this->hasOne(ProdukTmukDetail::class, 'produk_tmuk_kode', 'id');
    }

    public function detail2()
    {
        return $this->hasMany(ProdukTmukDetail::class, 'produk_tmuk_kode', 'id');
    }

    public function jual()
    {
        return $this->hasMany(PenjualanDetail::class, 'produk_tmuk_kode', 'id');
    }

    public function gr()
    {
        return $this->hasMany(GRDetail::class, 'produk_tmuk_kode', 'id');
    }

    public function so()
    {
        return $this->hasMany(SODetail::class, 'produk_tmuk_kode', 'id');
    }

    public function pyr()
    {
        return $this->hasMany(PYRDetail::class, 'produk_tmuk_kode', 'id');
    }

    public function ad()
    {
        return $this->hasMany(ADDetail::class, 'produk_tmuk_kode', 'id');
    }

    public function rr()
    {
        return $this->hasMany(RRDetail::class, 'produk_tmuk_kode', 'id');
    }

    public function scopeByStatusPo($query, $status)
    {

        return $query->whereHas('poDetail', function ($d) use ($status) {
                    $d->whereHas('po', function ($c) use ($status) {
                        $c->where('status', $status);
                    });
                });
    }


    public function scopeByTanggalPo($query, $tanggal)
    {

        return $query->whereHas('poDetail', function ($d) use ($tanggal) {
                    $d->whereHas('po', function ($c) use ($tanggal) {
                        $c->where('tgl_buat', '<=' ,Carbon::createFromFormat('d/m/Y', $tanggal)->format('Y-m-d'));
                    });
                });
    }

    public function getPoTanggal()
    {
        $return = [];

        $getDetail = $this->poDetail;
        if($getDetail->count() > 0)
        {
            foreach($getDetail as $detail)
            {
                $return[] = $detail->po->tgl_buat;
            }
        }

        return $return;
    }
}