<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\MemberCard;
use Lotte\Models\Master\JenisKustomer;
use Lotte\Models\Master\JenisAssortment;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Kota;
use Lotte\Models\Master\Kecamatan;
use Lotte\Models\Master\Provinsi;
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Pajak;
use Lotte\Models\Master\DetailVendorLokalTmuk;
use Lotte\Models\Trans\TransSync;
use Lotte\Models\Trans\TransTopupEscrow;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Kki;
use Lotte\Models\Trans\TransJurnal;

class Tmuk extends Model
{	
    protected $table = 'ref_tmuk';
    protected $fillable = ['lsi_id', 'kode', 'nama', 'alamat','provinsi_id', 'kota_id', 'kecamatan_id', 'kode_pos', 'asn', 'auto_approve', 'longitude', 'latitude', 'telepon', 'email', 'membercard_id', 'jenis_assortment_id', 'gross_area', 'selling_area', 'nama_cde', 'email_cde', 'rencana_pembukaan', 'aktual_pembukaan', 'rekening_escrow_id', 'bank_escrow_id', 'pajak_id','bank_pemilik_id','rekening_pemilik_id','saldo_deposit','saldo_scn','saldo_escrow','last_online'];

    public function assortment()
    {
        return $this->belongsTo(JenisAssortment::class, 'jenis_assortment_id', 'id');
    }
    public function membercard()
    {
        return $this->belongsTo(MemberCard::class, 'membercard_id', 'id');
    }

    public function promosi(){
        return $this->belongsToMany(Promosi::class, 'ref_detail_promosi_tmuk', 'tmuk_id', 'promosi_id');
    }
    // public function kustomer()
    // {
    //     return $this->belongsTo(JenisKustomer::class, 'membercard_id', 'id');
    // }
    // public function region()
    // {
    //     return $this->belongsTo(Region::class, 'region_id', 'id');
    // }
    public function jurnal(){
        return $this->hasMany(TransJurnal::class, 'kode','tmuk_kode');
    }
    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id', 'id');
    }
    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan_id', 'id');
    }
    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'provinsi_id', 'id');
    }
    public function rekeningescrow()
    {
        return $this->belongsTo(RekeningEscrow::class, 'rekening_escrow_id', 'id');
    }
    public function bankescrow()
    {
        return $this->belongsTo(BankEscrow::class, 'bank_escrow_id', 'id');
    }
    public function lsi()
    {
        return $this->belongsTo(Lsi::class, 'lsi_id', 'id');
    }
    public function pajak()
    {
        return $this->belongsTo(Pajak::class, 'pajak_id', 'id');
    }

    public function rekeningpemilik()
    {
        return $this->belongsTo(RekeningEscrow::class, 'rekening_pemilik_id', 'id');
    }
    public function bankpemilik()
    {
        return $this->belongsTo(BankEscrow::class, 'bank_pemilik_id', 'id');
    }
    public function detailvendor()
    {
        return $this->belongsTo(DetailVendorLokalTmuk::class, 'tmuk_id', 'id');
    }

    public function sync(){
        return $this->hasOne(TransSync::class,'tmuk_kode','kode');
    }

    public function topup(){
        return $this->hasMany(TransTopupEscrow::class,'tmuk_kode','kode');
    }

    public function produkTmuk()
    {
        return $this->hasMany(ProdukTmuk::class, 'tmuk_kode', 'kode');
    }

    public function rekapPenjualanTmuk()
    {
        return $this->hasMany(TransRekapPenjualanTmuk::class, 'tmuk_kode', 'kode');
    }

    public function kki()
    {
        return $this->hasOne(Kki::class, 'tmuk_id', 'id');
    }

    public function pyr(){
        return $this->hasMany(TransPyr::class, 'tmuk_kode', 'kode');
    }

    public function getLastSetor()
    {
        $return = '-';
        if(!is_null($this->topup))
        {
            if($this->topup->count() > 0)
            {
                if($this->topup()->orderBy('tanggal', 'ASC')->where('jenis', 1)->first())
                {
                    $return = $this->topup()->orderBy('tanggal', 'ASC')->where('jenis', 1)->first()->tanggal;
                }
            }
        }

        return $return;
    }
    

}
