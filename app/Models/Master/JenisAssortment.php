<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class JenisAssortment extends Model
{	
    protected $table = 'ref_jenis_assortment';
    protected $fillable = ['nama'];
    
    public function produkAssortment()
    {
        return $this->hasMany(ProdukAssortment::class, 'assortment_type_id', 'id');
    }
}