<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\Lsi;

class ProdukLsi extends Model
{	
    protected $table = 'ref_produk_lsi';
    protected $fillable = ['lsi_kode','produk_kode','stok_gmd','curr_sale_prc','bblm_flag','con1','dc1','con2','dc2','con3','dc3','limit'];
     // protected $fillable = ['kode','nama','bumun_cd','bumun_nm','l1_cd','l1_nm','l2_cd','l2_nm','l3_cd','l3_nm','l4_cd','l4_nm','width','length','height','wg','inner_width','inner_length','inner_height','inner_wg',];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode', 'kode');
    }

    public function lsi()
    {
        return $this->belongsTo(Lsi::class, 'lsi_kode', 'kode');
    }

}