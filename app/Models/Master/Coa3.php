<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Coa3 extends Model
{	
    protected $table = 'ref_coa3';
    protected $fillable = ['coa2_kode','kode','nama'];
}