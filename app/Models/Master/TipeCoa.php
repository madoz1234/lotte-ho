<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class TipeCoa extends Model
{	
    protected $table = 'ref_tipe_coa';
    protected $fillable = ['nama', 'kode'];

    public function coa()
    {
    	return $this->hasMany(Coa::class, 'tipe_coa_id');
    }
}