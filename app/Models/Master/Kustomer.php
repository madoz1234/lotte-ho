<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\MemberCard;
use Lotte\Models\Master\JenisKustomer;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Pajak;

class Kustomer extends Model
{	
    protected $table = 'ref_kustomer';
    protected $fillable = ['tmuk_id', 'kota_id', 'kecamatan_id', 'pajak_id', 'jeniskustomer_id', 'membercard_id', 'kode', 'alamat', 'kode_pos', 'longitude', 'latitude', 'limit_kredit', 'persen_diskon', 'cara_pembayaran', 'status_kredit'];

    public static function generateCode()
    {
        $last = static::orderBy('kode', 'desc')->first();
        $kode = (!is_null($last) && $k = $last->kode) ? intval($last->kode) : 0;

        return str_pad($kode + 1, 5, "0", STR_PAD_LEFT);
    }

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id', 'id');
    }
    
    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'kota_id', 'id');
    }
    
    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan_id', 'id');
    }

    public function membercard()
    {
        return $this->belongsTo(MemberCard::class, 'membercard_id', 'id');
    }

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_id', 'id');
    }

    public function kustomer()
    {
        return $this->belongsTo(JenisKustomer::class, 'jeniskustomer_id', 'id');
    }

    public function pajak()
    {
        return $this->belongsTo(Pajak::class, 'pajak_id', 'id');
    }

    // $generateDays = function($val){
    //     switch ($val) {
    //         case 1:
    //              $days = 'order_senin';  
    //             break;
    //         case 2:
    //              $days = 'order_selasa';  
                   
    //             break;
    //         case 3:
    //              $days = 'order_rabu';  
                   
    //             break;
    //         case 4:
    //              $days = 'order_kamis';  
                   
    //             break;
    //         case 5:
    //              $days = 'order_jumat';  
                   
    //             break;
    //         case 6:
    //              $days = 'order_sabtu';  
                   
    //             break;
    //         case 7:
    //              $days = 'order_minggu';  
                   
    //             break;
    //     }

    //     return $days;
        // };
}