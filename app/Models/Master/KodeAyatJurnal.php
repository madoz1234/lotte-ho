<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class KodeAyatJurnal extends Model
{	
    protected $table = 'ref_kode_ayat_jurnal';
    protected $fillable = ['interval_jatuh_tempo','dimensi_yang_diperlukan','pendapatan_yang_disimpan','laba_tahun','pertukaran_varian_akun','rekening_biaya_bank','batas_kredit','hari_yang_berlaku','jumlah_hari_pengiriman','dibebankan_kepengirim_akun','rekening_piutang','akun_penjualan1','akun_diskon_penjualan','rekening_diskon_penjualan','legal_text_invoice','pemasok_over_receive','faktur_over_charge','rekening_akun','pembelian_rekening_diskon','grn_clearing_account','akun_penjualan2','akun_inventaris1','akun_cogs','akun_penjualan3','akun_inventaris2','work_order','pembayaran_kembali_tmuk','stock_opname'];



    public function labatahun()
    {
        return $this->belongsTo(Tmuk::class, 'laba_tahun', 'id');
    }

    public function pertukaranvarianakun()
    {
        return $this->belongsTo(Tmuk::class, 'pertukaran_varian_akun', 'id');
    }

    public function dibebankankepengirimakun()
    {
        return $this->belongsTo(Tmuk::class, 'dibebankan_kepengirim_akun', 'id');
    }

    public function rekeningpiutang()
    {
        return $this->belongsTo(Tmuk::class, 'rekening_piutang', 'id');
    }

    public function akunpenjualan1()
    {
        return $this->belongsTo(Tmuk::class, 'akun_penjualan1', 'id');
    }

    public function akundiskonpenjualan()
    {
        return $this->belongsTo(Tmuk::class, 'akun_diskon_penjualan', 'id');
    }

    public function rekeningdiskonpenjualan()
    {
        return $this->belongsTo(Tmuk::class, 'rekening_diskon_penjualan', 'id');
    }

    public function rekeningakun()
    {
        return $this->belongsTo(Tmuk::class, 'rekening_akun', 'id');
    }

    public function pembelianrekeningdiskon()
    {
        return $this->belongsTo(Tmuk::class, 'pembelian_rekening_diskon', 'id');
    }

    public function grnclearingaccount()
    {
        return $this->belongsTo(Tmuk::class, 'grn_clearing_account', 'id');
    }

    public function akunpenjualan2()
    {
        return $this->belongsTo(Tmuk::class, 'akun_penjualan2', 'id');
    }

    public function akuninventaris1()
    {
        return $this->belongsTo(Tmuk::class, 'akun_inventaris1', 'id');
    }

    public function akuncogs()
    {
        return $this->belongsTo(Tmuk::class, 'akun_cogs', 'id');
    }

    public function akunpenjualan3()
    {
        return $this->belongsTo(Tmuk::class, 'akun_penjualan3', 'id');
    }

    public function akuninventaris2()
    {
        return $this->belongsTo(Tmuk::class, 'akun_inventaris2', 'id');
    }

    public function stockopname()
    {
        return $this->belongsTo(Tmuk::class, 'stock_opname', 'id');
    }

}