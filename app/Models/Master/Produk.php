<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;
use Lotte\Models\Trans\TransStock;
use Carbon\Carbon;

class Produk extends Model
{	
    protected $table = 'ref_produk';
    protected $fillable = ['kode','nama','bumun_cd','bumun_nm','l1_cd','l1_nm','l2_cd','l2_nm','l3_cd','l3_nm','l4_cd','l4_nm','width','length','height','wg','inner_width','inner_length','inner_height','inner_wg',];



    public function produkAssortment()
    {
        return $this->hasMany(ProdukAssortment::class, 'produk_kode', 'kode');
    }

    public function penjualanTmukDetail()
    {
        return $this->hasMany(TransRekapPenjualanTmukDetail::class, 'item_code', 'kode');
    }

    public function produklsi(){
    	return $this->belongsTo(ProdukLsi::class, 'kode','produk_kode' );
    }

    public function produksetting(){
        return $this->belongsTo(ProdukSetting::class, 'kode','produk_kode' );
    }

    public function stock(){
        return $this->hasMany(TransStock::class, 'produk_kode','kode');
    }

    public function kategori1(){
    	return $this->hasOne(Kategori1::class, 'kode','l1_cd');
    }

    public function scopeByStockTmuk($query, $tmuk_kode)
    {
        return $query->whereHas('stock', function($q) use ($tmuk_kode){
                    $q->where('tmuk_kode', $tmuk_kode)
                      ->where('qty', '>', 0);
                });
    }


    public function getTotalStock(){
        $return = 0;
        if(!is_null($this->stock))
        {
            $return = $this->stock->sum('qty');
        }

        return $return;
    }

    public function getStockByDay($day){
        $return = 0;
        if(!is_null($this->stock))
        {
            if($this->stock->count() > 0)
            {
                foreach($this->stock as $stock)
                {
                    if($day == 1)
                    {
                        $stockDate = Carbon::createFromFormat('Y-m-d',$stock->tgl_gr);
                        if($stockDate->isPast() == true)
                        {
                            if(Carbon::now()->diffInDays($stockDate) >= 0 && Carbon::now()->diffInDays($stockDate) <= 30)
                            {
                                $return += $stock->sisa;
                            }
                        }
                    }
                    if($day == 31)
                    {
                        $stockDate = Carbon::createFromFormat('Y-m-d',$stock->tgl_gr);
                        if($stockDate->isPast() == true)
                        {
                            if(Carbon::now()->diffInDays($stockDate) >= 31 && Carbon::now()->diffInDays($stockDate) <= 60)
                            {
                                $return += $stock->sisa;
                            }
                        }

                    }
                    if($day == 61)
                    {
                        $stockDate = Carbon::createFromFormat('Y-m-d',$stock->tgl_gr);
                        if($stockDate->isPast() == true)
                        {
                            if(Carbon::now()->diffInDays($stockDate) >= 61 && Carbon::now()->diffInDays($stockDate) <= 90)
                            {
                                $return += $stock->sisa;
                            }
                        }

                    }
                    if($day == 91)
                    {
                        $stockDate = Carbon::createFromFormat('Y-m-d',$stock->tgl_gr);
                        if($stockDate->isPast() == true)
                        {
                            if(Carbon::now()->diffInDays($stockDate) >= 91 && Carbon::now()->diffInDays($stockDate) <= 120)
                            {
                                $return += $stock->sisa;
                            }
                        }

                    }
                    if($day == 120)
                    {
                        $stockDate = Carbon::createFromFormat('Y-m-d',$stock->tgl_gr);
                        if($stockDate->isPast() == true)
                        {
                            if(Carbon::now()->diffInDays($stockDate) > 120)
                            {
                                $return += $stock->sisa;
                            }
                        }

                    }
                }
            }
        }
        return $return;
    }
}