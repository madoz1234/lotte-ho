<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Kategori3 extends Model
{	
    protected $table = 'ref_kategori3';
    protected $fillable = ['divisi_kode','kategori2_kode','kode','nama'];

    public function kategori2(){
    	return $this->belongsTo(Kategori2::class, 'kategori2_kode', 'kode');
    }

    public function kategori4(){
    	return $this->hasMany(Kategori4::class, 'kategori3_kode', 'kode');
    }
}