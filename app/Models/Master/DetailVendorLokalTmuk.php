<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\JenisKustomer;
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\Pajak;

class DetailVendorLokalTmuk extends Model
{	
    protected $table = 'ref_detail_vendor_tmuk';
    protected $fillable = ['vendor_tmuk_id', 'tmuk_id'];
    // protected $fillable = ['kode', 'nama', 'alamat', 'kota_id', 'kecamatan_id', 'kode_pos', 'nama_kotak', 'telepon', 'email', 'top', 'lead_time', 'jadwal_order', 'npwp_perusahaan', 'nama_perusahaan', 'alamat_perusahaan', 'rekening_escrow_id', 'kode_bank', 'akun_escrow_id', 'nomor_rekening'];
    // 

    public function vendor(){
        return $this->belongsTo(VendorLokalTmuk::class, 'vendor_tmuk_id', 'id');
    }
    public function tmuk(){
        return $this->belongsTo(Tmuk::class, 'tmuk_id', 'id');
    }
}