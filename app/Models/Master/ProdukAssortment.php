<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

use Lotte\Models\Master\JenisAssortment;
use Lotte\Models\Master\Produk;

class ProdukAssortment extends Model
{	
    protected $table = 'ref_produk_assortment';
    protected $fillable = ['assortment_type_id','produk_kode'];
    
    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode', 'kode');
    }
    
    public function assortment()
    {
        return $this->belongsTo(JenisAssortment::class, 'assortment_type_id', 'id');
    }

    public function produksetting()
    {
        return $this->belongsTo(ProdukSetting::class, 'produk_kode', 'produk_kode');
    }

    

}