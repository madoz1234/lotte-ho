<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\Provinsi;

class Kota extends Model
{	
    protected $table = 'ref_kota';
    protected $fillable = ['nama', 'provinsi_id'];
    
    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'provinsi_id', 'id');
    }
}
