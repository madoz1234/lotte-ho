<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Diskon extends Model
{	
    protected $table = 'ref_detail_promosi_diskon';
    protected $fillable = ['promosi_id', 'kode_produk', 'nama_produk', 'cost_price', 'harga_terdiskon', 'diskon', 'harga_awal', 'status'];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'kode_produk', 'kode');
    }
}