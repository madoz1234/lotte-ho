<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

use Lotte\Models\Master\ProdukTmuk;

use Carbon\Carbon;

class GRDetail extends Model
{	
    protected $table = 'trans_persediaan_gr_detail';
    protected $fillable = ['produk_tmuk_kode','qty','created_at','updated_at','date'];

    public function produktmuk()
    {
        return $this->belongsTo(ProdukTmuk::class, 'produk_tmuk_kode', 'id');
    }
}