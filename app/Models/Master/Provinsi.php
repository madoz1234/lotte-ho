<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Provinsi extends Model
{	
    protected $table = 'ref_provinsi';
    protected $fillable = ['nama'];
}
