<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\TipeBarang;
use Lotte\Models\Master\Rak;

class ProdukSetting extends Model
{	
    protected $table = 'ref_produk_setting';
    protected $fillable = ['produk_kode','tipe_barang_kode','jenis_barang_kode','locked_price','margin','uom1_produk_description','uom2_produk_description','uom3_produk_description','uom4_produk_description','uom1_satuan','uom1_selling_cek','uom1_order_cek','uom1_receiving_cek','uom1_return_cek','uom1_inventory_cek','uom1_rak_id','uom1_barcode','uom1_conversion','uom1_width','uom1_height','uom1_length','uom1_weight','uom1_boxtype','uom1_file','uom2_satuan','uom2_selling_cek','uom2_order_cek','uom2_receiving_cek','uom2_return_cek','uom2_inventory_cek','uom2_rak_id','uom2_barcode','uom2_conversion','uom2_width','uom2_height','uom2_length','uom2_weight','uom2_boxtype','uom2_file','uom3_satuan','uom3_selling_cek','uom3_order_cek','uom3_receiving_cek','uom3_return_cek','uom3_inventory_cek','uom3_rak_id','uom3_barcode','uom3_conversion','uom3_width','uom3_height','uom3_length','uom3_weight','uom3_boxtype','uom3_file','uom4_satuan','uom4_selling_cek','uom4_order_cek','uom4_receiving_cek','uom4_return_cek','uom4_inventory_cek','uom4_rak_id','uom4_barcode','uom4_conversion','uom4_width','uom4_height','uom4_length','uom4_weight','uom4_boxtype','uom4_file', 'tipe_produk','tipe_asset_id'];
    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode', 'kode');
    }

    public function tipebarang()
    {
        return $this->belongsTo(TipeBarang::class, 'tipe_barang_kode', 'kode');
    }

    public function jenisbarang()
    {
        return $this->belongsTo(JenisBarang::class, 'jenis_barang_kode', 'kode');
    }

    public function raksatu()
    {
        return $this->belongsTo(Rak::class, 'uom1_rak_id', 'id');
    }
    public function rakdua()
    {
        return $this->belongsTo(Rak::class, 'uom2_rak_id', 'id');
    }
    public function raktiga()
    {
        return $this->belongsTo(Rak::class, 'uom3_rak_id', 'id');
    }
    public function rakempat()
    {
        return $this->belongsTo(Rak::class, 'uom4_rak_id', 'id');
    }
}