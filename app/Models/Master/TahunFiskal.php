<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class TahunFiskal extends Model
{	
    protected $table = 'ref_tahun_fiskal';
    protected $fillable = ['tgl_awal', 'tgl_akhir', 'status'];

    /* fungsi biasa */
    public static function getTahunFiskal()
    {
    	$rcd = static::where('status', 1)->first();
    	if (!$rcd) {
    		$rcd = static::orderBy('tgl_awal', 'desc')->first();
    	}

    	return $rcd;
    }
}
