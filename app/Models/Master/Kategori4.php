<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Kategori4 extends Model
{	
    protected $table = 'ref_kategori4';
    protected $fillable = ['divisi_kode','kategori3_kode','kode','nama'];

    public function kategori3(){
    	return $this->belongsTo(Kategori3::class, 'kategori3_kode', 'kode');
    }
}