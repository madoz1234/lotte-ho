<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

use Lotte\Models\Master\ProdukTmuk;

use Carbon\Carbon;

class ProdukTmukDetail extends Model
{	
    protected $table = 'ref_produk_tmuk_detail';
    protected $fillable = ['produk_tmuk_kode','qty','hpp','nilai_persediaan','created_at','updated_at','date'];

    public function produktmuk()
    {
        return $this->belongsTo(ProdukTmuk::class, 'produk_tmuk_kode', 'id');
    }
}