<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Promo extends Model
{	
    protected $table = 'ref_detail_promosi_promo';
    protected $fillable = ['promosi_id', 'kode_produk', 'nama_produk', 'selling_price', 'harga_terdiskon', 'diskon', 'status'];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'kode_produk', 'kode');
    }
}