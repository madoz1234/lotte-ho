<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\JenisKustomer;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransMemberPoint;
use Lotte\Models\Trans\TransMemberPiutang;

class MemberCard extends Model
{	
    protected $table = 'ref_membercard';
    protected $fillable = ['nomor', 'nama', 'telepon', 'email', 'jeniskustomer_id', 'tmuk_id', 'alamat', 'notes', 'limit_kredit', 'tmuk_kode'];

  	public function kustomer(){
        return $this->belongsTo(JenisKustomer::class, 'jeniskustomer_id', 'id');
    }

    public function tmuk(){
        return $this->belongsTo(Tmuk::class, 'tmuk_id', 'id');
    }

    public function tmukkode(){
        return $this->belongsTo(Tmuk::class, 'tmuk_kode', 'kode');
    }

    public function point(){
        return $this->hasOne(TransMemberPoint::class, 'no_member', 'nomor');
    }

    public function piutang(){
        return $this->hasOne(TransMemberPiutang::class, 'no_member', 'nomor');
    }
}