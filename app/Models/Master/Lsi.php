<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Kota;
use Lotte\Models\Master\Provinsi;
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\Pajak;

class Lsi extends Model
{	
    protected $table = 'ref_lsi';
    protected $fillable = ['region_id', 'kota_id', 'rekening_escrow_id', 'bank_escrow_id', 'pajak_id', 'kode', 'nama', 'alamat', 'kode_pos', 'telepon', 'email', 'longitude', 'latitude'];

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id', 'id');
    }

    // public function provinsi()
    // {
    //     return $this->belongsTo(Provinsi::class, 'kota_id', 'id');
    // }

    public function rekeningescrow()
    {
        return $this->belongsTo(RekeningEscrow::class, 'rekening_escrow_id', 'id');
    }
    public function bankescrow()
    {
        return $this->belongsTo(BankEscrow::class, 'bank_escrow_id', 'id');
    }
    public function pajak()
    {
        return $this->belongsTo(Pajak::class, 'pajak_id', 'id');
    }
    public function tmuk(){
        return $this->hasMany(Tmuk::class, 'lsi_id');
    }
}
