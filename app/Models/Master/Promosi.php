<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\JenisKustomer;
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\Pajak;

class Promosi extends Model
{	
    protected $table = 'ref_promosi';
    protected $fillable = ['region_id', 'lsi_id', 'tgl_awal', 'tgl_akhir', 'nama', 'kode_promo', 'tipe_promo', 'status'];
    
    public function rekeningescrow()
    {
        return $this->belongsTo(RekeningEscrow::class, 'rekening_escrow_id');
    }
    
    public function bankescrow()
    {
        return $this->belongsTo(BankEscrow::class, 'bank_escrow_id');
    }


    public function pajak()
    {
        return $this->belongsTo(Pajak::class, 'pajak_id');
    }

    public function detailTmuk(){
        return $this->belongsToMany(Tmuk::class, 'ref_detail_promosi_tmuk', 'promosi_id', 'tmuk_id');
    }
    
    public function assortment()
    {
        return $this->belongsTo(JenisAssortment::class, 'jenis_assortment_id');
    }

    public function jeniskustomer()
    {
        return $this->belongsTo(JenisKustomer::class, 'membercard_id');
    }

    public function membercard()
    {
        return $this->belongsTo(MemberCard::class, 'membercard_id');
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id');
    }

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan_id');
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'kota_id');
    }

    public function detailDiskon()
    {
        return $this->hasMany(Diskon::class, 'promosi_id', 'id');
    }

    public function detailPromo()
    {
        return $this->hasMany(Promo::class, 'promosi_id', 'id');
    }

    public function detailBuyxgety()
    {
        return $this->hasOne(BuyXGetY::class, 'promosi_id', 'id');
    }

    public function detailPurchase()
    {
        return $this->hasOne(Purchase::class, 'promosi_id', 'id');
    }
}