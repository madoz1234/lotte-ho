<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Coa2 extends Model
{	
    protected $table = 'ref_coa2';
    protected $fillable = ['coa1_kode','kode','nama'];


    public function coa3(){
    	return $this->hasMany(Coa3::class, 'coa2_kode', 'kode');
    }
}