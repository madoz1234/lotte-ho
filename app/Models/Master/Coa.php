<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Trans\TransJurnal;

class Coa extends Model
{	
    protected $table = 'ref_coa';
    protected $fillable = ['tipe_coa_id','kode','nama','parent_kode','status'];

    public function type(){
    	return $this->belongsTo(TipeCoa::class, 'tipe_coa_id', 'id' );
    }

    // public function kode(){
    // 	return $this->belongsTo(TipeCoa::class, 'nama', 'kode');
    // }

    public function coaType(){
        return $this->hasMany(TipeCoa::class, 'tipe_coa_id', 'id' );
    }

    public function coaParent(){
        return $this->hasMany(Coa::class,  'parent_kode', 'kode' );
    }

    public function coaChilds(){
        return $this->hasMany(Coa::class,  'parent_kode', 'kode' );
    }


    public function childs(){
    	return $this->hasMany(Coa::class,  'parent_kode', 'kode' );
    }

    public function parent(){
        return $this->belongsTo(Coa::class,  'parent_kode', 'kode' );
    }    

    public function jurnal(){
        return $this->hasMany(TransJurnal::class,  'coa_kode', 'kode')->orderBy('tanggal', 'DESC');
    }

    //query

    public static function getDataCoaPertahun($param=[]){
        $tmuk = '';
        foreach ($param['tmuk'] as $key=>$val) {
            if($key == 0){
                $tmuk .= "'".$val."'";
            }else{
                $tmuk .= ",'".$val."'";
            }
        }

        return Coa::from(\DB::raw("(
            select rc.kode as kode_coa, tj.tmuk_kode as tmuk_kode,
            rt.nama as nama_tmuk, rc.nama as nama_coa, 
            EXTRACT(YEAR FROM tj.tanggal) as tahun, 
            EXTRACT(MONTH FROM tj.tanggal) as bulan, 
            sum(tj.jumlah) as jumlah

            from ref_coa rc 
            join trans_jurnal tj on rc.kode = tj.coa_kode 
            join ref_tmuk rt on tj.tmuk_kode = rt.kode
            WHERE tj.tmuk_kode in (".$tmuk.")  
            and   tj.tanggal between '".$param['from']."' and '".$param['to']."'
            group by nama_coa,tmuk_kode,kode_coa,bulan,tahun, rt.nama order by kode_coa asc
        ) tbl"));
    }
}