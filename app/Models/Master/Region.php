<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Region extends Model
{	
    protected $table = 'ref_region';
    protected $fillable = ['kode', 'area'];

    public static function generateCode()
    {
        $last = static::orderBy('kode', 'desc')->first();
        $kode = (!is_null($last) && $k = $last->kode) ? intval($last->kode) : 0;

        return str_pad($kode + 1, 5, "0", STR_PAD_LEFT);
    }

    public function lsi(){
    	return $this->hasMany(Lsi::class, 'region_id' );
    }
}
