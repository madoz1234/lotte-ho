<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Kategori2 extends Model
{	
    protected $table = 'ref_kategori2';
    protected $fillable = ['divisi_kode','kategori1_kode','kode','nama'];

    public function kategori1(){
    	return $this->belongsTo(Kategori1::class, 'kategori1_kode', 'kode');
    }

    public function kategori3(){
    	return $this->hasMany(Kategori3::class, 'kategori2_kode', 'kode');
    }
}