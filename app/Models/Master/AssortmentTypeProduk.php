<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class AssortmentTypeProduk extends Model
{	
    protected $table = 'ref_produk_assortment';
    protected $fillable = ['assortment_type_id','produk_kode'];
    
    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode');
    }
}