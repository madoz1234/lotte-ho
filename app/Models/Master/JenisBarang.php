<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class JenisBarang extends Model
{	
    protected $table = 'ref_jenis_barang';
    protected $fillable = ['kode','jenis'];

    public static function generateCode()
    {
        $last = static::orderBy('kode', 'desc')->first();
        $kode = (!is_null($last) && $k = $last->kode) ? intval($last->kode) : 0;

        return str_pad($kode + 1, 3, "0", STR_PAD_LEFT);
    }

}
