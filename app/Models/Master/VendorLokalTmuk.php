<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\JenisKustomer;
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\Pajak;
use Lotte\Models\Master\DetailVendorLokalTmuk;

class VendorLokalTmuk extends Model
{	
    protected $table = 'ref_vendor_tmuk';
    protected $fillable = ['kode', 'nama', 'alamat', 'kota_id', 'kecamatan_id', 'kode_pos', 'nama_kotak', 'telepon', 'email', 'top', 'lead_time', 'pajak_id', 'rekening_escrow_id', 'bank_escrow_id', 'order_senin', 'order_selasa', 'order_rabu', 'order_kamis', 'order_jumat', 'order_sabtu', 'order_minggu', 'contact_person', 'provinsi_id'];
    // protected $fillable = ['kode', 'nama', 'alamat', 'kota_id', 'kecamatan_id', 'kode_pos', 'nama_kotak', 'telepon', 'email', 'top', 'lead_time', 'jadwal_order', 'npwp_perusahaan', 'nama_perusahaan', 'alamat_perusahaan', 'rekening_escrow_id', 'kode_bank', 'akun_escrow_id', 'nomor_rekening'];
    // 
    public static function generateCode()
    {
        $last = static::orderBy('kode', 'desc')->first();
        $kode = (!is_null($last) && $k = $last->kode) ? intval($last->kode) : 0;

        return str_pad($kode + 1, 5, "0", STR_PAD_LEFT);
    }
    
    public function rekeningescrow()
    {
        return $this->belongsTo(RekeningEscrow::class, 'rekening_escrow_id', 'id');
    }
    
    public function bankescrow()
    {
        return $this->belongsTo(BankEscrow::class, 'bank_escrow_id', 'id');
    }


    public function pajak()
    {
        return $this->belongsTo(Pajak::class, 'pajak_id', 'id');
    }

    public function detailVendor(){
        return $this->belongsToMany(Tmuk::class, 'ref_detail_vendor_tmuk', 'vendor_tmuk_id', 'tmuk_id', 'id');
    }
    
    public function assortment()
    {
        return $this->belongsTo(JenisAssortment::class, 'jenis_assortment_id', 'id');
    }
    public function jeniskustomer()
    {
        return $this->belongsTo(JenisKustomer::class, 'membercard_id', 'id');
    }
    public function membercard()
    {
        return $this->belongsTo(MemberCard::class, 'membercard_id', 'id');
    }
    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }
    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id', 'id');
    }
    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan_id', 'id');
    }
    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'provinsi_id', 'id');
    }
}