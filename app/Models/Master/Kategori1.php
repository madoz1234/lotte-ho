<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Kategori1 extends Model
{	
    protected $table = 'ref_kategori1';
    protected $fillable = ['divisi_kode','kode','nama'];

    public function divisi(){
    	return $this->belongsTo(DivisiProduk::class, 'divisi_kode', 'kode');
    }

    public function kategori2(){
    	return $this->hasMany(Kategori2::class, 'kategori1_kode', 'kode');
    }
}