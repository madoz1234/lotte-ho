<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Pajak extends Model
{	
    protected $table = 'ref_pajak';
    protected $fillable = ['npwp', 'nama', 'alamat_npwp'];
}