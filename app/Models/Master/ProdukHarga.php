<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;

class ProdukHarga extends Model
{	
    protected $table = 'ref_harga_tmuk';
    protected $fillable = ['tmuk_kode','produk_kode','cost_price','suggest_price','change_price','margin_amount','map','gmd_price','created_at', 'created_by', 'update_at', 'update_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode', 'kode');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode', 'kode');
    }

    public function produksetting()
    {
        return $this->belongsTo(ProdukSetting::class, 'produk_kode', 'produk_kode');
    }
}