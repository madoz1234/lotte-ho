<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Truk extends Model
{	
    protected $table = 'ref_truk';
    protected $fillable = ['tipe','tinggi','panjang','lebar','kapasitas','volume'];
}