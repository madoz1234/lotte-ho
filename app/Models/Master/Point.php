<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Point extends Model
{	
    protected $table = 'ref_point';
    protected $fillable = ['tgl_berlaku', 'konversi', 'status', 'faktor_konversi', 'faktor_reedem'];

}