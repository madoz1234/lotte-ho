<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class Backup extends Model
{	
    protected $table = 'ref_backup';
    protected $fillable = ['nama'];
}
