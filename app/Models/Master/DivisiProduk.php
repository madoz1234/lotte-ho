<?php

namespace Lotte\Models\Master;

use Lotte\Models\Model;

class DivisiProduk extends Model
{	
    protected $table = 'ref_divisi';
    protected $fillable = ['kode','nama'];

    public function kategori1(){
    	return $this->hasMany(Kategori1::class, 'divisi_kode', 'kode');
    }
}