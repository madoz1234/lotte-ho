<?php

namespace Lotte\Models;

use Illuminate\Database\Eloquent\Model as Base;
use Lotte\Models\Traits\EntryBy;
use Lotte\Models\Traits\RaidModel;
use Lotte\Models\Traits\Utilities;


class Model extends Base
{
    use RaidModel, Utilities, EntryBy;
}
