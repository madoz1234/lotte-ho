<?php 
namespace Lotte\Models\Entrust;

use Zizaco\Entrust\EntrustRole;
use App\Models\Entrust\Permission;

class Role extends EntrustRole
{
	protected $fillable 	= ['name', 'display_name', 'description'];
}