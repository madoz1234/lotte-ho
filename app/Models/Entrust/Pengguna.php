<?php

namespace Lotte\Models\Entrust;

use Lotte\Models\Model;
use Lotte\Models\User;
use Lotte\Models\Master\Lsi;

class Pengguna extends Model
{	
    protected $table = 'ref_pengguna';
    protected $fillable = [
		'user_id',
		'nama_lengkap',
		'telepon',
		'email',
		'lsi_code',
		'deleted_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function lsi()
    {
        return $this->hasOne(Lsi::class, 'id', 'lsi_id');
    }
}
