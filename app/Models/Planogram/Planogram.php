<?php

namespace Lotte\Models\Planogram;

use Lotte\Models\Model;
use Lotte\Models\Master\JenisAssortment;

class Planogram extends Model
{
    protected $table = 'ref_planogram';
    protected $fillable = ['kode','nama', 'jenis_assortment_id', 'status'];
    protected $appends = ['pdf_url', 'pdf_path'];
    
    public function getPdfUrlAttribute()
    {
        return url("utama/planogram/rencana-planogram/{$this->id}/print");
    }

    public function getPdfPathAttribute()
    {
        return url("planograms/{$this->kode}.pdf");
    }

    public function assortment()
    {
        return $this->belongsTo(JenisAssortment::class, 'jenis_assortment_id');
    }

    public function detail()
    {
        return $this->hasMany(Detail::class, 'planogram_id');
    }
}
