<?php

namespace Lotte\Models\Planogram;

use Lotte\Models\Model;
use Lotte\Models\Master\Rak;
use Lotte\Models\Master\Produk;

class Detail extends Model
{
    protected $table = 'ref_planogram_detail';
    protected $fillable = ['planogram_id','rak_id', 'nomor_rak', 'planogram_json'];
    protected $appends = ['planogram_array'];
    // protected $casts = [
    //     'planogram_json' => 'array',
    // ];
    
    public function planogram()
    {
        return $this->belongsTo(Planogram::class, 'planogram_id');
    }

    public function rak()
    {
        return $this->belongsTo(Rak::class, 'rak_id');
    }

    public function getPlanogramArrayAttribute()
    {
        return json_decode($this->planogram_json);
    }

    public function renderRack()
    {
        $data = $this->planogram_array;
        $res = [];

        foreach ($data as $row) {
            $r = [];
            foreach ($row as $prod) {
                foreach ($prod->items as $item) {
                    $r[] = [
                        'produk' => Produk::with('produksetting')->where('kode', $prod->kode)->first()->toArray(),
                        'unit' => $prod->unit,
                        'facing' => $prod->facing,
                        'stack' => $item->stack,
                        'urutan' => $item->urutan,
                    ];
                }
            }
            usort($r, function($a, $b) {
                return $a['urutan'] - $b['urutan'];
            });

            $res[] = $r;
        }

        return $res;
    }
}
