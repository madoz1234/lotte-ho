<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TipeAset;

class TransAkuisisiAset extends Model
{	
	protected $table = 'trans_akuisisi_aset';
	protected $fillable = ['tmuk_kode', 'tipe_id', 'nama', 'no_seri', 'tanggal_pembelian', 'nilai_pembelian', 'kondisi', 'status'];

	public function tmuk(){
		return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode' );
	}
	public function tipeaset(){
		return $this->belongsTo(TipeAset::class, 'tipe_id','id' );
	}

	public function getItemAset($tmuk_kode,$tipe_id,$start){
		$data = TransAkuisisiAset::where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
				->where(\DB::raw('tanggal_pembelian'),'<=',$start)
				->where('tipe_id',$tipe_id)->get();
		return $data;
	}

	public function getUmurEkonomis($tipe_id){
		$data = TipeAset::find($tipe_id);
		if($data){
			return ($data->tingkat_depresiasi/12);
		}else{
			return 1;
		}
	}

}

