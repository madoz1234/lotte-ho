<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Kontainer;
use Lotte\Models\Master\Produk;

class TransKontainerDetail extends Model
{	
    protected $table = 'trans_kontainer_detail';
    protected $fillable = ['id_trans_kontainer','id_kontainer','produk_kode','jumlah_produk','no_kontainer','created_at','created_by','updated_at','updated_by'];

    public function kontainer(){
        return $this->belongsTo(Kontainer::class, 'id_kontainer', 'id');
    }

    public function transkontainer(){
        return $this->belongsTo(TransKontainer::class, 'id_trans_kontainer', 'id');
    }

    public function produk(){
        return $this->belongsTo(Produk::class, 'produk_kode', 'kode');
    }
}
