<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Produk;
class TransPyrDetail extends Model
{	
    protected $table = 'trans_pyr_detail';
    protected $fillable = ['pyr_id','produk_kode','qty','unit','price','keterangan'];


    function produks(){
    	return $this->belongsTo(Produk::class, 'produk_kode', 'kode');
    }
    function produk_setting(){
        return $this->belongsTo(ProdukSetting::class, 'produk_kode', 'produk_kode');
    }

    function pyr(){
    	return $this->belongsTo(TransPyr::class, 'pyr_id', 'id');
    }

    public static function sumPriceByKeteranganPusat($pyr_id, $keterangan, $tmuk_kode)
    {
    	if($tmuk_kode!=NULL){
            return static::whereHas('pyr', function($pyr) use ($pyr_id, $tmuk_kode) {
                $pyr->where('nomor_pyr', 'ILIKE', '%'.$pyr_id.'%')
                ->where('tmuk_kode', $tmuk_kode);
            })->where('keterangan', $keterangan)->sum('price');
        }else{
             return static::whereHas('pyr', function($pyr) use ($pyr_id) {
                 $pyr->where('nomor_pyr', 'ILIKE', '%'.$pyr_id.'%');
             })->where('keterangan', $keterangan)->sum('price');
        }
    }

    public static function sumPriceByKeterangan($pyr_id, $keterangan, $tmuk_kode)
    {
        if($tmuk_kode!=NULL){
            return static::whereHas('pyr', function($pyr) use ($pyr_id, $tmuk_kode) {
                $pyr->where('nomor_pyr', 'ILIKE', '%'.$pyr_id.'%')
                ->where('tmuk_kode', $tmuk_kode);
            })->where('keterangan', $keterangan)->sum('price');
        }else{
             return static::whereHas('pyr', function($pyr) use ($pyr_id) {
                 $pyr->where('nomor_pyr', 'ILIKE', '%'.$pyr_id.'%');
             })->where('keterangan', $keterangan)->sum('price');
        }
    }


}
