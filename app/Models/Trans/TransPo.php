<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPoDetail;
use Lotte\Models\Trans\TransMuatanDetail;

class TransPo extends Model
{	
    protected $table = 'trans_po';
    protected $fillable = ['nomor','nomor_pr','tmuk_kode','tgl_buat','total','keterangan','status','created_at','created_by','updated_at','updated_by', 'initial_stock'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function pr()
    {
        return $this->belongsTo(TransPr::class, 'nomor_pr','nomor');
    }

    public function detail(){
        return $this->hasMany(TransPoDetail::class, 'po_nomor', 'nomor');
    }

    public function kontainer(){
        return $this->hasOne(TransKontainer::class, 'po_nomor', 'nomor');
    }

    public function muatandetail(){
        return $this->hasOne(TransMuatanDetail::class, 'po_nomor', 'nomor');
    }

    public function lsi()
    {
        return $this->belongsTo(lsi::class, 'lsi_kode','id');
    }

    public function produklsi()
    {
        return $this->belongsTo(ProdukLsi::class, 'lsi_kode','id');
    }

    public static function generateCode($tmuk)
    {
        $str = "PO-" . date('Ymd') . $tmuk;

        $rcd = static::where('nomor', 'like', $str . '%')->orderBy('nomor', 'desc')->first();

        $nomor = 1;
        if ($rcd) {
            $nomor = (int) substr($rcd->nomor, -5);
            $nomor++;
        }

        return $str . str_pad($nomor, 5, "0", STR_PAD_LEFT);
    }

    // query
    public static function getServiceLevel(){
        return TransPo::from(\DB::raw('(
            select tp.nomor as nomor_po,
            tp.tgl_buat as tanggal_po,
            tpd.created_at as tanggal_pr,
            tp.tmuk_kode,
            tp.nomor_pr,
            tpd.qty_pr,
            tpd.qty_po
            from trans_po tp
            join trans_po_detail tpd on tpd.po_nomor=tp.nomor
        ) tblx'));
    }
    
    public static function getTotalMonthsPo($dayStart, $tmuk_kode)
    {
      return static::whereBetween('tgl_buat', [$dayStart->startOfMonth()->format('Y-m-d'), $dayStart->endOfMonth()->format('Y-m-d')])->where('tmuk_kode',$tmuk_kode)->sum('total');
    }
}
