<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransPo;

class TransPr extends Model
{	
    protected $table = 'trans_pr';
    protected $fillable = ['nomor','lsi_kode','tmuk_kode','tgl_kirim','total','keterangan','created_at','created_by','updated_at','updated_by','status'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function po()
    {
        return $this->belongsTo(TransPo::class, 'nomor','nomor_pr');
    }

    public function lsi()
    {
        return $this->belongsTo(lsi::class, 'lsi_kode','id');
    }

    public function produklsi()
    {
        return $this->belongsTo(ProdukLsi::class, 'lsi_kode','id');
    }

    public function detailpr(){
        return $this->hasMany(TransPrDetail::class, 'pr_nomor', 'nomor');
    }
}
