<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Coa;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransPyr;

class TransJurnal extends Model
{	
    protected $table = 'trans_jurnal';
    protected $fillable = ['tanggal', 'tmuk_kode', 'coa_kode', 'referensi', 'deskripsi', 'idtrans', 'jumlah', 'posisi', 'flag', 'delete'];


    public function coa()
    {
        return $this->belongsTo(Coa::class, 'coa_kode', 'kode');
    }

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode', 'kode');
    }

    public function tmuk_produk(){
        
    }

    public function reduce(){
        return $this->hasOne(TransReduceEscrow::class, 'id_transaksi', 'idtrans');
    }

    public function pyr(){
        return $this->hasOne(TransPyr::class, 'id_transaksi', 'idtrans');
    }

    /* utility */
    public static function createJurnal($param)
    {
        foreach ($param as $value) {
            $rcd = new TransJurnal();
            $rcd->fill($value);
            $rcd->save();
        }
    }

    public static function getHppPenjualan($dayStart, $tmuk_kode)
    {
      return static::whereBetween('tanggal', [$dayStart->startOfMonth()->format('Y-m-d 00:00:00'), $dayStart->endOfMonth()->format('Y-m-d 23:59:59')])->where('tmuk_kode',$tmuk_kode)->where('coa_kode', '5.1.0.0')->where('delete', 0)->sum('jumlah');
    }

    public static function getTotalJumlah($dayStart, $tmuk_kode, $coa_kode)
    {
      return static::whereBetween('tanggal', [$dayStart->startOfMonth()->format('Y-m-d 00:00:00'), $dayStart->endOfMonth()->format('Y-m-d 23:59:59')])->where('tmuk_kode',$tmuk_kode)->where('coa_kode', $coa_kode)->where('delete', 0)->sum('jumlah');
    }

    public static function saldoAwalKemarin($tmuk_kode, $coa_kode, $tgl_awal, $tgl_akhir)
    {
        $tanggal          = [$tgl_awal, $tgl_akhir];
        $saldo_awal       = [];
        $saldo_akhir      = [];
        $total_saldo_awal = 0;
        $jumlah_debit     = 0;
        $jumlah_kredit    = 0;
        $bersih           = 0;

        $records = static::where('tmuk_kode', $tmuk_kode)
                        ->where('coa_kode', $coa_kode)
                        ->whereBetween('tanggal', $tanggal)
                        ->where('delete', 0)
                        ->get();

        if ($records) {
            foreach ($records as $key => $value) {
                if ($key == 0) {
                    $saldo_awal[$key] = 0;
                }else{
                    $saldo_awal[$key] = $saldo_akhir[$key-1];
                }
                // debet
                $jumlah_debit = ($value->posisi == 'D') ? $value->jumlah : 0;
                //kredit
                $jumlah_kredit = ($value->posisi == 'K') ? $value->jumlah : 0;
                //cek posisi normal coa
                $posisi_normal = $value->coa->posisi;
                if ($posisi_normal == 'D') {
                    $bersih = $jumlah_debit - $jumlah_kredit;
                }else{
                    $bersih = $jumlah_kredit - $jumlah_debit;                                        
                }

                $total_saldo_awal = $saldo_akhir[$key] = $saldo_awal[$key] + $bersih;
            }
        }

        return $total_saldo_awal;
    }

    public static function getJumlahByCoa($coa, $tmuk, $tanggal)
    {
        return static::where('tmuk_kode', $tmuk)->where('coa_kode', $coa)->where('tanggal', '>=' ,$tanggal->copy()->startOfMonth()->format('Y-m-d H:i:s'))->where('tanggal', '<=' ,$tanggal->copy()->endOfMonth()->format('Y-m-d H:i:s'))->sum('jumlah');
    }
}
