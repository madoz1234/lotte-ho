<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\User;

class TransLogAudit extends Model
{	
    protected $table = 'trans_log_audit';
    protected $fillable = ['tanggal', 'user_id', 'tanggal_transaksi', 'type', 'ref', 'aksi', 'amount'];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function setLog($param)
    {
    	// init param
    	$param['tanggal'] = date('Y-m-d H:i:s');
    	$param['user_id'] = \Auth::user()->id;

    	// proses
    	$rcd = new static;
    	$rcd->fill($param);
    	$rcd->save();

    	return $rcd;
    }
    
}
