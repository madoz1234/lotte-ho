<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransTopupEscrow extends Model
{	
    protected $table = 'trans_topup_escrow';
    protected $fillable = ['tmuk_kode','saldo_awal','tanggal','jenis','jumlah','lampiran','periode','status','total_jual','created_at', 'created_by', 'update_at', 'update_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function detail()
    {
        return $this->hasMany(TransTopupEscrowDetail::class, 'id_topup_escrow','id');
    }
}
