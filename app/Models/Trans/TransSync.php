<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransSync extends Model
{	
    protected $table = 'trans_sync';
    protected $fillable = ['tmuk_kode','jenis','created_at','created_by','updated_at','updated_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }
}
