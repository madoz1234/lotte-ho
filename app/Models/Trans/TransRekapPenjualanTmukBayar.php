<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;

class TransRekapPenjualanTmukBayar extends Model
{	
    protected $table = 'trans_rekap_penjualan_bayar';
    protected $fillable = ['rekap_penjualan_kode','tipe','bank_id','bank_name','total','no_kartu'];

    public function penjualan()
    {
        return $this->belongsTo(TransRekapPenjualanTmuk::class, 'rekap_penjualan_kode','jual_kode');
    }
    
}
