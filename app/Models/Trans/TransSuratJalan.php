<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransSuratJalan extends Model
{	
    protected $table = 'trans_surat_jalan';
    protected $fillable = ['nomor','nomor_muatan','created_at','created_by','updated_at','updated_by','supir','nomor_kendaraan'];

    public function muatan()
    {
        return $this->belongsTo(TransMuatan::class, 'nomor_muatan','nomor');
    }
    
    public static function generateCode($kode)
    {
        $str = "DN-" . date('Ymd') . $kode;

        $rcd = static::where('nomor', 'like', $str . '%')->orderBy('nomor', 'desc')->first();

        $nomor = 1;
        if ($rcd) {
            $nomor = (int) substr($rcd->nomor, -5);
            $nomor++;
        }

        return $str . str_pad($nomor, 5, "0", STR_PAD_LEFT);
    }
}
