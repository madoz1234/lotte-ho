<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransMemberPointDetail extends Model
{	
    protected $table = 'trans_member_point_detail';
    protected $fillable = ['id_trans_member_point','tanggal','point','created_at', 'created_by', 'update_at', 'update_by'];

}
