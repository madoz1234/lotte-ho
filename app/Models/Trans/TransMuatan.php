<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransMuatan extends Model
{	
    protected $table = 'trans_muatan';
    protected $fillable = ['nomor','status','created_at','created_by','updated_at','updated_by'];

    // public function tmuk()
    // {
    //     return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    // }

    public function suratjalan()
    {
        return $this->hasOne(TransSuratJalan::class, 'nomor_muatan','nomor');
    }

    public function detail(){
        return $this->hasMany(TransMuatanDetail::class, 'nomor_muatan', 'nomor');
    }

    public static function generateCode($kode)
    {
        $str = "LT-" . date('Ymd') . $kode;

        $rcd = static::where('nomor', 'like', $str . '%')->orderBy('nomor', 'desc')->first();

        $nomor = 1;
        if ($rcd) {
            $nomor = (int) substr($rcd->nomor, -5);
            $nomor++;
        }

        return $str . str_pad($nomor, 5, "0", STR_PAD_LEFT);
    }
}
