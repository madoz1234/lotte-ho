<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Trans\TransOpening;
use Lotte\Models\Master\Produk;

class TransDetailOpening extends Model
{	
    protected $table = 'trans_detail_opening';
    protected $fillable = ['opening_id','produk_kode','qty_po','unit_po','unit_price','price'];

    public function opening()
    {
        return $this->belongsTo(TransOpening::class, 'id','opening_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode','kode');
    }
}
