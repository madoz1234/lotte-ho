<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Produk;
use Lotte\Models\Trans\TransPr;

class TransPrDetail extends Model
{	
    protected $table = 'trans_pr_detail';
    protected $fillable = ['pr_id','produk_kode','deskrisi','qty_order','unit_order','unit_order_price','qty_sell','unit_sell','qty_pr','unit_pr','total_price'];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode','kode');
    }


    public function pr(){
    	return $this->belongsTo(TransPr::class, 'nomor_pr', 'nomor_pr');
    }


}
