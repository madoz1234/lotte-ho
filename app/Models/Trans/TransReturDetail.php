<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransRetur;
use Lotte\Models\Master\Produk;

class TransReturDetail extends Model
{	
    protected $table = 'trans_retur_detail';
    protected $fillable = ['retur_id','produk_kode','qty','catatan','qty_retur','reason','created_at','created_by','updated_at','updated_by'];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode','kode');
    }

    public function detail()
    {
        return $this->belongsTo(TransRetur::class, 'retur_id','id');
    }
}
