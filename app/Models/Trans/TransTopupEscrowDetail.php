<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransTopupEscrowDetail extends Model
{	
    protected $table = 'trans_topup_escrow_detail';
    protected $fillable = ['id_topup_escrow','week','total','created_at', 'created_by', 'update_at', 'update_by'];
}
