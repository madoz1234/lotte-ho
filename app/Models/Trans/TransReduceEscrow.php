<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\User;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransReduceEscrowDetail;
use Lotte\Models\Trans\TransJurnal;

class TransReduceEscrow extends Model
{	
    protected $table = 'trans_reduce_escrow';
    protected $fillable = ['id', 'tanggal','po_nomor','tmuk_kode','saldo_dipotong','saldo_scn','saldo_escrow','verifikasi1_date','verifikasi1_user','verifikasi2_date','verifikasi2_user', 'verifikasi3_date', 'verifikasi3_user', 'verifikasi4_date', 'verifikasi4_user','id_transaksi', 'created_at', 'created_by', 'update_at', 'update_by', 'statush2h', 'keterangan','status'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function detailreduce()
    {
        return $this->hasMany(TransReduceEscrowDetail::class, 'reduce_escrow_id','id');
    }

    public function transpo()
    {
        return $this->belongsTo(TransPo::class, 'po_nomor','nomor');
    }

    public function verifikasiuser1()
    {
        return $this->belongsTo(User::class, 'verifikasi1_user');
    }

    public function verifikasiuser2()
    {
        return $this->belongsTo(User::class, 'verifikasi2_user');
    }

    public function verifikasiuser3()
    {
        return $this->belongsTo(User::class, 'verifikasi3_user');
    }

    public function verifikasiuser4()
    {
        return $this->belongsTo(User::class, 'verifikasi4_user');
    }

    public function jurnal(){
        return $this->belongsTo(TransJurnal::class, 'id_transaksi','idtrans');
    }
}