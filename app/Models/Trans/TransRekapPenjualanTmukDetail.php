<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Master\Produk;

class TransRekapPenjualanTmukDetail extends Model
{	
    protected $table = 'trans_rekap_penjualan_detail';
    protected $fillable = ['rekap_penjualan_kode','item_code','barcode','description','harga','harga_member','qty','satuan','discount','disc_percent','disc_amount', 'total', 'promo', 'tax_type', 'harga_hpp3'];

    public function rekappenjualan()
    {
        return $this->belongsTo(TransRekapPenjualanTmuk::class, 'rekap_penjualan_kode','jual_kode');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'item_code','kode');
    }

    public static function getTotalHpp($dayStart, $tmuk_kode)
    {
      return static::whereBetween('updated_at', [$dayStart->startOfMonth()->format('Y-m-d'), $dayStart
      ->endOfMonth()->format('Y-m-d')])
      ->whereHas('rekappenjualan', function($r) use ($tmuk_kode){
        if($tmuk_kode){
            $r->where('tmuk_kode', $tmuk_kode);
        }

      })->sum('harga_hpp3');
    }

    public static function getDataPerBarang($start='', $end=''){
        $query = '';
        if($start and $end){
            $query = "where trp.tanggal between '".$start."' and '".$end."'";
        }
        // dd($query);
        return TransRekapPenjualanTmukDetail::from(\DB::raw("(
                select DATE(trp.tanggal) as tanggal ,
                trp.tmuk_kode as tmuk_kod,
                trpd.item_code as item_code, 
                trpd.description as description,
                SUM(trpd.qty) as jml_qty,
                trpd.harga as harga,
                SUM(rht.map) as map,
                SUM(trpd.total) as jml_total 
                from trans_rekap_penjualan_detail trpd 
                join trans_rekap_penjualan trp on trpd.rekap_penjualan_kode = trp.jual_kode 
                join ref_harga_tmuk rht on trpd.item_code = rht.produk_kode 
                
                group by tmuk_kod, tanggal, item_code, harga, description order by tanggal desc
            ) tblx 
        "));
    }
    
}
