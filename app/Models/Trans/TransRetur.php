<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransReturDetail;

class TransRetur extends Model
{	
    protected $table = 'trans_retur';
    protected $fillable = ['tmuk_kode','nomor_po','nomor_retur','created_at','created_by','updated_at','updated_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }
    
    public function lsi()
    {
        return $this->belongsTo(lsi::class, 'lsi_kode','id');
    }

    public function po()
    {
        return $this->belongsTo(TransPo::class, 'nomor_po','id');
    }

    public function retur_detail()
    {
        return $this->hasMany(TransReturDetail::class, 'retur_id','id');
    }
}
