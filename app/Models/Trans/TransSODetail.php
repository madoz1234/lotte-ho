<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;


class TransSODetail extends Model
{	
    protected $table = 'trans_so_detail';
    protected $fillable = ['trans_so_id','produk_kode','qty_barcode','qty_system','hpp','created_at','created_by','updated_at','updated_by'];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode','kode');
    }

     public function produksetting(){
        return $this->belongsTo(ProdukSetting::class, 'produk_kode','produk_kode' );
    }
}
