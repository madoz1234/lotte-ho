<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Trans\TransDetailDirectInvoice;

class TransDirectInvoice extends Model
{	
    protected $table = 'trans_direct_invoice';
    protected $fillable = ['nomor','tmuk_kode','tgl_buat','tgl_kirim','created_at','created_by','updated_at','updated_by', 'vendor_lokal_id'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function vendor()
    {
        return $this->belongsTo(VendorLokalTmuk::class, 'vendor_lokal_id','id');
    }

    public function detaildirect()
    {
        return $this->hasMany(TransDetailDirectInvoice::class, 'direct_invoice_id','id');
    }

    public static function generateCodePr($kode)
    {
// dd($kode);
        $str = "DPR-" . date('Ymd') . $kode;

        $rcd = static::where('nomor', 'like', $str . '%')->orderBy('nomor', 'desc')->first();

        // dd($rcd);

        $nomor = 1;
        if ($rcd) {
            $nomor = (int) substr($rcd->nomor, -5);
            $nomor++;
        }

        // dd($nomor);

        return $str . str_pad($nomor, 5, "0", STR_PAD_LEFT);

    }

    public static function generateCodePyr($kode)
    {
// dd($kode);
        $str = "DPYR-" . date('Ymd') . $kode;

        $rcd = static::where('nomor', 'like', $str . '%')->orderBy('nomor', 'desc')->first();

        // dd($rcd);

        $nomor = 1;
        if ($rcd) {
            $nomor = (int) substr($rcd->nomor, -5);
            $nomor++;
        }

        // dd($nomor);

        return $str . str_pad($nomor, 5, "0", STR_PAD_LEFT);

    }

    public static function generateCodeOpening($kode)
    {
        $str = "OPENING-" . date('Ymd') . $kode;
        $rcd = static::where('nomor', 'like', $str . '%')->orderBy('nomor', 'desc')->first();
        $nomor = 1;
        if ($rcd) {
            $nomor = (int) substr($rcd->nomor, -5);
            $nomor++;
        }
        return $str . str_pad($nomor, 5, "0", STR_PAD_LEFT);
    }


}
