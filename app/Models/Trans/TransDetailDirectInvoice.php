<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Trans\TransDirectInvoice;
use Lotte\Models\Master\Produk;

class TransDetailDirectInvoice extends Model
{	
    protected $table = 'trans_detail_direct_invoice';
    protected $fillable = ['direct_invoice_id','produk_kode','qty_po','unit_po','unit_price','price','keterangan'];

    public function direct()
    {
        return $this->belongsTo(TransDirectInvoice::class, 'id','direct_invoice_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode','kode');
    }
}
