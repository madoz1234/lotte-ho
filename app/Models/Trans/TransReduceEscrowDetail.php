<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;

class TransReduceEscrowDetail extends Model
{	
    protected $table = 'trans_reduce_escrow_detail';
    protected $fillable = ['reduce_escrow_id','nomor_struk','total_belanja','unit', 'created_at', 'created_by', 'update_at', 'update_by'];


}
