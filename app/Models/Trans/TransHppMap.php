<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransPo;

class TransHppMap extends Model
{	
    protected $table = 'trans_hpp_map';
    protected $fillable = ['tanggal','po_nomor','produk_kode','tmuk_kode','qty','price','map','created_at', 'created_by', 'update_at', 'update_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function detailPo()
    {
        return $this->belongsTo(TransPo::class, 'po_nomor','nomor');
    }
}