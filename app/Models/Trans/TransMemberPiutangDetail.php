<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransMemberPiutangDetail extends Model
{	
    protected $table = 'trans_member_piutang_detail';
    protected $fillable = ['id_trans_member_piutang','tanggal','hutang','created_at', 'created_by', 'update_at', 'update_by'];

    public function piutang()
    {
        return $this->belongsTo(TransMemberPiutang::class, 'id_trans_member_piutang','id');
    }

}
