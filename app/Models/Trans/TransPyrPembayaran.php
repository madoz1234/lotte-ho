<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\User;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransPyr;

class TransPyrPembayaran extends Model
{	
    protected $table = 'trans_pyr_pembayaran';
    protected $fillable = ['nomor_pyr','tmuk_kode','saldo_deposit','saldo_scn','saldo_escrow','biaya_admin','status','created_at','created_by','updated_at','updated_by', 'verifikasi1_date','verifikasi1_user','verifikasi2_date','verifikasi2_user', 'verifikasi3_date', 'verifikasi3_user', 'verifikasi4_date', 'verifikasi4_user','group','statush2h','keterangan','nomorh2h'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }
    
    public function pyr()
    {
        return $this->belongsTo(TransPyr::class, 'nomor_pyr','nomor_pyr');
    }

    public function verifikasiuser1()
    {
        return $this->belongsTo(User::class, 'verifikasi1_user','id');
    }

    public function verifikasiuser2()
    {
        return $this->belongsTo(User::class, 'verifikasi2_user','id');
    }

    public function verifikasiuser3()
    {
        return $this->belongsTo(User::class, 'verifikasi3_user','id');
    }

    public function verifikasiuser4()
    {
        return $this->belongsTo(User::class, 'verifikasi4_user','id');
    }

    public function groups(){
        return $this->hasMany(TransPyrPembayaran::class, 'group','nomor_pyr');
    }
}
