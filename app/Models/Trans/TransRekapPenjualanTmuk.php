<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Trans\TransRekapPenjualanTmukBayar;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\Tmuk;

class TransRekapPenjualanTmuk extends Model
{   
    protected $table = 'trans_rekap_penjualan';
    protected $fillable = ['jual_kode', 'tmuk_kode', 'member_nomor', 'kasir_id', 'kassa_id', 'shift_id', 'reference_id', 'tanggal', 'subtotal', 'tax', 'discount', 'total', 'kembalian', 'note', 'flag_sum', 'jam', 'flag_sync','flag_delete'];

    public function bayar(){
        return $this->hasMany(TransRekapPenjualanTmukBayar::class, 'rekap_penjualan_kode', 'jual_kode');
    }

    public function detil(){
        return $this->hasMany(TransRekapPenjualanTmukDetail::class, 'rekap_penjualan_kode', 'jual_kode');
    }

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }
    public function jurnal()
    {
        return $this->belongsTo(TransJurnal::class, 'tmuk_kode','tmuk_kode');
    }

    public static function getTotalMonths($dayStart, $tmuk_kode)
    {
      return static::whereBetween('tanggal', [$dayStart->startOfMonth()->format('Y-m-d 00:00:00'), $dayStart->endOfMonth()->format('Y-m-d 23:59:59')])->where('tmuk_kode',$tmuk_kode)->sum('total');
    }

    public static function getCountMonths($dayStart, $tmuk_kode)
    {
      return static::whereBetween('tanggal', [$dayStart->startOfMonth()->format('Y-m-d 00:00:00'), $dayStart->endOfMonth()->format('Y-m-d 23:59:59')])->where('tmuk_kode',$tmuk_kode)->count();
    }

    public static function getCountStruk($dayStart, $tmuk_kode)
    {
      return static::whereBetween('tanggal', [$dayStart->startOfMonth()->format('Y-m-d 00:00:00'), $dayStart->endOfMonth()->format('Y-m-d 23:59:59')])->where('tmuk_kode',$tmuk_kode)->count('jual_kode');
    }

    public static function getTokoBuka($dayStart, $tmuk_kode)
    {
      return static::selectRaw("TO_CHAR(tanggal, 'dd') as tgl")
                     ->whereBetween('tanggal', [$dayStart->startOfMonth()->format('Y-m-d 00:00:00'), $dayStart->endOfMonth()->format('Y-m-d 23:59:59')])
                     ->where('tmuk_kode',$tmuk_kode)
                     ->groupBy('tgl')
                     ->get();
    }

    public static function getTotalJumlah($dayStart, $tmuk_kode)
    {
      return static::whereBetween('tanggal', [$dayStart->startOfMonth()->format('Y-m-d 00:00:00'), $dayStart->endOfMonth()->format('Y-m-d 23:59:59')])->where('tmuk_kode',$tmuk_kode)->sum('total');
    }


    public static function getDataSetoran(){
        return TransRekapPenjualanTmuk::from(\DB::raw("
          (
           select tmuk_kode, 
              nama_tmuk as nama_tmuk, 
              tanggal, sum(struk) as struk, 
              sum(total_hpp) as total_hpp, 
              sum((tunai-kembalian)) as tunai, 
              sum(kembalian) as kembalian, 
              sum(piutang) as piutang, 
              sum(points) as points, 
              sum(voucher) as voucher,
              sum(donasi) as donasi,
            (select SUM(tj.jumlah) as total from trans_jurnal tj where tj.tmuk_kode = tbl.tmuk_kode and tj.".'delete'." = '0' and tj.posisi = 'D' and tj.deskripsi ilike 'Top Up Escrow Balance - Setoran Penjualan Tunai' and date(tj.tanggal) = tbl.tanggal) as setoran,
              sum((tunai-kembalian)+voucher+points+piutang-donasi) total_penjualan
                  from (
                    select 
                rt.nama as nama_tmuk,
                count(p.jual_kode) as struk,
                p.tmuk_kode,
                date(p.tanggal) as tanggal,
                sum(p.total) as total_penjualan,
                sum(p.kembalian) as kembalian,
                sum(case when pb.tipe = 'TUNAI' then pb.total else 0 end) as tunai,
                sum(case when pb.tipe = 'PIUTANG' then pb.total else 0 end) as piutang,
                sum(case when pb.tipe = 'POINT' then pb.total else 0 end) as points,
                sum(case when pb.tipe = 'VOUCHER' then pb.total else 0 end) as voucher,
                sum(case when pb.tipe = 'DONASI' then pb.total else 0 end) as donasi,
                sum(pd.harga_hpp3) as total_hpp
                from trans_rekap_penjualan p
                
                join trans_rekap_penjualan_bayar pb on pb.rekap_penjualan_kode = p.jual_kode
                join (SELECT rekap_penjualan_kode, SUM(total) as total, SUM(harga_hpp3) as harga_hpp3 FROM trans_rekap_penjualan_detail GROUP BY rekap_penjualan_kode) pd on pd.rekap_penjualan_kode = p.jual_kode
            --    join trans_rekap_penjualan_detail pd on pd.rekap_penjualan_kode = p.jual_kode
                join ref_tmuk rt on rt.kode = p.tmuk_kode
                
--                 WHERE p.tmuk_kode = '0600400052'
--                and p.tanggal >= '2018-08-01 00:00:00' and p.tanggal < '2018-08-31 00:00:00'
                
                GROUP BY p.tmuk_kode, p.tanggal, rt.nama
                ORDER BY p.tanggal ASC
                  ) tbl
              group by tmuk_kode, tanggal, nama_tmuk
              order by tmuk_kode, tanggal ASC
          ) tblx
        "));
    }

    public static function getDataHarian(){
        return TransRekapPenjualanTmuk::from(\DB::raw("
          (
            select tmuk_kode, 
            nama_tmuk as nama_tmuk, 
            tanggal, sum(struk) as struk, 
            sum((tunai-kembalian)+voucher+points+piutang-donasi) as tunai, 
            sum(kembalian) as kembalian, 
            sum(piutang) as piutang, 
            sum(points) as points, 
            sum(voucher) as voucher,
            sum(donasi) as donasi,
            sum(diskon) as diskon,
            sum(total) as total_harga,
            sum(total_hpp) as total_hpp,
            sum(total_penjualan - total_hpp) as total_profit,
            sum((tunai-kembalian)+voucher+points+piutang-donasi+(voucher+points+piutang-donasi+diskon)) as total_penjualan
          from (
            select 
            rt.nama as nama_tmuk,
            count(p.jual_kode) as struk,
            p.tmuk_kode,
            to_char(p.tanggal, 'YYYY-MM-DD') as tanggal,
            sum(p.total) as total_penjualan,
            p.kembalian as kembalian,
            sum(case when pb.tipe = 'TUNAI' then pb.total else 0 end) as tunai,
            sum(case when pb.tipe = 'PIUTANG' then pb.total else 0 end) as piutang,
            sum(case when pb.tipe = 'POINT' then pb.total else 0 end) as points,
            sum(case when pb.tipe = 'VOUCHER' then pb.total else 0 end) as voucher,
            sum(case when pb.tipe = 'DONASI' then pb.total else 0 end) as donasi,
            sum(pd.diskon) as diskon,
            sum(pd.total_harga) as total,
            sum(pd.total_hpp) as total_hpp,
            sum(pd.total_profit) as total_profit
            from trans_rekap_penjualan p

            join trans_rekap_penjualan_bayar pb on pb.rekap_penjualan_kode = p.jual_kode
            join (SELECT rekap_penjualan_kode, tanggal, tmuk_kode, sum(diskon) as diskon, sum(total_harga) as total_harga, sum(total_hpp) as total_hpp, sum(total_profit) as total_profit
                FROM (
                  SELECT d.rekap_penjualan_kode, 
                       d.item_code, 
                       d.discount as diskon, 
                       d.harga, 
                       d.qty, 
                       to_char(p.tanggal, 'YYYY-MM-DD') as tanggal, 
                       p.tmuk_kode, d.harga_hpp3, (d.harga - d.harga_hpp3) as profit, 
                       (d.harga) as total_harga, 
                       (d.qty * d.harga_hpp3) as total_hpp,
                       ((d.harga - d.harga_hpp3) * d.qty) as total_profit
                  FROM trans_rekap_penjualan_detail d
                  JOIN trans_rekap_penjualan p ON p.jual_kode = d.rekap_penjualan_kode
                  JOIN ref_produk_tmuk pt ON pt.produk_kode = d.item_code AND pt.tmuk_kode = p.tmuk_kode
                ) as detail
                GROUP BY rekap_penjualan_kode, tanggal, tmuk_kode) pd on pd.rekap_penjualan_kode = p.jual_kode
            join ref_tmuk rt on rt.kode = p.tmuk_kode
--               WHERE p.tmuk_kode = '0600700033'
--                and p.tanggal BETWEEN '2019-01-08 00:00:00' and '2019-01-08 23:59:59'
            GROUP BY p.tmuk_kode, p.tanggal, rt.nama, p.kembalian
            ORDER BY p.tanggal ASC
          ) tbl
          group by tmuk_kode, tanggal, nama_tmuk
          order by tmuk_kode, tanggal ASC
          ) tblx
        "));
    }

      public static function getDataBulananHpp(){
        return TransRekapPenjualanTmuk::from(\DB::raw("
          (
            select tmuk_kode, 
            nama_tmuk as nama_tmuk, 
            tanggal, sum(struk) as struk, 
            sum((tunai-kembalian)+voucher+points+piutang-donasi) as tunai, 
            sum(kembalian) as kembalian, 
            sum(piutang) as piutang, 
            sum(points) as points, 
            sum(voucher) as voucher,
            sum(donasi) as donasi,
            sum(diskon) as diskon,
            sum(total) as total_harga,
            sum(total_hpp) as total_hpp,
            sum(total_profit) as total_profit,
            sum((tunai-kembalian)+voucher+points+piutang-donasi+(voucher+points+piutang-donasi+diskon)) as total_penjualan
          from (
            select 
            rt.nama as nama_tmuk,
            count(p.jual_kode) as struk,
            p.tmuk_kode,
            to_char(p.tanggal, 'yyyy-mm') as tanggal,
            sum(p.total) as total_penjualan,
            p.kembalian as kembalian,
            sum(case when pb.tipe = 'TUNAI' then pb.total else 0 end) as tunai,
            sum(case when pb.tipe = 'PIUTANG' then pb.total else 0 end) as piutang,
            sum(case when pb.tipe = 'POINT' then pb.total else 0 end) as points,
            sum(case when pb.tipe = 'VOUCHER' then pb.total else 0 end) as voucher,
            sum(case when pb.tipe = 'DONASI' then pb.total else 0 end) as donasi,
            sum(pd.diskon) as diskon,
            sum(pd.total_harga) as total,
            sum(pd.total_hpp) as total_hpp,
            sum(pd.total_profit) as total_profit
            from trans_rekap_penjualan p

            join trans_rekap_penjualan_bayar pb on pb.rekap_penjualan_kode = p.jual_kode
            join (SELECT rekap_penjualan_kode, tanggal, tmuk_kode, sum(diskon) as diskon, sum(total_harga) as total_harga, sum(total_hpp) as total_hpp, sum(total_profit) as total_profit
                FROM (
                  SELECT d.rekap_penjualan_kode, 
                       d.item_code, 
                       d.discount as diskon, 
                       d.harga, 
                       d.qty, 
                       to_char(p.tanggal, 'YYYY-MM-DD') as tanggal, 
                       p.tmuk_kode, d.harga_hpp3, (d.harga - d.harga_hpp3) as profit, 
                       (d.harga) as total_harga, 
                       (d.qty * d.harga_hpp3) as total_hpp,
                       ((d.harga - d.harga_hpp3) * d.qty) as total_profit
                  FROM trans_rekap_penjualan_detail d
                  JOIN trans_rekap_penjualan p ON p.jual_kode = d.rekap_penjualan_kode
                  JOIN ref_produk_tmuk pt ON pt.produk_kode = d.item_code AND pt.tmuk_kode = p.tmuk_kode
                ) as detail
                GROUP BY rekap_penjualan_kode, tanggal, tmuk_kode) pd on pd.rekap_penjualan_kode = p.jual_kode
            join ref_tmuk rt on rt.kode = p.tmuk_kode
--              WHERE p.tmuk_kode = '0600700033'
--               and p.tanggal BETWEEN '2018-12-01 00:00:00' and '2018-12-31 23:59:59'

            GROUP BY p.tmuk_kode, p.tanggal, rt.nama, p.kembalian
            ORDER BY p.tanggal ASC
          ) tbl
          group by tmuk_kode, tanggal, nama_tmuk
          order by tmuk_kode, tanggal ASC
          ) tblx
        "));
    }

      public static function getDataBulananPusat($start='', $end=''){
        $query = '';
        if($start and $end){
            $query = "where p.tanggal between '".$start." 00:00:00' and '".$end." 23:59:59'";
        }
        return TransRekapPenjualanTmuk::from(\DB::raw("
          (
            select tmuk_kode, 
            nama_tmuk as nama_tmuk, 
            tanggal, sum(struk) as struk, 
            sum((tunai-kembalian)+voucher+points+piutang-donasi) as tunai, 
            sum(kembalian) as kembalian, 
            sum(piutang) as piutang, 
            sum(points) as points, 
            sum(voucher) as voucher,
            sum(donasi) as donasi,
            sum(diskon) as diskon,
            sum(total) as total_harga,
            sum(total_hpp) as total_hpp,
            sum(total_profit) as total_profit,
            sum((tunai-kembalian)+voucher+points+piutang-donasi+(voucher+points+piutang-donasi+diskon)) as total_penjualan
          from (
            select 
            rt.nama as nama_tmuk,
            count(p.jual_kode) as struk,
            p.tmuk_kode,
            to_char(p.tanggal, 'yyyy-mm') as tanggal,
            sum(p.total) as total_penjualan,
            p.kembalian as kembalian,
            sum(case when pb.tipe = 'TUNAI' then pb.total else 0 end) as tunai,
            sum(case when pb.tipe = 'PIUTANG' then pb.total else 0 end) as piutang,
            sum(case when pb.tipe = 'POINT' then pb.total else 0 end) as points,
            sum(case when pb.tipe = 'VOUCHER' then pb.total else 0 end) as voucher,
            sum(case when pb.tipe = 'DONASI' then pb.total else 0 end) as donasi,
            sum(pd.diskon) as diskon,
            sum(pd.total_harga) as total,
            sum(pd.total_hpp) as total_hpp,
            sum(pd.total_profit) as total_profit
            from trans_rekap_penjualan p

            join trans_rekap_penjualan_bayar pb on pb.rekap_penjualan_kode = p.jual_kode
            join (SELECT rekap_penjualan_kode, tanggal, tmuk_kode, sum(diskon) as diskon, sum(total_harga) as total_harga, sum(total_hpp) as total_hpp, sum(total_profit) as total_profit
                FROM (
                  SELECT d.rekap_penjualan_kode, 
                       d.item_code, 
                       d.discount as diskon, 
                       d.harga, 
                       d.qty, 
                       to_char(p.tanggal, 'YYYY-MM-DD') as tanggal, 
                       p.tmuk_kode, ptd.hpp, (d.harga - ptd.hpp) as profit, 
                       (d.harga * d.qty) as total_harga, 
                       (d.qty * ptd.hpp) as total_hpp,
                       ((d.harga - ptd.hpp) * d.qty) as total_profit
                  FROM trans_rekap_penjualan_detail d
                  JOIN trans_rekap_penjualan p ON p.jual_kode = d.rekap_penjualan_kode
                  JOIN ref_produk_tmuk pt ON pt.produk_kode = d.item_code AND pt.tmuk_kode = p.tmuk_kode
                  LEFT JOIN ref_produk_tmuk_detail ptd ON ptd.produk_tmuk_kode = pt.id AND ptd.date = to_char(p.tanggal, 'YYYY-MM-DD')
                ) as detail
                GROUP BY rekap_penjualan_kode, tanggal, tmuk_kode) pd on pd.rekap_penjualan_kode = p.jual_kode
            join ref_tmuk rt on rt.kode = p.tmuk_kode

            ".$query."

            GROUP BY p.tmuk_kode, p.tanggal, rt.nama, p.kembalian
            ORDER BY p.tanggal ASC
          ) tbl
          group by tmuk_kode, tanggal, nama_tmuk
          order by tmuk_kode, tanggal ASC
          ) tblx
        "));
    }

    public static function getDataMingguanHpp($start='', $end=''){
      $query = '';
        if($start and $end){
            $query = "where  p.tanggal between '".$start." 00:00:00' and '".$end." 23:59:59'";
        }
        return TransRekapPenjualanTmuk::from(\DB::raw("
          (
            select tmuk_kode, 
            nama_tmuk as nama_tmuk, 
            -- tanggal,
            CAST((extract(day from tanggal)-1)/7 as int) + 1 as minggu,
            extract(month from tanggal) as bulan,
            extract(year from tanggal) as tahun,
            sum(struk) as struk, 
            sum((tunai-kembalian)+voucher+points+piutang-donasi) as tunai, 
            sum(kembalian) as kembalian, 
            sum(piutang) as piutang, 
            sum(points) as points, 
            sum(voucher) as voucher,
            sum(donasi) as donasi,
            sum(diskon) as diskon,
            sum(total) as total_harga,
            sum(total_hpp) as total_hpp,
            sum(total_profit) as total_profit,
            sum((tunai-kembalian)+voucher+points+piutang-donasi+(voucher+points+piutang-donasi+diskon)) as total_penjualan
          from (
            select 
            rt.nama as nama_tmuk,
            count(p.jual_kode) as struk,
            p.tmuk_kode,
--            to_char(p.tanggal, 'YYYY-MM-DD') as tanggal,
            p.tanggal as tanggal,
            sum(p.total) as total_penjualan,
            p.kembalian as kembalian,
            sum(case when pb.tipe = 'TUNAI' then pb.total else 0 end) as tunai,
            sum(case when pb.tipe = 'PIUTANG' then pb.total else 0 end) as piutang,
            sum(case when pb.tipe = 'POINT' then pb.total else 0 end) as points,
            sum(case when pb.tipe = 'VOUCHER' then pb.total else 0 end) as voucher,
            sum(case when pb.tipe = 'DONASI' then pb.total else 0 end) as donasi,
            sum(pd.diskon) as diskon,
            sum(pd.total_harga) as total,
            sum(pd.total_hpp) as total_hpp,
            sum(pd.total_profit) as total_profit
            from trans_rekap_penjualan p

            join trans_rekap_penjualan_bayar pb on pb.rekap_penjualan_kode = p.jual_kode
            join (SELECT rekap_penjualan_kode, tanggal, tmuk_kode, sum(diskon) as diskon, sum(total_harga) as total_harga, sum(total_hpp) as total_hpp, sum(total_profit) as total_profit
                FROM (
                  SELECT d.rekap_penjualan_kode, 
                       d.item_code, 
                       d.discount as diskon, 
                       d.harga, 
                       d.qty, 
                       to_char(p.tanggal, 'YYYY-MM-DD') as tanggal, 
                       p.tmuk_kode, d.harga_hpp3, (d.harga - d.harga_hpp3) as profit, 
                       (d.harga) as total_harga, 
                       (d.qty * d.harga_hpp3) as total_hpp,
                       ((d.harga - d.harga_hpp3) * d.qty) as total_profit
                  FROM trans_rekap_penjualan_detail d
                  JOIN trans_rekap_penjualan p ON p.jual_kode = d.rekap_penjualan_kode
                  JOIN ref_produk_tmuk pt ON pt.produk_kode = d.item_code AND pt.tmuk_kode = p.tmuk_kode
                ) as detail
                GROUP BY rekap_penjualan_kode, tanggal, tmuk_kode) pd on pd.rekap_penjualan_kode = p.jual_kode
            join ref_tmuk rt on rt.kode = p.tmuk_kode
            ".$query."
--                WHERE p.tmuk_kode = '0600700033'
--                and p.tanggal >= '2019-01-01 00:00:00' and p.tanggal < '2019-01-24 23:59:59'
            GROUP BY p.tmuk_kode, p.tanggal, rt.nama, p.kembalian
            ORDER BY p.tanggal ASC
          ) tbl
          GROUP BY tmuk_kode, minggu, bulan, tahun, nama_tmuk
          order by bulan, tahun, minggu, tmuk_kode ASC
          ) tblx
        "));
    }

    public static function getDataMingguan($start='', $end=''){
        $query = '';
        if($start and $end){
            $query = "where  p.tanggal between '".$start." 00:00:00' and '".$end." 23:59:59'";
        }
        return TransRekapPenjualanTmuk::from(\DB::raw("
          (
            select
             tmuk_kode,  
             years, month, week,
             nama_tmuk, 
             sum(struk) as struk, 
             sum(total_hpp) as total_hpp, 
             sum((tunai-kembalian)+voucher+points+piutang-donasi) as tunai, 
             sum(piutang) as piutang,
             sum(points) as points, 
             sum(diskon) as diskon, 
             sum(voucher) as voucher,
             sum((tunai-kembalian)+voucher+points+piutang-donasi+(voucher+points+piutang-donasi+diskon)) total_penjualan
                  from (
                    select  
                      p.tmuk_kode,
                      rt.nama as nama_tmuk,
                      sum(p.total) as total_penjualan,
                      sum(p.kembalian) as kembalian,
                      count(p.jual_kode) as struk,
                      extract(YEAR from p.tanggal) as years,
                      extract(MONTH from tanggal) as month, 
                      extract(WEEK from p.tanggal) as week,
                      sum(case when pb.tipe = 'TUNAI' then pb.total else 0 end) as tunai,
                      sum(case when pb.tipe = 'PIUTANG' then pb.total else 0 end) as piutang,
                      sum(case when pb.tipe = 'POINT' then pb.total else 0 end) as points,
                      sum(case when pb.tipe = 'VOUCHER' then pb.total else 0 end) as voucher,
                      sum(case when pb.tipe = 'DONASI' then pb.total else 0 end) as donasi,
                      sum(pd.harga_hpp3) as total_hpp,
                      SUM(pd.diskon) as diskon
                      from trans_rekap_penjualan p
                      
                      join trans_rekap_penjualan_bayar pb on pb.rekap_penjualan_kode = p.jual_kode
                      join (SELECT rekap_penjualan_kode,
                      SUM(total) as total,
                      SUM(harga_hpp3) as harga_hpp3,
                      SUM(discount) as diskon
                      FROM trans_rekap_penjualan_detail 
                      GROUP BY rekap_penjualan_kode) pd on pd.rekap_penjualan_kode = p.jual_kode
                      join ref_tmuk rt on rt.kode = p.tmuk_kode
                      ".$query."
--                      WHERE p.tmuk_kode = '0601800002'
--                      and p.tanggal >= '2018-08-01 00:00:00' and p.tanggal < '2018-08-30 23:59:59'

                      GROUP BY p.tmuk_kode , years, month, week, rt.nama
                      ORDER BY years, month, week
                  ) tbl
              group by tmuk_kode, years, month, week, nama_tmuk
              order by tmuk_kode, years, month, week DESC
            ) tblx
        "));   
    }

    public static function getDataBulanan($start='', $end=''){
        $query = '';
        if($start and $end){
            $query = "where  p.tanggal between '".$start." 00:00:00' and '".$end." 23:59:59'";
        }
        return TransRekapPenjualanTmuk::from(\DB::raw("
              (
                 select 
                   tmuk_kode, 
                   nama_tmuk, 
                   sum(struk) as struk, 
                   years, month, 
                   sum(total_hpp) as total_hpp, 
                   sum((tunai-kembalian)+voucher+points+piutang-donasi) as tunai, 
                   sum(piutang) as piutang, 
                   sum(points) as points, 
                   sum(diskon) as diskon,
                   sum(voucher) as voucher, 
                   sum((tunai-kembalian)+voucher+points+piutang-donasi+(voucher+points+piutang-donasi+diskon)) total_penjualan
                                    from (
                                      select 
                                        rt.nama as nama_tmuk,
                                        count(p.jual_kode) as struk,
                                        extract(YEAR from p.tanggal) as years,
                                        extract(MONTH from tanggal) as month, 
                                        p.tmuk_kode, SUM(p.total) as total_penjualan,
                                        sum(p.kembalian) as kembalian,
                                        sum(case when pb.tipe = 'TUNAI' then pb.total else 0 end) as tunai,
                                        sum(case when pb.tipe = 'PIUTANG' then pb.total else 0 end) as piutang,
                                        sum(case when pb.tipe = 'POINT' then pb.total else 0 end) as points,
                                        sum(case when pb.tipe = 'VOUCHER' then pb.total else 0 end) as voucher,
                                        sum(case when pb.tipe = 'DONASI' then pb.total else 0 end) as donasi,
                                        sum(pd.harga_hpp3) as total_hpp,
                                        sum(pd.diskon) as diskon
                                        from trans_rekap_penjualan p
                                        
                                        join trans_rekap_penjualan_bayar pb on pb.rekap_penjualan_kode = p.jual_kode
                                        join (SELECT rekap_penjualan_kode, SUM(total) as total, SUM(harga_hpp3) as harga_hpp3, SUM(discount) as diskon FROM trans_rekap_penjualan_detail GROUP BY rekap_penjualan_kode) pd on pd.rekap_penjualan_kode = p.jual_kode
                                        join ref_tmuk rt on rt.kode = p.tmuk_kode
                                        ".$query."
                                        GROUP BY p.tmuk_kode , years, month, rt.nama
                                        ORDER BY years, month
                                    ) tbl
                                group by tmuk_kode, years, month, nama_tmuk
                                order by years, month     
          ) tblx
          "));
    }

    public static function getDataPenjualanPerBarang($start='', $end=''){
        // dd('taraa');
        // if($start and $end){
        //     $query1 = "where pd.created_at between '".$start." 00:00:00' and '".$end." 23:59:59'";
        //     $query2 = "and td.date between '".$start."' and '".$end."'";
        // }
        return TransRekapPenjualanTmukDetail::from(\DB::raw("
          (
            SELECT 
            DATE(pd.created_at) as tanggal,
            pt.tmuk_kode as tmuk_kode,
            pd.item_code as produk,
            pd.description as nama,
            pd.qty as qty,
            pd.harga as harga,
            pd.total as total,
            case when td.hpp > 0 then td.hpp else 0 end as hpp
            FROM trans_rekap_penjualan_detail pd
            join trans_rekap_penjualan p on p.jual_kode = pd.rekap_penjualan_kode
            left join ref_produk_tmuk pt on pt.tmuk_kode = p.tmuk_kode AND pt.produk_kode = pd.item_code
            left join ref_produk_tmuk_detail td on td.produk_tmuk_kode = pt.id
            and td.date between '".$start."' and '".$end."'
            WHERE pd.created_at between '".$start." 00:00:00' and '".$end." 23:59:59'

            order by pd.created_at DESC
          ) tblx
        "));
    }
            /*".$query2."
            ".$query1."*/


    // LAPORAN PEMBELIAN

    public static function getDataPersediaan(){
      return TransRekapPenjualanTmukDetail::from(\DB::raw("(
        select
        trans_rekap_penjualan.tmuk_kode,
        date(trans_rekap_penjualan.tanggal) as tanggal,
        trans_rekap_penjualan_detail.qty,
        trans_rekap_penjualan.total,
        trans_rekap_penjualan_detail.harga,
        trans_rekap_penjualan.total - trans_rekap_penjualan.total as profit,
        (trans_rekap_penjualan.total - trans_rekap_penjualan.total) / trans_rekap_penjualan.total as margin
        from
        trans_rekap_penjualan
        inner join trans_rekap_penjualan_detail on 
        trans_rekap_penjualan_detail.rekap_penjualan_kode = trans_rekap_penjualan.jual_kode
      ) tblx"));
    } 

}


