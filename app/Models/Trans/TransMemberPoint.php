<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransMemberPoint extends Model
{	
    protected $table = 'trans_member_point';
    protected $fillable = ['tmuk_kode','no_member','nama_member','created_at', 'created_by', 'update_at', 'update_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function detail()
    {
        return $this->hasMany(TransMemberPointDetail::class, 'id_trans_member_point','id');
    }
}
