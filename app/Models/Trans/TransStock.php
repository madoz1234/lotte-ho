<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Produk;

class TransStock extends Model
{	
    protected $table = 'trans_stock';
    protected $fillable = ['tmuk_kode','produk_kode','tgl_gr','no_gr','qty','sisa','total','created_at','created_by','updated_at','updated_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_kode','kode');
    }
}
