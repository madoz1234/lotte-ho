<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransKontainer extends Model
{	
    protected $table = 'trans_kontainer';
    protected $fillable = ['nomor','po_nomor','tmuk_kode','created_at','created_by','updated_at','updated_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function po()
    {
        return $this->belongsTo(TransPo::class, 'po_nomor','nomor');
    }

    public function detail(){
        return $this->hasMany(TransKontainerDetail::class, 'id_trans_kontainer', 'id');
    }

    public static function generateCode($tmuk)
    {
        $str = "CL-" . date('Ymd') . $tmuk;

        $rcd = static::where('nomor', 'like', $str . '%')->orderBy('nomor', 'desc')->first();

        $nomor = 1;
        if ($rcd) {
            $nomor = (int) substr($rcd->nomor, -5);
            $nomor++;
        }

        return $str . str_pad($nomor, 5, "0", STR_PAD_LEFT);
    }
}
