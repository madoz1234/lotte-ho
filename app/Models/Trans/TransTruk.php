<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransTruk extends Model
{	
    protected $table = 'trans_truk';
    protected $fillable = ['po_nomor','tmuk_kode','created_at','created_by','updated_at','updated_by','group'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function po()
    {
        return $this->belongsTo(TransPo::class, 'po_nomor','nomor');
    }
}
