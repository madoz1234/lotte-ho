<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\Trans\TransPyrDetail;

class TransPyr extends Model
{	
    protected $table = 'trans_pyr';
    protected $fillable = ['nomor_pyr','tmuk_kode','vendor_lokal','tgl_buat','tgl_jatuh_tempo','keterangan','created_at','created_by','updated_at','updated_by', 'status', 'tipe','group'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function vendorlokal()
    {
        return $this->belongsTo(VendorLokalTmuk::class, 'vendor_lokal','id');
    }

    public function detailPyr()
    {
        return $this->hasMany(TransPyrDetail::class, 'pyr_id','id');
    }

    public function pembayaran()
    {
        return $this->hasOne(TransPyrPembayaran::class, 'nomor_pyr','nomor_pyr');
    }

    public function groups(){
        return $this->hasMany(TransPyr::class, 'group','nomor_pyr');
    }

    public function totals($id){
        $record = TransPyr::find($id);
        $string = isset($record->detailPyr) ? $record->detailPyr->sum('price') : 0;
        return FormatNumber($string);
    }

    public function vendors($id){
        $record = TransPyr::find($id);
        if($record->vendor_lokal == 0){
            $tmuk = Tmuk::with('lsi')->where('kode', $record->tmuk->kode)->first();
            return $tmuk->lsi->nama;
        }else{
            $vendor = VendorLokalTmuk::where('id', $record->vendor_lokal)->first();
            return isset($vendor) ? $vendor->nama : '-';
        }
    }

    public function vendorskode($id){
        $record = TransPyr::find($id);
        if($record->vendor_lokal == 0){
            $tmuk = Tmuk::with('lsi')->where('kode', $record->tmuk->kode)->first();
            return $tmuk->lsi->kode;
        }else{
            $vendor = VendorLokalTmuk::where('id', $record->vendor_lokal)->first();
            return isset($vendor) ? $vendor->kode : '-';
        }
    }

    public function bayarpyr($id){
        $record = TransPyr::where('nomor_pyr',$id)->first();
        $bpyr = TransPyrPembayaran::where('nomor_pyr',$record->nomor_pyr)->first();
        return $bpyr->id;
    }

    public function cekapprovalpyr($id){
        $record = TransPyr::where('nomor_pyr',$id)->first();
        $bpyr = TransPyrPembayaran::where('nomor_pyr',$record->nomor_pyr)->first();
        $string='';
        if($bpyr->verifikasi4_date==null){
            $string='disabled';
        }
        return $string;
    }

    public function urutan($id){
        $pyr = TransPyr::get();
        $position = $pyr->search(function ($person) use ($id) {
            return $person->id == $id;
        });

        return $position + 1;
    }
}
