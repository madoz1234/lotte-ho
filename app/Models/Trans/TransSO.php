<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransSODetail;

class TransSO extends Model
{	
    protected $table = 'trans_so';
    protected $fillable = ['tmuk_kode','tgl_so','qty_aktual','qty_system','selisih','selisih_rp','created_at','created_by','updated_at','updated_by', 'type', 'flag'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function detail()
    {
        return $this->hasMany(TransSODetail::class, 'trans_so_id','id');
    }
}
