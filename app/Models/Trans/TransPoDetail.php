<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Produk;

class TransPoDetail extends Model
{	
    protected $table = 'trans_po_detail';
    protected $fillable = ['po_nomor', 'produk_kode', 'qty_po', 'unit_po', 'qty_pr', 'unit_pr', 'price', 'created_at', 'created_by', 'updated_at', 'updated_by'];

	public function produk(){
		return $this->belongsTo(Produk::class, 'produk_kode','kode' );
	}

	public function po(){
		return $this->belongsTo(TransPo::class, 'po_nomor','nomor');
	}

}

