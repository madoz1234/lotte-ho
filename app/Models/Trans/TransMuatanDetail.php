<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Kontainer;
use Lotte\Models\Master\Produk;

class TransMuatanDetail extends Model
{	
    protected $table = 'trans_muatan_detail';
    protected $fillable = ['nomor_muatan','po_nomor','tmuk_kode','status','created_at','created_by','updated_at','updated_by'];

    public function muatan(){
        return $this->belongsTo(TransMuatan::class, 'nomor_muatan', 'nomor');
    }

    public function po(){
        return $this->belongsTo(TransPo::class, 'po_nomor', 'nomor');
    }

    public function tmuk(){
        return $this->belongsTo(Tmuk::class, 'tmuk_kode', 'kode');
    }
}
