<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransRekapGr extends Model
{	
    protected $table = 'trans_rekap_gr';
    protected $fillable = ['tmuk_kode','tgl_gr','no_gr','created_at','created_by','updated_at','updated_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }
}
