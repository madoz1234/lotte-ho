<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Trans\TransDetailOpening;

class TransOpening extends Model
{	
    protected $table = 'trans_opening';
    protected $fillable = ['nomor_pr','tmuk_kode','tgl_buat','tgl_kirim','created_at','created_by','updated_at','updated_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function detailOpening()
    {
        return $this->hasMany(TransDetailOpening::class, 'opening_id','id');
    }
}
