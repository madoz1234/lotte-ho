<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\MemberCard;

class TransMembercardPoint extends Model
{	
    protected $table = 'trans_membercard_point';
    protected $fillable = ['id_membercard','point','created_at','created_by','updated_at','updated_by'];

    public function membercard()
    {
        return $this->belongsTo(MemberCard::class, 'id_membercard','id');
    }
}
