<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransMemberPiutang extends Model
{	
    protected $table = 'trans_member_piutang';
    protected $fillable = ['tmuk_kode','no_member','nama_member','created_at', 'created_by', 'update_at', 'update_by'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'tmuk_kode','kode');
    }

    public function detail()
    {
        return $this->hasMany(TransMemberPiutangDetail::class, 'id_trans_member_piutang','id');
    }
}
