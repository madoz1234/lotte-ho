<?php

namespace Lotte\Models\Trans;

use Lotte\Models\Model;
use Lotte\Models\Master\Tmuk;

class TransRekapMember extends Model
{   
    protected $table = 'trans_rekap_member_tmuk';
    protected $fillable = ['nomor_member','br_name','br_address','branch_ref','contact_name','default_location','tax_group_id','br_post_address','notes','inactive','credit_limit','telepon','email','debtor_no'];

    public function tmuk()
    {
        return $this->belongsTo(Tmuk::class, 'debtor_no','kode');
    }
}