<?php

// use Spipu\Html2pdf\Html2pdf;
use Spipu\Html2Pdf\Html2Pdf;

if (!function_exists('uploadFile')) {
    function uploadFile($file, $path = '', $key = "himizu")
    {
        if ($file) {
            $path = trim($path, '/') . '/';
            $original = $file->getClientOriginalName();
            $filename = md5($original . time() . $key) . '.' . $file->getClientOriginalExtension();
            $file->move(storage_path($path), $filename);

            $filedata = new \stdClass;
            $filedata->file         = $path . $filename;
            $filedata->filename     = $filename;
            $filedata->originalname = $original;

            return $filedata;
        }

        return false;
    }
}

if (!function_exists('imagePdf')) {
    function imagePdf($url)
    {
        $path =  asset('storage/'.$url);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        return $base64;
    }
}

if (!function_exists('HTML2PDF')) {
    function HTML2PDF($content, $data=[], $ret=false)
    {
        ob_start();

        $content = $content;

        ob_get_clean();

        $html2pdf = new Html2Pdf(isset($data['oriented'])?$data['oriented']:'P', isset($data['paper'])?$data['paper']:'A4' ,'en',true,'UTF-8');

        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content);

        if(!$ret){
            $html2pdf->output((isset($data['label_file'])?$data['label_file']:'report').'.pdf');
        }else{
            $html2pdf->output($_SERVER['DOCUMENT_ROOT'].'planograms/'.(isset($data['label_file'])?$data['label_file']:'report').'.pdf', 'F');
        }
    }
}

if (!function_exists('rupiah')) {
    function rupiah($angka){
        $hasil =  number_format($angka,0, ',' , '.'); 
    return $hasil; 
    }
}

if (!function_exists('decimal_for_save')) {
    function decimal_for_save($text)
    {
        $text_edit = str_replace('.', '', $text);
        $newtext = str_replace(',', '.', $text_edit);

        return $newtext;
    }
}


if (!function_exists('decimal_for_display')) {
    function decimal_for_display($text)
    {
        $text = $text + 0;
        $newtext = str_replace('.', ',', $text);

        return $newtext;
    }
}

if(!function_exists('FormatNumber')){
    function FormatNumber($int){
        return number_format($int, 2, ',', '.');
    }
}

if(!function_exists('FormatCurrency')){
    function FormatCurrency($int, $dec=2){
        if($int < 0){
            return '('.number_format(abs($int), $dec , ',', '.').')';
        }elseif($int == 0){
            return '';
        }
        return number_format($int, $dec, ',', '.');
    }
}

if(!function_exists('priceBblm')){
    function priceBblm($produkLsi, $qty)
    {
        $disc = 0;

        if ($produkLsi->bblm_flag == '1') {

            if ($produkLsi->con3 != 0 && $qty >= $produkLsi->con3) {
                $disc = (float) $produkLsi->dc3;

            } else if ($produkLsi->con2 != 0 && $qty >= $produkLsi->con2) {
                $disc = (float) $produkLsi->dc2;

            } else if ($produkLsi->con1 != 0 && $qty >= $produkLsi->con1) {
                $disc = (float) $produkLsi->dc1;

            }
            
        }

        return ($produkLsi->curr_sale_prc - $disc) * $qty;
    }
}

if(!function_exists('curencyformat')){
    function curencyformat($curr)
    {
        if ($curr < 0 ) {
            return "(" . number_format(abs($curr), 0, ',', '.') . ")";
        }

        if ($curr == 0) {
            return '-';
        }

        return number_format($curr, 0, ',', '.');
    }
}

if(!function_exists('cal_days')){
    function cal_days($bulan='', $tahun='')
    {
        return cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
    }
}


if(!function_exists('option_triwulan')){
    function option_triwulan(){
     $start = \Carbon\Carbon::createFromFormat('Y', '2016');
     $end   = \Carbon\Carbon::createFromFormat('Y', date('Y'));
     $interval = $end->diffInYears($start);
     //dd($interval);
     $year = '';
     $opt = [];
     for($i=0; $i<=$interval; $i++) {
        $year   = date('Y', strtotime($end->format('Y') . ' -'.$i.'year' ));
        $opt[] = [ 
            'triwulan'=> $year.'.1', 
            'labels'=>'I/'.$year
        ];
        $opt[] = [ 
            'triwulan'=> $year.'.2', 
            'labels'=>'II/'.$year
        ];
        $opt[] = [ 
            'triwulan'=> $year.'.3', 
            'labels'=>'III/'.$year
        ];
        $opt[] = [ 
            'triwulan'=> $year.'.4', 
            'labels'=>'IV/'.$year
        ];
     }

     return $opt;
    }
}

if(!function_exists('header_triwulan')){
    function header_triwulan($start = '2017.2', $end='2018.2'){
        $starts = explode('.', $start);
        $ends   = explode('.', $end);
        // dd($ends[0]);
        // $start = 2017;
        // $end = 2018;
        $tw =[];
        for($s = $starts[0] ; $s<=$ends[0]; $s++){
            $tw[]   = [
                'triwulan' => $s.'.1',
                'label'=> "I/".$s
            ];
            $tw[]  = [
                'triwulan' => $s.'.2',
                'label'=> "II/".$s
            ];
            $tw[] = [
                'triwulan' => $s.'.3',
                'label'=> "III/".$s
            ];
            $tw[]  = [
                'triwulan' => $s.'.4',
                'label'=> "IV/".$s
            ];
        }
        $temp = [];

        foreach ($tw as $val) {
          if($val['triwulan'] >= $start and $val['triwulan'] <= $end)
            {
             $temp[] = $val;
            }
        }
        return $temp;
    }
}







