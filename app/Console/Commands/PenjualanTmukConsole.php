<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Trans\TransRekapPenjualanTmuk;


class PenjualanTmukConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:strukpenjualan --filename=""
    protected $signature = 'upload:penjualantmuk {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Penjualan Tmuk';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/tmp/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            //  looping / populate struk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];

                    $penjualantmuk = TransRekapPenjualanTmuk::where('jual_kode', $data[0])->first();
                    if (!$penjualantmuk) {
                        //jika data tidak ada insert
                        $penjualantmuk = new TransRekapPenjualanTmuk();
                        $penjualantmuk->jual_kode   = (string)$data[0];
                        $penjualantmuk->tmuk_kode   = (string)$data[1];
                        $penjualantmuk->kasir_id    = (integer)$data[2];
                        $penjualantmuk->kassa_id    = (integer)$data[3];
                        $penjualantmuk->shift_id    = (integer)$data[4];
                        $penjualantmuk->tanggal    = (string)$data[5];
                        $penjualantmuk->subtotal    = $this->convertToNumeric($data[6]);
                        $penjualantmuk->tax         = $this->convertToNumeric($data[7]);
                        $penjualantmuk->discount    = $this->convertToNumeric($data[8]);
                        $penjualantmuk->total       = $this->convertToNumeric($data[9]);
                        $penjualantmuk->kembalian   = $this->convertToNumeric($data[10]);

                        $penjualantmuk->created_at = date('Y-m-d H:i:s');
                        $penjualantmuk->created_by = 1;
                        var_dump($data[0].'->'.'Struk Tersimpan !');

                    } else {
                        //jika data sudah ada
                        $penjualantmuk->jual_kode   = (string)$data[0];
                        $penjualantmuk->tmuk_kode   = (string)$data[1];
                        $penjualantmuk->kasir_id    = (integer)$data[2];
                        $penjualantmuk->kassa_id    = (integer)$data[3];
                        $penjualantmuk->shift_id    = (integer)$data[4];
                        $penjualantmuk->tanggal    = (string)$data[5];
                        $penjualantmuk->subtotal    = $this->convertToNumeric($data[6]);
                        $penjualantmuk->tax         = $this->convertToNumeric($data[7]);
                        $penjualantmuk->discount    = $this->convertToNumeric($data[8]);
                        $penjualantmuk->total       = $this->convertToNumeric($data[9]);
                        $penjualantmuk->kembalian   = $this->convertToNumeric($data[10]);

                        $penjualantmuk->updated_at = date('Y-m-d H:i:s');
                        $penjualantmuk->updated_by = 1;
                        var_dump($data[0].'->'.'Struk Sudah Ada !');
                    }
                        $penjualantmuk->save();
            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

    public function convertToTimestamp($strFormat)
    {
        return date('Y-m-d H:i:s', $strFormat);
    }

}
