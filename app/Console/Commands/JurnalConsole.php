<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\SecTransaksi;
use Carbon\Carbon;


class JurnalConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:jurnal';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Update Jurnal';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses Update Jurnal...');

        // get tmuk
        $all_tmuk = Tmuk::get();
        // kamus jurnal
        $tahun_fiskal = TahunFiskal::getTahunFiskal();
        $deskripsi    = '';

        $now        = Carbon::now()->subMonths(1);
        $monthStart = $now->startOfMonth()->format('Y-m-d'). ' 00:00:00';
        $monthEnd   = $now->endOfMonth()->format('Y-m-d'). ' 23:59:59';

        // loop tmuk
        foreach ($all_tmuk as $tmuk) {
            $this->info('Update Jurnal Tmuk ' . $tmuk->kode . ' - ' . $tmuk->nama);

            //Modal Dasar
            $idtrans      = SecTransaksi::generate();
            TransJurnal::insert([
                [
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => date('Y-m-d H:i:s'),
                    'tmuk_kode'       => $tmuk->kode,
                    'referensi'       => '-',
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => '1.1.6.1',         //Deposit
                    'jumlah'          => 0,
                    'posisi'          => 'D',
                    'flag'            => '+',
                ],
                [
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => date('Y-m-d H:i:s'),
                    'tmuk_kode'       => $tmuk->kode,
                    'referensi'       => '-',
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => '3.1.1.0',         //Modal Awal
                    'jumlah'          => 0,
                    'posisi'          => 'K',
                    'flag'            => '-',
                ]
            ]);

            //Tambahan Modal Disetor
            $idtrans      = SecTransaksi::generate();
            TransJurnal::insert([
                [
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => date('Y-m-d H:i:s'),
                    'tmuk_kode'       => $tmuk->kode,
                    'referensi'       => '-',
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => '1.1.2.1',         //Rekening Escrow
                    'jumlah'          => 0,
                    'posisi'          => 'D',
                    'flag'            => '+',
                ],
                [
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => date('Y-m-d H:i:s'),
                    'tmuk_kode'       => $tmuk->kode,
                    'referensi'       => '-',
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => '3.1.2.0',         //Tambahan Modal Disetor
                    'jumlah'          => 0,
                    'posisi'          => 'K',
                    'flag'            => '-',
                ]
            ]);

            //Utang Usaha dan Peralatan Toko
            $idtrans      = SecTransaksi::generate();
            TransJurnal::insert([
                [
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => date('Y-m-d H:i:s'),
                    'tmuk_kode'       => $tmuk->kode,
                    'referensi'       => '-',
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => '2.1.1.0',         //Utang Usaha
                    'jumlah'          => 0,
                    'posisi'          => 'D',
                    'flag'            => '+',
                ],
                [
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => date('Y-m-d H:i:s'),
                    'tmuk_kode'       => $tmuk->kode,
                    'referensi'       => '-',
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => '1.2.6.0',         //Peralatan Toko
                    'jumlah'          => 0,
                    'posisi'          => 'K',
                    'flag'            => '-',
                ]
            ]);

            // Mencari laba bulan kemarin
            // $jurnal = TransJurnal::whereBetween('tanggal', array('2018-06-01 00:00:00', '2018-06-30 23:59:59'))
            $jurnal = TransJurnal::whereBetween('tanggal', array($monthStart, $monthEnd))
                                ->where('tmuk_kode', $tmuk->kode)
                                ->where('delete', 0)
                                ->get();
            
            $coas = ['4.1.0.0', '4.1.1.0', '4.2.0.0'];
            $penjualan_barang_dagang = $jurnal->filter(function($item) use ($coas){
                return in_array($item->coa_kode, $coas);
            })->sum('jumlah');

            $potongan_penjualan = $jurnal->filter(function ($item) {
                return $item->coa_kode == '4.3.0.0';
            })->sum('jumlah');

            $total_pendapatan_usaha = abs($penjualan_barang_dagang) + -abs($potongan_penjualan);

            $hpp = $jurnal->filter(function ($item) {
                return $item->coa_kode == '5.1.0.0';
            })->sum('jumlah');

            $coas = ['6.1.1.0', '6.1.1.1', '6.1.1.2', '6.1.1.3', '6.1.1.4'];
            $total_biaya_pemasaran_umum = $jurnal->filter(function($item) use ($coas){
                return in_array($item->coa_kode, $coas);
            })->sum('jumlah');

            $coas = ['6.2.1.1', '6.2.1.2', '6.2.1.3', '6.2.1.4', '6.2.2.1', '6.2.2.2', '6.2.2.3', '6.2.2.4', '6.2.2.5', '6.2.2.6', '6.2.2.7', '6.2.2.8', '6.2.2.9', '6.2.2.10', '6.2.3.1', '6.2.3.2', '6.2.3.3', '6.3.1.0', '6.3.2.0', '6.3.3.0', '6.2.2.11', '6.2.2.12', '6.2.2.13', '6.2.2.14', '6.2.2.15'];
            $total_beban_umum_administrasi = $jurnal->filter(function($item) use ($coas){
                return in_array($item->coa_kode, $coas);
            })->sum('jumlah');

            $selisih_kas_lebih = $jurnal->filter(function ($item) {
                return $item->coa_kode == '7.1.0.0';
            })->sum('jumlah');

            $selisih_kas_kurang = $jurnal->filter(function ($item) {
                return $item->coa_kode == '8.1.0.0';
            })->sum('jumlah');

            $penghasilan_deviden = $jurnal->filter(function ($item) {
                return $item->coa_kode == '7.2.0.0';
            })->sum('jumlah');

            $laba_penjualan_aktiva_tetap = $jurnal->filter(function ($item) {
                return $item->coa_kode == '7.3.0.0';
            })->sum('jumlah');

            $rugi_penjualan_aktiva_tetap = $jurnal->filter(function ($item) {
                return $item->coa_kode == '8.2.0.0';
            })->sum('jumlah');

            $penghasilan_sewa = $jurnal->filter(function ($item) {
                return $item->coa_kode == '7.4.0.0';
            })->sum('jumlah');
            
            $biaya_admin_bank = $jurnal->filter(function ($item) {
                return $item->coa_kode == '8.3.0.0';
            })->sum('jumlah');
            
            $penghasilan_lainnya = $jurnal->filter(function ($item) {
                return $item->coa_kode == '7.5.0.0';
            })->sum('jumlah');

            $beban_lainnya = $jurnal->filter(function ($item) {
                return $item->coa_kode == '8.4.0.0';
            })->sum('jumlah');

            $total_pendapatan_beban_lain2 = abs($selisih_kas_lebih) + -abs($selisih_kas_kurang) + -abs($penghasilan_deviden) + abs($laba_penjualan_aktiva_tetap) + -abs($rugi_penjualan_aktiva_tetap) + -abs($penghasilan_sewa) + -abs($biaya_admin_bank) + abs($penghasilan_lainnya) + -abs($beban_lainnya);

            $pendapatan_kotor = abs($total_pendapatan_usaha) - abs($hpp);
            $laba_usaha       = abs($pendapatan_kotor) + -abs($total_biaya_pemasaran_umum) + -abs($total_beban_umum_administrasi);
            $total_laba_rugi_bersih = $laba_usaha + $total_pendapatan_beban_lain2;

            if ($total_laba_rugi_bersih < 0) {
                //Laba Ditahan
                $idtrans      = SecTransaksi::generate();
                TransJurnal::insert([
                    [
                        'tahun_fiskal_id' => $tahun_fiskal->id,
                        'idtrans'         => $idtrans,
                        'tanggal'         => date('Y-m-d H:i:s'),
                        'tmuk_kode'       => $tmuk->kode,
                        'referensi'       => '-',
                        'deskripsi'       => 'Laba Bulan '.$now->format('F'),
                        'coa_kode'        => '3.4.0.0',         //Laba ditahan
                        'jumlah'          => abs($total_laba_rugi_bersih),
                        'posisi'          => 'D',
                        'flag'            => '-',
                    ]
                ]);
            }else{
                //Laba Ditahan
                $idtrans      = SecTransaksi::generate();
                TransJurnal::insert([
                    [
                        'tahun_fiskal_id' => $tahun_fiskal->id,
                        'idtrans'         => $idtrans,
                        'tanggal'         => date('Y-m-d H:i:s'),
                        'tmuk_kode'       => $tmuk->kode,
                        'referensi'       => '-',
                        'deskripsi'       => 'Laba Bulan '.$now->format('F'),
                        'coa_kode'        => '3.4.0.0',         //Laba ditahan
                        'jumlah'          => abs($total_laba_rugi_bersih),
                        'posisi'          => 'K',
                        'flag'            => '+',
                    ]
                ]);
            }

            
        }

        $this->info('Update Jurnal Done');
    }

}
