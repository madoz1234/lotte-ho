<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;
use Lotte\Libraries\ApiGmd;

/* modal */
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;

class UploadProdukGmdConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'upload:produk {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Produk Gmd';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');
        $lsi_kode = '06022'; // nanti dibuat dinamis

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/tmp/' . $filename);

        Excel::load($filename, function($reader) use ($lsi_kode) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $produks = [];
            $detilproduk = [];

            //  looping / populate produk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                if ($data[0]) {
                    $produks[]                        = $data[0];
                    $detilproduk[$data[0]]['deskripsi_uom_1'] = $data[1]; // deskripsi_uom_1
                    $detilproduk[$data[0]]['kode_tipe_barang'] = $data[2]; // kode_tipe_barang
                    $detilproduk[$data[0]]['kode_jenis_barang'] = $data[3]; // kode_jenis_barang
                    $detilproduk[$data[0]]['locked_price'] = $data[4]; // locked_price
                    $detilproduk[$data[0]]['barcode_uom_1'] = $data[5]; // barcode_uom_1
                    $detilproduk[$data[0]]['satuan_uom_1'] = $data[6]; // satuan_uom_1
                    $detilproduk[$data[0]]['satuan_uom_2'] = $data[7]; // satuan_uom_2
                    $detilproduk[$data[0]]['box_type_uom_1'] = $data[8]; // box_type_uom_1
                    $detilproduk[$data[0]]['box_type_uom_2'] = $data[9]; // box_type_uom_2
                    $detilproduk[$data[0]]['conversion_uom_2'] = $data[10]; // conversion_uom_2
                    $detilproduk[$data[0]]['margin'] = $this->convertToNumeric($data[11]); // margin
                    $detilproduk[$data[0]]['kode_pulsa'] = $data[12]; // kode pulsa
                }
            }

            // get produk from gmd
            $this->info('Jumlah Produk : ' . count($produks));
            $this->info('Cek Produk ke GMD...');
            $response = ApiGmd::getProduct($lsi_kode, $produks);
            //dd($response);
            // cek is json // ke deui
            $json = json_decode($response, true);

            if ($json['status'] == true) { // jika terdapat info dari gmd

                // loop data produk
                $this->output->progressStart(count($json['data'])); // init progress
                foreach ($json['data'] as $val) {
                    $result = $val;

                    // $this->info('Insert/Update Produk ' . $result['PROD_CD'] . ' : ' . $result['PROD_NM']);

                    // ins/upd -> ref_produk
                    $produk = Produk::where('kode', $result['PROD_CD'])->first();
                    if (!$produk) {
                        $produk = new Produk();
                        
                        $produk->created_at = date('Y-m-d H:i:s');
                        $produk->created_by = 1;
                    }

                    $this->fillProduk($produk, $result, $detilproduk);

                    // ins/upd -> ref_produk_setting
                    $prodset = ProdukSetting::where('produk_kode', $result['PROD_CD'])->first();
                    if (!$prodset) {
                        $prodset = new ProdukSetting();
                        
                        $prodset->created_at = date('Y-m-d H:i:s');
                        $prodset->created_by = 1;
                        
                        $this->fillProdukSetting($prodset, $result, $detilproduk);
                    }


                    $this->output->progressAdvance();
                }// end loop data produk
                $this->output->progressFinish();
            }

            // update proses
        });
    }

    private function fillProduk($produk, $data, $detilproduk=[])
    {
        $produk->kode         = $data['PROD_CD'];
        $produk->nama         = $data['PROD_NM'];
        $produk->bumun_cd     = $data['BUMUN_CD'];
        $produk->bumun_nm     = $data['BUMUN_NM'];
        $produk->l1_cd        = $data['L1_CD'];
        $produk->l1_nm        = $data['L1_NM'];
        $produk->l2_cd        = $data['L2_CD'];
        $produk->l2_nm        = $data['L2_NM'];
        $produk->l3_cd        = $data['L3_CD'];
        $produk->l3_nm        = $data['L3_NM'];
        $produk->l4_cd        = $data['L4_CD'];
        $produk->l4_nm        = $data['L4_NM'];
        $produk->width        = $data['WIDTH'];
        $produk->length       = $data['LENGTH'];
        $produk->height       = $data['HEIGHT'];
        $produk->wg           = $data['WG'];
        $produk->inner_width  = $data['INNER_WIDTH'];
        $produk->inner_length = $data['INNER_LENGTH'];
        $produk->inner_height = $data['INNER_HEIGHT'];
        $produk->inner_wg     = $data['INNER_WG'];
        // $produk->status_prod  = $data['STATUS_PROD'];
        
        $produk->updated_at = date('Y-m-d H:i:s');
        $produk->updated_by = 1;

        $produk->save();
    }

    private function fillProdukSetting($produk, $data, $detilproduk=[])
    {
        $produk->produk_kode        = $data['PROD_CD'];
        
        $produk->tipe_barang_kode   = isset($detilproduk[$data['PROD_CD']]['kode_tipe_barang']) ? $detilproduk[$data['PROD_CD']]['kode_tipe_barang'] : null;
        $produk->jenis_barang_kode  = isset($detilproduk[$data['PROD_CD']]['kode_jenis_barang']) ? $detilproduk[$data['PROD_CD']]['kode_jenis_barang'] : null;
        $produk->locked_price       = isset($detilproduk[$data['PROD_CD']]['locked_price']) ? $detilproduk[$data['PROD_CD']]['locked_price'] : 0;
        $produk->margin             = isset($detilproduk[$data['PROD_CD']]['margin']) ? $detilproduk[$data['PROD_CD']]['margin'] : 0;

        $produk->uom1_satuan        = isset($detilproduk[$data['PROD_CD']]['satuan_uom_1']) ? $detilproduk[$data['PROD_CD']]['satuan_uom_1'] : 0;
        $produk->uom1_selling_cek   = 1;
        $produk->uom1_order_cek     = 0;
        $produk->uom1_receiving_cek = 1;
        $produk->uom1_return_cek    = 0;
        $produk->uom1_inventory_cek = 1;
        $produk->uom1_rak_id        = null;
        $produk->uom1_barcode       = isset($detilproduk[$data['PROD_CD']]['barcode_uom_1']) ? $detilproduk[$data['PROD_CD']]['barcode_uom_1'] : null;
        $produk->uom1_conversion    = 1;
        $produk->uom1_width         = $data['WIDTH'];
        $produk->uom1_height        = $data['LENGTH'];
        $produk->uom1_length        = $data['HEIGHT'];
        $produk->uom1_weight        = $data['WG'];
        $produk->uom1_boxtype       = isset($detilproduk[$data['PROD_CD']]['box_type_uom_1']) ? $detilproduk[$data['PROD_CD']]['box_type_uom_1'] : 0;
        $produk->uom1_file          = 0;

        $produk->uom2_satuan        = isset($detilproduk[$data['PROD_CD']]['satuan_uom_2']) ? $detilproduk[$data['PROD_CD']]['satuan_uom_2'] : 0;
        $produk->uom2_selling_cek   = 0;
        $produk->uom2_order_cek     = 1;
        $produk->uom2_receiving_cek = 0;
        $produk->uom2_return_cek    = 1;
        $produk->uom2_inventory_cek = 0;
        $produk->uom2_rak_id        = null;
        $produk->uom2_barcode       = isset($detilproduk[$data['PROD_CD']]['barcode_uom_1']) ? $detilproduk[$data['PROD_CD']]['barcode_uom_1'] : null;
        $produk->uom2_conversion    = isset($detilproduk[$data['PROD_CD']]['conversion_uom_2']) ? $detilproduk[$data['PROD_CD']]['conversion_uom_2'] : 1;
        $produk->uom2_width         = $data['WIDTH'];
        $produk->uom2_height        = $data['LENGTH'];
        $produk->uom2_length        = $data['HEIGHT'];
        $produk->uom2_weight        = $data['WG'];
        $produk->uom2_boxtype       = isset($detilproduk[$data['PROD_CD']]['box_type_uom_2']) ? $detilproduk[$data['PROD_CD']]['box_type_uom_2'] : 0;
        $produk->uom2_file          = null;

        $produk->uom3_satuan        = null;
        $produk->uom3_selling_cek   = null;
        $produk->uom3_order_cek     = null;
        $produk->uom3_receiving_cek = null;
        $produk->uom3_return_cek    = null;
        $produk->uom3_inventory_cek = null;
        $produk->uom3_rak_id        = null;
        $produk->uom3_barcode       = null;
        $produk->uom3_conversion    = null;
        $produk->uom3_width         = null;
        $produk->uom3_height        = null;
        $produk->uom3_length        = null;
        $produk->uom3_weight        = null;
        $produk->uom3_boxtype       = null;
        $produk->uom3_file          = null;

        $produk->uom4_satuan        = null;
        $produk->uom4_selling_cek   = null;
        $produk->uom4_order_cek     = null;
        $produk->uom4_receiving_cek = null;
        $produk->uom4_return_cek    = null;
        $produk->uom4_inventory_cek = null;
        $produk->uom4_rak_id        = null;
        $produk->uom4_barcode       = null;
        $produk->uom4_conversion    = null;
        $produk->uom4_width         = null;
        $produk->uom4_height        = null;
        $produk->uom4_length        = null;
        $produk->uom4_weight        = null;
        $produk->uom4_boxtype       = null;
        $produk->uom4_file          = null;
        $produk->uom1_produk_description          = isset($detilproduk[$data['PROD_CD']]['deskripsi_uom_1']) ? $detilproduk[$data['PROD_CD']]['deskripsi_uom_1'] : null;
        $produk->uom2_file          = null;

        $produk->updated_at = date('Y-m-d H:i:s');
        $produk->updated_by = 1;
        $produk->save();
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

}
