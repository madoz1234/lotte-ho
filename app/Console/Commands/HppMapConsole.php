<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Trans\TransHppMap;

class HppMapConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'upload:hppmap {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload HppMap';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/tmp/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            //  looping / populate produk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                // return $data;
                // cek kode prod di table prod setting
                $cek = TransHppMap::where('produk_kode', $data[0])
                                    ->where('po_nomor', $data[1])
                                    ->where('tmuk_kode', $data[2])->first();
                if (!$cek) {
                    // $cek = new TransHppMap();
                    $cek->tanggal         = date('Y-m-d H:i:s');
                    $cek->produk_kode     = $data[0];
                    $cek->po_nomor        = $data[1];
                    $cek->tmuk_kode       = $data[2];
                    $cek->qty             = 0;
                    $cek->price           = $this->convertToNumeric($data[3]);
                    $cek->map             = $this->convertToNumeric($data[4]);

                    $cek->created_at             = date('Y-m-d H:i:s');
                    $cek->created_by             = 1;
                    var_dump('simpan jika tidak ada');
                } else {
                    $cek->price           = $this->convertToNumeric($data[3]);
                    $cek->map             = $this->convertToNumeric($data[4]);

                    $cek->updated_at      = date('Y-m-d H:i:s');
                    $cek->updated_by      = 1;
                    var_dump('sudah ada');
                }
                $cek->save();
            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

}
