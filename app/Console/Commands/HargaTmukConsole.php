<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\HargaTmuk;
use Lotte\Models\Master\ProdukLsi;

class HargaTmukConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'upload:hargatmuk {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Produk Harga Tmuk';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/tmp/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $produks = [];
            $detilproduk = [];

            //  looping / populate produk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                // return $data;
                // cek kode prod di table produk Lsi
                $cek_produk_gmd = Produk::with('produksetting','produklsi')->where('kode', $data[1])->first();
                    // Cek Produk Ada Tidak
                if ($cek_produk_gmd) {
                    $cekproduklsi = ProdukLsi::where('produk_kode', $data[1])->first();
                    if ($cekproduklsi ) {
                        //Produk Lsi
                        $setting = HargaTmuk::where('tmuk_kode', $data[0])->where('produk_kode', $data[1])->first();
                        if (!$setting ) {
                            //insert data
                            $setting = new HargaTmuk();
                            $setting->tmuk_kode       = (string)$data[0];
                            $setting->produk_kode     = (string)$data[1];
                            $setting->cost_price      = 0;
                            $setting->suggest_price   = 0;
                            $setting->change_price    = 0;
                            $setting->margin_amount   = $this->convertToNumeric($data[2]);
                            $setting->map             = (string)$data[3];
                            $setting->gmd_price       = $cek_produk_gmd->produklsi->curr_sale_prc;
                            $setting->created_at      = date('Y-m-d H:i:s');
                            $setting->created_by      = 1;
                            var_dump($data[1].'->'.'Produk Tersimpan !');
                        } else {
                            //update data
                            $setting->cost_price      = $this->convertToNumeric($data[2]);
                            $setting->suggest_price   = $this->convertToNumeric($data[3]);
                            $setting->change_price    = $this->convertToNumeric($data[4]);
                            $setting->margin_amount   = $this->convertToNumeric($data[5]);
                            $setting->map             = $this->convertToNumeric($data[6]);
                            $setting->gmd_price       = $cek_produk_gmd->produklsi->curr_sale_prc;

                            $setting->updated_at      = date('Y-m-d H:i:s');
                            $setting->updated_by      = 1;
                            var_dump($data[1].'->'.'Produk Sudah Ada !');
                        }       
                        $setting->save();
                    } else {
                        //Produk Lsi Belum Terdaftar
                        var_dump($data[1].'->'.'Produk Belum Terdaftar DiLsi !');
                    }
                }
                else{
                    var_dump($data[1].'->'.'Produk Tidak Ada Disystem !');
                }
                
            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

}
 