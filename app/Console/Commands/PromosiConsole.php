<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Master\PromosiDetailDiskon;

class PromosiConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'upload:promosi {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Pembayaran Tmuk';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/tmp/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            //  looping / populate produk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                // return $data;
                
                //
                        //jika data tidak ada insert
                        $cek = new PromosiDetailDiskon();
                        $cek->promosi_id   = (string)$data[0];
                        $cek->kode_produk   = (string)$data[1];
                        $cek->nama_produk    = (string)$data[2];
                        $cek->cost_price    = $this->convertToNumeric($data[3]);
                        $cek->harga_terdiskon    = $this->convertToNumeric($data[4]);
                        $cek->diskon    = $this->convertToNumeric($data[5]);
                        $cek->harga_awal    = $this->convertToNumeric($data[6]);

                        $cek->created_at = date('Y-m-d H:i:s');
                        $cek->created_by = 1;
                        $cek->save();
                        var_dump($data[0].'->'.'Promosi Tersimpan !');
            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

}
