<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Excel;
use Carbon;
use Storage;

/* modal */
use Lotte\Models\Master\Backup;

class BackupDatabaseConsole extends Command
{
    protected $signature = 'db:backup';

    protected $description = 'Backup the database';

    protected $process;

    public function __construct()
    {
        parent::__construct();

        $filename = Carbon\Carbon::now()->format('Ymd_H-i-s') . ".sql.gz";
        $this->process = new Process(sprintf(
            "pg_dump -h ". env('DB_HOST') ." -p ". env('DB_PORT') ." -U " . env('DB_USERNAME') . " immbo-production | gzip > " .storage_path('backups/'.$filename). ""
           // 'pg_dump --dbname=postgresql://postgres:S1n4rb4r0k4h@immbo-ho.lottegrosir.co.id:5456/immbo-production | gzip > ' .storage_path('backups/'.$filename). ''
        ));
    }

    public function handle()
    {
        try {
            $filename = Carbon\Carbon::now()->format('Ymd_H-i-s') . ".sql.gz";
            $this->process->mustRun();
            //save kedb
            $savebackup = new Backup;
            $savebackup->nama  = 'backups/'.$filename;
            $savebackup->created_at      = date('Y-m-d H:i:s');
            $savebackup->created_by      = 1;
            $savebackup->save();

            $this->info('The backup has been proceed successfully.');
        } catch (ProcessFailedException $exception) {
            $this->error('The backup process has been failed.');
        }
    }
}
