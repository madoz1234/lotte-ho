<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Master\PenjualanDetail;

class PersediaanTmukDetailConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'upload:persediaantmukdetail {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Tmuk Detail';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/tmp/persediaan/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            //  looping / populate produk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                // return $data;
                
                //cek produk tmuk
                $cek_produk_tmuk = ProdukTmuk::where('produk_kode', $data[0])->where('tmuk_kode', '0600400044')->first();
                //jika produk ada di refinsert
                if ($cek_produk_tmuk) {
                    $cek = PenjualanDetail::where('produk_tmuk_kode', $cek_produk_tmuk->id)->where('date', $data[2])->first();
                    if (!$cek) {
                        //jika produk tidak ada di ref_produk_tmuk_detail insert
                        $cek = new PenjualanDetail();
                        $cek->produk_tmuk_kode      = $cek_produk_tmuk->id;
                        $cek->qty                   = (string)$data[1];
                        $cek->date                  = (string)$data[2];

                        $cek->created_at = date('Y-m-d H:i:s');
                        $cek->created_by = 1;
                        var_dump($data[0].'->'.'Produk Qty Tersimpan!');
                    } else {
                        //jika produk ada di table ref_produk_tmuk_detail update
                        $cek->qty                   = $this->convertToNumeric($data[1]);
                        $cek->date                  = (string)$data[2];

                        $cek->updated_at = date('Y-m-d H:i:s');
                        $cek->updated_by = 1;
                        var_dump($data[0].'->'.'Update Produk Qty Yang Sudah Ada!');
                    }
                        $cek->save();
                } else {
                    //jika produk tidak ada di produk harga
                    var_dump($data[0].'->'.'Produk Tidak Ada!');
                }

            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

}
