<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Master\JenisAssortment;
use Lotte\Models\Master\ProdukAssortment;
// use Lotte\Models\Master\Produk;
// use Lotte\Models\Master\ProdukSetting;

class ProdukAssortmentConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'upload:produkassortment {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Produk Assortment';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/produksetting/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $produks = [];
            $detilproduk = [];

            //  looping / populate produk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                // var_dump($data[14],$data[15],$data[16],$data[17]);
                // var_dump((integer)$datas[);
                // return $data;
                // cek kode prod di table prod setting
                $cek_tipe_assortment = JenisAssortment::where('id', $data[0])->first();
                // var_dump($cek_tipe_assortment);
                if ($cek_tipe_assortment) {
                    $setting = ProdukAssortment::where('id', $data[1])->first();// masih salah
                    if (!$setting ) {
                        $setting = new ProdukAssortment();
                    }
                    
                    $setting->assortment_type_id        = (string)$data[0];

                    $setting->produk_kode   = (integer)$data[1];
                    
                    $setting->updated_at = date('Y-m-d H:i:s');
                    $setting->updated_by = 1;
                    $setting->created_at = date('Y-m-d H:i:s');
                    $setting->created_by = 1;
                    $setting->save();
                


                }
                else{
                    var_dump('Barang Tidak Ada !');
                }
                
            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

}
