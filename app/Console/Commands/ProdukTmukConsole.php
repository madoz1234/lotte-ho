<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\ProdukTmuk;


class ProdukTmukConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:strukpenjualan --filename=""
    protected $signature = 'upload:produktmuk {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Produk Tmuk';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/tmp/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            //  looping / populate struk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                    $cekproduksetting = ProdukSetting::where('produk_kode', $data[1])->first();
                    if ($cekproduksetting) {
                        $produktmuk = ProdukTmuk::where('tmuk_kode', $data[0])
                                                ->where('produk_kode', $data[1])->first();
                        if (!$produktmuk) {
                            //jika data tidak ada insert
                            $produktmuk = new ProdukTmuk();
                            $produktmuk->tmuk_kode   = (string)$data[0];
                            $produktmuk->produk_kode   = (string)$data[1];
                            //0->aktif, 1->nonaktif, 2->produk tidak ada diproduk setting
                            $produktmuk->flag    = 1;

                            $produktmuk->created_at = date('Y-m-d H:i:s');
                            $produktmuk->created_by = 1;
                            var_dump($data[1].'->'.'Produk Tersimpan !');

                        } else {
                            //jika data sudah ada
                            $produktmuk->tmuk_kode   = (string)$data[0];
                            $produktmuk->produk_kode   = (string)$data[1];
                            $produktmuk->flag    = 1;
                            var_dump($data[1].'->'.'Produk Sudah Ada !');
                        }
                            $produktmuk->save();
                    } else {
                        var_dump($data[1].'->'.'Produk Tidak Terdaftar !');
                    }
            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

    public function convertToTimestamp($strFormat)
    {
        return date('Y-m-d H:i:s', $strFormat);
    }

}
