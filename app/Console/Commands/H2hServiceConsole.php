<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use \Illuminate\Support\Facades\Storage;

use Excel;

/* modal */
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\SecTransaksi;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransReduceEscrowDetail;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransKontainerDetail;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Trans\TransMuatan;
use Lotte\Models\Trans\TransMuatanDetail;
use Lotte\Models\Trans\TransSuratJalan;
use Lotte\Models\Trans\TransHppMap;
use Lotte\Models\Master\Kontainer;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukHarga;
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Trans\TransLogAudit;
use Lotte\Models\Trans\TransPyrPembayaran;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransPyrDetail;
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\Master\TipeAset;
use Lotte\Models\Trans\TransAkuisisiAset;
use Lotte\Models\Master\DetailVendorLokalTmuk;

/* libraries */
use Lotte\Libraries\H2H;
use Lotte\Libraries\KimFtps;
use Carbon\Carbon;


class H2hServiceConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'h2h:service';

    private $host = 'ftp://fsrv.bri.co.id';
    private $port = 50021;
    private $user = 'lotteshopping';
    private $pass = 'lotte608!@';
    
    private $ftp;

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected $description = 'H2h Service';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->ftp = new \Lotte\Libraries\KimFtps($this->host, $this->port, $this->user, $this->pass);

        // upload file csv
        $this->uploadCsv();
        $this->uploadCsvPyr();
        
        // baca file balasan dari bri
        $this->checkResponseH2hPyrDanPo();
    }

    //Reduce Escrow
    public function uploadCsv()
    {
        $this->info('Proses Upload File CSV.');

        $escrow = TransReduceEscrow::where('statush2h', 1)->get();
        // loop
        foreach ($escrow as $item) {
            // upload to ftp
            $filename = $item->nomorh2h.'.csv';

            if ($this->ftp->upload('/Outgoing/'.$filename, storage_path('temp\\'.$filename))) {
                $this->info("File {$item->nomorh2h} berhasil diupload.");
                // update status reduce
                $item->statush2h = 2;
                $item->keterangan = 'Proses Bank H2H';
                $item->save();

                // unlink
                unlink(storage_path('temp\\'.$filename));
            } else {
                $this->info("File {$item->nomorh2h} gagal diupload.");
            }
        }   
    }

    //Pyr
    public function uploadCsvPyr()
    {
        $this->info('Proses Upload File CSV.');

        $escrow = TransPyrPembayaran::where('statush2h', 1)->get();
        // loop
        foreach ($escrow as $item) {
            // upload to ftp
            $filename = $item->nomorh2h.'.csv';

            if ($this->ftp->upload('/Outgoing/'.$filename, storage_path('temp\\'.$filename))) {
                $this->info("File {$item->nomorh2h} berhasil diupload.");
                // update status reduce
                $item->statush2h = 2;
                $item->keterangan = 'Proses Bank H2H';
                $item->save();

                // unlink
                unlink(storage_path('temp\\'.$filename));
            } else {
                $this->info("File {$item->nomorh2h} gagal diupload.");
            }
        }   
    }

    public function checkResponseH2hPyrDanPo()
    {
        $this->info('Proses Check H2H.');

        // cek file ke ftp bri
        $this->info('GET file response bank.');
        $respon = $this->ftp->getFiles('/Incoming/', 'nack|napr|ack');
        
        // loop files
        foreach ($respon as $file) {
            // download and save to temp
            $tmp_file = storage_path('temp/' . date('YmdHis') .'_'. $file);
            if ($this->ftp->download('/Incoming/' . $file, $tmp_file)){
                $this->info('Proses download file '.$file.' berhasil');
                // baca file content
                $contents = H2h::readResponseBri(file_get_contents($tmp_file));

                //cek jika nomor tersebut PO atau PYR
                foreach ($contents as $cekdata) {
                    if(substr($cekdata['CustRefNo'], 0, 2) == 'PO'){
                        //jika barang PO
                        foreach ($contents as $result) {
                            // $this->info($result['ResponseTransaction']);
                                    // var_dump($result['CustRefNo']);
                            if (substr($result['ResponseTransaction'], 0, 7) == 'SUCCESS') {
                                // update status yg ho reduce jadi success
                                $approval4 = TransReduceEscrow::where('nomorh2h', $result['CustRefNo'])->where('statush2h', 2)->first();
                                if ($approval4) {
                                    $approval4->statush2h = 3;
                                    $approval4->keterangan = $result['ResponseTransaction'];
                                    $approval4->save();
                                    
                                    //update saldo trans_reduce_escrow_detail
                                    $detail = TransReduceEscrowDetail::where('reduce_escrow_id', $approval4->id)->get(); 
                                    $tonja = $detail->sum('total_belanja');

                                    //skema pemotongan di mulai dari deposit, scn, escrow
                                    $tmuk_saldo = Tmuk::where('kode', $approval4->tmuk_kode)->first();
                                    $awal_saldo_deposit = $tmuk_saldo->saldo_deposit;
                                    $awal_saldo_escrow = $tmuk_saldo->saldo_escrow;

                                    $saldo_deposit = $awal_saldo_deposit - $tonja;
                                    if ($saldo_deposit < 0) {
                                        $saldo_escrow = $awal_saldo_escrow + $saldo_deposit;
                                        
                                        $tmuk_saldo->saldo_deposit = 0;
                                        $tmuk_saldo->saldo_escrow = $saldo_escrow;
                                    }else{
                                        $tmuk_saldo->saldo_deposit = $saldo_deposit;
                                    }

                                    if (isset($approval4->transpo->nomor_pr)) {
                                        $no_po = substr($approval4->transpo->nomor_pr, -4);
                                        if ($no_po == '0000') {
                                            $tmuk_saldo->nomor_transaksi = $approval4->po_nomor;
                                            $tmuk_saldo->flag_transaksi  = 1;
                                        }
                                    }
                                    $tmuk_saldo->save(); 

                                    // buat jurnal
                                    //update saldo dipotong trans_reduce_escrow
                                                $detail_escrow = TransReduceEscrow::where('id', $approval4->id)->first();
                                                $detail_escrow->saldo_dipotong       = $tonja;
                                                $detail_escrow->saldo_awal_deposit   = $awal_saldo_deposit;
                                                $detail_escrow->saldo_awal_escrow    = $awal_saldo_escrow;
                                                $detail_escrow->saldo_deposit        = $tmuk_saldo->saldo_deposit;
                                                $detail_escrow->saldo_escrow         = $tmuk_saldo->saldo_escrow;
                                                $detail_escrow->save();
                                                if($detail_escrow->save()){
                                                    //update status 2 di trnas_po
                                                    $po = TransPo::where('nomor', $approval4->po_nomor)->first();
                                                    $po->status = 2;
                                                    if($po->save()){
                                                        foreach ($po->detail as $key => $detail) {
                                                            // var_dump($detail);
                                                            //check qty_po != 0
                                                            if ($detail->qty_po != 0) {
                                                                //insert hpp map
                                                                $conversi_uom2 = isset($detail->produk->produksetting->uom2_conversion) ? $detail->produk->produksetting->uom2_conversion : 1;

                                                                $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $po->tmuk_kode)->get();

                                                                $po_qty = $detail->qty_po * $conversi_uom2; //ngambil hasil konversi
                                                                $po_price = $detail->price / $po_qty;
                                                                $total = $po_qty * $po_price;

                                                                if ($check->count() != 0) {
                                                                    $jml = 0;
                                                                    foreach ($check as $key => $data) {
                                                                        $jml += $data->qty * $data->price;
                                                                    }
                                                                    $new_map = ($jml + $total) / ($check->sum('qty') + $po_qty);

                                                                    $hpp_map = new TransHppMap;
                                                                    $hpp_map->fill([
                                                                        'tanggal'       => date('Y-m-d H:i:s'),
                                                                        'po_nomor'      => $detail->po_nomor,
                                                                        'produk_kode'   => $detail->produk_kode,
                                                                        'tmuk_kode'     => $po->tmuk_kode,
                                                                        'qty'           => $po_qty,
                                                                        'price'         => $po_price,
                                                                        'map'           => $new_map,
                                                                        'created_at'    => date('Y-m-d H:i:s'),
                                                                        'created_by'    => 1,
                                                                    ]);
                                                                    $hpp_map->save();
                                                                }else{
                                                                    $hpp_map = new TransHppMap;
                                                                    $hpp_map->fill([
                                                                        'tanggal'       => date('Y-m-d H:i:s'),
                                                                        'po_nomor'      => $detail->po_nomor,
                                                                        'produk_kode'   => $detail->produk_kode,
                                                                        'tmuk_kode'     => $po->tmuk_kode,
                                                                        'qty'           => $po_qty,
                                                                        'price'         => $po_price,
                                                                        'map'           => $po_price,
                                                                        'created_at'    => date('Y-m-d H:i:s'),
                                                                        'created_by'    => 1,
                                                                    ]);
                                                                    $hpp_map->save();
                                                                }

                                                                //insert ProdukHarga
                                                                $produk_harga = ProdukHarga::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $po->tmuk_kode)->first();

                                                                if ($produk_harga) {
                                                                    //cek kondisi change_price
                                                                    if ($produk_harga->change_price == 0) {
                                                                        $margin_produk = isset($detail->produk->produksetting->margin) ? $detail->produk->produksetting->margin : 1;
                                                                        $hpp           = $hpp_map->map;
                                                                        $margin_persen = ($margin_produk * $hpp) / 100;
                                                                        $hitung_margin = $margin_persen + $hpp ;
                                                                        $roundup       = round(round($hitung_margin)/100) * 100;

                                                                        $amount = $roundup - $hpp;

                                                                        $produk_harga->fill([
                                                                            'cost_price'    => $hpp_map->price,
                                                                            'suggest_price' => $roundup,
                                                                            'change_price'  => 0,
                                                                            'margin_amount' => $roundup - $hpp,
                                                                            'map'           => $hpp_map->map,
                                                                            'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
                                                                            'created_at'    => date('Y-m-d H:i:s'),
                                                                            'created_by'    => 1,
                                                                        ]);
                                                                        $produk_harga->save();
                                                                    }else{
                                                                        $produk_harga->fill([
                                                                            'cost_price'    => $hpp_map->price,
                                                                        // 'suggest_price' => 0,
                                                                        // 'change_price'  => 0,
                                                                            'margin_amount' => $produk_harga->change_price - $hpp_map->map,
                                                                            'map'           => $hpp_map->map,
                                                                            'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
                                                                            'updated_at'    => date('Y-m-d H:i:s'),
                                                                            'updated_by'    => 1,
                                                                        ]);
                                                                        $produk_harga->save();
                                                                    }
                                                                }else{
                                                                    $margin_produk = isset($detail->produk->produksetting->margin) ? $detail->produk->produksetting->margin : 1;
                                                                    $hpp           = $hpp_map->map;
                                                                    $margin_persen = ($margin_produk * $hpp) / 100;
                                                                    $hitung_margin = $margin_persen + $hpp ;
                                                                    $roundup       = round(round($hitung_margin)/100) * 100;

                                                                    $amount = $roundup - $hpp;

                                                                    $new_produk_harga = new ProdukHarga;
                                                                    $new_produk_harga->fill([
                                                                        'tmuk_kode'     => $po->tmuk_kode,
                                                                        'produk_kode'   => $detail->produk_kode,
                                                                        'cost_price'    => $hpp_map->price,
                                                                        'suggest_price' => $roundup,
                                                                        'change_price'  => 0,
                                                                        'margin_amount' => $roundup - $hpp,
                                                                        'map'           => $hpp_map->map,
                                                                        'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
                                                                        'created_at'    => date('Y-m-d H:i:s'),
                                                                        'created_by'    => 1,
                                                                    ]);
                                                                    $new_produk_harga->save();
                                                                }

                                                                //insert or update ProdukTmuk
                                                                $produk_tmuk = ProdukTmuk::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $po->tmuk_kode)->first();

                                                                if ($produk_tmuk) {
                                                                    //cek kondisi stock_awal
                                                                    if ($produk_tmuk->stock_awal != NULL ) {
                                                                        $produk_tmuk->stock_awal = $produk_tmuk->stock_akhir + $detail->qty_pr;
                                                                        $produk_tmuk->updated_at = date('Y-m-d H:i:s');
                                                                        $produk_tmuk->updated_by = 1;
                                                                        $produk_tmuk->save();
                                                                    }else{
                                                                        $produk_tmuk->stock_awal = $detail->qty_pr;
                                                                        $produk_tmuk->updated_at = date('Y-m-d H:i:s');
                                                                        $produk_tmuk->updated_by = 1;
                                                                        $produk_tmuk->save();
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }
                                                }

                                                // insrt to jurnal
                                                $th_fiskal      = TahunFiskal::getTahunFiskal();
                                                $idtrans        = SecTransaksi::generate(); // sementara
                                                // $tanggal        = date('Y-m-d H:i:s');
                                                $tanggal        = $approval4->verifikasi1_date;
                                                $tmuk_kode      = $approval4->tmuk_kode;
                                                
                                                $no_pr          = $detail_escrow->transpo->nomor_pr;

                                                $saldo_potong   = $detail_escrow->saldo_dipotong;
                                                $deposit        = $detail_escrow->saldo_awal_deposit;
                                                $escrow         = $detail_escrow->saldo_awal_escrow;

                                                $check = $deposit - $saldo_potong;
                                                if ($check <= 0) {
                                                    $selisih_deposit = $deposit;
                                                    $selisih_escrow = $escrow - ($check + $escrow);
                                                }else{
                                                    $selisih_deposit = $saldo_potong;
                                                    $selisih_escrow = 0;
                                                }
                                                //Jurnal
                                                $jurnal = TransJurnal::insert([
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $approval4->po_nomor,
                                                        'deskripsi' => 'Reduce Escrow',
                                                        'coa_kode' => '1.1.6.1', // deposit
                                                        'jumlah' => $selisih_deposit,
                                                        'posisi' => 'K',
                                                        'flag' => '-',
                                                    ],
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $approval4->po_nomor,
                                                        'deskripsi' => 'Reduce Escrow',
                                                        'coa_kode' => '1.1.2.1', // escrow
                                                        'jumlah' => $selisih_escrow,
                                                        'posisi' => 'K',
                                                        'flag' => '-',
                                                    ],
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $approval4->po_nomor,
                                                        'deskripsi' => 'Reduce Escrow',
                                                        'coa_kode' => '1.1.4.0', // persediaan barang dagang
                                                        'jumlah' => $saldo_potong,
                                                        'posisi' => 'D',
                                                        'flag' => '+',
                                                    ],
                                                ]);

                                                //update trans reduce escrow id_transaksi
                                                $approval4->id_transaksi = $idtrans;
                                                $approval4->save();
                                                //JAMES KONTAINER
                                                $kontainer = new TransKontainer;
                                                $kontainer->nomor = TransKontainer::generateCode($approval4->tmuk_kode);
                                                $kontainer->po_nomor = $approval4->po_nomor;
                                                $kontainer->tmuk_kode = $approval4->tmuk_kode;
                                                $kontainer->save();

                                                $voulume=0;

                                                $kont = Kontainer::where('tipe_box',1)->get();

                                                foreach ($kont as $value) {
                                                    $volume = $value->tinggi_dalam * $value->lebar_bawah * $value->panjang_bawah;
                                                    $vol=0;
                                                    $i = 1;
                                                    foreach ($kontainer->po->detail as $key) {
                                                        $jumlah_produk=0;
                                                        if($key->produk->produksetting->jenis_barang_kode=='001'){
                                                            if($key->produk->produksetting->uom2_boxtype==0){
                                                                for ($xx=1; $xx <= $key->qty_po ; $xx++) { 
                                                                    // var_dump($i);
                                                                    $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
                                                                    if($vol>=$volume){
                                                                        $vol = 0;
                                                                        // $i++;
                                                                        $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
                                                                    }
                                                                    if($vol<$volume){
                                                                        $jumlah_produk++;
                                                                        $temp_vol = ($key->produk->height/10) * ($key->produk->width/10) * ($key->produk->length/10);
                                                                        $temp = $vol + $temp_vol;
                                                                        if($temp>=$volume && $xx==$key->qty_po){
                                                                            $kont_detail = new TransKontainerDetail;
                                                                            $kont_detail->id_kontainer = $value->id;
                                                                            $kont_detail->id_trans_kontainer = $kontainer->id;
                                                                            $kont_detail->produk_kode = $key->produk->kode;
                                                                            $kont_detail->no_kontainer = $i;
                                                                            $kont_detail->jumlah_produk = $jumlah_produk;
                                                                            $kont_detail->save();

                                                                            $i++;
                                                                            $vol=0;
                                                                        }else if($temp<$volume && $xx==$key->qty_po){
                                                                            $kont_detail = new TransKontainerDetail;
                                                                            $kont_detail->id_kontainer = $value->id;
                                                                            $kont_detail->id_trans_kontainer = $kontainer->id;
                                                                            $kont_detail->produk_kode = $key->produk->kode;
                                                                            $kont_detail->no_kontainer = $i;
                                                                            $kont_detail->jumlah_produk = $jumlah_produk;
                                                                            $kont_detail->save();
                                                                            $jumlah_produk=0;
                                                                        }else if($temp>=$volume && $xx<$key->qty_po){
                                                                            $kont_detail = new TransKontainerDetail;
                                                                            $kont_detail->id_kontainer = $value->id;
                                                                            $kont_detail->id_trans_kontainer = $kontainer->id;
                                                                            $kont_detail->produk_kode = $key->produk->kode;
                                                                            $kont_detail->no_kontainer = $i;
                                                                            $kont_detail->jumlah_produk = $jumlah_produk;
                                                                            $kont_detail->save();

                                                                            $i++;
                                                                            $vol=0;
                                                                            $jumlah_produk=0;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                $maxkont=TransKontainerDetail::where('id_trans_kontainer',$kontainer->id)->max('no_kontainer');

                                                foreach ($kont as $value) {
                                                    $volume = $value->tinggi_dalam * $value->lebar_bawah * $value->panjang_bawah;
                                                    $vol=0;
                                                    $i = $maxkont;
                                                    $i++;
                                                    foreach ($kontainer->po->detail as $key) {
                                                        $jumlah_produk=0;
                                                        if($key->produk->produksetting->jenis_barang_kode=='002'){
                                                            if($key->produk->produksetting->uom2_boxtype==0){
                                                                for ($xx=1; $xx <= $key->qty_po ; $xx++) { 
                                                                   $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
                                                                   if($vol>=$volume){
                                                                    $vol = 0;
                                                                        // $i++;
                                                                    $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
                                                                }
                                                                if($vol<$volume){
                                                                    $jumlah_produk++;
                                                                    $temp_vol = ($key->produk->height/10) * ($key->produk->width/10) * ($key->produk->length/10);
                                                                    $temp = $vol + $temp_vol;
                                                                    if($temp>=$volume && $xx==$key->qty_po){
                                                                        $kont_detail = new TransKontainerDetail;
                                                                        $kont_detail->id_kontainer = $value->id;
                                                                        $kont_detail->id_trans_kontainer = $kontainer->id;
                                                                        $kont_detail->produk_kode = $key->produk->kode;
                                                                        $kont_detail->no_kontainer = $i;
                                                                        $kont_detail->jumlah_produk = $jumlah_produk;
                                                                        $kont_detail->save();

                                                                        $i++;
                                                                        $vol=0;
                                                                    }else if($temp<$volume && $xx==$key->qty_po){
                                                                        $kont_detail = new TransKontainerDetail;
                                                                        $kont_detail->id_kontainer = $value->id;
                                                                        $kont_detail->id_trans_kontainer = $kontainer->id;
                                                                        $kont_detail->produk_kode = $key->produk->kode;
                                                                        $kont_detail->no_kontainer = $i;
                                                                        $kont_detail->jumlah_produk = $jumlah_produk;
                                                                        $kont_detail->save();
                                                                        $jumlah_produk=0;
                                                                    }else if($temp>=$volume && $xx<$key->qty_po){
                                                                        $kont_detail = new TransKontainerDetail;
                                                                        $kont_detail->id_kontainer = $value->id;
                                                                        $kont_detail->id_trans_kontainer = $kontainer->id;
                                                                        $kont_detail->produk_kode = $key->produk->kode;
                                                                        $kont_detail->no_kontainer = $i;
                                                                        $kont_detail->jumlah_produk = $jumlah_produk;
                                                                        $kont_detail->save();

                                                                        $i++;
                                                                        $vol=0;
                                                                        $jumlah_produk=0;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            $max=TransKontainerDetail::where('id_trans_kontainer',$kontainer->id)->max('no_kontainer');

                                            foreach ($kontainer->po->detail as $key) {
                                                if($key->qty_po>0){
                                                    if($key->produk->produksetting->uom2_boxtype==1){
                                                        $max+=1;
                                                        $kont_detail = new TransKontainerDetail;
                                                        $kont_detail->id_kontainer = null;
                                                        $kont_detail->id_trans_kontainer = $kontainer->id;
                                                        $kont_detail->produk_kode = $key->produk->kode;
                                                        $kont_detail->no_kontainer = $max;
                                                        $kont_detail->jumlah_produk = $key->qty_po;
                                                        $kont_detail->save();
                                                    }
                                                }
                                            }

                                            $truk = new TransTruk;
                                            $truk->po_nomor = $approval4->po_nomor;
                                            $truk->tmuk_kode = $approval4->tmuk_kode;
                                            $truk->save();
                                    //sampai sini aja yahh---------------------------------------------------------------------------------------
                                }
                            } else { // hasilnya gagal
                                // update status yg ho reduce jadi gagal dan buka semua validasi
                                $cek_ponomor = TransReduceEscrow::where('nomorh2h', $result['CustRefNo'])->where('statush2h', 2)->first();
                                if ($cek_ponomor) {
                                    $cek_ponomor->verifikasi4_date = null;
                                    $cek_ponomor->verifikasi4_user = null;
                                    $cek_ponomor->statush2h        = 4;
                                    $cek_ponomor->keterangan       = $result['ResponseTransaction'];
                                    $cek_ponomor->save();
                                }
                            }
                        }

                        // move file ftp to processed
                        $respon = $this->ftp->move('/Incoming/'. $file, '/Incoming/PROCESSED/'. $file);
                        // var_dump($respon);

                        // remove temp file
                        $str = str_replace ("\\", "/", $tmp_file);
                        if (file_exists(storage_path($str.'.nack|.napr|.ack'))) {
                            unlink(storage_path('temp'.'"\"'.$tmp_file.'.nack|.napr|.ack'));
                        }else{
                            unlink($tmp_file);
                        } 
                        //jika barang PO
                    }else{
                        //jika barang PYR
                        foreach ($contents as $result) {
                            // $this->info($result['ResponseTransaction']);
                                    // var_dump($result['ResponseTransaction']);
                            if (substr($result['ResponseTransaction'], 0, 7) == 'SUCCESS') {
                                // update status yg ho reduce jadi success
                                $pembayaran = TransPyrPembayaran::where('nomorh2h', $result['CustRefNo'])->where('statush2h', 2)->first();
                                // $pembayaran = TransPyrPembayaran::find($id);
                                // var_dump($pembayaran->biaya_admin);
                                if ($pembayaran) {
                                        if(count($pembayaran->groups)==0){
                                            $biaya = $pembayaran->biaya_admin;
                                            $total_price = isset($pembayaran->pyr->detailPyr) ? $pembayaran->pyr->detailPyr->sum('price') : '0';

                                            $tmuk_saldo = Tmuk::where('kode', $pembayaran->tmuk_kode)->first();
                                                if($biaya !== 0){
                                                    if (substr($pembayaran->nomor_pyr, 0, -24) == 'OPENING') {
                                                        if ($pembayaran->pyr->vendor_lokal == 0) {
                                                            //skema pemotongan di mulai dari deposit, escrow
                                                            // dd('opening');
                                                            $awal_saldo_deposit = $tmuk_saldo->saldo_deposit;
                                                            $awal_saldo_escrow = $tmuk_saldo->saldo_escrow;

                                                            $saldo_deposit = $awal_saldo_deposit - ($total_price + $biaya);
                                                            if ($saldo_deposit < 0) {
                                                            // dd('escrow');
                                                                
                                                                $saldo_escrow = $awal_saldo_escrow + $saldo_deposit;
                                                                
                                                                $tmuk_saldo->saldo_deposit = 0;
                                                                $tmuk_saldo->saldo_escrow = $saldo_escrow;
                                                            }else{
                                                            // dd('deposit');
                                                                $tmuk_saldo->saldo_deposit = $saldo_deposit;
                                                            }
                                                        }else{
                                                            $tmuk_saldo->saldo_escrow = ($tmuk_saldo->saldo_escrow - $total_price) - $biaya;                            
                                                        }
                                                    }else{
                                                        // dd('bukan opening');
                                                        $tmuk_saldo->saldo_escrow = ($tmuk_saldo->saldo_escrow - $total_price) - $biaya;
                                                    }
                                                }
                                                $tmuk_saldo->save();

                                                // update Pyr pemesanan, biaya_admin, saldo deposit
                                                $pembayaran->saldo_escrow = $tmuk_saldo->saldo_escrow;

                                            // $pembayaran->verifikasi4_date = date('Y-m-d h:i:s');
                                            // $pembayaran->verifikasi4_user = 1;
                                            // $pembayaran->status = 1;
                                            $pembayaran->statush2h = 3;
                                            $pembayaran->keterangan = $result['ResponseTransaction'];
                                            $pembayaran->save();

                                            $pyr = TransPyr::where('nomor_pyr', $pembayaran->nomor_pyr)->first();
                                            $pyr->status = 2;
                                            $pyr->save();

                                            //jika tipe pyr 001 masuk hpp map
                                            if($pyr->tipe == '001' || $pyr->tipe == '002'){
                                                //insert hpp map
                                                foreach ($pyr->detailPyr as $key => $detail) {
                                                    $tipe_produk = $detail->produks->produksetting->tipe_produk;
                                                    if ($tipe_produk != 0) {
                                                        $conversi_uom2 = $detail->produks->produksetting->uom2_conversion;

                                                        $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                                                        $qty   = $detail->qty * $conversi_uom2;
                                                        $price = $detail->price / $qty;
                                                        $total = $qty * $price;

                                                        if ($check->count() != 0) {
                                                            $jml = 0;
                                                            foreach ($check as $key => $data) {
                                                                $jml += $data->qty * $data->price;
                                                            }
                                                            $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                                                            $hpp_map = new TransHppMap;
                                                            $hpp_map->fill([
                                                                'tanggal'       => date('Y-m-d H:i:s'),
                                                                'po_nomor'      => $pyr->nomor_pyr,
                                                                'produk_kode'   => $detail->produk_kode,
                                                                'tmuk_kode'     => $pyr->tmuk_kode,
                                                                'qty'           => $qty,
                                                                'price'         => $price,
                                                                'map'           => $new_map,
                                                                'created_at'    => date('Y-m-d H:i:s'),
                                                                'created_by'    => 1,
                                                            ]);
                                                            $hpp_map->save();
                                                        }else{
                                                            $hpp_map = new TransHppMap;
                                                            $hpp_map->fill([
                                                                'tanggal'       => date('Y-m-d H:i:s'),
                                                                'po_nomor'      => $pyr->nomor_pyr,
                                                                'produk_kode'   => $detail->produk_kode,
                                                                'tmuk_kode'     => $pyr->tmuk_kode,
                                                                'qty'           => $qty,
                                                                'price'         => $price,
                                                                'map'           => $price,
                                                                'created_at'    => date('Y-m-d H:i:s'),
                                                                'created_by'    => 1,
                                                            ]);
                                                            $hpp_map->save();
                                                        }
                                                    }
                                                }
                                            }

                                            //JURNAL
                                            $idtrans = SecTransaksi::generate();
                                            $th_fiskal = TahunFiskal::getTahunFiskal();
                                            $tanggal = $pembayaran->pyr->tgl_buat;

                                            $tanggal_aset = date('Y-m-d');
                                            $last_day = date("Y-m-t", strtotime($tanggal));
                                            $deposit = '1.1.6.1'; //deposit
                                            $rekening = '1.1.2.1'; //escrow
                                            
                                            $tmuk_kode = $pembayaran->tmuk_kode;
                                            $persediaan = $total_price + $biaya;
                                            $tipe = $pembayaran->pyr->tipe;
                                            $nomor_pyr = $pembayaran->pyr->nomor_pyr;

                                            //PYR OPENING
                                            if (substr($nomor_pyr, 0, -24) == 'OPENING') {
                                                //insert hpp map jika produk tipe trade (001)
                                                foreach ($pyr->detailPyr as $key => $detail) {
                                                    $tipe_barang_kode = $detail->produks->produksetting->tipe_barang_kode;
                                                    if ($tipe_barang_kode == '001') {
                                                        $conversi_uom2 = $detail->produks->produksetting->uom2_conversion;

                                                        $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                                                        // $qty   = $detail->qty * $conversi_uom2;
                                                        $qty   = $detail->qty;
                                                        $price = $detail->price / $qty;
                                                        $total = $qty * $price;

                                                        if ($check->count() != 0) {
                                                            $jml = 0;
                                                            foreach ($check as $key => $data) {
                                                                $jml += $data->qty * $data->price;
                                                            }
                                                            $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                                                            $hpp_map = new TransHppMap;
                                                            $hpp_map->fill([
                                                                'tanggal'       => date('Y-m-d H:i:s'),
                                                                'po_nomor'      => $pyr->nomor_pyr,
                                                                'produk_kode'   => $detail->produk_kode,
                                                                'tmuk_kode'     => $pyr->tmuk_kode,
                                                                'qty'           => $qty,
                                                                'price'         => $price,
                                                                'map'           => $new_map,
                                                                'created_at'    => date('Y-m-d H:i:s'),
                                                                'created_by'    => 1,
                                                            ]);
                                                            $hpp_map->save();
                                                        }else{
                                                            $hpp_map = new TransHppMap;
                                                            $hpp_map->fill([
                                                                'tanggal'       => date('Y-m-d H:i:s'),
                                                                'po_nomor'      => $pyr->nomor_pyr,
                                                                'produk_kode'   => $detail->produk_kode,
                                                                'tmuk_kode'     => $pyr->tmuk_kode,
                                                                'qty'           => $qty,
                                                                'price'         => $price,
                                                                'map'           => $price,
                                                                'created_at'    => date('Y-m-d H:i:s'),
                                                                'created_by'    => 1,
                                                            ]);
                                                            $hpp_map->save();
                                                        }
                                                    }
                                                }

                                                $check = TransAkuisisiAset::where('nomor_pyr', $nomor_pyr)->get();
                                                if ($check) {
                                                    foreach ($check as $detail) {
                                                        $akuisisi = TransAkuisisiAset::find($detail->id);
                                                        $akuisisi->status = 1;
                                                        if($akuisisi->save()){
                                                            //depresiasi
                                                            if (!is_null($akuisisi->tipe_id)) {
                                                                $depresiasi = TipeAset::find($akuisisi->tipe_id);
                                                                $depresiasi_x = $detail->nilai_pembelian / $depresiasi->tingkat_depresiasi;
                                                                for($x = 0; $x < $depresiasi->tingkat_depresiasi; $x++){
                                                                    if($x == 0){
                                                                        TransJurnal::insert([
                                                                            [
                                                                                'idtrans'         => $idtrans,
                                                                                'tahun_fiskal_id' => $th_fiskal->id,
                                                                                'tanggal'         => $tanggal,
                                                                                'tmuk_kode'       => $tmuk_kode,
                                                                                'referensi'       => $nomor_pyr,
                                                                                'deskripsi'       => $detail->produk_kode.'Depresiasi Bulan Ke-'.$x,
                                                                                'coa_kode'        => '6.3.3.0',
                                                                                'jumlah'          => 0,
                                                                                'posisi'          => 'D',
                                                                                'flag'            => '+',
                                                                            ],
                                                                            [
                                                                                'idtrans'         => $idtrans,
                                                                                'tahun_fiskal_id' => $th_fiskal->id,
                                                                                'tanggal'         => $tanggal,
                                                                                'tmuk_kode'       => $tmuk_kode,
                                                                                'referensi'       => $nomor_pyr,
                                                                                'deskripsi'       => $detail->produk_kode.'Depresiasi Bulan Ke-'.$x,
                                                                                'coa_kode'        => '1.2.7.0',
                                                                                'jumlah'          => 0,
                                                                                'posisi'          => 'K',
                                                                                'flag'            => '-',
                                                                            ]
                                                                        ]);
                                                                    }else{
                                                                        $day_next = Carbon::now()->addMonths($x)->endOfMonth()->format('Y-m-d');
                                                                        $nilai_depresiasi = $depresiasi_x * $x;
                                                                        $idtrans_next = SecTransaksi::generate();

                                                                        TransJurnal::insert([
                                                                            [
                                                                                'idtrans' => $idtrans_next,
                                                                                'tahun_fiskal_id' => $th_fiskal->id,
                                                                                'tanggal' => $day_next.' 00:00:00',
                                                                                'tmuk_kode' => $tmuk_kode,
                                                                                'referensi' => $nomor_pyr,
                                                                                'deskripsi' => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                                                'coa_kode' => '6.3.3.0',
                                                                                'jumlah' => $nilai_depresiasi,
                                                                                'posisi' => 'D',
                                                                                'flag' => '+',
                                                                            ],
                                                                            [
                                                                                'idtrans' => $idtrans_next,
                                                                                'tahun_fiskal_id' => $th_fiskal->id,
                                                                                'tanggal' => $day_next.' 00:00:00',
                                                                                'tmuk_kode' => $tmuk_kode,
                                                                                'referensi' => $nomor_pyr,
                                                                                'deskripsi' => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                                                'coa_kode' => '1.2.7.0',
                                                                                'jumlah' => $nilai_depresiasi,
                                                                                'posisi' => 'K',
                                                                                'flag' => '-',
                                                                            ]
                                                                        ]);
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }

                                            if($tipe == '001'){
                                                //Trade Lotte
                                                TransJurnal::insert([
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Pembayaran PyR - Trade Lotte',
                                                        'coa_kode' => $rekening, // escrow
                                                        'jumlah' => $total_price,
                                                        'posisi' => 'K',
                                                        'flag' => '-',
                                                    ],
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Pembayaran PyR - Trade Lotte',
                                                        'coa_kode' => '2.1.1.0', //Utang Usaha
                                                        'jumlah' => $total_price,
                                                        'posisi' => 'D',
                                                        'flag' => '+',
                                                    ]
                                                ]);
                                            }else if($tipe == '002'){
                                                //Trade Non-Lotte
                                                TransJurnal::insert([
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Pembayaran PyR - Trade Non Lotte',
                                                        'coa_kode' => $rekening, // escrow
                                                        'jumlah' => $total_price,
                                                        'posisi' => 'K',
                                                        'flag' => '-',
                                                    ],
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Pembayaran PyR - Trade Non Lotte',
                                                        'coa_kode' => '2.1.1.0', //Utang Usaha
                                                        'jumlah' => $total_price,
                                                        'posisi' => 'D',
                                                        'flag' => '+',
                                                    ]
                                                ]);
                                            }else if($tipe == '003'){
                                                //Non Trade Lotte
                                                TransJurnal::insert([
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Pembayaran PyR - Non Trade Lotte',
                                                        'coa_kode' => $rekening, // escrow
                                                        'jumlah' => $total_price,
                                                        'posisi' => 'K',
                                                        'flag' => '-',
                                                    ],
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Pembayaran PyR - Non Trade Lotte',
                                                        'coa_kode' => '2.1.1.0', //Utang Usaha
                                                        'jumlah' => $total_price,
                                                        'posisi' => 'D',
                                                        'flag' => '+',
                                                    ]
                                                ]);
                                            }else if($tipe == '004'){
                                                //Non Trade Non Lotte
                                                TransJurnal::insert([
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Pembayaran PyR - Non Trade Non Lotte',
                                                        'coa_kode' => $rekening, // escrow
                                                        'jumlah' => $total_price,
                                                        'posisi' => 'K',
                                                        'flag' => '-',
                                                    ],
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Pembayaran PyR - Non Trade Non Lotte',
                                                        'coa_kode' => '2.1.1.0', //Utang Usaha
                                                        'jumlah' => $total_price,
                                                        'posisi' => 'D',
                                                        'flag' => '+',
                                                    ]
                                                ]);
                                            }

                                            //update id_transaksi di pyr
                                            $pyr->id_transaksi = $idtrans;
                                            $pyr->save();

                                            //Biaya Admin
                                            TransJurnal::insert([
                                                [
                                                    'idtrans' => $idtrans,
                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                    'tanggal' => $tanggal,
                                                    'tmuk_kode' => $tmuk_kode,
                                                    'referensi' => $nomor_pyr,
                                                    'deskripsi' => 'Biaya Admin',
                                                    'coa_kode' => '1.1.2.1', //escrow
                                                    'jumlah' => $biaya,
                                                    'posisi' => 'K',
                                                    'flag' => '-',
                                                ],
                                                [
                                                    'idtrans' => $idtrans,
                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                    'tanggal' => $tanggal,
                                                    'tmuk_kode' => $tmuk_kode,
                                                    'referensi' => $nomor_pyr,
                                                    'deskripsi' => 'Biaya Admin',
                                                    'coa_kode' => '8.3.0.0', //Biaya Admin Bank
                                                    'jumlah' => $biaya,
                                                    'posisi' => 'D',
                                                    'flag' => '+',
                                                ]
                                            ]);
                                        
                                        //satuan bukan dalam pilihan
                                        }else{
                                            foreach ($pembayaran->groups as $value) {
                                                $pembayaran2 = TransPyrPembayaran::find($value->id);
                                                $biaya = $pembayaran2->biaya_admin;
                                                $total_price = isset($pembayaran2->pyr->detailPyr) ? $pembayaran2->pyr->detailPyr->sum('price') : '0';

                                                //skema pemotongan di mulai dari deposit
                                                $tmuk_saldo = Tmuk::where('kode', $pembayaran2->tmuk_kode)->first();
                                                    if($biaya !== 0){
                                                        if (substr($pembayaran2->nomor_pyr, 0, -24) == 'OPENING') {
                                                            if ($pembayaran2->pyr->vendor_lokal == 0) {
                                                                //skema pemotongan di mulai dari deposit, escrow
                                                                // dd('opening');
                                                                $awal_saldo_deposit = $tmuk_saldo->saldo_deposit;
                                                                $awal_saldo_escrow = $tmuk_saldo->saldo_escrow;

                                                                $saldo_deposit = $awal_saldo_deposit - ($total_price + $biaya);
                                                                if ($saldo_deposit < 0) {
                                                                // dd('escrow');
                                                                    
                                                                    $saldo_escrow = $awal_saldo_escrow + $saldo_deposit;
                                                                    
                                                                    $tmuk_saldo->saldo_deposit = 0;
                                                                    $tmuk_saldo->saldo_escrow = $saldo_escrow;
                                                                }else{
                                                                // dd('deposit');
                                                                    $tmuk_saldo->saldo_deposit = $saldo_deposit;
                                                                }
                                                            }else{
                                                                $tmuk_saldo->saldo_escrow = ($tmuk_saldo->saldo_escrow - $total_price) - $biaya;                            
                                                            }
                                                        }else{
                                                            // dd('bukan opening');
                                                            $tmuk_saldo->saldo_escrow = ($tmuk_saldo->saldo_escrow - $total_price) - $biaya;
                                                        }
                                                    }
                                                    $tmuk_saldo->save();
                                                    
                                                    // update Pyr pemesanan, biaya_admin, saldo deposit
                                                    $pembayaran2->saldo_escrow = $tmuk_saldo->saldo_escrow;
                                                    // $pembayaran2->verifikasi4_date = date('Y-m-d h:i:s');
                                                    // $pembayaran2->verifikasi4_user = 1;
                                                    // $pembayaran2->status = 1;
                                                    $pembayaran2->statush2h = 3;
                                                    $pembayaran2->keterangan = $result['ResponseTransaction'];
                                                    $pembayaran2->save();

                                                $pyr = TransPyr::where('nomor_pyr', $pembayaran2->nomor_pyr)->first();
                                                $pyr->status = 2;
                                                $pyr->save();

                                                //jika tipe pyr 001 masuk hpp map
                                                if($pyr->tipe == '001' || $pyr->tipe == '002'){
                                                    //insert hpp map
                                                    foreach ($pyr->detailPyr as $key => $detail) {
                                                        $tipe_produk = $detail->produks->produksetting->tipe_produk;
                                                        if ($tipe_produk != 0) {
                                                            $conversi_uom2 = $detail->produks->produksetting->uom2_conversion;

                                                            $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                                                            $qty   = $detail->qty * $conversi_uom2;
                                                            $price = $detail->price / $qty;
                                                            $total = $qty * $price;

                                                            if ($check->count() != 0) {
                                                                $jml = 0;
                                                                foreach ($check as $key => $data) {
                                                                    $jml += $data->qty * $data->price;
                                                                }
                                                                $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                                                                $hpp_map = new TransHppMap;
                                                                $hpp_map->fill([
                                                                    'tanggal'       => date('Y-m-d H:i:s'),
                                                                    'po_nomor'      => $pyr->nomor_pyr,
                                                                    'produk_kode'   => $detail->produk_kode,
                                                                    'tmuk_kode'     => $pyr->tmuk_kode,
                                                                    'qty'           => $qty,
                                                                    'price'         => $price,
                                                                    'map'           => $new_map,
                                                                    'created_at'    => date('Y-m-d H:i:s'),
                                                                    'created_by'    => 1,
                                                                ]);
                                                                $hpp_map->save();
                                                            }else{
                                                                $hpp_map = new TransHppMap;
                                                                $hpp_map->fill([
                                                                    'tanggal'       => date('Y-m-d H:i:s'),
                                                                    'po_nomor'      => $pyr->nomor_pyr,
                                                                    'produk_kode'   => $detail->produk_kode,
                                                                    'tmuk_kode'     => $pyr->tmuk_kode,
                                                                    'qty'           => $qty,
                                                                    'price'         => $price,
                                                                    'map'           => $price,
                                                                    'created_at'    => date('Y-m-d H:i:s'),
                                                                    'created_by'    => 1,
                                                                ]);
                                                                $hpp_map->save();
                                                            }
                                                        }

                                                        //test harga pembentukan harga non lotte
                                                        else {
                                                           $cek_produk_harga = substr($detail->produk_kode, -10,1);
                                                           if ($cek_produk_harga == '5' || $cek_produk_harga == '2') {
                                                                $conversi_uom2 = $detail->produks->produksetting->uom2_conversion ? $detail->produks->produksetting->uom2_conversion : 1;

                                                                    $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                                                                    $qty   = $detail->qty * $conversi_uom2;
                                                                    $price = $detail->price / $qty;
                                                                    $total = $qty * $price;

                                                                    if ($check->count() != 0) {
                                                                        $jml = 0;
                                                                        foreach ($check as $key => $data) {
                                                                            $jml += $data->qty * $data->price;
                                                                        }
                                                                        $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                                                                        $hpp_map = new TransHppMap;
                                                                        $hpp_map->fill([
                                                                            'tanggal'       => date('Y-m-d H:i:s'),
                                                                            'po_nomor'      => $pyr->nomor_pyr,
                                                                            'produk_kode'   => $detail->produk_kode,
                                                                            'tmuk_kode'     => $pyr->tmuk_kode,
                                                                            'qty'           => $qty,
                                                                            'price'         => $price,
                                                                            'map'           => $new_map,
                                                                            'created_at'    => date('Y-m-d H:i:s'),
                                                                            'created_by'    => 1,
                                                                        ]);
                                                                        $hpp_map->save();
                                                                    }else{
                                                                        $hpp_map = new TransHppMap;
                                                                        $hpp_map->fill([
                                                                            'tanggal'       => date('Y-m-d H:i:s'),
                                                                            'po_nomor'      => $pyr->nomor_pyr,
                                                                            'produk_kode'   => $detail->produk_kode,
                                                                            'tmuk_kode'     => $pyr->tmuk_kode,
                                                                            'qty'           => $qty,
                                                                            'price'         => $price,
                                                                            'map'           => $price,
                                                                            'created_at'    => date('Y-m-d H:i:s'),
                                                                            'created_by'    => 1,
                                                                        ]);
                                                                        $hpp_map->save();
                                                                    }
                                                           }
                                                        }
                                                        //test harga pembentukan harga non lotte
                                                        
                                                        // $test = $detail->price / $detail->qty;
                                                        // dd($detail->produk_kode);
                                                        //test
                                                        //insert ProdukHarga
                                                        //test array harga
                                                            // $cek_produk_harga = substr($detail->produk_kode, -10,1);
                                                            // dd($cek_produk_harga);
                                                            // if ($cek_produk_harga == '5' || $cek_produk_harga == '2') {
                                                                // foreach ($pyr->detailPyr as $key => $hasildata) {
                                                                //         dd($hasildata->produk_kode);
                                                                //     }
                                                            // }
                                                            // dd($detail->produk_kode);
                                                
                                                            $cek_produk_harga = substr($detail->produk_kode, -10,1);
                                                            if ($cek_produk_harga == '5' || $cek_produk_harga == '2') {
                                                                //test array harga 
                                                                $produk_harga = ProdukHarga::with('produksetting')
                                                                                    ->where('produk_kode', $detail->produk_kode)
                                                                                    ->where('tmuk_kode', $pyr->tmuk_kode)->first();
                                                                    // dd($detail->produks->produksetting->margin);
                                                                    $margin_produk = isset($detail->produks->produksetting->margin) ? $detail->produks->produksetting->margin : 1;
                                                                    $hpp           = $detail->price / $detail->qty;
                                                                    $margin_persen = ($margin_produk * $hpp) / 100;
                                                                    $hitung_margin = $margin_persen + $hpp ;
                                                                    $roundup       = round(round($hitung_margin)/100) * 100;
                                                                    $hargaawal     = $detail->price / $detail->qty;
                                                                    $marginpersen  = $margin_produk / 100;
                                                                    $suggest_price = ($hargaawal * $marginpersen) + $hargaawal;
                                                                    $suggest_total =  round($suggest_price,-2);
                                                                    $amount = $roundup - $hpp;
                                                                if ($produk_harga) {
                                                                    //cek kondisi change_price
                                                                    if ($produk_harga->change_price == 0) {
                                                                        // $margin_produk = isset($detail->produks->produksetting->margin) ? $detail->produks->produksetting->margin : 1;
                                                                        // $hpp           = $produk_harga->map;
                                                                        // $margin_persen = ($margin_produk * $hpp) / 100;
                                                                        // $hitung_margin = $margin_persen + $hpp ;
                                                                        // $roundup       = round(round($hitung_margin)/100) * 100;
                                                                        // $hargaawal     = $detail->price / $detail->qty;
                                                                        // $marginpersen  = $margin_produk / 100;
                                                                        // $suggest_price    = ($hargaawal * $marginpersen) + $hargaawal;

                                                                        // $amount = $roundup - $hpp;

                                                                        $produk_harga->fill([
                                                                            'cost_price'    => $hargaawal,
                                                                            'suggest_price' => $suggest_total,
                                                                            'change_price'  => 0,
                                                                            'margin_amount' => $roundup - $hpp,
                                                                            'map'           => $produk_harga->map,
                                                                            'gmd_price'     => $detail->price / $detail->qty,
                                                                            'created_at'    => date('Y-m-d H:i:s'),
                                                                            'created_by'    => 1,
                                                                        ]);
                                                                        $produk_harga->save();
                                                                    }else{
                                                                        $produk_harga->fill([
                                                                            'cost_price'    => $hargaawal,
                                                                        // 'suggest_price' => 0,
                                                                        // 'change_price'  => 0,
                                                                            'margin_amount' => $produk_harga->change_price - $hpp_map->map,
                                                                            'map'           => $hpp_map->map,
                                                                            'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
                                                                            'updated_at'    => date('Y-m-d H:i:s'),
                                                                            'updated_by'    => 1,
                                                                        ]);
                                                                        $produk_harga->save();
                                                                    }
                                                                }else{
                                                                    // $margin_produk = isset($detail->produks->produksetting->margin) ? $detail->produks->produksetting->margin : 1;
                                                                    // $hpp           = $detail->price / $detail->qty;
                                                                    // $margin_persen = ($margin_produk * $hpp) / 100;
                                                                    // $hitung_margin = $margin_persen + $hpp ;
                                                                    // $roundup       = round(round($hitung_margin)/100) * 100;
                                                                    // $hargaawal     = $detail->price / $detail->qty;
                                                                    // $marginpersen  = $margin_produk / 100;
                                                                    // $suggest_price = ($hargaawal * $marginpersen) + $hargaawal;
                                                                    // $suggest_total =  round($suggest_price);
                                                                    // $amount = $roundup - $hpp;

                                                                    $new_produk_harga = new ProdukHarga;
                                                                    $new_produk_harga->fill([
                                                                        'tmuk_kode'     => $pyr->tmuk_kode,
                                                                        'produk_kode'   => $detail->produk_kode,
                                                                        'cost_price'    => $hargaawal,
                                                                        'suggest_price' => $suggest_total,
                                                                        'change_price'  => 0,
                                                                        'margin_amount' => $roundup - $hpp,
                                                                        'map'           => $hargaawal,
                                                                        'gmd_price'     => $hargaawal,
                                                                        'created_at'    => date('Y-m-d H:i:s'),
                                                                        'created_by'    => 1,
                                                                    ]);
                                                                    $new_produk_harga->save();
                                                                }
                                                                //test

                                                            }
                                                            
                                                    }
                                                }
                                                
                                                //JURNAL
                                                $idtrans = SecTransaksi::generate();
                                                $th_fiskal = TahunFiskal::getTahunFiskal();
                                                $tanggal = $pembayaran2->pyr->tgl_buat;

                                                $tanggal_aset = date('Y-m-d');
                                                $last_day = date("Y-m-t", strtotime($tanggal));
                                                $deposit = '1.1.6.1'; //deposit
                                                $rekening = '1.1.2.1'; //escrow
                                                
                                                $tmuk_kode = $pembayaran2->tmuk_kode;
                                                $persediaan = $total_price + $biaya;
                                                $tipe = $pembayaran2->pyr->tipe;
                                                $nomor_pyr = $pembayaran2->pyr->nomor_pyr;

                                                //PYR OPENING
                                                if (substr($nomor_pyr, 0, -24) == 'OPENING') {
                                                    //insert hpp map jika produk tipe trade (001)
                                                    foreach ($pyr->detailPyr as $key => $detail) {
                                                        $tipe_barang_kode = $detail->produks->produksetting->tipe_barang_kode;
                                                        if ($tipe_barang_kode == '001') {
                                                            $conversi_uom2 = $detail->produks->produksetting->uom2_conversion;

                                                            $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                                                            // $qty   = $detail->qty * $conversi_uom2;
                                                            $qty   = $detail->qty;
                                                            $price = $detail->price / $qty;
                                                            $total = $qty * $price;

                                                            if ($check->count() != 0) {
                                                                $jml = 0;
                                                                foreach ($check as $key => $data) {
                                                                    $jml += $data->qty * $data->price;
                                                                }
                                                                $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                                                                $hpp_map = new TransHppMap;
                                                                $hpp_map->fill([
                                                                    'tanggal'       => date('Y-m-d H:i:s'),
                                                                    'po_nomor'      => $pyr->nomor_pyr,
                                                                    'produk_kode'   => $detail->produk_kode,
                                                                    'tmuk_kode'     => $pyr->tmuk_kode,
                                                                    'qty'           => $qty,
                                                                    'price'         => $price,
                                                                    'map'           => $new_map,
                                                                    'created_at'    => date('Y-m-d H:i:s'),
                                                                    'created_by'    => 1,
                                                                ]);
                                                                $hpp_map->save();
                                                            }else{
                                                                $hpp_map = new TransHppMap;
                                                                $hpp_map->fill([
                                                                    'tanggal'       => date('Y-m-d H:i:s'),
                                                                    'po_nomor'      => $pyr->nomor_pyr,
                                                                    'produk_kode'   => $detail->produk_kode,
                                                                    'tmuk_kode'     => $pyr->tmuk_kode,
                                                                    'qty'           => $qty,
                                                                    'price'         => $price,
                                                                    'map'           => $price,
                                                                    'created_at'    => date('Y-m-d H:i:s'),
                                                                    'created_by'    => 1,
                                                                ]);
                                                                $hpp_map->save();
                                                            }
                                                        }
                                                    }

                                                    $check = TransAkuisisiAset::where('nomor_pyr', $nomor_pyr)->get();
                                                    if ($check) {
                                                        foreach ($check as $detail) {
                                                            $akuisisi = TransAkuisisiAset::find($detail->id);
                                                            $akuisisi->status = 1;
                                                            if($akuisisi->save()){
                                                                if (!is_null($akuisisi->tipe_id)) {
                                                                    $depresiasi = TipeAset::find($akuisisi->tipe_id);
                                                                    $depresiasi_x = $detail->nilai_pembelian / $depresiasi->tingkat_depresiasi;
                                                                    for($x = 0; $x < $depresiasi->tingkat_depresiasi; $x++){
                                                                        if($x == 0){
                                                                            TransJurnal::insert([
                                                                                [
                                                                                    'idtrans'         => $idtrans,
                                                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                                                    'tanggal'         => $tanggal,
                                                                                    'tmuk_kode'       => $tmuk_kode,
                                                                                    'referensi'       => $nomor_pyr,
                                                                                    'deskripsi'       => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                                                    'coa_kode'        => '6.3.3.0',
                                                                                    'jumlah'          => 0,
                                                                                    'posisi'          => 'D',
                                                                                    'flag'            => '+',
                                                                                ],
                                                                                [
                                                                                    'idtrans'         => $idtrans,
                                                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                                                    'tanggal'         => $tanggal,
                                                                                    'tmuk_kode'       => $tmuk_kode,
                                                                                    'referensi'       => $nomor_pyr,
                                                                                    'deskripsi'       => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                                                    'coa_kode'        => '1.2.7.0',
                                                                                    'jumlah'          => 0,
                                                                                    'posisi'          => 'K',
                                                                                    'flag'            => '-',
                                                                                ]
                                                                            ]);
                                                                        }else{
                                                                            $day_next = Carbon::createFromFormat('Y-m-d', $tanggal)->addMonths($x)->endOfMonth()->format('Y-m-d');
                                                                            // $day_next = Carbon::now()->addMonths($x)->endOfMonth()->format('Y-m-d');
                                                                            $nilai_depresiasi = $depresiasi_x * $x;
                                                                            $idtrans_next = SecTransaksi::generate();

                                                                            TransJurnal::insert([
                                                                                [
                                                                                    'idtrans' => $idtrans_next,
                                                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                                                    'tanggal' => $day_next.' 00:00:00',
                                                                                    'tmuk_kode' => $tmuk_kode,
                                                                                    'referensi' => $nomor_pyr,
                                                                                    'deskripsi' => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                                                    'coa_kode' => '6.3.3.0',
                                                                                    'jumlah' => $nilai_depresiasi,
                                                                                    'posisi' => 'D',
                                                                                    'flag' => '+',
                                                                                ],
                                                                                [
                                                                                    'idtrans' => $idtrans_next,
                                                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                                                    'tanggal' => $day_next.' 00:00:00',
                                                                                    'tmuk_kode' => $tmuk_kode,
                                                                                    'referensi' => $nomor_pyr,
                                                                                    'deskripsi' => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                                                    'coa_kode' => '1.2.7.0',
                                                                                    'jumlah' => $nilai_depresiasi,
                                                                                    'posisi' => 'K',
                                                                                    'flag' => '-',
                                                                                ]
                                                                            ]);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                if($tipe == '001'){
                                                    //Trade Lotte
                                                    TransJurnal::insert([
                                                        [
                                                            'idtrans' => $idtrans,
                                                            'tahun_fiskal_id' => $th_fiskal->id,
                                                            'tanggal' => $tanggal,
                                                            'tmuk_kode' => $tmuk_kode,
                                                            'referensi' => $nomor_pyr,
                                                            'deskripsi' => 'Pembayaran PyR - Trade Lotte',
                                                            'coa_kode' => $rekening, // escrow
                                                            'jumlah' => $total_price,
                                                            'posisi' => 'K',
                                                            'flag' => '-',
                                                        ],
                                                        [
                                                            'idtrans' => $idtrans,
                                                            'tahun_fiskal_id' => $th_fiskal->id,
                                                            'tanggal' => $tanggal,
                                                            'tmuk_kode' => $tmuk_kode,
                                                            'referensi' => $nomor_pyr,
                                                            'deskripsi' => 'Pembayaran PyR - Trade Lotte',
                                                            'coa_kode' => '2.1.1.0', //Utang Usaha
                                                            'jumlah' => $total_price,
                                                            'posisi' => 'D',
                                                            'flag' => '+',
                                                        ]
                                                    ]);
                                                }else if($tipe == '002'){
                                                    //Trade Non-Lotte
                                                    TransJurnal::insert([
                                                        [
                                                            'idtrans' => $idtrans,
                                                            'tahun_fiskal_id' => $th_fiskal->id,
                                                            'tanggal' => $tanggal,
                                                            'tmuk_kode' => $tmuk_kode,
                                                            'referensi' => $nomor_pyr,
                                                            'deskripsi' => 'Pembayaran PyR - Trade Non Lotte',
                                                            'coa_kode' => $rekening, // escrow
                                                            'jumlah' => $total_price,
                                                            'posisi' => 'K',
                                                            'flag' => '-',
                                                        ],
                                                        [
                                                            'idtrans' => $idtrans,
                                                            'tahun_fiskal_id' => $th_fiskal->id,
                                                            'tanggal' => $tanggal,
                                                            'tmuk_kode' => $tmuk_kode,
                                                            'referensi' => $nomor_pyr,
                                                            'deskripsi' => 'Pembayaran PyR - Trade Non Lotte',
                                                            'coa_kode' => '2.1.1.0', //Utang Usaha
                                                            'jumlah' => $total_price,
                                                            'posisi' => 'D',
                                                            'flag' => '+',
                                                        ]
                                                    ]);
                                                }else if($tipe == '003'){
                                                    //Non Trade Lotte
                                                    TransJurnal::insert([
                                                        [
                                                            'idtrans' => $idtrans,
                                                            'tahun_fiskal_id' => $th_fiskal->id,
                                                            'tanggal' => $tanggal,
                                                            'tmuk_kode' => $tmuk_kode,
                                                            'referensi' => $nomor_pyr,
                                                            'deskripsi' => 'Pembayaran PyR - Non Trade Lotte',
                                                            'coa_kode' => $rekening, // escrow
                                                            'jumlah' => $total_price,
                                                            'posisi' => 'K',
                                                            'flag' => '-',
                                                        ],
                                                        [
                                                            'idtrans' => $idtrans,
                                                            'tahun_fiskal_id' => $th_fiskal->id,
                                                            'tanggal' => $tanggal,
                                                            'tmuk_kode' => $tmuk_kode,
                                                            'referensi' => $nomor_pyr,
                                                            'deskripsi' => 'Pembayaran PyR - Non Trade Lotte',
                                                            'coa_kode' => '2.1.1.0', //Utang Usaha
                                                            'jumlah' => $total_price,
                                                            'posisi' => 'D',
                                                            'flag' => '+',
                                                        ]
                                                    ]);
                                                }else if($tipe == '004'){
                                                    //Non Trade Non Lotte
                                                    TransJurnal::insert([
                                                        [
                                                            'idtrans' => $idtrans,
                                                            'tahun_fiskal_id' => $th_fiskal->id,
                                                            'tanggal' => $tanggal,
                                                            'tmuk_kode' => $tmuk_kode,
                                                            'referensi' => $nomor_pyr,
                                                            'deskripsi' => 'Pembayaran PyR - Non Trade Non Lotte',
                                                            'coa_kode' => $rekening, // escrow
                                                            'jumlah' => $total_price,
                                                            'posisi' => 'K',
                                                            'flag' => '-',
                                                        ],
                                                        [
                                                            'idtrans' => $idtrans,
                                                            'tahun_fiskal_id' => $th_fiskal->id,
                                                            'tanggal' => $tanggal,
                                                            'tmuk_kode' => $tmuk_kode,
                                                            'referensi' => $nomor_pyr,
                                                            'deskripsi' => 'Pembayaran PyR - Non Trade Non Lotte',
                                                            'coa_kode' => '2.1.1.0', //Utang Usaha
                                                            'jumlah' => $total_price,
                                                            'posisi' => 'D',
                                                            'flag' => '+',
                                                        ]
                                                    ]);
                                                }

                                                //update id_transaksi di pyr
                                                $pyr->id_transaksi = $idtrans;
                                                $pyr->save();

                                                //Biaya Admin
                                                TransJurnal::insert([
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Biaya Admin',
                                                        'coa_kode' => '1.1.2.1', //escrow
                                                        'jumlah' => $biaya,
                                                        'posisi' => 'K',
                                                        'flag' => '-',
                                                    ],
                                                    [
                                                        'idtrans' => $idtrans,
                                                        'tahun_fiskal_id' => $th_fiskal->id,
                                                        'tanggal' => $tanggal,
                                                        'tmuk_kode' => $tmuk_kode,
                                                        'referensi' => $nomor_pyr,
                                                        'deskripsi' => 'Biaya Admin',
                                                        'coa_kode' => '8.3.0.0', //Biaya Admin Bank
                                                        'jumlah' => $biaya,
                                                        'posisi' => 'D',
                                                        'flag' => '+',
                                                    ]
                                                ]);
                                            }
                                        }

                                        // insert to log audit
                                        TransLogAudit::setLog([
                                            'tanggal_transaksi' => date('Y-m-d'),
                                            'type'              => 'Pembayaran PyR',
                                            'ref'               => '',
                                            'aksi'              => 'Disetujui',
                                            'amount'            => $total_price,
                                        ]);

                                        return response([
                                            'status' => true,
                                            'data'  => $pembayaran
                                        ]);
                                }
                            } else { // hasilnya gagal
                                // update status yg ho reduce jadi gagal dan buka semua validasi
                                $cek_ponomor = TransPyrPembayaran::where('nomorh2h', $result['CustRefNo'])->where('statush2h', 2)->first();
                                if ($cek_ponomor) {
                                    $cek_ponomor->verifikasi4_date = null;
                                    $cek_ponomor->verifikasi4_user = null;
                                    $cek_ponomor->statush2h        = 4;
                                    $cek_ponomor->keterangan       = $result['ResponseTransaction'];
                                    $cek_ponomor->save();
                                }
                            }
                        }
                        // move file ftp to processed
                        $respon = $this->ftp->move('/Incoming/'. $file, '/Incoming/PROCESSED/'. $file);
                        // var_dump($respon);

                        // remove temp file
                        $str = str_replace ("\\", "/", $tmp_file);
                        if (file_exists(storage_path($str.'.nack|.napr|.ack'))) {
                            unlink(storage_path('temp'.'"\"'.$tmp_file.'.nack|.napr|.ack'));
                        }else{
                            unlink($tmp_file);
                        }   
                        //jika barang PYR
                    }
                }

            } else {
                $this->info('Proses download file '.$file.' gagal');
            }
        }
    }

}
