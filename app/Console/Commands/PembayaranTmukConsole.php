<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmukBayar;

class PembayaranTmukConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'upload:pembayarantmuk {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Pembayaran Tmuk';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/tmp/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            //  looping / populate produk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                // return $data;
                // cek kode prod di table prod setting
                $cek = TransRekapPenjualanTmukBayar::where('rekap_penjualan_kode', $data[0])
                                                    ->where('tipe', $data[1])
                                                    ->where('total', $data[4])->first();
                if (!$cek) {
                    $cek = new TransRekapPenjualanTmukBayar();
                    $cek->rekap_penjualan_kode   = (string)$data[0];
                    $cek->tipe                   = (string)$data[1];
                    $cek->bank_id                = (string)$data[2];
                    $cek->bank_name              = (string)$data[3];
                    $cek->total                  = $this->convertToNumeric($data[4]);
                    $cek->no_kartu               = (string)$data[5];

                    $cek->created_at             = date('Y-m-d H:i:s');
                    $cek->created_by             = 1;
                    var_dump($data[0].'=>'.'simpan jika tidak ada');
                } else {
                    var_dump($data[0].'=>'.'sudah ada');
                }
                $cek->save();
            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

}
