<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;

class ProdukSettingConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'upload:produksetting {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Produk Setting';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/produksetting/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $produks = [];
            $detilproduk = [];

            //  looping / populate produk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                // var_dump($data[14],$data[15],$data[16],$data[17]);
                // var_dump((integer)$datas[);
                // return $data;
                // cek kode prod di table prod setting
                $cek_produk_gmd = Produk::where('kode', $data[0])->first();
                
                if ($cek_produk_gmd) {
                    $setting = ProdukSetting::where('produk_kode', $data[0])->first();
                    if (!$setting ) {
                        $setting = new ProdukSetting();
                    }
                    
                    $setting->produk_kode        = (string)$data[0];

                    $setting->tipe_barang_kode   = (string)$data[1];
                    $setting->jenis_barang_kode  = (string)$data[2];
                    $setting->locked_price       = (string)$data[3];
                    $setting->margin             = $this->convertToNumeric($data[4]);

                    $setting->uom1_satuan        = (string)$data[5];
                    $setting->uom1_selling_cek   = (integer)$data[6];
                    $setting->uom1_order_cek     = (integer)$data[7];
                    $setting->uom1_receiving_cek = (integer)$data[8];
                    $setting->uom1_return_cek    = (integer)$data[9];
                    $setting->uom1_inventory_cek = (integer)$data[10];
                    // $setting->uom1_rak_id        = (integer)$data[11];
                    $setting->uom1_barcode       = (string)$data[12];
                    $setting->uom1_conversion    = (integer)$data[13];
                    // $setting->uom1_width         = $cek_produk_gmd['width'];
                    // $setting->uom1_length         = $cek_produk_gmd['length'];
                    // $setting->uom1_height         = $cek_produk_gmd['height'];
                    // $setting->uom1_weight         = $cek_produk_gmd['wg'];
                    $setting->uom1_width         = $this->convertToNumeric($data[14]);
                    $setting->uom1_height        = $this->convertToNumeric($data[15]);
                    $setting->uom1_length        = $this->convertToNumeric($data[16]);
                    $setting->uom1_weight        = $this->convertToNumeric($data[17]);
                    $setting->uom1_boxtype       = $this->convertToNumeric($data[18]);
                    $setting->uom1_file          = $this->convertToNumeric($data[19]);

                    $setting->uom2_satuan        = (string)$data[20];
                    $setting->uom2_selling_cek   = (integer)$data[21];
                    $setting->uom2_order_cek     = (integer)$data[22];
                    $setting->uom2_receiving_cek = (integer)$data[23];
                    $setting->uom2_return_cek    = (integer)$data[24];
                    $setting->uom2_inventory_cek = (integer)$data[25];
                    // $setting->uom2_rak_id        = (integer)$data[26];
                    $setting->uom2_barcode       = (string)$data[27];
                    $setting->uom2_conversion    = (integer)$data[28];
                    $setting->uom2_width         = $this->convertToNumeric($data[29]);
                    $setting->uom2_height        = $this->convertToNumeric($data[30]);
                    $setting->uom2_length        = $this->convertToNumeric($data[31]);
                    $setting->uom2_weight        = $this->convertToNumeric($data[32]);
                    $setting->uom2_boxtype       = $this->convertToNumeric($data[33]);
                    $setting->uom2_file          = $this->convertToNumeric($data[34]);
                    $setting->uom1_produk_description          = (string)$data[35];

                    // $setting->uom3_satuan        = (string)$data[35];
                    // $setting->uom3_selling_cek   = (integer)$data[36];
                    // $setting->uom3_order_cek     = (integer)$data[37];
                    // $setting->uom3_receiving_cek = (integer)$data[38];
                    // $setting->uom3_return_cek    = (integer)$data[39];
                    // $setting->uom3_inventory_cek = (integer)$data[40];
                    // // $setting->uom3_rak_id        = (integer)$data[41];
                    // $setting->uom3_barcode       = (string)$data[42];
                    // $setting->uom3_conversion    = (integer)$data[43];
                    // $setting->uom3_width         = $this->convertToNumeric($data[44]);
                    // $setting->uom3_height        = $this->convertToNumeric($data[45]);
                    // $setting->uom3_length        = $this->convertToNumeric($data[46]);
                    // $setting->uom3_weight        = $this->convertToNumeric($data[47]);
                    // $setting->uom3_boxtype       = $this->convertToNumeric($data[48]);
                    // $setting->uom3_file          = $this->convertToNumeric($data[49]);

                    // $setting->uom4_satuan        = (string)$data[50];
                    // $setting->uom4_selling_cek   = (integer)$data[51];
                    // $setting->uom4_order_cek     = (integer)$data[52];
                    // $setting->uom4_receiving_cek = (integer)$data[53];
                    // $setting->uom4_return_cek    = (integer)$data[54];
                    // $setting->uom4_inventory_cek = (integer)$data[55];
                    // // $setting->uom4_rak_id        = (integer)$data[56];
                    // $setting->uom4_barcode       = (string)$data[57];
                    // $setting->uom4_conversion    = (integer)$data[58];
                    // $setting->uom4_width         = $this->convertToNumeric($data[59]);
                    // $setting->uom4_height        = $this->convertToNumeric($data[60]);
                    // $setting->uom4_length        = $this->convertToNumeric($data[61]);
                    // $setting->uom4_weight        = $this->convertToNumeric($data[62]);
                    // $setting->uom4_boxtype       = $this->convertToNumeric($data[63]);
                    // $setting->uom4_file          = $this->convertToNumeric($data[64]);

                    $setting->updated_at = date('Y-m-d H:i:s');
                    $setting->updated_by = 1;
                    $setting->created_at = date('Y-m-d H:i:s');
                    $setting->created_by = 1;
                    $setting->save();
                


                }
                else{
                    var_dump('Barang Tidak Ada !');
                }
                
            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

}
