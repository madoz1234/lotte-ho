<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\SecTransaksi;
use Carbon\Carbon;


class DepresiasiConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:depresiasi';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Update Jurnal Depresiasi';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses Update Jurnal Depresiasi...');

        // get tmuk
        $data = [
            ['date' => '2018-09-21', 'referensi' => 'OPENING-20180921060060005700001', 'kode_produk' => '1081974000', 'price' => '3180000'],
            ['date' => '2018-09-21', 'referensi' => 'OPENING-20180921060060005700001', 'kode_produk' => '1083188000', 'price' => '860000'],
            ['date' => '2018-09-21', 'referensi' => 'OPENING-20180921060060005700001', 'kode_produk' => '1083189000', 'price' => '2098800'],
            ['date' => '2018-09-21', 'referensi' => 'OPENING-20180921060060005700001', 'kode_produk' => '1086412000', 'price' => '3915000'],
            ['date' => '2018-09-21', 'referensi' => 'OPENING-20180921060060005700001', 'kode_produk' => '1086413000', 'price' => '6642000'],
            ['date' => '2018-09-21', 'referensi' => 'OPENING-20180921060060005700001', 'kode_produk' => '1086414000', 'price' => '3636000'],
            ['date' => '2018-09-21', 'referensi' => 'OPENING-20180921060060005700001', 'kode_produk' => '1086415000', 'price' => '4540000'],
            ['date' => '2018-09-21', 'referensi' => 'OPENING-20180921060060005700001', 'kode_produk' => '1086416000', 'price' => '2785000'],
            ['date' => '2018-10-04', 'referensi' => 'OPENING-20181005060060005700001', 'kode_produk' => '1083611000', 'price' => '12990000'],
            ['date' => '2018-10-28', 'referensi' => 'OPENING-20181005060060005700004', 'kode_produk' => '1077424000', 'price' => '169000'],
            ['date' => '2018-10-28', 'referensi' => 'OPENING-20181005060060005700004', 'kode_produk' => '1066435000', 'price' => '199000'],
            ['date' => '2018-10-28', 'referensi' => 'OPENING-20181005060060005700004', 'kode_produk' => '1069866000', 'price' => '109000'],
            ['date' => '2018-10-28', 'referensi' => 'OPENING-20181005060060005700004', 'kode_produk' => '1080154000', 'price' => '1199000']
        ];

        $tmuk_kode = '0600600057';

        // kamus jurnal
        $tahun_fiskal = TahunFiskal::getTahunFiskal();
        $deskripsi    = '';

        // loop tmuk
        foreach ($data as $value) {
            $this->info('Update Jurnal Depresiasi Tmuk ' . $value['kode_produk'] . ' - ' . $value['price']);

            //Modal Dasar
            $idtrans = SecTransaksi::generate();
            $tanggal = Carbon::createFromFormat('Y-m-d',$value['date'])->format('Y-m-d 00:00:00');

            $depresiasi_x = $value['price'] / 60;
            for($x = 0; $x < 60; $x++){
                if($x == 0){
                    TransJurnal::insert([
                        [
                            'idtrans'         => $idtrans,
                            'tahun_fiskal_id' => $tahun_fiskal->id,
                            'tanggal'         => $tanggal,
                            'tmuk_kode'       => $tmuk_kode,
                            'referensi'       => $value['referensi'],
                            'deskripsi'       => $value['kode_produk'].' Depresiasi Bulan Ke-'.$x,
                            'coa_kode'        => '6.3.3.0',
                            'jumlah'          => 0,
                            'posisi'          => 'D',
                            'flag'            => '+',
                        ],
                        [
                            'idtrans'         => $idtrans,
                            'tahun_fiskal_id' => $tahun_fiskal->id,
                            'tanggal'         => $tanggal,
                            'tmuk_kode'       => $tmuk_kode,
                            'referensi'       => $value['referensi'],
                            'deskripsi'       => $value['kode_produk'].' Depresiasi Bulan Ke-'.$x,
                            'coa_kode'        => '1.2.7.0',
                            'jumlah'          => 0,
                            'posisi'          => 'K',
                            'flag'            => '-',
                        ]
                    ]);
                }else{
                    $day_next = Carbon::createFromFormat('Y-m-d',$value['date'])->addMonths($x)->endOfMonth()->format('Y-m-d 00:00:00');
                    $nilai_depresiasi = $depresiasi_x * $x;
                    $idtrans_next = SecTransaksi::generate();

                    TransJurnal::insert([
                        [
                            'idtrans'         => $idtrans_next,
                            'tahun_fiskal_id' => $tahun_fiskal->id,
                            'tanggal'         => $day_next,
                            'tmuk_kode'       => $tmuk_kode,
                            'referensi'       => $value['referensi'],
                            'deskripsi'       => $value['kode_produk'].' Depresiasi Bulan Ke-'.$x,
                            'coa_kode'        => '6.3.3.0',
                            'jumlah'          => $nilai_depresiasi,
                            'posisi'          => 'D',
                            'flag'            => '+',
                        ],
                        [
                            'idtrans'         => $idtrans_next,
                            'tahun_fiskal_id' => $tahun_fiskal->id,
                            'tanggal'         => $day_next,
                            'tmuk_kode'       => $tmuk_kode,
                            'referensi'       => $value['referensi'],
                            'deskripsi'       => $value['kode_produk'].' Depresiasi Bulan Ke-'.$x,
                            'coa_kode'        => '1.2.7.0',
                            'jumlah'          => $nilai_depresiasi,
                            'posisi'          => 'K',
                            'flag'            => '-',
                        ]
                    ]);
                }
            }
            
        }

        $this->info('Update Jurnal Depresiasi Done');
    }

}
