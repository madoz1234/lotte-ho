<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;

/* modal */
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;

class PenjualanDetailTmukConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'upload:penjualandetailtmuk {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Upload Penjualan Detail Tmuk';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* kamus data */
        $filename = $this->option('filename');

        // baca file excel yg telah di upload
        $this->info('Baca File Excel...');
        $filename = storage_path('upload-produk/tmp/' . $filename);

        Excel::load($filename, function($reader) {
            // get info worksheet
            $sheet         = $reader->getSheet(0); 
            $highestRow    = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            //  looping / populate produk from excel
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE)[0];
                // return $data;
                // cek kode prod di table trans_rekap_penjualan_detail
                $cek = TransRekapPenjualanTmukDetail::where('rekap_penjualan_kode', $data[0])
                                                     ->where('description', $data[3])
                                                     ->where('harga', $data[4])
                                                     ->where('harga_member', $data[5])
                                                     ->where('qty', $data[6])
                                                     ->where('satuan', $data[7])
                                                     ->where('discount', $data[8])
                                                     ->where('disc_percent', $data[9])
                                                     ->where('disc_amount', $data[10])
                                                     ->where('total', $data[11])
                                                     ->where('tax_type', $data[13])
                                                     ->where('harga_hpp3', $data[14])->first();
                if (!$cek) {
                    $cek = new TransRekapPenjualanTmukDetail();
                    $cek->rekap_penjualan_kode   = (string)$data[0];
                    $cek->item_code              = (integer)$data[1];
                    $cek->barcode                = (string)$data[2];
                    $cek->description            = (string)$data[3];
                    $cek->harga                  = $this->convertToNumeric($data[4]);
                    $cek->harga_member           = $this->convertToNumeric($data[5]);
                    $cek->qty                    = (string)$data[6];
                    $cek->satuan                 = (string)$data[7];
                    $cek->discount               = $this->convertToNumeric($data[8]);
                    $cek->disc_percent           = $this->convertToNumeric($data[9]);
                    $cek->disc_amount            = $this->convertToNumeric($data[10]);
                    $cek->total                  = $this->convertToNumeric($data[11]);
                    $cek->promo                  = (string)$data[12];
                    $cek->tax_type               = (string)$data[13];
                    $cek->harga_hpp3             = $this->convertToNumeric($data[14]);

                    $cek->created_at = date('Y-m-d H:i:s');
                    $cek->created_by = 1;
                    var_dump($data[0].'=>'.'Detail Struk Tersimpan !');
                } else {
                    var_dump($data[0].'=>'.'sudah ada');
                }
                $cek->save();
            }

        });
    }

    public function convertToNumeric($str)
    {
        $str = str_replace(',', '.', $str);

        return (float) $str;
    }

    public function potong($pret)
    {
        $pret = substr($pret,6);

        return (float) $pret;
    }

}
