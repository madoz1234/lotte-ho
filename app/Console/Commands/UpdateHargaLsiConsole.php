<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Excel;
use Lotte\Libraries\ApiGmd;

/* modal */
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\HargaTmuk;

class UpdateHargaLsiConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // php artisan upload:produk --filename=""
    protected $signature = 'update:harga';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Update harga lsi';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses Update Harga LSI...');

        // get lsi
        $reslsi = Lsi::get();

        // loop lsi
        foreach ($reslsi as $lsi) {
            $this->info('Update harga lsi ' . $lsi->nama);

            // populate produk
            $produks = [];
            $res_produk = Produk::get();

            // transform produk
            $count = 1;
            $idx   = 1;
            foreach ($res_produk as $prod) {
                if ($count == 400) {
                    $count = 1;
                    $idx++;
                }

                $produks[$idx][] = $prod->kode;
                $count++;
            }

            foreach ($produks as $prod) {
                // get harga lsi
                $response = ApiGmd::getProduct($lsi->kode, $prod);

                // cek is json // ke deui
                $json = json_decode($response, true);

                if ($json['status'] == true) { // jika terdapat info dari gmd

                    // loop data produk
                    $this->output->progressStart(count($json['data'])); // init progress
                    
                    foreach ($json['data'] as $val) {
                        $result = $val;

                        // ins/upd -> ref_produk_lsi
                        $produk = ProdukLsi::where('lsi_kode', $lsi->kode)
                                           ->where('produk_kode', $result['PROD_CD'])->first();

                        if (!$produk) {
                            $produk = new ProdukLsi();

                            $produk->lsi_kode    = $lsi->kode;
                            $produk->produk_kode = $result['PROD_CD'];
                            
                            $produk->created_at  = date('Y-m-d H:i:s');
                            $produk->created_by  = 1;
                        }

                        $this->fillProdukLsi($produk, $result);

                        // ins/upd -> ref_harga_tmuk
                        // $produk = HargaTmuk::where('lsi_kode', $lsi->kode)
                        //                    ->where('produk_kode', $result['PROD_CD'])->first();
                        // if (!$produk) {
                        //     $produk = new ProdukLsi();

                        //     $produk->lsi_kode    = $lsi->kode;
                        //     $produk->produk_kode = $result['PROD_CD'];
                            
                        //     $produk->created_at  = date('Y-m-d H:i:s');
                        //     $produk->created_by  = 1;
                        // }

                        $this->output->progressAdvance();
                    }// end loop data produk
                    $this->output->progressFinish();
                }   
            }
        }
    }

    private function fillProdukLsi($produk, $data)
    {

        $produk->stok_gmd      = (int) $data['QTY'];
        $produk->curr_sale_prc = $data['CURR_SALE_PRC'];
        $produk->bblm_flag     = $data['BBLM_FG'];
        $produk->con1          = $data['CON1'];
        $produk->dc1           = $data['DC1'];
        $produk->con2          = $data['CON2'];
        $produk->dc2           = $data['DC2'];
        $produk->con3          = $data['CON3'];
        $produk->dc3           = $data['DC3'];
        $produk->limit         = $data['LIMIT'];
        // $produk->status        = $data['STATUS_PRODUCT'];
        $produk->status        = strtoupper($data['STATUS_PRODUCT']);


        $produk->updated_at = date('Y-m-d H:i:s');
        $produk->updated_by = 1;

        $produk->save();
    }

    private function fillProdukSetting($produk, $data, $detilproduk=[])
    {
        
        $produk->uom4_file          = null;

        $produk->updated_at = date('Y-m-d H:i:s');
        $produk->updated_by = 1;
        $produk->save();
    }

}
