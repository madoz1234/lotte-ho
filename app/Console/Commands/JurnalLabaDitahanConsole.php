<?php

namespace Lotte\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

/* modal */
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\Tmuk;
use Carbon\Carbon;

class JurnalLabaDitahanConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:jurnallaba {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $description = 'Update Jurnal Laba Ditahan';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::createFromFormat('m-Y', $this->option('date'));

        $this->info('Proses Update Jurnal Laba Ditahan Tanggal '.$date->startOfMonth()->format('Y-m-d').' ...');

        // get tmuk
        $all_tmuk = Tmuk::get();
        // kamus jurnal
        $deskripsi    = '';
        $jurnal_date = $date->startOfMonth()->format('Y-m-d');
        $now         = $date->subMonths(1);
        $monthStart  = $now->startOfMonth()->format('Y-m-d'). ' 00:00:00';
        $monthEnd    = $now->endOfMonth()->format('Y-m-d'). ' 23:59:59';

        // loop tmuk
        foreach ($all_tmuk as $tmuk) {
            $this->info('Update Jurnal Laba Ditahan Tmuk ' . $tmuk->kode . ' - ' . $tmuk->nama);

            $check = TransJurnal::whereBetween('tanggal', array($jurnal_date.' 00:00:00', $jurnal_date.' 23:59:59'))
                                ->where('coa_kode', '3.4.0.0')
                                ->where('tmuk_kode', $tmuk->kode)
                                ->where('delete', 0)
                                ->first();

            if ($check) {
                // Mencari laba bulan kemarin
                $jurnal = TransJurnal::whereBetween('tanggal', array($monthStart, $monthEnd))
                                    ->where('tmuk_kode', $tmuk->kode)
                                    ->where('delete', 0)
                                    ->get();
                
                $coas = ['4.1.0.0', '4.1.1.0', '4.2.0.0'];
                $penjualan_barang_dagang = $jurnal->filter(function($item) use ($coas){
                    return in_array($item->coa_kode, $coas);
                })->sum('jumlah');

                $potongan_penjualan = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '4.3.0.0';
                })->sum('jumlah');

                $total_pendapatan_usaha = abs($penjualan_barang_dagang) + -abs($potongan_penjualan);

                $hpp = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '5.1.0.0';
                })->sum('jumlah');

                $coas = ['6.1.1.0', '6.1.1.1', '6.1.1.2', '6.1.1.3', '6.1.1.4'];
                $total_biaya_pemasaran_umum = $jurnal->filter(function($item) use ($coas){
                    return in_array($item->coa_kode, $coas);
                })->sum('jumlah');

                $coas = ['6.2.1.1', '6.2.1.2', '6.2.1.3', '6.2.1.4', '6.2.2.1', '6.2.2.2', '6.2.2.3', '6.2.2.4', '6.2.2.5', '6.2.2.6', '6.2.2.7', '6.2.2.8', '6.2.2.9', '6.2.2.10', '6.2.3.1', '6.2.3.2', '6.2.3.3', '6.3.1.0', '6.3.2.0', '6.3.3.0', '6.2.2.11', '6.2.2.12', '6.2.2.13', '6.2.2.14', '6.2.2.15'];
                $total_beban_umum_administrasi = $jurnal->filter(function($item) use ($coas){
                    return in_array($item->coa_kode, $coas);
                })->sum('jumlah');

                $selisih_kas_lebih = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '7.1.0.0';
                })->sum('jumlah');

                $selisih_kas_kurang = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '8.1.0.0';
                })->sum('jumlah');

                $penghasilan_deviden = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '7.2.0.0';
                })->sum('jumlah');

                $laba_penjualan_aktiva_tetap = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '7.3.0.0';
                })->sum('jumlah');

                $rugi_penjualan_aktiva_tetap = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '8.2.0.0';
                })->sum('jumlah');

                $penghasilan_sewa = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '7.4.0.0';
                })->sum('jumlah');
                
                $biaya_admin_bank = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '8.3.0.0';
                })->sum('jumlah');
                
                $penghasilan_lainnya = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '7.5.0.0';
                })->sum('jumlah');

                $beban_lainnya = $jurnal->filter(function ($item) {
                    return $item->coa_kode == '8.4.0.0';
                })->sum('jumlah');

                $total_pendapatan_beban_lain2 = abs($selisih_kas_lebih) + -abs($selisih_kas_kurang) + -abs($penghasilan_deviden) + abs($laba_penjualan_aktiva_tetap) + -abs($rugi_penjualan_aktiva_tetap) + -abs($penghasilan_sewa) + -abs($biaya_admin_bank) + abs($penghasilan_lainnya) + -abs($beban_lainnya);

                $pendapatan_kotor = abs($total_pendapatan_usaha) - abs($hpp);
                $laba_usaha       = abs($pendapatan_kotor) + -abs($total_biaya_pemasaran_umum) + -abs($total_beban_umum_administrasi);
                $total_laba_rugi_bersih = round($laba_usaha + $total_pendapatan_beban_lain2);

                if ($total_laba_rugi_bersih < 0) {
                    //Update Laba Ditahan
                    $check->jumlah = abs($total_laba_rugi_bersih);
                    $check->posisi = 'D';
                    $check->flag   = '-';
                    $check->save();
                }else{
                    //Update Laba Ditahan
                    $check->jumlah = abs($total_laba_rugi_bersih);
                    $check->posisi = 'K';
                    $check->flag   = '+';
                    $check->save();
                }

                $this->info('Success!, Jurnal Laba Ditahan '.$check->idtrans.' Telah Diupdate');
            }else{
                $this->info('Jurnal Laba Ditahan Tidak Ditemukan');
            }
            
        }

        $this->info('Proses Update Jurnal Laba Ditahan '.$date->startOfMonth()->format('Y-m-d').' Done');
    }

}
