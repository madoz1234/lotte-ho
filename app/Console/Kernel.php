<?php

namespace Lotte\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\UploadProdukGmdConsole::class,
        Commands\UpdateHargaLsiConsole::class,
        Commands\ProdukSettingConsole::class,
        Commands\ProdukAssortmentConsole::class,
        Commands\HargaTmukConsole::class,
        Commands\PenjualanTmukConsole::class,
        Commands\PenjualanDetailTmukConsole::class,
        Commands\PembayaranTmukConsole::class,
        Commands\ProdukTmukConsole::class,
        Commands\JurnalConsole::class,
        Commands\JurnalLabaDitahanConsole::class,
        Commands\PromosiConsole::class,
        Commands\ProdukTmukDetailConsole::class,
        Commands\PersediaanTmukDetailConsole::class,
        Commands\GrDetailConsole::class,
        Commands\SoDetailConsole::class,
        Commands\PyrDetailConsole::class,
        Commands\H2hServiceConsole::class,
        Commands\BackupDatabaseConsole::class,
        Commands\DepresiasiConsole::class,
        Commands\HppMapConsole::class,
        Commands\PoDetailConsole::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();

        $schedule->command('update:jurnal')
                 ->monthly()
                 ->at('00:00');
                 
        $schedule->command('h2h:service')
                 ->everyMinute();
        // $schedule->command('db:backup')
        //          ->dailyAt('00:15');
    }

    // protected function schedule(Schedule $schedule)
    // {
    //     $schedule->command('update:harga')
    //              ->dailyAt('12:35');
    // }
}
