<?php

namespace Lotte\Libraries;

class ApiGmd
{

	public static function getProduct($lsi, $produks=[])
	{
		// kamus data
		$url = config('app.gmd_url');

		// re format param
		// dd($lsi);
		$lsi = "'{$lsi}'";
		$produk = '';
		if (is_array($produks)) {
			$tmp = [];
			foreach ($produks as $val) {
				$tmp[] = "'{$val}'";
			}
			$produk = implode(',', $tmp);

		} else {
			$produk = "'{$produks}'";
		}

		$response = ApiGmd::postData($url . '/product', [
			'STR_CD' => $lsi,
			'PROD_CD' => $produk,
		]);

		return $response;
	}
	
	public static function getPrice($lsi, $produks=[])
	{
		// kamus data
		$url = config('app.gmd_url');

		// re format param
		// $lsi = "'{$lsi}'";
		$produk = '';
		if (is_array($produks)) {
			$tmp = [];
			foreach ($produks as $val) {
				$tmp[] = "'{$val}'";
			}
			$produk = implode(',', $produk);

		} else {
			$produk = "'{$produks}'";
		}

		$response = ApiGmd::postData($url . '/price', [
			'STR_CD' => $lsi,
			'PROD_CD' => $produk,
		]);

		return $response;
	}

	public static function getStruk($nomor)
	{
		// kamus data
		$url = config('app.gmd_url');

		$response = ApiGmd::getData($url . '/invoice/INV/' . $nomor);

		return $response;
	}

	/* utility */
	public static function postData($url, $param=[])
	{
		$apiKey = config('app.gmd_key');
		
		$curl = curl_init($url);
		$headers = array(
			'X-API-KEY: '.$apiKey.''
		);
		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
                //dd($param);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		
		return $curl_response;
	}

	public static function getData($url, $param=[])
	{
		$apiKey = config('app.gmd_key');
		
		$curl = curl_init($url);
		$headers = array(
			'X-API-KEY: '.$apiKey.''
		);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_POST, 0);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		
		return $curl_response;
	}
}
