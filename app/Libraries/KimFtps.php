<?php
// FTP yang basic-nya pake curl ^.^
namespace Lotte\Libraries;

/*
	cara pakai
	----------------------------------------
    $ftp = new KimFtps('ftp://fsrv.bri.co.id', 50021, 'lotteshopping', 'lotte608!@');
    
    #get files
    $respon = $ftp->getFiles('/Incoming/');

    #get files specified
    $respon = $ftp->getFiles('/Incoming/', 'xls|doc');

    #download file
    $respon = $ftp->download('/Outgoing/Payment_Priority_CMS_20181018_153649.csv', storage_path('temp/hasil-download.csv'));
    
    #upload file
    $respon = $ftp->upload('/OutGoing/test-upload-file.txt', storage_path('temp/upload.txt'));
    
    #delete file
    $respon = $ftp->delete('/test-upload-file.txt');
    
    #move file remote
    $respon = $ftp->move('/test-upload-file.txt', '/Outgoing/test-upload-file-move.txt');
    
    $ftp->disconnect();
*/

class KimFtps
{
	var $host = '';
	var $port = 21;
	var $user = '';
	var $pass = '';

	// handle curl
	var $curl;
	var $errorCode = '';
	var $errorMsg = '';

	public function __construct($_host, $_port, $_user, $_pass)
	{
		// set attributes
		$this->host = $_host;
		$this->port = $_port;
		$this->user = $_user;
		$this->pass = $_pass;

		// init curl
		$this->curl = curl_init();
	}

	public function disconnect()
	{
		curl_close ($this->curl);
	}

	public function ls($dir='')
	{
		// init option curl
		$this->initOpt();

		// custom option
	    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'LIST');
	    if ($dir) {
	    	curl_setopt($this->curl, CURLOPT_URL, $this->host.$dir);
	    }

	    // execute
	    $response = curl_exec ($this->curl);

	    // set error msg
		$this->errorCode = curl_errno($this->curl);
		$this->errorMsg  = curl_error($this->curl);

		// populate list
		$fichiers = [];
        $nbFichiers = 0;
        if (preg_match_all(
            '/([-dl])([rwxst-]{9})[ ]+([0-9]+)[ ]+([^ ]+)[ ]+(.+)[ ]+([0-9]+)[ ]+([a-zA-Z]+[ ]+[0-9]+)[ ]+([0-9:]+)[ ]+(.*)/',
            $response, $m, PREG_SET_ORDER)) {
            foreach ($m as $f) {
                $fichiers[$nbFichiers] = array();
                $fichiers[$nbFichiers]['dir']         = $f[1] == 'd';
                $fichiers[$nbFichiers]['filename']    = trim(str_replace(["\r", "\n"], ["", ""], $f[9]));
                $fichiers[$nbFichiers]['size']        = $f[6];
                $fichiers[$nbFichiers]['owner']       = $f[4];
                $fichiers[$nbFichiers]['group']       = $f[5];
                $fichiers[$nbFichiers]['permissions'] = $f[2];
                $fichiers[$nbFichiers]['mtime']       = "$f[7] $f[8]";
                $nbFichiers++;
            }
        }

        return $fichiers;
	}

	public function getFiles($dir='', $ext='')
	{
		$response = [];

		$result = $this->ls($dir);
		foreach ($result as $key => $value) {
			if ($value['dir'] == false) {
				if ($ext) {
					if(preg_match("/\.(".$ext.")$/", $value['filename'])) {
						$response[] = $value['filename'];
					}
				} else {
					$response[] = $value['filename'];
				}
			}
		}

		return $response;
	}

	public function download($remoteFile, $localFile)
	{
		// init file
		$file = fopen($localFile, 'w');

		// init option curl
		$this->initOpt();

		// custom option
	    curl_setopt($this->curl, CURLOPT_URL, $this->host.$remoteFile);
	    curl_setopt($this->curl, CURLOPT_FILE, $file); #output

	    // execute
	    $response = curl_exec ($this->curl);

	    // set error msg
		$this->errorCode = curl_errno($this->curl);
		$this->errorMsg  = curl_error($this->curl);

		// close file
		fclose($file);

		// return $response;
		return $this->errorCode == 0;
	}

	public function upload($remoteFile, $localFile)
	{
		// init file
		$file = fopen($localFile, 'r');

		// init option curl
		$this->initOpt();

		// custom option
	    curl_setopt($this->curl, CURLOPT_URL, $this->host.$remoteFile);
		curl_setopt($this->curl, CURLOPT_UPLOAD, 1);
	    curl_setopt($this->curl, CURLOPT_INFILE, $file);
	    curl_setopt($this->curl, CURLOPT_INFILESIZE, filesize($localFile));

	    // execute
	    $response = curl_exec ($this->curl);

	    // set error msg
		$this->errorCode = curl_errno($this->curl);
		$this->errorMsg  = curl_error($this->curl);

		// close file
		fclose($file);

		return $this->errorCode == 0;
	}

	public function delete($remoteFile)
	{
		// init option curl
		$this->initOpt();

		// custom option
		curl_setopt($this->curl, CURLOPT_QUOTE, array('DELE ' . $remoteFile));

	    // execute
	    $response = curl_exec ($this->curl);

	    // set error msg
		$this->errorCode = curl_errno($this->curl);
		$this->errorMsg  = curl_error($this->curl);

		return $this->errorCode == 0;
	}

	public function move($source, $destination)
	{
		$success = false;
		$filename_source = basename($source);
		// $filename_destination = basename($destination);
		$local_temp = storage_path('temp') . '/';

		// download file to local temp
		if ($this->download($source, $local_temp . $filename_source)) {
			// upload localfile to remote destination
			if ($this->upload($destination, $local_temp . $filename_source)) {
				$this->delete($source);
				$success = true;
			}
			// delete local file temp
			$this->unlink($local_temp . $filename_source);
		}
		
		return $success;
	}

	/* utilities */
	private function initOpt()
	{
		curl_reset($this->curl);

		curl_setopt($this->curl, CURLOPT_URL, $this->host);
	    curl_setopt($this->curl, CURLOPT_PORT, $this->port);
	    curl_setopt($this->curl, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
	    curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
	    //SSL stuff
	    curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);  //use for development only; unsecure 
	    curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);  //use for development only; unsecure
	    curl_setopt($this->curl, CURLOPT_FTP_SSL, CURLOPT_FTPSSLAUTH);
	    curl_setopt($this->curl, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_TLS); 
	    //curl_setopt($this->curl, CURLOPT_SSLVERSION, 3);
	    //end SSL
	    curl_setopt($this->curl, CURLOPT_VERBOSE, TRUE);
	    curl_setopt($this->curl, CURLOPT_TIMEOUT, 10);
	}

	private function unlink($filename)
	{
		if (file_exists($filename)) {
			unlink($filename);
		}
	}
}
