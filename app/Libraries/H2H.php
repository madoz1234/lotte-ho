<?php
namespace Lotte\Libraries;

class H2H
{
	public static function createCsv($param=[])
	{
		$attr = [
			'SequenceNumber' => '',
			'CustRefNo' => '',
			'InstructionCode' => 'IFT',
			'FxCode' => 'ID',
			'ValueDate' => '',
			'Currency' => 'IDR',
			'Amount' => '',
			'TrxRemark' => 'Invoice Payment',
			'RateVoucherCode' => '',
			'ChargeType' => 'OUR',
			'DebitAccount' => '',
			'SenderName' => '',
			'SenderAddress' => '',
			'SenderCountryCode' => '',
			'CreditAccount' => '',
			'BeneficiaryName' => '',
			'BeneficiaryAddress' => '',
			'BeneficiaryCountryCode' => 'ID',
			'BenBankIdentifier' => '',
			'BenBankName' => '',
			'BenBankAddress' => '',
			'BenBankCountryCode' => '',
			'InterBankIdentifier' => '',
			'InterBankName' => '',
			'InterBankAddress' => '',
			'InterBankCountryCode' => '',
			'Notification' => '',
			'BeneficiaryCategory' => '',
			'BeneficiaryRelation' => '',
			'BITransaction Code' => '',
			'TemplateCode' => '',
		];

		$attr = array_merge($attr, $param);

		$tmp = [];
		foreach ($attr as $val) {
			$tmp[] = $val;
		}

		$output = implode('|', $tmp);

		// save to storage
		file_put_contents(storage_path('temp\\' . $attr['CustRefNo'] . '.csv'), $output);
		// file_put_contents(storage_path('/temp/' . $attr['CustRefNo'] . '.csv'), $output);
	}

	public static function readResponseBri($str)
	{
		$output = [];

		$rows = explode("\n", $str);
		foreach ($rows as $row) {
			$cnt = explode("|", $row);
			$tpl = [
				'SequenceNumber'         => isset($cnt[0]) ? $cnt[0] : '',
				'CustRefNo'              => isset($cnt[1]) ? $cnt[1] : '',
				'InstructionCode'        => isset($cnt[2]) ? $cnt[2] : '',
				'FxCode'                 => isset($cnt[3]) ? $cnt[3] : '',
				'ValueDate'              => isset($cnt[4]) ? $cnt[4] : '',
				'Currency'               => isset($cnt[5]) ? $cnt[5] : '',
				'Amount'                 => isset($cnt[6]) ? $cnt[6] : '',
				'TrxRemark'              => isset($cnt[7]) ? $cnt[7] : '',
				'RateVoucherCode'        => isset($cnt[8]) ? $cnt[8] : '',
				'ChargeType'             => isset($cnt[9]) ? $cnt[9] : '',
				'DebitAccount'           => isset($cnt[10]) ? $cnt[10] : '',
				'SenderName'             => isset($cnt[11]) ? $cnt[11] : '',
				'SenderAddress'          => isset($cnt[12]) ? $cnt[12] : '',
				'SenderCountryCode'      => isset($cnt[13]) ? $cnt[13] : '',
				'CreditAccount'          => isset($cnt[14]) ? $cnt[14] : '',
				'BeneficiaryName'        => isset($cnt[15]) ? $cnt[15] : '',
				'BeneficiaryAddress'     => isset($cnt[16]) ? $cnt[16] : '',
				'BeneficiaryCountryCode' => isset($cnt[17]) ? $cnt[17] : '',
				'BenBankName'            => isset($cnt[18]) ? $cnt[18] : '',
				'BenBankIdentifier'      => isset($cnt[19]) ? $cnt[19] : '',
				'BenBankAddress'         => isset($cnt[20]) ? $cnt[20] : '',
				'BenBankCountryCode'     => isset($cnt[21]) ? $cnt[21] : '',
				'InterBankIdentifier'    => isset($cnt[22]) ? $cnt[22] : '',
				'InterBankName'          => isset($cnt[23]) ? $cnt[23] : '',
				'InterBankAddress'       => isset($cnt[24]) ? $cnt[24] : '',
				'InterBankCountryCode'   => isset($cnt[25]) ? $cnt[25] : '',
				'Notification'           => isset($cnt[26]) ? $cnt[26] : '',
				'BeneficiaryCategory'    => isset($cnt[27]) ? $cnt[27] : '',
				'BeneficiaryRelation'    => isset($cnt[28]) ? $cnt[28] : '',
				'BITransactionCode'      => isset($cnt[29]) ? $cnt[29] : '',
				'ResponseTransaction'    => isset($cnt[30]) ? $cnt[30] : '',
			];
			
			$output[] = $tpl;
		}

		return $output;
	}

}
