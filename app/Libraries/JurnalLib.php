<?php

namespace Lotte\Libraries;

/* models */
use Lotte\Models\Trans\TransJurnal;
use Carbon\Carbon;

class JurnalLib
{

	/*
		parameter
		  - tmuk_kode (string)
		  - tgl1 (string: yyyy-mm-dd)
		  - tgl2 (string: yyyy-mm-dd)
		  - template (array)
	*/
	public static function generate($tmuk, $tgl1, $tgl2, $tpl, $source=[])
	{
		$t1 = Carbon::parse($tgl1);
		$t2 = Carbon::parse($tgl2);

		$tmp = [];
		$header = [];

		$t2->addMonthNoOverflow();
		while ($t1->format('Y-m') != $t2->format('Y-m')) {
			
			$result = TransJurnal::selectRaw('tahun, bulan, bulanstr, coa_kode, (sekarang + (case when sebelum is null then 0 else sebelum end ) ) as jumlah')
						->from(\DB::raw("(
							select TO_CHAR(tanggal, 'yyyy') as tahun, TO_CHAR(tanggal, 'mm') as bulan, TO_CHAR(tanggal, 'Mon') as bulanstr,
									coa_kode, sum(case when flag = '-' then -jumlah else jumlah end) as sekarang,
									(
										select sum(case when flag = '-' then -jumlah else jumlah end) as jumlah
										from trans_jurnal
										where tanggal < '".$t1->startOfMonth()->format('Y-m-d')."'
										and tmuk_kode = '".$tmuk."'
										and coa_kode = tj_p.coa_kode
										and \"delete\" = 0
									) as sebelum
							from trans_jurnal tj_p
							where tanggal between '".$t1->startOfMonth()->format('Y-m-d')." 00:00:00' and '".$t1->endOfMonth()->format('Y-m-d')." 23:59:59'
							and tmuk_kode = '".$tmuk."'
							and \"delete\" = 0
							group by tahun, bulan, bulanstr, coa_kode
							order by tahun, bulan
						) tblx"))
						->get();

				// transform
				foreach ($result as $row) {
					$tmp[$row->bulan][$row->coa_kode] = $row;
				}

			$t1->addMonthNoOverflow();
		}

		foreach ($tmp as $key => $row) {
			foreach ($row as $val) {
				$header[$val->bulan] = $val->bulanstr . ' ' . $val->tahun;
				break;
			}
		}
		
		$return = $tpl;
		// generate
		foreach ($tpl as $key => $val) {
			// label
			if ($val['type'] == 'label') {
				// has child
				if (isset($val['child'])) {
					// loop child
					foreach ($val['child'] as $keychild => $valchild) {
						if ($valchild['type'] == 'post') {
							// init data
							$return[$key]['child'][$keychild]['data'] = [];
							// loop header
							foreach ($header as $bl => $v) {
								// loop coa
								$total = 0;
								foreach ($valchild['coa'] as $coa) {
									// check coa value in array
									if (isset($tmp[$bl][$coa])) {
										$total += (float) $tmp[$bl][$coa]->jumlah;
									}
								}

								if (isset($return[$key]['child'][$keychild]['value'])) {
									if ($return[$key]['child'][$keychild]['value'] == 'negative') {
										$total = -abs($total);
									} else {
										$total = abs($total);
									}
								}
								$return[$key]['child'][$keychild]['data'][$bl] = $total;
							}

						} else if ($valchild['type'] == 'ref') {
							// init data
							$return[$key]['child'][$keychild]['data'] = [];
							
							// loop header
							foreach ($header as $bl => $v) {
								// set value by ref
								$tmp_total = 0;
								if (isset($source[$valchild['source']]['content'][$valchild['exp']]['data'][$bl])) {
									$tmp_total = $source[$valchild['source']]['content'][$valchild['exp']]['data'][$bl];
								}

								$return[$key]['child'][$keychild]['data'][$bl] = $tmp_total;
							}
						}
					}
				}
			}

			// post
			else if ($val['type'] == 'post') {
				// init data
				$return[$key]['data'] = [];
				// loop header
				foreach ($header as $bl => $v) {
					// loop coa
					$total = 0;
					foreach ($val['coa'] as $coa) {
						// check coa value in array
						if (isset($tmp[$bl][$coa])) {
							$total += (float) $tmp[$bl][$coa]->jumlah;
						}
					}

					if (isset($return[$key]['value'])) {
						if ($return[$key]['value'] == 'negative') {
							$total = -abs($total);
						} else {
							$total = abs($total);
						}
					}
					$return[$key]['data'][$bl] = $total;
				}
			}

			// increase
			else if ($val['type'] == 'inc') {
				// init data
				$return[$key]['data'] = [];
				// loop header
				foreach ($header as $bl => $v) {
					// loop coa
					$total = 0;
					foreach ($val['exp'] as $exp) {
						// check exp value in array return
						$tmp_total = 0;
						if (isset($return[$exp])) {
							if ($return[$exp]['type'] == 'label' && isset($return[$exp]['child'])) { // jika punya child
								foreach ($return[$exp]['child'] as $keychild => $valchild) {
									$tmp_total += $return[$exp]['child'][$keychild]['data'][$bl];
								}

							} else if ($return[$exp]['type'] == 'post') { // post
								$tmp_total = $return[$exp]['data'][$bl];

							} else if ($return[$exp]['type'] == 'inc' || $return[$exp]['type'] == 'dec') { // dec/inc
								$tmp_total = $return[$exp]['data'][$bl];
							}
						}
						if ($total == 0) {
							$total = $tmp_total;
						} else {
							$total += $tmp_total;
						}
					}

					$return[$key]['data'][$bl] = $total;
				}
			}

			// decrease
			else if ($val['type'] == 'dec') {
				// init data
				$return[$key]['data'] = [];
				// loop header
				foreach ($header as $bl => $v) {
					// loop coa
					$total = 0;
					foreach ($val['exp'] as $exp) {
						// check exp value in array return
						$tmp_total = 0;
						if (isset($return[$exp])) {
							if ($return[$exp]['type'] == 'label' && isset($return[$exp]['child'])) { // jika punya child
								foreach ($return[$exp]['child'] as $keychild => $valchild) {
									$tmp_total += $return[$exp]['child'][$keychild]['data'][$bl];
								}

							} else if ($return[$exp]['type'] == 'post') { // post
								$tmp_total = $return[$exp]['data'][$bl];
							} else if ($return[$exp]['type'] == 'inc' || $return[$exp]['type'] == 'dec') { // dec/inc
								$tmp_total = $return[$exp]['data'][$bl];
							}
						}
						if ($total == 0) {
							$total = $tmp_total;
						} else {
							$total -= $tmp_total;
						}
					}

					$return[$key]['data'][$bl] = $total;
				}
			}

			// by ref
			else if ($val['type'] == 'ref') {
				// init data
				$return[$key]['data'] = [];

				// loop header
				foreach ($header as $bl => $v) {
					// set value by ref
					$tmp_total = 0;
					if (isset($source[$val['source']]['content'][$val['exp']]['data'][$bl])) {
						$tmp_total = $source[$val['source']]['content'][$val['exp']]['data'][$bl];
					}
					
					$return[$key]['data'][$bl] = $tmp_total;
				}
			}
		}

		return [
			'header' => $header,
			'content' => $return,
		];
	}
	
	public static function generateLaba($tmuk, $tgl1, $tgl2, $tpl, $source=[])
	{
		// get record jurnal by
		$result = TransJurnal::select(\DB::raw("TO_CHAR(tanggal, 'yyyy') as tahun, TO_CHAR(tanggal, 'mm') as bulan, TO_CHAR(tanggal, 'Mon') as bulanstr,  coa_kode, sum(case when flag = '-' then -jumlah else jumlah end) as jumlah"))
					->groupBy('tahun', 'bulan', 'bulanstr', 'coa_kode')
					->whereBetween('tanggal', [$tgl1, $tgl2])
					->where('tmuk_kode', $tmuk)
					->where('delete', 0)
					->orderBy('tahun')
					->orderBy('bulan')
					->get();
		// transform
		$tmp = [];
		foreach ($result as $row) {
			$tmp[$row->bulan][$row->coa_kode] = $row;
		}

		$header = [];
		foreach ($tmp as $key => $row) {
			foreach ($row as $val) {
				$header[$val->bulan] = $val->bulanstr . ' ' . $val->tahun;
				break;
			}
		}

		$return = $tpl;

		// generate
		foreach ($tpl as $key => $val) {
			// label
			if ($val['type'] == 'label') {
				// has child
				if (isset($val['child'])) {
					// loop child
					foreach ($val['child'] as $keychild => $valchild) {
						if ($valchild['type'] == 'post') {
							// init data
							$return[$key]['child'][$keychild]['data'] = [];
							// loop header
							foreach ($header as $bl => $v) {
								// loop coa
								$total = 0;
								foreach ($valchild['coa'] as $coa) {
									// check coa value in array
									if (isset($tmp[$bl][$coa])) {
										$total += (float) $tmp[$bl][$coa]->jumlah;
									}
								}

								if (isset($return[$key]['child'][$keychild]['value'])) {
									if ($return[$key]['child'][$keychild]['value'] == 'negative') {
										$total = -abs($total);
									} else {
										$total = abs($total);
									}
								}
								$return[$key]['child'][$keychild]['data'][$bl] = $total;
							}

						} else if ($valchild['type'] == 'ref') {
							// init data
							$return[$key]['child'][$keychild]['data'] = [];
							
							// loop header
							foreach ($header as $bl => $v) {
								// set value by ref
								$tmp_total = 0;
								if (isset($source[$valchild['source']]['content'][$valchild['exp']]['data'][$bl])) {
									$tmp_total = $source[$valchild['source']]['content'][$valchild['exp']]['data'][$bl];
								}

								$return[$key]['child'][$keychild]['data'][$bl] = $tmp_total;
							}
						}
					}
				}
			}

			// post
			else if ($val['type'] == 'post') {
				// init data
				$return[$key]['data'] = [];
				// loop header
				foreach ($header as $bl => $v) {
					// loop coa
					$total = 0;
					foreach ($val['coa'] as $coa) {
						// check coa value in array
						if (isset($tmp[$bl][$coa])) {
							$total += (float) $tmp[$bl][$coa]->jumlah;
						}
					}

					if (isset($return[$key]['value'])) {
						if ($return[$key]['value'] == 'negative') {
							$total = -abs($total);
						} else {
							$total = abs($total);
						}
					}
					$return[$key]['data'][$bl] = $total;
				}
			}

			// increase
			else if ($val['type'] == 'inc') {
				// init data
				$return[$key]['data'] = [];
				// loop header
				foreach ($header as $bl => $v) {
					// loop coa
					$total = 0;
					foreach ($val['exp'] as $exp) {
						// check exp value in array return
						$tmp_total = 0;
						if (isset($return[$exp])) {
							if ($return[$exp]['type'] == 'label' && isset($return[$exp]['child'])) { // jika punya child
								foreach ($return[$exp]['child'] as $keychild => $valchild) {
									$tmp_total += $return[$exp]['child'][$keychild]['data'][$bl];
								}

							} else if ($return[$exp]['type'] == 'post') { // post
								$tmp_total = $return[$exp]['data'][$bl];

							} else if ($return[$exp]['type'] == 'inc' || $return[$exp]['type'] == 'dec') { // dec/inc
								$tmp_total = $return[$exp]['data'][$bl];
							}
						}
						if ($total == 0) {
							$total = $tmp_total;
						} else {
							$total += $tmp_total;
						}
					}

					$return[$key]['data'][$bl] = $total;
				}
			}

			// decrease
			else if ($val['type'] == 'dec') {
				// init data
				$return[$key]['data'] = [];
				// loop header
				foreach ($header as $bl => $v) {
					// loop coa
					$total = 0;
					foreach ($val['exp'] as $exp) {
						// check exp value in array return
						$tmp_total = 0;
						if (isset($return[$exp])) {
							if ($return[$exp]['type'] == 'label' && isset($return[$exp]['child'])) { // jika punya child
								foreach ($return[$exp]['child'] as $keychild => $valchild) {
									$tmp_total += $return[$exp]['child'][$keychild]['data'][$bl];
								}

							} else if ($return[$exp]['type'] == 'post') { // post
								$tmp_total = $return[$exp]['data'][$bl];
							} else if ($return[$exp]['type'] == 'inc' || $return[$exp]['type'] == 'dec') { // dec/inc
								$tmp_total = $return[$exp]['data'][$bl];
							}
						}
						if ($total == 0) {
							$total = $tmp_total;
						} else {
							$total -= $tmp_total;
						}
					}

					$return[$key]['data'][$bl] = $total;
				}
			}

			// by ref
			else if ($val['type'] == 'ref') {
				// init data
				$return[$key]['data'] = [];

				// loop header
				foreach ($header as $bl => $v) {
					// set value by ref
					$tmp_total = 0;
					if (isset($source[$val['source']]['content'][$val['exp']]['data'][$bl])) {
						$tmp_total = $source[$val['source']]['content'][$val['exp']]['data'][$bl];
					}
					
					$return[$key]['data'][$bl] = $tmp_total;
				}
			}
		}

		return [
			'header' => $header,
			'content' => $return,
		];
	}

}
