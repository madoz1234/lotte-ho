<?php

namespace Lotte\Http\Controllers\Option;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

use Lotte\Models\Master\Kota;
use Lotte\Models\Master\Kecamatan;
use Lotte\Models\Master\Kelurahan;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\Master\ProdukTmuk;

class AjaxOptionController extends Controller
{
	public function getKota(Request $request)
    {
        $postData = $request->id_provinsi;
        echo Kota::options('nama', 'id', [
    			'filters' => ['provinsi_id' => $postData],
    		], '-- pilih --');
    }   

    public function getKecamatan(Request $request)
    {
        $postData = $request->id_kota;
        echo Kecamatan::options('nama', 'id', [
                'filters' => ['kota_id' => $postData],
            ], '-- pilih --');
    }   

    public function getKelurahan(Request $request)
    {
        $postData = $request->id_kecamatan;
        echo Kelurahan::options('nama', 'id', [
                'filters' => ['kecamatan_id' => $postData],
            ], '-- pilih --');
    }

    public function getReg(Request $request)
    {
        echo Region::options('area', 'id', [], '-- All Region --');
    }

    public function getRegion(Request $request)
    {
        $postData = $request->id_region;
        echo Lsi::options('nama', 'id', [
                'filters' => ['region_id' => $postData],
            ], '-- All LSI --');
    }

    public function getRegion2(Request $request)
    {
        $postData = $request->id_region;
        $data = Lsi::where('region_id',$postData)->get();
        // $string ='<option value="0">-- Pilih LSI --</option>';
        $string ='';
        foreach ($data as $value) {
            $string .='<option value="'.$value->id.'">'.$value->kode.' - '.$value->nama.'</option>';
        }
        echo $string;
    }

    public function getLsi(Request $request)
    {
        $postData = $request->id_lsi;
        $lsi = Lsi::find($postData);
        echo ProdukLsi::options('produk_kode', 'id', [
                'filters' => ['lsi_kode' => $lsi->kode, function($query){ $query->whereIn('status', ['NORMAL','ORDER STOP']); }],
            ], '-- All Kode--');
    }

    public function getLsiTmuk(Request $request)
    {
        $postData = $request->id_lsi;
        $lsi = Lsi::find($postData);
        echo Tmuk::options(function($query){ return $query->kode.' - '.$query->nama; }, 'id', [
                'filters' => ['lsi_id' => $lsi->id],
            ], '-- All TMUK--');
    }

    public function getLsiTmuk2(Request $request)
    {
        $postData = $request->id_lsi;
        $lsi = Lsi::find($postData);
        $data = Tmuk::where('lsi_id',$lsi->id)->get();
        $string ='';
        foreach ($data as $value) {
            $string .='<option value="'.$value->id.'">'.$value->kode.' - '.$value->nama.'</option>';
        }
        echo $string;
    }

    public function getLsiTmukCode(Request $request)
    {
        $postData = $request->id_lsi;
        $lsi = Lsi::find($postData);
        echo Tmuk::options('nama', 'kode', [
                'filters' => ['lsi_id' => $lsi->id],
            ], '-- All TMUK--');
    }
    
    public function getTmukLsi(Request $request)
    {
        $postData = $request->id_tmuk;

        $tmuk = Tmuk::find($postData);
        $lsi_id = $tmuk->lsi_id;

        echo 
        VendorLokalTmuk::options('nama', 'id',[], 'Vendor'),
        Lsi::options('nama', 'kode', ['filters' => ['id' => $lsi_id],], '-- pilih --');
    }

    public function getProdukByTmuk(Request $request)
    {
        $postData = $request->id_tmuk;

        $tmuk = Tmuk::find($postData);

        echo ProdukTmuk::options(function($query){ return $query->produk->kode.' - '.$query->produk->nama; }, 'produk_kode', [
                'filters' => ['tmuk_kode' => $tmuk->kode],
            ], '-- Pilih Produk --');
    }

    public function getVendor(Request $request)
    {
        $postData = $request->id_tmuk;
        $tmuk = Tmuk::with('detailVendor')->where('kode', $postData)->first();

        echo VendorLokalTmuk::options('nama', 'id', [
                'filters' => [function($query) use ($tmuk){ $query->whereHas('detailVendor', function ($query) use ($tmuk){
                                    $query->where('tmuk_id', $tmuk->id);
                                }); }],
            ], 'Vendor Lokal');
    }
}