<?php

namespace Lotte\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $breadcrumb = ["Home" => "#"];
    private $link = "";
    private $title = "Title";
    private $subtitle = "Subtitle";
    private $tableStruct = [];
    private $tableStructOutstandingProdukSync = [];
    private $tableStructMonitoringOnline = [];
    private $tableStructOutstandingPr = [];
    private $tableStructOutstandingPicking = [];
    private $tableStructOutstandingDelivery = [];
    private $tableStructFinanceSetoran = [];
    private $tableStructPenjualanKategori = [];
    private $tableStructDeviasiPenjualan= [];
    private $tableStructLastSoHarian= [];
    private $tableStructLastSoBulanan= [];
    private $tableStructStatistikPo= [];
    private $tableStructSetoranPenjualan= [];
    private $modalSize = "mini";

    public function setBreadcrumb($value=[])
    {
        $this->breadcrumb = $value;
    }

    public function pushBreadCrumb($value=[])
    {
        $this->breadcrumb = array_merge($this->breadcrumb, $value);
    }

    public function getBreadcrumb()
    {
        return $this->breadcrumb;
    }

    public function setTableStruct($value=[])
    {
    	$this->tableStruct = $value;
    }

    public function setTableStructOutstandingProdukSync($value=[])
    {
        $this->tableStructOutstandingProdukSync = $value;
    }

    public function setTableStructMonitoringOnline($value=[])
    {
        $this->tableStructMonitoringOnline = $value;
    }

    public function setTableStructOutstandingPr($value=[])
    {
        $this->tableStructOutstandingPr = $value;
    }

    public function setTableStructOutstandingPicking($value=[])
    {
        $this->tableStructOutstandingPicking = $value;
    }

    public function setTableStructOutstandingDelivery($value=[])
    {
        $this->tableStructOutstandingDelivery = $value;
    }

    public function setTableStructFinanceSetoran($value=[])
    {
        $this->tableStructFinanceSetoran = $value;
    }

    public function setTableStructPenjualanKategori($value=[])
    {
        $this->tableStructPenjualanKategori = $value;
    }

    public function setTableStructDeviasiPenjualan($value=[])
    {
        $this->tableStructDeviasiPenjualan = $value;
    }

    public function setTableStructLastSoHarian($value=[])
    {
        $this->tableStructLastSoHarian = $value;
    }

    public function setTableStructLastSoBulanan($value=[])
    {
        $this->tableStructLastSoBulanan = $value;
    }

    public function setTableStructStatistikPo($value=[])
    {
        $this->tableStructStatistikPo = $value;
    }

    public function setTableStructSetoranPenjualan($value=[])
    {
        $this->tableStructSetoranPenjualan = $value;
    }

    public function getTableStruct()
    {
    	return $this->tableStruct;
    }

    public function setTitle($value="")
    {
    	$this->title = $value;
    }

    public function getTitle()
    {
    	return $this->title;
    }

    public function setSubtitle($value="")
    {
    	$this->subtitle = $value;
    }

    public function getSubtitle()
    {
    	return $this->subtitle;
    }

    public function setLink($value="")
    {
        $this->link = $value;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setModalSize($value="mini")
    {
    	$this->modalSize = $value;
    }

    public function getModalSize()
    {
    	return $this->modalSize;
    }

    public function render($view, $additional=[])
    {
    	$data = [
    		'breadcrumb' => $this->breadcrumb,
    		'title'		 => $this->title,
            'subtitle'   => $this->subtitle,
            'pageUrl'    => $this->link,
            'modalSize'  => $this->modalSize,
            'tableStruct'=> $this->tableStruct,
            'tableStructOutstandingProdukSync'=> $this->tableStructOutstandingProdukSync,
            'tableStructMonitoringOnline'=> $this->tableStructMonitoringOnline,
            'tableStructOutstandingPr'=> $this->tableStructOutstandingPr,
            'tableStructOutstandingPicking'=> $this->tableStructOutstandingPicking,
            'tableStructOutstandingDelivery'=> $this->tableStructOutstandingDelivery,
    		'tableStructFinanceSetoran'=> $this->tableStructFinanceSetoran,
            'tableStructPenjualanKategori'=> $this->tableStructPenjualanKategori,
            'tableStructDeviasiPenjualan'=> $this->tableStructDeviasiPenjualan,
            'tableStructLastSoHarian'=> $this->tableStructLastSoHarian,
            'tableStructLastSoBulanan'=> $this->tableStructLastSoBulanan,
            'tableStructStatistikPo'=> $this->tableStructStatistikPo,
            'tableStructSetoranPenjualan'=> $this->tableStructSetoranPenjualan,
            'mockup'     => true
    	];

		return view($view, array_merge($data, $additional));
    }


    public function makeButton($params = [])
    {
        $settings = [
            'class'    => 'blue',
            'label'    => 'Button',
            'tooltip'  => '',
            'target'   => url('/'),
            'disabled' => '',
        ];

        $btn = '';
        $datas = '';
        $attrs = '';

        if (isset($params['datas'])) {
            foreach ($params['datas'] as $k => $v) {
                $datas .= " data-{$k}=\"{$v}\"";
            }
        }

        if (isset($params['attributes'])) {
            foreach ($params['attributes'] as $k => $v) {
                $attrs .= " {$k}=\"{$v}\"";
            }
        }

        switch ($params['type']) {
            case "print":
                $settings['class']   = 'icon print';
                $settings['label']   = '<i class="print icon"></i>';
                $settings['tooltip'] = 'Cetak Data';
                
                $params  = array_merge($settings, $params);
                $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                break;
            case "verify":
                $settings['class']   = 'green icon verify';
                $settings['label']   = '<i class="checkmark icon"></i>';
                $settings['tooltip'] = 'Proses';
                
                $params  = array_merge($settings, $params);
                $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                break;
            case "tolak":
                $settings['class']   = 'red icon tolak';
                $settings['label']   = '<i class="minus circle icon"></i>';
                $settings['tooltip'] = 'Tolak';
                
                $params  = array_merge($settings, $params);
                $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                break;
            case "delete":
                $settings['class']   = 'red icon delete';
                $settings['label']   = '<i class="trash icon"></i>';
                $settings['tooltip'] = 'Hapus Data';
                
                $params  = array_merge($settings, $params);
                $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                break;
            case "edit":
                $settings['class']   = 'orange icon edit';
                $settings['label']   = '<i class="edit icon"></i>';
                $settings['tooltip'] = 'Ubah Data';
                
                $params  = array_merge($settings, $params);
                $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                break;
            case "detail":
                $settings['class']   = 'blue icon detail';
                $settings['label']   = '<i class="eye icon"></i>';
                $settings['tooltip'] = 'Detail Data';
                
                $params  = array_merge($settings, $params);
                $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                break;
            case "other":
                $settings['class']   = 'orange icon';
                $settings['label']   = '<i class="edit icon"></i>';
                $settings['tooltip'] = 'Ubah Data';
                $settings['event']   = '';
                $params              = array_merge($settings, $params);
                $extends             = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$params['event']}  class='ui mini orange icon button' {$params['disabled']}>{$params['label']}</button>\n";
                break;
            case "url":
                $settings['class']   = 'orange icon';
                $settings['label']   = '<i class="edit icon"></i>';
                $settings['tooltip'] = 'Ubah Data';

                $params              = array_merge($settings, $params);
                $extends             = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<a href='{$params['url']}' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</a>\n";
                break;

            default:
                $settings['class']   = 'blue icon';
                $settings['label']   = '<i class="eye icon"></i>';
                
                $params  = array_merge($settings, $params);
                $extends = '';
                if($params['tooltip'] != '')
                {
                    $extends = " data-content='{$params['tooltip']}'";
                }

                $btn = "<a href='{$params['target']}' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</a>\n";
        }

        return $btn;
    }
}
