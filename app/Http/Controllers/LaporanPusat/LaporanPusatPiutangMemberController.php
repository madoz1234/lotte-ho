<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransMemberPiutang;
use Lotte\Models\Trans\TransMemberPiutangDetail;
use Lotte\Models\Master\MemberCard;
use Lotte\Libraries\JurnalLib;
use Illuminate\Support\Collection;

use Datatables;
use Carbon\Carbon;


class LaporanPusatPiutangMemberController extends Controller
{
    protected $link = 'laporan-pusat/';
    public $tpl_labarugi;
    public $tpl_neraca;
    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Keuangan");   
    }

  public function postGridTemp(Request $request){
    switch ($request->types) {
      case 'pembayaran-member':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-piutang-member.'.$request->types, $data);
      break;
      case 'daftar-member':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-piutang-member.'.$request->types, $data);
      break;
      case 'umur-piutang-member':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-piutang-member.'.$request->types, $data);
      break;
      default:
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-piutang-member.'.$request->types, $data);
      break;
    }
  }

    public function postGridAll(Request $request){
    // dd($request->all());
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }
    $result= $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }
    $request['tmuk_kode']= $temp;
    $record = [];



    switch ($request->types) {
      case 'pembayaran-member':
        $records = TransMemberPiutang::with('detail')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('created_at', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $records = $records->get();
        $rec = New Collection;
        foreach ($records as $row){
            $rec->push([
                'tanggals' => $row->created_at->format('Y-m-d'),
                'no_member' => $row->no_member,
                'nama_member' => $row->nama_member,
                'jumlah_pembayaran' => '0',
                'sisa_piutang' => '0',
            ]);
        }
        return Datatables::of($rec)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->make(true);
      break;
      case 'daftar-member':
        $records = MemberCard::select('*');
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('created_at', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $records = $records->get();

        $rec = New Collection;
        foreach ($records as $row){
            $rec->push([
                'no_member'       => $row->nomor,
                'nama_member'     => $row->nama,
                'alamat'          => $row->alamat,
                'telepon'         => $row->telepon,
                'email'           => $row->email,
                'limit_kredit'    => $row->limit_kredit==null ? '-': rupiah($row->limit_kredit),
                'total_order'     => '0',
                'total_pembelian' => '0',
                'last_visit'      => '0',
                'ave_pembelian'   => '0'
            ]);
        }
        // dd($rec->toArray());
        return Datatables::of($rec)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->make(true);
      break;
      case 'umur-piutang-member':

        $records = TransJurnal::select(\DB::raw('tmuk_kode, tanggal, idtrans, sum(jumlah) as jml'))->groupBy('tmuk_kode', 'tanggal', 'idtrans')->whereIn('coa_kode', ['1.1.3.1'])
        ->groupBy('tanggal')->orderBy('tanggal','desc')
        ->whereIn('tmuk_kode', $temp)
        ->where('delete', 0);
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $record = $records->get();

        // dd($records);

        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })

        ->addColumn('umur', function ($record) {
            return '0' ;
        })
        ->addColumn('jml', function ($record) {
            return rupiah($record->jml) ;
        })
        ->addColumn('30_hari', function ($record) {
            return '0';
        })
        ->addColumn('60_hari', function ($record) {
            return '0';
        })
        ->addColumn('90_hari', function ($record) {
            return '0';
        })
        ->addColumn('lebih_90_hari', function ($record) {
            return '0';
        })
        ->make(true);
      break;
      default:
        $records = TransMemberPiutang::with('detail')->select('*');
        $records->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          // $records->whereBetween('created_at', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
          $records->whereBetween(\DB::raw('created_at'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $records->get();
        $temp =[];
        $rec = New Collection;

        foreach ($record as $row) {
            foreach ($row->detail as $val) {
                $rec->push([
                'tanggal_transaksi' => $val->tanggal,
                'piutang_member' => rupiah($val->hutang),
                'nama_member' => $val->piutang->no_member.' - '.$val->piutang->nama_member,
                ]);        
            }    
        }
        return Datatables::of($rec)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->make(true);
      break;
    }
  }

  //PRINT PDF
  public function postPrint(Request $request){

    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }

    $request['tmukd'] = clone  $reg;

    $result= $reg->get();
    $temp=[];
    $temps=[];

    foreach ($result as $val){
      $temp[] = ['kode'=>$val->kode, 'nama'=>$val->nama];
      $temps[] = $val->kode;
    }
    $request['tmuk_kode']=$temps;

    switch ($request->types) {
      case 'pembayaran-member':
        $records = TransMemberPiutang::with('detail')
        ->whereIn('tmuk_kode', $temps);
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('created_at', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $records = $records->get();
        $rec = New Collection;
        foreach ($records as $row){
            $rec->push([
                'tanggals' => $row->created_at->format('Y-m-d'),
                'no_member' => $row->no_member,
                'nama_member' => $row->nama_member,
                'jumlah_pembayaran' => '0',
                'sisa_piutang' => '0',
            ]);
        }
        // $this->printPdf($request, $rec);
        return $this->printPdf($request, $rec);
      break;
      case 'umur-piutang-member':


        $records = TransJurnal::select(\DB::raw('tmuk_kode, tanggal, idtrans, sum(jumlah) as jml'))->groupBy('tmuk_kode', 'tanggal', 'idtrans')->whereIn('coa_kode', ['1.1.3.1'])
        ->groupBy('tanggal')->orderBy('tanggal','desc')
        ->where('delete', 0)
        ->whereIn('tmuk_kode', $temps);
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }

        $records = $records->get();

        // dd($records);


        $rec = New Collection;
        foreach ($records as $row){
            $rec->push([
                'id_trans' => $row->idtrans,
                'tanggals' => $row->tanggal,
                'jumlah' => $row->jml,
            ]);
        }
        return $this->printPdf($request, $rec);
        // $this->printPdf($request, $rec);
      break;
      case 'daftar-member':
        $records = MemberCard::select('*');
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('created_at', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $records = $records->get();

        $rec = New Collection;
        foreach ($records as $row){
            $rec->push([
                'no_member'       => $row->nomor,
                'nama_member'     => $row->nama,
                'alamat'          => $row->alamat,
                'telepon'         => $row->telepon,
                'email'           => $row->email,
                'limit_kredit'    => $row->limit_kredit==null ? '-': rupiah($row->limit_kredit),
                'total_order'     => '0',
                'total_pembelian' => '0',
                'last_visit'      => '0',
                'ave_pembelian'   => '0'
            ]);
        }
        return $this->printPdf($request, $rec);
        // $this->printPdf($request, $rec);
      break;
      default:
        $records = TransMemberPiutang::with('detail')->select('*');
        $records->whereIn('tmuk_kode', $temps);
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('created_at', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $record = $records->get();
        $temp =[];
        $rec = New Collection;

        foreach ($record as $row) {
            foreach ($row->detail as $val) {
                $rec->push([
                'tanggal_transaksi' => $val->tanggal,
                'piutang_member' => rupiah($val->hutang),
                'nama_member' => $val->piutang->no_member.' - '.$val->piutang->nama_member,
                ]);        
            }    
        }
        return $this->printPdf($request, $rec);
        // $this->printPdf($request, $rec);
      break;
    }
  }

  // GENERATE LAPORAN pdf
  public function printPdf($request, $record=''){
    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $request['tmukd']->first()->lsi->region->area;
    }
    if($request->lsi_id){
      $lsi          = $request['tmukd']->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $request['tmukd']->first()->kode.'-'.$request['tmukd']->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];
    // dd($record);
    // $content = view('report.laporan-pusat.akun-piutang-member.'.$request->types,$data);
    
    // HTML2PDF($content, $data);

    $pdf = \PDF::loadView('report.laporan-pusat.akun-piutang-member.'.$request->types, $data);
    return $pdf->setOption('margin-bottom', 5)
    ->setOption('margin-top', 5)
    ->setOption('margin-left', 3)
    ->setOption('margin-right', 3)
    ->inline();
  }
  //PRINT EXCEL
  public function postPrintExcel(Request $request){
    if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }
    $request['temp'] = $temp;

    switch ($request->types) {      
      case 'pembayaran-member':
        $records = TransMemberPiutang::with('detail')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $records = $records->get();
        $rec = New Collection;
        $temp = [];
            foreach ($records as $row){
                $temp[] = [
                    $row->created_at->format('Y-m-d'),
                    $row->no_member,
                    $row->nama_member,
                    '0',
                    '0',
                ];
            }
            $data = [
              'tittle'     => 'Rekap Pembayaran Member',
              'tittle_set' => 'Rekap Pembayaran Member',
              'header_set' => array(
                'Tanggal',
                'No Member',
                'Nama Member',
                'Jumlah Pembayaran (Rp)',
                'Sisa Piutang (Rp)'
              ),
              'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20),
              'creator'    => 'Yusup Supriatna',
              'record'     => $temp,
            ];
            $this->printExcel($request, $data);
        break;
        case 'daftar-member':
            $records = MemberCard::select('*');
            if($request->tanggal_start && $request->tanggal_end){
              $records->whereBetween('created_at', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
            }
            $records = $records->get();

            $rec = New Collection;
            $temp = [];
            foreach ($records as $row){
                $temp[] = [
                    $row->nomor,
                    $row->nama,
                    $row->alamat,
                    $row->telepon,
                    $row->email,
                    $row->limit_kredit==null ? '-': rupiah($row->limit_kredit),
                    '0',
                    '0',
                    '0',
                    '0'
                ];
            }
            $data = [
              'tittle'     => 'Rekap Pembayaran Member',
              'tittle_set' => 'Rekap Pembayaran Member',
              'header_set' => array(
                'No Member',
                'Nama Member',
                'Alamat',
                'Telepon',
                'Email',
                'Limit Kredit',
                'Total Order',
                'Total Pembelian',
                'Last Visit',
                'Ave Pembelian'
              ),

              'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20),
              'creator'    => 'Yusup Supriatna',
              'record'     => $temp,
            ];
            $this->printExcel($request, $data);
        break;
        default:
            $records = TransMemberPiutang::with('detail')->select('*');
            $records->whereIn('tmuk_kode', $temp);
            if($request->tanggal_start && $request->tanggal_end){
              $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
            }
            $record = $records->get();
            $temp =[];
            $rec = New Collection;

            foreach ($record as $row) {
                foreach ($row->detail as $val) {
                    $temp[] = [
                        $val->tanggal,
                        $val->hutang,
                        $val->piutang->no_member.' - '.$val->piutang->nama_member,
                    ];        
                }    
            }
            $data = [
              'tittle'     => 'Rekap Piutang Member',
              'tittle_set' => 'Rekap Piutang Member',
              'header_set' => array(            
                'Kode Produk',
                'Nama Produk',
                'UOM'),
              'set_width'  => array('A'=>20,'B'=>20,'C'=>20),
              'creator'    => 'Yusup Supriatna',
              'record'     => $temp,
            ];
            $this->printExcel($request, $data);
        break;
    }
  }
  // //GENERATE LAPORAN EXCEL
  public function printExcel($request='', $data=[]){
    
    $Export = Excel::create(isset($data['tittle'])?$data['tittle']:'default', function($excel) use ($request, $data){
    $excel->setTitle(isset($data['tittle'])?$data['tittle']:'default');
        $excel->setCreator(isset($data['creator'])?$data['creator']:'Aplikasi Akuntansi')->setCompany('Lotte');
        $excel->setDescription('Export');

        $excel->sheet(isset($data['tittle'])?$data['tittle']:'default', function($sheet) use ($request, $data){
        $tray = array_keys($data['set_width']);
        $last_cell = end($tray);
        $sheet->row(1, array(
            isset($data['tittle_set'])?$data['tittle_set']:'default'
        ));

        $sheet->row(2, array(
          'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
        ));
        $sheet->row(3, isset($data['header_set'])?$data['header_set']:[]);

        $row = 4;
        $no =1;
        foreach ($data['record'] as $elm) {
            $sheet->row($row, $elm);
          $row++;
        }
        $sheet->cells('E4'.':'.$last_cell.''.$row, function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
        });
        $sheet->setBorder('A3:'.$last_cell.''.$row, 'thin');
        $sheet->mergeCells('A1:'.$last_cell.'1');
        $sheet->mergeCells('A2:'.$last_cell.'2');
        $sheet->cells('A1:A1', function($cells){
          $cells->setFontSize(14);
          $cells->setFontWeight('bold');
          $cells->setAlignment('center');
        });
        $sheet->cells('A2:'.$last_cell.'2', function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
        });
        $sheet->cells('A3:'.$last_cell.'3', function($cells){
          $cells->setAlignment('center');
          $cells->setBackground('#999999');
          $cells->setFontColor('#FFFFFF');
        });
        $sheet->setWidth(isset($data['set_width'])?$data['set_width']:[]);
        $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->setWrapText(true);
        $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top')
        );
        $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'center')
        );

        });
    });
    $Export ->download('xls');
  }

}


