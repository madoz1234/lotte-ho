<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransJurnal;

use Datatables;
use Carbon\Carbon;

class LaporanArusKasBankController extends Controller
{
    protected $link = 'laporan-pusat/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Pusat");   
    }

  public function postGridTemp(Request $request){
    $this->setLink($this->link.'arus-kas-bank');
    switch ($request->types) {
      case 'arus-kas-bank':
        $data = [
          'lists_grid' => 'arus-kas-bank',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.arus-kas-bank.arus-kas-bank', $data);
      break;
      case 'arus-kas-per-akun':
        $data = [
          'lists_grid' => 'arus-kas-per-akun',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.arus-kas-bank.arus-kas-per-akun', $data);
      break;
      case 'mutasi-escrow':
        $data = [
          'lists_grid' => 'mutasi-escrow',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.arus-kas-bank.mutasi-escrow', $data);
      break;
    }
  }

  public function postGridAll(Request $request){
    if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }
    $result= $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    switch ($request->types) {
      case 'arus-kas-bank':

        $record = TransJurnal::select('*')
                                    ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
                                    ->where('delete', 0)
                                    ->orderBy('tanggal')

        ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        }

        // dd($record);

        return Datatables::of($record)
          ->addColumn('num', function ($record) use ($request) {
              return $request->get('start');
          })
          ->addColumn('coa_kode', function ($record) {
              return $record->coa->nama;
          })
          ->addColumn('awal', function ($record) {
            
            if($record->count() != 0){
              $saldo_awal = [];
              $saldo_akhir= [];
              $nama       = [];
              $i          = 1; 
              $saldo_awal[$i] = 0;


                $nama[$i] = $record->coa->nama;
                            if(isset($nama[$i - 1]) && $nama[$i - 1] == $nama[$i])
                            {
                                if(isset($saldo_akhir[$i - 1]))
                                {
                                    $saldo_awal[$i] = $saldo_akhir[$i - 1];
                                }
                            }



            }   
            return rupiah($saldo_awal[$i]) ;
          })
          ->addColumn('debit', function ($record) {
              return $record->posisi == 'D' ? rupiah($record->jumlah) : '0';
          })
          ->addColumn('kredit', function ($record) {
              return $record->posisi == 'K' ? rupiah($record->jumlah) : '0';
          })
          ->addColumn('bersih', function ($record) {

              $posisi_normal = $record->coa->posisi;

              if ($posisi_normal == 'D') {
                  $bersih = ($record->posisi == 'D' ? $record->jumlah : '0') - ($record->posisi == 'K' ? $record->jumlah : '0') ;
              }else{
                  $bersih = ($record->posisi == 'K' ? $record->jumlah : '0') - ($record->posisi == 'D' ? $record->jumlah : '0');                                        
              }


              return rupiah($bersih);
          })
          ->addColumn('akhir', function ($record) {
              return '0' ;
          })

          ->make(true);
      break;
      case 'arus-kas-per-akun':

        $record = TransJurnal::from(\DB::raw("(SELECT
                                    trans_jurnal.tanggal,
                                    trans_jurnal.tmuk_kode,
                                    trans_jurnal.delete,
                                    trans_jurnal.coa_kode,
                                    SUM(trans_jurnal.jumlah)AS jml
                                    FROM
                                    trans_jurnal
                                    GROUP BY trans_jurnal.coa_kode, trans_jurnal.tanggal, trans_jurnal.tmuk_kode, trans_jurnal.delete ) d"))
                                    ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
                                    ->where('delete', 0)
                                    ->orderBy('tanggal')
                                    ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        }
        // dd($record);

        return Datatables::of($record)
          ->addColumn('num', function ($record) use ($request) {
              return $request->get('start');
          })
          ->addColumn('nama_akun', function ($record) {
              return $record->coa->nama;
          })
          ->addColumn('jumlah', function ($record) {
              return rupiah($record->jml) ;
          })

          ->make(true);
      break;
      case 'mutasi-escrow':
        $record = TransJurnal::select('*')
                                    ->whereIn('coa_kode', ['1.1.2.1'])
                                    ->where('delete', 0)
                                    ->orderBy('tanggal')

        ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        }

        return Datatables::of($record)
          ->addColumn('num', function ($record) use ($request) {
              return $request->get('start');
          })
          ->addColumn('nama_akun', function ($record) {
              return $record->coa->nama;
          })
          ->addColumn('debit', function ($record) {
              return $record->posisi == 'D' ? rupiah($record->jumlah) : '0';
          })
          ->addColumn('kredit', function ($record) {
              return $record->posisi == 'K' ? rupiah($record->jumlah) : '0';
          })

          ->make(true);
      break;

    }
  }

  //PRINT PDF
  public function postPrint(Request $request){
   if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // dd($request->all());
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $request['tmukd']->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    $request['temp'] = $temp;
    switch ($request->types) {
      case 'arus-kas-bank':
        $rangkuman = TransJurnal::select('*')
        ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
        ->where('delete', 0)
        ->orderBy('tanggal');

        $rangkuman->whereIn('tmuk_kode',  $request->temp);
        if($request->tanggal_start and $request->tanggal_end){
        $rangkuman->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        }

        $record = $rangkuman->get();

        return $this->printPdf($request, $record);
      break;
      case 'arus-kas-per-akun':
        $rangkuman = TransJurnal::from(\DB::raw("(SELECT
              trans_jurnal.tanggal,
              trans_jurnal.tmuk_kode,
              trans_jurnal.delete,
              trans_jurnal.coa_kode,
              SUM(trans_jurnal.jumlah)AS jml
              FROM
              trans_jurnal
              GROUP BY trans_jurnal.coa_kode, trans_jurnal.tanggal, trans_jurnal.tmuk_kode, trans_jurnal.delete ) d"))
              ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
              ->where('delete', 0)
              ->orderBy('tanggal')
              ->whereIn('tmuk_kode',  $request->temp);
              if($request->tanggal_start and $request->tanggal_end){
                $rangkuman->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
              }
            $record = $rangkuman->get();

        return $this->printPdf($request, $record);
      break;
      case 'mutasi-escrow':
        $saldo = 0;
        $before = TransJurnal::select('*')
                                ->where('delete', 0)
                                ->whereIn('coa_kode',['1.1.2.1']);
                                // ->orderBy('tanggal','asc');

        $before->whereIn('tmuk_kode',  $request->temp);
        if($request->tanggal_start and $request->tanggal_end){
          // $before->whereBetween(\DB::raw('DATE(tanggal)'),'<', array($request->tanggal_start, $request->tanggal_end));
          $before->where(\DB::raw('DATE(tanggal)'),'<',$request->tanggal_start);
        }

        $before_get = $before->get();
        if($before_get->count()!=0){
            foreach ($before_get as $row) {
                if($row->posisi=='D'){
                    $saldo=$saldo-$row->jumlah;
                }else{
                    $saldo+=$row->jumlah;
                }
            }
        }

        $request['ledger'] = $saldo;

        $rangkuman = TransJurnal::select('*')
                                ->whereIn('coa_kode',['1.1.2.1'])
                                ->where('delete', 0)
                                ->orderBy('tanggal','DESC');
        $rangkuman->whereIn('tmuk_kode',  $request->temp);
        if($request->tanggal_start and $request->tanggal_end){
          $rangkuman->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        }


        $record = $rangkuman->get();
        return $this->printPdf($request, $record);
      break;
    }
  }

  // GENERATE LAPORAN pdf
  public function printPdf($request, $record=''){
    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $request['tmukd']->first()->lsi->region->area;
    }
    if($request->lsi_id){
      $lsi          = $request['tmukd']->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $request['tmukd']->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'saldo'   => isset($request['ledger'])?$request['ledger']:[],
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];
    
    $pdf = \PDF::loadView('report.laporan-pusat.akun-kas-bank.'.$request->types, $data);
    return $pdf->setOption('margin-bottom', 5)
    ->setOption('margin-top', 5)
    ->setOption('margin-left', 3)
    ->setOption('margin-right', 3)
    ->inline();
  }


 
  //PRINT EXCEL
  public function postPrintExcel(Request $request){
    // dd($request->all());
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }
    $request['temp'] = $temp;
    switch ($request->types) {
      case 'arus-kas-bank':
              $record = TransJurnal::select('*')
          ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
          ->where('delete', 0)
          ->orderBy('tanggal')->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->get();

        $temp = [];
        $i =1;
        foreach ($record as $row) {
          $posisi_normal = $row->coa->posisi;

          if ($posisi_normal == 'D') {
            $bersih = ($row->posisi == 'D' ? $row->jumlah : '0') - ($row->posisi == 'K' ? $row->jumlah : '0') ;
          }else{
            $bersih = ($row->posisi == 'K' ? $row->jumlah : '0') - ($row->posisi == 'D' ? $row->jumlah : '0');
          }
          $temp[] = [
            $i,
            $row->tanggal,
            $row->coa->nama,
            $row->posisi == 'D' ? rupiah($row->jumlah) : '0',
            $row->posisi == 'K' ? rupiah($row->jumlah) : '0',
            $bersih,
            $row->deskripsi,
          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Jurnal Kas & Bank',
          'tittle_set' => 'Jurnal Kas & Bank',
          'header_set' => array(
            '#',
            'Tanggal',
            'Nama Akun',
            'Perubahan Debit',
            'Perubahan Kredit',
            'Perubahan Bersih',
            'Keterangan'
          ),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'arus-kas-per-akun':
        $record = TransJurnal::from(\DB::raw("(SELECT
          trans_jurnal.tanggal,
          trans_jurnal.tmuk_kode,
          trans_jurnal.delete,
          trans_jurnal.coa_kode,
          SUM(trans_jurnal.jumlah)AS jml
          FROM
          trans_jurnal
          GROUP BY trans_jurnal.coa_kode, trans_jurnal.tanggal, trans_jurnal.tmuk_kode, trans_jurnal.delete ) d"))
          ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
          ->where('delete', 0)
          ->orderBy('tanggal')
          ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->get();
        $temp = [];
        $i=1;
        foreach ($record as $row) {
          $temp[] = [
              $i,
              $row->tanggal,
              $row->coa->nama,
              $row->jml,

          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Arus Kas per Akun',
          'tittle_set' => 'Arus Kas per Akun',
          'header_set' => array(
            '#',
            'Tanggal',
            'Nama Akun',
            'Jumlah'
          ),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      default:
        $record = TransJurnal::select('*')
                                    ->whereIn('coa_kode', ['1.1.2.1'])
                                    ->where('delete', 0)
                                    ->orderBy('tanggal')

        ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->get();
        $temp = [];
        $i = 1;
        foreach ($record as $row) {
          $temp[] = [
            $i,
            $row->tanggal,
            $row->deskripsi,
            $row->posisi == 'D' ? rupiah($row->jumlah) : '0',
            $row->posisi == 'K' ? rupiah($row->jumlah) : '0'
          ];
          $i++;
        }

        $data = [
          'tittle'     => 'Jurnal Kas & Bank',
          'tittle_set' => 'Jurnal Kas & Bank',
          'header_set' => array(
            '#',
            'Tanggal',
            'Keterangan',
            'Debit',
            'Kredit'
          ),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
    }
  }
  //GENERATE LAPORAN EXCEL
  public function printExcel($request='', $data=[]){
    
    $Export = Excel::create(isset($data['tittle'])?$data['tittle']:'default', function($excel) use ($request, $data){
    $excel->setTitle(isset($data['tittle'])?$data['tittle']:'default');
    $excel->setCreator(isset($data['creator'])?$data['creator']:'Aplikasi Akuntansi')->setCompany('Lotte');
    $excel->setDescription('Export');

    $excel->sheet(isset($data['tittle'])?$data['tittle']:'default', function($sheet) use ($request, $data){
    $tray = array_keys($data['set_width']);
    $last_cell = end($tray);
    $sheet->row(1, array(
        isset($data['tittle_set'])?$data['tittle_set']:'default'
    ));

    $sheet->row(2, array(
      'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
    ));
    $sheet->row(3, isset($data['header_set'])?$data['header_set']:[]);

    $row = 4;
    $no =1;
    foreach ($data['record'] as $elm) {
        // dd($elm);
        $sheet->row($row, $elm);
      $row++;
    }
    $sheet->cells('E4'.':'.$last_cell.''.$row, function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->setBorder('A3:'.$last_cell.''.$row, 'thin');
    $sheet->mergeCells('A1:'.$last_cell.'1');
    $sheet->mergeCells('A2:'.$last_cell.'2');
    $sheet->cells('A1:A1', function($cells){
      $cells->setFontSize(14);
      $cells->setFontWeight('bold');
      $cells->setAlignment('center');
    });
    $sheet->cells('A2:'.$last_cell.'2', function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->cells('A3:'.$last_cell.'3', function($cells){
      $cells->setAlignment('center');
      $cells->setBackground('#999999');
      $cells->setFontColor('#FFFFFF');
    });
    $sheet->setWidth(isset($data['set_width'])?$data['set_width']:[]);
    $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->setWrapText(true);
    $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top')
    );
    $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top', 'horizontal' => 'center')
    );

    });
    });
    $Export ->download('xls');
  }

}


