<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Coa;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Kki;
use Lotte\Models\Master\Produk;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransPyr;

use Lotte\Libraries\JurnalLib;

use Datatables;
use Carbon\Carbon;


class LaporanPusatKeuanganController extends Controller
{
    protected $link = 'laporan-pusat/';
    public $tpl_labarugi;
    public $tpl_neraca;
    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Keuangan");   
        $this->tpl_labarugi = [
            'pendapatan_usaha' => [
                'text'  => 'Pendapatan Usaha',
                'type'  => 'label',
                'child' => [
                    'penjualan_barang_dagang' => [
                        'text'  => 'Penjualan Barang Dagang',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['4.1.0.0'],
                    ],
                    'potongan_penjualan' => [
                        'text'  => 'Potongan Penjualan',
                        'type'  => 'post',
                        'value' => 'negative',
                        'coa'   => ['4.3.0.0'],
                    ]
                ]
            ],
            'total_pendapatan_usaha' => [
                'text'  => 'Total Pendapatan Usaha',
                'type'  => 'post',
                'value' => 'positive', // positive, negative
                'coa'   => ['4.1.0.0', '4.1.1.0', '4.2.0.0', '4.3.0.0'],
            ],
            'hpp' => [
                'text'  => 'Harga Pokok Penjualan',
                'type'  => 'post',
                'value' => 'positive', // positive, negative
                'coa'   => ['5.1.0.0'],
            ],
            'pendapatan_kotor' => [
                'text' => 'Pendapatan Kotor',
                'type' => 'dec',
                'exp'  => ['pendapatan_usaha', 'hpp'],
            ],
            'biaya_pemasaran_umun_adm' => [
                'text' => 'Biaya Pemasaran Umum & Adm',
                'type' => 'label',
                'child'=> [
                    'biaya_pemasaran' => [
                        'text'  => 'Biaya Pemasaran',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.1.1.0', '6.1.1.1', '6.1.1.2', '6.1.1.3', '6.1.1.4'],
                    ],
                ]
            ],
            'total_biaya_pemasaran_umum' => [
                'text' => 'Total Biaya Pemasaran Umum & Adm',
                'type' => 'inc',
                'exp'  => ['biaya_pemasaran_umun_adm'],
            ],
            'beban_umum_administrasi' => [
                'text' => 'Biaya Pemasaran Umum & Adm',
                'type' => 'label',
                'child' => [
                    'biaya_gaji_lembur' => [
                        'text'  => 'Biaya Gaji, Lembur & THR',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.1.1'],
                    ],
                    'biaya_makan_karyawan' => [
                        'text'  => 'Biaya Makan Karyawan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.1.2'],
                    ],
                    'biaya_tunjangan_kesehatan' => [
                        'text'  => 'Biaya Tunjangan Kesehatan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.1.3'],
                    ],
                    'biaya_asuransi' => [
                        'text'  => 'Biaya Asuransi',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.1.4'],
                    ],
                    'biaya_pengiriman' => [
                        'text'  => 'Biaya Pengiriman',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.1'],
                    ],
                    'biaya_air' => [
                        'text'  => 'Biaya Air',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.2'],
                    ],
                    'biaya_listrik' => [
                        'text'  => 'Biaya Listrik',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.3'],
                    ],
                    'biaya_telp_internet' => [
                        'text'  => 'Biaya Telp & Internet',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.4'],
                    ],
                    'biaya_keamanan' => [
                        'text'  => 'Biaya Keamanan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.5'],
                    ],
                    'biaya_lainnya_sumbangan' => [
                        'text'  => 'Biaya Lainnya (sumbangan, dll)',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.6'],
                    ],
                    'plastik_pembungkus' => [
                        'text'  => 'Plastik Pembungkus',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.7'],
                    ],
                    'barang_rusak' => [
                        'text'  => 'Barang Rusak',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.8'],
                    ],
                    'barang_hilang' => [
                        'text'  => 'Barang Hilang',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.9'],
                    ],
                    'biaya_distribusi' => [
                        'text'  => 'Biaya Distribusi (Adm)',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.10'],
                    ],
                    'beban_pemeliharaan' => [
                        'text'  => 'Beban Pemeliharaan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.3.1', '6.2.3.2', '6.2.3.3'],
                    ],
                    'biaya_penyusutan' => [
                        'text'  => 'Biaya Penyusutan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.3.1.0', '6.3.2.0', '6.3.3.0'],
                    ],
                    'biaya_bensin' => [
                        'text'  => 'Biaya Bensin',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.11'],
                    ],
                    'biaya_perlengkapan_toko' => [
                        'text'  => 'Biaya Perlengkapan Toko',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.12'],
                    ],
                    'biaya_sewa_toko' => [
                        'text'  => 'Biaya Sewa Toko',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.13'],
                    ],
                    'biaya_lain_operasional' => [
                        'text'  => 'Biaya Lain-Lain Operasional',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.14'],
                    ],
                    'biaya_lain_nonoperasional' => [
                        'text'  => 'Biaya Lain-Lain Non-Operasional',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.15'],
                    ],
                ]
            ],
            'total_beban_umum_administrasi' => [
                'text' => 'Total Beban Umum & Administrasi',
                'type' => 'inc',
                'exp'  => ['beban_umum_administrasi'],
            ],
            'laba_usaha' => [
                'text' => 'Laba Usaha',
                'type' => 'inc',
                'exp'  => ['pendapatan_kotor', 'total_biaya_pemasaran_umum', 'total_beban_umum_administrasi'],
            ],
            'pendapatan_beban_lain2' => [
                'text' => 'Pendapatan (Beban) Lain-lain',
                'type' => 'label',
                'child'=> [
                    'selisih_kas_lebih' => [
                        'text'  => 'Selisih Kas Lebih',
                        'type'  => 'post',
                        'value' => 'positive', // positive, negative
                        'coa'   => ['7.1.0.0'],
                    ],
                    'selisih_kas_kurang' => [
                        'text'  => 'Selisih Kas Kurang',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['8.1.0.0'],
                    ],
                    'penghasilan_deviden' => [
                        'text'  => 'Penghasilan Deviden',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['7.2.0.0'],
                    ],
                    'laba_penjualan_aktiva_tetap' => [
                        'text'  => 'Laba Penjualan Aktiva Tetap',
                        'type'  => 'post',
                        'value' => 'positive', // positive, negative
                        'coa'   => ['7.3.0.0'],
                    ],
                    'rugi_penjualan_aktiva_tetap' => [
                        'text'  => 'Rugi Penjualan Aktiva Tetap',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['8.2.0.0'],
                    ],
                    'penghasilan_sewa' => [
                        'text'  => 'Penghasilan Sewa',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['7.4.0.0'],
                    ],
                    'biaya_admin_bank' => [
                        'text'  => 'Biaya Admin Bank',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['8.3.0.0'],
                    ],
                    'penghasilan_lainnya' => [
                        'text'  => 'Penghasilan Lainnya',
                        'type'  => 'post',
                        'value' => 'positive', // positive, negative
                        'coa'   => ['7.5.0.0'],
                    ],
                    'beban_lainnya' => [
                        'text'  => 'Beban Lainnya',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['8.4.0.0'],
                    ],
                ],
            ],
            'total_pendapatan_beban_lain2' => [
                'text' => 'Total Pendapatan (Beban) Lain-lain',
                'type' => 'inc',
                'exp'  => ['pendapatan_beban_lain2']
            ],
            'total_laba_rugi_bersih' => [
                'text' => 'Laba (Rugi) Bersih',
                'type' => 'inc',
                'exp'  => ['laba_usaha', 'total_pendapatan_beban_lain2']
            ],
        ];

        $this->tpl_neraca = [
            'aset' => [
                'text'  => 'ASET',
                'type'  => 'label',
            ],
            'aset_lancar' => [
                'text'  => 'ASET LANCAR',
                'type'  => 'label',
                'child' => [
                    'kas' => [
                        'text'  => 'Kas',
                        'type'  => 'post',
                        'value' => 'positive',
                        // 'coa'   => ['1.1.1.1', '1.1.1.2'],
                        'coa'   => ['1.1.1.1', '7.1.0.0', '8.1.0.0'],
                    ],
                    'kas_hasil_penjualan' => [
                        'text'  => ' - Kas Hasil Penjualan',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.1.1'],
                    ],
                    'kas_kecil' => [
                        'text'  => ' - Kas Kecil',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.1.2'],
                    ],
                    'bank' => [
                        'text'  => 'Bank',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.2.1', '1.1.2.2', '1.1.2.3'],
                    ],
                    'piutang_usaha' => [
                        'text'  => 'Piutang Usaha',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.3.1'],
                    ],
                    'persediaan_barang_dagang' => [
                        'text'  => 'Persediaan Barang Dagang',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.4.0'],
                    ],
                    'deposit' => [
                        'text'  => 'Deposit',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.6.1'],
                    ],
                    'aktiva_lancar_lainnya' => [
                        'text'  => 'Aktiva Lancar Lainnya',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.7.0'],
                    ],
                ]
            ],
            'jml_aset_lancar' => [
                'text' => 'Jumlah Aset Lancar',
                // 'type' => 'inc',
                // 'exp'  => ['aset_lancar'],
                'type'  => 'post',
                'value' => 'positive',
                'coa'   => ['1.1.1.1', '7.1.0.0', '8.1.0.0', '1.1.2.1', '1.1.2.2', '1.1.2.3', '1.1.3.1', '1.1.4.0', '1.1.6.1', '1.1.7.0'],
            ],
            'aset_tidak_lancar' => [
                'text'  => 'ASET TIDAK LANCAR',
                'type'  => 'label',
                'child' => [
                    'tanah' => [
                        'text'  => 'Tanah',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.2.1.0'],
                    ],
                    'gedung' => [
                        'text'  => 'Gedung',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.2.2.0'],
                    ],
                    'kendaraan' => [
                        'text'  => 'Kendaraan',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.2.4.0'],
                    ],
                    'peralatan_toko' => [
                        'text'  => 'Peralatan Toko',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.2.6.0'],
                        // 'coa'   => ['1.2.6.0', '1.2.7.0'],
                    ],
                    'akumulasi_penyusutan' => [
                        'text'  => 'Akumulasi Penyusutan',
                        'type'  => 'post',
                        'value' => 'negative',
                        'coa'   => ['1.2.3.0', '1.2.5.0', '1.2.7.0'],
                    ]
                ]
            ],
            'jml_aset_tidak_lancar' => [
                'text' => 'Jumlah Tidak Aset Lancar',
                'type' => 'inc',
                'exp'  => ['aset_tidak_lancar'],
            ],
            'jml_aset' => [
                'text' => 'Jumlah Aset',
                'type' => 'inc',
                'exp'  => ['jml_aset_lancar', 'jml_aset_tidak_lancar'],
            ],
            'liabilitas_dan_ekuitas' => [
                'text' => 'LIABILITAS DAN EKUITAS',
                'type' => 'label',
            ],
            'liabilitas_jangka_pendek' => [
                'text' => 'LIABILITAS JANGKA PENDEK',
                'type' => 'label',
                'child'=> [
                    'utang_usaha' => [
                        'text'  => 'Utang Usaha',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['2.1.1.0'],
                    ],
                    'hutang_jangka_panjang_jatuh_tempo' => [
                        'text'  => 'Hutang Jangka Panjang yang Jatuh Tempo dalam Waktu Setahun',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['2.1.1.1', '2.1.2.1', '2.1.5.1'],
                    ],
                ]
            ],
            'jml_liabilitas_jangka_pendek' => [
                'text' => 'Jumlah Liabilitas Jangka Pendek',
                'type' => 'inc',
                'exp'  => ['liabilitas_jangka_pendek'],
            ],
            'liabilitas_jangka_panjang' => [
                'text' => 'LIABILITAS JANGKA PANJANG',
                'type' => 'label',
                'child'=> [
                    'hutang_jangka_panjang' => [
                        'text'  => 'Hutang Jangka Panjang',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['2.2.1.0', '2.2.2.0', '2.2.3.0'],
                    ],
                ]
            ],
            'jml_liabilitas_jangka_panjang' => [
                'text' => 'Jumlah Liabilitas Jangka Panjang',
                'type' => 'inc',
                'exp'  => ['liabilitas_jangka_panjang'],
            ],
            'jml_liabilitas' => [
                'text' => 'Jumlah Liabilitas',
                'type' => 'inc',
                'exp'  => ['jml_liabilitas_jangka_pendek', 'jml_liabilitas_jangka_panjang'],
            ],
            // ==tambahan ,baru tamplate
            'ekuitas' => [
                'text' => 'EKUITAS',
                'type' => 'label',
                'child'=> [
                    'modal_dasar' => [
                        'text'  => 'Modal Dasar',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['3.1.1.0'],
                    ],
                    'tambahan_modal_disetor' => [
                        'text'  => 'Tambahan Modal Disetor',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['3.1.2.0'],
                    ],
                    'opening_balance_equity' => [
                        'text'  => 'Opening Balance Equity',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['3.2.0.0'],
                    ],
                    'deviden' => [
                        'text'  => 'Deviden',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['3.3.0.0'],
                    ],
                    'laba_ditahan' => [
                        'text'  => 'Laba Ditahan',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['3.4.0.0'],
                    ],
                    'laba_bulan_ini' => [
                        'text'  => 'Laba Bulan Ini',
                        'type'  => 'ref',
                        'source' => 'src_laba',
                        'exp'   => 'total_laba_rugi_bersih',
                    ],
                ]
            ],
            'jumlah_ekuitas' => [
                'text' => 'Jumlah Ekuitas',
                'type' => 'inc',
                'exp'  => ['ekuitas']
            ],
            'jumlah_liabilitas_dan_ekuitas' => [
                'text' => 'Jumlah Liabilitas Dan Ekuitas',
                'type' => 'inc',
                'exp'  => ['jml_liabilitas', 'jumlah_ekuitas'],
            ],
            // ==
        ];
    }

  public function postGridTemp(Request $request){
    // dd($request->all());

    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }
    $result= $reg->get();
    $temp=[];
    $temp_id=[];

    foreach ($result as $val){
      $temp_id[] = $val->id;
      $temp[] = $val->kode;
    }
    // $request['tmuk_kode']= $temp;
    $record = [];

    switch ($request->types) {
      case 'laba-rugi':
        $request['tanggal_start']  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
        $request['tanggal_end']    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
        $tanggal_start = Carbon::createFromFormat('m/Y', $request->tanggal_start)->startOfMonth();
        $tanggal_end   = Carbon::createFromFormat('m/Y', $request->tanggal_end)->endOfMonth();

        foreach ($temp as $val) {
            $record[] = JurnalLib::generateLaba($val, $tanggal_start, $tanggal_end, $this->tpl_labarugi);
        }

        $data = [
            'records' => $record,
            'start'  => $request->tanggal_start,
            'end'    => $request->tanggal_end,
        ];

        return $this->render('modules.laporan.laporan-pusat.lists.keuangan.'.$request->types, $data);
      break;
      case 'laporan-laba-ditahan':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.keuangan.'.$request->types, $data);
      break;
      case 'laporan-piutang':
        if($request->tanggal_start and $request->tanggal_end){
          $tanggal_start = Carbon::createFromFormat('m/Y', $request->tanggal_start)->startOfMonth()->format('Y-m-d');
          $tanggal_end   = Carbon::createFromFormat('m/Y', $request->tanggal_end)->endOfMonth()->format('Y-m-d');
        }

        $records = TransJurnal::select('*')
                    ->whereIn('coa_kode', ['2.1.1.0'])
                    ->whereIn('tmuk_kode', $temp)
                    ->where('delete', 0);

        if($request->tanggal_start and $request->tanggal_end){
          $records->whereBetween('tanggal', array($tanggal_start, $tanggal_end));
        }

        $data = [
            'records' => $records->get(),
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.keuangan.'.$request->types, $data);
      break;
      case 'fokus-keuangan':
      // dd($request->all())
        $request['tanggal_start']  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
        $request['tanggal_end']    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
        $diff = Carbon::createFromFormat('m/Y', $request->tanggal_start)->diffInMonths(Carbon::createFromFormat('m/Y', $request->tanggal_end));
        $kki = Kki::select('*')->whereIn('tmuk_id', $temp_id)->where('status', 1)->get();

        $jurnal = TransJurnal::select('*');
        if($request->tmuk_kode){
            $jurnal->where('tmuk_kode', $temp);
        }
        $jurnal->where('delete', 0)
        ->select('*')->get();
         $data = [
            'records'   => $kki,
            'diff'      => $diff,
            'dayStart'  => Carbon::createFromFormat('m/Y', $request->tanggal_start)->startOfMonth(),
            'tmuk_kode' => $request->tmuk_kode,
            'jurnal'    => $jurnal,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.keuangan.'.$request->types, $data);
      break;
      case 'realisasi-rab':
      // dd($request->all());
        $request['tanggal_start']  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
        $request['tanggal_end']    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
        $saldo  = TransJurnal::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("jumlah") as total_penjualan, COUNT("deskripsi") as struk, tmuk_kode  '))->groupBy('year', 'month', 'tmuk_kode')
                     // ->where('tmuk_kode', '0600400044')
                      ->where('deskripsi', 'ilike', '%Modal%')
                     // ->whereBetween(\DB::raw('tanggal'), array($tanggal_start, $tanggal_end))
                     ->where('coa_kode', '1.1.2.1')
                     ->where('delete', '0')
                     ->get();
            
            $records = Kki::select('*')
            ->whereIn('tmuk_id', $temp_id)
            ->where('status', 1);

            $rcd     = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("total") as total_penjualan, COUNT("jual_kode") as struk  '))->groupBy('year', 'month')
                        // ->where('tmuk_kode', $temp)
                        // ->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end))
                        ->get();
                        // dd($rcd);
            // $recordakuisisi = TransPyr::with('detailPyr')->where('status', 2)
            //                  ->whereIn('tmuk_kode', $temp)
            //                  ->where('nomor_pyr', 'like', '%OPENING%')
            //                  ->first();
            // dd($recordakuisisi);
            $data = [
                'saldo'     => $saldo,
                'detail'    => $records->get(),
                // 'akuisisi'  => $recordakuisisi,
                'penjualan' => $rcd,
                'start'     => $request->tanggal_start,
                'end'       => $request->tanggal_end
            ];
            // dd($data);
        return $this->render('modules.laporan.laporan-pusat.lists.keuangan.'.$request->types, $data);
      break;
      case 'pengeluaran-tahun':
      // dd($request->all());
        $tanggal_start  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
        $tanggal_end    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));


        
        $from           = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_start);
        $to             = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_end);
        $diff_in_months = $to->diffInMonths($from);
        $header         = [];
        $nilai          = [];
        $array          = [];

        for($i=0; $i<=$diff_in_months; $i++){
            $header[] = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ));
            $nilai[date('Y-m', strtotime($from->format('Ymd') . ' +'.$i.'month'))]  = "0.00";
        }
        $result    = Coa::getDataCoaPertahun([
            'tmuk' => $temp,
            'from' => $from->format('Y-m') . '-01 00:00:00',
            'to'   => $to->format('Y-m') . '-'.$to->endOfMonth()->format('d').' 23:59:59',
        ]);
        // dd($request['tmuk_kode']);


        $result = $result->get();

        // dd($result);

        $temp      = [];
        $temp_rest = [];
        
        foreach ($result as $rc) {
            // $temp[$rc->kode_coa.'-'.$rc->nama_coa]['nama_coa'] = $rc->nama_coa;
            $temp[$rc->kode_coa.'-'.$rc->nama_coa][$rc->tmuk_kode.'-'.$rc->nama_tmuk][$rc->tahun.'-'.(strlen($rc->bulan)==1?'0'.$rc->bulan:$rc->bulan)] = $rc->jumlah;
        }

        $data = [
            'records'    => $temp,
            'header'     => $header,
            'nilai_temp' => $nilai,
            'start'      => $request->tanggal_start,
            'end'        => $request->tanggal_end,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.keuangan.'.$request->types, $data);
      break;
      default:
        $request['tanggal_start']  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
        $request['tanggal_end']    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
        $tanggal_start = Carbon::createFromFormat('m/Y', $request->tanggal_start)->startOfMonth();
        $tanggal_end   = Carbon::createFromFormat('m/Y', $request->tanggal_end)->endOfMonth();

        foreach ($temp as $val) {
            $labarugi = JurnalLib::generateLaba($val, $tanggal_start, $tanggal_end, $this->tpl_labarugi);
            $record[]   = JurnalLib::generate($val, $tanggal_start, $tanggal_end, $this->tpl_neraca, ['src_laba' => $labarugi]);
        }
        $data = [
            'records' => $record,
            'start'  => $request->tanggal_start,
            'end'    => $request->tanggal_end,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.keuangan.neraca', $data);
      break;
    }
  }

    public function postGridAll(Request $request){
    // if($request->tanggal_start && $request->tanggal_end){
    //     $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
    //     $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    // }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }
    $result= $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    switch ($request->types) {
      case 'laba-rugi':
      break;
      case 'laporan-laba-ditahan':
            $tanggal_start  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
            $tanggal_end    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
            $from           = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_start);
            $to             = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_end);
            $records = TransJurnal::select('*')
               ->whereIn('coa_kode', ['3.4.0.0'])
               ->whereIn('tmuk_kode', $temp)
               ->where('delete', 0);

            $records->whereBetween('tanggal', array($from->format('Y-m') . '-01 00:00:00', $to->format('Y-m') . '-'.$to->endOfMonth()->format('d').' 23:59:59'));

            $record = $records->get();
            return Datatables::of($record)
              ->addColumn('num', function ($record) use ($request) {
                  return $request->get('start');
              })
              ->addColumn('tanggal', function ($record) {
                  return \Carbon\Carbon::parse($record->tanggal)->format('d/m/Y');
              })
              ->addColumn('deskripsi', function ($record) {
                  return ucfirst($record->deskripsi);
              })
              ->addColumn('saldo', function ($record) {
                  return FormatNumber($record->jumlah);
              })

              ->make(true);
      break;
      case 'laporan-piutang':
      break;
      case 'fokus-keuangan':
      break;
      case 'realisasi-rab':
      break;
      case 'pengeluaran-tahun':
      break;
      default:
      break;
    }
  }

  //PRINT PDF
  public function postPrint(Request $request){

    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }

    $request['tmukd'] = clone  $reg;

    $result= $reg->get();
    $temp=[];
    $temps=[];

    foreach ($result as $val){
      $temp[] = ['kode'=>$val->kode, 'nama'=>$val->nama];
      $temps[] = $val->kode;
      $temp_id[] = $val->id;
    }
    $request['tmuk_kode']=$temps;

    switch ($request->types) {
      case 'laba-rugi':
            $request['tanggal_start']  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
            $request['tanggal_end']    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
            $tanggal_start = Carbon::createFromFormat('m/Y', $request->tanggal_start)->startOfMonth()->format('Y-m-d');
            $tanggal_end   = Carbon::createFromFormat('m/Y', $request->tanggal_end)->endOfMonth()->format('Y-m-d');
            foreach ($temp as $val) {
                $record[] = [ 
                    'tmuk_nama'=> $val['kode'].'-'.$val['nama'],
                    'tmuk_list'=> JurnalLib::generateLaba($val['kode'], $tanggal_start, $tanggal_end, $this->tpl_labarugi)];
            }
        return $this->printPdf($request, $record);
      break;
      case 'laporan-laba-ditahan':
            $tanggal_start  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
            $tanggal_end    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
            $from           = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_start);
            $to             = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_end);
            $records = TransJurnal::select('*')
               ->whereIn('coa_kode', ['3.4.0.0'])
               ->whereIn('tmuk_kode', $temps)
               ->where('delete', 0);

            $records->whereBetween('tanggal', array($from->format('Y-m') . '-01 00:00:00', $to->format('Y-m') . '-'.$to->endOfMonth()->format('d').' 23:59:59'));
            
            $record = $records->get();
            return $this->printPdf($request, $record);
      break;
      case 'fokus-keuangan':

      break;
      case 'laporan-piutang':

        $tanggal_start  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
        $tanggal_end    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
        $from           = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_start);
        $to             = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_end);

        $records = TransJurnal::with('tmuk')
                    ->whereIn('coa_kode', ['2.1.1.0'])
                    ->whereIn('tmuk_kode', $temps)
                    ->where('delete', 0);

        $records->whereBetween('tanggal', array($from->format('Y-m') . '-01 00:00:00', $to->format('Y-m') . '-'.$to->endOfMonth()->format('d').' 23:59:59'));

        $record = $records->get();
        // dd($records);

        return $this->printPdf($request, $record);
      break;
      case 'realisasi-rab':
      // dd($request->all());
        $tanggal_start  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
        $tanggal_end    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
        $record = Kki::select('*')
                        // ->where('tmuk_id', $temp_id)
                        ->where('status', 1);
                        // ->get();
        $data = $record->get();

        $saldo  = TransJurnal::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("jumlah") as total_penjualan, COUNT("deskripsi") as struk, tmuk_kode  '))->groupBy('year', 'month', 'tmuk_kode')
                   // ->where('tmuk_kode', $temps)
                      ->where('deskripsi', 'ilike', '%Modal%')
                     // ->whereBetween(\DB::raw('tanggal'), array($tanggal_start, $tanggal_end))
                     ->where('coa_kode', '1.1.2.1')
                     ->where('delete', '0')
                     ->get();
        $request['saldo'] = $saldo;
                    
        // $rcd = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("total") as total_penjualan, COUNT("jual_kode") as struk, tmuk_kode  '))->groupBy('year', 'month', 'tmuk_kode')
        //            // ->where('tmuk_kode', $temps)
        //            // ->whereBetween(\DB::raw('tanggal'), array($tanggal_start, $tanggal_end))
        //            ->get();
                    
        // $recordakuisisi = TransPyr::with('detailPyr')
                         // ->where('status', 2)
                            // ->where('tmuk_kode', $temps)
                            // ->where('nomor_pyr', 'like', '%OPENING%')
                           // ->first();
        // $request['recordakuisisi'] = $recordakuisisi;
                                     // dd($recordakuisisi);
        return $this->printPdf($request, $data);
      break;
      case 'pengeluaran-tahun':
        $tanggal_start  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
        $tanggal_end    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
        $from           = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_start);
        $to             = \Carbon\Carbon::createFromFormat('m/Y', $tanggal_end);
        $diff_in_months = $to->diffInMonths($from);
        $header         = [];
        $nilai          = [];
        $array          = [];
        for($i=0; $i<=$diff_in_months; $i++){
            $header[] = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ));
            $nilai[date('Y-m', strtotime($from->format('Ymd') . ' +'.$i.'month'))]  = "0.00";
        }

        $result    = Coa::getDataCoaPertahun([
            'tmuk' => $request['tmuk_kode'],
            'from' => $from->format('Y-m') . '-01 00:00:00',
            'to'   => $to->format('Y-m') . '-'.$to->endOfMonth()->format('d').' 23:59:59',
        ]);
        $result = $result->get();
        $temp      = [];
        $temp_rest = [];
        
        foreach ($result as $rc) {
            $temp[$rc->kode_coa.'-'.$rc->nama_coa][$rc->tmuk_kode.'-'.$rc->nama_tmuk][$rc->tahun.'-'.(strlen($rc->bulan)==1?'0'.$rc->bulan:$rc->bulan)] = $rc->jumlah;
        }
        

        $record = $temp;

        $request['header']     = $header;
        $request['nilai_temp'] = $nilai;
        // dd('tara');
        return $this->printPdf($request, $record);
      break;
      default:
            $request['tanggal_start']  = ($request->tanggal_start!==''?$request->tanggal_start:('01'.date('/Y')));
            $request['tanggal_end']    = ($request->tanggal_end!==''?$request->tanggal_end:('12'.date('/Y')));
            $tanggal_start = Carbon::createFromFormat('m/Y', $request->tanggal_start)->startOfMonth();
            $tanggal_end   = Carbon::createFromFormat('m/Y', $request->tanggal_end)->endOfMonth();
            foreach ($temp as $val) {
                $labarugi = JurnalLib::generateLaba($val['kode'], $tanggal_start, $tanggal_end, $this->tpl_labarugi);
                $record[]   = [ 
                    'tmuk_nama' => $val['kode'].'-'.$val['nama'],
                    'tmuk_list' => JurnalLib::generate($val['kode'], $tanggal_start, $tanggal_end, $this->tpl_neraca, ['src_laba' => $labarugi])
                ];
            }
            return $this->printPdf($request, $record);
      break;
    }
  }

  // GENERATE LAPORAN pdf
  public function printPdf($request, $record=''){
    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $request['tmukd']->first()->lsi->region->area;
    }
    if($request->lsi_id){
      $lsi          = $request['tmukd']->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $request['tmukd']->first()->kode.'-'.$request['tmukd']->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];
    // dd($record);
    $pdf = \PDF::loadView('report.laporan-pusat.keuangan.'.$request->types, $data);
    return $pdf->setOption('margin-bottom', 5)
    ->setOption('margin-top', 5)
    ->setOption('margin-left', 3)
    ->setOption('margin-right', 3)
    ->inline();
  }

  //PRINT EXCEL
  // public function postPrintExcel(Request $request){
  //   if($request->tanggal_start && $request->tanggal_end){
  //       $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
  //       $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
  //   }
  //   $reg = Tmuk::with('lsi','lsi.region');
  //   if($request->lsi_id){
  //     $reg->whereHas('lsi',  function($ql) use($request){
  //       $ql->where('id', (integer)$request->lsi_id);
  //     });
  //   }
  //   if($request->region_id){
  //     $reg->whereHas('lsi.region', function($qr) use($request){
  //       $qr->where('id', (integer)$request->region_id);
  //     });
  //   }
  //   if($request->tmuk_kode){
  //     $reg->where('id', (integer)$request->tmuk_kode);
  //   }
  //   // dd($request->all());
  //   $request['tmukd'] = clone  $reg;

  //   $result = $reg->get();

  //   // $result_tmuk = $request['tmukd']->get();
  //   $temp=[];

  //   foreach ($result as $val){
  //     $temp[] = 
  //     [
  //       'kode'=> $val->kode, 
  //       'nama'=> $val->nama
  //   ];
  //   }

  //   switch ($request->types) {
  //     case 'penjualan-mingguan':
  //     $this->excelMingguan($request);
  //     break;    
  //     default:
  //         foreach ($temp as $val) {
  //               $labarugi = JurnalLib::generateLaba($val['kode'], $tanggal_start, $tanggal_end, $this->tpl_labarugi);
  //               $record[]   = JurnalLib::generate($val['kode'], $tanggal_start, $tanggal_end, $this->tpl_neraca, ['src_laba' => $labarugi]);
  //         }
  //       $this->printPdf($request, $record);
  //     break;
  //   }
  // }

  

  //GENERATE LAPORAN EXCEL
  // public function excelHarian($request=''){
  //   $rangkuman = TransRekapPenjualanTmuk::getDataHarian();
  //   $rangkuman->whereIn('tmuk_kode',  $request->temp);
  //   if($request->tanggal_start and $request->tanggal_end){
  //       $rangkuman->whereBetween(\DB::raw('p.tanggal'), array($request->tanggal_start, $request->tanggal_end));
  //       }
  //       $record = $rangkuman->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();

  //       $data =[];
  //       //dd($record->toArray());
  //       foreach($record->toArray() as $val){
  //           $data[$val['tmuk_kode']]['child'][] = $val;      
  //       }
  //       $temps = [];
  //       foreach($data as $key=>$val){
  //           $struk           = 0;
  //           $tunai           = 0;
  //           $total_hpp       = 0;
  //           $profit          = 0;
  //           $total_penjualan = 0;
  //           $margin          = 0;
  //           foreach($val['child'] as $tot){
  //               $struk +=(integer)$tot['struk'];
  //               $tunai +=(integer)$tot['tunai'];
  //               $total_hpp +=(integer)$tot['total_hpp'];
  //               $profit +=(integer)$tot['total_penjualan']-(integer)$tot['total_hpp'];
  //               $total_penjualan +=(integer)$tot['total_penjualan'];
  //           }
  //           $temps[$key]['total'] = [
  //             "tmuk_kode"       => $key,
  //             "tanggal"         => $tot['nama_tmuk'],
  //             "struk"           => $struk,
  //             "tunai"           => $tunai,
  //             "total_hpp"       => $total_hpp,
  //             "profit"          => $profit,
  //             "total_penjualan" => $total_penjualan,
  //             "margin"          => ((integer)$profit/(integer)$total_penjualan)*100
  //           ];
            
  //           $temps[$key]['child'] = $val['child'];
  //       }

  //       $request['record']=$temps;

  //       $Export = Excel::create('Rekap Penjualan Perhari ', function($excel) use ($request){
  //       $excel->setTitle('Rekap Penjualan Perhari');
  //       $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
  //       $excel->setDescription('Export');

  //       $excel->sheet('Rekap Penjualan Perhari', function($sheet) use ($request){
  //       $sheet->row(1, array(
  //         'Rekap Penjualan Perhari'
  //         ));

  //       $sheet->row(2, array(
  //       'Periode : ' . $request->start . ' - ' . $request->end
  //         ));
  //       $sheet->row(3, array(
  //         'NO',
  //         'Kode TMUK',
  //         'Tanggal',
  //         'Jumlah Struk',
  //         'Total Penjualan (Rp)',
  //         'Total HPP',
  //         'Profit',
  //         'Margin',
  //         ));

  //       $row = 4;
  //       $no =1;
  //       foreach ($request->record as $elm) {
  //           // dd($elm['total']);
  //           $sheet->row($row, array(
  //               $no,
  //               $elm['total']['tmuk_kode'],
  //               $elm['total']['tanggal'],
  //               $elm['total']['struk'],
  //               rupiah($elm['total']['total_penjualan']),
  //               rupiah($elm['total']['total_hpp']),
  //               rupiah( $elm['total']['profit']),
  //               round($elm['total']['margin'],2) ,
  //         ));
  //         $row = $row+1;
  //         foreach ($elm['child'] as $elms) {      
  //           $sheet->row($row, array(
  //               $no,
  //               '',
  //               $elms['tanggal'],
  //               $elms['struk'],
  //               rupiah($elms['total_penjualan']),
  //               rupiah($elms['total_hpp']),
  //               rupiah($elms['total_penjualan']-$elms['points']),
  //               (($elms['total_penjualan']-$elms['points'])/$elms['total_penjualan'])*100,
  //               ));
  //           $row=$row;
  //           $row++;$no++;
  //         }
  //       }
  //       $sheet->cells('E4'.':H'.$row, function($cells){
  //         $cells->setFontSize(12);
  //         $cells->setAlignment('center');
  //       });
  //       $sheet->setBorder('A3:H'.$row, 'thin');
  //       $sheet->mergeCells('A1:H1');
  //       $sheet->mergeCells('A2:H2');
  //       $sheet->cells('A1:A1', function($cells){
  //         $cells->setFontSize(14);
  //         $cells->setFontWeight('bold');
  //         $cells->setAlignment('center');
  //       });
  //       $sheet->cells('A2:H2', function($cells){
  //         $cells->setFontSize(12);
  //         $cells->setAlignment('center');
  //       });
  //       $sheet->cells('A3:H3', function($cells){
  //         $cells->setAlignment('center');
  //         $cells->setBackground('#999999');
  //         $cells->setFontColor('#FFFFFF');
  //       });
  //       $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20));
  //       $sheet->getStyle('B4:H'.$row)->getAlignment()->setWrapText(true);
  //       $sheet->getStyle('B4:H'.$row)->getAlignment()->applyFromArray(
  //         array('vertical' => 'top')
  //       );
  //       $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
  //         array('vertical' => 'top', 'horizontal' => 'center')
  //       );

  //       });
  //   });
  //   $Export ->download('xls');
  // }

}
