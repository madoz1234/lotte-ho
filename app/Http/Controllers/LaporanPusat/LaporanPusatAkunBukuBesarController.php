<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\Coa;
use Datatables;
use Illuminate\Support\Collection;
use Carbon\Carbon;


class LaporanPusatAkunBukuBesarController extends Controller
{
    protected $link = 'laporan-pusat/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Pusat");   
    }

  public function postGridTemp(Request $request){
    // dd($request->all());
    //query temp
    $this->setLink($this->link.'akun-buku-besar');
    switch ($request->types) {
      case 'bukti-jurnal-umum':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-buku-besar.'.$request->types, $data);
      break;
      case 'ringkasan-buku-besar':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-buku-besar.'.$request->types, $data);
      break;
      case 'neraca-saldo':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-buku-besar.'.$request->types, $data);

      break;
      case 'daftar-akun':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-buku-besar.'.$request->types, $data);
      break;
      case 'penjualan-mingguan':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-buku-besar.'.$request->types, $data);
      break;
      default:
        $data = [
          'lists_grid' => 'all-penjualan',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-buku-besar.'.$request->types, $data);
      break;
    }
  }

  public function postGridAll(Request $request){
    // dd($request->all());
    // 
    if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }

    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }
    $result= $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    //query temp
    // 
     switch ($request->types) {
      case 'bukti-jurnal-umum':
          $records = TransJurnal::select(DB::raw('DATE("tanggal") as date, coa_kode, SUM("jumlah") as jml, posisi, idtrans'))->groupBy('tmuk_kode','date', 'coa_kode', 'posisi', 'idtrans')
          ->whereIn('tmuk_kode', $temp)
          ->where('delete', 0);

        if($request->tanggal_start && $request->tanggal_end){
       
          $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }

        $record = $records->orderBy(\DB::raw('tmuk_kode'),'DESC');
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('nama', function ($record){
            return $record->coa->nama;
        })
        ->addColumn('debit', function ($record){
            return $record->posisi=='D'? rupiah($record->jml):'-';
        })
        ->addColumn('kredit', function ($record){
            return $record->posisi=='K'? rupiah($record->jml):'-';
        })
        ->make(true);
      break;
      case 'ringkasan-buku-besar':
        $records = TransJurnal::with('coa')
                    ->whereIn('tmuk_kode', $temp)
                    ->where('delete', 0);
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $record = $records->orderBy('tanggal')->orderBy('coa_kode')->get();
                
        $i           = 1; 
        $saldo_awal  = [];
        $saldo_akhir = [];
        $temp        = [];
        $rec = new Collection;
        foreach ($record as $data){
            $tanggal         = \Carbon\Carbon::parse($data->tanggal)->format('Y-m-d');
            $coa             = $data->coa_kode.' - '. $data->coa->nama;
            $saldo_awal[$i]  = ($i == 1? 0 : $saldo_akhir[$i-1]);
            $jumlah_debit    = $data->posisi=='D' ? $data->jumlah : '0';
            $jumlah_kredit   = $data->posisi=='K' ? $data->jumlah : '0';
            $posisi_normal   = $data->coa->posisi;
            $bersih          = ($posisi_normal == 'D'?($jumlah_debit - $jumlah_kredit):($jumlah_kredit - $jumlah_debit));
            $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
            $rec->push([
                'tanggal'       => $tanggal,
                'coa'           => $coa,
                'saldo_awal'    => rupiah((integer)$saldo_awal[$i]),
                'jumlah_debit'  => rupiah((integer)$jumlah_debit),
                'jumlah_kredit' => rupiah((integer)$jumlah_kredit),
                'posisi_normal' => rupiah((integer)$posisi_normal),
                'bersih'        => rupiah((integer)$bersih),
                'saldo_akhir'   => rupiah((integer)$saldo_akhir[$i])
            ]);
         $i++;
        }
        // 
        return Datatables::of($rec)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->make(true);
      break;
      case 'neraca-saldo':
            $records = Coa::with(['jurnal' => function($q) use ($temp, $request){
                    $q->whereIn('tmuk_kode', $temp);
                    if($request->tanggal_start && $request->tanggal_end){
                        $q->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
                    }
                }])->orderBy('kode');

            $record = $records->get();
            $i           = 1; 
            $saldo_awal = [];
            $saldo_akhir = [];
            $rec = new Collection;
            foreach ($record as $data){
                $nama_akun       =  $data->kode.' - '.$data->nama;
                $saldo_awal[$i]  = ($i == 1? 0 :$saldo_akhir[$i-1]); 
                $jumlah_debit    = $data->jurnal->where('posisi', 'D')->sum('jumlah');
                $jumlah_kredit   = $data->jurnal->where('posisi', 'K')->sum('jumlah');
                $posisi_normal   = $data->posisi;
                $bersih          = ($posisi_normal == 'D' ?( $jumlah_debit - $jumlah_kredit):($jumlah_kredit - $jumlah_debit));
                $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                $rec->push([
                    'nama_akun'     => $nama_akun,
                    'saldo_awal'    => rupiah((integer)$saldo_awal[$i]),
                    'jumlah_debit'  => rupiah((integer)$jumlah_debit),
                    'jumlah_kredit' => rupiah((integer)$jumlah_kredit),
                    'posisi_normal' => rupiah((integer)$posisi_normal),
                    'bersih'        => rupiah((integer)$bersih),
                    'saldo_akhir'   => rupiah((integer)$saldo_akhir[$i])
                ]);
                $i++; 
            }
        return Datatables::of($rec)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->make(true);
      break;
      case 'daftar-akun':
        $record = Coa::with('creator')
        ->whereHas('jurnal',  function($record) use ($request, $temp){
            $record->whereIn('tmuk_kode', $temp)
                   ->where('delete', 0);
        })
        ->select('*')->orderBy('kode');
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('status', function ($record){
            return $record->status = 1 ? ' Aktif' : 'Non-Aktif';
        })
        ->make(true);
      break;
      default:
           $record = Coa::with(['jurnal' => function($q) use ($temp, $request){
           $q->whereIn('tmuk_kode', $temp)->where('delete', 0);
                if($request->tanggal_start && $request->tanggal_end){
                    $q->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
                }
                $q = $q->orderBy('tanggal');
                return $q;
            }]);
            if($request->coa){
                $record->where('kode', $request->coa);
            }
            $record->orderBy('kode', 'ASC');

            $record = $record->get();
            $temp = [];

            $rec = new Collection; 
            foreach ($record as $key => $row){
                if($row->jurnal->count() != 0){
                    $i           = 1; 
                    $saldo_awal = [];
                    $saldo_akhir = [];
                    foreach ($row->jurnal->sortBy('tanggal') as $key => $rw){
                        $nama_akun       = $rw->coa_kode.' - '.$rw->coa->nama;
                        $tanggal         = \Carbon\Carbon::parse($rw->tanggal)->format('Y-m-d');
                        $id_tsransaksi   = $rw->idtrans;
                        $jumlah_debit    = $rw->posisi=='D' ? $rw->jumlah : '0';
                        $jumlah_kredit   = $rw->posisi=='K' ? $rw->jumlah : '0';
                        $saldo_awal[$i]  = ($i == 1?0:$saldo_akhir[$i-1]);
                        $posisi_normal   = $row->posisi;
                        $bersih          = ($posisi_normal == 'D'?($jumlah_debit - $jumlah_kredit):($jumlah_kredit - $jumlah_debit));
                        $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                        $rec->push([
                            'nama_akun'     => $nama_akun,
                            'tanggal'       => $tanggal,
                            'id_tsransaksi' => $id_tsransaksi,
                            'saldo_awal'    => rupiah((integer)$saldo_awal[$i]),
                            'jumlah_debit'  => rupiah((integer)$jumlah_debit),
                            'jumlah_kredit' => rupiah((integer)$jumlah_kredit),
                            'posisi_normal' => rupiah((integer)$posisi_normal),
                            'bersih'        => rupiah((integer)$bersih),
                            'saldo_akhir'   => rupiah((integer)$saldo_akhir[$i]),
                        ]);
                         $i++;
                   }
                }
            }
            return Datatables::of($rec)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->make(true);
      break;
    }
  }
  //PRINT PDF
  public function postPrint(Request $request){
   if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // 
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $request['tmukd']->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    $request['temp'] = $temp;
    switch ($request->types) {
      case 'bukti-jurnal-umum':
          $records = TransJurnal::select(DB::raw('DATE("tanggal") as date, coa_kode, SUM("jumlah") as jml, posisi, idtrans'))->groupBy('tmuk_kode','date', 'coa_kode', 'posisi', 'idtrans')
          ->whereIn('tmuk_kode', $temp)
          ->where('delete', 0);

        if($request->tanggal_start && $request->tanggal_end){
       
          $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }

        $record = $records->get(); 
        $temp = [];
        foreach ($record as $row) {
          $temp[]=[
            'date'     => $row->date,
            'coa_kode' => $row->coa_kode,
            'nama'     => $row->coa->nama,
            'debit'    => $row->posisi=='D'? rupiah($row->jml):'-',
            'kredit'   => $row->posisi=='K'? rupiah($row->jml):'-',
            'idtrans'  => $row->idtrans
          ];
        }
        $record = $temp;
        return $this->printPdf($request, $record);
      break;
      case 'ringkasan-buku-besar':
        $records = TransJurnal::with('coa')
                    ->whereIn('tmuk_kode', $temp)
                    ->where('delete', 0);
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $record = $records->orderBy('tanggal')->orderBy('coa_kode')->get();
                
        $i           = 1; 
        $saldo_awal  = [];
        $saldo_akhir = [];
        $temp        = [];
        $rec = new Collection;
        foreach ($record as $data){
            $tanggal         = \Carbon\Carbon::parse($data->tanggal)->format('Y-m-d');
            $coa             = $data->coa_kode.' - '. $data->coa->nama;
            $saldo_awal[$i]  = ($i == 1? 0 : $saldo_akhir[$i-1]);
            $jumlah_debit    = $data->posisi=='D' ? $data->jumlah : '0';
            $jumlah_kredit   = $data->posisi=='K' ? $data->jumlah : '0';
            $posisi_normal   = $data->coa->posisi;
            $bersih          = ($posisi_normal == 'D'?($jumlah_debit - $jumlah_kredit):($jumlah_kredit - $jumlah_debit));
            $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
            $temp[] = [
               'tanggal'       => $tanggal,
               'coa'           => $coa,
               'saldo_awal'    => rupiah((integer)$saldo_awal[$i]),
               'jumlah_debit'  => rupiah((integer)$jumlah_debit),
               'jumlah_kredit' => rupiah((integer)$jumlah_kredit),
               'posisi_normal' => rupiah((integer)$posisi_normal),
               'bersih'        => rupiah((integer)$bersih),
               'saldo_akhir'   => rupiah((integer)$saldo_akhir[$i])
            ];

         $i++;
        }    
        $record = $temp;
        return $this->printPdf($request, $record);
      break;
      case 'neraca-saldo':
            $records = Coa::with(['jurnal' => function($q) use ($temp, $request){
                    $q->whereIn('tmuk_kode', $temp);
                    if($request->tanggal_start && $request->tanggal_end){
                        $q->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
                    }
                }])->orderBy('kode');

            $record = $records->get();
            $i           = 1; 
            $saldo_awal  = [];
            $saldo_akhir = [];
            $temp        = [];
            $rec         = new Collection;
            foreach ($record as $data){
                $nama_akun       =  $data->kode.' - '.$data->nama;
                $saldo_awal[$i]  = ($i == 1? 0 :$saldo_akhir[$i-1]); 
                $jumlah_debit    = $data->jurnal->where('posisi', 'D')->sum('jumlah');
                $jumlah_kredit   = $data->jurnal->where('posisi', 'K')->sum('jumlah');
                $posisi_normal   = $data->posisi;
                $bersih          = ($posisi_normal == 'D' ?( $jumlah_debit - $jumlah_kredit):($jumlah_kredit - $jumlah_debit));
                $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                $temp[] = [
                    'nama_akun'     => $nama_akun,
                    'saldo_awal'    => rupiah((integer)$saldo_awal[$i]),
                    'jumlah_debit'  => rupiah((integer)$jumlah_debit),
                    'jumlah_kredit' => rupiah((integer)$jumlah_kredit),
                    'posisi_normal' => rupiah((integer)$posisi_normal),
                    'bersih'        => rupiah((integer)$bersih),
                    'saldo_akhir'   => rupiah((integer)$saldo_akhir[$i])
                ];
                $i++; 
            }
        $record = $temp;
        return $this->printPdf($request, $record);
        
        $this->printExcel($request, $data);
      break;
      case 'daftar-akun':
        $record = Coa::with('creator')
        ->whereHas('jurnal',  function($record) use ($request, $temp){
            $record->whereIn('tmuk_kode', $temp)
                   ->where('delete', 0);
        })
        ->select('*')->orderBy('kode');
        $record = $record->get();
        $temp = [];
        $i=1;
        foreach ($record as $row) {
          $temp[] =[
            'i'      => $i,
            'kode'   => $row->kode,
            'nama'   => $row->nama,
            'status' => $row->status==1?'Aktif':'Tidak Aktif',
          ];
        }
        $record = $temp;
        return $this->printPdf($request, $record);
      break;
      default:
          $record = Coa::with(['jurnal' => function($q) use ($temp, $request){
           $q->whereIn('tmuk_kode', $temp)->where('delete', 0);
                if($request->tanggal_start && $request->tanggal_end){
                    $q->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
                }
                $q = $q->orderBy('tanggal');
                return $q;
            }]);
            if($request->coa){
                    $record->where('kode', $request->coa);
            }
            $record->orderBy('kode', 'ASC');

            $record = $record->get();
            $temp = [];

            $rec = new Collection; 
            foreach ($record as $key => $row){
                if($row->jurnal->count() != 0){
                    $i           = 1; 
                    $saldo_awal = [];
                    $saldo_akhir = [];
                    foreach ($row->jurnal->sortBy('tanggal') as $key => $rw){
                        $nama_akun       = $rw->coa_kode.' - '.$rw->coa->nama;
                        $tanggal         = \Carbon\Carbon::parse($rw->tanggal)->format('Y-m-d');
                        $id_tsransaksi   = $rw->idtrans;
                        $jumlah_debit    = $rw->posisi=='D' ? $rw->jumlah : '0';
                        $jumlah_kredit   = $rw->posisi=='K' ? $rw->jumlah : '0';
                        $saldo_awal[$i]  = ($i == 1?0:$saldo_akhir[$i-1]);
                        $posisi_normal   = $row->posisi;
                        $bersih          = ($posisi_normal == 'D'?($jumlah_debit - $jumlah_kredit):($jumlah_kredit - $jumlah_debit));
                        $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                        $temp[] = [
                            'nama_akun'     => $nama_akun,
                            'tanggal'       => $tanggal,
                            'id_tsransaksi' => $id_tsransaksi,
                            'saldo_awal'    => rupiah((integer)$saldo_awal[$i]),
                            'jumlah_debit'  => rupiah((integer)$jumlah_debit),
                            'jumlah_kredit' => rupiah((integer)$jumlah_kredit),
                            'bersih'        => rupiah((integer)$bersih),
                            'saldo_akhir'   => rupiah((integer)$saldo_akhir[$i]),
                        ];
                         $i++;
                   }
                }
            }
        $record = $temp;
        return $this->printPdf($request, $record);
      break;
    }
  }

  //PRINT EXCEL
  public function postPrintExcel(Request $request){
    if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // 
    $tmukd = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $request['tmukd']->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }
    $request['temp'] = $temp;
    switch ($request->types) {
      case 'bukti-jurnal-umum':
          $records = TransJurnal::select(DB::raw('DATE("tanggal") as date, coa_kode, SUM("jumlah") as jml, posisi, idtrans'))->groupBy('tmuk_kode','date', 'coa_kode', 'posisi', 'idtrans')
          ->whereIn('tmuk_kode', $temp)
          ->where('delete', 0);

        if($request->tanggal_start && $request->tanggal_end){
       
          $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }

        $record = $records->get(); 
        $temp = [];
        $i=1;
        foreach ($record as $row) {
          $temp[]=[
            $i,
            'date'     => $row->date,
            'coa_kode' => $row->coa_kode,
            'nama'     => $row->coa->nama,
            'debit'    => $row->posisi=='D'? rupiah($row->jml):'-',
            'kredit'   => $row->posisi=='K'? rupiah($row->jml):'-',
            'idtrans'  => $row->idtrans
          ];
          $i++;
        }

        $data = [
          'tittle'     => 'Jurnal Umum',
          'tittle_set' => 'Jurnal Umum',
          'header_set' => array(
            'No',
            'Tanggal',
            'No Akun',
            'Nama Akun',
            'Debit',
            'Kredit',
            'ID Transaksi'
          ),
          'set_width'  => array('A'=>9,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp
        ];
        $this->printExcel($request, $data);
      break;
      case 'ringkasan-buku-besar':
        $records = TransJurnal::with('coa')
                    ->whereIn('tmuk_kode', $temp)
                    ->where('delete', 0);
        if($request->tanggal_start && $request->tanggal_end){
          $records->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        }
        $record = $records->orderBy('tanggal')->orderBy('coa_kode')->get();
                
        $i           = 1; 
        $saldo_awal  = [];
        $saldo_akhir = [];
        $temp        = [];
        $rec = new Collection;
        foreach ($record as $data){
            $tanggal         = \Carbon\Carbon::parse($data->tanggal)->format('Y-m-d');
            $coa             = $data->coa_kode.' - '. $data->coa->nama;
            $saldo_awal[$i]  = ($i == 1? 0 : $saldo_akhir[$i-1]);
            $jumlah_debit    = $data->posisi=='D' ? $data->jumlah : '0';
            $jumlah_kredit   = $data->posisi=='K' ? $data->jumlah : '0';
            $posisi_normal   = $data->coa->posisi;
            $bersih          = ($posisi_normal == 'D'?($jumlah_debit - $jumlah_kredit):($jumlah_kredit - $jumlah_debit));
            $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
            $temp[] = [
               $i,
               $tanggal,
               $coa,
               rupiah((integer)$saldo_awal[$i]),
               rupiah((integer)$jumlah_debit),
               rupiah((integer)$jumlah_kredit),
               rupiah((integer)$posisi_normal),
               rupiah((integer)$bersih),
               rupiah((integer)$saldo_akhir[$i])
            ];

         $i++;
        }    
        $data = [
          'tittle'     => 'Ringkasan Buku Besar',
          'tittle_set' => 'Ringkasan Buku Besar',
          'header_set' => array(
            'No',
            'Tanggal',
            'Coa',
            'Saldo Awal',
            'Jumlah Debit',
            'Jumlah Kredit',
            'Posisi Normal',
            'Bersih',
            'Saldo Akhir'
          ),
          'set_width'  => array('A'=>9,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp
        ];
        $this->printExcel($request, $data);
      break;
      case 'neraca-saldo':
            $records = Coa::with(['jurnal' => function($q) use ($temp, $request){
                    $q->whereIn('tmuk_kode', $temp);
                    if($request->tanggal_start && $request->tanggal_end){
                        $q->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
                    }
                }])->orderBy('kode');

            $record = $records->get();
            $i           = 1; 
            $saldo_awal  = [];
            $saldo_akhir = [];
            $temp        = [];
            $rec         = new Collection;
            foreach ($record as $data){
                $nama_akun       =  $data->kode.' - '.$data->nama;
                $saldo_awal[$i]  = ($i == 1? 0 :$saldo_akhir[$i-1]); 
                $jumlah_debit    = $data->jurnal->where('posisi', 'D')->sum('jumlah');
                $jumlah_kredit   = $data->jurnal->where('posisi', 'K')->sum('jumlah');
                $posisi_normal   = $data->posisi;
                $bersih          = ($posisi_normal == 'D' ?( $jumlah_debit - $jumlah_kredit):($jumlah_kredit - $jumlah_debit));
                $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                $temp[] = [
                    $i,
                    'nama_akun'     => $nama_akun,
                    'saldo_awal'    => rupiah((integer)$saldo_awal[$i]),
                    'jumlah_debit'  => rupiah((integer)$jumlah_debit),
                    'jumlah_kredit' => rupiah((integer)$jumlah_kredit),
                    'posisi_normal' => rupiah((integer)$posisi_normal),
                    'bersih'        => rupiah((integer)$bersih),
                    'saldo_akhir'   => rupiah((integer)$saldo_akhir[$i])
                ];
                $i++; 
            }
        $data = [
          'tittle'     => 'Neraca Saldo',
          'tittle_set' => 'Neraca Saldo',
          'header_set' => array(
              'No',
              'Nama Akun',
              'Saldo Awal',
              'Jumlah Debit',
              'Jumlah Kredit',
              'Posisi Normal',
              'Bersih',
              'Saldo Akhir'
          ),
          'set_width'  => array('A'=>9,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp
        ];
        $this->printExcel($request, $data);
      break;
      case 'daftar-akun':
        $record = Coa::with('creator')
        ->whereHas('jurnal',  function($record) use ($request, $temp){
            $record->whereIn('tmuk_kode', $temp)
                   ->where('delete', 0);
        })
        ->select('*')->orderBy('kode');
        $record = $record->get();
        $temp = [];
        $i=1;
        foreach ($record as $row) {
          $temp[] =[
            $i,
            $row->kode,
            $row->nama,
            $row->status==1?'Aktif':'Tidak Aktif',
          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Daftar COA',
          'tittle_set' => 'Daftar COA',
          'header_set' => array(
            '# ',
            'No Akun',
            'Nama Akun',
            'Status'
          ),
          'set_width'  => array('A'=>9,'B'=>20,'C'=>20, 'D'=>40),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp
        ];
        $this->printExcel($request, $data);
      break;
      default:
          
          $record = Coa::with(['jurnal' => function($q) use ($temp, $request){
           $q->whereIn('tmuk_kode', $temp)->where('delete', 0);
                if($request->tanggal_start && $request->tanggal_end){
                    $q->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
                }
                $q = $q->orderBy('tanggal');
                return $q;
            }]);
            if($request->coa){
                    $record->where('kode', $request->coa);
            }
            $record->orderBy('kode', 'ASC');

            $record = $record->get();
            $temp = [];

            $rec = new Collection; 
            foreach ($record as $key => $row){
                if($row->jurnal->count() != 0){
                    $i           = 1; 
                    $saldo_awal = [];
                    $saldo_akhir = [];
                    foreach ($row->jurnal->sortBy('tanggal') as $key => $rw){
                        $nama_akun       = $rw->coa_kode.' - '.$rw->coa->nama;
                        $tanggal         = \Carbon\Carbon::parse($rw->tanggal)->format('Y-m-d');
                        $id_tsransaksi   = $rw->idtrans;
                        $jumlah_debit    = $rw->posisi=='D' ? $rw->jumlah : '0';
                        $jumlah_kredit   = $rw->posisi=='K' ? $rw->jumlah : '0';
                        $saldo_awal[$i]  = ($i == 1?0:$saldo_akhir[$i-1]);
                        $posisi_normal   = $row->posisi;
                        $bersih          = ($posisi_normal == 'D'?($jumlah_debit - $jumlah_kredit):($jumlah_kredit - $jumlah_debit));
                        $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;
                        $temp[] = [
                            $i,
                            'nama_akun'     => $nama_akun,
                            'tanggal'       => $tanggal,
                            'id_tsransaksi' => $id_tsransaksi,
                            'saldo_awal'    => rupiah((integer)$saldo_awal[$i]),
                            'jumlah_debit'  => rupiah((integer)$jumlah_debit),
                            'jumlah_kredit' => rupiah((integer)$jumlah_kredit),
                            'bersih'        => rupiah((integer)$bersih),
                            'saldo_akhir'   => rupiah((integer)$saldo_akhir[$i]),
                        ];
                         $i++;
                   }
                }
            }
        $data = [
          'tittle'     => 'Jurnal per Akun',
          'tittle_set' => 'Jurnal per Akun',
          'header_set' => array(
            'No',
            'Nama Akun',
            'Tanggal',
            'ID Transaksi',
            'Saldo Awal',
            'Perubahan Debit',
            'Perubahan Kredit',
            'Perubahan Bersih',
            'Saldo Akhir'
          ),
          'set_width'  => array('A'=>5,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp
        ];
        $this->printExcel($request, $data);
      break;
    }
  }

  // GENERATE LAPORAN pdf
  public function printPdf($request, $record=''){
    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $request['tmukd']->first()->lsi->region->area;
    }
    if($request->lsi_id){
      $lsi          = $request['tmukd']->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $request['tmukd']->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'saldo'   => isset($request['ledger'])?$request['ledger']:[],
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];
    
    $pdf = \PDF::loadView('report.laporan-pusat.akun-buku-besar.'.$request->types, $data);
    return $pdf->setOption('margin-bottom', 5)
    ->setPaper('a4')
    ->setOption('margin-top', 5)
    ->setOption('margin-left', 3)
    ->setOption('margin-right', 3)
    ->inline();
  }

  //GENERATE LAPORAN EXCEL
  public function printExcel($request='', $data=[]){
    
    $Export = Excel::create(isset($data['tittle'])?$data['tittle']:'default', function($excel) use ($request, $data){
        $excel->setTitle(isset($data['tittle'])?$data['tittle']:'default');
        $excel->setCreator(isset($data['creator'])?$data['creator']:'Aplikasi Akuntansi')->setCompany('Lotte');
        $excel->setDescription('Export');

        $excel->sheet(isset($data['tittle'])?$data['tittle']:'default', function($sheet) use ($request, $data){
        if(isset($data['set_width'])){
            $tray = array_keys($data['set_width']);
            $last_cell = end($tray);
        }
        $sheet->row(1, array(
            isset($data['tittle_set'])?$data['tittle_set']:'default'
        ));

        $sheet->row(2, array(
          'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
        ));
        $sheet->row(3, isset($data['header_set'])?$data['header_set']:[]);

        $row = 4;
        $no =1;
        foreach ($data['record'] as $elm) {
            $sheet->row($row, $elm);
          $row++;
        }
        $sheet->cells('E4'.':'.$last_cell.''.$row, function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
        });
        $sheet->setBorder('A3:'.$last_cell.''.$row, 'thin');
        $sheet->mergeCells('A1:'.$last_cell.'1');
        $sheet->mergeCells('A2:'.$last_cell.'2');
        $sheet->cells('A1:A1', function($cells){
          $cells->setFontSize(14);
          $cells->setFontWeight('bold');
          $cells->setAlignment('center');
        });
        $sheet->cells('A2:'.$last_cell.'2', function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
        });
        $sheet->cells('A3:'.$last_cell.'3', function($cells){
          $cells->setAlignment('center');
          $cells->setBackground('#999999');
          $cells->setFontColor('#FFFFFF');
        });
        $sheet->setWidth(isset($data['set_width'])?$data['set_width']:[]);
        $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->setWrapText(true);
        $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top')
        );
        $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'left')
        );
        $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'right')
        );
        $sheet->getStyle('E4:E'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'right')
        );

        });
    });
    $Export ->download('xls');
  }

}

