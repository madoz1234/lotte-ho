<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransAkuisisiAset;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Region;
use Carbon\Carbon;
use Datatables;

class LaporanPusatAktivaTetapController extends Controller
{
    protected $link = 'laporan-pusat/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Pusat");   
    }

  // public function postGridTemp(Request $request){
  //   $this->setLink($this->link.'akun-utang-usaha');
  //   switch ($request->types) {
  //     case 'laporan-pembayaran':
  //       $data = [
  //         'lists_grid' => $request->types,
  //       ];
  //       return $this->render('modules.laporan.laporan-pusat.lists.akun-utang-usaha.'.$request->types, $data);
  //     break;
  //     case 'saldo-pemasok':
  //       $data = [
  //         'lists_grid' => $request->types,
  //       ];
  //       return $this->render('modules.laporan.laporan-pusat.lists.akun-utang-usaha.'.$request->types, $data);
  //     break;
  //     default:
  //       $data = [
  //         'lists_grid' => $request->types,
  //       ];
  //       return $this->render('modules.laporan.laporan-pusat.lists.akun-utang-usaha.'.$request->types, $data);
  //     break;
  //   }
  // }

  public function postGridTemp(Request $request){
    //query temp
    $this->setLink($this->link.'aktiva-tetap');
    switch ($request->types) {
      case 'aktiva-tetap':
        $data = [
          'lists_grid' => 'aktiva-tetap',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.aktiva-tetap.aktiva-tetap', $data);
      break;
      case 'asset-listing':
        $data = [
          'lists_grid' => 'asset-listing',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.aktiva-tetap.asset-listing', $data);
      break;
      case 'aktiva-tetap-per-tipe':
        $data = [
          'lists_grid' => 'aktiva-tetap-per-tipe',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.aktiva-tetap.aktiva-tetap-per-tipe', $data);
      break;
    }
  }

  public function postGridAll(Request $request){
    // dd($request->All());

    if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }
    $result= $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    //query temp
    switch ($request->types) {
      case 'aktiva-tetap':

        $record = TransAkuisisiAset::from(\DB::raw("(SELECT
                  trans_akuisisi_aset.tmuk_kode,
                  trans_akuisisi_aset.nama,
                  trans_akuisisi_aset.tanggal_pembelian,
                  trans_akuisisi_aset.nilai_pembelian,
                  ref_tipe_aset.tingkat_depresiasi / 12 AS umur_ekonomis,
                  trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi AS penyusutan_perbulan,
                  trans_akuisisi_aset.nilai_pembelian - (trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi) AS nilai_buku
                  FROM trans_akuisisi_aset
                  JOIN ref_tipe_aset ON trans_akuisisi_aset.tipe_id = ref_tipe_aset.id)  d"));

        $record->whereIn('tmuk_kode',  $temp);
        // dd($record);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal_pembelian)'), array($request->tanggal_start, $request->tanggal_end));
        }

        // dd($record->get());
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('tmuk_kode', function ($record) {
            return $record->tmuk->nama ;
        })
        ->addColumn('unit', function ($record) {
            return '1' ;
        })
        ->addColumn('nilai_pembelian', function ($record) {
            return rupiah($record->nilai_pembelian);
        })
        ->addColumn('penyusutan_perbulan', function ($record) {
            return rupiah($record->penyusutan_perbulan);
        })
        ->addColumn('nilai_buku', function ($record) {
            return rupiah($record->nilai_buku);
        })


        ->make(true);
      break;

      case 'asset-listing':
        $record = TransAkuisisiAset::select('*')

        ->whereIn('tmuk_kode',  $temp);
        // dd($record);
        // if($request->tanggal_start and $request->tanggal_end){
        //   $record->whereBetween(\DB::raw('DATE(tanggal_pembelian)'), array($request->tanggal_start, $request->tanggal_end));
        // }

        // dd($record->get());
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })

        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama ;
        })
        ->addColumn('tipe', function ($record) {
            if($record->tipe_id == NULL ){
              return '-' ;
            }else{
              return $record->tipeaset->tipe ;
            }
        })
        ->addColumn('nilai_pembelian', function ($record) {
            return rupiah($record->nilai_pembelian) ;
        })
        ->make(true);
      break;
      case 'aktiva-tetap-per-tipe':
        $record = TransJurnal::whereIn('coa_kode', ['1.2.1.0','1.2.2.0','1.2.3.0','1.2.4.0','1.2.5.0','1.2.6.0','1.2.7.0'])->select('*')

        ->whereIn('tmuk_kode',  $temp)
        ->where('delete', 0);
        // dd($record);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        }

        // dd($record->get());
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })

        ->addColumn('nama', function ($record) {
            return $record->coa->nama ;
        })
        ->addColumn('jumlah', function ($record) {
            return rupiah($record->jumlah) ;
        })
        ->addColumn('akumulasi', function ($record) {
            return '' ;
        })
        ->addColumn('depresi', function ($record) {
            return '' ;
        })
        ->addColumn('book', function ($record) {
            return '' ;
        })

        ->make(true);
      break;
      
    }
  }

  //PRINT PDF
  public function postPrint(Request $request){
   if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // dd($request->all());
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $request['tmukd']->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    $request['temp'] = $temp;
    switch ($request->types) {
      case 'aktiva-tetap':

      $rangkuman = TransAkuisisiAset::from(\DB::raw("(SELECT
                  trans_akuisisi_aset.tmuk_kode,
                  trans_akuisisi_aset.nama,
                  trans_akuisisi_aset.tanggal_pembelian,
                  trans_akuisisi_aset.nilai_pembelian,
                  ref_tipe_aset.tingkat_depresiasi / 12 AS umur_ekonomis,
                  trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi AS penyusutan_perbulan,
                  trans_akuisisi_aset.nilai_pembelian - (trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi) AS nilai_buku
                  FROM trans_akuisisi_aset
                  JOIN ref_tipe_aset ON trans_akuisisi_aset.tipe_id = ref_tipe_aset.id)  d"));

      $rangkuman->whereIn('tmuk_kode',  $temp);
        // dd($record);
        if($request->tanggal_start and $request->tanggal_end){
          $rangkuman->whereBetween(\DB::raw('DATE(tanggal_pembelian)'), array($request->tanggal_start, $request->tanggal_end));
        }



      $record = $rangkuman->get();


      // $this->aktivatetap($request);
      return $this->printPdf($request, $record);
      break;
      case 'asset-listing':

      $rangkuman = TransAkuisisiAset::select('*');

      $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
      // if($request->tanggal_start and $request->tanggal_end){
      //   $rangkuman->whereBetween(\DB::raw('tanggal_pembelian'), array($request->tanggal_start, $request->tanggal_end));
      // }

      $record = $rangkuman->get();


      return $this->printPdf($request, $record);
      break;
      case 'aktiva-tetap-per-tipe':

      $rangkuman = TransJurnal::whereIn('coa_kode', ['1.2.1.0','1.2.2.0','1.2.3.0','1.2.4.0','1.2.5.0','1.2.6.0','1.2.7.0'])->select('*');

      $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp)->where('delete', 0);
      if($request->tanggal_start and $request->tanggal_end){
        $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
      }

      $record = $rangkuman->get();
      // dd($record);
      return $this->printPdf($request, $record);
      break;
    }
  }

  // GENERATE LAPORAN pdf
  public function printPdf($request, $record=''){
    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $request['tmukd']->first()->lsi->region->area;
    }
    if($request->lsi_id){
      $lsi          = $request['tmukd']->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $request['tmukd']->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'saldo'   => isset($request['ledger'])?$request['ledger']:[],
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];
    
    $pdf = \PDF::loadView('report.laporan-pusat.aktiva-tetap.'.$request->types, $data);
    return $pdf->setOption('margin-bottom', 5)
    ->setOption('margin-top', 5)
    ->setOption('margin-left', 3)
    ->setOption('margin-right', 3)
    ->inline();
  }



  // public function aktivatetap($request)
  // {
  //   $rangkuman = TransAkuisisiAset::from(\DB::raw("(SELECT
  //                 trans_akuisisi_aset.tmuk_kode,
  //                 trans_akuisisi_aset.nama,
  //                 trans_akuisisi_aset.tanggal_pembelian,
  //                 trans_akuisisi_aset.nilai_pembelian,
  //                 ref_tipe_aset.tingkat_depresiasi / 12 AS umur_ekonomis,
  //                 trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi AS penyusutan_perbulan,
  //                 trans_akuisisi_aset.nilai_pembelian - (trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi) AS nilai_buku
  //                 FROM trans_akuisisi_aset
  //                 JOIN ref_tipe_aset ON trans_akuisisi_aset.tipe_id = ref_tipe_aset.id)  d"));

  //   // $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
  //   // if($request->tanggal_start and $request->tanggal_end){
  //   //   $rangkuman->whereBetween(\DB::raw('tanggal_pembelian'), array($request->tanggal_start, $request->tanggal_end));
  //   // }

  //   $record = $rangkuman->get();
  //   // dd($record);

  //   $data         = $record;
  //   $region       ='All';
  //   $lsi          ='All';
  //   $tmuk         ='All';
  //   $req          = $request->all();
  //   if($request->region_id){
  //     $region       = $request['tmukd']->first()->lsi->region->area;
  //   }
  //   if($request->lsi_id){
  //   // dd($result_tmuk);
  //     $lsi          = $request['tmukd']->first()->lsi->nama;
  //   }
  //   if($request->tmuk_kode){
  //     $tmuk         = $request['tmukd']->first()->nama;
  //   }

  //   $tahun_fiskal = TahunFiskal::getTahunFiskal();

  //   $data = [
  //     'records' => $record,
  //     'request' => $req,
  //     'fiskal'  => $tahun_fiskal,
  //     'region'  => $region,
  //     'lsi'     => $lsi,
  //     'tmuk'    => $tmuk,
  //   ];

  //   $content = $this->render('report.laporan-pusat.aktiva-tetap.aktiva-tetap', $data);
  //   // return $content;

  //   HTML2PDF($content, $data);
  // }


  
  // public function assetListing($request)
  // {
  //   $rangkuman = TransAkuisisiAset::select('*');

  //   $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
  //   if($request->tanggal_start and $request->tanggal_end){
  //     $rangkuman->whereBetween(\DB::raw('tanggal_pembelian'), array($request->tanggal_start, $request->tanggal_end));
  //   }

  //   $record = $rangkuman->get();
  //   // dd($record);

  //   $data         = $record;
  //   $region       ='All';
  //   $lsi          ='All';
  //   $tmuk         ='All';
  //   $req          = $request->all();
  //   if($request->region_id){
  //     $region       = $request['tmukd']->first()->lsi->region->area;
  //   }
  //   if($request->lsi_id){
  //   // dd($result_tmuk);
  //     $lsi          = $request['tmukd']->first()->lsi->nama;
  //   }
  //   if($request->tmuk_kode){
  //     $tmuk         = $request['tmukd']->first()->nama;
  //   }

  //   $tahun_fiskal = TahunFiskal::getTahunFiskal();

  //   $data = [
  //     'records' => $record,
  //     'request' => $req,
  //     'fiskal'  => $tahun_fiskal,
  //     'region'  => $region,
  //     'lsi'     => $lsi,
  //     'tmuk'    => $tmuk,
  //   ];

  //   $content = $this->render('report.laporan-pusat.aktiva-tetap.asset-listing', $data);
  //   // return $content;

  //   HTML2PDF($content, $data);
  // }

  // public function aktivatetappertipe($request)
  // {
  //   $rangkuman = TransJurnal::whereIn('coa_kode', ['1.2.1.0','1.2.2.0','1.2.3.0','1.2.4.0','1.2.5.0','1.2.6.0','1.2.7.0'])->select('*');

  //   $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
  //   if($request->tanggal_start and $request->tanggal_end){
  //     $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
  //   }

  //   $record = $rangkuman->get();
  //   // dd($record);

  //   $data         = $record;
  //   $region       ='All';
  //   $lsi          ='All';
  //   $tmuk         ='All';
  //   $req          = $request->all();
  //   if($request->region_id){
  //     $region       = $request['tmukd']->first()->lsi->region->area;
  //   }
  //   if($request->lsi_id){
  //   // dd($result_tmuk);
  //     $lsi          = $request['tmukd']->first()->lsi->nama;
  //   }
  //   if($request->tmuk_kode){
  //     $tmuk         = $request['tmukd']->first()->nama;
  //   }

  //   $tahun_fiskal = TahunFiskal::getTahunFiskal();

  //   $data = [
  //     'records' => $record,
  //     'request' => $req,
  //     'fiskal'  => $tahun_fiskal,
  //     'region'  => $region,
  //     'lsi'     => $lsi,
  //     'tmuk'    => $tmuk,
  //   ];

  //   $content = $this->render('report.laporan-pusat.aktiva-tetap.aktiva-tetap-per-tipe', $data);
  //   // return $content;

  //   HTML2PDF($content, $data);
  // }

 



  //PRINT EXCEL
  public function postPrintExcel(Request $request){
    // dd($request->All());
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }
    $request['temp'] = $temp;
    switch ($request->types) {
      case 'aktiva-tetap':
        //   $record = TransJurnal::select('*')
        //   ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
        //   ->where('delete', 0)
        //   ->orderBy('tanggal')->whereIn('tmuk_kode',  $temp);
        // if($request->tanggal_start and $request->tanggal_end){
        //   $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
        // }
        // $record = $record->get();
        $record = TransAkuisisiAset::from(\DB::raw("(SELECT
                  trans_akuisisi_aset.tmuk_kode,
                  trans_akuisisi_aset.nama,
                  trans_akuisisi_aset.tanggal_pembelian,
                  trans_akuisisi_aset.nilai_pembelian,
                  ref_tipe_aset.tingkat_depresiasi / 12 AS umur_ekonomis,
                  trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi AS penyusutan_perbulan,
                  trans_akuisisi_aset.nilai_pembelian - (trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi) AS nilai_buku
                  FROM trans_akuisisi_aset
                  JOIN ref_tipe_aset ON trans_akuisisi_aset.tipe_id = ref_tipe_aset.id)  d"))->whereIn('tmuk_kode',  $temp);

        

        if($request->tanggal_start and $request->tanggal_end){
          $start    = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
          $end      = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
          $record->whereBetween(\DB::raw('DATE(tanggal_pembelian)'), array($start, $end));
        }

        $record = $record->get();
        // dd($record);

        $temp = [];
        $i=1;
        foreach ($record as $row) {
          $temp[] = [
              $i,
              $row->tmuk->nama,
              $row->nama,
              $row->tanggal_pembelian,
              '1',
              $row->nilai_pembelian,
              $row->nilai_pembelian,
              $row->umur_ekonomis,
              $row->penyusutan_perbulan,
              $row->nilai_buku,

          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Daftar Aktiva Tetap ',
          'tittle_set' => 'Daftar Aktiva Tetap ',
          'header_set' => array(
            '#',
            'TMUK',
            'Keterangan',
            'Tanggal Perolehan',
            'Unit',
            'Harga',
            'Jumlah',
            'Umur Ekonomis',
            'Penyusutan perBulan',
            'Nilai Buku',
          ),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>40, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
        break;
        case 'aktiva-tetap-per-tipe':


        $record = TransJurnal::whereIn('coa_kode', ['1.2.1.0','1.2.2.0','1.2.3.0','1.2.4.0','1.2.5.0','1.2.6.0','1.2.7.0'])->select('*')
        ->whereIn('tmuk_kode',  $temp)->where('delete', 0);

        if($request->tanggal_start and $request->tanggal_end){
          $start  = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
          $end    = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');

          $record->whereBetween(\DB::raw('DATE(tanggal)'), array($start, $end));
        }

        $record = $record->get();
        // dd($record);

        $temp = [];
        $i=1;
        foreach ($record as $row) {
          $temp[] = [
              $i,
              $row->coa->nama,
              $row->jumlah,
              '',
              '',
              '',
              $row->tanggal,


          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Daftar Aktiva Tetap per Tipe ',
          'tittle_set' => 'Daftar Aktiva Tetap per Tipe ',
          'header_set' => array(
            '#',
            'Nama Aktiva',
            'Harga Perolehan',
            'Akumulasi Depresiasi',
            'Book Value',
            'Depresiasi Tahun Ini',
            'Tanggal Pembelian',
          ),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>40, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
        break;

        case 'asset-listing':
        $record = TransAkuisisiAset::select('*')

        ->whereIn('tmuk_kode',  $temp);
        // dd($record);
        // if($request->tanggal_start and $request->tanggal_end){
        //   $record->whereBetween(\DB::raw('DATE(tanggal_pembelian)'), array($request->tanggal_start, $request->tanggal_end));
        // }

        $record = $record->get();
        // dd($record);

        $temp = [];
        $i=1;
        foreach ($record as $row) {
          $temp[] = [
              $i,
              $row->tmuk->nama,
              $row->tipe_id == NULL ? '-' : $row->tipeaset->tipe,
              $row->nama,
              $row->no_seri,
              $row->tanggal_pembelian,
              $row->nilai_pembelian,


          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Asset Listing ',
          'tittle_set' => 'Asset Listing ',
          'header_set' => array(
            '#',
            'Nama Aktiva',
            'Harga Perolehan',
            'Akumulasi Depresiasi',
            'Book Value',
            'Depresiasi Tahun Ini',
            'Tanggal Pembelian',
          ),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>40, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
        break;
    }
  }


   // //GENERATE LAPORAN EXCEL
  public function printExcel($request='', $data=[]){
    
    $Export = Excel::create(isset($data['tittle'])?$data['tittle']:'default', function($excel) use ($request, $data){
    $excel->setTitle(isset($data['tittle'])?$data['tittle']:'default');
    $excel->setCreator(isset($data['creator'])?$data['creator']:'Aplikasi Akuntansi')->setCompany('Lotte');
    $excel->setDescription('Export');

    $excel->sheet(isset($data['tittle'])?$data['tittle']:'default', function($sheet) use ($request, $data){
    $tray = array_keys($data['set_width']);
    $last_cell = end($tray);
    $sheet->row(1, array(
        isset($data['tittle_set'])?$data['tittle_set']:'default'
    ));

    $sheet->row(2, array(
      'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
    ));
    $sheet->row(3, isset($data['header_set'])?$data['header_set']:[]);

    $row = 4;
    $no =1;
    foreach ($data['record'] as $elm) {
        // dd($elm);
        $sheet->row($row, $elm);
      $row++;
    }
    $sheet->cells('E4'.':'.$last_cell.''.$row, function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->setBorder('A3:'.$last_cell.''.$row, 'thin');
    $sheet->mergeCells('A1:'.$last_cell.'1');
    $sheet->mergeCells('A2:'.$last_cell.'2');
    $sheet->cells('A1:A1', function($cells){
      $cells->setFontSize(14);
      $cells->setFontWeight('bold');
      $cells->setAlignment('center');
    });
    $sheet->cells('A2:'.$last_cell.'2', function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->cells('A3:'.$last_cell.'3', function($cells){
      $cells->setAlignment('center');
      $cells->setBackground('#999999');
      $cells->setFontColor('#FFFFFF');
    });
    $sheet->setWidth(isset($data['set_width'])?$data['set_width']:[]);
    $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->setWrapText(true);
    $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top')
    );
    $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top', 'horizontal' => 'center')
    );

    });
    });
    $Export ->download('xls');
  }

}


