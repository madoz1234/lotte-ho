<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransSO;
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Master\HargaTmuk;
use Lotte\Models\Master\TahunFiskal;
use Illuminate\Support\Collection;
use Lotte\Models\Master\Produk;


use Datatables;
use Carbon\Carbon;

class LaporanPusatPersediaanController extends Controller
{
    protected $link = 'laporan-pusat/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Pusat");   
    }

  public function postGridTemp(Request $request){
    // dd($request->all());
    $this->setLink($this->link.'persediaan');
    switch ($request->types) {
      case 'pergerakan-persediaan':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.persediaan.'.$request->types, $data);
      break;
      case 'stok-opname':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.persediaan.'.$request->types, $data);
      break;
      case 'hpp-map':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.persediaan.'.$request->types, $data);
      break;
      case 'umur-persediaan':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.persediaan.'.$request->types, $data);
      break;
      default:
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.persediaan.'.$request->types, $data);
      break;
    }
  }

  public function postGridAll(Request $request){
    if($request->tanggal_start){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }
    $result= $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    switch ($request->types) {
      case 'pergerakan-persediaan':
        $record = ProdukTmuk::with(['tmuk', 'produk.produksetting'])
        ->whereIn('tmuk_kode', $temp);

        if($request->tanggal_start){
          $record->where(\DB::raw('date(updated_at)'), $request->tanggal_start);
        }
        $record->orderBy('updated_at', 'ASC');
        $record = $record;
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('kode_produk', function ($record) {
            return $record->produk->kode;
        })
        ->addColumn('nama_produk', function ($record) {
            return $record->produk->nama;
        })
        ->addColumn('barcode_1', function ($record) {
            return $record->produk->produksetting->uom1_barcode;
        })
        ->addColumn('barcode_2', function ($record) {
            return $record->produk->produksetting->uom2_barcode;
        })
        ->addColumn('tanggal_pergerakan_stok', function ($record) {
            return Carbon::parse($record->updated_at)->format('Y-m-d');
        })
        ->addColumn('stok_awal', function ($record) {
            $data = $record->stock_awal == ''? number_format($record->stock_akhir):number_format($record->stock_awal);
            return number_format($data);
        })
        ->addColumn('stok_akhir', function ($record) {
            $data = $record->stock_akhir?number_format($record->stock_akhir):number_format($record->stock_awal);
            return $data;
        })
        ->make(true);
      break;
      case 'stok-opname':
        $record = TransSO::with('tmuk.lsi');
        $record->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start){
          $record->where(\DB::raw('date(created_at)'), $request->tanggal_start);
        }
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->addColumn('tgl_so', function ($record) {
            return Carbon::parse($record->tgl_so)->format('d/m/Y');
        })
        ->addColumn('qty_aktual', function ($record) {
            return number_format($record->qty_aktual);
        })
        ->addColumn('qty_system', function ($record) {
            return number_format($record->qty_system);
        })
        ->addColumn('selisih', function ($record) {
            return number_format($record->selisih);
        })
        ->make(true);
      break;
      case 'hpp-map':
        $record = HargaTmuk::with(['produk.produksetting', 'po_detail']);
        if($request->tanggal_start){
          $record->where(\DB::raw('DATE(created_at)'), $request->tanggal_start);
        }
        $record->whereIn('tmuk_kode', $temp);
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('kode_produk', function ($record) {
            return $record->produk_kode;
        })
        ->addColumn('nama_produk', function ($record) {
            return $record->produk->nama;
        })
        ->addColumn('uom2_satuan', function ($record) {
            return $record->produk->produksetting->uom2_satuan;
        })
        ->addColumn('map', function ($record) {
            return number_format(round($record->map));
        })
        ->make(true);
      break;
      case 'umur-persediaan':
        $record = Produk::with('stock')->whereHas('stock', function($q) use ($temp){
                    $q->whereIn('tmuk_kode', $temp)
                      ->where('qty', '>', 0);
                });
        $record = $record->get();
        $temp = [];
        $rec = new Collection;
        foreach ($record as $row) {
          $rec->push([
              'kode_produk'      => $row->kode,
              'deskripsi_produk' => $row->nama,
              'total_qty'        => $row->getTotalStock(),
              'Qty1'             => number_format($row->getStockByDay(1)),
              'qty31'            => number_format($row->getStockByDay(31)),
              'qty61'            => number_format($row->getStockByDay(61)),
              'qty91'            => number_format($row->getStockByDay(91)),
              'qty120'           => number_format($row->getStockByDay(120)),
          ]);
        }
        // dd($rec);
       return Datatables::of($rec)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->make(true);
      break;
      default:
        // $record = ProdukTmuk::with('produk','produksetting','hargaJual','detail')
        // ->whereHas('detail', function($q) use ($request){
        //     if ($request->tanggal_start) {
        //       $q->where('date', $request->tanggal_start);
        //     }
        //   });
        // $record->whereIn('tmuk_kode', $temp);


        $record = ProdukTmuk::with(['detail' => function($q) use ($request){
                                  if ($request->tanggal_start) {
                                      $q->where('date', $request->tanggal_start);
                                    }
                              }], 'produk', 'produksetting')
                                      ->orderBy('updated_at', 'ASC');


        $record->whereIn('tmuk_kode', $temp)->get();



        // dd($record->toSql());
       return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('category_1', function ($record) {
            return isset($record->produk->l1_nm) ? $record->produk->l1_nm : '-';
        })
        ->addColumn('category_2', function ($record) {
            return isset($record->produk->l2_nm) ? $record->produk->l2_nm : '-';
        })
        ->addColumn('kode_produk', function ($record) {
            return $record->produk->kode;
        })
        ->addColumn('nama_produk', function ($record) {
            return $record->produk->nama;
        })
        ->addColumn('uom', function ($record) {
            return isset($record->produksetting->uom1_satuan) ? $record->produksetting->uom1_satuan : '-';
        })
        ->addColumn('qty', function ($record) {
            $qty = $record->detail()->first()->qty;
            return number_format($qty);
        })
        ->addColumn('cost_price', function ($record) {
            $map = $record->detail()->first()->hpp;
            return number_format($map);
        })
        ->addColumn('jumlah', function ($record) {
          return number_format($record->detail()->first()->nilai_persediaan);
        })
        ->make(true);
      break;
    }
  }

  //PRINT PDF
  public function postPrint(Request $request){
    if($request->tanggal_start){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // dd($request->all());
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $tmukd->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    $request['temp'] = $temp;
    switch ($request->types) {
      case 'pergerakan-persediaan':
        $record = ProdukTmuk::with(['tmuk', 'produk.produksetting'])
        ->whereIn('tmuk_kode', $temp);

        if($request->tanggal_start){
          $record->where(\DB::raw('date(updated_at)'), $request->tanggal_start);
        }
        $record = $record->orderBy('updated_at', 'ASC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'stok-opname':
        $record = TransSO::with('tmuk.lsi');
        $record->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start){
          $record->where(\DB::raw('date(created_at)'), $request->tanggal_start);
        }
        $record = $record->get();
        return $this->printPdf($request, $record);
      break;
      case 'hpp-map':
        $record = HargaTmuk::with(['produk.produksetting', 'po_detail']);
        if($request->tanggal_start){
          $record->where(\DB::raw('DATE(created_at)'), $request->tanggal_start);
        }
        $record->whereIn('tmuk_kode', $temp);
        $record = $record->get();
        return $this->printPdf($request, $record);
      break;
      case 'umur-persediaan':
        $record = Produk::with('stock')->whereHas('stock', function($q) use ($temp){
                    $q->whereIn('tmuk_kode', $temp)
                      ->where('qty', '>', 0);
                });
        $record = $record->get();
        $temp = [];
        $rec = new Collection;
        $i = 1;
        foreach ($record as $row) {
          $temp[] = [
              'no'      => $i,
              'kode_produk'      => $row->kode,
              'deskripsi_produk' => $row->nama,
              'total_qty'        => $row->getTotalStock(),
              'Qty1'             => number_format($row->getStockByDay(1)),
              'qty31'            => number_format($row->getStockByDay(31)),
              'qty61'            => number_format($row->getStockByDay(61)),
              'qty91'            => number_format($row->getStockByDay(91)),
              'qty120'           => number_format($row->getStockByDay(120)),
          ];
          $i++;
        }
        $record = $temp;
        return $this->printPdf($request, $record);
      break;
      default:
        $record = ProdukTmuk::with('produk','produksetting','hargaJual','detail')
        ->whereHas('produksetting',  function($r){
          $r->where('tipe_produk',1);
        })
        ->whereHas('detail', function($q) use ($request){
            if ($request->tanggal_start) {
              $q->where('date','=', $request->tanggal_start);
            }
          });
        $record = $record->whereIn('tmuk_kode', $temp);
        $temp = [];
        foreach ($record->get() as $row) {
          if($row->produk->l1_nm !==NULL){
            $temp[$row->tmuk_kode.'-'.$row->tmuk->nama][$row->produk->l1_nm][] =[
              'tmuk_kode'   => $row->tmuk_kode,
              'category_1'  => isset($row->produk->l1_nm) ? $row->produk->l1_nm : '-',
              'category_2'  => isset($row->produk->l2_nm) ? $row->produk->l2_nm : '-',
              'kode_produk' => $row->produk->kode,
              'nama_produk' => $row->produk->nama,
              'uom'         => isset($row->produksetting->uom1_satuan) ? $row->produksetting->uom1_satuan : '-',
              'qty'         => $row->detail()->first()->qty,
              'cost_price'  => $row->detail()->first()->hpp,
              'jumlah'      => $row->detail()->first()->nilai_persediaan
            ];
          }
        }
        $record = $temp;
        return $this->printPdf($request, $record);
      break;
    }
  }

  //PRINT EXCEL
  public function postPrintExcel(Request $request){
    if($request->tanggal_start){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }
    $request['temp'] = $temp;

    switch ($request->types) {      
      case 'pergerakan-persediaan':
        $record = ProdukTmuk::with(['tmuk', 'produk.produksetting'])
        ->whereIn('tmuk_kode', $temp);

        if($request->tanggal_start){
          $record->where(\DB::raw('date(updated_at)'), $request->tanggal_start);
        }
        $record = $record->orderBy('updated_at', 'ASC')->take(100)->get();
        // dd($record->toArray());
        $temp = [];
        $i=1;
        foreach ($record as $row) {
          $temp[] = [
                $i,
                $row->produk->kode,
                $row->produk->nama,
                $row->produk->produksetting->uom1_barcode,
                $row->produk->produksetting->uom2_barcode,
                \Carbon\Carbon::parse($row->updated_at)->format('Y-m-d'),
                $row->stock_awal == ''?number_format($row->stock_akhir):number_format($row->stock_awal),
                $row->stock_akhir?number_format($row->stock_akhir):number_format($row->stock_awal)
          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Rekap Pergerakan Persediaan',
          'tittle_set' => 'Rekap Pergerakan Persediaan',
          'header_set' => array(
              'Kode Produk',
              'Nama Produk',
              'Barcode 1',
              'Barcode 2',
              'Tanggal Pergerakan Stok',
              'Stok Awal',
              'Stok Akhir'
          ),
          'set_width'  => array('A'=>9,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'stok-opname':
        $record = TransSO::with('tmuk.lsi');
        $record->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start){
          $record->where(\DB::raw('date(created_at)'), $request->tanggal_start);
        }
        $record = $record->get();
        $temp=[];
        $i =1;
        foreach ($record as $row) {
          $temp[]= [
            $i,
           $row->tmuk->lsi->nama,
           $row->tmuk->nama,
           \Carbon\Carbon::parse($row->tgl_so)->format('d/m/Y'),
           number_format($row->qty_aktual),
           number_format($row->qty_system),
           number_format($row->selisih)
          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Rekap Stok Opname',
          'tittle_set' => 'Rekap Stok Opname',
          'header_set' => array(
              'No',
              'LSI TMUK',
              'Tanggal',
              'Stok Opname',
              'Qty Aktual',
              'Qty System',
              'Selisih'
          ),
          'set_width'  => array('A'=>9,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'hpp-map':
        $record = HargaTmuk::with(['produk.produksetting', 'po_detail']);
        if($request->tanggal_start){
          $record->where(\DB::raw('DATE(created_at)'), $request->tanggal_start);
        }
        $record->whereIn('tmuk_kode', $temp);
        $record = $record->get()->take(100);
        $temp = [];
        $i =1;
        foreach ($record as $row) {
          $temp[] = [
                $i,
                $row->produk_kode,
                $row->produk->nama,
                $row->produk->produksetting->uom2_satuan,
                number_format(round($row->map))
          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Rekap HPP (MAP)',
          'tittle_set' => 'Rekap HPP (MAP)',
          'header_set' => array(
            '#',
            'Kode Produk',
            'Nama Produk',
            'UOM HPP (Rp)'
          ),
          'set_width'  => array('A'=>9,'B'=>20,'C'=>20, 'D'=>40, 'E'=>40),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'umur-persediaan':
        $record = Produk::with('stock')->whereHas('stock', function($q) use ($temp){
                    $q->whereIn('tmuk_kode', $temp)
                      ->where('qty', '>', 0);
                });
        $record = $record->get();
        $temp = [];
        $rec = new Collection;
        $i = 1;
        foreach ($record as $row) {
          $temp[] = [
              'no'      => $i,
              'kode_produk'      => $row->kode,
              'deskripsi_produk' => $row->nama,
              'total_qty'        => $row->getTotalStock(),
              'Qty1'             => number_format($row->getStockByDay(1)),
              'qty31'            => number_format($row->getStockByDay(31)),
              'qty61'            => number_format($row->getStockByDay(61)),
              'qty91'            => number_format($row->getStockByDay(91)),
              'qty120'           => number_format($row->getStockByDay(120)),
          ];
          $i++;
        }
        $data = [
          'tittle'     => 'Rekap Umur Persediaan',
          'tittle_set' => 'Rekap Umur Persediaan',
          'header_set' => array(
            '#',
            'Kode Produk',
            'Deskripsi Produk',
            'Total Qty',
            'Qty (1-30 hari)',
            'Qty (31-60 hari)',
            'Qty (61-90 hari)',
            'Qty (91-120 hari)',
            'Qty >120 hari'
          ),
          'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'H'=>20, 'I'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      default:
            $record = ProdukTmuk::with('produk','produksetting','hargaJual','detail')
            ->whereHas('produksetting',  function($r){
              $r->where('tipe_produk',1);
            })
            ->whereHas('detail', function($q) use ($request){
                if ($request->tanggal_start) {
                  $q->where('date', $request->tanggal_start);
                }
              });
            $record = $record->whereIn('tmuk_kode', $temp)->take(100);
            $record = $record->get();
            $temp = [];
            foreach ($record as $row) {
                    if($row->produk->l1_nm !==NULL){
                    $temp[$row->tmuk_kode.'-'.$row->tmuk->nama][$row->produk->l1_nm][] =[
                      'tmuk_kode'   => $row->tmuk_kode,
                      'kode_produk' => $row->produk->kode,
                      'nama_produk' => $row->produk->nama,
                      'uom'         => isset($row->produksetting->uom1_satuan) ? $row->produksetting->uom1_satuan : '-',
                      'qty'         => $row->detail()->first()->qty,
                      'cost_price'  => $row->detail()->first()->hpp,
                      'jumlah'      => $row->detail()->first()->nilai_persediaan
                    ];
                }
        }
        $dat = [];
        foreach ($temp as $key=>$row){
          $dat[] = [$key,'','','','',''];
          foreach ($row as $keys => $elm){
              $jum_total = 0;
              foreach ($elm as $child){
                $dat[] = [$child['kode_produk'],$child['nama_produk'],$child['uom'],$child['qty'],$child['cost_price'],number_format($child['jumlah'])];
                $jum_total += $child['jumlah'];
              }
              $dat[] = ['SUB TOTAL '.$keys,'','','','',number_format($jum_total)];
          }
        }
        // dd($dat);
        $data = [
          'tittle'     => 'Laporan Nilai Persediaan',
          'tittle_set' => 'Laporan Nilai Persediaan',
          'header_set' => array('Kode Produk','Nama Produk','UOM','Qty','HPP (MAP)','Jumlah'),
          'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $dat,
        ];
        $this->printExcel($request, $data);
      break;
    }
  }

  // GENERATE LAPORAN pdf
  public function printPdf($request, $record=''){
    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $request['tmukd']->first()->lsi->region->area;
    }
    if($request->lsi_id){
      $lsi          = $request['tmukd']->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $request['tmukd']->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'saldo'   => isset($request['ledger'])?$request['ledger']:[],
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];
    
    $pdf = \PDF::loadView('report.laporan-pusat.persediaan.'.$request->types, $data);
    return $pdf->setOption('margin-bottom', 5)
    ->setPaper('a4')
    ->setOption('margin-top', 5)
    ->setOption('margin-left', 3)
    ->setOption('margin-right', 3)
    ->inline();
  }

  // //GENERATE LAPORAN EXCEL
  public function printExcel($request='', $data=[]){
    
    $Export = Excel::create(isset($data['tittle'])?$data['tittle']:'default', function($excel) use ($request, $data){
    $excel->setTitle(isset($data['tittle'])?$data['tittle']:'default');
    $excel->setCreator(isset($data['creator'])?$data['creator']:'Aplikasi Akuntansi')->setCompany('Lotte');
    $excel->setDescription('Export');

    $excel->sheet(isset($data['tittle'])?$data['tittle']:'default', function($sheet) use ($request, $data){
    $tray = array_keys($data['set_width']);
    $last_cell = end($tray);
    $sheet->row(1, array(
        isset($data['tittle_set'])?$data['tittle_set']:'default'
    ));

    $sheet->row(2, array(
      'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
    ));
    $sheet->row(3, isset($data['header_set'])?$data['header_set']:[]);

    $row = 4;
    $no =1;
    foreach ($data['record'] as $elm) {
        // dd($elm);
        $sheet->row($row, $elm);
      $row++;
    }
    $sheet->cells('E4'.':'.$last_cell.''.$row, function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->setBorder('A3:'.$last_cell.''.$row, 'thin');
    $sheet->mergeCells('A1:'.$last_cell.'1');
    $sheet->mergeCells('A2:'.$last_cell.'2');
    $sheet->cells('A1:A1', function($cells){
      $cells->setFontSize(14);
      $cells->setFontWeight('bold');
      $cells->setAlignment('center');
    });
    $sheet->cells('A2:'.$last_cell.'2', function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->cells('A3:'.$last_cell.'3', function($cells){
      $cells->setAlignment('center');
      $cells->setBackground('#999999');
      $cells->setFontColor('#FFFFFF');
    });
    $sheet->setWidth(isset($data['set_width'])?$data['set_width']:[]);
    $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->setWrapText(true);
    $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top')
    );
    $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top', 'horizontal' => 'center')
    );

    });
    });
    $Export ->download('xls');
  }

}


