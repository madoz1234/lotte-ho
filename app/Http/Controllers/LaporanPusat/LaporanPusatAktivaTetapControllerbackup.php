<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransAkuisisiAset;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Region;
use Carbon\Carbon;


class LaporanPusatAktivaTetapControllerbackup extends Controller
{

  public function postGridTemp(Request $request){
    // dd($request->all());
    $reg = Tmuk::with('lsi','lsi.region');

    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }

    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }

    $result= $reg->get();

    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    //query temp
    switch ($request->types) {
      case 'aktiva-tetap':
        $records = TransAkuisisiAset::select('*');

        if($request->start != '')
        {
            $records->where('created_at', '>=', Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d H:i:s'));
        }

        if($request->end != '')
        {
            $records->where('created_at', '<=', Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d H:i:s'));
        }

        $records->whereIn(\DB::raw('tmuk_kode'),  $temp);
        $records = $records->get();

        $data = [
          'detail' => $records
        ];

      return $this->render('modules.laporan.laporan-pusat.lists.aktiva-tetap.aktiva-tetap', $data);

      break;
      case 'aktiva-tetap-per-tipe':
      $records = TransJurnal::whereIn('coa_kode', ['1.2.1.0','1.2.2.0','1.2.3.0','1.2.4.0','1.2.5.0','1.2.6.0','1.2.7.0'])->select('*');

      if($request->start != '')
      {
          $records->where('tanggal', '>=', Carbon::createFromFormat('d/m/Y', $request->start)->startOfDay()->format('Y-m-d H:i:s'));
      }

      if($request->end != '')
      {
          $records->where('tanggal', '<=', Carbon::createFromFormat('d/m/Y', $request->end)->endOfDay()->format('Y-m-d H:i:s'));
      }

      $records->whereIn(\DB::raw('tmuk_kode'),  $temp);
      $records = $records->get();

      $data = [
        'detail' => $records
      ];

      return $this->render('modules.laporan.laporan-pusat.lists.aktiva-tetap.aktiva-tetap-per-tipe', $data);
      break;
      default:
      break;
    }
  }
  //PRINT PDF
  public function postPrint(Request $request){
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // dd($request->all());
    $tmukd = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $tmukd->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    $request['temp'] = $temp;
    switch ($request->types) {
      case 'penjualan-mingguan':
      $this->mingguan($request);
      break;
      case 'penjualan-bulanan':
      $this->bulanan($request);
      break;
      case 'penjualan-per-barang':
      $this->perbarang($request);
      break;
      default:
      $this->harian($request);
      break;
    }
  }

  //PRINT EXCEL
  public function postPrintExcel(Request $request){
    $reg = Tmuk::with('lsi','lsi.region');

    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // dd($request->all());
    $tmukd = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $tmukd->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }
    $request['temp'] = $temp;
    switch ($request->types) {
      case 'aktiva-tetap':
      $this->excelTetap($request);
      break;
      case 'aktiva-tetap-per-tipe':
      $this->excelPertipe($request);
      break;
      default:
      break;
    }
  }

  // GENERATE LAPORAN pdf
  public function harian($request){

    $rangkuman = TransRekapPenjualanTmuk::getDataHarian();
    $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
    if($request->start and $request->end){
      $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end));
    }

    $record = $rangkuman->get();

    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $tmukd->first()->lsi->region->area;
    }
    if($request->lsi_id){
    // dd($result_tmuk);
      $lsi          = $tmukd->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $tmukd->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];

    $content = $this->render('report.laporan-pusat.penjualan.penjualan-harian', $data);
    // return $content;

    HTML2PDF($content, $data);
  }

  public function mingguan($request=''){
    $rangkuman = TransRekapPenjualanTmuk::getDataMingguan($request->start, $request->end);
    $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
    // if($request->start and $request->end){
    //   $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end));
    // }

    $record = $rangkuman->get();
    // dd($record->toArray());

    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $tmukd->first()->lsi->region->area;
    }

    if($request->lsi_id){
      $lsi          = $tmukd->first()->lsi->nama;
    }

    if($request->tmuk_kode){
      $tmuk         = $tmukd->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];

    $content = $this->render('report.laporan-pusat.penjualan.penjualan-mingguan', $data);
    // return $content;
    HTML2PDF($content, $data);
  }

  public function bulanan($request=''){
    $rangkuman = TransRekapPenjualanTmuk::getDataBulanan($request->start, $request->end);
    $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
    // if($request->start and $request->end){
    //   $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end));
    // }

    $record = $rangkuman->get();

    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $tmukd->first()->lsi->region->area;
    }

    if($request->lsi_id){
      $lsi          = $tmukd->first()->lsi->nama;
    }

    if($request->tmuk_kode){
      $tmuk         = $tmukd->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];

    $content = $this->render('report.laporan-pusat.penjualan.penjualan-bulanan', $data);
    // return $content;
    HTML2PDF($content, $data);
  }

  public function perbarang($request=''){
    $rangkuman = TransRekapPenjualanTmukDetail::getDataPerBarang($request->start, $request->end);
    $rangkuman->whereIn(\DB::raw('tmuk_kod'),  $request->temp);
    // if($request->start and $request->end){
    //   $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end));
    // }

    $record = $rangkuman->get();
    // dd($record->toArray());

    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $tmukd->first()->lsi->region->area;
    }

    if($request->lsi_id){
      $lsi          = $tmukd->first()->lsi->nama;
    }

    if($request->tmuk_kode){
      $tmuk         = $tmukd->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];

    $content = $this->render('report.laporan-pusat.penjualan.penjualan-per-barang', $data);
    // return $content;
    HTML2PDF($content, $data);
  }

  //GENERATE LAPORAN EXCEL
  public function excelTetap($request=''){
    // dd($request->all());
    $record = TransAkuisisiAset::select('*');

    if($request->start != '')
    {
        $record->where('tanggal_pembelian', '>=', Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d'));
    }

    if($request->end != '')
    {
        $record->where('tanggal_pembelian', '<=', Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d'));
    }

    $record->whereIn(\DB::raw('tmuk_kode'),  $request->temp);

    $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();

    $data =[];

    $req            = $request->all();

    $Export = Excel::create('Aktiva Tetap ', function($excel) use ($request, $record){
    $excel->setTitle('Rekap Aktiva Tetap');
    $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
    $excel->setDescription('Export');

    $excel->sheet('Rekap Aktiva Tetap', function($sheet) use ($request, $record){
    $sheet->row(1, array(
      'Rekap Aktiva Tetap'
      ));

    $sheet->row(3, array(
        'No',
        'TMUK',
        'Tipe Aset',
        'Nama Aset',
        'Nomor Seri',
        'Tanggal Pembelian',
        'Nilai Pembelian'
      ));

      $row = 4;
      $no =1;
      foreach ($record as $val) {
          $sheet->row($row, array(
              $no,
              $val->tmuk->nama,
              $val->tipe->tipe,
              $val->nama,
              $val->no_seri,
              $val->tanggal_pembelian,
              $val->nilai_pembelian,
              ));
          $row=$row;
          $row++;$no++;
      }
      $sheet->cells('E4'.':F'.$row, function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
      });
      $sheet->setBorder('A3:G'.$row, 'thin');
      $sheet->mergeCells('A1:G1');
      $sheet->mergeCells('A2:G2');
      $sheet->cells('A1:A1', function($cells){
          $cells->setFontSize(14);
          $cells->setFontWeight('bold');
          $cells->setAlignment('center');
      });
      $sheet->cells('A2:B2', function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
      });
      $sheet->cells('A3:G3', function($cells){
          $cells->setAlignment('center');
          $cells->setBackground('#999999');
          $cells->setFontColor('#FFFFFF');
      });
      $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20,'D'=>50,'E'=>30,'F'=>20, 'G'=>20));
      $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
      $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top')
      );
      $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'center')
      );

      });
    });
    $Export ->download('xls');
  }
  public function excelPertipe($request=''){
    // dd($request->all());
    $record = TransJurnal::select('*')->whereIn('coa_kode', ['1.2.1.0','1.2.2.0','1.2.3.0','1.2.4.0','1.2.5.0','1.2.6.0','1.2.7.0']);

    if($request->start != '')
    {
        $record->where('tanggal', '>=', Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d'));
    }

    if($request->end != '')
    {
        $record->where('tanggal', '<=', Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d'));
    }

    $record->whereIn(\DB::raw('tmuk_kode'),  $request->temp);

    $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();

    $data =[];

    $req            = $request->all();

    $Export = Excel::create('Aktiva Tetap Per Tipe ', function($excel) use ($request, $record){
    $excel->setTitle('Rekap Aktiva Tetap Per Tipe');
    $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
    $excel->setDescription('Export');

    $excel->sheet('Rekap Aktiva Tetap Per Tipe', function($sheet) use ($request, $record){
    $sheet->row(1, array(
      'Rekap Aktiva Tetap Per Tipe'
      ));

    $sheet->row(3, array(
        'No',
        'Nama Aktiva',
        'Harga Perolehan',
        'Akumulasi Depresiasi',
        'Book Value',
        'Depresiasi Tahun Ini',
        'Tanggal Pembelian'
      ));

      $row = 4;
      $no =1;
      foreach ($record as $val) {
          $sheet->row($row, array(
              $no,
              $val->coa->nama,
              $val->jumlah,
              '',
              '',
              '',
              $val->tanggal,
              ));
          $row=$row;
          $row++;$no++;
      }
      $sheet->cells('E4'.':G'.$row, function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
      });
      $sheet->setBorder('A3:G'.$row, 'thin');
      $sheet->mergeCells('A1:G1');
      $sheet->mergeCells('A2:G2');
      $sheet->cells('A1:A1', function($cells){
          $cells->setFontSize(14);
          $cells->setFontWeight('bold');
          $cells->setAlignment('center');
      });
      $sheet->cells('A2:G2', function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
      });
      $sheet->cells('A3:G3', function($cells){
          $cells->setAlignment('center');
          $cells->setBackground('#999999');
          $cells->setFontColor('#FFFFFF');
      });
      $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20));
      $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
      $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top')
      );
      $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'center')
      );

      });
    });
    $Export ->download('xls');
  }

}
