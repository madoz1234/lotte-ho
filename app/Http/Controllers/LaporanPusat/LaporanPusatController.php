<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Carbon\Carbon;
use Excel;

// Models
// use Lotte\Models\Trans\TransJurnal;
// use Lotte\Models\Trans\TransAkuisisiAset;
// use Lotte\Models\Master\Tmuk;
// use Lotte\Models\Master\TahunFiskal;
// use Lotte\Models\Master\Region;

class LaporanPusatController extends Controller
{
    protected $link = 'laporan-pusat/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Pusat");
    }

    public function getAktivaTetap(Request $request)
    {
        $this->setLink($this->link.'aktiva-tetap');
        $this->setTitle("Aktiva Tetap","");
        $this->setSubtitle("&nbsp");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Pusat' => '#']);
        $data = [
            'menus' => 'aktiva-tetap',
        ];

        return $this->render('modules.laporan.laporan-pusat.index', $data);
    }

    public function getAkunBukuBesar(Request $request)
    {
        $this->setLink($this->link.'akun-buku-besar');
        $this->setTitle("Akun Buku Besar","");
        $this->setSubtitle("&nbsp");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Pusat' => '#']);
        $data = [
            'menus' => 'akun-buku-besar',
        ];

        return $this->render('modules.laporan.laporan-pusat.index', $data);
    }

    public function getAkunKasBank(Request $request)
    {
        $this->setLink($this->link.'arus-kas-bank');
        $this->setTitle("Arus Kas Bank","");
        $this->setSubtitle("&nbsp");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Pusat' => '#']);
        $data = [
            'menus' => 'arus-kas-bank',
            'types' => 'arus-kas-bank',
        ];

        return $this->render('modules.laporan.laporan-pusat.index', $data);
    }


    public function getAkunUtangUsaha(Request $request)
    {

        $this->setLink($this->link.'akun-utang-usaha');
        $this->setTitle("Akun Utang Usaha","");
        $this->setSubtitle("&nbsp");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Pusat' => '#']);
        $data = [
            'menus' => 'akun-utang-usaha',
            'types' => 'akun-utang-usaha',
        ];

        return $this->render('modules.laporan.laporan-pusat.index', $data);
    }

    public function getAkunPiutangMember(Request $request)
    {
        $this->setLink($this->link.'akun-piutang-member');
        $this->setTitle("Akun Piutang Member","");
        $this->setSubtitle("&nbsp");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Pusat' => '#']);
        $data = [
            'menus' => 'akun-piutang-member',
            'types' => 'rekap-piutang-member',
        ];

        return $this->render('modules.laporan.laporan-pusat.index', $data);
    }

    public function getKeuangan(Request $request)
    {
        $this->setLink($this->link.'keuangan');
        $this->setTitle("Keuangan","");
        $this->setSubtitle("&nbsp");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Pusat' => '#']);
        $data = [
            'menus'  => 'keuangan',
            'types'  => 'keuangan',
            'months' => true,
            'view_list' => true,
        ];

        return $this->render('modules.laporan.laporan-pusat.index', $data);
    }

    public function getPembelian(Request $request)
    {
        $this->setLink($this->link.'pembelian');
        $this->setTitle("Pembelian","");
        $this->setSubtitle("&nbsp");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Pusat' => '#']);
        $data = [
            'menus' => 'pembelian',
            'types' => 'rekap-pembelian-persediaan',
        ];

        return $this->render('modules.laporan.laporan-pusat.index', $data);
    }

    public function getPenjualan(Request $request)
    {
        $this->setLink($this->link.'penjualan');
        $this->setTitle("Penjualan","");
        $this->setSubtitle("&nbsp");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Pusat' => '#']);
        $data = [
            'menus' => 'penjualan',
            'types' => 'all-penjualan',
        ];

        return $this->render('modules.laporan.laporan-pusat.index', $data);
    }

    public function getPersediaan(Request $request)
    {
        $this->setLink($this->link.'persediaan');
        $this->setTitle("Persediaan","");
        $this->setSubtitle("&nbsp");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Pusat' => '#']);
        $data = [
            'menus' => 'persediaan',
            'types' => 'nilai-persediaan',
            'priode' => true,
        ];

        return $this->render('modules.laporan.laporan-pusat.index', $data);
    }

}
