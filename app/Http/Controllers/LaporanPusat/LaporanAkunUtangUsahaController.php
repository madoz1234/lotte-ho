<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\Trans\TransMuatanDetail;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransRetur;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransRekapGr;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransJurnal;

use Datatables;
use Carbon\Carbon;

class LaporanAkunUtangUsahaController extends Controller
{
    protected $link = 'laporan-pusat/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Pusat");   
    }

  public function postGridTemp(Request $request){
    //query temp
    $this->setLink($this->link.'akun-utang-usaha');
    switch ($request->types) {
      case 'laporan-pembayaran':
        $data = [
          'lists_grid' => 'laporan-pembayaran',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-utang-usaha.laporan-pembayaran', $data);
      break;
      case 'saldo-pemasok':
        $data = [
          'lists_grid' => 'saldo-pemasok',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-utang-usaha.saldo-pemasok', $data);
      break;
      case 'analysis-umur-pemasok':
        $data = [
          'lists_grid' => 'analysis-umur-pemasok',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.akun-utang-usaha.analysis-umur-pemasok', $data);
      break;
      
    }
  }

    public function postGridAll(Request $request){
      // dd($request->All());

      if($request->tanggal_start && $request->tanggal_end){
          $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
          $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
      }
      $reg = Tmuk::with('lsi','lsi.region');
      if($request->lsi_id){
        $reg->whereHas('lsi',  function($ql) use($request){
          $ql->where('id', $request->lsi_id);
        });
      }
      if($request->region_id){
        $reg->whereHas('lsi.region', function($qr) use($request){
          $qr->where('id', $request->region_id);
        });
      }
      if($request->tmuk_kode){
        $reg->where('id', $request->tmuk_kode);
      }
      $result= $reg->get();
      $temp=[];

      foreach ($result as $val){
        $temp[] = $val->kode;
      }

      //query temp
      switch ($request->types) {
        case 'laporan-pembayaran':
          $record = TransPyr::select('*')
            ->where('status',2)
            ->orderBy('vendor_lokal','asc')
            ->orderBy('tgl_jatuh_tempo','asc')

          ->whereIn('tmuk_kode',  $temp);
          if($request->tanggal_start and $request->tanggal_end){
            $record->whereBetween(\DB::raw('DATE(tgl_jatuh_tempo)'), array($request->tanggal_start, $request->tanggal_end));
          }

          // dd($record->get());
          return Datatables::of($record)
          ->addColumn('num', function ($record) use ($request) {
              return $request->get('start');
          })
          ->addColumn('vendor_lokal', function ($record) {
            if($record->vendor_lokal == 100){
              return $record->tmuk->nama ;
            }else if($record->vendor_lokal == 0){
              return $record->tmuk->lsi->nama ;
            }else{
              return $record->vendorlokal->nama;
            }
          })
          ->addColumn('tipe', function ($record) {
              return 'Supplier Invoice' ;
          })
          ->addColumn('vendor', function ($record) {
              return $record->urutan($record->id) ;
          })
          ->addColumn('total', function ($record) {
              return rupiah($record->total);
          })
          ->make(true);
        break;
        case 'saldo-pemasok':
          $record = TransPyr::from(\DB::raw("(SELECT
              trans_jurnal.coa_kode,
              trans_jurnal.tanggal,
              trans_pyr.tgl_buat,
              trans_pyr.vendor_lokal,
              trans_jurnal.tmuk_kode,
              trans_jurnal.idtrans as id_jurnal,
              trans_pyr.nomor_pyr,
              trans_jurnal.jumlah as charges_jurnal,
              trans_jurnal.deskripsi,
              trans_jurnal.posisi,
              trans_pyr_pembayaran.verifikasi4_user
              FROM trans_pyr
              LEFT JOIN trans_jurnal ON trans_pyr.id_transaksi = trans_jurnal.idtrans
              LEFT JOIN trans_pyr_pembayaran ON trans_pyr.nomor_pyr = trans_pyr_pembayaran.nomor_pyr )  d"))->where('coa_kode', '2.1.1.0')->whereIn('tmuk_kode',  $temp);

            
          if($request->tanggal_start and $request->tanggal_end){
            $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
          }

    
          return Datatables::of($record)
          ->addColumn('num', function ($record) use ($request) {
              return $request->get('start');
          })
          ->addColumn('vendor', function ($record) {

            // return $record->vendor_lokal;


            if($record->vendor_lokal == 100){
              return $record->tmuk->nama ;
            }else if($record->vendor_lokal == 0){
              return $record->tmuk->lsi->nama ;
            }else{
              return $record->vendorlokal->nama;
            }
          })
          ->addColumn('tgl_buat', function ($record) {
              return $record->deskripsi == 'Pembayaran PyR - Trade Non Lotte' ? '' : $record->tanggal ;
          })
          ->addColumn('debit', function ($record) {
              return $record->posisi == 'K' ? '0': rupiah($record->charges_jurnal) ;
          })
          ->addColumn('kredit', function ($record) {
              return $record->posisi == 'D' ? '0': rupiah($record->charges_jurnal) ;
          })
          ->addColumn('allocated', function ($record) {
              return $record->verifikasi4_user == NULL ? '0': rupiah($record->charges_jurnal);
          })
          ->addColumn('outstanding', function ($record) {
              return $record->verifikasi4_user == NULL ? rupiah($record->charges_jurnal) : '0';
          })

          ->make(true);
        break;

        case 'analysis-umur-pemasok':
          $record = TransPyr::select('*')
                              ->whereIn('tmuk_kode',  $temp)
                              ->orderBy('vendor_lokal','asc')
                              ->orderBy('tgl_jatuh_tempo','asc');
                 
          if($request->tanggal_start and $request->tanggal_end){
            $record->whereBetween(\DB::raw('DATE(tgl_jatuh_tempo)'), array($request->tanggal_start, $request->tanggal_end));
          }

          return Datatables::of($record)
          ->addColumn('num', function ($record) use ($request) {
              return $request->get('start');
          })
          ->addColumn('deskripsi', function ($record) {
              return 'Supplier Invoice' ;
          })
          ->addColumn('current', function ($record) {
              return '0' ;
          })
          ->addColumn('jumlah_satu', function ($record) {
                    $i=0;
                    $vendor='';

                   if($i==0){
                              $vendor = $record->vendor_lokal;
                          }else{
                              if($record->vendor_lokal!=$vendor){
                                  $i=0;
                                  $vendor = $record->vendor_lokal;
                              }
                          }

                          $st  = date_create(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$record->created_at)->format('Y-m-d'));
                          $en    = date_create();
                          $diff   = date_diff( $st, $en );
                          $day = $diff->days; 
              return $day <= 30 ? rupiah(isset($record->detailPyr) ? $record->detailPyr->sum('price') : 0) : '0'  ;
          })
          ->addColumn('jumlah_dua', function ($record) {
                    $i=0;
                    $vendor='';

                   if($i==0){
                              $vendor = $record->vendor_lokal;
                          }else{
                              if($record->vendor_lokal!=$vendor){
                                  $i=0;
                                  $vendor = $record->vendor_lokal;
                              }
                          }

                          $st  = date_create(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$record->created_at)->format('Y-m-d'));
                          $en    = date_create();
                          $diff   = date_diff( $st, $en );
                          $day = $diff->days;  
              return $day > 30 && $day <= 60 ? rupiah(isset($record->detailPyr) ? $record->detailPyr->sum('price') : 0) : '0'  ;
          })
          ->addColumn('jumlah_tiga', function ($record) {
                    $i=0;
                    $vendor='';

                   if($i==0){
                              $vendor = $record->vendor_lokal;
                          }else{
                              if($record->vendor_lokal!=$vendor){
                                  $i=0;
                                  $vendor = $record->vendor_lokal;
                              }
                          }

                          $st  = date_create(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$record->created_at)->format('Y-m-d'));
                          $en    = date_create();
                          $diff   = date_diff( $st, $en );
                          $day = $diff->days;      
              return $day > 60 ? rupiah(isset($record->detailPyr) ? $record->detailPyr->sum('price') : 0) : '0'  ;
          })
          ->addColumn('balance', function ($record) {
              return rupiah(isset($record->detailPyr) ? $record->detailPyr->sum('price') : 0) ;
          })
          ->make(true);
        break;
      }
    }

    //PRINT PDF
    public function postPrint(Request $request){
     if($request->tanggal_start && $request->tanggal_end){
          $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
          $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
      }
      $reg = Tmuk::with('lsi','lsi.region');
      if($request->lsi_id){
        $reg->whereHas('lsi',  function($ql) use($request){
          $ql->where('id', (integer)$request->lsi_id);
        });
      }
      if($request->region_id){
        $reg->whereHas('lsi.region', function($qr) use($request){
          $qr->where('id', (integer)$request->region_id);
        });
      }
      if($request->tmuk_kode){
        $reg->where('id', (integer)$request->tmuk_kode);
      }
      // dd($request->all());
      $request['tmukd'] = clone  $reg;

      $result = $reg->get();

      // $result_tmuk = $request['tmukd']->get();
      $temp=[];

      foreach ($result as $val){
        $temp[] = $val->kode;
      }

      $request['temp'] = $temp;
      switch ($request->types) {
        case 'saldo-pemasok':

        $rangkuman = TransPyr::from(\DB::raw("(SELECT
        trans_jurnal.coa_kode,
        trans_jurnal.tanggal,
        trans_pyr.tgl_buat,
        trans_pyr.vendor_lokal,
        trans_jurnal.tmuk_kode as tmuk_jurnal,
        trans_jurnal.idtrans as id_jurnal,
        trans_pyr.nomor_pyr,
        trans_jurnal.jumlah as charges_jurnal,
        trans_jurnal.deskripsi,
        trans_jurnal.posisi
        FROM trans_pyr
        LEFT JOIN trans_jurnal ON trans_pyr.id_transaksi = trans_jurnal.idtrans
        LEFT JOIN trans_pyr_pembayaran ON trans_pyr.nomor_pyr = trans_pyr_pembayaran.nomor_pyr )  d"))->where('coa_kode', '2.1.1.0');
        $rangkuman->whereIn(\DB::raw('tmuk_jurnal'),  $request->temp);
        if($request->tanggal_start and $request->tanggal_end){
          $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
        }

        $record = $rangkuman->get();
        return $this->printPdf($request, $record);

        break;
        case 'laporan-pembayaran':
          $rangkuman = TransPyr::select('*')
                              ->where('status',2)
                              ->orderBy('vendor_lokal','asc')
                              ->orderBy('tgl_jatuh_tempo','asc');


          $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
          if($request->tanggal_start and $request->tanggal_end){
            $rangkuman->whereBetween(\DB::raw('tgl_buat'), array($request->tanggal_start, $request->tanggal_end));
          }

          $record = $rangkuman->get();


          return $this->printPdf($request, $record);
        break;
        case 'analysis-umur-pemasok':
          $rangkuman = TransPyr::select('*')
                              ->orderBy('vendor_lokal','asc')
                              ->orderBy('tgl_jatuh_tempo','asc');


          $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
          if($request->tanggal_start and $request->tanggal_end){
            $rangkuman->whereBetween(\DB::raw('tgl_jatuh_tempo'), array($request->tanggal_start, $request->tanggal_end));
          }

          $record = $rangkuman->get();


          return $this->printPdf($request, $record);
        break;
      }
    }

    // GENERATE LAPORAN pdf
  public function printPdf($request, $record=''){
    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $request['tmukd']->first()->lsi->region->area;
    }
    if($request->lsi_id){
      $lsi          = $request['tmukd']->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $request['tmukd']->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'saldo'   => isset($request['ledger'])?$request['ledger']:[],
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];
    
    $pdf = \PDF::loadView('report.laporan-pusat.akun-utang-usaha.'.$request->types, $data);
    return $pdf->setOption('margin-bottom', 5)
    ->setOption('margin-top', 5)
    ->setOption('margin-left', 3)
    ->setOption('margin-right', 3)
    ->inline();
  }





    // public function saldoPemasok($request){
    //  $rangkuman = TransPyr::from(\DB::raw("(SELECT
    //     trans_jurnal.coa_kode,
    //     trans_jurnal.tanggal,
    //     trans_pyr.tgl_buat,
    //     trans_pyr.vendor_lokal,
    //     trans_jurnal.tmuk_kode as tmuk_jurnal,
    //     trans_jurnal.idtrans as id_jurnal,
    //     trans_pyr.nomor_pyr,
    //     trans_jurnal.jumlah as charges_jurnal,
    //     trans_jurnal.deskripsi,
    //     trans_jurnal.posisi
    //     FROM trans_pyr
    //     LEFT JOIN trans_jurnal ON trans_pyr.id_transaksi = trans_jurnal.idtrans
    //     LEFT JOIN trans_pyr_pembayaran ON trans_pyr.nomor_pyr = trans_pyr_pembayaran.nomor_pyr )  d"))->where('coa_kode', '2.1.1.0');
    //   $rangkuman->whereIn(\DB::raw('tmuk_jurnal'),  $request->temp);
    //   if($request->tanggal_start and $request->tanggal_end){
    //     $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
    //   }

    //   $record = $rangkuman->get();

    //   $data         = $record;
    //   $region       ='All';
    //   $lsi          ='All';
    //   $tmuk         ='All';
    //   $req          = $request->all();
    //   if($request->region_id){
    //     $region       = $request['tmukd']->first()->lsi->region->area;
    //   }
    //   if($request->lsi_id){
    //   // dd($result_tmuk);
    //     $lsi          = $request['tmukd']->first()->lsi->nama;
    //   }
    //   if($request->tmuk_kode){
    //     $tmuk         = $request['tmukd']->first()->nama;
    //   }

    //   $tahun_fiskal = TahunFiskal::getTahunFiskal();

    //   $data = [
    //     'records' => $record,
    //     'request' => $req,
    //     'fiskal'  => $tahun_fiskal,
    //     'region'  => $region,
    //     'lsi'     => $lsi,
    //     'tmuk'    => $tmuk,
    //   ];

    //   $content = $this->render('report.laporan-pusat.akun-utang-usaha.saldo-pemasok', $data);
    //   // return $content;

    //   HTML2PDF($content, $data);
    // }
    // public function laporanPembayaran($request){



    //   $rangkuman = TransPyr::select('*')
    //                           ->where('status',2)
    //                           ->orderBy('vendor_lokal','asc')
    //                           ->orderBy('tgl_jatuh_tempo','asc');


    //   $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
    //   if($request->tanggal_start and $request->tanggal_end){
    //     $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
    //   }

    //   $record = $rangkuman->get();

      

    //   // dd($record);

    //   $data         = $record;
    //   $region       ='All';
    //   $lsi          ='All';
    //   $tmuk         ='All';
    //   $req          = $request->all();
    //   if($request->region_id){
    //     $region       = $request['tmukd']->first()->lsi->region->area;
    //   }
    //   if($request->lsi_id){
    //   // dd($result_tmuk);
    //     $lsi          = $request['tmukd']->first()->lsi->nama;
    //   }
    //   if($request->tmuk_kode){
    //     $tmuk         = $request['tmukd']->first()->nama;
    //   }

    //   $tahun_fiskal = TahunFiskal::getTahunFiskal();

    //   $data = [
    //     'records' => $record,
    //     'request' => $req,
    //     'fiskal'  => $tahun_fiskal,
    //     'region'  => $region,
    //     'lsi'     => $lsi,
    //     'tmuk'    => $tmuk,
    //   ];

    //   $content = $this->render('report.laporan-pusat.akun-utang-usaha.laporan-pembayaran', $data);
    //   // return $content;

    //   HTML2PDF($content, $data);
    // }
    // public function analysisUmurPemasok($request)
    // {
    //   $rangkuman = TransPyr::select('*')
    //                           ->orderBy('vendor_lokal','asc')
    //                           ->orderBy('tgl_jatuh_tempo','asc');


    //   $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
    //   if($request->tanggal_start and $request->tanggal_end){
    //     $rangkuman->whereBetween(\DB::raw('tgl_jatuh_tempo'), array($request->tanggal_start, $request->tanggal_end));
    //   }

    //   $record = $rangkuman->get();
    //   // dd($record);

    //   $data         = $record;
    //   $region       ='All';
    //   $lsi          ='All';
    //   $tmuk         ='All';
    //   $req          = $request->all();
    //   if($request->region_id){
    //     $region       = $request['tmukd']->first()->lsi->region->area;
    //   }
    //   if($request->lsi_id){
    //   // dd($result_tmuk);
    //     $lsi          = $request['tmukd']->first()->lsi->nama;
    //   }
    //   if($request->tmuk_kode){
    //     $tmuk         = $request['tmukd']->first()->nama;
    //   }

    //   $tahun_fiskal = TahunFiskal::getTahunFiskal();

    //   $data = [
    //     'records' => $record,
    //     'request' => $req,
    //     'fiskal'  => $tahun_fiskal,
    //     'region'  => $region,
    //     'lsi'     => $lsi,
    //     'tmuk'    => $tmuk,
    //   ];

    //   $content = $this->render('report.laporan-pusat.akun-utang-usaha.analysis-umur-pemasok', $data);
    //   // return $content;

    //   HTML2PDF($content, $data);
    // }

   public function postPrintExcel(Request $request){
      if($request->tanggal_start && $request->tanggal_end){
          $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
          $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
      }
      $reg = Tmuk::with('lsi','lsi.region');
      if($request->lsi_id){
        $reg->whereHas('lsi',  function($ql) use($request){
          $ql->where('id', (integer)$request->lsi_id);
        });
      }
      if($request->region_id){
        $reg->whereHas('lsi.region', function($qr) use($request){
          $qr->where('id', (integer)$request->region_id);
        });
      }
      if($request->tmuk_kode){
        $reg->where('id', (integer)$request->tmuk_kode);
      }
      $request['tmukd'] = clone  $reg;

      $result = $reg->get();
      $temp=[];

      foreach ($result as $val){
        $temp[] = $val->kode;
      }

      $request['temp'] = $temp;
      switch ($request->types) {
        case 'saldo-pemasok':
            $record = TransPyr::from(\DB::raw("(SELECT
            trans_jurnal.coa_kode,
            trans_jurnal.tanggal,
            trans_pyr.tgl_buat,
            trans_pyr.vendor_lokal,
            trans_jurnal.tmuk_kode,
            trans_jurnal.idtrans as id_jurnal,
            trans_pyr.nomor_pyr,
            trans_jurnal.jumlah as charges_jurnal,
            trans_jurnal.deskripsi,
            trans_jurnal.posisi,
            trans_pyr_pembayaran.verifikasi4_user
            FROM trans_pyr
            LEFT JOIN trans_jurnal ON trans_pyr.id_transaksi = trans_jurnal.idtrans
            LEFT JOIN trans_pyr_pembayaran ON trans_pyr.nomor_pyr = trans_pyr_pembayaran.nomor_pyr )  d"))->where('coa_kode', '2.1.1.0')->whereIn('tmuk_kode',  $temp);
            if($request->tanggal_start and $request->tanggal_end){
              $record->whereBetween(\DB::raw('DATE(tanggal)'), array($request->tanggal_start, $request->tanggal_end));
            }
            $record       = $record->get();

            $temp         = [];
            $vendor_lokal = "";
            $i = 1;
            foreach ($record as $row) {
              if($row->vendor_lokal == 100){
                $vendor_lokal = $row->tmuk->nama ;
              }else if($row->vendor_lokal == 0){
                $vendor_lokal = $row->tmuk->lsi->nama ;
              }else{
                $vendor_lokal = isset($row->vendorlokal->nama)? $row->vendorlokal->nama:'-';
              }
              $temp[] = [
                $i,
                $vendor_lokal,
                $row->tanggal,
                $row->tgl_buat,
                $row->deskripsi == 'Pembayaran PyR - Trade Non Lotte' ? '' : $row->tanggal,
                $row->posisi == 'K' ? '0': rupiah($row->charges_jurnal),
                $row->posisi == 'D' ? '0': rupiah($row->charges_jurnal),
                $row->verifikasi4_user == NULL ? '0': rupiah($row->charges_jurnal),
                $row->verifikasi4_user == NULL ? rupiah($row->charges_jurnal) : '0'
              ];
              $i++;
            }

            $data = [
              'tittle'     => 'Saldo Pemasok',
              'tittle_set' => 'Saldo Pemasok',
              'header_set' => array(
                '#',
                'Vendor Lokal',
                'Trans Type',
                'Date',
                'Due Date',
                'Charges',
                'Credits',
                'Allocated',
                'Outstanding'
              ),
              'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20),
              'creator'    => 'Yusup Supriatna',
              'record'     => $temp,
            ];
            $this->printExcel($request, $data);
        break;
        case 'laporan-pembayaran':
          $record = TransPyr::select('*')
            ->where('status',2)
            ->orderBy('vendor_lokal','asc')
            ->orderBy('tgl_jatuh_tempo','asc')

          ->whereIn('tmuk_kode',  $temp);
          if($request->tanggal_start and $request->tanggal_end){
            $record->whereBetween(\DB::raw('DATE(tgl_jatuh_tempo)'), array($request->tanggal_start, $request->tanggal_end));
          }

          $record = $record->get();
          $temp = [];
          $i = 1;
          foreach ($record as $row) {
            $vendorlokal = '';
            if($row->vendor_lokal == 100){
              $vendorlokal = $row->tmuk->nama ;
            }else if($row->vendor_lokal == 0){
              $vendorlokal = $row->tmuk->lsi->nama ;
            }else{
              $vendorlokal = $row->vendorlokal->nama;
            }
            $temp[] = [
              $i,
              $vendorlokal,
              'Supplier Invoice',
              $row->urutan($row->id),
              $row->tgl_jatuh_tempo,
              rupiah($row->total)
            ];
            $i++;
          }

          $data = [
            'tittle'     => 'Laporan Pembayaran',
            'tittle_set' => 'Laporan Pembayaran',
            'header_set' => array(
              'No',
              'Vendor Lokal',
              'Tipe Transaksi',
              '#',
              'Jatuh Tempo',
              'Total (Rp)'
            ),
            'set_width'  => array('A'=>7,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20),
            'creator'    => 'Yusup Supriatna',
            'record'     => $temp,
          ];
          $this->printExcel($request, $data);
        break;
        case 'analysis-umur-pemasok':
          $record = TransPyr::select('*')
            ->whereIn('tmuk_kode',  $temp)
            ->orderBy('vendor_lokal','asc')
            ->orderBy('tgl_jatuh_tempo','asc');
                 
          if($request->tanggal_start and $request->tanggal_end){
            $record->whereBetween(\DB::raw('DATE(tgl_jatuh_tempo)'), array($request->tanggal_start, $request->tanggal_end));
          }
          $record = $record->get();

          $temp = [];
          $i = 1;
          foreach ($record as $row) {
            $i=0;
            $vendor='';

           if($i==0){
                      $vendor = $row->vendor_lokal;
                  }else{
                      if($row->vendor_lokal!=$vendor){
                          $i=0;
                          $vendor = $row->vendor_lokal;
                      }
                  }
                  $st  = date_create(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$row->created_at)->format('Y-m-d'));
                  $en    = date_create();
                  $diff   = date_diff( $st, $en );
                  $day_1 = $diff->days; 
                  $day_1 = $day_1 <= 30 ? rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) : '0';
                  $day_2 = $diff->days;  
                  $day_1 = $day_2 > 30 && $day_2 <= 60 ? rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) : '0';
                  $day_3 = $diff->days;      
                  $day_1 = $day_3 > 60 ? rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) : '0';
                  $balance = rupiah(isset($row->detailPyr) ? $row->detailPyr->sum('price') : 0) ;

            $temp[] = [
              $i,
              'Supplier Invoice',
              $row->tgl_jatuh_tempo,
              '0',
              $day_1,
              $day_2,
              $day_3,
              $balance,
            ];
            $i++;
          }
          $data = [
            'tittle'     => 'Laporan Analysis Pemasok',
            'tittle_set' => 'Laporan Analysis Pemasok',
            'header_set' => array(
              '#',
              'Tipe Transaksi',
              'Currency',
              'Current',
              '1-30 Days',
              '31-60 Days',
              'Over 60 Days',
              'Total Balance'
            ),
            'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20),
            'creator'    => 'Yusup Supriatna',
            'record'     => $temp,
          ];
          $this->printExcel($request, $data);
        break;
      }
   }

  //GENERATE LAPORAN EXCEL
  public function printExcel($request='', $data=[]){
    
    $Export = Excel::create(isset($data['tittle'])?$data['tittle']:'default', function($excel) use ($request, $data){
        $excel->setTitle(isset($data['tittle'])?$data['tittle']:'default');
        $excel->setCreator(isset($data['creator'])?$data['creator']:'Aplikasi Akuntansi')->setCompany('Lotte');
        $excel->setDescription('Export');

        $excel->sheet(isset($data['tittle'])?$data['tittle']:'default', function($sheet) use ($request, $data){
        if(isset($data['set_width'])){
            $tray = array_keys($data['set_width']);
            $last_cell = end($tray);
        }
        $sheet->row(1, array(
            isset($data['tittle_set'])?$data['tittle_set']:'default'
        ));

        $sheet->row(2, array(
          'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
        ));
        $sheet->row(3, isset($data['header_set'])?$data['header_set']:[]);

        $row = 4;
        $no =1;
        foreach ($data['record'] as $elm) {
            $sheet->row($row, $elm);
          $row++;
        }
        $sheet->cells('E4'.':'.$last_cell.''.$row, function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
        });
        $sheet->setBorder('A3:'.$last_cell.''.$row, 'thin');
        $sheet->mergeCells('A1:'.$last_cell.'1');
        $sheet->mergeCells('A2:'.$last_cell.'2');
        $sheet->cells('A1:A1', function($cells){
          $cells->setFontSize(14);
          $cells->setFontWeight('bold');
          $cells->setAlignment('center');
        });
        $sheet->cells('A2:'.$last_cell.'2', function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
        });
        $sheet->cells('A3:'.$last_cell.'3', function($cells){
          $cells->setAlignment('center');
          $cells->setBackground('#999999');
          $cells->setFontColor('#FFFFFF');
        });
        $sheet->setWidth(isset($data['set_width'])?$data['set_width']:[]);
        $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->setWrapText(true);
        $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top')
        );
        $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'left')
        );
        $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'right')
        );
        $sheet->getStyle('E4:E'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'right')
        );

        });
    });
    $Export ->download('xls');
  }
}


