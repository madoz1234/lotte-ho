<?php

namespace Lotte\Http\Controllers\LaporanPusat;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;

use Datatables;
use Carbon\Carbon;


class LaporanPusatPenjualanController extends Controller
{
    protected $link = 'laporan-pusat/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Pusat");   
    }

  public function postGridTemp(Request $request){
    //query temp
    $this->setLink($this->link.'penjualan');
    switch ($request->types) {
      case 'penjualan-mingguan':
        $data = [
          'lists_grid' => 'penjualan-mingguan',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.penjualan.penjualan-mingguan', $data);
      break;
      case 'penjualan-bulanan':
        $data = [
          'lists_grid' => 'penjualan-bulanan',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.penjualan.penjualan-bulanan', $data);
      break;
      case 'penjualan-per-barang':
        $data = [
          'lists_grid' => 'penjualan-per-barang',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.penjualan.penjualan-per-barang', $data);
      break;
      case 'setoran-penjualan-pusat':
        $data = [
          'lists_grid' => 'setoran-penjualan-pusat',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.penjualan.setoran-penjualan-pusat', $data);
      break;
      default:
        $data = [
          'lists_grid' => 'all-penjualan',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.penjualan.all-penjualan', $data);
      break;
    }
  }

  public function postGridAll(Request $request){
    if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }
    $result= $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    //query temp
    switch ($request->types) {
      case 'penjualan-mingguan':
        $record = TransRekapPenjualanTmuk::getDataMingguanHpp($request->tanggal_start, $request->tanggal_end);
        $record->whereIn(\DB::raw('tmuk_kode'),  $temp);
        $record = $record;
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
          // dd($record);
            return $request->get('start');
        })
        ->addColumn('periode', function ($record) {
          
            return 'Tahun : '.$record->tahun.' Minggu :'. $record->minggu;
        })

        ->addColumn('tmuk_kode', function ($record) {
            return $record->tmuk->nama;
        })

        ->addColumn('total_penjualan', function ($record) {
            return rupiah($record->total_penjualan);
        })
        ->addColumn('tunai', function ($record) {
            return rupiah($record->tunai);
        })
        ->addColumn('piutang', function ($record) {
            return rupiah($record->piutang);
        })
        ->addColumn('points', function ($record) {
            return rupiah($record->points);
        })
        ->addColumn('diskon', function ($record) {
            return rupiah($record->diskon);
        })
        ->addColumn('voucher', function ($record) {
            return rupiah($record->voucher);
        })

        ->make(true);
      break;
      case 'penjualan-bulanan':
        $record = TransRekapPenjualanTmuk::getDataBulananPusat($request->tanggal_start, $request->tanggal_end);
        $record->whereIn(\DB::raw('tmuk_kode'),  $temp);
        $record = $record;
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('periode', function ($record) use ($request) {
            return \Carbon\Carbon::parse($record->tanggal)->format('F Y');
            // return 'Tahun :'. $record->years. ' Bulan :'. $record->month;
        })
        ->addColumn('tmuk_kode', function ($record) {
            return $record->tmuk->nama;
        })

        ->addColumn('total_penjualan', function ($record) {
            return rupiah($record->total_penjualan);
        })
        ->addColumn('tunai', function ($record) {
            return rupiah($record->tunai);
        })
        ->addColumn('piutang', function ($record) {
            return rupiah($record->piutang);
        })
        ->addColumn('points', function ($record) {
            return rupiah($record->points);
        })
        ->addColumn('diskon', function ($record) {
            return rupiah($record->diskon);
        })
        ->addColumn('voucher', function ($record) {
            return rupiah($record->voucher);
        })
        ->make(true);
      break;
      case 'penjualan-per-barang':
        $rangkuman = TransRekapPenjualanTmuk::getDataPenjualanPerBarang($request->tanggal_start, $request->tanggal_end);
        $rangkuman->whereIn('tmuk_kode',  $temp);
        // if($request->tanggal_start && $request->tanggal_end){
        //   $rangkuman->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        // }

        // $record = $rangkuman->orderBy(\DB::raw('tmuk_kode'),'DESC');
        $record = $rangkuman;
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('tanggal', function ($record) {
            return (\Carbon\Carbon::parse($record->tanggal)->format('Y-m-d'));
        })
        ->addColumn('produk', function ($record) {
            return $record->produk;
        })
        ->addColumn('nama', function ($record) {
            return $record->nama;
        })
        ->addColumn('qty', function ($record) {
            return $record->qty;
        })
        ->addColumn('harga', function ($record) {
            return rupiah($record->harga);
        })
        ->addColumn('total', function ($record) {
            return rupiah($record->total);
        })
        ->addColumn('hpp', function ($record) {
            return rupiah($record->hpp);
        })
        ->addColumn('profit',function($record){
          $profit = $record->total - $record->hpp;
          return rupiah($profit);
        })
        ->addColumn('total_margin',function($record){
          $profit = $record->total - $record->hpp;
          $total_margin = $profit / $record->total;
          return round($total_margin,2);
        })
        ->make(true);
      break;
      case 'setoran-penjualan-pusat':
        // $rangkuman = TransRekapPenjualanTmuk::getDataSetoran();
        $rangkuman = TransRekapPenjualanTmuk::getDataSetoran();
        $rangkuman->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
        }

        $record = $rangkuman->orderBy(\DB::raw('tmuk_kode'),'DESC');
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })

        ->addColumn('tmuk_kode', function ($record) {
            return $record->tmuk->nama;
        })

        ->addColumn('total_penjualan', function ($record) {
            return rupiah($record->total_penjualan);
        })
        ->addColumn('setoran', function ($record) {
            return rupiah($record->setoran);
        })

        ->make(true);
      break;

      default:
        $rangkuman = TransRekapPenjualanTmuk::getDataHarian();
        $rangkuman->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
        }

        $record = $rangkuman->orderBy(\DB::raw('tmuk_kode'),'DESC');
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('tmuk_kode', function ($record) {
            return $record->tmuk->nama;
        })

        ->addColumn('total_penjualan', function ($record) {
            return rupiah($record->total_penjualan);
        })
        ->addColumn('tunai', function ($record) {
            return rupiah($record->tunai);
        })
        ->addColumn('piutang', function ($record) {
            return rupiah($record->piutang);
        })
        ->addColumn('points', function ($record) {
            return rupiah($record->points);
        })
        ->addColumn('diskon', function ($record) {
            return rupiah($record->diskon);
        })
        ->addColumn('voucher', function ($record) {
            return rupiah($record->voucher);
        })
        ->make(true);
      break;
    }
  }
  //PRINT PDF
  public function postPrint(Request $request){
   if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // dd($request->all());
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $request['tmukd']->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    $request['temp'] = $temp;
    switch ($request->types) {
      case 'penjualan-mingguan':
        $rangkuman = TransRekapPenjualanTmuk::getDataMingguanHpp($request->tanggal_start, $request->tanggal_end);
        $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);

        $record = $rangkuman->get();

        return $this->printPdf($request, $record);
      break;
      case 'penjualan-bulanan':
        $rangkuman = TransRekapPenjualanTmuk::getDataBulananPusat($request->tanggal_start, $request->tanggal_end);
        $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
        $record = $rangkuman->get();
        return $this->printPdf($request, $record);
      break;
      case 'penjualan-per-barang':
        $rangkuman = TransRekapPenjualanTmuk::getDataPenjualanPerBarang($request->tanggal_start, $request->tanggal_end);
        $rangkuman->whereIn('tmuk_kode',  $request->temp);
        // if($request->tanggal_start and $request->tanggal_end){
        //   $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
        // }

        $record = $rangkuman->get();
        return $this->printPdf($request, $record);
        // test
        // $rangkuman = TransRekapPenjualanTmuk::getDataPenjualanPerBarang($request->tanggal_start, $request->tanggal_end);
        // $rangkuman->whereIn('tmuk_kode',  $temp);
        // if($request->tanggal_start && $request->tanggal_end){
        //   $rangkuman->whereBetween('tanggal', array($request->tanggal_start. ' 00:00:00', $request->tanggal_end. ' 23:59:59'));
        // }

        // $record = $rangkuman->orderBy(\DB::raw('tmuk_kode'),'DESC');
        // $record = $rangkuman;
        // test
      break;
      case 'setoran-penjualan-pusat':
        $rangkuman = TransRekapPenjualanTmuk::getDataSetoran();
        $rangkuman->whereIn('tmuk_kode',  $request->temp);
        if($request->tanggal_start and $request->tanggal_end){
          $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
        }
        // }

        $record = $rangkuman->get();
        return $this->printPdf($request, $record);
      break;        
      default:
        $rangkuman = TransRekapPenjualanTmuk::getDataHarian();
        $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);
        if($request->tanggal_start and $request->tanggal_end){
          $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
        }

        $record = $rangkuman->get();
        return $this->printPdf($request, $record);
      break;
    }
  }

  //PRINT EXCEL
  public function postPrintExcel(Request $request){
    if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // dd($request->all());
    $tmukd = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $request['tmukd']->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }
    $request['temp'] = $temp;
    switch ($request->types) {
      case 'penjualan-mingguan':
      $this->excelMingguan($request);
      break;
      case 'penjualan-bulanan':
      $this->excelBulanan($request);
      break;
      case 'penjualan-per-barang':
      $this->excelPerbarang($request);
      break;        
      default:
      $this->excelHarian($request);
      break;
    }
  }

  //GENERATE LAPORAN EXCEL
  public function excelHarian($request=''){
    $rangkuman = TransRekapPenjualanTmuk::getDataHarian();
    // dd($rangkuman);
    $rangkuman->whereIn('tmuk_kode',  $request->temp);
    if($request->tanggal_start and $request->tanggal_end){
    $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
    }
    $record = $rangkuman->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();

    $data =[];
    //dd($record->toArray());
    foreach($record->toArray() as $val){
        $data[$val['tmuk_kode']]['child'][] = $val;      
    }
    $temps = [];
    foreach($data as $key=>$val){
        $struk           = 0;
        $tunai           = 0;
        $piutang         = 0;
        $points          = 0;
        $diskon          = 0;
        $voucher         = 0;
        $total_hpp       = 0;
        $profit          = 0;
        $total_penjualan = 0;
        $margin          = 0;
        foreach($val['child'] as $tot){
            $struk +=(integer)$tot['struk'];
            $tunai +=(integer)$tot['tunai'];
            $piutang +=(integer)$tot['piutang'];
            $points +=(integer)$tot['points'];
            $diskon +=(integer)$tot['diskon'];
            $voucher +=(integer)$tot['voucher'];
            $total_hpp +=(integer)$tot['total_hpp'];
            $profit +=(integer)$tot['total_penjualan']-(integer)$tot['total_hpp'];
            $total_penjualan +=(integer)$tot['total_penjualan'];
        }
        $temps[$key]['total'] = [
          "tmuk_kode"       => $key,
          "tanggal"         => $tot['nama_tmuk'],
          "struk"           => $struk,
          "tunai"           => $tunai,
          "piutang"         => $piutang,
          "points"          => $points,
          "diskon"          => $diskon,
          "voucher"         => $voucher,
          "total_hpp"       => $total_hpp,
          "profit"          => $profit,
          "total_penjualan" => $total_penjualan,
          "margin"          => ((integer)$profit/(integer)$total_penjualan)*100
        ];
        
        $temps[$key]['child'] = $val['child'];
    }

    $request['record']=$temps;

    $Export = Excel::create('Rekap Penjualan Perhari ', function($excel) use ($request){
    $excel->setTitle('Rekap Penjualan Perhari');
    $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
    $excel->setDescription('Export');

    $excel->sheet('Rekap Penjualan Perhari', function($sheet) use ($request){
    $sheet->row(1, array(
      'Rekap Penjualan Perhari'
      ));

    $sheet->row(2, array(
    'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
      ));
    $sheet->row(3, array(
      'NO',
      'Kode TMUK',
      'Tanggal',
      'Total Penjualan (Rp)',
      'Total Tunai (Rp)',
      'Piutang (Rp)',
      'Points',
      'Diskon',
      'Voucher',
      ));

    $row = 4;
    $no =1;
    foreach ($request->record as $elm) {
        // dd($elm['total']['points']);
        $sheet->row($row, array(
            $no,
            $elm['total']['tmuk_kode'],
            $elm['total']['tanggal'],
            number_format($elm['total']['total_penjualan']),
            number_format($elm['total']['tunai']),
            number_format($elm['total']['piutang']),
            number_format($elm['total']['points']),
            number_format($elm['total']['diskon']),
            number_format($elm['total']['voucher']),
      ));
      $row = $row+1;
      foreach ($elm['child'] as $elms) {      
        $sheet->row($row, array(
            $no,
            '',
            $elm['total']['tanggal'],
            number_format($elm['total']['total_penjualan']),
            number_format($elm['total']['tunai']),
            number_format($elm['total']['piutang']),
            number_format($elm['total']['points']),
            number_format($elm['total']['diskon']),
            number_format($elm['total']['voucher']),
            ));
        $row=$row;
        $row++;$no++;
      }
    }
    $sheet->cells('D4'.':I'.$row, function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->setBorder('A3:I'.$row, 'thin');
    $sheet->mergeCells('A1:I1');
    $sheet->mergeCells('A2:I2');
    $sheet->cells('A1:A1', function($cells){
      $cells->setFontSize(14);
      $cells->setFontWeight('bold');
      $cells->setAlignment('center');
    });
    $sheet->cells('A2:I2', function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->cells('A3:I3', function($cells){
      $cells->setAlignment('center');
      $cells->setBackground('#999999');
      $cells->setFontColor('#FFFFFF');
    });
    $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20, 'I'=>20));
    $sheet->getStyle('B4:I'.$row)->getAlignment()->setWrapText(true);
    $sheet->getStyle('B4:I'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top')
    );
    $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top', 'horizontal' => 'center')
    );

    });
    });
    $Export ->download('xls');
  }
  public function excelMingguan($request=''){
    $rangkuman = TransRekapPenjualanTmuk::getDataMingguan($request->tanggal_start, $request->tanggal_end);
    $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);

    $record = $rangkuman->get();

    $data = [];
    foreach($record->toArray() as $val){
        $data[$val['tmuk_kode']]['child'][] = $val;      
    }
    $temps = [];
    foreach($data as $key=>$val){
        $struk           = 0;
        $tunai           = 0;
        $piutang         = 0;
        $points          = 0;
        $diskon          = 0;
        $voucher         = 0;
        $total_hpp       = 0;
        $profit          = 0;
        $total_penjualan = 0;
        $margin          = 0;
        foreach($val['child'] as $tot){
            $tunai           +=(integer)$tot['tunai'];
            $piutang         +=(integer)$tot['piutang'];
            $points          +=(integer)$tot['points'];
            $diskon          +=(integer)$tot['diskon'];
            $voucher         +=(integer)$tot['voucher'];
            $struk           +=(integer)$tot['struk'];
            $total_hpp       +=(integer)$tot['total_hpp'];
            $profit          +=(integer)$tot['total_penjualan']-(integer)$tot['total_hpp'];
            $total_penjualan +=(integer)$tot['total_penjualan'];
        }
        $temps[$key]['total'] = [
            "tmuk_kode"       => $key,
            "periode"         => $tot['nama_tmuk'],
            "struk"           => $struk,
            "tunai"           => $tunai,
            "piutang"         => $piutang,
            "points"          => $points,
            "diskon"          => $diskon,
            "voucher"         => $voucher,
            "total_hpp"       => $total_hpp,
            "profit"          => $profit,
            "total_penjualan" => $total_penjualan,
            "margin"          => ((integer)$profit/(integer)$total_penjualan)*100
        ];
        $temps[$key]['child'] = $val['child'];
    }

    $request['record'] = $temps;

     $Export = Excel::create('Rekap Penjualan Perminggu ', function($excel) use ($request){
    $excel->setTitle('Rekap Penjualan Perminggu');
    $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
    $excel->setDescription('Export');

    $excel->sheet('Rekap Penjualan Perminggu', function($sheet) use ($request){
    $sheet->row(1, array(
      'Rekap Penjualan Perminggu'
      ));

    $sheet->row(2, array(
    'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
      ));
    $sheet->row(3, array(
      'NO',
      'Kode TMUK',
      'Priode',
      'Total Penjualan (Rp)',
      'Penjualan Tunai',
      'Piutang',
      'Poin',
      'Diskon',
      'Voucher',
      ));

    $row = 4;
    $no =1;
    foreach ($request->record as $elm) {
        // dd($elm['total']);
        $sheet->row($row, array(
            $no,
            $elm['total']['tmuk_kode'],
            $elm['total']['periode'],
            number_format($elm['total']['total_penjualan']),
            number_format($elm['total']['tunai']),
            number_format($elm['total']['piutang']),
            number_format($elm['total']['points']),
            number_format($elm['total']['diskon']),
            number_format($elm['total']['voucher']),
      ));
      $row = $row+1;
      foreach ($elm['child'] as $elms) {    
      // dd($elms) ;
        $sheet->row($row, array(
            $no,
            '',
            'Minggu : '. $elms['week'].' Tahun : '.$elms['years'],
            number_format($elms['total_penjualan']),
            number_format($elms['tunai']),
            number_format($elms['piutang']),
            number_format($elms['points']),
            number_format($elms['diskon']),
            number_format($elms['voucher']),
            ));
        $row=$row;
        $row++;$no++;
      }
    }
    $sheet->cells('E4'.':I'.$row, function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->setBorder('A3:I'.$row, 'thin');
    $sheet->mergeCells('A1:I1');
    $sheet->mergeCells('A2:I2');
    $sheet->cells('A1:A1', function($cells){
      $cells->setFontSize(14);
      $cells->setFontWeight('bold');
      $cells->setAlignment('center');
    });
    $sheet->cells('A2:I2', function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->cells('A3:I3', function($cells){
      $cells->setAlignment('center');
      $cells->setBackground('#999999');
      $cells->setFontColor('#FFFFFF');
    });
    $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>40, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20, 'I'=>20));
    $sheet->getStyle('B4:I'.$row)->getAlignment()->setWrapText(true);
    $sheet->getStyle('B4:I'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top')
    );
    $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top', 'horizontal' => 'center')
    );

    });
    });
    $Export ->download('xls');
  }
  public function excelBulanan($request=''){
    $rangkuman = TransRekapPenjualanTmuk::getDataBulanan($request->tanggal_start, $request->tanggal_end);
    $rangkuman->whereIn(\DB::raw('tmuk_kode'),  $request->temp);

    $record = $rangkuman->get();

    $data = [];
            foreach($record->toArray() as $val){
                $data[$val['tmuk_kode']]['child'][] = $val;      
            }
            $temps = [];
            foreach($data as $key=>$val){
                $struk           = 0;
                $tunai           = 0;
                $piutang         = 0;
                $points          = 0;
                $diskon          = 0;
                $voucher         = 0;
                $total_hpp       = 0;
                $profit          = 0;
                $total_penjualan = 0;
                $margin          = 0;
                foreach($val['child'] as $tot){
                    $tunai           +=(integer)$tot['tunai'];
                    $piutang         +=(integer)$tot['piutang'];
                    $points          +=(integer)$tot['points'];
                    $diskon          +=(integer)$tot['diskon'];
                    $voucher         +=(integer)$tot['voucher'];
                    $struk           +=(integer)$tot['struk'];
                    $total_hpp       +=(integer)$tot['total_hpp'];
                    $profit          +=(integer)$tot['total_penjualan']-(integer)$tot['total_hpp'];
                    $total_penjualan +=(integer)$tot['total_penjualan'];
                }
                $temps[$key]['total'] = [
                    "tmuk_kode"       => $key,
                    "periode"         => $tot['nama_tmuk'],
                    "struk"           => $struk,
                    "tunai"           => $tunai,
                    "piutang"         => $piutang,
                    "points"          => $points,
                    "diskon"          => $diskon,
                    "voucher"         => $voucher,
                    "total_hpp"       => $total_hpp,
                    "profit"          => $profit,
                    "total_penjualan" => $total_penjualan,
                    "margin"          => ((integer)$profit/(integer)$total_penjualan)*100
                ];
                $temps[$key]['child'] = $val['child'];
            }

    $request['record'] = $temps;

    $Export = Excel::create('Rekap Penjualan per Bulan ', function($excel) use ($request){
    $excel->setTitle('Rekap Penjualan Perbulan');
    $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
    $excel->setDescription('Export');

    $excel->sheet('Rekap Penjualan Perbulan', function($sheet) use ($request){
    $sheet->row(1, array(
      'Rekap Penjualan Perbulan'
      ));

    $sheet->row(2, array(
    'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
      ));
    $sheet->row(3, array(
      'NO',
      'Kode TMUK',
      'Periode',
      'Total Penjualan (Rp)',
      'Penjualan Tunai (Rp)',
      'Piutang (Rp)',
      'Poin (Rp)',
      'Diskon (Rp)',
      'Voucher (Rp)',
      ));

    $row = 4;
    $no =1;
    foreach ($request->record as $elm) {
        // dd($elm['total']);
        $sheet->row($row, array(
            $no,
            $elm['total']['tmuk_kode'],
            $elm['total']['periode'],
            number_format($elm['total']['total_penjualan']),
            number_format($elm['total']['tunai']),
            number_format($elm['total']['piutang']),
            number_format($elm['total']['points']),
            number_format($elm['total']['diskon']),
            number_format($elm['total']['voucher']),
      ));
      $row = $row+1;
      foreach ($elm['child'] as $elms) {    
      // dd($elms) ;
        $sheet->row($row, array(
            $no,
            '',
            'Tahun : '.$elms['years'].' Bulan : '. $elms['month'],
            number_format($elms['total_penjualan']),
            number_format($elm['total']['tunai']),
            number_format($elm['total']['piutang']),
            number_format($elm['total']['points']),
            number_format($elm['total']['diskon']),
            number_format($elm['total']['voucher']),
            ));
        $row=$row;
        $row++;$no++;
      }
    }
    $sheet->cells('E4'.':I'.$row, function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->setBorder('A3:I'.$row, 'thin');
    $sheet->mergeCells('A1:I1');
    $sheet->mergeCells('A2:I2');
    $sheet->cells('A1:A1', function($cells){
      $cells->setFontSize(14);
      $cells->setFontWeight('bold');
      $cells->setAlignment('center');
    });
    $sheet->cells('A2:I2', function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->cells('A3:I3', function($cells){
      $cells->setAlignment('center');
      $cells->setBackground('#999999');
      $cells->setFontColor('#FFFFFF');
    });
    $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>40, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20, 'I'=>20));
    $sheet->getStyle('B4:I'.$row)->getAlignment()->setWrapText(true);
    $sheet->getStyle('B4:I'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top')
    );
    $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top', 'horizontal' => 'center')
    );

    });
    });
    $Export ->download('xls');
  }
  public function excelPerbarang($request=''){
    $rangkuman = TransRekapPenjualanTmuk::getDataPenjualanPerBarang($request->tanggal_start, $request->tanggal_end);
    $rangkuman->whereIn('tmuk_kode',  $request->temp);
    $records = $rangkuman->get();
    $data =[];
            foreach($records->toArray() as $val){
                $data[$val['tmuk_kode']]['child'][] = $val;      
            }
            $temps = [];
            $all_qty           = 0;
            $all_produk        = 0;
            foreach($data as $key=>$val){
                $qty           = 0;
                $produk        = 0;
                $nama          = 0;
                $harga         = 0;
                $total         = 0;
                $hpp           = 0;
                $profit        = 0;
                $hasilprofit   = 0;
                foreach($val['child'] as $tot){
                    $qty            +=(integer)$tot['qty'];
                    $produk         +=(integer)$tot['produk'];
                    $nama           +=(integer)$tot['nama'];
                    $harga          +=(integer)$tot['harga'];
                    $total          +=(integer)$tot['total'];
                    $hpp            +=(integer)$tot['hpp'];
                }
                $all_qty           += $qty;

                $temps[$key]['total'] = [
                  "tmuk_kode"       => $key,
                  "produk"          => $produk,
                  "nama"            => $nama,
                  "qty"             => $qty,
                  "harga"           => $harga,
                  "total"           => $total,
                  "hpp"             => $hpp
                ];
                
                $temps[$key]['child'] = $val['child'];
            }
    $request['record'] = $temps;
    // test
    $Export = Excel::create('Rekap Penjualan per Barang ', function($excel) use ($request){
    $excel->setTitle('Rekap Penjualan per Barang');
    $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
    $excel->setDescription('Export');

    $excel->sheet('Rekap Penjualan per Barang ', function($sheet) use ($request){
    $sheet->row(1, array(
      'Rekap Penjualan per Barang '
      ));

    $sheet->row(2, array(
    'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
      ));
    // test
    $sheet->row(3, array(
      'NO',
      'Tanggal',
      'Kode Produk',
      'Nama Produk',
      'Qty',
      'Harga',
      'Total',
      'Hpp',
      'Profit',
      'Profit Margin',
      ));

    $row = 4;
    $no =1;
    foreach ($request->record as $elm) {
        // dd($elm['total']);
        $sheet->row($row, array(
            $no,
            $elm['total']['tmuk_kode'],
      ));
      $row = $row+1;
      foreach ($elm['child'] as $elms) {    
      // dd($elms) ;
        $sheet->row($row, array(
            $no,
            $elms['tanggal'],
            $elms['produk'],
            $elms['nama'],
            $elms['qty'],
            number_format($elms['harga']),
            number_format($elms['total']),
            number_format($elms['hpp']),
            number_format($elms['total']-$elms['hpp']),
            round(($elms['total']-$elms['hpp'])/$elms['total'],2),
            ));
        $row=$row;
        $row++;$no++;
      }
    }
    // test
    $sheet->cells('E4'.':J'.$row, function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->setBorder('A3:J'.$row, 'thin');
    $sheet->mergeCells('A1:J1');
    $sheet->mergeCells('A2:J2');
    $sheet->cells('A1:A1', function($cells){
      $cells->setFontSize(14);
      $cells->setFontWeight('bold');
      $cells->setAlignment('center');
    });
    $sheet->cells('A2:J2', function($cells){
      $cells->setFontSize(12);
      $cells->setAlignment('center');
    });
    $sheet->cells('A3:J3', function($cells){
      $cells->setAlignment('center');
      $cells->setBackground('#999999');
      $cells->setFontColor('#FFFFFF');
    });
    $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>80, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20));
    $sheet->getStyle('B4:J'.$row)->getAlignment()->setWrapText(true);
    $sheet->getStyle('B4:J'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top')
    );
    $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
      array('vertical' => 'top', 'horizontal' => 'center')
    );

    });
    });
    $Export ->download('xls');
  }

  // GENERATE LAPORAN pdf
  public function printPdf($request, $record=''){
    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $request['tmukd']->first()->lsi->region->area;
    }
    if($request->lsi_id){
      $lsi          = $request['tmukd']->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $request['tmukd']->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'saldo'   => isset($request['ledger'])?$request['ledger']:[],
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];
    
    $pdf = \PDF::loadView('report.laporan-pusat.penjualan.'.$request->types, $data);
    return $pdf->setOption('margin-bottom', 5)
    ->setPaper('a4')
    ->setOption('margin-top', 5)
    ->setOption('margin-left', 3)
    ->setOption('margin-right', 3)
    ->inline();
  }

}


