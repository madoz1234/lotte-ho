<?php
namespace Lotte\Http\Controllers\LaporanPusat;
ini_set('memory_limit', '-1');
set_time_limit((60*60)*2);

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Trans\TransMuatanDetail;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransRetur;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransRekapGr;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;

use Datatables;
use Carbon\Carbon;

class LaporanPusatPembelianController extends Controller
{
    protected $link = 'laporan-pusat/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Pusat");   
    }

  public function postGridTemp(Request $request){
    $this->setLink($this->link.'pembelian');
    switch ($request->types) {
      // case 'rekap-pembelian-persediaan':
      // break;
      case 'lembar-po-btdk':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'lembar-po-stdk':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'po-barang-trade-ke-lsi':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'po-barang-non-trade-ke-lsi':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'po-barang-trade-ke-vendor-lokal':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'lembar-picking-result-pt':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'lembar-label-kontainer-cl':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'lembar-daftar-muatan-it':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'lembar-surat-jalan-dn':           
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);     
      break;
      case 'rekap-delivery':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);  
      break;
      case 'rekap-gr-grn':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'lembar-konfirmasi-retur-rr':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'rekap-service-level-sl':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      case 'laporan-pembelian-persediaan':
        $data = [
          'lists_grid' => $request->types,
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.'.$request->types, $data);
      break;
      default:
        $data = [
          'lists_grid' => 'pembelian-persediaan',
        ];
        return $this->render('modules.laporan.laporan-pusat.lists.pembelian.rekap-pembelian-persediaan', $data);
      break;
    }
  }

  public function postGridAll(Request $request){
    if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
        // dd($request['tanggal_start']);
    }
    // dd($request->all());
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', $request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', $request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', $request->tmuk_kode);
    }
    $result= $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    switch ($request->types) {
      // case 'rekap-pembelian-persediaan':
      // break;
      case 'lembar-po-btdk':
        $record = TransPo::select('*')
        ->where('status', 1)
        ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_buat)'), array($request->tanggal_start, $request->tanggal_end));
        }

        return Datatables::of($record)
          ->addColumn('num', function ($record) use ($request) {
              return $request->get('start');
          })
          ->addColumn('lsi', function ($record) {
              return $record->tmuk->lsi->nama;
          })
          ->addColumn('tmuk', function ($record) {
              return $record->tmuk->nama;
          })
          ->addColumn('total', function ($record) {
              return rupiah($record->total);
          })
          ->make(true);
      break;
      case 'lembar-po-stdk':
        $record = TransPo::select('*')
        ->where('status', 2)
        ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
            $record->whereBetween(\DB::raw('DATE(tgl_buat)'), array($request->tanggal_start, $request->tanggal_end));
        }

        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->addColumn('total', function ($record) {
              return rupiah($record->total);
          })
        ->make(true);
      break;
      case 'po-barang-trade-ke-lsi':
        $record = TransPyr::select('*')
          ->where('tipe', '001')
          ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('tgl_buat'), array($request->tanggal_start, $request->tanggal_end));
        }
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->addColumn('total', function ($record) {
            return rupiah($record->total);
        })
        ->make(true);
      break;
      case 'po-barang-non-trade-ke-lsi':
        $record = TransPyr::select('*')
        ->where('tipe', '003')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('tgl_buat'), array($request->tanggal_start, $request->tanggal_end));
        }
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->addColumn('total', function ($record) {
              return rupiah($record->total);
          })
        ->make(true);
      break;
      case 'po-barang-trade-ke-vendor-lokal':
        $record = TransPyr::select('*')
        ->where('tipe', '002')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('tgl_buat'), array($request->tanggal_start, $request->tanggal_end));
        }
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->addColumn('total', function ($record) {
              return rupiah($record->total);
          })
        ->make(true);
      break;
      case 'lembar-picking-result-pt':
        $record = TransPr::select('*')
        ->where('status', 1)
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('tgl_buat'), array($request->tanggal_start, $request->tanggal_end));
        }
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->addColumn('total', function ($record) {
              return rupiah($record->total);
          })
        ->make(true);
      break;
      case 'lembar-label-kontainer-cl':
        $record = TransKontainer::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->make(true);
      break;
      case 'lembar-daftar-muatan-it':
        $record = TransMuatanDetail::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk_kode', function ($record) {
            return $record->tmuk->nama;
        })
        ->make(true);
      break;
      case 'lembar-surat-jalan-dn':                
        $record = TransMuatanDetail::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
          // dd($record);
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->addColumn('tanggal_muatan', function ($record) {
            return $record->muatan->created_at ;
        })
        ->addColumn('nomor_muatan', function ($record) {
            return $record->nomor_muatan ;
        })
        ->addColumn('tanggal_kirim', function ($record) {
            return $record->muatan->suratjalan->created_at;
        })
        ->addColumn('nomor', function ($record) {
            return $record->muatan->suratjalan->nomor ;
        })
        ->make(true);
      break;
      case 'rekap-delivery':
        $record = TransMuatanDetail::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
         return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
          // dd($record);
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->addColumn('nomor_surat', function ($record) {
            return $record->muatan->suratjalan->nomor;
        })
        ->make(true);
      break;
      case 'rekap-gr-grn':
        $record = TransRekapGr::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_gr)'), array($request->tanggal_start, $request->tanggal_end));
        }
         return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
          // dd($record);
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->make(true);
      break;
      case 'lembar-konfirmasi-retur-rr':
        $record = TransRetur::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
         return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
          // dd($record);
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk->nama;
        })
        ->addColumn('tgl_buat', function ($record) {
            return $record->po->tgl_buat ;
        })
        ->addColumn('nomor', function ($record) {
            return $record->po->nomor;
        })
        ->make(true);
      break;
      case 'rekap-service-level-sl':
        $record = TransPo::getServiceLevel();
        $record->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal_po)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC');
        // dd($record->toSql());
       return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->tmuk->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {

            return $record->tmuk->nama;
        })
        ->addColumn('sl', function ($record) {
            return rupiah((($record->qty_po/$record->qty_pr)*100)).'%';
        })
        ->addColumn('tanggal_pr', function ($record) {
            return $record->pr->created_at;
        })
        ->make(true);
      break;
      case 'laporan-pembelian-persediaan':
       $record = Tmuk::with('pyr', 'pyr.detailPyr', 'pyr.detailPyr.produks')
       ->whereIn('kode', $temp);
       $record->whereHas('pyr.detailPyr', function($q) use ($request){
            if($request->tanggal_start && $request->tanggal_end){
               $q->whereBetween(\DB::raw('DATE(tgl_buat)'),array($request->tanggal_start, $request->tanggal_end));
            }
       });

       return Datatables::of($record)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
            return $record->lsi->nama;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->nama;
        })
        ->addColumn('total', function ($record) {
            return rupiah($record->pyr->sum('total'));
        })
        ->make(true);
      break;
      default:
          // dd($request->all());
            // $rangkuman = TransRekapPenjualanTmuk::getDataPersediaan();
            // if($request->tanggal_start and $request->tanggal_end){
            //   $rangkuman->whereBetween(\DB::raw('tanggal'), [$request->tanggal_start, $request->tanggal_end]);
            // }
            // $rangkuman->whereIn('tmuk_kode',  $temp);
            // $record = $rangkuman->orderBy(\DB::raw('tmuk_kode'),'DESC');

            $record = TransRekapPenjualanTmuk::from(\DB::raw("(select
                                    trans_rekap_penjualan.tmuk_kode,
                                    trans_rekap_penjualan.tanggal,
                                    trans_rekap_penjualan_detail.qty,
                                    trans_rekap_penjualan.total,
                                    trans_rekap_penjualan_detail.harga,
                                    trans_rekap_penjualan.total - trans_rekap_penjualan.total as profit,
                                    (trans_rekap_penjualan.total - trans_rekap_penjualan.total) / trans_rekap_penjualan.total as margin
                                    from
                                    trans_rekap_penjualan
                                    inner join trans_rekap_penjualan_detail on trans_rekap_penjualan_detail.rekap_penjualan_kode = trans_rekap_penjualan.jual_kode ) d"));
            
            if($request->tanggal_start and $request->tanggal_end){
              $record->whereBetween(\DB::raw('tanggal'),array($request->tanggal_start, $request->tanggal_end));
            }
            if($request->tmuk_kode){
              $record->where('tmuk_kode', $temp);
            }       
            // dd($records->get());
           return Datatables::of($record)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('lsi', function ($record) {
                return $record->tmuk->lsi->nama;
            })
            ->addColumn('tmuk', function ($record) {
                return $record->tmuk->nama;
            })
            ->addColumn('penjualan', function ($record) {
                return rupiah($record->total);
            })
            ->addColumn('cost_price', function ($record) {
                return rupiah($record->harga);
            })
            ->addColumn('profit_margin', function ($record) {
                return rupiah($record->margin);
            })
            ->make(true);
      break;
    }
  }

  //PRINT PDF
  public function postPrint(Request $request){
    if($request->tanggal_start && $request->tanggal_end){
        $request['tanggal_start'] = Carbon::createFromFormat('d/m/Y',$request->tanggal_start)->format('Y-m-d');
        $request['tanggal_end']   = Carbon::createFromFormat('d/m/Y',$request->tanggal_end)->format('Y-m-d');
    }
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    // dd($request->all());
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();

    // $result_tmuk = $tmukd->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }

    $request['temp'] = $temp;
    switch ($request->types) {
      case 'lembar-po-btdk':
        $record = TransPo::select('*')
        ->where('status', 1)
        ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_buat)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'lembar-po-stdk':
        $record = TransPo::select('*')
        ->where('status', 2)
        ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
            $record->whereBetween(\DB::raw('DATE(tgl_buat)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'po-barang-trade-ke-lsi':
        $record = TransPyr::select('*')
        ->where('tipe', '001')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_buat)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();

        // dd($record);
        return $this->printPdf($request, $record);
      break;
      case 'po-barang-non-trade-ke-lsi':
        $record = TransPyr::select('*')
        ->where('tipe', '003')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_buat)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'po-barang-trade-ke-vendor-lokal':
        $record = TransPyr::select('*')
        ->where('tipe', '002')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_buat)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'lembar-picking-result-pt':
        $record = TransPr::select('*')
        ->where('status', 1)
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_kirim)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'lembar-label-kontainer-cl':
        $record = TransKontainer::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'lembar-daftar-muatan-it':
        $record = TransMuatanDetail::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'lembar-surat-jalan-dn':           
        $record = TransMuatanDetail::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'rekap-delivery':
        $record = TransMuatanDetail::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'rekap-gr-grn':
        $record = TransRekapGr::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_gr)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'lembar-konfirmasi-retur-rr':
        $record = TransRetur::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
      case 'rekap-service-level-sl':
        $record = TransPo::getServiceLevel();
        $record->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal_po)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        // dd($record->toArray());
        return $this->printPdf($request, $record);
      break;
      case 'laporan-pembelian-persediaan':

        $record = Tmuk::with('pyr', 'pyr.detailPyr', 'pyr.detailPyr.produks')
               ->whereIn('kode', $temp);

       $record->whereHas('pyr.detailPyr', function($q) use ($request){
            if($request->tanggal_start && $request->tanggal_end){
               $q->whereBetween(\DB::raw('DATE(tgl_buat)'),array($request->tanggal_start, $request->tanggal_end));
            }
       });
       $record =$record->get();
       // dd($record);
        return $this->printPdf($request, $record);
      break;
      default:
        $rangkuman = TransRekapPenjualanTmuk::getDataPersediaan();
        $rangkuman->whereIn('tmuk_kode',  $request->temp);
        if($request->tanggal_start and $request->tanggal_end){
          $rangkuman->whereBetween(\DB::raw('tanggal'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $rangkuman->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        return $this->printPdf($request, $record);
      break;
    }
  }

  //PRINT EXCEL
  public function postPrintExcel(Request $request){
    // dd($request->all());
    $reg = Tmuk::with('lsi','lsi.region');
    if($request->lsi_id){
      $reg->whereHas('lsi',  function($ql) use($request){
        $ql->where('id', (integer)$request->lsi_id);
      });
    }
    if($request->region_id){
      $reg->whereHas('lsi.region', function($qr) use($request){
        $qr->where('id', (integer)$request->region_id);
      });
    }
    if($request->tmuk_kode){
      $reg->where('id', (integer)$request->tmuk_kode);
    }
    $request['tmukd'] = clone  $reg;

    $result = $reg->get();
    $temp=[];

    foreach ($result as $val){
      $temp[] = $val->kode;
    }
    $request['temp'] = $temp;
    switch ($request->types) {
      case 'lembar-po-btdk':
        $record = TransPo::select('*')
        ->where('status', 1)
        ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_buat)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        // dd($record);
        $temp = [];
        foreach($record as $row){
          $temp[]=[
            'lsi'       => $row->tmuk->lsi->nama,
            'tmuk'      => $row->tmuk->nama,
            'nomor'     => $row->nomor,
            'nomor_pr'  => $row->nomor_pr,
            'tmuk_kode' => $row->tmuk_kode,
            'tgl_buat'  => $row->tgl_buat,
            'total'     => $row->total,
          ];
        }
        $data = [
          'tittle'     => 'Lembar PO BTDK',
          'tittle_set' => 'Lembar PO BTDK',
          'header_set' => array('LSI','TMUK','Nomor PO','Nomor PR','TMUK Kode','Tanggal Buat','Total'),
          'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'lembar-po-stdk':
        $record = TransPo::select('*')
        ->where('status', 2)
        ->whereIn('tmuk_kode',  $temp);
        if($request->tanggal_start and $request->tanggal_end){
            $record->whereBetween(\DB::raw('DATE(tgl_buat)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        $temp = [];
        foreach($record as $row){
          $temp[]=[
            'lsi'       => $row->tmuk->lsi->nama,
            'tmuk'      => $row->tmuk->nama,
            'nomor'     => $row->nomor,
            'nomor_pr'  => $row->nomor_pr,
            'tmuk_kode' => $row->tmuk_kode,
            'tgl_buat'  => $row->tgl_buat,
            'total'     => $row->total,
          ];
        }
        $data = [
          'tittle'     => 'Lembar PO STDK',
          'tittle_set' => 'Lembar PO STDK',
          'header_set' => array('LSI','TMUK','Nomor PO','Nomor PR','TMUK Kode','Tanggal Buat','Total'),
          'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'po-barang-trade-ke-lsi':
        $record = TransPyr::select('*')
        ->where('tipe', '001')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_kirim)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        // dd($record);
        $data = [
          'tittle'     => 'PO Barang Trade ke LSI',
          'tittle_set' => 'PO Barang Trade ke LSI',
          'header_set' => array('NO','Kode TMUK','Tanggal','Jumlah Struk','Total Penjualan (Rp)','Total HPP','Profit','Margin'),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => [],
        ];
        $this->printExcel($request, $data);
      break;
      case 'po-barang-non-trade-ke-lsi':
        $record = TransPyr::select('*')
        ->where('tipe', '003')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_kirim)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        $data = [
          'tittle'     => 'PO Barang non trade ke LSI',
          'tittle_set' => 'PO Barang non trade ke LSI',
          'header_set' => array('NO','Kode TMUK','Tanggal','Jumlah Struk','Total Penjualan (Rp)','Total HPP','Profit','Margin'),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => [],
        ];
        $this->printExcel($request, $data);
      break;
      case 'po-barang-trade-ke-vendor-lokal':
        $record = TransPyr::select('*')
        ->where('tipe', '002')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_kirim)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        // dd($record);
        $temp = [];
        foreach($record as $row){
          $temp [] = [
              'lsi'             => $row->tmuk->lsi->nama,
              'tmuk'            => $row->tmuk->nama,
              'nomor_pyr'       => $row->nomor_pyr,
              'tmuk_kode'       => $row->tmuk_kode,
              'vendor_lokal'    => $row->vendor_lokal,
              'tgl_buat'        => $row->tgl_buat,
              'tgl_jatuh_tempo' => $row->tgl_jatuh_tempo,
              'total'           => $row->total,
          ];
        }
        $data = [
          'tittle'     => 'PO Barang Trade ke Vendor Lokal',
          'tittle_set' => 'PO Barang Trade ke Vendor Lokal',
          'header_set' => array('Lsi','Tmuk','Nomor PYR','Tmuk Kode','Vendor Lokal','Tgl Buat','Tgl Jatuh Tempo','Total'),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'lembar-picking-result-pt':
        $record = TransPr::select('*')
        ->where('status', 1)
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tgl_kirim)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        $temp = [];
        foreach ($record as $row) {
          $temp[] = [
            'lsi'       => $row->tmuk->lsi->nama,
            'tmuk'      => $row->tmuk->nama,
            'nomor'     => $row->nomor,
            'tmuk_kode' => $row->tmuk_kode,
            'tgl_buat'  => $row->tgl_buat,
            'tgl_kirim' => $row->tgl_kirim,
            'total'     => $row->total
          ];
        }
        $data = [
          'tittle'     => 'Lembar Picking Result',
          'tittle_set' => 'Lembar Picking Result (PT)',
          'header_set' => array('Lsi','Tmuk','Nomor PO','Tmuk Kode','Tgl Buat','Tgl Kirim','Total'),
          'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'lembar-label-kontainer-cl':
        $record = TransKontainer::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }

        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        $data = [
          'tittle'     => 'Lembar Label Kontainer',
          'tittle_set' => 'Lembar Label Kontainer (CL)',
          'header_set' => array('NO','Kode TMUK','Tanggal','Jumlah Struk','Total Penjualan (Rp)','Total HPP','Profit','Margin'),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => [],
        ];
        $this->printExcel($request, $data);
      break;
      case 'lembar-daftar-muatan-it':
        $record = TransMuatanDetail::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        $temp = [];
        foreach ($record as $row) {
            $temp[] = [
              'lsi'          => $row->tmuk->lsi->nama,
              'tmuk'         => $row->tmuk->nama,
              'tmuk_kode'    => $row->tmuk_kode,
              'nomor_muatan' => $row->nomor_muatan,
              'po_nomor'     => $row->po_nomor,
              'status'       => $row->status,
            ];
        }
        $data = [
          'tittle'     => 'Rekap Service Level',
          'tittle_set' => 'Rekap Service Level (SL)',
          'header_set' => array('Lsi','Tmuk','Tmuk Kode','Nomor Muatan','Nomor Po','Status'),
          'set_width'  => array('A'=>20,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'lembar-surat-jalan-dn':  
        $record = TransMuatanDetail::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        $temp = [];
        foreach ($record as $row) {
          $temp[] =[
            'lsi'          => $row->tmuk->lsi->nama,
            'tmuk'         => $row->tmuk->nama,
            'nomor_muatan' => $row->nomor_muatan,
            'po_nomor'     => $row->po_nomor,
            'tmuk_kode'    => $row->tmuk_kode,
            'status'       => $row->status
          ];
        }
        $data = [
          'tittle'     => 'Lembar Surat Jalan (Dn)',
          'tittle_set' => 'Lembar Surat Jalan (Dn)',
          'header_set' => array('Lsi','Tmuk','Nomor Muatan','Po Nomor','Tmuk Kode','Status'),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);      
      break;
      case 'rekap-delivery':
        $record = TransMuatanDetail::select('*')
        ->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(created_at)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        $temp = [];
        foreach ($record as $row) {
          $temp[] =[
            'lsi'          => $row->tmuk->lsi->nama,
            'tmuk'         => $row->tmuk->nama,
            'nomor_muatan' => $row->nomor_muatan,
            'po_nomor'     => $row->po_nomor,
            'tmuk_kode'    => $row->tmuk_kode,
            'status'       => $row->status
          ];
        }
         $data = [
          'tittle'     => 'Rekap Delivery',
          'tittle_set' => 'Rekap Delivery',
          'header_set' => array('Lsi','Tmuk','Nomor Muatan','Po Nomor','Tmuk Kode','Status'),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'rekap-gr-grn':
         $data = [
          'tittle'     => 'Rekap GR/GRN',
          'tittle_set' => 'Rekap GR/GRN',
          'header_set' => array('NO','Kode TMUK','Tanggal','Jumlah Struk','Total Penjualan (Rp)','Total HPP','Profit','Margin'),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => [],
        ];
        $this->printExcel($request, $data);
      break;
      case 'lembar-konfirmasi-retur-rr':
            $data = [
                'tittle'     => 'Lembar Konfirmasi Retur',
                'tittle_set' => 'Lembar Konfirmasi Retur (RR)',
                'header_set' => array('NO','Kode TMUK','Tanggal','Jumlah Struk','Total Penjualan (Rp)','Total HPP','Profit','Margin'),
                'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20),
                'creator'    => 'Yusup Supriatna',
                'record'     => [],
            ];
            $this->printExcel($request, $data);
      break;

      case 'rekap-service-level-sl':
        $record = TransPo::getServiceLevel();
        $record->whereIn('tmuk_kode', $temp);
        if($request->tanggal_start and $request->tanggal_end){
          $record->whereBetween(\DB::raw('DATE(tanggal_po)'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $record->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        // dd($record->toArray());
        $temp = [];
        foreach ($record as $row) {
          $temp[]= [
            'lsi'        => $row->tmuk->lsi->nama,
            'tmuk'       => $row->tmuk->nama,
            'nomor_po'   => $row->nomor_po,
            'tanggal_po' => $row->tanggal_po,
            'tanggal_pr' => $row->tanggal_pr,
            'tmuk_kode'  => $row->tmuk_kode,
            'nomor_pr'   => $row->nomor_pr,
            'qty_pr'     => $row->qty_pr,
            'qty_po'     => $row->qty_po
          ];
        }
        $data = [
          'tittle'     => 'Rekap Service Level',
          'tittle_set' => 'Rekap Service Level (SL)',
          'header_set' => array('lsi','tmuk','nomor_po','tanggal_po','tanggal_pr','tmuk_kode','nomor_pr','qty_pr','qty_po'),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
      case 'laporan-pembelian-persediaan':
            $record = Tmuk::with('pyr', 'pyr.detailPyr', 'pyr.detailPyr.produks')
                   ->whereIn('kode', $temp);

            $record->whereHas('pyr.detailPyr', function($q) use ($request){
                if($request->tanggal_start && $request->tanggal_end){
                   $q->whereBetween(\DB::raw('DATE(tgl_buat)'),array($request->tanggal_start, $request->tanggal_end));
                }
            });
            $record = $record->get();
            $data_list = [];

            foreach ($record as $tmuk){
                $data_list[] = [$tmuk->kode.'-'.$tmuk->nama,'','','',''];
                foreach ($tmuk->pyr as $pyr){
                    $data_list[] = [$pyr->nomor_pyr,$pyr->tgl_buat,'','',''];
                    $total_price = 0;
                    $total_unit  = 0;
                    $total_all   = 0;
                    
                    foreach($pyr->detailPyr as $dpyr){
                    
                        $total_price += (integer)$dpyr->price;
                        $total_unit  += $dpyr->qty;
                        $total_all   += (integer)$dpyr->price*(integer)$dpyr->qty;
                    
                        $data_list[] = [
                            $dpyr->produk_kode.'-'.$dpyr->produks->nama,
                            $dpyr->unit,
                            number_format((integer)$dpyr->price,2),
                            $dpyr->qty,
                            number_format((integer)$dpyr->price*(integer)$dpyr->qty, 2)];
                    }
                    $data_list[] = ['SUB TOLTAL','',number_format($total_price,2), $total_unit, number_format($total_all,2)];
                }
            }

            $data = [
              'tittle'     => 'Laporan Pembelian Persediaan',
              'tittle_set' => 'Laporan Pembelian Persediaan',
              'header_set' => array('Item Code','Unit','Harga','Quantity Order','Total'),
              'set_width'  => array('A'=>50,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20),
              'creator'    => 'Yusup Supriatna',
              'record'     => $data_list,
            ];
            $this->printExcel($request, $data);
      break;
      default:
        $rangkuman = TransRekapPenjualanTmuk::getDataPersediaan();
        $rangkuman->whereIn('tmuk_kode',  $request->temp);
        if($request->tanggal_start and $request->tanggal_end){
          $rangkuman->whereBetween(\DB::raw('tanggAl'), array($request->tanggal_start, $request->tanggal_end));
        }
        $record = $rangkuman->orderBy(\DB::raw('tmuk_kode'),'DESC')->get();
        $temp = $temp;
        foreach($record as $row){
            $temp[] = [
            'lsi' => $row->tmuk->lsi->nama,
            'tmuk' => $row->tmuk->nama,
            'tmuk_kode' => $row->tmuk_kode,
            'tanggal'   => $row->tanggal,
            'qty'       => $row->qty,
            'total'     => $row->total,
            'harga'     => $row->harga,
            'profit'    => $row->profit,
            'margin'    => $row->margin,
          ];
        }
        $data = [
          'tittle'     => 'Rekap Pembelian Persediaan',
          'tittle_set' => 'Rekap Pembelian Persediaan',
          'header_set' => array('Lsi','Tmuk','Tmuk Kode','Tanggal','Qty','Total','Harga','Profit','Margin'),
          'set_width'  => array('A'=>6,'B'=>20,'C'=>20, 'D'=>40, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20),
          'creator'    => 'Yusup Supriatna',
          'record'     => $temp,
        ];
        $this->printExcel($request, $data);
      break;
    }
  }

  // GENERATE LAPORAN pdf
  public function printPdf($request, $record=''){
    $data         = $record;
    $region       ='All';
    $lsi          ='All';
    $tmuk         ='All';
    $req          = $request->all();
    if($request->region_id){
      $region       = $request['tmukd']->first()->lsi->region->area;
    }
    if($request->lsi_id){
      $lsi          = $request['tmukd']->first()->lsi->nama;
    }
    if($request->tmuk_kode){
      $tmuk         = $request['tmukd']->first()->nama;
    }

    $tahun_fiskal = TahunFiskal::getTahunFiskal();

    $data = [
      'records' => $record,
      'request' => $req,
      'fiskal'  => $tahun_fiskal,
      'region'  => $region,
      'lsi'     => $lsi,
      'tmuk'    => $tmuk,
    ];
    // $content = view('report.laporan-pusat.pembelian.'.$request->types,$data);
    // // return $content;
    // HTML2PDF($content, $data);
      $pdf = \PDF::loadView('report.laporan-pusat.pembelian.'.$request->types, $data);
      return $pdf->setOption('margin-bottom', 5)
      ->setOption('margin-top', 5)
      ->setOption('margin-left', 5)
      ->setOption('margin-right', 5)
      ->inline();
  }

  //GENERATE LAPORAN EXCEL
  public function printExcel($request='', $data=[]){
    
    $Export = Excel::create(isset($data['tittle'])?$data['tittle']:'default', function($excel) use ($request, $data){
        $excel->setTitle(isset($data['tittle'])?$data['tittle']:'default');
        $excel->setCreator(isset($data['creator'])?$data['creator']:'Aplikasi Akuntansi')->setCompany('Lotte');
        $excel->setDescription('Export');

        $excel->sheet(isset($data['tittle'])?$data['tittle']:'default', function($sheet) use ($request, $data){
        if(isset($data['set_width'])){
            $tray = array_keys($data['set_width']);
            $last_cell = end($tray);
        }
        $sheet->row(1, array(
            isset($data['tittle_set'])?$data['tittle_set']:'default'
        ));

        $sheet->row(2, array(
          'Periode : ' . $request->tanggal_start . ' - ' . $request->tanggal_end
        ));
        $sheet->row(3, isset($data['header_set'])?$data['header_set']:[]);

        $row = 4;
        $no =1;
        foreach ($data['record'] as $elm) {
            $sheet->row($row, $elm);
          $row++;
        }
        $sheet->cells('E4'.':'.$last_cell.''.$row, function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
        });
        $sheet->setBorder('A3:'.$last_cell.''.$row, 'thin');
        $sheet->mergeCells('A1:'.$last_cell.'1');
        $sheet->mergeCells('A2:'.$last_cell.'2');
        $sheet->cells('A1:A1', function($cells){
          $cells->setFontSize(14);
          $cells->setFontWeight('bold');
          $cells->setAlignment('center');
        });
        $sheet->cells('A2:'.$last_cell.'2', function($cells){
          $cells->setFontSize(12);
          $cells->setAlignment('center');
        });
        $sheet->cells('A3:'.$last_cell.'3', function($cells){
          $cells->setAlignment('center');
          $cells->setBackground('#999999');
          $cells->setFontColor('#FFFFFF');
        });
        $sheet->setWidth(isset($data['set_width'])?$data['set_width']:[]);
        $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->setWrapText(true);
        $sheet->getStyle('B4:'.$last_cell.''.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top')
        );
        $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'left')
        );
        $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'right')
        );
        $sheet->getStyle('E4:E'.$row)->getAlignment()->applyFromArray(
          array('vertical' => 'top', 'horizontal' => 'right')
        );

        });
    });
    $Export ->download('xls');
  }

}


