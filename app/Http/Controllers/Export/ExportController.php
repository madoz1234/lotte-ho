<?php

namespace Lotte\Http\Controllers\Export;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

use Lotte\Models\Master\Region;
use Lotte\Models\Master\Provinsi;
use Lotte\Models\Master\Kecamatan;
use Lotte\Models\Master\JenisKustomer;
use Lotte\Models\Master\MemberCard;
use Lotte\Models\Master\Kota;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Pajak;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\TipeAset;
use Lotte\Models\Master\Kontainer;
use Lotte\Models\Master\Truk;
use Lotte\Models\Master\Rak;
use Lotte\Models\Master\UnitUom;
use Lotte\Models\Master\Kustomer;
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\Master\JenisAssortment;
use Lotte\Models\Master\TipeBarang;
use Lotte\Models\Master\JenisBarang;
use Lotte\Models\Master\ProdukHarga;
use Lotte\Models\Master\ProdukAssortment;
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Master\TipeCoa;
use Lotte\Models\Master\Coa;
use Lotte\Models\Master\SaldoMinimal;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\Kki;
use Lotte\Models\Trans\TransOpening;
use Lotte\Models\Master\DivisiProduk;
use Lotte\Models\Master\Kategori1;
use Lotte\Models\Master\Kategori2;
use Lotte\Models\Master\Kategori3;
use Lotte\Models\Master\Kategori4;
use Lotte\Models\Master\Point;
use Lotte\Models\Trans\TransRetur;
use Lotte\Models\Trans\TransAkuisisiAset;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;

class ExportController extends Controller
{
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */

    //Export Region
    public function getRegion(Request $request)
    {
        // dd($request->all());
        $region = Region::select('*');
        if ($kode = $request->kode) {
            $region->where('kode', 'like', '%' . $kode . '%');
        }
        if ($area = $request->area) {
            $region->where('area', 'like', '%' . $area . '%');
        }
        $record = $region->get();
        // $record = Region::all();
        
        $ExportRegion = Excel::create('DAFTAR REGION ', function($excel) use ($record){
            $excel->setTitle('Daftar Region');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR REGION ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR REGION '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode',
                    'Nama'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kode,
                        $val->area,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:C1');
                $sheet->mergeCells('A2:C2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>10,'C'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportRegion ->download('xls');
    }

    //Export Provinsi
    public function getProvinsi(Request $request)
    {
        // dd($request->all());
        $records = Provinsi::with('creator')
                         ->select('*');

        // Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        $record = $records->get();
        
        $ExportProvinsi = Excel::create('DAFTAR PROVINSI ', function($excel) use ($record){
            $excel->setTitle('Daftar Provinsi');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR PROVINSI ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR PROVINSI '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Nama'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':B'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:B'.$row, 'thin');
                $sheet->mergeCells('A1:B1');
                $sheet->mergeCells('A2:B2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:B3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportProvinsi ->download('xls');
    }

    //Export Kota
    public function getKota(Request $request)
    {
        // dd($request->all());
        $records = Kota::with('creator')
                         ->select('*');

        // Filters
        if ($provinsi_id = $request->provinsi_id) {
            $records->where('provinsi_id',$provinsi_id);
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        $record = $records->get();
        
        $ExportKota = Excel::create('DAFTAR KOTA ', function($excel) use ($record){
            $excel->setTitle('Daftar Kota');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR KOTA ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR KOTA '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Provinsi',
                    'Kota'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->provinsi->nama,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:C1');
                $sheet->mergeCells('A2:C2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:C2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>30));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportKota ->download('xls');
    }

    //Export Kecamatan
    public function getKecamatan(Request $request)
    {
        // dd($request->all());
        $records = Kecamatan::with('creator')
                         ->select('*');
        // Filters
        if ($kota_id = $request->kota_id) {
            $records->where('kota_id',$kota_id);
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        // dd($request->nama);
        $record = $records->get();
        
        $ExportKecamatan = Excel::create('DAFTAR KECAMATAN ', function($excel) use ($record){
            $excel->setTitle('Daftar Kecamatan');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR KECAMATAN ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR KECAMATAN '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kota',
                    'Kecamatan'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kota->nama,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:C1');
                $sheet->mergeCells('A2:C2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:C2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>30));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportKecamatan ->download('xls');
    }

     //Export AkuisisiAset
    public function getAkuisisiAset(Request $request)
    {
        // dd($request->all());
        // $akuisisi_aset = TransAkuisisiAset::select('*');
        $akuisisi_aset = TransAkuisisiAset::whereIn('status', ['1']);

        if ($tmuk_kode = $request->tmuk_kode) {
            $akuisisi_aset->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
            }
        if ($tipe_id = $request->tipe_id) {
            $akuisisi_aset->where('tipe_id', $tipe_id);
            }
        if ($nama = $request->nama) {
            $akuisisi_aset->where('nama', 'ilike', '%' . $nama . '%');
            }

        $record = $akuisisi_aset->get();
        
        $ExportAkuisisiAset = Excel::create('DAFTAR AKUISISI ASET ', function($excel) use ($record){
            $excel->setTitle('Daftar Akuisisi Aset');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR AKUISISI ASET ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR AKUISISI ASET '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'TMUK',
                    'Tipe Aset',
                    'Nama Aset',
                    'Nomor Seri',
                    'Tanggal Pembelian',
                    'Nilai Pembelian',
                    'Kondisi',
                    ));

                $row = 4;
                $no =1;
                // dd($record);
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->nama,
                        $val->tipe->tipe,
                        $val->nama,
                        $val->no_seri,
                        $val->tanggal_pembelian,
                        $val->nilai_pembelian,
                        // $val->kondisi,
                         $val->uom1_selling_cek==1?'Baru': 'Bekas',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':H'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:H'.$row, 'thin');
                $sheet->mergeCells('A1:H1');
                $sheet->mergeCells('A2:H2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:H3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>10,'C'=>20, 'D'=>80, 'E'=>50, 'F'=>20, 'G'=>20, 'H'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportAkuisisiAset ->download('xls');
    }
    
    //Export Jenis Kustomer
    public function getJenisKustomer(Request $request)
    {
        // dd($request->all());
        $records = JenisKustomer::with('creator')
                         ->select('*')
                         ->orderBy('created_at', 'desc');

        //Filters
        // if ($kode = $request->kode) {
        //     $records->where('kode', 'like', '%' . $kode . '%');
        // }
        if ($jenis = $request->jenis) {
            $records->where('jenis', 'ilike', '%' . $jenis . '%');
        }

        $record = $records->get();
        
        $ExportJenisKustomer = Excel::create('DAFTAR JENIS KUSTOMER ', function($excel) use ($record){
            $excel->setTitle('Daftar Jenis Kustomer');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR Jenis Kustomer ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR JENIS KUSTOMER '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode',
                    'Nama Jenis'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kode,
                        $val->jenis,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:C1');
                $sheet->mergeCells('A2:C2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>10,'C'=>30));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportJenisKustomer ->download('xls');
    }

    //Export MemberCard
    public function getMemberCard(Request $request)
    {
        // dd($request->all());
        $records = MemberCard::select('*')->orderBy('created_at', 'desc');
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        if ($jeniskustomer_id = $request->jeniskustomer_id) {
            $records->where('jeniskustomer_id',$jeniskustomer_id);
        }
        $record = $records->get();
        
        $ExportJenisKustomer = Excel::create('DAFTAR MEMBERCARD ', function($excel) use ($record){
            $excel->setTitle('Daftar MemberCard');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR MemberCard ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR MEMBERCARD '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Nomor',
                    'Jenis Kustomers',
                    'Alamat',
                    'Nama Pemilik',
                    'telepon',
                    'email',
                    'Notes',

                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->nomor,
                        $val->kustomer->jenis,
                        $val->alamat,
                        $val->nama,
                        $val->telepon ? $val->telepon : '-',
                        $val->email ? $val->email : '-',
                        $val->notes ? $val->notes : '-',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':H'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:H'.$row, 'thin');
                $sheet->mergeCells('A1:H1');
                $sheet->mergeCells('A2:H2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:H3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>30,'D'=>80,'E'=>40,'F'=>20, 'G'=>40, 'H'=>20,));
                $sheet->getStyle('B4:H'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:H'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportJenisKustomer ->download('xls');
    }

    //Export Lsi
    public function getLsi(Request $request)
    {
        // dd($request->all());
        $lsi = Lsi::select('*');
        if ($kode = $request->kode) {
            $lsi->where('kode', 'like', '%' . $kode . '%');
        }
        if ($nama = $request->nama) {
            $lsi->where('nama', 'like', '%' . $nama . '%');
        }
        $record = $lsi->get();
        
        $ExportLsi = Excel::create('DAFTAR LSI ', function($excel) use ($record){
            $excel->setTitle('Daftar Lsi');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR LSI ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR LSI '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Region',
                    'Kode Lsi',
                    'Nama Lsi',
                    'Alamat',
                    'Kode Pos',
                    'Telepon',
                    'Email',
                    'Provinsi',
                    'Kota',
                    'Longitude',
                    'Latitude',
                    'NPWP Perusahaan',
                    'Nama Perusahaan',
                    'Akun Escrow',
                    'Kode Bank',
                    'Akun Bank',
                    'Nomor Rekening',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->region->area,
                        $val->kode,
                        $val->nama,
                        $val->alamat,
                        $val->kode_pos ? $val->kode_pos : '-',
                        $val->telepon,
                        $val->email ? $val->email : '-',
                        $val->kota->provinsi->nama,
                        $val->kota->nama,
                        $val->longitude,
                        $val->latitude,
                        $val->pajak->npwp,
                        $val->pajak->nama,
                        $val->rekeningescrow->nama_pemilik,
                        $val->bankescrow->kode_bank,
                        $val->bankescrow->nama,
                        $val->rekeningescrow->nomor_rekening,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':R'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:R'.$row, 'thin');
                $sheet->mergeCells('A1:R1');
                $sheet->mergeCells('A2:R2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:R3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20,'D'=>50,'E'=>80,'F'=>30,'G'=>30,'H'=>30,'I'=>30,'J'=>30,'K'=>30,'L'=>30,'M'=>30,'N'=>40,'O'=>30,'P'=>30,'Q'=>30,'R'=>30));
                $sheet->getStyle('B4:R'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:R'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('E4:E'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'justify')
                );

            });
        });
        $ExportLsi ->download('xls');
    }

    //Export Tmuk
    public function getTmuk(Request $request)
    {
        // dd($request->all());
        // $lsi = Tmuk::with('membercard')->select('*');
        // if ($kode = $request->kode) {
        //     $lsi->where('kode', 'like', '%' . $kode . '%');
        // }
        // if ($nama = $request->nama) {
        //     $lsi->where('nama', 'like', '%' . $nama . '%');
        // }

        $records = Tmuk::with('creator')
                         ->select('*');

        //Filters
        if ($lsi_id = $request->lsi_id) {
            $records->where('lsi_id', 'like', '%' . $lsi_id . '%');
        }
        if ($kode = $request->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        // dd($request->kode);
        $record = $records->get();
        
        $ExportLsi = Excel::create('DAFTAR TMUK ', function($excel) use ($record){
            $excel->setTitle('Daftar Tmuk');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR TMUK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR TMUK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Lsi Induk',
                    'Kode Lsi',
                    'Nama Lsi',
                    'Alamat',
                    'Kode Pos',
                    'Telepon',
                    'Email',
                    'Provinsi',
                    'Kota',
                    'Kecamatan',
                    'Asn',
                    'Auto Approve',
                    'Longitude',
                    'Latitude',
                    'No MemberCard',
                    'Nama MemberCard',
                    'Jenis MemberCard',
                    'Assortment Type',
                    'Gross Area',
                    'Selling Area',
                    'Nama CDE',
                    'Email CDE',
                    'Rencana Pembukaan Toko',
                    'Aktual Pembukaan Toko',
                    'NPWP Perusahaan',
                    'Nama Perusahaan',
                    'Akun Escrow',
                    'Akun Bank Escrow',
                    'Nomor Rekening Escrow',
                    'Akun pribadi Pemilik',
                    'Kode Bank Pemilik',
                    'Nama Bank Pemilik',
                    'Nomor Rekening Pemilik',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    // dd($val->rekening_pemilik_id);
                    $sheet->row($row, array(
                        $no,
                        $val->lsi->nama,
                        $val->kode,
                        $val->nama,
                        $val->alamat,
                        $val->kode_pos ? $val->kode_pos : '-',
                        $val->telepon,
                        $val->email ? $val->email : '-',
                        $val->provinsi->nama,
                        $val->kota->nama,
                        $val->kecamatan->nama,
                        $val->asn==1?'Ya': 'Tidak',
                        $val->auto_approve==1?'Ya': 'Tidak',
                        $val->longitude,
                        $val->latitude,
                        $val->membercard->nomor,
                        $val->membercard->nama,
                        $val->membercard->kustomer->jenis,
                        $val->assortment->nama,
                        $val->gross_area,
                        $val->selling_area,
                        $val->nama_cde,
                        $val->email_cde,
                        $val->rencana_pembukaan,
                        $val->aktual_pembukaan ? $val->aktual_pembukaan : '-',
                        $val->pajak->npwp,
                        $val->pajak->nama,
                        $val->rekeningescrow->nama_pemilik,
                        $val->bankescrow->nama,
                        $val->rekeningescrow->nomor_rekening,
                        // $val->rekeningpemilik->nama_pemilik,
                        // $val->bankpemilik->nama,
                        // $val->bankpemilik->kode_bank,
                        // $val->rekeningpemilik->nomor_rekening,
                        
                        $val->bank_pemilik_id ? $val->rekeningpemilik->nama_pemilik : '-',
                        $val->rekening_pemilik_id ? $val->bankpemilik->kode_bank : '-',
                        $val->bank_pemilik_id ? $val->bankpemilik->nama : '-',
                        $val->rekening_pemilik_id ? $val->rekeningpemilik->nomor_rekening : '-',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':AH'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:AH'.$row, 'thin');
                $sheet->mergeCells('A1:AH1');
                $sheet->mergeCells('A2:AH2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:AH2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:AH3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>40,'C'=>30,'D'=>30,'E'=>110,'F'=>30,'G'=>30,'H'=>30,'I'=>30,'J'=>30,'K'=>30,'L'=>30,'M'=>30,'N'=>30,'O'=>30,'P'=>30,'Q'=>30,'R'=>30,'S'=>30,'T'=>30,'U'=>30,'V'=>30,'W'=>30,'X'=>30,'Y'=>30,'Z'=>30,'AA'=>30,'AB'=>50,'AC'=>30, 'AD'=>30, 'AE'=>30, 'AF'=>40, 'AG'=>30, 'AH'=>30));
                $sheet->getStyle('B4:AH'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:AH'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportLsi ->download('xls');
    }

    //Export Pajak
    public function getPajak(Request $request)
    {
        // dd($request->all());
        $pajak = Pajak::select('*');
        if ($npwp = $request->npwp) {
            $pajak->where('npwp', 'like', '%' . $npwp . '%');
        }
        if ($nama = $request->nama) {
            $pajak->where('nama', 'like', '%' . $nama . '%');
        }
        $record = $pajak->get();
        // $record = Region::all();
        
        $ExportPajak = Excel::create('DAFTAR PAJAK ', function($excel) use ($record){
            $excel->setTitle('Daftar Pajak');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR PAJAK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR PAJAK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Nomor NPWP',
                    'Nama',
                    'Alamat'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->npwp,
                        $val->nama,
                        $val->alamat_npwp,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });

                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>40, 'D'=>80));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getNumberFormat();

            });
        });
        $ExportPajak ->download('xls');
    }

    //Export Bank Escrow
    public function getBankEscrow(Request $request)
    {
        // dd($request->all());
        $pajak = BankEscrow::select('*');
        if ($npwp = $request->npwp) {
            $pajak->where('npwp', 'like', '%' . $npwp . '%');
        }
        if ($nama = $request->nama) {
            $pajak->where('nama', 'like', '%' . $nama . '%');
        }
        $record = $pajak->get();
        // $record = Region::all();
        
        $ExportPajak = Excel::create('DAFTAR BANK ', function($excel) use ($record){
            $excel->setTitle('Daftar Bank');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR BANK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR BANK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tgl. Mulai',
                    'Nama',
                    'Kode Swift',
                    'Kode Bank',
                    'Telepon',
                    'Email',
                    'Kode Pos',
                    'Kota',
                    'Alamat',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tgl_mulai,
                        $val->nama,
                        $val->kode_swift,
                        $val->kode_bank,
                        $val->telepon,
                        $val->email ? $val->email : '-',
                        $val->kode_pos ? $val->kode_pos : '-',
                        $val->kota->nama,
                        $val->alamat,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':J'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:J'.$row, 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:J3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>40,'D'=>20,'E'=>10,'F'=>20,'G'=>30,'H'=>10,'I'=>20, 'J'=>110));
                // $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                // $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                //     array('vertical' => 'top')
                // );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('E4:E'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('F4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('G4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('H4:H'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('I4:I'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('J4:J'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );


            });
        });
        $ExportPajak ->download('xls');
    }

    //Export Rekening Escrow
    public function getRekeningEscrow(Request $request)
    {
        // dd($request->all());
        $rekeningescrow = RekeningEscrow::select('*');
        if ($nama_pemilik = $request->nama_pemilik) {
            $rekeningescrow->where('nama_pemilik', 'like', '%' . $nama_pemilik . '%');
        }
        if ($nomor_rekening = $request->nomor_rekening) {
            $rekeningescrow->where('nomor_rekening', 'like', '%' . $nomor_rekening . '%');
        }
        $record = $rekeningescrow->get();
        // $record = Region::all();
        
        $ExportRekeningEscrow = Excel::create('DAFTAR REKENING BANK ', function($excel) use ($record){
            $excel->setTitle('Daftar Rekening Bank');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR Rekening Bank ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR REKENING BANK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Bank Escrow',
                    'Nomor Rekening',
                    'Nama Pemilik',
                    'Status',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->bankescrow->nama,
                        $val->nomor_rekening,
                        $val->nama_pemilik,
                        $val->status==1?'Aktif': 'Non Aktif',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':E'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:E'.$row, 'thin');
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:E3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>35,'C'=>20,'D'=>60,'E'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportRekeningEscrow ->download('xls');
    }

    //Export Tahun Fiskal
    public function getTahunFiskal(Request $request)
    {
        // dd($request->all());
        $tahunfiskal = TahunFiskal::select('*');
        if ($tgl_awal = $request->tgl_awal) {
            $tahunfiskal->where('tgl_awal', 'like', '%' . $tgl_awal . '%');
        }
        if ($tgl_akhir = $request->tgl_akhir) {
            $tahunfiskal->where('tgl_akhir', 'like', '%' . $tgl_akhir . '%');
        }
        $record = $tahunfiskal->get();
        // $record = Region::all();
        
        $ExportTahunFiskal = Excel::create('DAFTAR TAHUN FISKAL ', function($excel) use ($record){
            $excel->setTitle('Daftar Tahun Fiskal');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR TAHUN FISKAL ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR TAHUN FISKAL '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Mulai Tahun Fiskal',
                    'Selesai Tahun Fiskal',
                    'Status',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tgl_awal,
                        $val->tgl_akhir,
                        $val->status==1?'Berjalan': 'Selesai',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20,'D'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportTahunFiskal ->download('xls');
    }

    //Export Aset Terdepresiasi
    public function getTipeAset(Request $request)
    {
        // dd($request->all());
        $asetterdepresiasi = TipeAset::select('*');
        if ($tipe = $request->tipe) {
            $asetterdepresiasi->where('tipe', 'like', '%' . $tipe . '%');
        }
        $record = $asetterdepresiasi->get();
        // $record = Region::all();
        
        $ExportAsetTerdepresiasi = Excel::create('DAFTAR TIPE ASET TERDEPRESIASI ', function($excel) use ($record){
            $excel->setTitle('Daftar Tipe Aset Terdepresiasi');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR TIPE ASET TERDEPRESIASI ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR TIPE ASET TERDEPRESIASI '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Nama Tipe',
                    'Tingkat Terdepresiasi',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tipe,
                        $val->tingkat_depresiasi,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:C1');
                $sheet->mergeCells('A2:C2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>25));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportAsetTerdepresiasi ->download('xls');
    }

    //Export Kontainer
    public function getKontainer(Request $request)
    {
        // dd($request->all());
        $asetkontainer = Kontainer::select('*');
        if ($nama = $request->nama) {
            $asetkontainer->where('nama', 'like', '%' . $nama . '%');
        }
        $record = $asetkontainer->get();
        // $record = Region::all();
        
        $ExportAsetTerdepresiasi = Excel::create('DAFTAR KONTAINER ', function($excel) use ($record){
            $excel->setTitle('Daftar Kontainer');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR KONTAINER ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR KONTAINER '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Nama Tipe',
                    'Tinggi Luar',
                    'Tinggi Dalam',
                    'Panjang Atas',
                    'Panjang Bawah',
                    'Lebar Atas',
                    'Lebar Bawah',
                    'Volume',
                    'Tipe Box',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->nama,
                        $val->tinggi_luar,
                        $val->tinggi_dalam,
                        $val->panjang_atas,
                        $val->panjang_bawah,
                        $val->lebar_atas,
                        $val->lebar_bawah,
                        $val->volume,
                        $val->tipe_box==1?'InnerBox': 'OutBox',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':J'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:J'.$row, 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:J2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:J3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20,'E'=>20,'F'=>20,'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportAsetTerdepresiasi ->download('xls');
    }

    //Export Truk
    public function getTruk(Request $request)
    {
        // dd($request->all());
        $asettruk = Truk::select('*');
        if ($nama = $request->nama) {
            $asettruk->where('nama', 'like', '%' . $nama . '%');
        }
        $record = $asettruk->get();
        // $record = Region::all();
        
        $ExportTruk = Excel::create('DAFTAR TRUK ', function($excel) use ($record){
            $excel->setTitle('Daftar Truk');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR TRUK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR TRUK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Nama Tipe',
                    'Tinggi',
                    'Panjang',
                    'Lebar',
                    'Kapasitas',
                    'Volume',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tipe,
                        $val->tinggi,
                        $val->panjang,
                        $val->lebar,
                        $val->kapasitas,
                        $val->volume,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':G'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20,'D'=>20,'E'=>20,'F'=>20,'G'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportTruk ->download('xls');
    }

    //Export Truk
    public function getRak(Request $request)
    {
        // dd($request->all());
        $rak = Rak::select('*');
        if ($tipe_rak = $request->tipe_rak) {
            $rak->where('tipe_rak', 'like', '%' . $tipe_rak . '%');
        }
        $record = $rak->get();
        // $record = Region::all();
        
        $ExportRak = Excel::create('DAFTAR RAK ', function($excel) use ($record){
            $excel->setTitle('Daftar Rak');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR RAK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR RAK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tipe Rak',
                    'Shelving',
                    'Tinggi Rak',
                    'Panjang Rak',
                    'Lebar Rak',
                    'Hanger',
                    'Tinggi Hanger',
                    'Panjang Hanger',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tipe_rak,
                        $val->shelving,
                        $val->tinggi_rak,
                        $val->panjang_rak,
                        $val->lebar_rak,
                        $val->hanger==1?'Ya': 'Tidak',
                        $val->tinggi_hanger ? $val->tinggi_hanger : '-',
                        $val->panjang_hanger ? $val->panjang_hanger : '-',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':I'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:I'.$row, 'thin');
                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:I3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportRak ->download('xls');
    }

    //Export Unit Uom
    public function getUnitUom(Request $request)
    {
        // dd($request->all());
        $asetterdepresiasi = UnitUom::select('*');
        if ($name = $request->name) {
            $asetterdepresiasi->where('name', 'like', '%' . $name . '%');
        }
        $record = $asetterdepresiasi->get();
        // $record = Region::all();
        
        $ExportUnitUom = Excel::create('DAFTAR UNIT UOM ', function($excel) use ($record){
            $excel->setTitle('Daftar Unit UOM');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR UNIT UOM ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR UNIT UOM'
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Unit Uom'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->nama
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':B'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:B'.$row, 'thin');
                $sheet->mergeCells('A1:B1');
                $sheet->mergeCells('A2:B2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:B3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportUnitUom ->download('xls');
    }

    //Export Kustomer
    public function getKustomer(Request $request)
    {
        // dd($request->all());
        $records = JenisKustomer::with('creator')
                         ->select('*')
                         ->orderBy('created_at', 'desc');
        if ($name = $request->name) {
            $records->where('name', 'like', '%' . $name . '%');
        }
        $record = $records->get();
        // $record = Region::all();
        
        $ExportJenisKustomer = Excel::create('DAFTAR KUSTOMER ', function($excel) use ($record){
            $excel->setTitle('Daftar Kustomer');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR KUSTOMER ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR KUSTOMER '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'TMUK Induk',
                    'Kode Kustomer',
                    'Jenis Kustomer',
                    'Nomor MemberCard',
                    'Nama Pemilik',
                    'alamat',
                    'Kode Pos',
                    'Kota',
                    'Kecamatan',
                    'No Telepon',
                    'Email',
                    'Logitude',
                    'Latitude',
                    'Nomor NPWP',
                    'Nama Perusahaan',
                    'Limit Kredit',
                    'Persen Diskon',
                    'Cara Pembayaran',
                    'Status Kredit',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    //cara pembayaran
                    if($val->cara_pembayaran == '1'){
                        $pembayaran = 'Tunai';
                    }else if($val->cara_pembayaran == '2'){
                        $pembayaran = 'Jatuh Tempo Tgl 15  Bulan Berikutnya';
                    }
                    if ($val->cara_pembayaran == '3'){
                        $pembayaran = 'Jatuh Tempo Di Akhir Bulan Berikutnya';
                    }else if ($val->cara_pembayaran == '4') {
                        $pembayaran = 'Jatuh Tempo Dalam 10 hari';
                    }

                    //status_kredit
                    if($val->status_kredit == '1'){
                        $kredit = 'No More Work Until Payment Recaived';
                    }else if($val->status_kredit == '2'){
                        $kredit = 'In LiQuidation';
                    }
                    if ($val->status_kredit == '3'){
                        $kredit = 'Good History';
                    }else if ($val->status_kredit == '4') {
                        $kredit = 'Harap Pilih Salah Satu';
                    }
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->nama,
                        $val->kode,
                        $val->kustomer->jenis,
                        $val->membercard->nomor,
                        $val->membercard->nama,
                        $val->alamat,
                        $val->kode_pos,
                        $val->kota->nama,
                        $val->kecamatan->nama,
                        $val->membercard->telepon,
                        $val->membercard->email,
                        $val->longitude,
                        $val->latitude,
                        $val->pajak->npwp,
                        $val->pajak->nama,
                        $val->limit_kredit,
                        $val->persen_diskon.'%',
                        $pembayaran,
                        $kredit,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':T'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:T'.$row, 'thin');
                $sheet->mergeCells('A1:T1');
                $sheet->mergeCells('A2:T2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:T2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:T3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20,'D'=>20,'E'=>20,'F'=>20,'G'=>100,'H'=>20,'I'=>20,'J'=>20,'K'=>20,'L'=>20,'M'=>20,'N'=>20,'O'=>20,'P'=>20,'Q'=>20,'R'=>20,'S'=>40,'T'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('G4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'justify')
                );

            });
        });
        $ExportJenisKustomer ->download('xls');
    }

    //Export Vendor Lokal TMUK
    public function getVendorTmuk(Request $request)
    {
        // dd($request->all());
        $vendortmuk = VendorLokalTmuk::select('*');
        if ($kode = $request->kode) {
            $vendortmuk->where('kode', 'like', '%' . $kode . '%');
        }
        if ($nama = $request->nama) {
            $vendortmuk->where('nama', 'like', '%' . $nama . '%');
        }
        $record = $vendortmuk->get();
        
        $ExportVendorTmuk = Excel::create('DAFTAR VENDOR LOKAL TMUK ', function($excel) use ($record){
            $excel->setTitle('Daftar Vendor Lokal Tmuk');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR VENDOR LOKAL TMUK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR VENDOR LOKAL TMUK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode Vendor Lokal',
                    'Nama Vandor Lokal',
                    'Alamat',
                    // 'Provinsi',
                    'Kota',
                    'Kecamatan',
                    'Kode Pos',
                    'Telepon',
                    'Email',
                    'Top',
                    'Lead Time',
                    'Jadwal Order Senin',
                    'Jadwal Order Selasa',
                    'Jadwal Order Rabu',
                    'Jadwal Order Kamis',
                    'Jadwal Order Jumat',
                    'Jadwal Order Sabtu',
                    'Jadwal Order Minggu',
                    'Contact Person',
                    'NPWP Perusahaan',
                    'Nama Perusahaan',
                    // 'Akun Escrow',
                    'Kode Bank',
                    'Akun Bank',
                    'Nomor Rekening',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kode,
                        'Vendor Lokal - '.$val->nama,
                        $val->alamat,
                        $val->kota->nama,
                        $val->kecamatan->nama,
                        $val->kode_pos,
                        $val->telepon,
                        $val->email,
                        $val->top.' Hari',
                        $val->lead_time.' Hari',
                        $val->order_senin==1?'Order': 'Tidak Order',
                        $val->order_selasa==2?'Order': 'Tidak Order',
                        $val->order_rabu==3?'Order': 'Tidak Order',
                        $val->order_kamis==4?'Order': 'Tidak Order',
                        $val->order_jumat==5?'Order': 'Tidak Order',
                        $val->order_sabtu==6?'Order': 'Tidak Order',
                        $val->order_minggu==7?'Order': 'Tidak Order',
                        $val->contact_person,
                        $val->pajak_id==null ? '' : $val->pajak->npwp,
                        $val->pajak_id==null ? '' : $val->pajak->nama,
                        // $val->rekeningescrow->nama_pemilik,
                        $val->bankescrow->kode_bank,
                        $val->bankescrow->nama,
                        $val->rekeningescrow->nomor_rekening,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':X'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:X'.$row, 'thin');
                $sheet->mergeCells('A1:X1');
                $sheet->mergeCells('A2:X2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:X3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>40,'D'=>100,'E'=>20,'F'=>30,'G'=>30,'H'=>30,'I'=>30,'J'=>30,'K'=>30,'L'=>30,'M'=>30,'N'=>30,'O'=>30,'P'=>30,'Q'=>30,'R'=>30,'S'=>30,'T'=>30,'U'=>30,'V'=>30,'W'=>30, 'X'=>30));
                $sheet->getStyle('B4:Y'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:Y'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'justify')
                );

            });
        });
        $ExportVendorTmuk ->download('xls');
    }

    //Export Tamplate Produk GMD
    public function getTamplateGmd(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\produk-gmd.xlsx', function($reader) use ($filter) {
            // get data logsheet
           
        })->download('xls');
    }

    //Export Tamplate Produk
    public function getTamplateProduk(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\produk-gmd.xlsx', function($reader) use ($filter) {
            // get data logsheet
           
        })->download('xls');
    }


    //Export Tamplate Aktifasi Produk
    public function getTamplateProdukAssortment(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\produk-assortment.xls', function($reader) use ($filter) {
            // get data logsheet
           
        })->download('xls');
    }

    //Export Tamplate Aktifasi Produk
    public function getTamplateProdukTmuk(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\produk-tmuk.xls', function($reader) use ($filter) {
            // get data logsheet
           
        })->download('xls');
    }

    //Export Tamplate Opening Toko
    public function getTamplateOpeningToko(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\opening-toko.xlsx', function($reader) use ($filter) {
            // get data logsheet
           
        })->download('xls');
    }

     //Export Tamplate DPO
    public function getTamplateDpr(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\dpr.xlsx', function($reader) use ($filter) {
            // get data logsheet
           
        })->download('xls');
    }

     //Export Tamplate DPYR
    public function getTamplateDpyr(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\dpyr.xlsx', function($reader) use ($filter) {
            // get data logsheet
           
        })->download('xls');
    }

    //Export Tamplate Kustomer
    public function getTamplateKustomer(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\kustomer.xlsx', function($reader) use ($filter) {
            // get data logsheet
           
        })->download('xls');
    }

    //Export Tamplate Aktifasi Produk
    public function getTamplateKki(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\kki.xls', function($reader) use ($filter) {
            // get data logsheet
           
        })->download('xls');
    }

    //Export Assortment Type
    public function getAssortmentType(Request $request)
    {
        // dd($request->all());
        $assortment_type = JenisAssortment::select('*');
        if ($nama = $request->nama) {
            $assortment_type->where('nama', 'like', '%' . $nama . '%');
        }

        $record = $assortment_type->get();
        // $record = Region::all();
        
        $ExportAssortmentType = Excel::create('Assortment Type', function($excel) use ($record){
            $excel->setTitle('Daftar Assortment Type');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR ASSORTMENT TYPE ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR ASSSORTMENT TYPE '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Assortment'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':B'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:B'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:B3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportAssortmentType ->download('xls');
    }

    //Export Tipe Barang
    public function getTipeBarang(Request $request)
    {
        // dd($request->all());
        $tipe_barang = TipeBarang::select('*');
        if ($nama = $request->nama) {
            $tipe_barang->where('nama', 'like', '%' . $nama . '%');
        }

        $record = $tipe_barang->get();
        
        $ExportTipeBarang = Excel::create(' Tipe Barang', function($excel) use ($record){
            $excel->setTitle('Daftar Tipe Barang');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTA TIPE BARANG ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR TIPE BARANG '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode',
                    'Nama'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kode,
                        $val->nama
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20, 'C'=>20,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportTipeBarang ->download('xls');
    }

    //Export Jenis Barang
    public function getJenisBarang(Request $request)
    {
        // dd($request->all());
        $tipe_barang = JenisBarang::select('*');
        if ($nama = $request->nama) {
            $tipe_barang->where('nama', 'like', '%' . $nama . '%');
        }

        $record = $tipe_barang->get();
        
        $ExportJenisBarang = Excel::create(' Jenis Barang', function($excel) use ($record){
            $excel->setTitle('Daftar Jenis Barang');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTA JENIS BARANG ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR JENIS BARANG '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode',
                    'Jenis'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kode,
                        $val->jenis
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20, 'C'=>30,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportJenisBarang ->download('xls');
    }

    //Export Harga
    public function getHarga(Request $request)
    {
        // dd($request->all());
        $records = ProdukHarga::with('produk')
                             ->select('*');
        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }
        if ($produk_kode = $request->produk_kode) {
            $records->where('produk_kode', 'like', '%' . $produk_kode . '%');
        }
        if ($nama = $request->nama) {
            $records->whereHas('produk', function ($query) use ($nama){
                $query->where('nama', 'ilike', '%' . $nama . '%');
            });
        }
        // dd($request->nama);
        $record = $records->get();
        // test
        // $records = ProdukHarga::with('creator', 'produk')
        //                  ->select('*');

        // if ($produk_kode = $request->produk_kode) {
        //     $records->where('produk_kode', 'like', '%' . $produk_kode . '%');
        // }
        // if ($tmuk_kode = $request->tmuk_kode) {
        //     $records->where('tmuk_kode',$tmuk_kode);
        // }
        
        // if ($nama = $request->nama) {
        //     $records->whereHas('produk', function ($query) use ($nama){
        //         $query->where('nama', 'ilike', '%' . $nama . '%');
        //     });
        // }
        // $record = $records->get();
        // test
        
        $ExportHargaTmuk = Excel::create('Harga Produk', function($excel) use ($record){
            $excel->setTitle('Daftar Harga Produk');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR HARGA PRODUK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR HARGA PRODUK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode Produk',
                    'Nama Produk',
                    'Convert',
                    'Nama TMUK',
                    'Cost price',
                    'Suggest price',
                    'Change price',
                    'Margin amount',
                    'MAP',
                    'GMD price',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->produk_kode,
                        $val->produk->nama,
                        $val->produksetting->uom2_conversion,
                        $val->tmuk->nama,
                        $val->cost_price,
                        $val->suggest_price,
                        $val->change_price,
                        $val->margin_amount,
                        $val->map,
                        $val->gmd_price,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('D4'.':K'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:K'.$row, 'thin');
                $sheet->mergeCells('A1:K1');
                $sheet->mergeCells('A2:K2');
                $sheet->cells('A1:K1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:K2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:K3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20, 'C'=>50,'D'=>30,'E'=>20,'F'=>20,'G'=>20,'H'=>20,'I'=>20,'J'=>20,'K'=>20,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportHargaTmuk ->download('xls');
    }

    //Export Produk Assortment
    public function getProdukAssortment(Request $request)
    {
        // dd($request->all());
        // $record = ProdukAssortment::select('*');
        $record = ProdukAssortment::with('creator', 'produk', 'assortment', 'produksetting')
                         ->select('ref_produk_assortment.*');
        //Filters
        if ($produk_kode = $request->produk_kode) {
            $record->where('produk_kode',$produk_kode);
        }
        if ($assortment_type_id = $request->assortment_type_id) {
            $record->where('assortment_type_id',$assortment_type_id);
        }
        
        if ($nama = $request->nama) {
            $record->whereHas('produk', function ($query) use ($nama){
                $query->where('nama', 'ilike', '%' . $nama . '%');
            });
        }

        if ($tipe_barang_kode = $request->tipe_barang_kode) {
            $record->whereHas('produksetting', function ($query) use ($tipe_barang_kode){
                $query->where('tipe_barang_kode', 'like', '%' . $tipe_barang_kode . '%');
            });
        }

        if ($jenis_barang_kode = $request->jenis_barang_kode) {
            $record->whereHas('produksetting', function ($query) use ($jenis_barang_kode){
                $query->where('jenis_barang_kode', 'like', '%' . $jenis_barang_kode . '%');
            });
        }

        $record = $record->get();
        
        $ExportAssortmentType = Excel::create('Produk Assortment', function($excel) use ($record){
            $excel->setTitle('Daftar Produk Assortment');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR PRODUK ASSSORTMENT ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR PRODUK ASSSORTMENT '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode Produk',
                    'Nama Produk',
                    'Assortment Type',
                    'Type Barang',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        isset($val->produk_kode) ? $val->produk_kode : '-',
                        isset($val->produk_kode) ? $val->produk->nama : '-',
                        isset($val->assortment_type_id) ? $val->assortment->nama : '-',
                        $val->produksetting->tipe_produk==1?'Produk Gmd': 'Produk Non Gmd',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':E'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:E'.$row, 'thin');
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:E2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:E3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20, 'C'=>50, 'D'=>20, 'E'=>20,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportAssortmentType ->download('xls');
    }

     //Export Produk Assortment
    public function getProdukTmuk(Request $request)
    {
        // dd($request->all());
        // $record = ProdukTmuk::select('*');
        $record = ProdukTmuk::with('creator','produk', 'tmuk', 'produksetting')
                         ->select('*');
        //Filters
        if ($produk_kode = $request->produk_kode) {
            $record->where('produk_kode', 'like', '%' . $produk_kode . '%');
        }
        
        if ($nama = $request->nama) {
            $record->whereHas('produk', function ($query) use ($nama){
                $query->where('nama', 'ilike', '%' . $nama . '%');
            });
        }
        if ($tmuk_kode = $request->tmuk_kode) {
            $record->where('tmuk_kode',$tmuk_kode);
        }

        if ($tipe_barang_kode = $request->tipe_barang_kode) {
            $record->whereHas('produksetting', function ($query) use ($tipe_barang_kode){
                $query->where('tipe_barang_kode', 'like', '%' . $tipe_barang_kode . '%');
            });
        }

        if ($jenis_barang_kode = $request->jenis_barang_kode) {
            $record->whereHas('produksetting', function ($query) use ($jenis_barang_kode){
                $query->where('jenis_barang_kode', 'like', '%' . $jenis_barang_kode . '%');
            });
        }

        $flag = $request->flag;
        if ($flag != '') {
            $record->where('flag', $flag);
        }

        $record = $record->get();
        
        $ExportAssortmentType = Excel::create('Produk TMUK', function($excel) use ($record){
            $excel->setTitle('Daftar Produk TMUK');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR PRODUK TMUK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR PRODUK TMUK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'TMUK',
                    'Kode Produk',
                    'Status',
                    'Nama Produk',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        isset($val->tmuk_kode) ? $val->tmuk_kode : '-',
                        isset($val->produk_kode) ? $val->produk_kode : '-',
                        $val->flag==0?'Aktif': 'Non Aktif',
                        isset($val->produk_kode) ? $val->produk->nama : '-',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':E'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:E'.$row, 'thin');
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:E2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:E3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20, 'C'=>20, 'D'=>15, 'E'=>80,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportAssortmentType ->download('xls');
    }

    //Export Type Coa
    public function getTypeCoa(Request $request)
    {
        $record = Coa::with('coaParent')->where('parent_kode' , NULL)->get();
        
        $ExportCoa = Excel::create('COA ', function($excel) use ($record){
            $excel->setTitle('COA');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('COA ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'COA '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    // 'Tipe',
                    'Kode',
                    'Nama',
                    'status'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        // $val->tipe_coa_id,
                        $val->kode,
                        $val->nama,
                        // $val->status,
                        $val->status==1?'Aktif': 'Non-Aktif',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>50, 'D'=>30,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'left')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );


            });
        });
        $ExportCoa ->download('xls');
    }

    //Export Coa 1
    public function getCoa1(Request $request)
    {
        $record = Coa::with('coaParent')->where('parent_kode' , NULL)->get();
        
        $ExportCoa = Excel::create('COA 1 ', function($excel) use ($record){
            $excel->setTitle('COA 1');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('COA 1 ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'COA 1 '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tipe',
                    'Kode',
                    'Nama'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->type->nama,
                        $val->kode,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>20, 'D'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );


            });
        });
        $ExportCoa ->download('xls');
    }

    //Export Kat 1
    public function getKat1(Request $request)
    {
        // $record = Coa::with('coaParent')->where('parent_kode' , NULL)->get();
        $record = Kategori1::select('*')->get();

        // dd($record);
        
        $ExportCoa = Excel::create('Kategori 1 ', function($excel) use ($record){
            $excel->setTitle('Kategori 1');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Kategori 1 ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'Kategori 1 '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Divisi',
                    'Kode',
                    'Nama'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->divisi->nama,
                        $val->kode,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>20, 'D'=>30));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );


            });
        });
        $ExportCoa ->download('xls');
    }

    //Export Kat 1
    public function getKat2(Request $request)
    {
        // $record = Coa::with('coaParent')->where('parent_kode' , NULL)->get();
        $record = Kategori2::select('*')->get();

        // dd($record);
        
        $ExportCoa = Excel::create('Kategori 2 ', function($excel) use ($record){
            $excel->setTitle('Kategori 2');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Kategori 2 ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'Kategori 2 '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Divisi',
                    'Kode',
                    'Nama'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kategori1->nama,
                        $val->kode,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>20, 'D'=>50));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );


            });
        });
        $ExportCoa ->download('xls');
    }

    //Export Kat 1
    public function getKat3(Request $request)
    {
        // $record = Coa::with('coaParent')->where('parent_kode' , NULL)->get();
        $record = Kategori3::select('*')->get();

        // dd($record);
        
        $ExportCoa = Excel::create('Kategori 3 ', function($excel) use ($record){
            $excel->setTitle('Kategori 3');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Kategori 3 ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'Kategori 3 '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Divisi',
                    'Kode',
                    'Nama'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kategori2->nama,
                        $val->kode,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>20, 'D'=>50));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );


            });
        });
        $ExportCoa ->download('xls');
    }

    //Export Kat 1
    public function getKat4(Request $request)
    {
        // $record = Coa::with('coaParent')->where('parent_kode' , NULL)->get();
        $record = Kategori4::select('*')->get();

        // dd($record);
        
        $ExportCoa = Excel::create('Kategori 4 ', function($excel) use ($record){
            $excel->setTitle('Kategori 4');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Kategori 4 ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'Kategori 4 '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Divisi',
                    'Kode',
                    'Nama'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kategori3->nama,
                        $val->kode,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>20, 'D'=>50));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );


            });
        });
        $ExportCoa ->download('xls');
    }

    //Export Konfirmasi Retur
    public function getKonfirmasiRetur(Request $request)
    {
        // $record = Coa::with('coaParent')->where('parent_kode' , NULL)->get();
        $record = TransRetur::select('*')->get();

        // dd($record);
        
        $ExportCoa = Excel::create('Konfirmasi Retur ', function($excel) use ($record){
            $excel->setTitle('Konfirmasi Retur');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Konfirmasi Retur ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'Konfirmasi Retur '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal RR',
                    'Nomor PO',
                    'Nomor RR',
                    'Tmuk',
                    'Nilai Retur'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->created_at,
                        $val->nomor_po,
                        $val->nomor_retur,
                        $val->tmuk->nama,
                        $val->nomor_po,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>30,'D'=>30,'E'=>30,'F'=>30,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );


            });
        });
        $ExportCoa ->download('xls');
    }
    //Export divisi
    public function getDivisi(Request $request)
    {
        // $record = Coa::with('coaParent')->where('parent_kode' , NULL)->get();
        $record = DivisiProduk::select('*')->get();

        // dd($record);
        
        $ExportCoa = Excel::create('Divisi ', function($excel) use ($record){
            $excel->setTitle('Divisi');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Divisi ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'Divisi '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode',
                    'Nama'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kode,
                        $val->nama,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:C1');
                $sheet->mergeCells('A2:C2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>30,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );


            });
        });
        $ExportCoa ->download('xls');
    }

    //Export Saldo Minimal
    public function getSaldoMinimal(Request $request)
    {
        // dd($request->all());
        $saldominimal = SaldoMinimal::select('*');
        
        $record = $saldominimal->get();
        
        $ExportSaldoMinimal = Excel::create('DAFTAR SALDO MINIMAL MENGENDAP ', function($excel) use ($record){
            $excel->setTitle('Daftar Saldo Minimal Mengendap');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR SALDO MINIMAL MENGENDAP ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR SALDO MINIMAL MENGENDAP '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tahun Fiskal',
                    'Saldo Minimal Mengendap (Rp)'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tahunfiskal->tgl_awal,
                        rupiah($val->saldo_minimal_mengendap)
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:C1');
                $sheet->mergeCells('A2:C2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:C2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20, 'C'=>40,));
                $sheet->getStyle('B4:B'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'right')
                );

            });
        });
        $ExportSaldoMinimal ->download('xls');
    }

    //Export Saldo Minimal
    public function getPoint(Request $request)
    {
        // dd($request->all());
        $point = Point::select('*');

        // dd($point);
        
        $record = $point->get();
        
        $ExportSaldoMinimal = Excel::create('POINT ', function($excel) use ($record){
            $excel->setTitle('POINT');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('POINT ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'POINT '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tahun Berlaku',
                    'Kelipatan (Rp)',
                    'Faktor Konversi',
                    'Faktor Reedem (Point/Rp)',
                    'Status',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tgl_berlaku,
                        $val->konversi,
                        $val->faktor_konversi,
                        $val->faktor_reedem,
                        // $val->status,
                        $val->status==1?'Aktif': 'Non-Aktif',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:F2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20, 'C'=>40, 'D'=>20, 'E'=>40, 'F'=>20,));
                $sheet->getStyle('B4:B'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'right')
                );

            });
        });
        $ExportSaldoMinimal ->download('xls');
    }

    //Export Upload Produk
    public function getUploadProduk(Request $request)
    {
        // dd($request->all());
        $produkgmd = Produk::with('creator')->whereHas('produksetting', function($query){
                                                $query->where('tipe_produk', 1);
                                            })->get();
 
        if ($kode = $request->kode) {
            $produkgmd = Produk::where('kode', 'like', '%' . $kode . '%')
                                            ->whereHas('produksetting', function($q){
                                                $q->where('tipe_produk', 1);
                                            })->get();
        }
        if ($nama = $request->nama) {
            $produkgmd = Produk::with('creator')->where('nama', 'like', '%' . $nama . '%');
        }
        if ($bumun_nm = $request->bumun_nm) {
            $produkgmd = Produk::with('creator')->where('bumun_nm',$bumun_nm);
        }
        if ($l1_nm = $request->l1_nm) {
            $produkgmd = Produk::with('creator')->where('l1_nm',$l1_nm);
        }
        $record = $produkgmd;
        
        $ExportUploadProduk = Excel::create('DAFTAR PRODUK GMD ', function($excel) use ($record){
            $excel->setTitle('Daftar Produk Gmd');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR PRODUK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR PRODUK GMD '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode',
                    'Nama',
                    'Tipe Produk',
                    'Kode Divisi',
                    'Nama Divisi',
                    'Kode Kategori 1',
                    'Nama Kategori 1',
                    'Kode Kategori 2',
                    'Nama Kategori 2',
                    'Kode Kategori 3',
                    'Nama Kategori 3',
                    'Kode Kategori 4',
                    'Nama Kategori 4',
                    'width',
                    'length',
                    'height',
                    'wg',
                    'inner_width',
                    'inner_length',
                    'inner_height',
                    'inner_wg',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kode,
                        $val->nama,
                        'Produk GMD',
                        $val->bumun_cd,
                        $val->bumun_nm,
                        $val->l1_cd,
                        $val->l1_nm,
                        $val->l2_cd,
                        $val->l2_nm,
                        $val->l3_cd,
                        $val->l3_nm,
                        $val->l4_cd,
                        $val->l4_nm,
                        $val->width,
                        $val->length,
                        $val->height,
                        $val->wg,
                        $val->inner_width,
                        $val->inner_length,
                        $val->inner_height,
                        $val->inner_wg,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('D4'.':V'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:V'.$row, 'thin');
                $sheet->mergeCells('A1:V1');
                $sheet->mergeCells('A2:V2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:V3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>50,'D'=>15,'E'=>20,'F'=>20,'G'=>20,'H'=>40,'I'=>20,'J'=>40,'K'=>20,'L'=>40,'M'=>20,'N'=>40,'O'=>20,'P'=>20,'Q'=>20,'R'=>10,'S'=>15,'T'=>15,'U'=>15, 'V'=>10));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportUploadProduk ->download('xls');
    }

    //Export Upload Non Produk
    public function getUploadNonProduk(Request $request)
    {
        // dd($request->all());
        $rak = ProdukSetting::whereIn('tipe_barang_kode', ['004', '003', '002']);
        // $rak = ProdukSetting::with('creator', 'produk', 'tipebarang')->select('*');
        // if ($tipe_rak = $request->tipe_rak) {
        //     $rak->where('tipe_rak', 'like', '%' . $tipe_rak . '%');
        // }
        if ($produk_kode = $request->produk_kode) {
            $rak->where('produk_kode', 'like', '%' . $produk_kode . '%');
        }
        if ($tipe_barang_kode = $request->tipe_barang_kode) {
            $rak->where('tipe_barang_kode',$tipe_barang_kode);
        }
        $record = $rak->get();
        
        $ExportRak = Excel::create('DAFTAR PRODUK NON GMD', function($excel) use ($record){
            $excel->setTitle('Daftar Produk Non GMD');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR PRODUK NON GMD', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR PRODUK NON GMD'
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Produk Kode',
                    'Nama Produk',
                    'Tipe Produk',
                    'Tipe Barang',
                    'Jenis Barang',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    //tipe barang
                    if($val->tipe_barang_kode == '001'){
                        $tipe = 'Trade Lotte';
                    }else if($val->tipe_barang_kode == '002'){
                        $tipe = 'Trade Non-Lotte';
                    }
                    if ($val->tipe_barang_kode == '003'){
                        $tipe = 'Non Trade Lotte';
                    }else if ($val->tipe_barang_kode == '004') {
                        $tipe = 'Non Trade Non-Lotte';
                    }

                    //jenis barang
                    if($val->jenis_barang_kode == '001'){
                        $jenis = 'Food';
                    }else if($val->jenis_barang_kode == '002'){
                        $jenis = 'Non-Food';
                    }
                    if ($val->jenis_barang_kode == '003'){
                        $jenis = 'Biaya-biaya';
                    }else if ($val->jenis_barang_kode == '004') {
                        $jenis = 'Aset';
                    }

                    $sheet->row($row, array(
                        $no,
                        $val->produk_kode,
                        $val->produk->nama,
                        'Produk Non GMD',
                        $tipe,
                        $jenis,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('D4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:F1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:F2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>80, 'D'=>20, 'E'=>20, 'F'=>20,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportRak ->download('xls');
    }

    //Export Produk Setting
    public function getProduk(Request $request)
    {
        // dd($request->all());
        $getproduksetting = ProdukSetting::with('creator', 'produk', 'tipebarang')->select('*');
        if ($produk_kode = $request->produk_kode) {
            $getproduksetting->where('produk_kode', 'like', '%' . $produk_kode . '%');
        }

        if ($nama = $request->nama) {
            $getproduksetting->whereHas('produk', function ($query) use ($nama){
                $query->where('nama', 'ilike', '%' . $nama . '%');
            });
        }

        $tipe_produk = $request->tipe_produk;
        if ($tipe_produk != '') {
            $getproduksetting->where('tipe_produk', $tipe_produk);
        }

        if ($tipe_barang_kode = $request->tipe_barang_kode) {
            $getproduksetting->where('tipe_barang_kode',$tipe_barang_kode);
        }

        if ($jenis_barang_kode = $request->jenis_barang_kode) {
            $getproduksetting->where('jenis_barang_kode',$jenis_barang_kode);
        }

        if ($bumun_nm = $request->bumun_nm) {
            $getproduksetting->whereHas('produk', function ($query) use ($bumun_nm){
                $query->where('bumun_nm', 'like', '%' . $bumun_nm . '%');
            });
        }

        if ($l1_nm = $request->l1_nm) {
            $getproduksetting->whereHas('produk', function ($query) use ($l1_nm){
                $query->where('l1_nm', 'like', '%' . $l1_nm . '%');
            });
        }
        $record = $getproduksetting->get();
        // $record = Region::all();
        
        $ExportGetProdukSetting = Excel::create('DAFTAR PRODUK ', function($excel) use ($record){
            $excel->setTitle('Daftar Produk');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR PRODUK ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR PRODUK '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Produk Kode',
                    'Nama Produk',
                    'Tipe Produk',
                    'Tipe Barang',
                    'Jenis Barang',
                    'Locked Price',
                    'Margin',
                    'UOM 1 satuan',
                    'UOM 1 selling unit',
                    'UOM 1 order unit',
                    'UOM 1 receiving unit',
                    'UOM 1 return unit',
                    'UOM 1 inventory unit',
                    'UOM 1 barcode',
                    'UOM 1 conversion',
                    'UOM 1 width',
                    'UOM 1 height',
                    'UOM 1 length',
                    'UOM 1 weight',
                    'UOM 1 boxtype',

                    'UOM 2 satuan',
                    'UOM 2 selling unit',
                    'UOM 2 order unit',
                    'UOM 2 receiving unit',
                    'UOM 2 return unit',
                    'UOM 2 inventory unit',
                    'UOM 2 barcode',
                    'UOM 2 conversion',
                    'UOM 2 width',
                    'UOM 2 height',
                    'UOM 2 length',
                    'UOM 2 weight',
                    'UOM 2 boxtype',

                    'UOM 3 satuan',
                    'UOM 3 selling unit',
                    'UOM 3 order unit',
                    'UOM 3 receiving unit',
                    'UOM 3 return unit',
                    'UOM 3 inventory unit',
                    'UOM 3 barcode',
                    'UOM 3 conversion',
                    'UOM 3 width',
                    'UOM 3 height',
                    'UOM 3 length',
                    'UOM 3 weight',
                    'UOM 3 boxtype',

                    'UOM 4 satuan',
                    'UOM 4 selling unit',
                    'UOM 4 order unit',
                    'UOM 4 receiving unit',
                    'UOM 4 return unit',
                    'UOM 4 inventory unit',
                    'UOM 4 barcode',
                    'UOM 4 conversion',
                    'UOM 4 width',
                    'UOM 4 height',
                    'UOM 4 length',
                    'UOM 4 weight',
                    'UOM 4 boxtype',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->produk_kode,
                        $val->produk->nama,
                        $val->tipe_produk==1?'Produk GMD': 'Produk Non GMD',
                        $val->tipe_barang_kode,
                        $val->jenis_barang_kode,
                        $val->locked_price==1?'Ya': 'Tidak',
                        $val->margin,
                        $val->uom1_satuan,
                        $val->uom1_selling_cek==1?'Ya': 'Tidak',
                        $val->uom1_order_cek==1?'Ya': 'Tidak',
                        $val->uom1_receiving_cek==1?'Ya': 'Tidak',
                        $val->uom1_return_cek==1?'Ya': 'Tidak',
                        $val->uom1_inventory_cek==1?'Ya': 'Tidak',
                        $val->uom1_barcode,
                        $val->uom1_conversion,
                        $val->uom1_width,
                        $val->uom1_height,
                        $val->uom1_length,
                        $val->uom1_weight,
                        $val->uom1_boxtype==1? 'Outerbox': 'Inerbox',

                        $val->uom2_satuan,
                        $val->uom2_selling_cek==1?'Ya': 'Tidak',
                        $val->uom2_order_cek==1?'Ya': 'Tidak',
                        $val->uom2_receiving_cek==1?'Ya': 'Tidak',
                        $val->uom2_return_cek==1?'Ya': 'Tidak',
                        $val->uom2_inventory_cek==1?'Ya': 'Tidak',
                        $val->uom2_barcode,
                        $val->uom2_conversion,
                        $val->uom2_width,
                        $val->uom2_height,
                        $val->uom2_length,
                        $val->uom2_weight,
                        $val->uom2_boxtype==1? 'Outerbox': 'Inerbox',

                        $val->uom3_satuan,
                        $val->uom3_selling_cek==1?'Ya': 'Tidak',
                        $val->uom3_order_cek==1?'Ya': 'Tidak',
                        $val->uom3_receiving_cek==1?'Ya': 'Tidak',
                        $val->uom3_return_cek==1?'Ya': 'Tidak',
                        $val->uom3_inventory_cek==1?'Ya': 'Tidak',
                        $val->uom3_barcode,
                        $val->uom3_conversion,
                        $val->uom3_width,
                        $val->uom3_height,
                        $val->uom3_length,
                        $val->uom3_weight,
                        $val->uom3_boxtype==1? 'Outerbox': 'Inerbox',

                        $val->uom4_satuan,
                        $val->uom4_selling_cek==1?'Ya': 'Tidak',
                        $val->uom4_order_cek==1?'Ya': 'Tidak',
                        $val->uom4_receiving_cek==1?'Ya': 'Tidak',
                        $val->uom4_return_cek==1?'Ya': 'Tidak',
                        $val->uom4_inventory_cek==1?'Ya': 'Tidak',
                        $val->uom4_barcode,
                        $val->uom4_conversion,
                        $val->uom4_width,
                        $val->uom4_height,
                        $val->uom4_length,
                        $val->uom4_weight,
                        $val->uom4_boxtype==1? 'Outerbox': 'Inerbox',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':BH'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:BH'.$row, 'thin');
                $sheet->mergeCells('A1:BH1');
                $sheet->mergeCells('A2:BH2');
                $sheet->cells('A1:BH1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:BH2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:BH3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>40, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20, 'K'=>20,'L'=>20,'M'=>20,'N'=>20,'O'=>20,'P'=>20,'Q'=>20,'R'=>20,'S'=>20,'T'=>20,'U'=>20,'V'=>20,'W'=>20,'X'=>20,'Y'=>20,'Z'=>20,'AA'=>20,'AB'=>20,'AC'=>20, 'AD'=>20, 'AE'=>20, 'AF'=>20, 'AG'=>20, 'AH'=>20, 'AI'=>20, 'AJ'=>20, 'AK'=>20,'AL'=>20,'AM'=>20,'AN'=>20,'AO'=>20,'AP'=>20,'AQ'=>20,'AR'=>20,'AS'=>20,'AT'=>20,'AU'=>20,'AV'=>20,'AW'=>20,'AX'=>20,'AY'=>20,'AZ'=>20,'BA'=>20,'BB'=>20,'BC'=>20,'BD'=>20,'BE'=>20,'BF'=>20,'BG'=>20, 'BH'=>20,));
                $sheet->getStyle('B4:BH'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:BH'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportGetProdukSetting ->download('xls');
    }

    //Export Kki
    public function getKki(Request $request)
    {
        // dd($request->all());
        $records = Kki::select('*');
            if ($nomor = $request->nomor) {
                $records->where('nomor', 'like', '%' . $nomor . '%');
            }
            if ($tmuk_id = $request->tmuk_id) {
                $records->where('tmuk_id',$tmuk_id);
            }

        $record = $records->get();
        
        $ExportDataKKi = Excel::create('DAFTAR KKI ', function($excel) use ($record){
            $excel->setTitle('Daftar Kki');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR KKI ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR KKI '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Nomor KKI',
                    'TMUK',
                    'LSI',
                    'Tanggal Submit',
                    'Status',
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->nomor,
                        $val->tmuk->nama,
                        $val->tmuk->lsi->nama,
                        $val->tanggal_submit,
                        $val->status==1?'Berjalan': 'Selesai',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:F2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>40, 'D'=>40, 'E'=>20, 'F'=>20,));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportDataKKi ->download('xls');
    }

    //Export Opening Toko
    public function getOpeningToko(Request $request)
    {
        // dd($request->all());
        $opening = TransOpening::with('creator')->select('*');

        if($tgl_buat = $request->tgl_buat){
            $dates = \Carbon\Carbon::parse($tgl_buat)->format('Y-m-d');
            $opening->where(\DB::raw('DATE(tgl_buat)'),$dates);
        }

        if ($tmuk_kode = $request->tmuk_kode) {
            $opening->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }

        $record = $opening->get();
        
        $ExportOpening = Excel::create('DAFTAR OPENING TOKO ', function($excel) use ($record){
            $excel->setTitle('Daftar Opening toko');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR OPENING TOKO ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR OPENING TOKO '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal Opening',
                    'Tanggal Kirim'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk_kode.''.$val->tmuk->nama,
                        $val->tgl_buat,
                        $val->tgl_kirim,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                // $sheet->cells('E4'.':E'.$row, function($cells){
                //     $cells->setFontSize(12);
                //     $cells->setAlignment('center');
                // });
                $sheet->setBorder('A3:E'.$row, 'thin');
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->cells('A1:A1', function($cells){ 
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:E3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>30,'C'=>30, 'D'=>15, 'E'=>15));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportOpening ->download('xls');
    }

    //Download Opening Toko
    public function getDownloadOpeningToko(Request $request)
    {
        $tmuk = Tmuk::find($request->tmuk_id);

        $pa = ProdukAssortment::with('creator', 'produk', 'assortment')
                         ->whereHas('produk.produksetting', function($query){
                            $query->where('tipe_produk', 1);
                         })->where('assortment_type_id', $tmuk->assortment->id)
                         ->select('ref_produk_assortment.*');

        $record = $pa->get();
        
        $DownloadTemplateOpening = Excel::create('Opening Toko '.$tmuk->kode, function($excel) use ($record, $tmuk){
            $excel->setTitle('Template Opening toko');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Template');

            $excel->sheet('Opening Toko '.$tmuk->kode, function($sheet) use ($record){
                $sheet->row(1, array(
                    'produk_kode',
                    'qty',
                    ));

                $row = 2;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $val->produk->kode,
                        0,
                        ));
                    $row=$row;
                    $row++;
                }
                $sheet->setWidth(array('A'=>20,'B'=>15));
                $sheet->cells('A1:B1', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->getStyle('A2:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'left')
                );
                $sheet->getStyle('B2:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'left')
                );

            });
        });
        $DownloadTemplateOpening ->download('xls');
    }

    //Export Pyr
    public function getPyr(Request $request)
    {
        // dd($request->all());
        // $payment = TransPyr::select('*');
        $payment = TransPyr::with('creator','groups')
        ->where(function ($query) {
            $query->where('group',null)
                  ->orWhere(\DB::raw('trans_pyr.nomor_pyr'),\DB::raw('trans_pyr.group'));
        })
        ->orderBy('tgl_buat', 'DESC')
        ->select('*');
        //if ($kode = $request->kode) {
            //$payment->where('kode', 'like', '%' . $kode . '%');
        //}
        //if ($area = $request->area) {
            //$payment->where('area', 'like', '%' . $area . '%');
        //}

        if($tgl_buat = $request->tgl_buat){
            $dates = \Carbon\Carbon::parse($tgl_buat)->format('Y-m-d');
            $payment->where(\DB::raw('DATE(tgl_buat)'),$dates);
        }
        if ($nomor_pyr = $request->nomor_pyr) {
            $payment->where('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
        }
        if ($tmuk_kode = $request->tmuk_kode) {
            $payment->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }
        if ($vendor_lokal = $request->vendor_lokal) {
            $payment->where('vendor_lokal',$vendor_lokal);
        }
        $record = $payment->get();
        // $record = TransPyr::all();
        
        $ExportPayment = Excel::create('DAFTAR PAYMENT REQUEST ', function($excel) use ($record){
            $excel->setTitle('Daftar PAYMENT REQUEST');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR PAYMENT REQUEST ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR PAYMENT REQUEST '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal PYR',
                    'Nomor PYR',
                    'Tmuk',
                    'Vendor',
                    'Tanggal Jatuh Tempo',
                    'Jumlah Pembelian (Rp)'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    //vendorlokal
                    if($val->vendor_lokal == 0){
                        $data = isset($val->tmuk->lsi->nama) ? $val->tmuk->lsi->nama : '-';
                    }else{
                        $data = isset($val->vendorlokal->nama) ? $val->vendorlokal->nama : '-';
                    }
                    $sheet->row($row, array(
                        $no,
                        $val->tgl_buat,
                        $val->nomor_pyr,
                        $val->tmuk->kode.' - '.$val->tmuk->nama,
                        $data,
                        $val->tgl_jatuh_tempo,
                        $val->total,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('D4'.':G'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>30,'D'=>30,'E'=>30,'F'=>20,'G'=>25));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportPayment ->download('xls');
    }

    //Export Dashboard
    public function getPenjualanKategori(Request $request)
    {
        dd($request->all());
        $penjualankategori = TransRekapPenjualanTmukDetail::select('*');
        //if ($kode = $request->kode) {
            //$penjualankategori->where('kode', 'like', '%' . $kode . '%');
        //}
        //if ($area = $request->area) {
            //$penjualankategori->where('area', 'like', '%' . $area . '%');
        //}

        if ($region_id = $request->region_id) {
            $penjualankategori->where('region_id',$region_id);
        }
        $record = $penjualankategori->get();
        
        $ExportPenjualanKategori = Excel::create('DAFTAR PENJUALAN KATEGORI 1 ', function($excel) use ($record){
            $excel->setTitle('Daftar PENJUALAN KATEGORI 1');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('DAFTAR PENJUALAN KATEGORI 1 ', function($sheet) use ($record){
                $sheet->row(1, array(
                    'DAFTAR PENJUALAN KATEGORI 1 '
                    ));

                $sheet->row(2, array(
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kategori 1',
                    'Penjulan (Rp.)',
                    'Kontribusi dari Sales (%)'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->produk->l1_nm,
                        $val->rekappenjualan->total,
                        $val->produk->l1_nm,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('D4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>30,'D'=>30));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $ExportPenjualanKategori ->download('xls');
    }
}
 
