<?php

namespace Lotte\Http\Controllers\Rincian;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransPoDetail;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransSO;
use Lotte\Models\Trans\TransStock;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\HargaTmuk;
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Lsi;
use Datatables;
use Lotte\Models\Master\TahunFiskal;


class AkunEscrowController extends Controller
{
    protected $link = 'rincian/akun-escrow';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Akun Escrow TMUK");
        $this->setSubtitle("&nbsp;");
        $this->setModalSize("fullscreen");
        $this->setBreadcrumb(['Rincian' => '#', 'Akun Escrow TMUK' => '#']);
        $this->setTableStruct([
            [
                'data' => 'kode_tmuk',
                'name' => 'kode_tmuk',
                'label' => 'Kode TMUK',
                'orderable' => false,
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            /* --------------------------- */
            [
                'data' => 'nama_tmuk',
                'name' => 'nama_tmuk',
                'label' => 'Nama TMUK',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'saldo_escrow',
                'name' => 'saldo_escrow',
                'label' => 'Saldo Escrow',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'norek_escrow',
                'name' => 'norek_escrow',
                'label' => 'Norek Escrow',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'atas_nama',
                'name' => 'atas_nama',
                'label' => 'Atas Nama',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'induk_lsi',
                'name' => 'induk_lsi',
                'label' => 'Induk LSI',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'tgl_buka',
                'name' => 'tgl_buka',
                'label' => 'Tanggal Buka',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'no_member',
                'name' => 'no_member',
                'label' => 'No. Member',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
        ]);
    }

    public function grid(Request $request)
    {
        $records = Tmuk::with('creator');

        if ($region = $request->region) {
            $records->whereHas('lsi', function ($query) use ($region){
                $query->whereHas('region', function ($query2) use ($region){
                    $query2->where('id',$region);
                });
            });
        }

        if ($lsi = $request->lsi) {
            $records->whereHas('lsi', function ($query) use ($lsi){
                $query->where('id',$lsi);
            });
        }

        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('id',$tmuk_kode);
        }

        return Datatables::of($records)
        ->addColumn('kode_tmuk',function($record){
            return $record->kode;
        })

        ->addColumn('nama_tmuk',function($record){
            return $record->nama;
        })

        ->addColumn('saldo_escrow',function($record){
            return rupiah($record->saldo_escrow);
        })

        ->addColumn('norek_escrow',function($record){
            return $record->rekeningescrow->nomor_rekening;
        })
        ->addColumn('atas_nama',function($record){
            return $record->rekeningescrow->nama_pemilik;
        })
        ->addColumn('induk_lsi',function($record){
            return $record->lsi->kode;
        })
        ->addColumn('tgl_buka',function($record){
            return $record->aktual_pembukaan;
        })
        ->addColumn('no_member',function($record){
            return $record->membercard->nomor;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.laporan.rincian.akun-escrow.index', ['mockup' => false]);
    }

    public function cetak(Request $request){

        $data = $request->all();
        $records = Tmuk::with('creator');
        $regions = 'ALL';
        $lsis = 'ALL';
        $tmuks = 'ALL';

        if ($region = $request->region) {
            $records->whereHas('lsi', function ($query) use ($region){
                $query->whereHas('region', function ($query2) use ($region){
                    $query2->where('id',$region);
                });
            });

            $a = Region::find($region);
            $regions = $a->area;
        }

        if ($lsi = $request->lsi) {
            $records->whereHas('lsi', function ($query) use ($lsi){
                $query->where('id',$lsi);
            });

            $b = Lsi::find($lsi);
            $lsis = $b->nama;
        }

        if ($tmuk = $request->tmuk) {
            $records->where('id',$tmuk);

            $c = Tmuk::find($tmuk);
            $tmuks = $c->nama;
        }

        $record = $records->get();
        $tahun_fiskal   = TahunFiskal::getTahunFiskal();

        $data = [
            'oriented'   => 'L',
            'paper'      => 'A4',
            'label_file' => 'Daftar Akun Escrow',
        ];

        $content = view('report.akun-escrow', [
            'records'   => $record,
            'fiskal'    => $tahun_fiskal,
            'region'    => $regions,
            'lsi'       => $lsis,
            'tmuk'      => $tmuks,
        ]);

        HTML2PDF($content, $data);
    }
}
