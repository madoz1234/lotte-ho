<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransPyrDetail;
use Lotte\Models\Trans\TransMuatanDetail;
use Lotte\Models\Trans\TransMuatan;
use Lotte\Models\Trans\TransSuratJalan;
use Lotte\Models\Trans\Truk;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRetur;
use Lotte\Models\Trans\TransReturDetail;
use Lotte\Models\Trans\TransRekapGr;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Tmuk;

use Carbon\Carbon;

class PembelianController extends Controller
{
    protected $link = 'laporan/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan");
        
    }
    public function grid(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;
        $data  = [];

        if($request->start and $request->end){
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
        }

        switch ($request->type) {
            case 'rekap-pembelian-persediaan':
                    if($request->start and $request->end){
                         $records = TransRekapPenjualanTmuk::from(\DB::raw("(select
                                    trans_rekap_penjualan.tmuk_kode,
                                    trans_rekap_penjualan.tanggal,
                                    trans_rekap_penjualan_detail.qty,
                                    trans_rekap_penjualan.total,
                                    trans_rekap_penjualan_detail.harga,
                                    trans_rekap_penjualan.total - trans_rekap_penjualan.total as profit,
                                    (trans_rekap_penjualan.total - trans_rekap_penjualan.total) / trans_rekap_penjualan.total as margin
                                    from
                                    trans_rekap_penjualan
                                    inner join trans_rekap_penjualan_detail on trans_rekap_penjualan_detail.rekap_penjualan_kode = trans_rekap_penjualan.jual_kode ) d"))->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tanggal'), $tanggal);

                                    // dd($records->get());

                        $data = [
                            'detail' => $records->get(),
                            'start'  => $start,
                            'end'    => $end,
                        ];
                    }
                break;
            case 'lembar-po-stdk':
                    if($request->start and $request->end){
                        $records = TransPo::select('*')
                                    ->where('status', 2)
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

                        $data = [
                            'detail' => $records->get(),
                            'start'  => $start,
                            'end'    => $end,
                        ];
                    }
                break;
            case 'lembar-po-btdk':
                    if($request->start and $request->end){
                        $records = TransPo::select('*')
                                    ->where('status', 1)
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

                        $data = [
                            'detail' => $records->get(),
                            'start'  => $start,
                            'end'    => $end,
                        ];
                    }
                break;
            case 'po-barang-trade-ke-lsi':
                    if($request->start and $request->end){
                        $records = TransPyr::select('*')
                                    ->where('tipe', '001')
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

                        $data = [
                            'detail' => $records->get(),
                            'start'  => $start,
                            'end'    => $end,
                        ];
                    }
                break;
            case 'po-barang-non-trade-ke-lsi':
                    if($request->start and $request->end){
                            $records = TransPyr::select('*')
                                        ->where('tipe', '003')
                                        ->where('tmuk_kode', $tmuk_kode)
                                        ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            case 'po-barang-trade-ke-vendor-lokal':
                    if($request->start and $request->end){
                            $records = TransPyr::select('*')
                                        ->where('tipe', '002')
                                        ->where('tmuk_kode', $tmuk_kode)
                                        ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            case 'lembar-picking-result-pt':
                    if($request->start and $request->end){
                            $records = TransPr::select('*')
                                         ->where('status', 1)
                                         ->where('tmuk_kode', $tmuk_kode)
                                         ->whereBetween(\DB::raw('created_at'), $tanggal);

                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            case 'lembar-label-kontainer-cl':
                    if($request->start and $request->end){
                            $records = TransKontainer::select('*')
                                                        ->where('tmuk_kode', $tmuk_kode)
                                                        ->whereBetween(\DB::raw('created_at'), $tanggal);

                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            case 'lembar-daftar-muatan-it':
                    if($request->start and $request->end){
                            $records = TransMuatanDetail::select('*')
                                                        ->where('tmuk_kode', $tmuk_kode)
                                                        ->whereBetween(\DB::raw('created_at'), $tanggal);
                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            case 'lembar-surat-jalan-dn':
                    if($request->start and $request->end){

                            $records = TransMuatanDetail::select('*')
                                                        ->where('tmuk_kode', $tmuk_kode)
                                                        ->whereBetween(\DB::raw('created_at'), $tanggal);

                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            case 'rekap-delivery':
                    if($request->start and $request->end){

                            $records = TransMuatanDetail::select('*')
                                                        ->where('tmuk_kode', $tmuk_kode)
                                                        ->whereBetween(\DB::raw('created_at'), $tanggal);

                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            case 'rekap-gr-grn':
                    if($request->start and $request->end){

                            $records = TransRekapGr::select('*')
                                                        ->where('tmuk_kode', $tmuk_kode)
                                                        ->whereBetween(\DB::raw('created_at'), $tanggal);

                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            case 'lembar-konfirmasi-retur-rr':
                    if($request->start and $request->end){
                           $records = TransRetur::select('*')
                                                    ->where('tmuk_kode', $tmuk_kode)
                                                    ->whereBetween(\DB::raw('created_at'), $tanggal);

                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            case 'rekap-service-level-sl':
                    if($request->start and $request->end){
                            $records = TransPo::select('*')
                                                ->where('tmuk_kode', $tmuk_kode)
                                                ->whereBetween(\DB::raw('tgl_buat'), $tanggal);



                            $data = [
                                'detail' => $records->get(),
                                'start'  => $start,
                                'end'    => $end,
                            ];
                    }
                break;
            default:
                break;
        }
        /*return views*/
        return view('modules.laporan.laporan.lists.pembelian.'.$request->type, $data);
    }

    //PRINT LAPORAN
    // PRINT LAPORAN PDF
    //PEMBELIAN
    public function printPembelian(Request $request){
        // dd($request->all());
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $tmuk_kode = $tmuk->kode;
        $start  = '';
        $end    = '';
        $record = [];
        $data   = [];

        if($request->start and $request->end){
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
        }

        switch ($request->laporan) {
            case 'rekap-pembelian-persediaan':
                $records = TransRekapPenjualanTmuk::from(\DB::raw("(select
                                    trans_rekap_penjualan.tmuk_kode,
                                    trans_rekap_penjualan.tanggal,
                                    trans_rekap_penjualan_detail.qty,
                                    trans_rekap_penjualan.total,
                                    trans_rekap_penjualan_detail.harga,
                                    trans_rekap_penjualan.total - trans_rekap_penjualan.total as profit,
                                    (trans_rekap_penjualan.total - trans_rekap_penjualan.total) / trans_rekap_penjualan.total as margin
                                    from
                                    trans_rekap_penjualan
                                    inner join trans_rekap_penjualan_detail on trans_rekap_penjualan_detail.rekap_penjualan_kode = trans_rekap_penjualan.jual_kode ) d"))->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tanggal'), $tanggal);

                $record = $records->get();
                // dd($record);

                $req            = $request->all();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();

                // dd($region);

                $data = [
                'records'   => $record,
                'request'   => $req,
                'fiskal'    => $tahun_fiskal,
                'region'    => $region,
                'lsi'       => $lsi,
                'tmuk'      => $tmuk,
                ];
                break;
            case 'lembar-po-btdk':
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
                $records = TransPo::select('*')
                                    ->where('status', 1)
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

                $record = $records->get();
                // dd($record);

                $req            = $request->all();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();

                $data = [
                'records'   => $record,
                'request'   => $req,
                'fiskal'    => $tahun_fiskal,
                'region'    => $region,
                'lsi'       => $lsi,
                'tmuk'      => $tmuk,
                ];
                break;
            case 'lembar-po-stdk':
                // $list_detail_po =function($po){
                // $detail = TransPo::has('detail')->select(DB::raw('
                //             trans_po.nomor as nomor,
                //             trans_po.nomor_pr as nomor_pr,

                //             ref_produk.bumun_nm as bumun_nm,
                //             ref_produk.l1_nm as l1_nm,
                //             ref_produk.nama as nama,
                //             ref_produk.kode as produk_kode,
                //             ref_produk_setting.uom1_barcode as uom1_barcode,
                //             ref_produk.nama as nama,
                            
                //             ref_produk_setting.uom1_selling_cek as uom1_selling_cek,

                //             ref_produk_lsi.lsi_kode as lsi_kode,
                //             ref_produk_lsi.stok_gmd as stok_gmd,
                //             ref_produk_lsi.curr_sale_prc as harga_estimasi,

                //             trans_po_detail.id as id_detail,
                //             trans_po_detail.qty_po as qty_po,
                //             trans_po_detail.unit_po as unit_po,
                //             trans_po_detail.qty_pr as qty_pr,

                //             trans_pr_detail.qty_order as qty_order,
                //             trans_pr_detail.unit_order as unit_order,
                //             trans_pr_detail.qty_sell as qty_sell,
                //             trans_pr_detail.unit_sell as unit_sell,
                //             trans_pr_detail.unit_pr as unit_pr,

                //             trans_po_detail.price as harga_aktual
                //         '))
                //       ->join('trans_po_detail', 'trans_po.nomor', '=', 'trans_po_detail.po_nomor')
                //       ->join('ref_produk', 'trans_po_detail.produk_kode', '=', 'ref_produk.kode')
                //       ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                //       ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                //       ->join('trans_pr_detail', function($join) {
                //             $join->on('trans_po_detail.produk_kode', '=', 'trans_pr_detail.produk_kode')
                //                   ->on('trans_pr_detail.pr_nomor', '=', 'trans_po.nomor_pr');
                //       })
                //       ->where(DB::raw('trans_po.nomor'), $po->nomor)
                //       ->where(DB::raw('ref_produk_lsi.lsi_kode'), $po->tmuk->lsi->kode)->get();
                //     $req['total_harga_aktual'] = FormatNumber($detail->sum('harga_aktual'));
                //     $bekok = array_merge($detail->toArray(),$req);
                //     return $bekok;
                // };

                // $po = TransPo::select('*');

                // if($request->status){
                //     $po->where('status', $request->status);
                // }

                // if($request->start and $request->end){
                //     $start = $request->start;
                //     $end   = $request->end;
                //     $po->whereBetween(\DB::raw('tgl_buat'), array($start, $end));
                // }

                // $pos = $po->get();

                // $temp = [];
                // foreach ($pos as $record) {
                // $detail_rec = $list_detail_po($record);
                // $temp[] = [
                //     'nomor'     => $record->nomor,
                //     'lsi_kode'  => $record->tmuk->lsi->kode,
                //     'lsi_nama'  => $record->tmuk->lsi->nama,
                //     'tmuk_kode' => $record->tmuk->kode,
                //     'tmuk_nama' => $record->tmuk->nama,
                //     'tgl_buat'  => $record->tgl_buat?\Carbon\Carbon::parse($record->tgl_buat)->format('Y/M/d')  : '',
                //     'tgl_kirim' => $record->pr->tgl_kirim?\Carbon\Carbon::parse($record->pr->tgl_kirim)->format('Y/M/d') : '',
                //     'detail' => $detail_rec,

                // ];
                // }
                // $data = [
                // 'oriented' => 'L',
                // 'records'   => $temp,
                // ];
                $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                $tanggal = [$start,$end];
                $records = TransPo::select('*')
                                    ->where('status', 2)
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

                $record = $records->get();
                // dd($record);

                $req            = $request->all();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();

                $data = [
                'records'   => $record,
                'request'   => $req,
                'fiskal'    => $tahun_fiskal,
                'region'    => $region,
                'lsi'       => $lsi,
                'tmuk'      => $tmuk,
                ];
                break;
            case 'po-barang-trade-ke-lsi':
                $records = TransPyr::select('*')->where('tipe', '001')
                                                ->where('tmuk_kode', $tmuk_kode)
                                                ->whereBetween(\DB::raw('created_at'), $tanggal);


                $data           = $records->get();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();

                foreach ($data as $pyr) {
                    $id_pyr         = $pyr->id;
                }
                $temp = [];
                foreach ($data as $record) {
                    $totals = 0;
                    foreach ($record->detailPyr as $val) {
                        ///
                        $total = $val->price*$val->qty;

                        $totals += $val->price*$val->qty;

                        $detail[] = [
                            'item_code'   => $val->produk_kode,
                            'description' => $val->produks->nama,
                            'unit'        => $val->unit,
                            'qty'         => $val->qty,
                            'harga'       => $val->price,
                            'harga_total'   => $total,
                        ];
                    }

                    $temp[] = [
                        'nomor_pyr'          => $record->nomor_pyr,
                        'tmuk_kode'          => $record->tmuk_kode,
                        'tmuk'               => $record->tmuk->nama,
                        'vendor_lokal'       => $record->vendor_lokal,
                        'tgl_buat'           => $record->tgl_buat,
                        'tgl_jatuh_tempo'    => $record->tgl_jatuh_tempo,
                        'keterangan'         => $record->keterangan,
                        'periode_start'      => $request->start,
                        'periode_end'        => $request->end,
                        'tahun_fiskal_awal'  => $tahun_fiskal->tgl_awal,
                        'tahun_fiskal_akhir' => $tahun_fiskal->tgl_akhir,
                        'lsi'                => $record->tmuk->lsi->nama,
                        'region'             => $record->tmuk->lsi->region->area,
                        'detail'             => $detail,  
                        'total_harga'        => $record->detailPyr->sum('price'),
                        'total_qty'          => $record->detailPyr->sum('qty'),
                        'total_sluruh_harga'        => $totals,

                                              // 'item_code'         => $detail->produk_kode,
                    ];
                }
                // dd($temp);

                $data = [
                // 'oriented' => 'L',
                'record'   => $temp,
                ];
                break;
            case 'po-barang-non-trade-ke-lsi':
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
                $records = TransPyr::select('*')->where('tipe', '003')
                                                ->where('tmuk_kode', $tmuk_kode)
                                                ->whereBetween(\DB::raw('created_at'), $tanggal);

                $data           = $records->get();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();

                foreach ($data as $pyr) {
                    $id_pyr         = $pyr->id;
                }
                $temp = [];
                foreach ($data as $record) {
                    $totals = 0;
                    foreach ($record->detailPyr as $val) {
                        ///
                        $total = $val->price*$val->qty;

                        $totals += $val->price*$val->qty;

                        $detail[] = [
                            'item_code'   => $val->produk_kode,
                            'description' => $val->produks->nama,
                            'unit'        => $val->unit,
                            'qty'         => $val->qty,
                            'harga'       => $val->price,
                            'harga_total'   => $total,
                        ];
                    }

                    $temp[] = [
                        'nomor_pyr'          => $record->nomor_pyr,
                        'tmuk_kode'          => $record->tmuk_kode,
                        'tmuk'               => $record->tmuk->nama,
                        'vendor_lokal'       => $record->vendor_lokal,
                        'tgl_buat'           => $record->tgl_buat,
                        'tgl_jatuh_tempo'    => $record->tgl_jatuh_tempo,
                        'keterangan'         => $record->keterangan,
                        'periode_start'      => $request->start,
                        'periode_end'        => $request->end,
                        'tahun_fiskal_awal'  => $tahun_fiskal->tgl_awal,
                        'tahun_fiskal_akhir' => $tahun_fiskal->tgl_akhir,
                        'lsi'                => $record->tmuk->lsi->nama,
                        'region'             => $record->tmuk->lsi->region->area,
                        'detail'             => $detail,  
                        'total_harga'        => $record->detailPyr->sum('price'),
                        'total_qty'          => $record->detailPyr->sum('qty'),
                        'total_sluruh_harga'        => $totals,

                                              // 'item_code'         => $detail->produk_kode,
                    ];
                }
                // dd($temp);

                $data = [
                // 'oriented' => 'L',
                'record'   => $temp,
                ];
                break;
            case 'po-barang-trade-ke-vendor-lokal':
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
                $records = TransPyr::select('*')->where('tipe', '002')
                                                ->where('tmuk_kode', $tmuk_kode)
                                                ->whereBetween(\DB::raw('created_at'), $tanggal);

                $data           = $records->get();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();

                foreach ($data as $pyr) {
                    $id_pyr         = $pyr->id;
                }
                $temp = [];
                foreach ($data as $record) {
                    $totals = 0;
                    foreach ($record->detailPyr as $val) {
                        ///
                        $total = $val->price*$val->qty;

                        $totals += $val->price*$val->qty;

                        $detail[] = [
                            'item_code'   => $val->produk_kode,
                            'description' => $val->produks->nama,
                            'unit'        => $val->unit,
                            'qty'         => $val->qty,
                            'harga'       => $val->price,
                            'harga_total'   => $total,
                        ];
                    }

                    $temp[] = [
                        'nomor_pyr'          => $record->nomor_pyr,
                        'tmuk_kode'          => $record->tmuk_kode,
                        'tmuk'               => $record->tmuk->nama,
                        'vendor_lokal'       => $record->vendor_lokal,
                        'tgl_buat'           => $record->tgl_buat,
                        'tgl_jatuh_tempo'    => $record->tgl_jatuh_tempo,
                        'keterangan'         => $record->keterangan,
                        'periode_start'      => $request->start,
                        'periode_end'        => $request->end,
                        'tahun_fiskal_awal'  => $tahun_fiskal->tgl_awal,
                        'tahun_fiskal_akhir' => $tahun_fiskal->tgl_akhir,
                        'lsi'                => $record->tmuk->lsi->nama,
                        'region'             => $record->tmuk->lsi->region->area,
                        'detail'             => $detail,  
                        'total_harga'        => $record->detailPyr->sum('price'),
                        'total_qty'          => $record->detailPyr->sum('qty'),
                        'total_sluruh_harga'        => $totals,

                                              // 'item_code'         => $detail->produk_kode,
                    ];
                }
                // dd($temp);

                $data = [
                // 'oriented' => 'L',
                'record'   => $temp,
                ];
                break;
            case 'lembar-picking-result-pt':
                // $list_detail_pr =function($pr){
                // $detail = TransPr::has('detailpr')->select(DB::raw('ref_produk_lsi.lsi_kode as lsi_kode,
                //             ref_produk.bumun_nm as bumun_nm,
                //             ref_produk.l1_nm as l1_nm,
                //             trans_pr_detail.produk_kode as produk_kode,
                //             ref_produk_setting.uom1_barcode as uom1_barcode,
                //             ref_produk.nama as nama,
                //             ref_produk_lsi.stok_gmd as stok_gmd,
                //             trans_pr_detail.unit_order as unit_order,
                //             trans_pr_detail.qty_order as qty_order,
                //             trans_pr_detail.qty_sell as qty_sell,
                //             trans_pr_detail.unit_sell as unit_sell,
                //             trans_pr_detail.qty_pr as qty_pr,
                //             trans_pr_detail.unit_pr as unit_pr,
                //             trans_pr_detail.qty_pick as qty_pick'
                //         ))
                //       ->join('trans_pr_detail', 'trans_pr.nomor', '=', 'trans_pr_detail.pr_nomor')
                //       ->join('ref_produk', 'trans_pr_detail.produk_kode', '=', 'ref_produk.kode')
                //       ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                //       ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                //       ->where(DB::raw('trans_pr.nomor'), $pr->nomor)
                //       ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->get()->sortBy('id_detail')->toArray();
                //     return $detail;
                // };

                // $pr = TransPr::select('*');

                // if($request->status){
                //     $pr->where('status', $request->status);
                // }
                // if($request->start and $request->end){
                //     $start = $request->start;
                //     $end   = $request->end;
                //     $pr->whereBetween(\DB::raw('created_at'), array($start, $end));
                // }

                // $pos = $pr->get();

                // $temp = [];
                // foreach ($pos as $record) {
                // $detail_rec = $list_detail_pr($record);

                // $temp[] = [
                //     'nomor'     => $record->nomor,
                //     'lsi_kode'  => $record->tmuk->lsi->kode,
                //     'lsi_nama'  => $record->tmuk->lsi->nama,
                //     'tmuk_kode' => $record->tmuk->kode,
                //     'tmuk_nama' => $record->tmuk->nama,
                //     'tgl_buat'  => $record->tgl_buat,
                //     'tgl_kirim' => $record->tgl_kirim,
                //     'detail'    => $detail_rec,

                // ];
                // }
                // $data = [
                // 'oriented' => 'L',
                // 'records'   => $temp,
                // ];
                $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                $tanggal = [$start,$end];
                $records = TransPr::select('*')
                                                     ->where('status', 1)
                                                     ->where('tmuk_kode', $tmuk_kode)
                                                     ->whereBetween(\DB::raw('created_at'), $tanggal);

                $record = $records->get();
                // dd($record);

                $req            = $request->all();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
// dd($region);
                $data = [
                'records'   => $record,
                'request'   => $req,
                'fiskal'    => $tahun_fiskal,
                'region'    => $region,
                'lsi'       => $lsi,
                'tmuk'      => $tmuk,
                ];

                break;
            case 'lembar-label-kontainer-cl':
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
                $records = TransKontainer::select('*')
                                            ->where('tmuk_kode', $tmuk_kode)
                                            ->whereBetween(\DB::raw('created_at'), $tanggal);

                $data = $records->get();

                $temp = [];
                foreach ($data as $record) {
                $temp[] = [
                    'nomor_pr'  => $record->po->pr->nomor,
                    'nomor_po'  => $record->po_nomor,
                    'nomor_cl'  => $record->nomor,
                    'tmuk_kode' => $record->tmuk->kode,
                    'tmuk_nama' => $record->tmuk->nama,
                    'lsi_nama'  => $record->tmuk->lsi->nama,
                    // 'tgl_buat'  => $record->tgl_buat,
                    // 'tgl_kirim' => $record->tgl_kirim,
                ];
                }

                $data = [
                'oriented' => 'L',
                'record'   => $temp,
                ];
                break;
            case 'lembar-daftar-muatan-it':
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
                $records = TransMuatanDetail::select('*')
                                                ->where('tmuk_kode', $tmuk_kode)
                                                ->whereBetween(\DB::raw('created_at'), $tanggal);

                $record = $records->get();
                // dd($record);

                $req            = $request->all();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
// dd($tmuk);
                $data = [
                'records'   => $record,
                'request'   => $req,
                'fiskal'    => $tahun_fiskal,
                'region'    => $region,
                'lsi'       => $lsi,
                'tmuk'      => $tmuk,
                ];
                // dd($data);

                break;
            case 'lembar-surat-jalan-dn':
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
                $records = TransMuatanDetail::select('*')
                                                ->where('tmuk_kode', $tmuk_kode)
                                                ->whereBetween(\DB::raw('created_at'), $tanggal);

                $record = $records->get();
                // dd($record);

                $req            = $request->all();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
// dd($tmuk);
                $data = [
                'records'   => $record,
                'request'   => $req,
                'fiskal'    => $tahun_fiskal,
                'region'    => $region,
                'lsi'       => $lsi,
                'tmuk'      => $tmuk,
                ];

                // dd($data);
                break;
            case 'rekap-delivery':
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
                $records = TransMuatanDetail::select('*')
                                                ->where('tmuk_kode', $tmuk_kode)
                                                ->whereBetween(\DB::raw('created_at'), $tanggal);

                $record = $records->get();
                // dd($record);

                $req            = $request->all();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
// dd($tmuk);
                $data = [
                'records'   => $record,
                'request'   => $req,
                'fiskal'    => $tahun_fiskal,
                'region'    => $region,
                'lsi'       => $lsi,
                'tmuk'      => $tmuk,
                ];

                // dd($data);
                break;
            case 'rekap-gr-grn':
                $records = TransRekapGr::select('*')
                                                ->where('tmuk_kode', $tmuk_kode)
                                                ->whereBetween(\DB::raw('created_at'), $tanggal);

                $record = $records->get();
                // dd($record);

                $req            = $request->all();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
// dd($record);
                $data = [
                'records'   => $record,
                'request'   => $req,
                'fiskal'    => $tahun_fiskal,
                'region'    => $region,
                'lsi'       => $lsi,
                'tmuk'      => $tmuk,
                ];

                // dd($data);
                break;
            case 'lembar-konfirmasi-retur-rr':
                $records = TransReturDetail::with('creator')
                         ->select('*');

                    if($request->start and $request->end){
                        $start = $request->start;
                        $end   = $request->end;
                        $records->whereBetween(\DB::raw('created_at'), array($request->start, $request->end));
                    }

                $data = $records->get();
                // dd($data);

                $temp = [];
                foreach ($data as $record) {
                $temp[] = [
                    'produk_kode'   => $record->produk_kode,
                    'nomor_retur'   => $record->detail->nomor_retur,
                    'date'          => $record->detail->created_at,
                    'tmuk_kode'     => $record->detail->tmuk_kode,
                    'tmuk_nama'     => $record->detail->tmuk->nama,
                    'lsi_kode'      => $record->detail->tmuk->lsi->kode,
                    'lsi_nama'      => $record->detail->tmuk->lsi->nama,
                    'produk_kode'   => $record->produk_kode,
                    'barcode'       => $record->produk->bumun_cd,
                    'produk_nama'   => $record->produk->nama,
                    'qty'           => $record->qty,
                    'reason'        => $record->reason,
                    'qty_retur'     => $record->qty_retur,
                ];
                }

                $data = [
                'oriented' => 'L',
                'record'   => $temp,
                ];

                // dd($data);
                break;
            case 'rekap-service-level-sl':
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
                $records = TransPo::select('*')
                                        ->where('tmuk_kode', $tmuk_kode)
                                        ->whereBetween(\DB::raw('created_at'), $tanggal);

                    $record         = $records->get();
                    $req            = $request->all();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                    // dd($req);

                    $data = [
                    'records'   => $record,
                    'request'   => $req,
                    'fiskal'    => $tahun_fiskal,
                    'region'    => $region,
                    'lsi'       => $lsi,
                    'tmuk'      => $tmuk,
                    ];
                break;
                case 'rekap-delivery':
                // $records = TransPo::with('detail')->select('*');
                $records = TransSuratJalan::with('creator')
                         ->select('*');

                // $records = TransMemberPiutang::with('detail')->select('*');
                // dd($record);

                        // dd($records);
                // if ($tmuk_kode = $tmuk_kode) {
                //     $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                // }
                if(isset($tanggal)){
                         $records->whereHas('detail',  function($records) use ($tanggal){
                                $records->whereBetween('tanggal', $tanggal);
                         });
                    }

                    $record         = $records->get();
                    $req            = $request->all();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                    // dd($record);

                    $data = [
                    'records'   => $record,
                    'request'   => $req,
                    'fiskal'    => $tahun_fiskal,
                    'region'    => $region,
                    'lsi'       => $lsi,
                    'tmuk'      => $tmuk,
                    ];
                break;
            default:
                break;
        }

        $content = view('report.laporan.pembeliaan.'.$request->laporan, $data);
        // return $content;
        HTML2PDF($content, $data);
    }
    // END LAPORAN PEMBELIAN



    // PRINT LAPORAN EXEL
    //PEMBELIAN
    public function exelPembelian(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $record = [];
        $data   = [];
        $temp   = [];
        $tmuk_kode = $tmuk->kode;

        if($request->start and $request->end){
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
        }
        switch ($request->laporan) {
            case 'rekap-pembelian-persediaan':
                $this->exelPembelianPersediaan($tmuk_kode, $tanggal);
                break;
            case 'lembar-po-btdk':
                $this->exelPoBtdk($tmuk_kode, $tanggal);
                break;
            case 'lembar-po-stdk':
                $this->exelPoStdk($tmuk_kode, $tanggal);
                break;
            case 'po-barang-trade-ke-lsi':
                $this->exelPoBarangTradeKeLsi($tmuk_kode, $tanggal);
                break;
            case 'po-barang-non-trade-ke-lsi':
                $this->exelPoBarangNonTradeKeLsi($tmuk_kode, $tanggal);
                break;
            case 'po-barang-trade-ke-vendor-lokal':
                $this->exelPoBarangTradeKeVendorLokal($tmuk_kode, $tanggal);
                break;
            case 'lembar-picking-result-pt':
                $this->exelPickingResult($tmuk_kode, $tanggal);
                break;
            case 'lembar-label-kontainer-cl':
                $this->exelLabelKontainer($tmuk_kode, $tanggal);
                break;
            case 'lembar-daftar-muatan-it':
                $this->exelDaftarMuatan($tmuk_kode, $tanggal);
                break;
            case 'lembar-surat-jalan-dn':
                $this->exelSuratJalan($tmuk_kode, $tanggal);
                break;
            case 'rekap-delivery':
                $this->exelRekapDelivery($tmuk_kode, $tanggal);
                break;
            case 'rekap-gr-grn':
                $this->exelRekapGr($tmuk_kode, $tanggal);
                break;
            case 'lembar-konfirmasi-retur-rr':
                $this->exelKonfirmasiReturn($tmuk_kode, $tanggal);
                break;
            case 'rekap-service-level-sl':
                $this->exelServiceLevel($tmuk_kode, $tanggal);
                break;
    
            default:
                // code...
                break;
        }
    }

    public function exelPembelianPersediaan($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);

        $records = TransRekapPenjualanTmuk::from(\DB::raw("(select
                                    trans_rekap_penjualan.tmuk_kode,
                                    trans_rekap_penjualan.tanggal,
                                    trans_rekap_penjualan_detail.qty,
                                    trans_rekap_penjualan.total,
                                    trans_rekap_penjualan_detail.harga,
                                    trans_rekap_penjualan.total - trans_rekap_penjualan.total as profit,
                                    (trans_rekap_penjualan.total - trans_rekap_penjualan.total) / trans_rekap_penjualan.total as margin
                                    from
                                    trans_rekap_penjualan
                                    inner join trans_rekap_penjualan_detail on trans_rekap_penjualan_detail.rekap_penjualan_kode = trans_rekap_penjualan.jual_kode ) d"))->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tanggal'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('REKAP PEMBELIAN PERSEDIAAN ', function($excel) use ($record, $start, $end){
            $excel->setTitle('REKAP PEMBELIAN PERSEDIAAN');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('REKAP PEMBELIAN PERSEDIAAN ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'REKAP PEMBELIAN PERSEDIAAN '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal Penjualan',
                    'Qty',
                    'Penjualan (Rp)',
                    'Cost Price (Rp)',
                    'Profit (Rp)',
                    'Profit Margin (%)'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        Carbon::parse($val->tanggal)->format('d/m/Y'),
                        $val->qty,
                        $val->total,
                        $val->harga,
                        $val->profit,
                        $val->margin,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->setBorder('A3:I'.$row, 'thin');
                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:I2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:I3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>40,'C'=>20, 'D'=>15, 'E'=>15, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20));
                $sheet->getStyle('B4:I'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:I'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls');
    }

     public function exelPoBtdk($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);

        $records = TransPo::select('*')
                                    ->where('status', 1)
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Lembar Purchase Order (BTDK) ', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Purchase Order (BTDK)');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Purchase Order (BTDK) ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Purchase Order (BTDK) '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal PO',
                    'Nomor PO',
                    'Nilai PO BTDK (Rp)'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->tgl_buat,
                        $val->nomor,
                        $val->total,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:F2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

     public function exelPoStdk($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);

        $records = TransPo::select('*')
                                    ->where('status', 2)
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Lembar Purchase Order (STDK) ', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Purchase Order (STDK)');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Purchase Order (STDK) ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Purchase Order (STDK) '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal PO',
                    'Nomor PO',
                    'Nilai PO STDK (Rp)'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->tgl_buat,
                        $val->nomor,
                        $val->total,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:F2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelPoBarangTradeKeLsi($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);

        $records = TransPyr::select('*')
                                    ->where('tipe', '001')
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Rekap PO Barang Trade ke LSI ', function($excel) use ($record, $start, $end){
            $excel->setTitle('Rekap PO Barang Trade ke LSI');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap PO Barang Trade ke LSI ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Rekap PO Barang Trade ke LSI '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal PyR',
                    'Nomor PyR',
                    'Nilai PyR (Rp)',
                    'Tipe Barang'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->tgl_buat,
                        $val->nomor_pyr,
                        $val->total,
                        $val->tipe,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':G'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>20, 'G'=>20));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelPoBarangNonTradeKeLsi($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);

        $records = TransPyr::select('*')
                                    ->where('tipe', '003')
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Rekap PO Barang Non-Trade LSI', function($excel) use ($record, $start, $end){
            $excel->setTitle('Rekap PO Barang Non-Trade LSI');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap PO Barang Non-Trade LSI', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Rekap PO Barang Non-Trade LSI '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal PyR',
                    'Nomor PyR',
                    'Nilai PyR (Rp)',
                    'Tipe Barang'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->tgl_buat,
                        $val->nomor_pyr,
                        $val->total,
                        $val->tipe,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':G'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>20, 'G'=>20));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }


     public function exelPoBarangTradeKeVendorLokal($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);

        $records = TransPyr::select('*')
                                    ->where('tipe', '002')
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tgl_buat'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Rekap PO Trade Vendor Lokal', function($excel) use ($record, $start, $end){
            $excel->setTitle('Rekap PO Trade Vendor Lokal');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap PO Trade Vendor Lokal', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Rekap PO Trade Vendor Lokal '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal PyR',
                    'Nomor PyR',
                    'Nilai PyR (Rp)',
                    'Tipe Barang'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->tgl_buat,
                        $val->nomor_pyr,
                        $val->total,
                        $val->tipe,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':G'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>20, 'G'=>20));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelPickingResult($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);
        $records = TransPr::select('*')
                            ->where('status', 1)
                            ->where('tmuk_kode', $tmuk_kode)
                            ->whereBetween(\DB::raw('created_at'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Lembar Picking Result (PT)', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Picking Result (PT)');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Picking Result (PT)', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Picking Result (PT) '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal PR',
                    'Nomor PR',
                    'Tanggal Picking'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->tgl_buat,
                        $val->nomor,
                        $val->updated_at,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:F2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelLabelKontainer($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);
        $records = TransKontainer::select('*')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->whereBetween(\DB::raw('created_at'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Lembar Label Kontainer (CL)', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Label Kontainer (CL)');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Label Kontainer (CL)', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Label Kontainer (CL) '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal PO',
                    'No Picking',
                    'Nomor PO',
                    'Nomor Kontainer'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->po->tgl_buat,
                        $val->po->nomor_pr,
                        $val->po_nomor,
                        $val->nomor,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':G'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>40, 'G'=>40));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelDaftarMuatan($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);
        $records = TransMuatanDetail::select('*')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->whereBetween(\DB::raw('created_at'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Lembar Daftar Muatan (LT)', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Daftar Muatan (LT)');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Daftar Muatan (LT)', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Daftar Muatan (LT) '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal Muatan',
                    'Nomor Daftar Muatan'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->created_at,
                        $val->nomor_muatan,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':E'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:E'.$row, 'thin');
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:E2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:E3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40));
                $sheet->getStyle('B4:E'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:E'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelSuratJalan($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);
       $records = TransMuatanDetail::select('*')
                        ->where('tmuk_kode', $tmuk_kode)
                        ->whereBetween(\DB::raw('created_at'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Lembar Surat Jalan (DN)', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Surat Jalan (DN)');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Surat Jalan (DN)', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Surat Jalan (DN) '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal Muatan',
                    'Nomor Daftar Muatan',
                    'Tanggal Kirim',
                    'Nomor Surat Jalan'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->muatan->created_at,
                        $val->nomor_muatan,
                        $val->muatan->suratjalan->created_at,
                        $val->muatan->suratjalan->nomor,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':G'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>20, 'G'=>40));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelRekapDelivery($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);
       $records = TransMuatanDetail::select('*')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->whereBetween(\DB::raw('created_at'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Rekap Delivery', function($excel) use ($record, $start, $end){
            $excel->setTitle('Rekap Delivery');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Delivery', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Rekap Delivery '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal Surat Jalan',
                    'Nomor Surat Jalan',
                    'Nomor Muatan'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->created_at,
                        $val->muatan->suratjalan->nomor,
                        $val->nomor_muatan,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:F2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>40));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelRekapGr($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);
       $records = TransRekapGr::select('*')
                            ->where('tmuk_kode', $tmuk_kode)
                            ->whereBetween(\DB::raw('created_at'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Rekap GR (GRN)', function($excel) use ($record, $start, $end){
            $excel->setTitle('Rekap GR (GRN)');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap GR (GRN)', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Rekap GR (GRN) '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal GR',
                    'Nomor GR'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->tgl_gr,
                        $val->no_gr,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':E'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:E'.$row, 'thin');
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:E2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:E3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40));
                $sheet->getStyle('B4:E'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:E'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelKonfirmasiReturn($tmuk_kode, $tanggal)
    {
            // dd($tmuk_kode);
       $records = TransRetur::select('*')
                            ->where('tmuk_kode', $tmuk_kode)
                            ->whereBetween(\DB::raw('created_at'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Lembar Konfirmasi Retur (RR)', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Konfirmasi Retur (RR)');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Konfirmasi Retur (RR)', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Konfirmasi Retur (RR) '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal PO',
                    'Nomor PO',
                    'Tanggal RR',
                    'Nomor RR'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->po->tgl_buat,
                        $val->nomor_po,
                        $val->created_at,
                        $val->nomor_retur,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':G'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>20, 'G'=>40));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelServiceLevel($tmuk_kode, $tanggal)
    {
        $records = TransReduceEscrow::from(\DB::raw("(SELECT
                trans_po.tmuk_kode,
                trans_po.nomor_pr,
                trans_po_detail.qty_pr,
                trans_po.tgl_buat,
                trans_po.nomor,
                trans_po_detail.qty_po,
                trans_po.created_at,
                trans_po_detail.qty_po::decimal / trans_po_detail.qty_pr::decimal *100 AS sl
            FROM
                trans_po
            JOIN trans_po_detail ON trans_po_detail.po_nomor = trans_po.nomor   )  d"))->where('tmuk_kode', $tmuk_kode)
                        ->whereBetween(\DB::raw('created_at'), $tanggal);

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Rekap Service Level (SL)', function($excel) use ($record, $start, $end){
            $excel->setTitle('Rekap Service Level (SL)');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Service Level (SL)', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Rekap Service Level (SL) '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'LSI',
                    'TMUK',
                    'Tanggal PR',
                    'Nomor PR',
                    'Qty PR',
                    'Tanggal PO',
                    'Nomor PO',
                    'Qty PO',
                    'SL %'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->lsi->nama,
                        $val->tmuk->nama,
                        $val->tgl_buat,
                        $val->nomor_pr,
                        $val->qty_pr,
                        $val->created_at,
                        $val->nomor,
                        $val->qty_po,
                        $val->sl,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':J'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:J'.$row, 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:J2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:J3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>20, 'G'=>20, 'H'=>40, 'I'=>20, 'J'=>20));
                $sheet->getStyle('B4:J'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:J'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

}
