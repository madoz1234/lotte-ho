<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

//Models
use Lotte\Models\Master\Coa;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransJurnal;

use Carbon\Carbon;

class AkunBukuBesarController extends Controller
{
    protected $link = 'laporan/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan");
        
    }
    public function grid(Request $request)
    {
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start  = '';
        $end    = '';
        $coa    = '';
        $tmuk_kode = $tmuk->kode;
        $tgl_opening_tmuk = $tmuk->created_at;
        $record = [];
        $tanggal = null;
        $saldo_kemarin = 0;
        $saldo_tanggal_awal = [];

        if($request->start and $request->end){
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
        }
        switch ($request->type) {
            case 'daftar-history-gl':
                if($request->start and $request->end and $request->coa){
                    $coa = $request->coa;

                    $record = Coa::with(['jurnal' => function($q) use ($tmuk_kode, $tanggal){
                                       return $q->where('tmuk_kode', $tmuk_kode)
                                                ->where('delete', 0)
                                                ->whereBetween('tanggal', $tanggal)
                                                ->orderBy('tanggal');
                                    }])->where('kode', $request->coa)->orderBy('kode', 'ASC');
                        
                    $record = $record->get();

                    $coa_neraca = [
                        '1.1.0.0', //Aktiva Lancar
                            '1.1.1.0', '1.1.2.0', '1.1.3.0', '1.1.4.0', '1.1.5.0', '1.1.6.0', '1.1.7.0', '1.1.1.1', '1.1.1.2', '1.1.2.1', '1.1.3.1', '1.1.3.1', '1.1.6.1', 
                        '1.2.0.0', //Aktiva Tetap
                            '1.2.1.0', '1.2.2.0', '1.2.3.0', '1.2.4.0', '1.2.5.0', '1.2.6.0', '1.2.7.0', 
                        '2.1.0.0', //Utang Jangka Pendek
                            '2.1.1.0', '2.1.2.0', '2.1.3.0', '2.1.4.0', '2.1.5.0', '2.1.1.1', '2.1.2.1', '2.1.5.1', 
                        '2.2.0.0', //Utang Jangka Panjang
                            '2.2.1.0', '2.2.2.0', '2.2.3.0', 
                        '3.1.0.0', //Modal
                            '3.1.1.0', '3.1.2.0', 
                        '3.2.0.0', //OPENING BALANCE EQUITY
                        '3.3.0.0', //Deviden
                        '3.4.0.0', //Laba Ditahan
                    ];

                    if (in_array($coa, $coa_neraca)) {
                        $tgl_awal = Carbon::parse($tgl_opening_tmuk)->format('Y-m-d');
                        $tgl_kemarin = Carbon::createFromFormat('d/m/Y', $request->start)->subDay(1);
                        $tgl_akhir = Carbon::parse($tgl_kemarin)->format('Y-m-d');

                        $saldo_kemarin  = TransJurnal::saldoAwalKemarin($tmuk_kode, $coa, $tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59');
                        // dd($tgl_awal, $tgl_akhir);
                    }
                }
                break;
            case 'bukti-jurnal-umum':
                if($request->start and $request->end){
                    $records = TransJurnal::select(DB::raw('DATE("tanggal") as date, coa_kode, SUM("jumlah") as jml, posisi, idtrans'))->groupBy('date', 'coa_kode', 'posisi', 'idtrans')
                        ->where('tmuk_kode', $tmuk_kode)
                        ->where('delete', 0)
                        ->whereBetween('tanggal', $tanggal);

                    $record = $records->orderBy('date')->orderBy('coa_kode')->get();
                }
                break;
            case 'daftar-akun':
                if($request->start){
                    $date = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $tanggal = [$date.' 00:00:00',$date.' 23:59:59'];
                    $records = Coa::with('creator')
                            ->whereHas('jurnal',  function($record) use ($tanggal, $tmuk_kode){
                                $record->whereBetween('tanggal', $tanggal)
                                       ->where('tmuk_kode', $tmuk_kode)
                                       ->where('delete', 0);
                            })
                            ->select('*')->orderBy('kode');

                    $data = [
                        'record'  => $records->get(),
                        'date'    => Carbon::createFromFormat('d/m/Y', $request->start)->format('Y/m/d'),
                    ];
                }
                break;
            case 'ringkasan-buku-besar':
                if($request->start and $request->end){
                    $records = TransJurnal::with('coa')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->where('delete', 0)
                                ->whereBetween('tanggal', $tanggal);

                    $record = $records->orderBy('tanggal')->orderBy('coa_kode')->get();
                }
                break;
            case 'neraca-saldo':
                if($request->start and $request->end){
                    $records = Coa::with(['jurnal' => function($q) use ($tmuk_kode, $tanggal){
                                   return $q->where('tmuk_kode', $tmuk_kode)->whereBetween('tanggal',$tanggal);
                                }])->orderBy('kode');

                    $record = $records->get();
                    
                    foreach ($record as $key => $coa) {
                        $tgl_awal = Carbon::parse($tgl_opening_tmuk)->format('Y-m-d');
                        $tgl_kemarin = Carbon::createFromFormat('d/m/Y', $request->start)->subDay(1);
                        $tgl_akhir = Carbon::parse($tgl_kemarin)->format('Y-m-d');

                        $saldo_tanggal_awal[$coa->kode]  = TransJurnal::saldoAwalKemarin($tmuk_kode, $coa->kode, $tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59');
                    }
                }
                break;
            default:
                break;
        }
        if($request->start and $request->end){
            $data = [
                'detail'              => [],
                'record'              => $record,
                'saldo_kemarin'       => $saldo_kemarin,
                'saldo_tanggal_awal'  => $saldo_tanggal_awal,
                'tanggal'             => $tanggal,
                'start'               => Carbon::createFromFormat('d/m/Y', $request->start)->format('Y/m/d'),
                'end'                 => Carbon::createFromFormat('d/m/Y', $request->end)->format('Y/m/d'),
                'coa'                 => $coa,
                'link'                => '$record->links()',
            ];
        }
        /*return views*/
        return $this->render('modules.laporan.laporan.lists.akun-buku-besar.'.$request->type, $data);
    }

    //  PRINT LAPORAN
    //  PRINT LAPORAN PDF
    //  PEMBELIAN
    public function printAkunBukuBesar(Request $request){
        // dd($request->all());
        // 
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);
        $tmuk_kode = $tmuk->kode;
        $tgl_opening_tmuk = $tmuk->created_at;

        $saldo_kemarin = 0;
        $start  = '';
        $end    = '';
        $record = [];

        switch ($request->laporan) {
            case 'daftar-history-gl':
                if($request->start and $request->end){
                    $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }

                $record = Coa::with(['jurnal' => function($q) use ($tmuk_kode, $tanggal){
                                   return $q->where('tmuk_kode', $tmuk_kode)
                                            ->where('delete', 0)
                                            ->whereBetween('tanggal', $tanggal)
                                            ->orderBy('tanggal');
                                }])->orderBy('kode', 'ASC');

                if ($request->coa) {
                    $record = $record->where('kode', $request->coa);
                }

                $coa = $request->coa;
                $coa_neraca = [
                    '1.1.0.0', //Aktiva Lancar
                        '1.1.1.0', '1.1.2.0', '1.1.3.0', '1.1.4.0', '1.1.5.0', '1.1.6.0', '1.1.7.0', '1.1.1.1', '1.1.1.2', '1.1.2.1', '1.1.3.1', '1.1.3.1', '1.1.6.1', 
                    '1.2.0.0', //Aktiva Tetap
                        '1.2.1.0', '1.2.2.0', '1.2.3.0', '1.2.4.0', '1.2.5.0', '1.2.6.0', '1.2.7.0', 
                    '2.1.0.0', //Utang Jangka Pendek
                        '2.1.1.0', '2.1.2.0', '2.1.3.0', '2.1.4.0', '2.1.5.0', '2.1.1.1', '2.1.2.1', '2.1.5.1', 
                    '2.2.0.0', //Utang Jangka Panjang
                        '2.2.1.0', '2.2.2.0', '2.2.3.0', 
                    '3.1.0.0', //Modal
                        '3.1.1.0', '3.1.2.0', 
                    '3.2.0.0', //OPENING BALANCE EQUITY
                    '3.3.0.0', //Deviden
                    '3.4.0.0', //Laba Ditahan
                ];

                if (in_array($coa, $coa_neraca)) {
                    $tgl_awal = Carbon::parse($tgl_opening_tmuk)->format('Y-m-d');
                    $tgl_kemarin = Carbon::createFromFormat('d/m/Y', $request->start)->subDay(1);
                    $tgl_akhir = Carbon::parse($tgl_kemarin)->format('Y-m-d');

                    $saldo_kemarin  = TransJurnal::saldoAwalKemarin($tmuk_kode, $request->coa, $tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59');
                }

                $record         = $record->get();
                $req            = $request->all();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;

                $data = [
                    'records'       => $record,
                    'request'       => $req,
                    'saldo_kemarin' => $saldo_kemarin,
                    'fiskal'        => $tahun_fiskal,
                    'region'        => $region,
                    'lsi'           => $lsi,
                    'tmuk'          => $tmuk,
                    'label_file'    => 'LAPORAN JURNAL PER AKUN '.$tmuk_kode.' '.$start.' s/d '.$end,
                ];

                break;
            case 'keseluruhan-jurnal':
                if($request->start and $request->end){
                    $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }

                $records = TransJurnal::with('creator')
                            ->where('delete', 0)
                            ->where('tmuk_kode', $tmuk_kode)
                            ->whereBetween('tanggal', $tanggal);

                $record         = $records->get();
                $req            = $request->all();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;

                $data = [
                    'records'   => $record,
                    'request'   => $req,
                    'fiskal'    => $tahun_fiskal,
                    'region'    => $region,
                    'lsi'       => $lsi,
                    'tmuk'      => $tmuk,
                    'label_file' => 'LAPORAN KESELURUHAN JURNAL '.$tmuk_kode.' '.$start.' s/d '.$end,
                ];
                break;
            case 'daftar-akun':
                $date = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $tanggal = [$date.' 00:00:00',$date.' 23:59:59'];
                $records = Coa::with('creator')
                        ->whereHas('jurnal',  function($q) use ($tanggal, $tmuk_kode){
                            $q->whereBetween('tanggal',$tanggal)
                              ->where('tmuk_kode', $tmuk_kode)
                              ->where('delete', 0);
                        })
                        ->select('*')->orderBy('kode');

                $record         = $records->get();
                $req            = $request->all();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tgl            = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');

                $data = [
                    'records'   => $record,
                    'request'   => $req,
                    'fiskal'    => $tahun_fiskal,
                    'region'    => $region,
                    'lsi'       => $lsi,
                    'tmuk'      => $tmuk,
                    'label_file' => 'LAPORAN DAFTAR COA '.$tmuk_kode.' '.$tgl,
                ];
                break;
            case 'ringkasan-buku-besar':
                if($request->start and $request->end){
                    $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }

                $records = TransJurnal::with('coa')
                            ->where('tmuk_kode', $tmuk_kode)
                            ->where('delete', 0)
                            ->whereBetween('tanggal', $tanggal);

                $record         = $records->orderBy('tanggal')->orderBy('coa_kode')->get();
                $req            = $request->all();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;

                $data = [
                    'records'   => $record,
                    'request'   => $req,
                    'fiskal'    => $tahun_fiskal,
                    'region'    => $region,
                    'lsi'       => $lsi,
                    'tmuk'      => $tmuk,
                    'label_file' => 'LAPORAN RINGKASAN BUKU BESAR '.$tmuk_kode.' '.$start.' s/d '.$end,
                ];

                break;
            case 'neraca-saldo':
                if($request->start and $request->end){
                    $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }

                $records = Coa::with(['jurnal' => function($q) use ($tmuk_kode, $tanggal){
                               return $q->where('tmuk_kode', $tmuk_kode)->where('delete', 0)->whereBetween(\DB::raw('tanggal'),$tanggal);
                            }])->orderBy('kode');

                $record         = $records->get();
                $req            = $request->all();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;

                $data = [
                    'records'   => $record,
                    'request'   => $req,
                    'fiskal'    => $tahun_fiskal,
                    'region'    => $region,
                    'lsi'       => $lsi,
                    'tmuk'      => $tmuk,
                    'label_file' => 'LAPORAN NERACA SALDO '.$tmuk_kode.' '.$start.' s/d '.$end,
                ];

                break;
            case 'bukti-jurnal-umum':
                if($request->start and $request->end){
                    $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }
    
                $records = TransJurnal::select(DB::raw('DATE("tanggal") as date, coa_kode, SUM("jumlah") as jml, posisi, idtrans'))->groupBy('date', 'coa_kode', 'posisi', 'idtrans')
                    ->where('tmuk_kode', $tmuk_kode)
                    ->where('delete', 0)
                    ->whereBetween('tanggal', $tanggal);


                $record         = $records->orderBy('date')->orderBy('coa_kode')->get();
                $req            = $request->all();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;

                $data = [
                    'records'   => $record,
                    'request'   => $req,
                    'fiskal'    => $tahun_fiskal,
                    'region'    => $region,
                    'lsi'       => $lsi,
                    'tmuk'      => $tmuk,
                    'label_file' => 'LAPORAN JURNAL UMUM '.$tmuk_kode.' '.$start.' s/d '.$end,
                ];

                break;
            default:

                break;
        }
        // $data = [
        //     'start'   => $start,
        //     'end'   => $end,
        //     'record'   => $record,
        // ];

        $content = view('report.laporan.akun-buku-besar.'.$request->laporan, $data);

        // return $content;
        HTML2PDF($content, $data);
    }
    // END LAPORAN PEMBELIAN



    //  PRINT LAPORAN EXEL
    //  PEMBELIAN
    public function exelAkunBukuBesar(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);
        $tmuk_kode = $tmuk->kode;
        $tgl_opening_tmuk = $tmuk->created_at;

        $saldo_kemarin = 0;
        $start  = '';
        $end    = '';
        $record = [];
        $tanggal = null;

        switch ($request->laporan) {
            case 'daftar-history-gl':
                if($request->start and $request->end){
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }

                if ($request->coa) {
                    $coa = $request->coa;

                    $coa_neraca = [
                        '1.1.0.0', //Aktiva Lancar
                            '1.1.1.0', '1.1.2.0', '1.1.3.0', '1.1.4.0', '1.1.5.0', '1.1.6.0', '1.1.7.0', '1.1.1.1', '1.1.1.2', '1.1.2.1', '1.1.3.1', '1.1.3.1', '1.1.6.1', 
                        '1.2.0.0', //Aktiva Tetap
                            '1.2.1.0', '1.2.2.0', '1.2.3.0', '1.2.4.0', '1.2.5.0', '1.2.6.0', '1.2.7.0', 
                        '2.1.0.0', //Utang Jangka Pendek
                            '2.1.1.0', '2.1.2.0', '2.1.3.0', '2.1.4.0', '2.1.5.0', '2.1.1.1', '2.1.2.1', '2.1.5.1', 
                        '2.2.0.0', //Utang Jangka Panjang
                            '2.2.1.0', '2.2.2.0', '2.2.3.0', 
                        '3.1.0.0', //Modal
                            '3.1.1.0', '3.1.2.0', 
                        '3.2.0.0', //OPENING BALANCE EQUITY
                        '3.3.0.0', //Deviden
                        '3.4.0.0', //Laba Ditahan
                    ];

                    if (in_array($coa, $coa_neraca)) {
                        $tgl_awal = Carbon::parse($tgl_opening_tmuk)->format('Y-m-d');
                        $tgl_kemarin = Carbon::createFromFormat('d/m/Y', $request->start)->subDay(1);
                        $tgl_akhir = Carbon::parse($tgl_kemarin)->format('Y-m-d');

                        $saldo_kemarin  = TransJurnal::saldoAwalKemarin($tmuk_kode, $request->coa, $tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59');
                    }
                }else{
                    $coa = 0;
                }

                $this->exelDaftarHistoryGl($tmuk_kode, $tanggal, $coa, $saldo_kemarin);
                break;
            case 'keseluruhan-jurnal':
                if($request->start and $request->end){
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }
                $this->exelKeseluruhanJurnal($tmuk_kode, $tanggal);
                break;
            case 'daftar-akun':
                $tanggal = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $this->exelDaftarAkun($tmuk_kode, $tanggal);
                break;
            case 'ringkasan-buku-besar':
                if($request->start and $request->end){
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }
                $this->exelRingkasanBukuBesar($tmuk_kode, $tanggal);
                break;
            case 'neraca-saldo':
                if($request->start and $request->end){
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }
                $this->exelNeracaSaldo($tmuk_kode, $tanggal);
                break;
            case 'bukti-jurnal-umum':
                if($request->start and $request->end){
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
                }
                $this->exelBuktiJurnalUmum($tmuk_kode, $tanggal);
                break;
            default:

                break;
        }
    }

    public function exelPoBtdk(){

        $data = [
            'title'     => ['PURCHASE ORDER (STDK)'],
            'header'    => [ 
                    'NO PO',
                    'LSI',
                    'TMUK',
                    'Tanggal Pemesanan',
                    'PO Type',
                    'Expected Delivery Dat',
                    'Kode Produk',
                    'Barcode',
                    'Nama Produk',
                    'Qty/UOM',
                    'UOM',
                    'Order Qty',
                    'Harga Satuan',
                    'Total Harga'
                ],
        ];

        $reports = \Excel::create($data['title'][0], function($excel) use ($data){
            $excel->setTitle($data['title'][0]);
            $excel->setCreator('ho.dev')->setCompany('ho.dev');
            $excel->setDescription('Export');
            $excel->sheet($data['title'][0], function($sheet) use ($data){
                // SEET TITTLE
                $sheet->mergeCells('A1:N1');
                $sheet->mergeCells('A2:N2');
                $sheet->cells('A1:N2', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });

                $row =1;
                foreach ($data['title'] as $key=>$title) {
                    $sheet->row($row,array($title));
                    $row++;
                }


                $sheet->cells('A4:N4', function($cells){
                    $cells->setFontSize(12);
                    $cells->setBackground("#80bfff");
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                
                $sheet->row(4, $data['header']);
                $row = 5;

                // foreach ($data['list_data'] as $row_data) {
                //     $sheet->row($row, $row_data);
                //     $row++;
                // }
                $row = $row+1;
                $sheet->mergeCells('B'.$row.':N'.$row);
                $sheet->cells('A'.$row.':N'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                // $sheet->row($row, $data['total']);

                $range = "A4:N".$row;
                $sheet->setBorder($range, 'thin');

            });
        });

        $reports->export('xls');
    }

    public function exelDaftarHistoryGl($tmuk_kode, $tanggal, $coa, $saldo_kemarin){
        $records = Coa::with(['jurnal' => function($q) use ($tmuk_kode, $tanggal){
                           return $q->where('tmuk_kode', $tmuk_kode)
                                    ->where('delete', 0)
                                    ->whereBetween('tanggal', $tanggal)
                                    ->orderBy('tanggal');
                        }])->orderBy('kode', 'ASC');

        if ($coa != 0) {
            $records = $records->where('kode', $coa);
        }

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Lembar Jurnal Per Akun ', function($excel) use ($record, $start, $end, $saldo_kemarin){
            $excel->setTitle('Lembar Jurnal Per Akun');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Jurnal Per Akun ', function($sheet) use ($record, $start, $end, $saldo_kemarin){
                $sheet->row(1, array(
                    'Lembar Jurnal Per Akun'
                    ));
                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'Nama Akun',
                    'Tanggal',
                    'ID Transaksi',
                    'Saldo Awal',
                    'Perubahan Debit',
                    'Perubahan Kredit',
                    'Perubahan Bersih',
                    'Saldo Akhir'
                    ));

                $row = 4;
                foreach ($record as $val) {
                    $i           = 1; 
                    $saldo_awal  = [];
                    $saldo_akhir = [];
                    foreach ($val->jurnal->sortBy('tanggal') as $key => $rw){
                        //saldo awal
                        if ($i == 1) {
                            $saldo_awal[$i] = $saldo_kemarin;
                        }else{
                            $saldo_awal[$i] = $saldo_akhir[$i-1];
                        }
                        //jumlah debit
                        $jumlah_debit = $rw->posisi=='D' ? $rw->jumlah : '0';
                        //jumlah kredit
                        $jumlah_kredit = $rw->posisi=='K' ? $rw->jumlah : '0';
                        //bersih
                        $posisi_normal = $val->posisi;
                        if ($posisi_normal == 'D') {
                            $bersih = $jumlah_debit - $jumlah_kredit;
                        }else{
                            $bersih = $jumlah_kredit - $jumlah_debit;                                        
                        }
                        //saldo akhir
                        $saldo_akhir[$i] = $saldo_awal[$i] + $bersih;

                        //insert to row
                        $sheet->row($row, array(
                            $rw->coa_kode.' - '.$rw->coa->nama,
                            Carbon::parse($rw->tanggal)->format('d/m/Y'),
                            $rw->idtrans,
                            $saldo_awal[$i],
                            abs($jumlah_debit),
                            abs($jumlah_kredit),
                            $bersih,
                            $saldo_akhir[$i]
                        ));

                        $row=$row;
                        $row++;$i++;
                    }
                }
                $sheet->setBorder('A3:H'.($row-1), 'thin');
                $sheet->mergeCells('A1:H1');
                $sheet->mergeCells('A2:H2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:H2', function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:H3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>45,'B'=>15,'C'=>20, 'D'=>15, 'E'=>15, 'F'=>15, 'G'=>15, 'H'=>15));
                $sheet->getStyle('B4:H'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:H'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('C4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls');
    }

    public function exelKeseluruhanJurnal($tmuk_kode, $tanggal){
        //canaya
    }

    public function exelDaftarAkun($tmuk_kode, $tanggal){
        $date = [$tanggal.' 00:00:00',$tanggal.' 23:59:59'];
        $records = Coa::with('creator')
                ->whereHas('jurnal',  function($q) use ($date, $tmuk_kode){
                    $q->whereBetween('tanggal',$date)
                      ->where('tmuk_kode', $tmuk_kode)
                      ->where('delete', 0);
                })
                ->select('*')->orderBy('kode');

        $record = $records->get();
        
        $Export = Excel::create('Lembar Daftar COA ', function($excel) use ($record, $tanggal){
            $excel->setTitle('Lembar Daftar COA');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Daftar COA ', function($sheet) use ($record, $tanggal){
                $sheet->row(1, array(
                    'Lembar Daftar COA'
                    ));
                $sheet->row(2, array(
                    'Periode : '.$tanggal
                    ));
                $sheet->row(3, array(
                    '#',
                    'No Akun',
                    'Nama Akun',
                    'Status'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $status = ($val->status == 1) ? 'Aktif' : 'Non-Aktif';
                    $sheet->row($row, array(
                        $no,
                        $val->kode,
                        $val->nama,
                        $status,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.($row-1), 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:D2', function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>40, 'D'=>20));
                $sheet->getStyle('B4:D'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls');
    }

    public function exelRingkasanBukuBesar($tmuk_kode, $tanggal){
        $records = TransJurnal::with('coa')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->where('delete', 0)
                                ->whereBetween('tanggal', $tanggal);

        $record = $records->get();

        $start = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[1])->format('d/m/Y');
        
        $Export = Excel::create('Lembar Ringkasan Buku Besar ', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Ringkasan Buku Besar');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Ringkasan Buku Besar ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Ringkasan Buku Besar'
                    ));
                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    '#',
                    'Tanggal',
                    'Nama Akun',
                    'Saldo Awal',
                    'Perubahan Debit',
                    'Perubahan Kredit',
                    'Perubahan Bersih',
                    'Saldo Akhir'
                    ));

                $row = 4;
                $no =1;
                $saldo_awal = [];
                $saldo_akhir = [];
                foreach ($record as $val) {
                    //saldo awal
                    if ($no == 1) {
                        $saldo_awal[$no] = 0;
                    }else{
                        $saldo_awal[$no] = $saldo_akhir[$no-1];
                    }
                    //perubahan debit
                    $jumlah_debit = ($val->posisi == 'D') ? $val->jumlah : 0;
                    //perubahan kredit
                    $jumlah_kredit = ($val->posisi == 'K') ? $val->jumlah : 0;
                    //perubahan bersih
                    $posisi_normal = $val->posisi;
                    if ($posisi_normal == 'D') {
                        $bersih = $jumlah_debit - $jumlah_kredit;
                    }else{
                        $bersih = $jumlah_kredit - $jumlah_debit;                                        
                    }
                    //saldo akhir
                    $saldo_akhir[$no] = $saldo_awal[$no] + $bersih;

                    $sheet->row($row, array(
                        $no,
                        Carbon::parse($val->tanggal)->format('d/m/Y'),
                        $val->coa->kode.' - '.$val->coa->nama,
                        $saldo_awal[$no],
                        $jumlah_debit,
                        $jumlah_kredit,
                        $bersih,
                        $saldo_akhir[$no]
                    ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->setBorder('A3:H'.($row-1), 'thin');
                $sheet->mergeCells('A1:H1');
                $sheet->mergeCells('A2:H2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:H2', function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:H3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>5,'B'=>15,'C'=>45,'D'=>15,'E'=>15,'F'=>15,'G'=>15,'H'=>15));
                $sheet->getStyle('B4:H'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:H'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls');
    }

    public function exelNeracaSaldo($tmuk_kode, $tanggal){
        $records = Coa::with(['jurnal' => function($q) use ($tmuk_kode, $tanggal){
                       return $q->where('tmuk_kode', $tmuk_kode)->where('delete', 0)->whereBetween('tanggal',$tanggal);
                    }])->orderBy('kode');

        $record = $records->get();

        $start = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[1])->format('d/m/Y');
        
        $Export = Excel::create('Lembar Neraca COA ', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Neraca COA');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Neraca COA ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Neraca COA'
                    ));
                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    '#',
                    'Nama Akun',
                    'Saldo Awal',
                    'Perubahan Debit',
                    'Perubahan Kredit',
                    'Perubahan Bersih',
                    'Saldo Akhir'
                    ));

                $row = 4;
                $no =1;
                $saldo_awal = [];
                $saldo_akhir = [];
                foreach ($record as $val) {
                    //saldo awal
                    if ($no == 1) {
                        $saldo_awal[$no] = 0;
                    }else{
                        $saldo_awal[$no] = $saldo_akhir[$no-1];
                    }
                    //perubahan debit
                    $jumlah_debit = $val->jurnal->where('posisi', 'D')->sum('jumlah');
                    //perubahan kredit
                    $jumlah_kredit = $val->jurnal->where('posisi', 'K')->sum('jumlah');
                    //perubahan bersih
                    $posisi_normal = $val->posisi;
                    if ($posisi_normal == 'D') {
                        $bersih = $jumlah_debit - $jumlah_kredit;
                    }else{
                        $bersih = $jumlah_kredit - $jumlah_debit;                                        
                    }
                    //saldo akhir
                    $saldo_akhir[$no] = $saldo_awal[$no] + $bersih;

                    $sheet->row($row, array(
                        $no,
                        $val->kode.' - '.$val->nama,
                        $saldo_awal[$no],
                        $jumlah_debit,
                        $jumlah_kredit,
                        $bersih,
                        $saldo_akhir[$no]
                    ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->setBorder('A3:G'.($row-1), 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>5,'B'=>45,'C'=>15,'D'=>15,'E'=>15,'F'=>15,'G'=>15));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls');
    }

    public function exelBuktiJurnalUmum($tmuk_kode, $tanggal){
        $records = TransJurnal::select(DB::raw('DATE("tanggal") as date, coa_kode, SUM("jumlah") as jml, posisi, idtrans'))->groupBy('date', 'coa_kode', 'posisi', 'idtrans')
                    ->where('tmuk_kode', $tmuk_kode)
                    ->where('delete', 0)
                    ->whereBetween('tanggal', $tanggal);

        $record = $records->get();

        $start = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[1])->format('d/m/Y');
        
        $Export = Excel::create('Lembar Jurnal Umum ', function($excel) use ($record, $start, $end){
            $excel->setTitle('Lembar Jurnal Umum');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Lembar Jurnal Umum ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Lembar Jurnal Umum'
                    ));
                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    '#',
                    'Tanggal',
                    'No Akun',
                    'Nama Akun',
                    'Debit',
                    'Kredit',
                    'ID Transaksi'
                    ));

                $row = 4;
                $no =1;
                $saldo_awal = [];
                $saldo_akhir = [];
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        Carbon::parse($val->tanggal)->format('d/m/Y'),
                        $val->coa->kode,
                        $val->coa->nama,
                        $val->posisi=='D'? $val->jml : '-',
                        $val->posisi=='K'? $val->jml : '-',
                        $val->idtrans
                    ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->setBorder('A3:G'.($row-1), 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>5,'B'=>15,'C'=>15,'D'=>35,'E'=>15,'F'=>15,'G'=>20));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('G4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls');
    }

}
