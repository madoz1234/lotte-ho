<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

//Models
use Lotte\Models\Trans\TransPyrPembayaran;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Coa;

use Carbon\Carbon;

class AkunUtangUsahaController extends Controller
{
    protected $link = 'laporan/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan");
        
    }
    public function grid(Request $request)
    {
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;
        $tgl_opening_tmuk = $tmuk->created_at;
        $data  = [];

        if($request->start and $request->end){
            $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
        }

        switch ($request->type) {
            case 'saldo-pemasok':
                    if($request->start and $request->end and $request->vendor){
                        $records = TransPyr::from(\DB::raw("(SELECT
                                              trans_jurnal.coa_kode,
                                              trans_jurnal.tanggal,
                                              trans_pyr.tgl_buat,
                                              trans_pyr.vendor_lokal,
                                              trans_jurnal.tmuk_kode,
                                              trans_jurnal.idtrans as id_jurnal,
                                              trans_pyr.nomor_pyr,
                                              trans_jurnal.jumlah as charges_jurnal,
                                              trans_jurnal.deskripsi,
                                              trans_jurnal.posisi,
                                              trans_pyr_pembayaran.verifikasi4_user
                                              FROM trans_pyr
                                              LEFT JOIN trans_jurnal ON trans_pyr.id_transaksi = trans_jurnal.idtrans
                                              LEFT JOIN trans_pyr_pembayaran ON trans_pyr.nomor_pyr = trans_pyr_pembayaran.nomor_pyr )  d"))
                                            ->where('coa_kode', '2.1.1.0')
                                            ->where('tmuk_kode',  $tmuk_kode)
                                            ->where('vendor_lokal', $request->vendor)
                                            ->whereBetween(\DB::raw('date(tanggal)'),array($start,$end));

                        // dd($records->get());

                      

                        $data = [
                        'record' => $records->get(),
                        'start'  => $start,
                        'end'    => $end,
                        ];
                    }
                break;
            case 'laporan-pembayaran':
                    if($request->start and $request->end){
                        $records = TransPyr::select('*')
                                                    ->where('tmuk_kode',$tmuk_kode)
                                                    ->where('status',2)
                                                    ->whereBetween(\DB::raw('date(tgl_jatuh_tempo)'),array($start,$end))
                                                    ->orderBy('vendor_lokal','asc')
                                                    ->orderBy('tgl_jatuh_tempo','asc');

                      

                        $data = [
                        'record' => $records->get(),
                        'start'  => $start,
                        'end'    => $end,
                        ];
                    }
                break;
            case 'umur-pemasok':
                    if($request->start and $request->end){
                        $records = TransPyr::select('*')
                                                    ->where('tmuk_kode',$tmuk_kode)
                                                    ->whereBetween(\DB::raw('date(tgl_jatuh_tempo)'),array($start,$end))
                                                    ->orderBy('vendor_lokal','asc')
                                                    ->orderBy('tgl_jatuh_tempo','asc');

                      

                        $data = [
                        'record' => $records->get(),
                        'start'  => $start,
                        'end'    => $end,
                        ];
                    }
                break;
            default:
                break;
        }
        return view('modules.laporan.laporan.lists.akun-utang-usaha.'.$request->type,$data);
    }

    public function printAkunUtangUsaha(Request $request){
        $data           = $request->all();
        $tmuk           = Tmuk::find($data['tmuk_kode']);
        
        $start          = date('Y-m-d');
        $end            = date('Y-m-d');
        $tmuk_kode      = $tmuk->kode;
        $tmuk_id        = $tmuk->id;
        $record         = '';
        $laporan        = '';
        $recordakuisisi = '';
        $saldo          = '';
        $rcd            = '';
        $kki            = '';
        $jurnal_perform = '';
        $saldo          = '';
        $dayStart       = '';
        $diff           = '';
        
        // if($request->start and $request->end){
        //     $start = Carbon::createFromFormat('d/m/Y', $request->start)->startOfMonth()->format('Y-m-d');
        //     $end   = Carbon::createFromFormat('d/m/Y', $request->end)->endOfMonth()->format('Y-m-d');

        //     $diff = Carbon::createFromFormat('m/Y', $request->start)->diffInMonths(Carbon::createFromFormat('m/Y', $request->end));
        // }
        if($request->start and $request->end){
            $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
        }
        $data   = [];
        $temp   = [];

        switch ($request->laporan) {
            case 'saldo-pemasok':
                    // if($request->start and $request->end and $request->vendor){
                    if($request->start and $request->end){

                        $records = TransPyr::from(\DB::raw("(SELECT
                                              trans_jurnal.coa_kode,
                                              trans_jurnal.tanggal,
                                              trans_pyr.tgl_buat,
                                              trans_pyr.vendor_lokal,
                                              trans_jurnal.tmuk_kode,
                                              trans_jurnal.idtrans as id_jurnal,
                                              trans_pyr.nomor_pyr,
                                              trans_jurnal.jumlah as charges_jurnal,
                                              trans_jurnal.deskripsi,
                                              trans_jurnal.posisi,
                                              trans_pyr_pembayaran.verifikasi4_user
                                              FROM trans_pyr
                                              LEFT JOIN trans_jurnal ON trans_pyr.id_transaksi = trans_jurnal.idtrans
                                              LEFT JOIN trans_pyr_pembayaran ON trans_pyr.nomor_pyr = trans_pyr_pembayaran.nomor_pyr )  d"))
                                            ->where('coa_kode', '2.1.1.0')
                                            ->where('tmuk_kode',  $tmuk_kode)
                                            // ->where('vendor_lokal', $request->vendor)
                                            ->whereBetween(\DB::raw('date(tanggal)'),array($start,$end));

                      

                        $record = $records->get();
                    }
                break;
            case 'laporan-pembayaran':
                    if($request->start and $request->end){
                        $records = TransPyr::select('*')
                                                    ->where('tmuk_kode',$tmuk_kode)
                                                    ->where('status',2)
                                                    ->whereBetween(\DB::raw('date(tgl_jatuh_tempo)'),array($start,$end))
                                                    ->orderBy('vendor_lokal','asc')
                                                    ->orderBy('tgl_jatuh_tempo','asc');

                      

                        $record = $records->get();
                    }
                break;
            case 'umur-pemasok':
                    if($request->start and $request->end){
                        $records = TransPyr::select('*')
                                                    ->where('tmuk_kode',$tmuk_kode)
                                                    ->whereBetween(\DB::raw('date(tgl_jatuh_tempo)'),array($start,$end))
                                                    ->orderBy('vendor_lokal','asc')
                                                    ->orderBy('tgl_jatuh_tempo','asc');

                      

                        $record = $records->get();
                    }
                break;
            default:
                break;
        }

        $data = [
            'request'           => $request->all(),
            'start'             => Carbon::parse($start)->format('d/m/Y'),
            'end'               => Carbon::parse($end)->format('d/m/Y'),
            'tmuk_kode'         => $tmuk_kode,
            'tmuk'              => $tmuk->nama,
            'lsi'               => $tmuk->lsi->nama,
            'fiskal'            => TahunFiskal::getTahunFiskal(),
            'region'            => $tmuk->lsi->region->area,
            'record'            => $record,
            // 'recordakuisisi'    => $recordakuisisi,
            // 'saldo'             => $saldo,
            // 'penjualan'         => $rcd,
            'label_file'        => $laporan.' '.$tmuk_kode.' '.$start.' s/d '.$end,
            // 'dayStart'          => Carbon::createFromFormat('m/Y', $request->start)->startOfMonth(),
            'periode'           => $start.' s/d '.$end,
            // 'kki'               => $kki,
            // 'jurnal_perform'    => $jurnal_perform,
            // 'diff'              => $diff,
        ];


        $content = view('report.laporan.akun-utang-usaha.'.$request->laporan, $data);

        // return $content;
        HTML2PDF($content, $data);
    }

    // PRINT LAPORAN EXEL
    //PEMBELIAN
    public function exelAkunUtangUsaha(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $record = [];
        $data   = [];
        $temp   = [];
        $tmuk_kode = $tmuk->kode;

        if($request->start and $request->end){
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
        }

        // dd($tanggal);
        switch ($request->laporan) {
            case 'saldo-pemasok':
                $this->exelSaldoPemasok($tmuk_kode, $tanggal);
                break;
            case 'umur-pemasok':
                $this->exelUmurPemasok($tmuk_kode, $tanggal);
                break;
            case 'laporan-pembayaran':
                $this->exelLaporanPembayaran($tmuk_kode, $tanggal);
                break;
    
            default:
                // code...
                break;
        }
    }

    public function exelSaldoPemasok($tmuk_kode, $tanggal)
    {
        $records = TransPyr::from(\DB::raw("(SELECT
                                              trans_jurnal.coa_kode,
                                              trans_jurnal.tanggal,
                                              trans_pyr.tgl_buat,
                                              trans_pyr.vendor_lokal,
                                              trans_jurnal.tmuk_kode,
                                              trans_jurnal.idtrans as id_jurnal,
                                              trans_pyr.nomor_pyr,
                                              trans_jurnal.jumlah as charges_jurnal,
                                              trans_jurnal.deskripsi,
                                              trans_jurnal.posisi,
                                              trans_pyr_pembayaran.verifikasi4_user
                                              FROM trans_pyr
                                              LEFT JOIN trans_jurnal ON trans_pyr.id_transaksi = trans_jurnal.idtrans
                                              LEFT JOIN trans_pyr_pembayaran ON trans_pyr.nomor_pyr = trans_pyr_pembayaran.nomor_pyr )  d"))
                                            ->where('coa_kode', '2.1.1.0')
                                            ->where('tmuk_kode',  $tmuk_kode)
                                            ->whereBetween(\DB::raw('date(tanggal)'),array($tanggal));

        $record = $records->get();
        
        // dd($record);
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Saldo Pemasok', function($excel) use ($record, $start, $end){
            $excel->setTitle('Saldo Pemasok');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Saldo Pemasok', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Saldo Pemasok'
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Vendor Lokal',
                    'Trans Type',
                    '#',
                    'Date',
                    'Due Date',
                    'Charges',
                    'Credits',
                    'Allocated',
                    'Outstanding'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    if($val->vendor_lokal == 100){
                        $vendor_lokal = $val->tmuk->nama ;
                      }else if($val->vendor_lokal == 0){
                        $vendor_lokal = $val->tmuk->lsi->nama ;
                      }else{
                        $vendor_lokal = isset($val->vendorlokal->nama)? $val->vendorlokal->nama:'-';
                      }

                            if($val->vendor_lokal == 100)
                                $ref =  $val->tmuk->kode ;
                            elseif($val->vendor_lokal == 0)
                                $ref = $val->tmuk->lsi->kode;
                            else
                                $ref = $val->vendorlokal->kode;


                            if($val->verifikasi4_user == NULL)
                                    $charges = $val->posisi == 'K' ? '0': rupiah($val->charges_jurnal);
                                else
                                    $charges = rupiah(0);

                    $sheet->row($row, array(
                        $no,
                        $vendor_lokal,
                        $val->verifikasi4_user == NULL ? 'Supplier Invoice': 'Pembayaran Ke Pemasok',
                        $ref,
                        $val->tgl_buat,
                        $val->tgl_buat,
                        $charges,
                        $val->verifikasi4_user == NULL ? '0': rupiah($val->charges_jurnal),
                        $val->verifikasi4_user == NULL ? '0': rupiah($val->charges_jurnal),
                        $val->verifikasi4_user == NULL ? rupiah($val->charges_jurnal) : '0',
                        ));
                    $row=$row;
                    $row++;$no++;
                }


                $sheet->cells('E4'.':J'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:J'.$row, 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:J2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:J3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>40, 'G'=>40, 'H'=>40, 'I'=>40, 'J'=>40));
                $sheet->getStyle('B4:J'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:J'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelUmurPemasok($tmuk_kode, $tanggal)
    {
        $records = TransPyr::select('*')
                            ->where('tmuk_kode',$tmuk_kode)
                            ->whereBetween(\DB::raw('date(tgl_jatuh_tempo)'),array($tanggal))
                            ->orderBy('vendor_lokal','asc')
                            ->orderBy('tgl_jatuh_tempo','asc');

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Analysis Umur Utang Usaha', function($excel) use ($record, $start, $end){
            $excel->setTitle('Analysis Umur Utang Usaha');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Analysis Umur Utang Usaha', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Analysis Umur Utang Usaha'
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Supplier',
                    'Currency',
                    '1-30 Days',
                    '31-60 Days',
                    'Over 60 Days',
                    'Total Balance'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    if($val->vendor_lokal == 100){
                        $vendor_lokal = $val->tmuk->nama ;
                      }else if($val->vendor_lokal == 0){
                        $vendor_lokal = $val->tmuk->lsi->nama ;
                      }else{
                        $vendor_lokal = isset($val->vendorlokal->nama)? $val->vendorlokal->nama:'-';
                      }

                      $st       = date_create(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$val->created_at)->format('Y-m-d'));
                      $en       = date_create();
                      $diff     = date_diff( $st, $en );
                      $day_1    = $diff->days; 
                      $day_satu    = $day_1 <= 30 ? rupiah(isset($val->detailPyr) ? $val->detailPyr->sum('price') : 0) : '0';
                      $day_2    = $diff->days;  
                      $day_dua    = $day_2 > 30 && $day_2 <= 60 ? rupiah(isset($val->detailPyr) ? $val->detailPyr->sum('price') : 0) : '0';
                      $day_3    = $diff->days;      
                      $day_tiga    = $day_3 > 60 ? rupiah(isset($val->detailPyr) ? $val->detailPyr->sum('price') : 0) : '0';
                      $balance  = rupiah(isset($val->detailPyr) ? $val->detailPyr->sum('price') : 0) ;

                    $sheet->row($row, array(
                        $no,
                        $vendor_lokal,
                        $val->tgl_jatuh_tempo,
                        $day_satu,
                        $day_dua,
                        $day_tiga,
                        $balance,
                        ));
                    $row=$row;
                    $row++;$no++;
                }


                $sheet->cells('E4'.':J'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>40, 'F'=>40, 'G'=>40));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelLaporanPembayaran($tmuk_kode, $tanggal)
    {
        $records = TransPyr::select('*')
                            ->where('tmuk_kode',$tmuk_kode)
                            ->where('status',2)
                            ->whereBetween(\DB::raw('date(tgl_jatuh_tempo)'),array($tanggal))
                            ->orderBy('vendor_lokal','asc')
                            ->orderBy('tgl_jatuh_tempo','asc');

        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Laporan Pembayaran', function($excel) use ($record, $start, $end){
            $excel->setTitle('Laporan Pembayaran');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Laporan Pembayaran', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Laporan Pembayaran'
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Supplier',
                    '#',
                    'Jatuh tempo',
                    'Total'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    if($val->vendor_lokal == 100){
                        $vendor_lokal = $val->tmuk->nama ;
                      }else if($val->vendor_lokal == 0){
                        $vendor_lokal = $val->tmuk->lsi->nama ;
                      }else{
                        $vendor_lokal = isset($val->vendorlokal->nama)? $val->vendorlokal->nama:'-';
                      }


                    $sheet->row($row, array(
                        $no,
                        $vendor_lokal,
                        $val->urutan($val->id),
                        $val->tgl_jatuh_tempo,
                        rupiah(isset($val->detailPyr) ? $val->detailPyr->sum('price') : 0) 
                        // $day_satu,
                        // $day_dua,
                        // $day_tiga,
                        // $balance,
                        ));
                    $row=$row;
                    $row++;$no++;
                }


                $sheet->cells('E4'.':J'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:E'.$row, 'thin');
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:E2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:E3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>40,'C'=>20, 'D'=>30, 'E'=>40));
                $sheet->getStyle('B4:E'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:E'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

}
