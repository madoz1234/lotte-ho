<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

//Models
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;
use Lotte\Models\Trans\TransRekapPenjualanTmukBayar;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransJurnal;
use Carbon\Carbon;

class PenjualanController extends Controller
{
    protected $link = 'laporan/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan");
        
    }
    public function grid(Request $request)
    {
        // dd($request->all());

        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);


        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;
        // dd($tmuk_kode);
        $data  = [];
        switch ($request->type) {
          case 'penjualan-harian':
              $record = collect(new TransRekapPenjualanTmuk);
              $utang = collect(new TransRekapPenjualanTmuk);
              $cash = collect(new TransRekapPenjualanTmuk);
              $vou = collect(new TransRekapPenjualanTmuk);
              $poin = collect(new TransRekapPenjualanTmuk);
              if($request->start and $request->end){
                  // $records = TransRekapPenjualanTmuk::select(DB::raw('DATE("tanggal") as date, SUM("total") as total_penjualan'))->groupBy('date');
                  $records = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as date, SUM(trans_rekap_penjualan_detail.harga * trans_rekap_penjualan_detail.qty) as total_penjualan'))
                  ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->groupBy('date');

                  $donasi = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                  ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->where('trans_rekap_penjualan_bayar.tipe', 'DONASI')
                  ->groupBy('tanggal');

                  $tunai = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                  ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->where('trans_rekap_penjualan_bayar.tipe', 'TUNAI')
                  ->groupBy('tanggal');

                  $voucher = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                  ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->where('trans_rekap_penjualan_bayar.tipe', 'VOUCHER')
                  ->groupBy('tanggal');

                  $diskon = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as date , SUM(trans_rekap_penjualan_detail.discount) as diskon'))
                  ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->groupBy('date');

                  $point = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                  ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->where('trans_rekap_penjualan_bayar.tipe', 'POINT')
                  ->groupBy('tanggal');
                  // dd($point);
                  // dd($request->all());
                  $piutang = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                  ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->where('trans_rekap_penjualan_bayar.tipe', 'PIUTANG')
                  ->groupBy('tanggal');

                  if ($tmuk_kode = $tmuk_kode) {
                    $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    $donasi->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    $tunai->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    $voucher->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    $diskon->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    $point->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    $piutang->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                  }

                  if($request->start and $request->end){
                  $start = $request->start;
                  $end   = $request->end;
                  $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $point->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $donasi->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $tunai->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $voucher->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $diskon->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $piutang->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  }

                  $record = $records->get();
                  $utang = $piutang->get();
                  $vou = $voucher->get();
                  $dis = $diskon->get();
                  $cash = $tunai->get();
                  $dos = $donasi->get();
                  $poin = $point->get();
                  $data = [
                  'detail' => $record,
                  'piutang' => $utang,
                  'donasi' => $dos,
                  'tunai' => $cash,
                  'voucher' => $vou,
                  'diskon' => $dis,
                  'point' => $poin,
                  // 'pembayaran' => $bayars,
                  'start'  => $start,
                  'end'    => $end,
                  ];
              }
            break;
          case 'penjualan-mingguan':
              if($request->start==true and $request->end==true){

                $records = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year,extract(MONTH from tanggal) as month, extract(WEEK from tanggal) as week, SUM("total") as total_penjualan'))->groupBy('year', 'week','month')->orderBy('week','year','desc')->orderBy('week','desc');
                // test
                // $records = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as date, SUM(trans_rekap_penjualan_detail.harga * trans_rekap_penjualan_detail.qty) as total_penjualan'))
                //   ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                //   ->groupBy('date');
                // test

                $tunai = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from tanggal) as month,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                ->where('trans_rekap_penjualan_bayar.tipe', 'TUNAI')
                ->groupBy('year','week','month');

                $donasi = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from tanggal) as month,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                ->where('trans_rekap_penjualan_bayar.tipe', 'DONASI')
                ->groupBy('year','week','month');

                $diskon = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from tanggal) as month,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_detail.discount) as diskon'))
                ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                ->groupBy('year','week','month');

                $voucher = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from tanggal) as month,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                ->where('trans_rekap_penjualan_bayar.tipe', 'VOUCHER')
                ->groupBy('year','week','month');

                $point = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from tanggal) as month,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                ->where('trans_rekap_penjualan_bayar.tipe', 'POINT')
                ->groupBy('year','week','month');

                $piutang = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from tanggal) as month,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                ->where('trans_rekap_penjualan_bayar.tipe', 'PIUTANG')
                ->groupBy('year','week','month');

                if ($tmuk_kode = $tmuk_kode) {
                $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                $donasi->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                $tunai->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                $voucher->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                $diskon->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                $point->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                $piutang->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                }

                if($request->start==true and $request->end==true){
                $start = $request->start;
                $end   = $request->end;
                $records->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $point->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $piutang->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $donasi->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $tunai->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $voucher->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $diskon->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                }

                $record = $records->get();
                $utang = $piutang->get();
                $dos = $donasi->get();
                $cash = $tunai->get();
                $poin = $point->get();
                $vou = $voucher->get();
                $dis = $diskon->get();

                // dd($record);

                $data = [
                'detail' => $record,
                'piutang' => $utang,
                'donasi' => $dos,
                'tunai' => $cash,
                'voucher' => $vou,
                'diskon' => $dis,
                'point' => $poin,
                'start'  => $start,
                'end'    => $end,
                ];
              }
            break;
          case 'penjualan-bulanan':
              if($request->start and $request->end){
                $records = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("total") as total_penjualan'))->groupBy('year', 'month')
                  ->orderBy('year','desc')
                  ->orderBy('month','desc')
                  ->where('tmuk_kode', $tmuk_kode);

                $donasi = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                 ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->where('trans_rekap_penjualan_bayar.tipe', 'DONASI')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $tunai = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                 ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->where('trans_rekap_penjualan_bayar.tipe', 'TUNAI')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $voucher = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                 ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->where('trans_rekap_penjualan_bayar.tipe', 'VOUCHER')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $diskon = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year, extract(MONTH from trans_rekap_penjualan.tanggal) as month, SUM(trans_rekap_penjualan_detail.discount) as diskon, SUM(trans_rekap_penjualan.kembalian) as kembali'))
                 ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $point = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                 ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->where('trans_rekap_penjualan_bayar.tipe', 'POINT')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $piutang = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                 ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->where('trans_rekap_penjualan_bayar.tipe', 'PIUTANG')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $start = $request->start.'-1';
                $_end   = $request->end.'-1';
                $end = date("Y-m-t", strtotime($_end));

                // dd($start);
                $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $donasi->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $point->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $piutang->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $tunai->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $voucher->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $diskon->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));

                $data = [
                  'detail'  => $records->get(),
                  'donasi'  => $donasi->get(),
                  'piutang' => $piutang->get(),
                  'tunai'   => $tunai->get(),
                  'voucher' => $voucher->get(),
                  'diskon'  => $diskon->get(),
                  'point'   => $point->get(),
                  'start'   => $start,
                  'end'     => $end,
                ];
              }
            break;
          case 'penjualan-per-barang':
              if($request->start and $request->end){
                $start = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $end   = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');

                // $records = TransRekapPenjualanTmukDetail::select(DB::raw('DATE(trans_rekap_penjualan.tanggal) as tanggal , trans_rekap_penjualan_detail.item_code as item_code, trans_rekap_penjualan_detail.description as description,
                //   SUM(trans_rekap_penjualan_detail.qty) as jml_qty, trans_rekap_penjualan_detail.harga as harga,
                //   SUM(trans_rekap_penjualan_detail.total) as jml_total'))
                //  ->join('trans_rekap_penjualan', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                //  ->join('ref_harga_tmuk', 'trans_rekap_penjualan_detail.item_code', '=', 'ref_harga_tmuk.produk_kode')
                //  ->groupBy('tanggal','item_code','harga','description')
                //  ->whereHas('rekappenjualan', function ($query) use ($tmuk_kode){
                //       $query->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                //   })
                //  ->whereBetween(\DB::raw('DATE(trans_rekap_penjualan.tanggal)'), array($start, $end));
                $records = TransRekapPenjualanTmukDetail::from(\DB::raw("
                (
                  SELECT 
                  DATE(pd.created_at) as tanggal,
                  pt.tmuk_kode as tmuk_kode,
                  pd.item_code as produk,
                  pd.description as nama,
                  pd.qty as qty,
                  pd.harga as harga,
                  pd.total as total,
                  case when td.hpp > 0 then td.hpp else 0 end as hpp
                  FROM trans_rekap_penjualan_detail pd
                  join trans_rekap_penjualan p on p.jual_kode = pd.rekap_penjualan_kode
                  left join ref_produk_tmuk pt on pt.tmuk_kode = p.tmuk_kode AND pt.produk_kode = pd.item_code
                  left join ref_produk_tmuk_detail td on td.produk_tmuk_kode = pt.id
                  and td.date between '".$start."' and '".$end."'
                  WHERE pd.created_at between '".$start." 00:00:00' and '".$end." 23:59:59'

                    order by pd.created_at DESC
                ) tblx
              "));
                // dd($records);

                $data = [
                  'detail' => $records->get(),
                  'start'  => Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d'),
                  'end'    => Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d'),
                ];
              }
            break;
          case 'setoran-penjualan':
              $record = collect(new TransRekapPenjualanTmuk);
              // $saldo = collect(new TransJurnal);
              if($request->start and $request->end){
                  $records = TransRekapPenjualanTmuk::select(DB::raw('DATE("tanggal") as date, SUM("total") as total_penjualan'))
                              ->groupBy('date')
                              ->where('tmuk_kode', $tmuk_kode);

                  // $saldo = TransJurnal::select(DB::raw('DATE("tanggal") as date, SUM("jumlah") as total_penjualan'))
                  $saldo  = TransJurnal::select(DB::raw('DATE("tanggal") as date, SUM("jumlah") as total'))->groupBy('date')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->where('delete', '0')
                                ->where('posisi', 'D')
                                ->where('deskripsi', 'ilike', 'Top Up Escrow Balance - Setoran Penjualan Tunai');
                                // ->whereBetween(\DB::raw('tanggal'), array($start, $end))
                                // ->where('coa_kode', '1.1.1.1');
                                // ->groupBy('date');
                                // ->orderBy('tanggal');
                                // ->get();

                  // if ($tmuk_kode = $tmuk_kode) {
                  //   $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                  // }
                  

                  if($request->start and $request->end){
                  $start = $request->start;
                  $end   = $request->end;
                  $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $saldo   = $saldo;
                  // $saldo->whereBetween(\DB::raw('date(trans_jurnal.tanggal)'),array($start,$end));
                  }

                  $record = $records->get();
                  $saldo = $saldo->get();

                  $data = [
                  'saldo'  => $saldo,
                  'detail' => $record,
                  'start'  => $start,
                  'end'    => $end,
                  ];
              }
            break;
          default:
            break;
        }
        /*return views*/
        return view('modules.laporan.laporan.lists.penjualan.'.$request->type, $data);
    }

    //PRINT LAPORAN
    // PRINT LAPORAN PDF
    //PEMBELIAN
    public function printPenjualan(Request $request){

        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        // dd($request->all());
        $record = [];
        $data   = [];
        $temp   = [];
        $tmuk_kode = $tmuk->kode;

        $data = [
        'records'   => $temp,
        ];
        switch ($request->laporan) {
            case 'penjualan-harian':
            $records = TransRekapPenjualanTmuk::select(DB::raw('DATE("tanggal") as date, SUM("total") as total_penjualan, COUNT("jual_kode") as struk'))->groupBy('date');
            $donasi = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'DONASI')
             ->groupBy('tanggal');

            $tunai = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'TUNAI')
             ->groupBy('tanggal');

            $voucher = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'VOUCHER')
             ->groupBy('tanggal');

            $diskon = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as date , SUM(trans_rekap_penjualan_detail.discount) as diskon'))
             ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->groupBy('date');

             $point = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'POINT')
             ->groupBy('tanggal');

             $piutang = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'PIUTANG')
             ->groupBy('tanggal');

            if ($tmuk_kode = $tmuk_kode) {
                $records->where('tmuk_kode', $tmuk_kode);
                $donasi->where('tmuk_kode', $tmuk_kode);
                $tunai->where('tmuk_kode', $tmuk_kode);
                $voucher->where('tmuk_kode', $tmuk_kode);
                $diskon->where('tmuk_kode', $tmuk_kode);
                $point->where('tmuk_kode', $tmuk_kode);
                $piutang->where('tmuk_kode', $tmuk_kode);
            }
            if($request->start and $request->end){
              $start = $request->start;
              $end   = $request->end;
              $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $point->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $donasi->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $tunai->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $diskon->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $voucher->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $piutang->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
            }

            $record          = $records->get();
            $donasi          = $donasi->get();
            $utang           = $piutang->get();
            $poin            = $point->get();
            $cash            = $tunai->get();
            $diskon          = $diskon->get();
            $voucher         = $voucher->get();

            $data           = $record;
            $req            = $request->all();
            $region         = $tmuk->lsi->region->area;
            $lsi            = $tmuk->lsi->nama;
            $tmuk           = $tmuk->nama;
            $tahun_fiskal   = TahunFiskal::getTahunFiskal();

            $data = [
            'records'   => $record,
            'donasi'   => $donasi,
            'piutang'   => $utang,
            'point'   => $poin,
            'tunai'   => $cash,
            'diskon'   => $diskon,
            'voucher'   => $voucher,
            'request'   => $req,
            'fiskal'    => $tahun_fiskal,
            'region'    => $region,
            'lsi'       => $lsi,
            'tmuk'      => $tmuk,
            ];
            break;
            case 'penjualan-mingguan':

            $records = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year, extract(WEEK from tanggal) as week, SUM("total") as total_penjualan'))->groupBy('year', 'week');

            $donasi = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'DONASI')
             ->groupBy('year','week');

            $tunai = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'TUNAI')
             ->groupBy('year','week');

            $voucher = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'VOUCHER')
             ->groupBy('year','week');

            $diskon = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_detail.discount) as diskon'))
             ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->groupBy('year','week');

             $point = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'POINT')
             ->groupBy('tanggal');

             $piutang = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'PIUTANG')
             ->groupBy('tanggal');

            if ($tmuk_kode = $tmuk_kode) {
                $records->where('tmuk_kode', $tmuk_kode);
                $donasi->where('tmuk_kode', $tmuk_kode);
                $tunai->where('tmuk_kode', $tmuk_kode);
                $voucher->where('tmuk_kode', $tmuk_kode);
                $diskon->where('tmuk_kode', $tmuk_kode);
                $point->where('tmuk_kode', $tmuk_kode);
                $piutang->where('tmuk_kode', $tmuk_kode);
            }
            if($request->start==true and $request->end==true){
                $start = $request->start;
                $end   = $request->end;
                $records->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $donasi->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $tunai->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $voucher->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $diskon->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $point->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $piutang->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
            }

            $record         = $records->get();
            $dos         = $donasi->get();
            $utang         = $piutang->get();
            $poin         = $point->get();
            $cash         = $tunai->get();
            $vou         = $voucher->get();
            $diskon         = $diskon->get();

            $data           = $record;
            $req            = $request->all();
            $region         = $tmuk->lsi->region->area;
            $lsi            = $tmuk->lsi->nama;
            $tmuk           = $tmuk->nama;
            $tahun_fiskal   = TahunFiskal::getTahunFiskal();

            $data = [
            'records'   => $record,
            'donasi'   => $dos,
            'piutang'   => $utang,
            'point'   => $poin,
            'tunai'   => $cash,
            'voucher'   => $vou,
            'diskon'   => $diskon,
            'request'   => $req,
            'fiskal'    => $tahun_fiskal,
            'region'    => $region,
            'lsi'       => $lsi,
            'tmuk'      => $tmuk,
            ];
            break;
            case 'penjualan-bulanan':
            //
            if($request->start and $request->end){
              $records = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("total") as total_penjualan'))->groupBy('year', 'month')
                ->orderBy('year','desc')
                ->orderBy('month','desc')
                ->where('tmuk_kode', $tmuk_kode);

              $donasi = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
               ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
               ->where('trans_rekap_penjualan_bayar.tipe', 'DONASI')
               ->groupBy('year','month')
               ->where('tmuk_kode', $tmuk_kode);

              $tunai = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
               ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
               ->where('trans_rekap_penjualan_bayar.tipe', 'TUNAI')
               ->groupBy('year','month')
               ->where('tmuk_kode', $tmuk_kode);

              $voucher = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
               ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
               ->where('trans_rekap_penjualan_bayar.tipe', 'VOUCHER')
               ->groupBy('year','month')
               ->where('tmuk_kode', $tmuk_kode);

              $diskon = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year, extract(MONTH from trans_rekap_penjualan.tanggal) as month, SUM(trans_rekap_penjualan_detail.discount) as diskon, SUM(trans_rekap_penjualan.kembalian) as kembali'))
                 ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

              $point = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
               ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
               ->where('trans_rekap_penjualan_bayar.tipe', 'POINT')
               ->groupBy('year','month')
               ->where('tmuk_kode', $tmuk_kode);

              $piutang = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
               ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
               ->where('trans_rekap_penjualan_bayar.tipe', 'PIUTANG')
               ->groupBy('year','month')
               ->where('tmuk_kode', $tmuk_kode);

              $start = $request->start.'-1';
              $_end   = $request->end.'-1';
              $end = date("Y-m-t", strtotime($_end));

              // dd($start);
              $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $donasi->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $point->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $piutang->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $tunai->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $voucher->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
              $diskon->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));

              $record         = $records->get();
              $dos         = $donasi->get();
              $utang         = $piutang->get();
              $poin         = $point->get();
              $cash         = $tunai->get();
              $vou         = $voucher->get();
              $dis         = $diskon->get();

              $data           = $record;
              $req            = $request->all();
              $region         = $tmuk->lsi->region->area;
              $lsi            = $tmuk->lsi->nama;
              $tmuk           = $tmuk->nama;
              $tahun_fiskal   = TahunFiskal::getTahunFiskal();

              $data = [
                'start'   => $start,
                'end'     => $end,

                'records'   => $record,
                'piutang'   => $utang,
                'point'   => $poin,
                'tunai'   => $cash,
                'voucher'   => $vou,
                'diskon'   => $dis,
                'donasi'   => $dos,
                'request'   => $req,
                'fiskal'    => $tahun_fiskal,
                'region'    => $region,
                'lsi'       => $lsi,
                'tmuk'      => $tmuk,
              ];
            }
            //
            break;
            case 'penjualan-per-barang':
            
            if($request->start and $request->end){
                $start = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $end   = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');

                $records = TransRekapPenjualanTmukDetail::from(\DB::raw("
                (
                  SELECT 
                  DATE(pd.created_at) as tanggal,
                  pt.tmuk_kode as tmuk_kode,
                  pd.item_code as produk,
                  pd.description as nama,
                  pd.qty as qty,
                  pd.harga as harga,
                  pd.total as total,
                  case when td.hpp > 0 then td.hpp else 0 end as hpp
                  FROM trans_rekap_penjualan_detail pd
                  join trans_rekap_penjualan p on p.jual_kode = pd.rekap_penjualan_kode
                  left join ref_produk_tmuk pt on pt.tmuk_kode = p.tmuk_kode AND pt.produk_kode = pd.item_code
                  left join ref_produk_tmuk_detail td on td.produk_tmuk_kode = pt.id
                  and td.date between '".$start."' and '".$end."'
                  WHERE pd.created_at between '".$start." 00:00:00' and '".$end." 23:59:59'

                    order by pd.created_at DESC
                ) tblx
              "));

                 $record         = $records->get();
                 // dd($record);

                $data           = $record;
                $req            = $request->all();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
            
                $data = [
                  'records'   => $record,
                  'request'   => $req,
                  'fiskal'    => $tahun_fiskal,
                  'region'    => $region,
                  'lsi'       => $lsi,
                  'tmuk'      => $tmuk,
                ];
              }
            break;
            case 'setoran-penjualan':
              if($request->start and $request->end){
                  $records = TransRekapPenjualanTmuk::select(DB::raw('DATE("tanggal") as date, SUM("total") as total_penjualan'))
                              ->groupBy('date')
                              ->where('tmuk_kode', $tmuk_kode);

                  $saldo  = TransJurnal::select(DB::raw('DATE("tanggal") as date, SUM("jumlah") as total'))->groupBy('date')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->where('delete', '0')
                                ->where('posisi', 'D')
                                ->where('deskripsi', 'ilike', 'Top Up Escrow Balance - Setoran Penjualan Tunai');

                  if($request->start==true and $request->end==true){
                  $start = $request->start;
                  $end   = $request->end;
                  // $saldo   = $saldo;
                  $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'), array($request->start, $request->end));
                  }

                  $record = $records->get();
                  $saldo = $saldo->get();

                  $data           = $record;
                  $req            = $request->all();
                  $region         = $tmuk->lsi->region->area;
                  $lsi            = $tmuk->lsi->nama;
                  $tmuk           = $tmuk->nama;
                  $tahun_fiskal   = TahunFiskal::getTahunFiskal();

                  $data = [
                  'records'   => $record,
                  'saldo'     => $saldo,
                  'request'   => $req,
                  'fiskal'    => $tahun_fiskal,
                  'region'    => $region,
                  'lsi'       => $lsi,
                  'tmuk'      => $tmuk,
                  ];
              }
            break;
            default:

            break;
        }

        $content = view('report.laporan.penjualan.'.$request->laporan, $data);

        // return $content;
        HTML2PDF($content, $data);
    }
    // END LAPORAN PEMBELIAN



    // PRINT LAPORAN EXEL
    //PEMBELIAN
    public function exelPenjualan(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $request = $request;
        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;
        // dd($tmuk_kode);
        $data  = [];
        switch ($request->laporan) {
            case 'penjualan-harian':
            $this->exelPenjualanHarian($tmuk_kode, $request);
            break;
            case 'penjualan-mingguan':
            $this->exelPenjualanMingguan($tmuk_kode, $request);
            break;
            case 'penjualan-bulanan':
            $this->exelPenjualanBulanan($tmuk_kode, $request);
            break;
            case 'penjualan-per-barang':
            $this->exelPenjualanPerBarang($tmuk_kode, $request);
            break;
            case 'setoran-penjualan':
            $this->exelSetoranPenjualan($tmuk_kode, $request);
            break;
            default:

            break;
        }
    }

    public function exelPenjualanHarian($tmuk_kode, $request)
    {
      // dd($request->start);

        $record = collect(new TransRekapPenjualanTmuk);
              $utang = collect(new TransRekapPenjualanTmuk);
              $cash = collect(new TransRekapPenjualanTmuk);
              $poin = collect(new TransRekapPenjualanTmuk);
              $vou = collect(new TransRekapPenjualanTmuk);

                  $records = TransRekapPenjualanTmuk::select(DB::raw('DATE("tanggal") as date, SUM("total") as total_penjualan'))->groupBy('date');

                  $tunai = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                  ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->where('trans_rekap_penjualan_bayar.tipe', 'TUNAI')
                  ->groupBy('tanggal');

                  $voucher = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                  ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->where('trans_rekap_penjualan_bayar.tipe', 'VOUCHER')
                  ->groupBy('tanggal');

                  $diskon = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as date , SUM(trans_rekap_penjualan_detail.discount) as diskon'))
                  ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->groupBy('date');

                  $point = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                  ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->where('trans_rekap_penjualan_bayar.tipe', 'POINT')
                  ->groupBy('tanggal');

                  $piutang = TransRekapPenjualanTmuk::select(DB::raw('date(trans_rekap_penjualan.tanggal) as tanggal , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                  ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                  ->where('trans_rekap_penjualan_bayar.tipe', 'PIUTANG')
                  ->groupBy('tanggal');

                  if ($tmuk_kode = $tmuk_kode) {
                  $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                  $tunai->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                  $voucher->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                  $point->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                  $piutang->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                  $diskon->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                  }

                  if($request->start and $request->end){
                  $start = $request->start;
                  $end   = $request->end;
                  $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $point->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $tunai->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $voucher->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $diskon->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  $piutang->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  }

                  $record = $records->get();
                  $hutang = $piutang->get();
                  $tunai = $tunai->get();
                  $diskon = $diskon->get();
                  $voucher = $voucher->get();
                  $point = $point->get();
        
        $Export = Excel::create('PENJUALAN HARIAN ', function($excel) use ($record, $tunai, $hutang, $point, $voucher, $diskon, $request){
            $excel->setTitle('PENJUALAN HARIAN');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('PENJUALAN HARIAN ', function($sheet) use ($record, $tunai, $hutang, $point, $voucher, $diskon, $request){
                $sheet->row(1, array(
                    'PENJUALAN HARIAN '
                    ));

                $sheet->row(2, array(
                  'Periode : ' . $request->start . ' - ' . $request->end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal',
                    'Total Penjualan (Rp)',
                    'Penjualan Tunai (Rp)',
                    'Piutang (Rp)',
                    'Poin (Rp)',
                    'Diskon (Rp)',
                    'Voucher (Rp)'
                    ));

                $row = 4;
                $no =1;
                foreach ($record->sortBy('date') as $val) {

                        $cash=0;
                        foreach($tunai as $tn){
                            if($tn->tanggal==$val->date){
                              $cash=$cash+($tn->tunai-$tn->kembali);
                            }
                        }

                        $utang=0;
                        foreach($hutang as $tang){
                            if($tang->tanggal==$val->date){
                              $utang=$utang+($tang->tunai-$tang->kembali);
                            }
                        }

                        $poin=0;
                        foreach($point as $in){
                            if($in->tanggal==$val->date){
                              $poin=$poin+($in->tunai-$in->kembali);
                            }
                        }

                        $dis=0;
                        foreach($diskon as $rowdis){
                            if($rowdis->date==$val->date){
                              $dis=$rowdis->diskon;
                            }
                        }

                        $vou=0;
                        foreach($voucher as $vc){
                            if($vc->tanggal==$val->date){
                              $vou=$vou+($vc->kembali);
                            }
                        }

                    $sheet->row($row, array(
                        $no,
                        $val->date,
                        rupiah($cash + $utang + $poin + $dis + $vou),
                        rupiah($cash),
                        rupiah($utang),
                        rupiah($poin),
                        number_format($dis),
                        number_format($vou),
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':H'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:H'.$row, 'thin');
                $sheet->mergeCells('A1:H1');
                $sheet->mergeCells('A2:H2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:H2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:H3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>25, 'D'=>25, 'E'=>25, 'F'=>25, 'G' =>25, 'H' =>25));
                $sheet->getStyle('B4:H'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:H'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelPenjualanMingguan($tmuk_kode, $request)
    {
      // dd($request->start);

        $records = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year,extract(MONTH from tanggal) as month, extract(WEEK from tanggal) as week, SUM("total") as total_penjualan'))->groupBy('year', 'week','month');

            $tunai = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'TUNAI')
             ->groupBy('year','week');

            $voucher = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'VOUCHER')
             ->groupBy('year','week');

            $diskon = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_detail.discount) as diskon , SUM(trans_rekap_penjualan.kembalian) as kembali'))
             ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->groupBy('year','week');

             $point = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'POINT')
             ->groupBy('tanggal');

             $piutang = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(WEEK from trans_rekap_penjualan.tanggal) as week , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
             ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
             ->where('trans_rekap_penjualan_bayar.tipe', 'PIUTANG')
             ->groupBy('tanggal');

            if ($tmuk_kode = $tmuk_kode) {
              $records->where('tmuk_kode',$tmuk_kode);
              $tunai->where('tmuk_kode',$tmuk_kode);
              $voucher->where('tmuk_kode',$tmuk_kode);
              $diskon->where('tmuk_kode',$tmuk_kode);
              $point->where('tmuk_kode',$tmuk_kode);
              $piutang->where('tmuk_kode',$tmuk_kode);
            }

            if($request->start==true and $request->end==true){
                $start = $request->start;
                $end   = $request->end;
                $records->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $tunai->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $voucher->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $diskon->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $point->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
                $piutang->whereBetween(\DB::raw('date(tanggal)'), array($request->start, $request->end));
            }

            $record         = $records->get();
            $hutang         = $piutang->get();
            $point          = $point->get();
            $tn             = $tunai->get();
            $voucher        = $voucher->get();
            $diskon         = $diskon->get();

            // dd($tn);

        
        $Export = Excel::create('PENJUALAN MINGGUAN ', function($excel) use ($record, $tn, $hutang, $point, $voucher, $diskon, $request){
            $excel->setTitle('PENJUALAN MINGGUAN');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('PENJUALAN MINGGUAN ', function($sheet) use ($record, $tn, $hutang, $point, $voucher, $diskon, $request){
                $sheet->row(1, array(
                    'PENJUALAN MINGGUAN '
                    ));

                $sheet->row(2, array(
                  'Periode : ' . $request->start . ' - ' . $request->end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Minggu ke-',
                    'Tahun',
                    'Total Penjualan (Rp)',
                    'Penjualan Tunai (Rp)',
                    'Piutang (Rp)',
                    'Poin (Rp)',
                    'Diskon (Rp)',
                    'Voucher (Rp)'
                    ));

                $row = 4;
                $no =1;
                
                foreach ($record->sortBy('week') as $val) {

                      $cash=0;
                      foreach ($tn as $nai) {
                              if($nai->week==$val->week && $nai->year==$val->year) {
                                  $cash=$cash+($nai->tunai-$nai->kembali);
                              }
                            }
                    
                      $utang=0;
                      foreach ($hutang as $tang) {
                              if($tang->week==$val->week && $tang->year==$val->year) {
                                  $utang=$utang+$tang->tunai;
                              }
                            }

                      $poin=0;
                      foreach ($point as $in) {
                              if($in->week==$val->week && $in->year==$val->year) {
                                  $poin=$poin+$in->tunai;
                              }
                            }

                      $vou=0;
                      foreach ($voucher as $nai) {
                              if($nai->week==$val->week && $nai->year==$val->year) {
                                  $vou=$vou+($nai->tunai);
                              }
                            }

                      $dis=0; 
                      foreach($diskon as $rowdis)
                        if($rowdis->week==$val->week && $rowdis->year==$val->year)
                            $dis=$rowdis->diskon;


                    $sheet->row($row, array(
                        $no,
                        $val->week,
                        $val->year,
                        rupiah($cash + $utang + $poin + $vou + $dis),
                        rupiah($cash),
                        rupiah($utang),
                        rupiah($poin),
                        number_format($dis),
                        number_format($vou),
                        ));
                    $row=$row;
                    $row++;$no++;
                    // }
                  
                }
                $sheet->cells('E4'.':I'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:I'.$row, 'thin');
                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:I2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:I3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>25, 'I'=>25));
                $sheet->getStyle('B4:I'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:I'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelPenjualanBulanan($tmuk_kode, $request)
    {
      // dd($request->start);

        $records = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("total") as total_penjualan'))->groupBy('year', 'month')
                  ->orderBy('year','desc')
                  ->orderBy('month','desc')
                  ->where('tmuk_kode', $tmuk_kode);

                $tunai = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                 ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->where('trans_rekap_penjualan_bayar.tipe', 'TUNAI')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $voucher = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai , SUM(trans_rekap_penjualan.kembalian) as kembali'))
                 ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->where('trans_rekap_penjualan_bayar.tipe', 'VOUCHER')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $diskon = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year, extract(MONTH from trans_rekap_penjualan.tanggal) as month, SUM(trans_rekap_penjualan_detail.discount) as diskon'))
                 ->join('trans_rekap_penjualan_detail', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $point = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                 ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->where('trans_rekap_penjualan_bayar.tipe', 'POINT')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $piutang = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from trans_rekap_penjualan.tanggal) as year,extract(MONTH from trans_rekap_penjualan.tanggal) as month , SUM(trans_rekap_penjualan_bayar.total) as tunai'))
                 ->join('trans_rekap_penjualan_bayar', 'trans_rekap_penjualan_bayar.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
                 ->where('trans_rekap_penjualan_bayar.tipe', 'PIUTANG')
                 ->groupBy('year','month')
                 ->where('tmuk_kode', $tmuk_kode);

                $start = $request->start.'-1';
                $_end   = $request->end.'-1';
                $end = date("Y-m-t", strtotime($_end));

                // dd($start);
                $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $point->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $piutang->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $tunai->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $voucher->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                $diskon->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));

                $record = $records->get();
                $hutang = $piutang->get();
                $tn = $tunai->get();
                $point = $point->get();
                $voucher = $voucher->get();
                $diskon = $diskon->get();

        
        $Export = Excel::create('PENJUALAN BULANAN ', function($excel) use ($record, $tn, $hutang, $point, $voucher, $diskon, $request){
            $excel->setTitle('PENJUALAN BULANAN');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('PENJUALAN BULANAN ', function($sheet) use ($record, $tn, $hutang, $point, $voucher, $diskon, $request){
                $sheet->row(1, array(
                    'PENJUALAN BULANAN '
                    ));

                $sheet->row(2, array(
                  'Periode : ' . $request->start . ' - ' . $request->end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Bulan ke-',
                    'Tahun',
                    'Total Penjualan (Rp)',
                    'Penjualan Tunai (Rp)',
                    'Piutang (Rp)',
                    'Poin (Rp)',
                    'Diskon (Rp)',
                    'Voucher (Rp)',
                    ));

                $row = 4;
                $no =1;
                foreach ($record->sortBy('month') as $val) {
                      $cash=0;
                      foreach ($tn as $nai) {
                              if($nai->month==$val->month && $nai->year==$val->year) {
                                  $cash=$cash+($nai->tunai-$nai->kembali);
                              }
                            }
                    
                      $utang=0;
                      foreach ($hutang as $tang) {
                              if($tang->month==$val->month && $tang->year==$val->year) {
                                  $utang=$utang+$tang->tunai;
                              }
                            }

                      $poin=0;
                      foreach ($point as $in) {
                              if($in->month==$val->month && $in->year==$val->year) {
                                  $poin=$poin+$in->tunai;
                              }
                            }

                      $vou=0;
                      foreach ($voucher as $in) {
                              if($in->month==$val->month && $in->year==$val->year) {
                                  $vou=$vou+$in->tunai;
                              }
                            }

                      $dis=0;
                    foreach ($diskon as $rowdis){
                        if($rowdis->month==$val->month && $rowdis->year==$val->year) {
                            $dis=$rowdis->diskon;
                          }
                        }

                    $sheet->row($row, array(
                        $no,
                        $val->month,
                        $val->year,
                        rupiah($cash + $utang + $poin + $vou + $dis),
                        rupiah($cash),
                        rupiah($utang),
                        rupiah($poin),
                        number_format($dis),
                        number_format($vou),
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':I'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:I'.$row, 'thin');
                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:I2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:I3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>25, 'I'=>25));
                $sheet->getStyle('B4:I'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:I'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelPenjualanPerBarang($tmuk_kode, $request)
    {
      // dd($request->start);

        $start = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
        $end   = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');

        // $records = TransRekapPenjualanTmukDetail::select(DB::raw('DATE(trans_rekap_penjualan.tanggal) as tanggal , trans_rekap_penjualan_detail.item_code as item_code, trans_rekap_penjualan_detail.description as description,
        //   SUM(trans_rekap_penjualan_detail.qty) as jml_qty, trans_rekap_penjualan_detail.harga as harga,
        //   SUM(trans_rekap_penjualan_detail.total) as jml_total,
        //   SUM(trans_rekap_penjualan_detail.total)  - SUM(trans_rekap_penjualan_detail.total) as profit,
        //   (SUM(trans_rekap_penjualan_detail.total)  - SUM(trans_rekap_penjualan_detail.total)) / SUM(trans_rekap_penjualan_detail.total) as profitmargin '))
        //  ->join('trans_rekap_penjualan', 'trans_rekap_penjualan_detail.rekap_penjualan_kode', '=', 'trans_rekap_penjualan.jual_kode')
        //  ->join('ref_harga_tmuk', 'trans_rekap_penjualan_detail.item_code', '=', 'ref_harga_tmuk.produk_kode')
        //  ->groupBy('tanggal','item_code','harga','description')->orderBy('tanggal','desc')
        //  ->whereHas('rekappenjualan', function ($query) use ($tmuk_kode){
        //       $query->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        //   })
        //  ->whereBetween(\DB::raw('DATE(trans_rekap_penjualan.tanggal)'), array($start, $end));
        $records = TransRekapPenjualanTmukDetail::from(\DB::raw("
                (
                  SELECT 
                  DATE(pd.created_at) as tanggal,
                  pt.tmuk_kode as tmuk_kode,
                  pd.item_code as produk,
                  pd.description as nama,
                  pd.qty as qty,
                  pd.harga as harga,
                  pd.total as total,
                  case when td.hpp > 0 then td.hpp else 0 end as hpp
                  FROM trans_rekap_penjualan_detail pd
                  join trans_rekap_penjualan p on p.jual_kode = pd.rekap_penjualan_kode
                  left join ref_produk_tmuk pt on pt.tmuk_kode = p.tmuk_kode AND pt.produk_kode = pd.item_code
                  left join ref_produk_tmuk_detail td on td.produk_tmuk_kode = pt.id
                  and td.date between '".$start."' and '".$end."'
                  WHERE pd.created_at between '".$start." 00:00:00' and '".$end." 23:59:59'

                    order by pd.created_at DESC
                ) tblx
              "));
        $record = $records->get();
                // dd($record);
        
        $Export = Excel::create('Rekap Penjualan per Barang ', function($excel) use ($record, $request){
            $excel->setTitle('Rekap Penjualan per Barang');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Penjualan per Barang ', function($sheet) use ($record, $request){
                $sheet->row(1, array(
                    'Rekap Penjualan per Barang '
                    ));

                $sheet->row(2, array(
                  'Periode : ' . $request->start . ' - ' . $request->end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal',
                    'Kode Produk',
                    'Nama Produk',
                    'Qty',
                    'Harga Jual (Rp)',
                    'Penjualan',
                    'Hpp (Rp)',
                    'Profit (Rp)',
                    'Profit Margin %'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                      $profit    = number_format(round($val->hpp));
                      $penjualan = $val->total;
                      $hasilprofit = $val->total - $val->hpp; 
                
                 
                      $penjualan = $val->total;
                      $profitmargin = $hasilprofit / $penjualan; 
                

                    $sheet->row($row, array(
                        $no,
                        $val->tanggal,
                        $val->produk,
                        $val->nama,
                        $val->qty,
                        number_format($val->harga),
                        number_format($val->total),
                        number_format($val->hpp),
                        $profit,
                        round($profitmargin,2),
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':J'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:J'.$row, 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:J2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:J3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>50, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20));
                $sheet->getStyle('B4:J'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:J'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }
    public function exelSetoranPenjualan($tmuk_kode, $request)
    {
      // dd($request->start);

        $record = collect(new TransRekapPenjualanTmuk);
              // $utang = collect(new TransRekapPenjualanTmuk);
              // $cash = collect(new TransRekapPenjualanTmuk);
              // $poin = collect(new TransRekapPenjualanTmuk);
                  $records = TransRekapPenjualanTmuk::select(DB::raw('DATE("tanggal") as date, SUM("total") as total_penjualan'))
                              ->groupBy('date')
                              ->where('tmuk_kode', $tmuk_kode);

                  $saldo  = TransJurnal::select(DB::raw('DATE("tanggal") as date, SUM("jumlah") as total'))->groupBy('date')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->where('delete', '0')
                                ->where('posisi', 'D')
                                ->where('deskripsi', 'ilike', 'Top Up Escrow Balance - Setoran Penjualan Tunai');

                  // $records = TransRekapPenjualanTmuk::select(DB::raw('DATE("tanggal") as date, SUM("total") as total_penjualan'))->groupBy('date');

                  if ($tmuk_kode = $tmuk_kode) {
                  $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                  }

                  if($request->start and $request->end){
                  $start = $request->start;
                  $end   = $request->end;
                  $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                  }

                  $record = $records->get();
                  $saldo = $saldo->get();
                  // $tunai = $tunai->get();
                  // $point = $point->get();
        
        $Export = Excel::create('SETORAN PENJUALAN ', function($excel) use ($record, $saldo, $request){
            $excel->setTitle('SETORAN PENJUALAN');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('SETORAN PENJUALAN ', function($sheet) use ($record, $saldo, $request){
                $sheet->row(1, array(
                    'SETORAN PENJUALAN '
                    ));

                $sheet->row(2, array(
                  'Periode : ' . $request->start . ' - ' . $request->end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal',
                    'Penjualan',
                    'Setoran',
                    ));

                $row = 4;
                $no =1;
                foreach ($record->sortBy('date') as $val) {
                        $setoran=0;
                        foreach($saldo as $tn){
                            if($tn->date==$val->date){
                              $setoran=$tn->total;
                            }
                        }

                    $sheet->row($row, array(
                        $no,
                        $val->date,
                        number_format($val->total_penjualan),
                        number_format($setoran),
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:D2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }
}
