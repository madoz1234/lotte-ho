<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Kki;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Coa;


// libraries
use Lotte\Libraries\JurnalLib;
use Carbon\Carbon;

class LaporanKeuanganController extends Controller
{
    protected $link = 'laporan/';

    public $tpl_labarugi;
    public $tpl_neraca;

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan");

        // template neraca laba rugi
        $this->tpl_labarugi = [
            'pendapatan_usaha' => [
                'text'  => 'Pendapatan Usaha',
                'type'  => 'label',
                'child' => [
                    'penjualan_barang_dagang' => [
                        'text'  => 'Penjualan Barang Dagang',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['4.1.0.0'],
                    ],
                    'potongan_penjualan' => [
                        'text'  => 'Potongan Penjualan',
                        'type'  => 'post',
                        'value' => 'negative',
                        'coa'   => ['4.3.0.0'],
                    ]
                ]
            ],
            'total_pendapatan_usaha' => [
                'text'  => 'Total Pendapatan Usaha',
                'type'  => 'post',
                'value' => 'positive', // positive, negative
                'coa'   => ['4.1.0.0', '4.1.1.0', '4.2.0.0', '4.3.0.0'],
            ],
            'hpp' => [
                'text'  => 'Harga Pokok Penjualan',
                'type'  => 'post',
                'value' => 'negative', // positive, negative
                'coa'   => ['5.1.0.0'],
            ],
            'pendapatan_kotor' => [
                'text' => 'Pendapatan Kotor',
                // 'type' => 'dec',
                'type' => 'inc',
                'exp'  => ['total_pendapatan_usaha', 'hpp'],
            ],
            'biaya_pemasaran_umun_adm' => [
                'text' => 'Biaya Pemasaran Umum & Adm',
                'type' => 'label',
                'child'=> [
                    'biaya_pemasaran' => [
                        'text'  => 'Biaya Pemasaran',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.1.1.0', '6.1.1.1', '6.1.1.2', '6.1.1.3', '6.1.1.4'],
                    ],
                ]
            ],
            'total_biaya_pemasaran_umum' => [
                'text' => 'Total Biaya Pemasaran Umum & Adm',
                'type' => 'inc',
                'exp'  => ['biaya_pemasaran_umun_adm'],
            ],
            'beban_umum_administrasi' => [
                'text' => 'Biaya Pemasaran Umum & Adm',
                'type' => 'label',
                'child' => [
                    'biaya_gaji_lembur' => [
                        'text'  => 'Biaya Gaji, Lembur & THR',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.1.1'],
                    ],
                    'biaya_makan_karyawan' => [
                        'text'  => 'Biaya Makan Karyawan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.1.2'],
                    ],
                    'biaya_tunjangan_kesehatan' => [
                        'text'  => 'Biaya Tunjangan Kesehatan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.1.3'],
                    ],
                    'biaya_asuransi' => [
                        'text'  => 'Biaya Asuransi',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.1.4'],
                    ],
                    'biaya_pengiriman' => [
                        'text'  => 'Biaya Pengiriman',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.1'],
                    ],
                    'biaya_air' => [
                        'text'  => 'Biaya Air',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.2'],
                    ],
                    'biaya_listrik' => [
                        'text'  => 'Biaya Listrik',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.3'],
                    ],
                    'biaya_telp_internet' => [
                        'text'  => 'Biaya Telp & Internet',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.4'],
                    ],
                    'biaya_keamanan' => [
                        'text'  => 'Biaya Keamanan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.5'],
                    ],
                    'biaya_lainnya_sumbangan' => [
                        'text'  => 'Biaya Lainnya (sumbangan, dll)',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.6'],
                    ],
                    'plastik_pembungkus' => [
                        'text'  => 'Plastik Pembungkus',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.7'],
                    ],
                    'barang_rusak' => [
                        'text'  => 'Barang Rusak',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.8'],
                    ],
                    'barang_hilang' => [
                        'text'  => 'Barang Hilang',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.9'],
                    ],
                    'biaya_distribusi' => [
                        'text'  => 'Biaya Distribusi (Adm)',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.10'],
                    ],
                    'beban_pemeliharaan' => [
                        'text'  => 'Beban Pemeliharaan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.3.1', '6.2.3.2', '6.2.3.3'],
                    ],
                    'biaya_penyusutan' => [
                        'text'  => 'Biaya Penyusutan',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.3.1.0', '6.3.2.0', '6.3.3.0'],
                    ],
                    'biaya_bensin' => [
                        'text'  => 'Biaya Bensin',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.11'],
                    ],
                    'biaya_perlengkapan_toko' => [
                        'text'  => 'Biaya Perlengkapan Toko',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.12'],
                    ],
                    'biaya_sewa_toko' => [
                        'text'  => 'Biaya Sewa Toko',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.13'],
                    ],
                    'biaya_lain_operasional' => [
                        'text'  => 'Biaya Lain-Lain Operasional',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.14'],
                    ],
                    'biaya_lain_nonoperasional' => [
                        'text'  => 'Biaya Lain-Lain Non-Operasional',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['6.2.2.15'],
                    ],
                ]
            ],
            'total_beban_umum_administrasi' => [
                'text' => 'Total Beban Umum & Administrasi',
                'type' => 'inc',
                'exp'  => ['beban_umum_administrasi'],
            ],
            'laba_usaha' => [
                'text' => 'Laba Usaha',
                'type' => 'inc',
                'exp'  => ['pendapatan_kotor', 'total_biaya_pemasaran_umum', 'total_beban_umum_administrasi'],
            ],
            'pendapatan_beban_lain2' => [
                'text' => 'Pendapatan (Beban) Lain-lain',
                'type' => 'label',
                'child'=> [
                    'selisih_kas_lebih' => [
                        'text'  => 'Selisih Kas Lebih',
                        'type'  => 'post',
                        'value' => 'positive', // positive, negative
                        'coa'   => ['7.1.0.0'],
                    ],
                    'selisih_kas_kurang' => [
                        'text'  => 'Selisih Kas Kurang',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['8.1.0.0'],
                    ],
                    'penghasilan_deviden' => [
                        'text'  => 'Penghasilan Deviden',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['7.2.0.0'],
                    ],
                    'laba_penjualan_aktiva_tetap' => [
                        'text'  => 'Laba Penjualan Aktiva Tetap',
                        'type'  => 'post',
                        'value' => 'positive', // positive, negative
                        'coa'   => ['7.3.0.0'],
                    ],
                    'rugi_penjualan_aktiva_tetap' => [
                        'text'  => 'Rugi Penjualan Aktiva Tetap',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['8.2.0.0'],
                    ],
                    'penghasilan_sewa' => [
                        'text'  => 'Penghasilan Sewa',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['7.4.0.0'],
                    ],
                    'biaya_admin_bank' => [
                        'text'  => 'Biaya Admin Bank',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['8.3.0.0'],
                    ],
                    'penghasilan_lainnya' => [
                        'text'  => 'Penghasilan Lainnya',
                        'type'  => 'post',
                        'value' => 'positive', // positive, negative
                        'coa'   => ['7.5.0.0'],
                    ],
                    'beban_lainnya' => [
                        'text'  => 'Beban Lainnya',
                        'type'  => 'post',
                        'value' => 'negative', // positive, negative
                        'coa'   => ['8.4.0.0'],
                    ],
                ],
            ],
            'total_pendapatan_beban_lain2' => [
                'text' => 'Total Pendapatan (Beban) Lain-lain',
                'type' => 'inc',
                'exp'  => ['pendapatan_beban_lain2']
            ],
            'total_laba_rugi_bersih' => [
                'text' => 'Laba (Rugi) Bersih',
                'type' => 'inc',
                'exp'  => ['laba_usaha', 'total_pendapatan_beban_lain2']
            ],
        ];

        $this->tpl_neraca = [
            'aset' => [
                'text'  => 'ASET',
                'type'  => 'label',
            ],
            'aset_lancar' => [
                'text'  => 'ASET LANCAR',
                'type'  => 'label',
                'child' => [
                    'kas' => [
                        'text'  => 'Kas',
                        'type'  => 'post',
                        'value' => 'positive',
                        // 'coa'   => ['1.1.1.1', '1.1.1.2'],
                        'coa'   => ['1.1.1.1', '7.1.0.0', '8.1.0.0'],
                    ],
                    'kas_hasil_penjualan' => [
                        'text'  => ' - Kas Hasil Penjualan',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.1.1'],
                    ],
                    'kas_kecil' => [
                        'text'  => ' - Kas Kecil',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.1.2'],
                    ],
                    'bank' => [
                        'text'  => 'Bank',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.2.1', '1.1.2.2', '1.1.2.3'],
                    ],
                    'piutang_usaha' => [
                        'text'  => 'Piutang Usaha',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.3.1'],
                    ],
                    'persediaan_barang_dagang' => [
                        'text'  => 'Persediaan Barang Dagang',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.4.0'],
                    ],
                    'deposit' => [
                        'text'  => 'Deposit',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.6.1'],
                    ],
                    'aktiva_lancar_lainnya' => [
                        'text'  => 'Aktiva Lancar Lainnya',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.1.7.0'],
                    ],
                ]
            ],
            'jml_aset_lancar' => [
                'text' => 'Jumlah Aset Lancar',
                // 'type' => 'inc',
                // 'exp'  => ['aset_lancar'],
                'type'  => 'post',
                'value' => 'positive',
                'coa'   => ['1.1.1.1', '7.1.0.0', '8.1.0.0', '1.1.2.1', '1.1.2.2', '1.1.2.3', '1.1.3.1', '1.1.4.0', '1.1.6.1', '1.1.7.0'],
            ],
            'aset_tidak_lancar' => [
                'text'  => 'ASET TIDAK LANCAR',
                'type'  => 'label',
                'child' => [
                    'tanah' => [
                        'text'  => 'Tanah',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.2.1.0'],
                    ],
                    'gedung' => [
                        'text'  => 'Gedung',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.2.2.0'],
                    ],
                    'kendaraan' => [
                        'text'  => 'Kendaraan',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.2.4.0'],
                    ],
                    'peralatan_toko' => [
                        'text'  => 'Peralatan Toko',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['1.2.6.0'],
                        // 'coa'   => ['1.2.6.0', '1.2.7.0'],
                    ],
                    'akumulasi_penyusutan' => [
                        'text'  => 'Akumulasi Penyusutan',
                        'type'  => 'post',
                        'value' => 'negative',
                        'coa'   => ['1.2.3.0', '1.2.5.0', '1.2.7.0'],
                    ]
                ]
            ],
            'jml_aset_tidak_lancar' => [
                'text' => 'Jumlah Tidak Aset Lancar',
                'type' => 'inc',
                'exp'  => ['aset_tidak_lancar'],
            ],
            'jml_aset' => [
                'text' => 'Jumlah Aset',
                'type' => 'inc',
                'exp'  => ['jml_aset_lancar', 'jml_aset_tidak_lancar'],
            ],
            'liabilitas_dan_ekuitas' => [
                'text' => 'LIABILITAS DAN EKUITAS',
                'type' => 'label',
            ],
            'liabilitas_jangka_pendek' => [
                'text' => 'LIABILITAS JANGKA PENDEK',
                'type' => 'label',
                'child'=> [
                    'utang_usaha' => [
                        'text'  => 'Utang Usaha',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['2.1.1.0'],
                    ],
                    'hutang_jangka_panjang_jatuh_tempo' => [
                        'text'  => 'Utang Jangka Panjang yang Jatuh Tempo dalam Waktu Setahun',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['2.1.1.1', '2.1.2.1', '2.1.5.1'],
                    ],
                ]
            ],
            'jml_liabilitas_jangka_pendek' => [
                'text' => 'Jumlah Liabilitas Jangka Pendek',
                'type' => 'inc',
                'exp'  => ['liabilitas_jangka_pendek'],
            ],
            'liabilitas_jangka_panjang' => [
                'text' => 'LIABILITAS JANGKA PANJANG',
                'type' => 'label',
                'child'=> [
                    'hutang_jangka_panjang' => [
                        'text'  => 'Utang Jangka Panjang',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['2.2.1.0', '2.2.2.0', '2.2.3.0'],
                    ],
                ]
            ],
            'jml_liabilitas_jangka_panjang' => [
                'text' => 'Jumlah Liabilitas Jangka Panjang',
                'type' => 'inc',
                'exp'  => ['liabilitas_jangka_panjang'],
            ],
            'jml_liabilitas' => [
                'text' => 'Jumlah Liabilitas',
                'type' => 'inc',
                'exp'  => ['jml_liabilitas_jangka_pendek', 'jml_liabilitas_jangka_panjang'],
            ],
            // ==tambahan ,baru tamplate
            'ekuitas' => [
                'text' => 'EKUITAS',
                'type' => 'label',
                'child'=> [
                    'modal_dasar' => [
                        'text'  => 'Modal Dasar',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['3.1.1.0'],
                    ],
                    'tambahan_modal_disetor' => [
                        'text'  => 'Tambahan Modal Disetor',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['3.1.2.0'],
                    ],
                    'opening_balance_equity' => [
                        'text'  => 'Opening Balance Equity',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['3.2.0.0'],
                    ],
                    'deviden' => [
                        'text'  => 'Deviden',
                        'type'  => 'post',
                        'value' => 'positive',
                        'coa'   => ['3.3.0.0'],
                    ],
                    'laba_ditahan' => [
                        'text'  => 'Laba Ditahan',
                        'type'  => 'post',
                        // 'value' => 'positive',
                        'coa'   => ['3.4.0.0'],
                    ],
                    'laba_bulan_ini' => [
                        'text'  => 'Laba Bulan Ini',
                        'type'  => 'ref',
                        'source' => 'src_laba',
                        'exp'   => 'total_laba_rugi_bersih',
                    ],
                ]
            ],
            'jumlah_ekuitas' => [
                'text' => 'Jumlah Ekuitas',
                'type' => 'inc',
                'exp'  => ['ekuitas']
            ],
            'jumlah_liabilitas_dan_ekuitas' => [
                'text' => 'Jumlah Liabilitas Dan Ekuitas',
                'type' => 'inc',
                'exp'  => ['jml_liabilitas', 'jumlah_ekuitas'],
            ],
            // ==
        ];
    }

    public function grid(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start  = date('Y-m-d');
        $end    = date('Y-m-d');

        $tmuk_kode = $tmuk->kode;
        $tgl_opening_tmuk = $tmuk->created_at;
        $tmuk_id = $tmuk->id;
        $record = '';
        // $data   = [];
        if($request->start and $request->end){
            $start = Carbon::createFromFormat('m/Y', $request->start)->startOfMonth();
            $end   = Carbon::createFromFormat('m/Y', $request->end)->endOfMonth();

            $diff = Carbon::createFromFormat('m/Y', $request->start)->diffInMonths(Carbon::createFromFormat('m/Y', $request->end));
        }

        switch ($request->type) {
            case 'fokus-keuangan':
            // performance TMUK
                    if($request->start and $request->end){
                        $kki = Kki::select('*')->where('tmuk_id', $tmuk_id)->where('status', 1)->get();

                        $jurnal = TransJurnal::where('tmuk_kode', $tmuk_kode)
                                    ->where('delete', 0)
                                    ->select('*');

                        $date = date('Y-m-d');
                        $struk = TransRekapPenjualanTmuk::select('*')->where('tmuk_kode', $tmuk_kode)->whereBetween('tanggal', [$date.' 00:00:00', $date.' 23:59:59'])->get();
                        $struk_akhir = $struk->count('jual_kode');

                         $data = [
                            'records'       => $kki,
                            'start'         => $start,
                            'end'           => $end,
                            'tmuk_kode'     => $tmuk_kode,
                            'diff'          => $diff,
                            'dayStart'      => Carbon::createFromFormat('m/Y', $request->start)->startOfMonth(),
                            'jurnal'        => $jurnal,
                            'struk_akhir'   => $struk_akhir,
                        ];
                    }
                break;
            case 'laba-rugi-perbadingan-anggaran':
                    
                break;
            case 'laba-rugi':
                if($request->start and $request->end){
                    $record = JurnalLib::generateLaba($tmuk_kode, $start, $end, $this->tpl_labarugi);
                    // dd($record);

                    $data = [
                    'record' => $record,
                    'start'  => $start,
                    'end'    => $end,
                    ];
                }
                break;

            case 'pengeluaran-tahun':
                if($request->start and $request->end){

                    $ends = $end->format('Y-m-d');
                    $starts = $end->subMonths(11)->startOfMonth()->format('Y-m-d');
                    
                    
                    $from           = \Carbon\Carbon::createFromFormat('Y-m-d', $starts);
                    $to             = \Carbon\Carbon::createFromFormat('Y-m-d', $ends);
                    $diff_in_months = $to->diffInMonths($from);
                    $header         = [];
                    $nilai          = [];
                    $array          = [];
                    for($i=0; $i<=$diff_in_months; $i++){
                        $header[] = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ));
                        $nilai[date('Y-m', strtotime($from->format('Ymd') . ' +'.$i.'month'))]  = "0.00";
                    }
                    $tmk = [$tmuk_kode];
                    $result    = Coa::getDataCoaPertahun([
                        'tmuk' => $tmk,
                        'from' => $from->format('Y-m') . '-01 00:00:00',
                        'to'   => $to->format('Y-m') . '-'.$to->endOfMonth()->format('d').' 23:59:59',
                    ]);


                    $result = $result->get();
                    $temp      = [];
                    $temp_rest = [];
                    
                    foreach ($result as $rc) {
                        $temp[$rc->kode_coa.'-'.$rc->nama_coa][$rc->tmuk_kode.'-'.$rc->nama_tmuk][$rc->tahun.'-'.(strlen($rc->bulan)==1?'0'.$rc->bulan:$rc->bulan)] = $rc->jumlah;
                    }

                   
                    $start = Carbon::createFromFormat('m/Y',$request->start)->format('Y-m-d');
                    $end = Carbon::createFromFormat('m/Y',$request->end)->format('Y-m-d');

                    $data = [
                    'records'    => $temp,
                    'header'     => $header,
                    'nilai_temp' => $nilai,
                    'start'  => $start,
                    'end'    => $end,
                    ];
                }
                break;
            case 'laporan-laba-ditahan':
                    if($request->start and $request->end){
                        $records = TransJurnal::select('*')
                                    ->whereIn('coa_kode', ['3.4.0.0'])
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->where('delete', 0)
                                    ->whereBetween('tanggal', array($start, $end))
                                    ->orderBy('tanggal');
                        
                        $data = [
                            'detail' => $records->get(),
                            'start'  => $start,
                            'end'    => $end,
                        ];
                    }
                break;
            case 'laporan-piutang':
                    if($request->start and $request->end){
                        $records = TransJurnal::select('*')
                                    ->whereIn('coa_kode', ['2.1.1.0'])
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->where('delete', 0)
                                    ->whereBetween('tanggal', array($start, $end));

                        $data = [
                            'detail' => $records->get(),
                            'start'  => $start,
                            'end'    => $end,
                        ];
                    }
                break;
            case 'neraca-perbandingan-anggaran':
                    
                break;
            case 'neraca':
                if($request->start and $request->end){
                    $labarugi = JurnalLib::generateLaba($tmuk_kode, $start, $end, $this->tpl_labarugi);
                    $record = JurnalLib::generate($tmuk_kode, $start, $end, $this->tpl_neraca, ['src_laba' => $labarugi]);

                    $data = [
                    'record' => $record,
                    'start'  => $start,
                    'end'    => $end,
                    ];
                }
                break;
            case 'realisasi-rab':
                if($request->start and $request->end){
                    $saldo  = TransJurnal::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("jumlah") as total_penjualan, COUNT("deskripsi") as struk  '))->groupBy('year', 'month')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->where('deskripsi', 'ilike', '%Modal%')
                                ->whereBetween(\DB::raw('tanggal'), array($start, $end))
                                ->where('coa_kode', '1.1.2.1')
                                ->where('delete', '0')
                                ->get();
                    // dd($saldo);
                    $records = Kki::select('*')->where('tmuk_id', $tmuk_id)->where('status', 1);
                    $rcd     = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("total") as total_penjualan, COUNT("jual_kode") as struk  '))->groupBy('year', 'month')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->whereBetween(\DB::raw('tanggal'), array($start, $end))
                                ->get();
                    
                    $recordakuisisi = TransPyr::with('detailPyr')->where('status', 2)
                                     ->where('tmuk_kode', $tmuk_kode)
                                     ->where('nomor_pyr', 'like', '%OPENING%')
                                     ->first();
                    
                    //get for initial stock
                    $record_po = TransPo::with('detail')->where('status', 2)
                                     ->where('tmuk_kode', $tmuk_kode)
                                     ->where('initial_stock', 1)
                                     ->first();
                    if (isset($record_po)) {
                        $reduce = TransReduceEscrow::where('po_nomor', $record_po->nomor)->first();
                    }

                    $data = [
                        'stock_awal_po' => isset($reduce) ? $reduce->saldo_dipotong : 0,
                        'saldo'         => $saldo,
                        'detail'        => $records->get(),
                        'akuisisi'      => $recordakuisisi,
                        'penjualan'     => $rcd,
                        'start'         => $start,
                        'end'           => $end,
                    ];
                }
                break;
        }

        return view('modules.laporan.laporan.lists.keuangan.'.$request->type, $data);
    }

    //PEMBELIAN
    public function printLaporanKeuangan(Request $request){
        $data           = $request->all();
        $tmuk           = Tmuk::find($data['tmuk_kode']);
        
        $start          = date('Y-m-d');
        $end            = date('Y-m-d');
        $tmuk_kode      = $tmuk->kode;
        $tmuk_id        = $tmuk->id;
        $record         = '';
        $laporan        = '';
        $recordakuisisi = '';
        $saldo          = '';
        $rcd            = '';
        $kki            = '';
        $jurnal_perform = '';
        $saldo          = '';
        $dayStart       = '';
        $diff           = '';
        $struk_akhir    = '';
        
        if($request->start and $request->end){
            $start = Carbon::createFromFormat('m/Y', $request->start)->startOfMonth();
            $end   = Carbon::createFromFormat('m/Y', $request->end)->endOfMonth();

            $diff = Carbon::createFromFormat('m/Y', $request->start)->diffInMonths(Carbon::createFromFormat('m/Y', $request->end));
        }
        $data   = [];
        $temp   = [];

        switch ($request->laporan) {
            case 'pengeluaran-tahun':
                if($request->start and $request->end){
                    $start = Carbon::createFromFormat('m/Y', $request->start)->startOfMonth();
                    $end   = Carbon::createFromFormat('m/Y', $request->end)->endOfMonth();

                    $ends = $end->format('Y-m-d');
                    $starts = $end->subMonths(11)->startOfMonth()->format('Y-m-d');
                    
                    
                    $from           = \Carbon\Carbon::createFromFormat('Y-m-d', $starts);
                    $to             = \Carbon\Carbon::createFromFormat('Y-m-d', $ends);
                    $diff_in_months = $to->diffInMonths($from);
                    $header         = [];
                    $nilai          = [];
                    $array          = [];
                    for($i=0; $i<=$diff_in_months; $i++){
                        $header[] = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ));
                        $nilai[date('Y-m', strtotime($from->format('Ymd') . ' +'.$i.'month'))]  = "0.00";
                    }
                    $tmk = [$tmuk_kode];
                    $result    = Coa::getDataCoaPertahun([
                        'tmuk' => $tmk,
                        'from' => $from->format('Y-m') . '-01 00:00:00',
                        'to'   => $to->format('Y-m') . '-'.$to->endOfMonth()->format('d').' 23:59:59',
                    ]);


                    $result = $result->get();
                    $temp      = [];
                    $temp_rest = [];
                    
                    foreach ($result as $rc) {
                        $temp[$rc->kode_coa.'-'.$rc->nama_coa][$rc->tmuk_kode.'-'.$rc->nama_tmuk][$rc->tahun.'-'.(strlen($rc->bulan)==1?'0'.$rc->bulan:$rc->bulan)] = $rc->jumlah;
                    }

                   
                    $start = Carbon::createFromFormat('m/Y',$request->start)->format('Y-m-d');
                    $end = Carbon::createFromFormat('m/Y',$request->end)->format('Y-m-d');

                    $data = [
                        'start'             => Carbon::parse($start)->format('m/Y'),
                        'end'               => Carbon::parse($end)->format('m/Y'),
                        'tmuk_kode'         => $tmuk_kode,
                        'tmuk'              => $tmuk->nama,
                        'lsi'               => $tmuk->lsi->nama,
                        'tahun_fiskal'      => TahunFiskal::getTahunFiskal(),
                        'region'            => $tmuk->lsi->region->area,
                        'records'           => $temp,
                        'header'            => $header,
                        'nilai_temp'        => $nilai,
                        'recordakuisisi'    => $recordakuisisi,
                        'saldo'             => $saldo,
                        'penjualan'         => $rcd,
                        'label_file'        => $laporan.' '.$tmuk_kode.' '.$start.' s/d '.$end,
                        'dayStart'          => Carbon::createFromFormat('m/Y', $request->start)->startOfMonth(),
                        'periode'           => $start.' s/d '.$end,
                        'kki'               => $kki,
                        'jurnal_perform'    => $jurnal_perform,
                        'diff'              => $diff,
                    ];


                    $content = view('report.laporan.keuangan.'.$request->laporan, $data);

                    // return $content;
                    HTML2PDF($content, $data);
                }
                break;
            case 'fokus-keuangan':
                    $kki = Kki::select('*')->where('tmuk_id', $tmuk_id)->where('status', 1)->get();

                    $jurnal_perform = TransJurnal::where('tmuk_kode', $tmuk_kode)
                                    ->where('delete', 0)
                                    ->select('*');
                    $date = date('Y-m-d');
                    $struk = TransRekapPenjualanTmuk::select('*')->where('tmuk_kode', $tmuk_kode)->whereBetween('tanggal', [$date.' 00:00:00', $date.' 23:59:59'])->get();
                    $struk_akhir = $struk->count('jual_kode');
                                    
                    $laporan = 'LAPORAN FOKUS KEUANGAN';
                break;
            case 'laba-rugi-perbadingan-anggaran':
                    $data = [
                        'records'   => $temp,
                    ];

                    $laporan = 'LAPORAN LABARUGI PERBANDINGAN ANGGARAN';
                break;
            case 'laba-rugi':
                    $record = JurnalLib::generateLaba($tmuk_kode, $start, $end, $this->tpl_labarugi);

                    $laporan = 'LAPORAN LABA RUGI';
                break;
            case 'laporan-laba-ditahan':
                    $records = TransJurnal::select('*')
                        ->whereIn('coa_kode', ['3.4.0.0'])
                        ->where('tmuk_kode', $tmuk_kode)
                        ->where('delete', 0)
                        ->whereBetween('tanggal', array($start, $end))
                        ->orderBy('tanggal');

                    $record = $records->get();

                    $laporan = 'LAPORAN LABA DITAHAN';
                break;
            case 'laporan-piutang':
                    $records = TransJurnal::select('*')
                                ->whereIn('coa_kode', ['2.1.1.0'])
                                ->where('tmuk_kode', $tmuk_kode)
                                ->where('delete', 0)
                                ->whereBetween('tanggal', array($start, $end));
                    
                     $record = $records->get();

                    $laporan = 'LAPORAN PIUTANG';
                break;
            case 'neraca-perbandingan-anggaran':
                    $data = [
                        'records'   => $temp,
                    ];

                    $laporan = 'LAPORAN NERACA PERBANDINGAN ANGGARAN';
                break;
            case 'neraca':
                    $labarugi = JurnalLib::generateLaba($tmuk_kode, $start, $end, $this->tpl_labarugi);
                    $record = JurnalLib::generate($tmuk_kode, $start, $end, $this->tpl_neraca, ['src_laba' => $labarugi]);

                    $laporan = 'LAPORAN NERACA';
                break;
            case 'realisasi-rab':
                    $record = Kki::select('*')->where('tmuk_id', $tmuk_id)->where('status', 1)->get();
                    $saldo  = TransJurnal::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("jumlah") as total_penjualan, COUNT("deskripsi") as struk  '))->groupBy('year', 'month')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->where('deskripsi', 'ilike', '%Modal%')
                                ->whereBetween(\DB::raw('tanggal'), array($start, $end))
                                ->where('coa_kode', '1.1.2.1')
                                ->where('delete', '0')
                                ->get();
                    $rcd = TransRekapPenjualanTmuk::select(DB::raw('extract(YEAR from tanggal) as year, extract(MONTH from tanggal) as month, SUM("total") as total_penjualan, COUNT("jual_kode") as struk  '))->groupBy('year', 'month')
                                ->where('tmuk_kode', $tmuk_kode)
                                ->whereBetween(\DB::raw('tanggal'), array($start, $end))
                                ->get();
                    $recordakuisisi = TransPyr::with('detailPyr')->where('status', 2)
                                     ->where('tmuk_kode', $tmuk_kode)
                                     ->where('nomor_pyr', 'like', '%OPENING%')
                                     ->first();
                    $laporan = 'LAPORAN REALISASI RAB';
                break;
            default:

                break;
        }

        $data = [
            'start'             => Carbon::parse($start)->format('d/m/Y'),
            'end'               => Carbon::parse($end)->format('d/m/Y'),
            'tmuk_kode'         => $tmuk_kode,
            'tmuk'              => $tmuk->nama,
            'lsi'               => $tmuk->lsi->nama,
            'tahun_fiskal'      => TahunFiskal::getTahunFiskal(),
            'region'            => $tmuk->lsi->region->area,
            'record'            => $record,
            'recordakuisisi'    => $recordakuisisi,
            'saldo'             => $saldo,
            'penjualan'         => $rcd,
            'label_file'        => $laporan.' '.$tmuk_kode.' '.$start.' s/d '.$end,
            'dayStart'          => Carbon::createFromFormat('m/Y', $request->start)->startOfMonth(),
            'periode'           => $start.' s/d '.$end,
            'kki'               => $kki,
            'jurnal_perform'    => $jurnal_perform,
            'diff'              => $diff,
            'struk_akhir'       => $struk_akhir,
        ];


        $content = view('report.laporan.keuangan.'.$request->laporan, $data);

        // return $content;
        HTML2PDF($content, $data);
    }
    // END LAPORAN PEMBELIAN

    // PRINT LAPORAN EXEL
    // PEMBELIAN
    public function exelLaporanKeuangan(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start  = date('Y-m-d');
        $end    = date('Y-m-d');
        $tmuk_kode = $tmuk->kode;
        $tmuk_id = $tmuk->id;
        $record = '';
        $laporan = '';
        
        if($request->start and $request->end){
            $start   = Carbon::createFromFormat('m/Y', $request->start)->startOfMonth()->format('Y-m-d');
            $end     = Carbon::createFromFormat('m/Y', $request->end)->endOfMonth()->format('Y-m-d');
            $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
        }
        $data   = [];
        $temp   = [];

        switch ($request->laporan) {
            case 'neraca':
                    
                break;
            case 'laba-rugi':
                    
                break;
            case 'laporan-laba-ditahan':
                $this->exelLaporanLabaDitahan($tmuk_kode, $tanggal);
                break;
            case 'laporan-piutang':
                $this->exelLaporanPiutang($tmuk_kode, $tanggal);
                break;
            case 'pengeluaran-tahun':
                $this->exelPengeluaranTahunan($tmuk_kode, $tanggal, $start, $end);
                break;
            case 'realisasi-rab':
                    
                break;
            default:

                break;
        }
    }

    public function exelPoBtdk(){
        $data = [
            'title'     => ['PURCHASE ORDER (STDK)'],
            'header'    => [ 
                    'NO PO',
                    'LSI',
                    'TMUK',
                    'Tanggal Pemesanan',
                    'PO Type',
                    'Expected Delivery Dat',
                    'Kode Produk',
                    'Barcode',
                    'Nama Produk',
                    'Qty/UOM',
                    'UOM',
                    'Order Qty',
                    'Harga Satuan',
                    'Total Harga'
                ],
        ];

        $reports = \Excel::create($data['title'][0], function($excel) use ($data){
            $excel->setTitle($data['title'][0]);
            $excel->setCreator('ho.dev')->setCompany('ho.dev');
            $excel->setDescription('Export');
            $excel->sheet($data['title'][0], function($sheet) use ($data){
                // SEET TITTLE
                $sheet->mergeCells('A1:N1');
                $sheet->mergeCells('A2:N2');
                $sheet->cells('A1:N2', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });

                $row =1;
                foreach ($data['title'] as $key=>$title) {
                    $sheet->row($row,array($title));
                    $row++;
                }


                $sheet->cells('A4:N4', function($cells){
                    $cells->setFontSize(12);
                    $cells->setBackground("#80bfff");
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                
                $sheet->row(4, $data['header']);
                $row = 5;

                // foreach ($data['list_data'] as $row_data) {
                //     $sheet->row($row, $row_data);
                //     $row++;
                // }
                $row = $row+1;
                $sheet->mergeCells('B'.$row.':N'.$row);
                $sheet->cells('A'.$row.':N'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                // $sheet->row($row, $data['total']);

                $range = "A4:N".$row;
                $sheet->setBorder($range, 'thin');

            });
        });

        $reports->export('xls');
    }

    public function exelLaporanLabaDitahan($tmuk_kode, $tanggal){
        $records = TransJurnal::select('*')
                                    ->whereIn('coa_kode', ['3.4.0.0'])
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->where('delete', 0)
                                    ->whereBetween('tanggal', $tanggal)
                                    ->orderBy('tanggal');
        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Laporan Laba Ditahan ', function($excel) use ($record, $start, $end){
            $excel->setTitle('Laporan Laba Ditahan');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Laporan Laba Ditahan ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Laporan Laba Ditahan'
                    ));
                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal',
                    'Deskripsi',
                    'Saldo (Rp)'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        Carbon::parse($val->tanggal)->format('d/m/Y'),
                        ucfirst($val->deskripsi),
                        $val->jumlah
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.($row-1), 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:D2', function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>40, 'D'=>20));
                $sheet->getStyle('B4:D'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls');
    }

    public function exelLaporanPiutang($tmuk_kode, $tanggal){
        $records = TransJurnal::select('*')
                                    ->whereIn('coa_kode', ['2.1.1.0'])
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->where('delete', 0)
                                    ->whereBetween('tanggal', $tanggal);
        $record = $records->get();
        
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('Laporan Piutang ', function($excel) use ($record, $start, $end){
            $excel->setTitle('Laporan Piutang');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Laporan Piutang ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'Laporan Piutang'
                    ));
                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal',
                    'Deskripsi',
                    'Debit',
                    'Kredit'
                    ));

                $row = 4;
                $no =1;
                $total_debit = 0;
                $total_kredit = 0;
                foreach ($record as $val) {
                    $total_debit += $val->posisi=='D' ? $val->jumlah : '0';
                    $total_kredit += $val->posisi=='K' ? $val->jumlah : '0';
                    // $total_debit=0;
                        // if($val->posisi=='D'){
                        //   $total_debit=$total_debit += $val->jumlah;
                        // }

                    // $total_kredit=0;
                        // if($val->posisi=='K'){
                        //   $total_kredit=$total_kredit += $val->jumlah;
                        // }
                    $sheet->row($row, array(
                        $no,
                        Carbon::parse($val->tanggal)->format('d/m/Y'),
                        ucfirst($val->deskripsi),
                        number_format($val->posisi=='D' ? $val->jumlah : '0'),
                        number_format($val->posisi=='K' ? $val->jumlah : '0'),
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->row($row, array(
                    '',
                    '',
                    'Total',
                    number_format($total_debit),
                    number_format($total_kredit),
                ));
                $sheet->cells('E4'.':E'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:E'.($row), 'thin');
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:E2', function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:E3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>40, 'D'=>20, 'E'=>20));
                $sheet->getStyle('B4:E'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:E'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls');
    }

    public function exelPengeluaranTahunan($tmuk_kode, $tanggal, $start, $end)
    {

        $from           = \Carbon\Carbon::createFromFormat('Y-m-d', $start);
        $to             = \Carbon\Carbon::createFromFormat('Y-m-d', $end);
        $diff_in_months = $to->diffInMonths($from);
        $header         = [];
        $nilai          = [];
        $array          = [];
        for($i=0; $i<=$diff_in_months; $i++){
            $header[] = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ));
            $nilai[date('Y-m', strtotime($from->format('Ymd') . ' +'.$i.'month'))]  = "0.00";
        }
        $tmk = [$tmuk_kode];
        $result    = Coa::getDataCoaPertahun([
            'tmuk' => $tmk,
            'from' => $from->format('Y-m') . '-01 00:00:00',
            'to'   => $to->format('Y-m') . '-'.$to->endOfMonth()->format('d').' 23:59:59',
        ]);

        // $result = $result->get();
        // $temp      = [];
        // $temp_rest = [];
        
        // foreach ($result as $rc) {
        //     $temp[$rc->kode_coa.'-'.$rc->nama_coa][$rc->tmuk_kode.'-'.$rc->nama_tmuk][$rc->tahun.'-'.(strlen($rc->bulan)==1?'0'.$rc->bulan:$rc->bulan)] = $rc->jumlah;
        // }
        // dd($temp);


        $coa = Coa::whereHas('jurnal', function ($jurnal) use ($tmuk_kode) {
                $jurnal->where('tmuk_kode', $tmuk_kode);
        })->orderBy('kode')->get();

        foreach($coa as $c)
        {
            for($i=0; $i<=$diff_in_months; $i++){
                $temp[$c->kode.'-'.$c->nama][$tmuk_kode][$from->copy()->addMonths($i)->format('Y').'-'.$from->copy()->addMonths($i)->format('m')] = TransJurnal::getJumlahByCoa($c->kode, $tmuk_kode, $from->copy()->addMonths($i)) == NULL ? 0 : TransJurnal::getJumlahByCoa($c->kode, $tmuk_kode, $from->copy()->addMonths($i));
            }
        }
        // dd($temp);
        
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('PENGELUARAN TAHUN ', function($excel) use ($header, $temp, $nilai, $start, $end){
            $excel->setTitle('PENGELUARAN TAHUN');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('PENGELUARAN TAHUN ', function($sheet) use ($header, $temp, $nilai, $start, $end){
                $sheet->row(1, array(
                    'PENGELUARAN TAHUN'
                    ));
                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));

                array_unshift($header, 'No', 'Nama Akun');
                $sheet->row(3, $header);

                $row = 4;
                $no =1;
                foreach ($temp as $coa => $datacoa) {
                    foreach ($datacoa as $tmuk => $datatmuk) {
                        $val = [];
                        foreach ($datatmuk as $thbl => $nilai) {
                            $val[] = $nilai;
                        }
                        array_unshift($val, $no, $coa);
                        $sheet->row($row, $val);
                    }
                    $row++;$no++;
                }
                $sheet->cells('E4'.':N'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:N'.($row-1), 'thin');
                $sheet->mergeCells('A1:N1');
                $sheet->mergeCells('A2:N2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:N2', function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:N3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>40,'C'=>20, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20, 'K'=>20, 'L'=>20, 'M'=>20, 'N'=>20));
                $sheet->getStyle('B4:N'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:N'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls');
    }
}
