<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransPoDetail;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransSO;
use Lotte\Models\Trans\TransSODetail;
use Lotte\Models\Trans\TransStock;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\HargaTmuk;
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Master\ProdukTmukDetail;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\GRDetail;
use Lotte\Models\Master\SODetail;
use Lotte\Models\Master\RRDetail;
use Lotte\Models\Master\ADDetail;
use Lotte\Models\Master\PYRDetail;
use Lotte\Models\Master\PenjualanDetail;
use Illuminate\Support\Collection;

use Carbon\Carbon;

class PersediaanController extends Controller
{
    protected $link = 'laporan/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan");
        
    }
    public function grid(Request $request)
    {
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);
        $start = '';
        $end   = '';
        $tipe   = '';
        $tmuk_kode = $tmuk->kode;
        $nama_tmuk = $tmuk->nama;

        if($request->start and $request->end){
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
        }

        switch ($request->type) {
            case 'nilai-persediaan':
                if($request->start){
					$start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    // $records->whereBetween(\DB::raw('date(trans_rekap_penjualan.tanggal)'),array($start,$end));
                    $records = ProdukTmuk::with(['detail' => function($q) use ($start){
                    							$q->where('date', $start);
                							}], 'produk', 'produksetting')
	                    					->where('tmuk_kode', $tmuk_kode)
	                                		->orderBy('updated_at', 'ASC')->get();
                    // dd($records);
                    $data = [
                        'data' => $records,
                        'start'  => Carbon::createFromFormat('d/m/Y', $request->start)->format('Y/m/d'),
                    ];
                }
                break;
            case 'pergerakan-persediaan':
                if($request->start and $request->end){
                	$cek = [];
                	$tmuk = [];
                	$start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                	$end  = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                	$tanggal = [$start,$end];

                    $kode_produk = $request->kode_produk;
                	
                	$records = ProdukTmuk::where('produk_kode', $kode_produk)
                						->with(['detail2' => function($q) use ($start, $end){
                    						$q->whereBetween('date', [$start, $end])
                    						  ->orderBy('date');
                						}, 'produk'])
                                		->where('tmuk_kode', $tmuk_kode)
                                        ->get();

                    
                    $date = date_create($start);
					date_sub($date, date_interval_create_from_date_string('1 days'));
					$yesterday =  date_format($date, 'Y-m-d');
                    foreach ($records->first()->detail2 as $key => $value) {
                    	$ad = ADDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
                    					->where('date', $value->date)
                    					->first();
                    	$pyr = PYRDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
				                    	->where('date', $value->date)
				                    	->first();
                    	$jual = PenjualanDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
				                    	->where('date', $value->date)
				                    	->first();
                    	$rr = RRDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
				                    	->where('date', $value->date)
				                    	->first();
                    	$so = SODetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
				                    	->where('date', $value->date)
				                    	->first();
                    	$gr = GRDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
				                    	->where('date', $value->date)
				                    	->first();

                    	if($ad != null){
                    		$data_ad = $ad->qty;	
                    	}else{
                    		$data_ad = 0;
                    	}

                    	if($pyr != null){
                    		$data_pyr = $pyr->qty;	
                    	}else{
                    		$data_pyr = 0;
                    	}

                    	if($jual != null){
                    		$data_jual = $jual->qty;	
                    	}else{
                    		$data_jual = 0;
                    	}

                    	if($rr != null){
                    		$data_rr = $rr->qty;	
                    	}else{
                    		$data_rr = 0;
                    	}
                    	if($so != null){
                    		$data_so = $so->qty;	
                    	}else{
                    		$data_so = 0;
                    	}

                    	if($gr != null){
                    		$data_gr = $gr->qty;	
                    	}else{
                    		$data_gr = 0;
                    	}

						$hasil[] = [
	                        'id' => $value->produk_tmuk_kode,
	                        'date' => $value->date,
	                        'qty' => $value->qty,
	                        'hpp' => $value->hpp,
	                        'nilai' => $value->nilai_persediaan,
	                        'ad' => $data_ad,
	                        'pyr' => $data_pyr,
	                        'jual' => $data_jual,
	                        'rr' => $data_rr,
	                        'so' => $data_so,
	                        'gr' => $data_gr,
	                    ];
                    }
                    $prod = Produk::where('kode', $kode_produk)
                                       ->first();

                    if($prod != null){
                    	$kategori = $prod->l1_nm;
                    	$nama =$prod->nama;
                    }else{
                    	$kategori = '-';
                    	$nama = '-';
                    }

                    function tgl_indonesia($date){
                    	$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                    	$tahun     = substr($date, 0, 4);
                    	$bulan     = substr($date, 5, 2);
                    	$tgl    = substr($date, 8, 2);
                    	$waktu    = substr($date,11, 5);
                    	$hari    = date("w", strtotime($date));
                    	$result = $tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
                    	return $result;
                    }

	                    $tmuk[] = [
	                    	'nama' => $nama_tmuk,
	                    	'tanggal' => $tanggal,
	                    	'kategori' => $kategori,
	                    	'nama_produk' => $nama,
	                    	'kode' => $kode_produk,
	                    	'awal' => tgl_indonesia($start),
	                    	'akhir' => tgl_indonesia($end),
	                    ];

                    $begining = ProdukTmukDetail::where('date','=', $yesterday)
                    							->where('produk_tmuk_kode','=', $value->produk_tmuk_kode)
	                                			->first();
                    if(count($begining) > 0){
                    	$beg[] = [
	                        'qty' => isset($begining) ? $begining->qty : '0',
	                        'hpp' => isset($begining) ? $begining->hpp : '0',
	                        'nilai' => isset($begining) ? $begining->nilai_persediaan : '0',
	                    ];
                    }else{
                    	$beg[] = [
                            'qty'   => $records->first()->detail2->first()->qty,
                            'hpp'   => $records->first()->detail2->first()->hpp,
                            'nilai' => $records->first()->detail2->first()->nilai_persediaan,
	                    ];
                    }
                    
                    $data = [
                        'hasil'       => $hasil,
                        'begining'    => $beg,
                        'tmuk'        => $tmuk,
                        'hitung'      => count($records->first()->detail2),
                        'start'       => Carbon::createFromFormat('d/m/Y', $request->start)->format('Y/m/d'),
                        'end'         => Carbon::createFromFormat('d/m/Y', $request->end)->format('Y/m/d'),
                        'kode_produk' => $kode_produk,
                        'kode_tmuk'   => $tmuk_kode,
                    ];
                }
                break;
            case 'stok-opname':
                if($request->start and $request->tipe){
                	$start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $records = TransSO::with('tmuk.lsi','detail.produk','detail.produksetting','detail.produksetting')
			                            ->where('tgl_so', $start)
			                            ->where('tmuk_kode', $tmuk_kode)
			                            ->where('type', $request->tipe)
			                            ->get();

                    $data = [
                        'data' => $records,
                        'tipe' => $request->tipe,
                        'start'  => Carbon::createFromFormat('d/m/Y', $request->start)->format('Y/m/d'),
                    ];
                }
                break;
            case 'hpp-map':
                if($request->start){
					$start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    // $records = HargaTmuk::with(['produk.produksetting', 'po_detail'])
                    //             ->whereBetween('created_at', array($start.' 00:00:00', $start.' 23:59:59'))
                    //             ->where('tmuk_kode', $tmuk_kode);

                    $records = ProdukTmuk::with(['detail' => function($q) use ($start){
                    							$q->where('date', $start);
                							}], 'produk', 'produksetting')
	                    					->where('tmuk_kode', $tmuk_kode)
	                                		->orderBy('updated_at', 'ASC')->get();

                    $data = [
                        'data' => $records,
                        'start'  => Carbon::createFromFormat('d/m/Y', $request->start)->format('Y/m/d'),
                    ];
                }
                break;
            case 'umur-persediaan':
                $records = Produk::byStockTmuk($tmuk_kode)->get();
                $data = [
                    'detail' => $records,
                    // 'start'  => $start,
                    // 'end'    => $end,
                    // 'tmuk'   => $tmuk_kode,
                ];
                break;
            default:
            break;
        }
        /*return views*/
        return view('modules.laporan.laporan.lists.persediaan.'.$request->type, $data);
    }

    public function printPdf($tipe_laporan, $data){
	    $pdf = \PDF::loadView('report.laporan.persediaan.'.$tipe_laporan, $data);
	    return $pdf->setOption('margin-bottom', 5)
	    	->setPaper('a4')
	    	->setOption('margin-top', 5)
	    	->setOption('margin-left', 5)
	    	->setOption('margin-right', 5)
	    	->inline();
 	 }

    //  PRINT LAPORAN
    //  PRINT LAPORAN PDF
    //  PEMBELIAN
    public function printPersediaan(Request $request){
    	function tgl_indonesia($date){
    		$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    		$tahun     = substr($date, 0, 4);
    		$bulan     = substr($date, 5, 2);
    		$tgl    = substr($date, 8, 2);
    		$waktu    = substr($date,11, 5);
    		$hari    = date("w", strtotime($date));
    		$result = $tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
    		return $result;
    	}
        // dd($request->all());
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start  = '';
        $tmuk_kode = $tmuk->kode;
        $record = [];
        $kode = $request->kode_produk;
        $nama_tmuk = $tmuk->nama;
        if($request->start and $request->end){
            $start  = $request->start;
            $end    = $request->end;
            $tanggal = [$start,$end];
        }

        switch ($request->laporan) {
            case 'nilai-persediaan':
                    if($request->start){
						$start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                        $record = ProdukTmuk::with(['detail' => function($q) use ($start){
                    							$q->where('date', $start);
                							}], 'produk', 'produksetting')
	                    					->where('tmuk_kode', $tmuk_kode)
	                                		->orderBy('updated_at', 'ASC')->get();
                        $req            = $request->all();
                        $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                        $region         = $tmuk->lsi->region->area;
                        $lsi            = $tmuk->lsi->nama;
                        $tmuk           = $tmuk->nama;

                        $data = [
                            'request'   => $req,
                            'fiskal'    => $tahun_fiskal,
                            'region'    => $region,
                            'lsi'       => $lsi,
                            'tmuk'      => $tmuk,
                            'detail' => $record,
                            'start'  => tgl_indonesia($start),
                            'label_file' => 'Rekap Nilai Persediaan '.$tmuk_kode.' '.tgl_indonesia($start),
                        ];
                    }
                    break;
            case 'pergerakan-persediaan':
                    if($request->start and $request->end){
                    	$cek = [];
                    	$tmuk = [];
                    	$kode_produk = $kode;
                    	$start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    	$end  = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');

                    	$records = ProdukTmuk::where('produk_kode', $kode_produk)
                    						 ->with(['detail2' => function($q) use ($start, $end){
                    								$q->whereBetween('date', [$start, $end])
                    						->orderBy('date');
                    							}, 'produk'])
                    						->where('tmuk_kode', $tmuk_kode)
                    						->get();

                    	$date = date_create($start);
                    	date_sub($date, date_interval_create_from_date_string('1 days'));
                    	$yesterday =  date_format($date, 'Y-m-d');
                    	foreach ($records->first()->detail2 as $key => $value) {
                    		$ad = ADDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
                    						->where('date', $value->date)
                    						->first();
                    		$pyr = PYRDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
                    						->where('date', $value->date)
                    						->first();
                    		$jual = PenjualanDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
                    						->where('date', $value->date)
                    						->first();
                    		$rr = RRDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
                    						->where('date', $value->date)
                    						->first();
                    		$so = SODetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
                    						->where('date', $value->date)
                    						->first();
                    		$gr = GRDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
                    						->where('date', $value->date)
                    						->first();

                    		if($ad != null){
                    			$data_ad = $ad->qty;	
                    		}else{
                    			$data_ad = 0;
                    		}

                    		if($pyr != null){
                    			$data_pyr = $pyr->qty;	
                    		}else{
                    			$data_pyr = 0;
                    		}

                    		if($jual != null){
                    			$data_jual = $jual->qty;	
                    		}else{
                    			$data_jual = 0;
                    		}

                    		if($rr != null){
                    			$data_rr = $rr->qty;	
                    		}else{
                    			$data_rr = 0;
                    		}
                    		if($so != null){
                    			$data_so = $so->qty;	
                    		}else{
                    			$data_so = 0;
                    		}

                    		if($gr != null){
                    			$data_gr = $gr->qty;	
                    		}else{
                    			$data_gr = 0;
                    		}

                    		$hasil[] = [
                    			'id' => $value->produk_tmuk_kode,
                    			'date' => $value->date,
                    			'qty' => $value->qty,
                    			'hpp' => $value->hpp,
                    			'nilai' => $value->nilai_persediaan,
                    			'ad' => $data_ad,
                    			'pyr' => $data_pyr,
                    			'jual' => $data_jual,
                    			'rr' => $data_rr,
                    			'so' => $data_so,
                    			'gr' => $data_gr,
                    		];
                    	}
                    	$prod = Produk::where('kode', $kode_produk)
                    					->first();

                    	if($prod != null){
                    		$kategori = $prod->l1_nm;
                    		$nama =$prod->nama;
                    	}else{
                    		$kategori = '-';
                    		$nama = '-';
                    	}

                    	$tmuk[] = [
                    		'nama' => $nama_tmuk,
                    		'tanggal' => $tanggal,
                    		'kategori' => $kategori,
                    		'nama_produk' => $nama,
                    		'kode' => $kode_produk,
                    		'awal' => $start,
                    		'akhir' => $end,
                    	];

                    	$begining = ProdukTmukDetail::where('date','=', $yesterday)
                    								->where('produk_tmuk_kode','=', $value->produk_tmuk_kode)
                    								->first();
                    	if(count($begining) > 0){
                    		$beg[] = [
                    			'qty' => isset($begining) ? $begining->qty : '0',
                    			'hpp' => isset($begining) ? $begining->hpp : '0',
                    			'nilai' => isset($begining) ? $begining->nilai_persediaan : '0',
                    		];
                    	}else{
                    		$beg[] = [
                    			'qty'   => $records->first()->detail2->first()->qty,
                    			'hpp'   => $records->first()->detail2->first()->hpp,
                    			'nilai' => $records->first()->detail2->first()->nilai_persediaan,
                    		];
                    	}

                    	$data = [
                    		'hasil'       => $hasil,
                    		'begining'    => $beg,
                    		'tmuk'        => $tmuk,
                    		'hitung'      => count($records->first()->detail2),
                    		'start'       => Carbon::createFromFormat('d/m/Y', $request->start)->format('Y/m/d'),
                    		'end'         => Carbon::createFromFormat('d/m/Y', $request->end)->format('Y/m/d'),
                    		'kode_produk' => $kode_produk,
                    		'kode_tmuk'   => $tmuk_kode,
                    		'label_file' => 'Pergerakan Persediaan '.$tmuk_kode.' '.$start,
                    	];
                    }
                    break;
            case 'stok-opname':
                $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $record = TransSO::with('tmuk.lsi','detail.produk','detail.produksetting','detail.produksetting')
	                        ->where('tgl_so', $start)
	                        ->where('tmuk_kode', $tmuk_kode)
	                        ->where('type', $request->tipe)
	                        ->get();

                $req            = $request->all();
                $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                $region         = $tmuk->lsi->region->area;
                $lsi            = $tmuk->lsi->nama;
                $tmuk           = $tmuk->nama;

                $data = [
                    'request'    => $req,
                    'fiskal'     => $tahun_fiskal,
                    'region'     => $region,
                    'lsi'        => $lsi,
                    'tmuk'       => $tmuk,
                    'detail'     => $record,
                    'start'      => $start,
                    'label_file' => 'Rekap Stok Opname '.$tmuk_kode.' '.$start,
                ];
                break;
            case 'hpp-map':
                if($request->start){
						$start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                        $record = ProdukTmuk::with(['detail' => function($q) use ($start){
                    							$q->where('date', $start);
                							}], 'produk', 'produksetting')
	                    					->where('tmuk_kode', $tmuk_kode)
	                                		->orderBy('updated_at', 'ASC')->get();
                        $req            = $request->all();
                        $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                        $region         = $tmuk->lsi->region->area;
                        $lsi            = $tmuk->lsi->nama;
                        $tmuk           = $tmuk->nama;

                        $data = [
                            'request'   => $req,
                            'fiskal'    => $tahun_fiskal,
                            'region'    => $region,
                            'lsi'       => $lsi,
                            'tmuk'      => $tmuk,
                            'detail' => $record,
                            'start'  => $start,
                            'label_file' => 'Rekap HPP (MAP) '.$tmuk_kode.' '.$start,
                        ];
                    }
                break;
            case 'umur-persediaan':
                $record = Produk::byStockTmuk($tmuk_kode)->select('*');
                
                    $record         = $record->get();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                $data = [
                    'fiskal'     => $tahun_fiskal,
                    'region'     => $region,
                    'lsi'        => $lsi,
                    'tmuk'       => $tmuk,
                    'detail'     => $record,
                    'tmuk'       => $tmuk_kode,
                    'label_file' => 'Rekap Umur Persediaan '.$tmuk_kode.' '.\Carbon\Carbon::today()->toDateString(),
                ];
                break;
            default:
            break;
        }
        $tipe_laporan = $request->laporan;
        // $content = view('report.laporan.persediaan.'.$request->laporan, $data);
        return $this->printPdf($tipe_laporan, $data);
        // HTML2PDF($content, $data);
    }
    // END LAPORAN PEMBELIAN



    //  PRINT LAPORAN EXEL
    //  PEMBELIAN
    public function exelPersediaan(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start  = '';
        $tmuk_kode = $tmuk->kode;
        $nama_tmuk = $tmuk->nama;
        $record = [];

        if($request->start and $request->end){
            $start  = $request->start;
            $end    = $request->end;
            $tanggal = [$start,$end];
        }

        switch ($request->laporan) {
            case 'nilai-persediaan':
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $current_date_time = Carbon::now();
					$end = $current_date_time->format('Y-m-d');
					$tanggal = [$start];
                    $this->exelNilaiPersediaan($tmuk_kode, $tanggal);
                break;
            case 'pergerakan-persediaan':
                if($request->start and $request->end){
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $kode = $request->kode_produk;
                }

				$cek = [];
				$tmuk = [];
				$kode_produk = $kode;

				$records = ProdukTmuk::where('produk_kode', $kode_produk)
									  ->with(['detail2' => function($q) use ($start, $end){
											$q->whereBetween('date', [$start, $end])
									  ->orderBy('date');
										}, 'produk'])
									  ->where('tmuk_kode', $tmuk_kode)
									  ->get();

				$date = date_create($start);
				date_sub($date, date_interval_create_from_date_string('1 days'));
				$yesterday =  date_format($date, 'Y-m-d');
				foreach ($records->first()->detail2 as $key => $value) {
					$ad = ADDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
									->where('date', $value->date)
									->first();
					$pyr = PYRDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
									->where('date', $value->date)
									->first();
					$jual = PenjualanDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
									->where('date', $value->date)
									->first();
					$rr = RRDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
									->where('date', $value->date)
									->first();
					$so = SODetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
									->where('date', $value->date)
									->first();
					$gr = GRDetail::where('produk_tmuk_kode', $value->produk_tmuk_kode)
									->where('date', $value->date)
									->first();

					if($ad != null){
						$data_ad = $ad->qty;	
					}else{
						$data_ad = 0;
					}

					if($pyr != null){
						$data_pyr = $pyr->qty;	
					}else{
						$data_pyr = 0;
					}

					if($jual != null){
						$data_jual = $jual->qty;	
					}else{
						$data_jual = 0;
					}

					if($rr != null){
						$data_rr = $rr->qty;	
					}else{
						$data_rr = 0;
					}
					if($so != null){
						$data_so = $so->qty;	
					}else{
						$data_so = 0;
					}

					if($gr != null){
						$data_gr = $gr->qty;	
					}else{
						$data_gr = 0;
					}

					$hasil[] = [
						'id' => $value->produk_tmuk_kode,
						'date' => $value->date,
						'qty' => $value->qty,
						'hpp' => $value->hpp,
						'nilai' => $value->nilai_persediaan,
						'ad' => $data_ad,
						'pyr' => $data_pyr,
						'jual' => $data_jual,
						'rr' => $data_rr,
						'so' => $data_so,
						'gr' => $data_gr,
					];
				}
				$prod = Produk::where('kode', $kode_produk)
								->first();

				if($prod != null){
					$kategori = $prod->l1_nm;
					$nama =$prod->nama;
				}else{
					$kategori = '-';
					$nama = '-';
				}

				$tmuk[] = [
					'nama' => $nama_tmuk,
					'tanggal' => $tanggal,
					'kategori' => $kategori,
					'nama_produk' => $nama,
					'kode' => $kode_produk,
					'awal' => $start,
					'akhir' => $end,
				];

				$begining = ProdukTmukDetail::where('date','=', $yesterday)
                    							->where('produk_tmuk_kode','=', $value->produk_tmuk_kode)
	                                			->first();
				if(count($begining) > 0){
					$beg[] = [
						'qty' => isset($begining) ? $begining->qty : '0',
						'hpp' => isset($begining) ? $begining->hpp : '0',
						'nilai' => isset($begining) ? $begining->nilai_persediaan : '0',
					];
				}else{
					$beg[] = [
						'qty'   => $records->first()->detail2->first()->qty,
						'hpp'   => $records->first()->detail2->first()->hpp,
						'nilai' => $records->first()->detail2->first()->nilai_persediaan,
					];
				}

				$data[0] = [
					'hasil'       => $hasil,
					'begining'    => $beg,
					'tmuk'        => $tmuk,
					'hitung'      => count($records->first()->detail2),
					'start'       => Carbon::createFromFormat('d/m/Y', $request->start)->format('Y/m/d'),
					'end'         => Carbon::createFromFormat('d/m/Y', $request->end)->format('Y/m/d'),
					'kode_produk' => $kode_produk,
					'kode_tmuk'   => $tmuk_kode,
				];

                $this->exelPergerakanPersediaan($tmuk_kode, $start, $end, $kode, $data);
                break;
            case 'stok-opname':
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $tipe = $request->tipe;
                $this->exelStokOpname($tmuk_kode, $start, $tipe);
                break;
            case 'hpp-map':
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $current_date_time = Carbon::now();
					$end = $current_date_time->format('Y-m-d');
                    $this->exelHppMap($tmuk_kode, $start);
                break;
            case 'umur-persediaan':
                    $tanggal  = \Carbon\Carbon::yesterday()->toDateString();
                    $this->exelUmurPersediaan($tmuk_kode, $tanggal);
                break;
            default:

                break;
        }
    }

    public function exelNilaiPersediaan($tmuk_kode, $start)
    {
    	function tgl_indonesia($date){
    		$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    		$tahun     = substr($date, 0, 4);
    		$bulan     = substr($date, 5, 2);
    		$tgl    = substr($date, 8, 2);
    		$waktu    = substr($date,11, 5);
    		$hari    = date("w", strtotime($date));
    		$result = $tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
    		return $result;
    	}
        $record = ProdukTmuk::with(['detail' => function($q) use ($start){
                    							$q->where('date', $start);
                							}], 'produk', 'produksetting')
	                    					->where('tmuk_kode', $tmuk_kode)
	                                		->orderBy('updated_at', 'ASC')->get();

        $Export = Excel::create('Rekap Nilai Persediaan ', function($excel) use ($record, $start){
            $excel->setTitle('Rekap Nilai Persediaan');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Nilai Persediaan', function($sheet) use ($record, $start){
                $sheet->row(1, array(
                    'Rekap Nilai Persediaan '
                    ));

                $sheet->row(2, array(
                    'Periode : '.tgl_indonesia($start[0])
                    ));
                $sheet->row(3, array(
                    'No',
                    'Category 1',
                    'Category 2',
                    'Kode Produk',
                    'Nama Produk',
                    'UOM',
                    'Qty',
                    'Cost Price (MAP)',
                    'Jumlah'
                    ));

                $row = 4;
                $no =1;
                $total_qty =0;
                $total_map =0;
                $total_nilai =0;
                foreach ($record as $val) {
                    $qty = isset($val->detail) ? $val->detail->qty : 0;
                    $map = isset($val->detail) ? $val->detail->hpp : 0;
                    $sheet->row($row, array(
                        $no,
                        isset($val->produk->l1_nm) ? $val->produk->l1_nm : '-',
                        isset($val->produk->l2_nm) ? $val->produk->l2_nm : '-',
                        $val->produk->kode,
                        $val->produk->nama,
                        isset($val->produksetting->uom1_satuan) ? $val->produksetting->uom1_satuan : '-',
                        $qty,
                        $map,
                        $qty*$map,
                        ));
                    $row=$row;
                    $row++;$no++;
                    $total_qty +=$qty;
                    $total_map +=$map;
                    $total_nilai +=($qty*$map);
                }

                $a = 'A'.$row; // or any value
				$b = 'E'.$row; // or any value
				$c = 'G'.$row; // or any value
				$d = 'I'.$row; // or any value
				$e = 'F'.$row; // or any value
                $sheet->setBorder('A3:I'.$row, 'thin');
                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');
                $sheet->mergeCells("$a:$b");
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:I2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:I3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->cells("$a:$e", function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                    $cells->setFontWeight('bold');
                });
                $sheet->cells("$c:$d", function($cells){
                    $cells->setAlignment('right');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                    $cells->setFontWeight('bold');
                });
                $sheet->freezePane('A4');
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>15, 'E'=>40, 'F'=>15, 'G'=>9, 'H'=>15, 'I'=>15));
                $sheet->getStyle('B4:I'.($row - 1))->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:I'.($row - 1))->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.($row - 1))->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.($row - 1))->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('F4:F'.($row - 1))->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->row($row, array(
                	'',
                	'',
                	'',
                	'',
                	'',
                	'Total',
                	$total_qty,
                	number_format($total_map),
                	number_format($total_nilai)
                ));
            });
        });
        $Export ->download('xls'); 
    }

    public function exelPergerakanPersediaan($tmuk_kode, $start, $end, $kode, $data)
    {
       	$Export = Excel::create('Rekap Pergerakan Persediaan ', function($excel) use ($tmuk_kode, $start, $end, $kode, $data){
            $excel->setTitle('Rekap Pergerakan Persediaan');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Pergerakan Persediaan', function($sheet) use ($tmuk_kode, $start, $end, $kode, $data){
            	function tgl_indonesia($date){
            		$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            		$tahun     = substr($date, 0, 4);
            		$bulan     = substr($date, 5, 2);
            		$tgl    = substr($date, 8, 2);
            		$waktu    = substr($date,11, 5);
            		$hari    = date("w", strtotime($date));
            		$result = $tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
            		return $result;
            	}
                $sheet->row(1, array(
                    'Rekap Pergerakan Persediaan '
                    ));

                $sheet->row(2, array(
                    'Periode : '.tgl_indonesia($start).' -  '.tgl_indonesia($end)
                    ));
                $sheet->row(3, array(
                    'Nama Tmuk',
                    ': '.$data[0]['tmuk'][0]['nama'],
                    ));
                $sheet->row(4, array(
                    'Kategori',
                    ': '.$data[0]['tmuk'][0]['kategori'],
                    ));
                $sheet->row(5, array(
                    'Kode Produk',
                    ': '.$data[0]['tmuk'][0]['kode'],
                    ));
                $sheet->row(6, array(
                    'Nama Produk',
                    ': '.$data[0]['tmuk'][0]['nama_produk'],
                    ));
                $sheet->row(7, array(
                    'Tanggal',
                    ': '.tgl_indonesia($data[0]['tmuk'][0]['awal']).' - '.tgl_indonesia($data[0]['tmuk'][0]['akhir']),
                    ));
                $sheet->row(8, array(
                    'Date',
                    'Receiving',
                    'Sales',
                    'Returning',
                    'Adjustment',
                    'Stock Opname',
                    'Payment Request',
                    'Balance Qty',
                    'HPP',
                    'Balance Amount'
                    ));
                $sheet->freezePane('A10');
                $sheet->row(9, array(
                    'Begining Stock',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    $data[0]['begining'][0]['qty'],
                    number_format($data[0]['begining'][0]['hpp']),
                    number_format($data[0]['begining'][0]['nilai'])
                    ));

                $row = 10;$no =1;$tot_gr=0;$tot_jual=0;$tot_rr=0;$tot_ad=0;$tot_so=0;$tot_pyr=0;$tot_qty=0;$tot_hpp=0;$tot_nilai=0;
               	for($i=0;$i<$data[0]['hitung'];$i++){
	           		$sheet->row($row, array(
	                    tgl_indonesia($data[0]['hasil'][$i]['date']),
	                    number_format($data[0]['hasil'][$i]['gr']),
	                    number_format($data[0]['hasil'][$i]['jual']),
	                    number_format($data[0]['hasil'][$i]['rr']),
	                    number_format($data[0]['hasil'][$i]['ad']),
	                    number_format($data[0]['hasil'][$i]['so']),
	                    number_format($data[0]['hasil'][$i]['pyr']),
	                    number_format($data[0]['hasil'][$i]['qty']),
	                    number_format($data[0]['hasil'][$i]['hpp']),
	                    number_format($data[0]['hasil'][$i]['nilai']),
	                ));
	                $tot_gr += $data[0]['hasil'][$i]['gr'];
	                $tot_jual += $data[0]['hasil'][$i]['jual'];
	                $tot_rr += $data[0]['hasil'][$i]['rr'];
	                $tot_ad += $data[0]['hasil'][$i]['ad'];
	                $tot_so += $data[0]['hasil'][$i]['so'];
	                $tot_pyr += $data[0]['hasil'][$i]['pyr'];
	                $tot_qty += $data[0]['hasil'][$i]['qty'];
	                $tot_hpp += $data[0]['hasil'][$i]['hpp'];
	                $tot_nilai += $data[0]['hasil'][$i]['nilai'];
	                $row=$row;
	                $row++;
               	}
                $a = 'A'.$row; // or any value
				$b = 'B'.$row; // or any value
				$c = 'J'.$row; // or any value
				$d = 'I'.$row; // or any value
				$e = 'F'.$row; // or any value
                $sheet->setBorder('A8:J'.$row, 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('B3:E3');
                $sheet->mergeCells('B4:E4');
                $sheet->mergeCells('B5:E5');
                $sheet->mergeCells('B6:E6');
                $sheet->mergeCells('B7:E7');
                $sheet->mergeCells('A2:J2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:J2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A9', function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                    $cells->setBackground('#FFFF00');
                    $cells->setFontColor('#000000F');
                });
                $sheet->cells('A10:A'.($row-1), function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('center');
                });
                $sheet->cells('B10:J'.($row-1), function($cells){
                    $cells->setFontSize(11);
                    $cells->setAlignment('right');
                });
                $sheet->cells('A8:J8', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->cells('B9:J9', function($cells){
                    $cells->setAlignment('right');
                    $cells->setBackground('#FFFF00');
                    $cells->setFontColor('#000000F');
                });
                $sheet->cells("$a", function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->cells("$b:$c", function($cells){
                    $cells->setAlignment('right');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A' =>20,'B' =>15,'C' =>15, 'D' =>15, 'E' =>15, 'F' =>15, 'G' =>15, 'H' =>15, 'I' =>15, 'J' =>15));
                $sheet->row($row, array(
                	'Total',
                	$tot_gr,
                	$tot_jual,
                	$tot_rr,
                	$tot_ad,
                	$tot_so,
                	$tot_pyr,
                	$tot_qty,
                	$tot_hpp,
                	$tot_nilai
                ));
            });
        });
        $Export ->download('xls'); 
    }

    public function exelStokOpname($tmuk_kode, $start, $tipe)
    {
        $record = TransSO::with('tmuk.lsi','detail.produk','detail.produksetting','detail.produksetting')
			                            ->where('tgl_so', $start)
			                            ->where('tmuk_kode', $tmuk_kode)
			                            ->where('type', $tipe)
			                            ->get();
        $Export = Excel::create('Rekap Stok Opname ', function($excel) use ($record, $start){
            $excel->setTitle('Rekap Stok Opname');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Stok Opname', function($sheet) use ($record, $start){
                $sheet->row(1, array(
                    'Rekap Stok Opname '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start
                    ));
                $sheet->row(3, array(
                    'No',
                    'Category',
                    'Divisi',
                    'Kode Produk',
                    'Nama Produk',
                    'HPP',
                    'Jumlah Fisik',
                    'Jumlah System',
                    'Variance Qty',
                    'Variance Value'
                    ));

                foreach ($record as $val) {
	                $row = 4;
	                $no =1;
	                $i = 1; $selisih_stok=0; $total=0; $total_hpp=0; $total_fisik=0; $total_system=0; $total_variance=0; 
	                $total_variance_val=0;
                	foreach ($val->detail as $key => $value) {
	                		if($value->qty_system > $value->qty_barcode){
	                			$selisih_stok = ($value->qty_system - $value->qty_barcode) * -1;
	                		}elseif($value->qty_system < 0 AND $value->qty_system < $value->qty_barcode){
	                			$selisih_stok = (($value->qty_system * -1) + $value->qty_barcode);
	                		}elseif($value->qty_system >= 0 AND $value->qty_system < $value->qty_barcode){
	                			$selisih_stok = $value->qty_barcode - $value->qty_system;
	                		}
	                		$variance_val = ($value->qty_barcode * $value->hpp);

	                    $sheet->row($row, array(
	                        $no,
	                        isset($value->produk->l1_nm) ? $value->produk->l1_nm : '-',
	                        isset($value->produk->l2_nm) ? $value->produk->l2_nm : '-',
	                        $value->produk->kode,
	                        $value->produk->nama,
	                        number_format($value->hpp),
	                        $value->qty_barcode,
	                        $value->qty_system,
	                        $selisih_stok,
	                        number_format($variance_val)
	                    ));
	                    $row=$row;
	                    $row++;$no++;
	                    $total_hpp += $value->hpp;
	                    $total_fisik += $value->qty_barcode;
	                    $total_system += $value->qty_system;
	                    $total_variance += $selisih_stok;
	                    $total_variance_val += $variance_val;
	                    $selisih_stok = 0;
                	}
                }
                $a = 'A'.$row; // or any value
				$b = 'D'.$row; // or any value
				$c = 'F'.$row; // or any value
				$d = 'J'.$row; // or any value
				$e = 'E'.$row; // or any value
                $sheet->setBorder('A3:J'.$row, 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->mergeCells("$a:$b");

                $sheet->cells("$a:$e", function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                    $cells->setFontWeight('bold');
                });

                $sheet->cells("$c:$d", function($cells){
                    $cells->setAlignment('right');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                    $cells->setFontWeight('bold');
                });

                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:J2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:J3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->freezePane('A4');
                $sheet->setWidth(array('A'=>6,'B'=>25,'C'=>30, 'D'=>20, 'E'=>50, 'F'=>15, 'G'=>13, 'H'=>13, 'I'=>13, 'J'=>15));
                $sheet->getStyle('B4:J'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:J'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('F4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'right')
                );
                $sheet->getStyle('J4:J'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'right')
                );

                $sheet->row($row, array(
                	'',
                	'',
                	'',
                	'',
                	'Total',
                	number_format($total_hpp),
                	number_format($total_fisik),
                	number_format($total_system),
                	number_format($total_variance),
                	number_format($total_variance_val)
                ));

            });
        });
        $Export ->download('xls'); 
    }
    
    public function exelHppMap($tmuk_kode, $start)
    {

        $record = ProdukTmuk::with(['detail' => function($q) use ($start){
                    							$q->where('date', $start);
                							}], 'produk', 'produksetting')
	                    					->where('tmuk_kode', $tmuk_kode)
	                                		->orderBy('updated_at', 'ASC')->get();

        $Export = Excel::create('Rekap HPP MAP ', function($excel) use ($record, $start){
            $excel->setTitle('Rekap HPP MAP');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap HPP MAP', function($sheet) use ($record, $start){
                $sheet->row(1, array(
                    'Rekap HPP MAP '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode Produk',
                    'Nama Produk',
                    'UOM',
                    'HPP (Rp)'
                    ));
				$sheet->freezePane('A4');
                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $map = isset($val->detail) ? $val->detail->hpp : 0;
                    $sheet->row($row, array(
                        $no,
                        $val->produk_kode,
                        $val->produk->nama,
                        isset($val->produksetting->uom1_satuan) ? $val->produksetting->uom1_satuan : '-',
                        $map
                    ));
                    $row=$row;
                    $row++;$no++;
                }

                $sheet->setBorder('A3:E'.($row-1), 'thin');
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:E2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:E3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>50, 'D'=>15, 'E'=>15));
                $sheet->getStyle('B4:E'.($row-1))->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:E'.($row-1))->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:B'.($row-1))->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('D4:D'.($row-1))->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
            });
        });
        $Export ->download('xls'); 
    }

    public function exelUmurPersediaan($tmuk_kode, $tanggal)
    {
        $records = Produk::byStockTmuk($tmuk_kode)->select('*');

        $record = $records->get();

        $Export = Excel::create('Rekap Umur Persediaan ', function($excel) use ($record, $tanggal){
            $excel->setTitle('Rekap Umur Persediaan');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Umur Persediaan', function($sheet) use ($record, $tanggal){
                $sheet->row(1, array(
                    'Rekap Umur Persediaan '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$tanggal
                    ));
                $sheet->row(3, array(
                    'No',
                    'Kode Produk',
                    'Deskripsi Produk',
                    'Total Qty',
                    'Qty (1-30 hari)',
                    'Qty (31-60 hari)',
                    'Qty (61-90 hari)',
                    'Qty (91-120 hari)',
                    'Qty >120 hari'
                ));

                $row = 4;$no =1;$total_qty=0;$total_qty1=0;$total_qty2=0;$total_qty3=0;$total_qty4=0;$total_qty5=0;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->kode,
                        $val->nama,
                        $val->getTotalStock(),
                        $val->getStockByDay(1),
                        $val->getStockByDay(31),
                        $val->getStockByDay(61),
                        $val->getStockByDay(91),
                        $val->getStockByDay(120)
                    ));
                    $total_qty += $val->getTotalStock();
                    $total_qty1 += $val->getStockByDay(1);
                    $total_qty2 += $val->getStockByDay(31);
                    $total_qty3 += $val->getStockByDay(61);
                    $total_qty4 += $val->getStockByDay(91);
                    $total_qty5 += $val->getStockByDay(120);
                    $row=$row;
                    $row++;$no++;
                }
                $a = 'A'.$row; // or any value
				$b = 'B'.$row; // or any value
				$c = 'C'.$row; // or any value
				$d = 'D'.$row; // or any value
				$e = 'I'.$row; // or any value

                $sheet->setBorder('A3:I'.$row, 'thin');
                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');
                $sheet->mergeCells("$a:$b");
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->freezePane('A4');
                $sheet->cells('A2:I2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:I3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->cells("$a:$c", function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->cells("$d:$e", function($cells){
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>50, 'D'=>15, 'E'=>15, 'F'=>15, 'G'=>15, 'H'=>15, 'I'=>15));
                $sheet->getStyle('B4:I'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:I'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->row($row, array(
                	'',
                	'',
                	'Total',
                	$total_qty,
                	$total_qty1,
                	$total_qty2,
                	$total_qty3,
                	$total_qty4,
                	$total_qty5
                ));

            });
        });
        $Export ->download('xls'); 
    }

}
