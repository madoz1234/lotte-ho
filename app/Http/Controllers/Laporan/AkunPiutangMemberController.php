<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;
use Carbon\Carbon;
//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransMemberPiutang;
use Lotte\Models\Trans\TransMemberPiutangDetail;
use Lotte\Models\Master\MemberCard;

class AkunPiutangMemberController extends Controller
{
    protected $link = 'laporan/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan");
        
    }
    public function grid(Request $request)
    {
        // dd($request->all());
        // 
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);


        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;
        $data  = [];
        // if($request->start and $request->end){
        //     $start = $request->start;
        //     $end   = $request->end;
        //     $tanggal = [$start,$end];
        // }

        // $data = [
        // 'detail' => [],
        // 'start'  => $start,
        // 'end'    => $end,
        // ];
        switch ($request->type) {
            case 'transaksi-belum-lunas':

                // $records = TransJurnal::select(\DB::raw('tmuk_kode, tanggal, idtrans, sum(jumlah) as jml'))->groupBy('tmuk_kode', 'tanggal', 'idtrans')->whereIn('coa_kode', ['1.1.3.1']);
                
                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $records = TransMemberPiutang::with('detail')->select('*');

                            // dd($records);
                    if ($tmuk_kode = $tmuk_kode) {
                        $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                ->whereBetween(\DB::raw('date(created_at)'), array($request->start, $request->end));
                    }

                    // if(isset($tanggal)){
                    //          $records->whereHas('detail',  function($records) use ($tanggal){
                    //                 $records->whereBetween('tanggal', $tanggal);
                    //          });
                    //     }

                    // $records = $records->get();
                    $data = [
                    'detail' => $records->get(),
                    'start'  => $start,
                    'end'    => $end,
                    ];
                }
                break;
                //test
                // case 'pergerakan-persediaan':
                //         if($request->start and $request->end){
                //             $start = $request->start;
                //             $end   = $request->end;

                //             $records = ProdukTmuk::with(['tmuk', 'produk.produksetting'])
                //                             ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                //                             ->whereBetween(\DB::raw('date(updated_at)'), array($request->start, $request->end))
                //                             ->select('*');

                //             $data = [
                //                 'detail' => $records->get(),
                //                 'start'  => $start,
                //                 'end'    => $end,
                //             ];
                //         }
                //         break;
                //test
            case 'piutang-member':
                    // $records = TransJurnal::select('*')->whereIn('coa_kode', ['1.1.3.1'])->get();
                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;

                    $records = TransJurnal::select(\DB::raw('tmuk_kode, tanggal, idtrans, sum(jumlah) as jml'))->groupBy('tmuk_kode', 'tanggal', 'idtrans')->whereIn('coa_kode', ['1.1.3.1'])
                                ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                ->where('delete', 0)
                                ->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end))
                                ->select('*');
                    // $records = $records->get();

                    // dd($records);
                    // if ($tmuk_kode = $tmuk_kode) {
                    //     $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    // }

                            // dd($records);
                    $data = [
                    'detail' => $records->get(),
                    'start'  => $start,
                    'end'    => $end,
                    ];
                }
            break;
            case 'umur-piutang-member':
            // $records = TransJurnal::select(\DB::raw('tmuk_kode, tanggal, idtrans, sum(jumlah) as jml'))->groupBy('tmuk_kode', 'tanggal', 'idtrans')->whereIn('coa_kode', ['1.1.3.1']);
            //     if ($tmuk_kode = $tmuk_kode) {
            //         $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
            //     }
            //     if($request->start==true and $request->end==true){
            //         $start = $request->start;
            //         $end   = $request->end;
            //         $records->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end));
            //     }
            //     $records = $records->get();

            //             // dd($records);
            //     $data = [
            //     'detail' => $records,
            //     'start'  => $start,
            //     'end'    => $end,
            //     ];
                // if ($tmuk_kode = $tmuk_kode) {
                //     $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                // }
                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $records = TransJurnal::select(\DB::raw('tmuk_kode, tanggal, idtrans, sum(jumlah) as jml'))->groupBy('tmuk_kode', 'tanggal', 'idtrans')->whereIn('coa_kode', ['1.1.3.1'])
                            ->groupBy('tanggal')->orderBy('tanggal','desc')
                            ->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end))
                            ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                            ->where('delete', 0);
                $records = $records->get();

                        // dd($records);
                $data = [
                'detail' => $records,
                'start'  => $start,
                'end'    => $end,
                ];
                }
            break;
            case 'pembayaran-member':

                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $records = TransMemberPiutang::with('detail')
                            ->whereBetween(\DB::raw('created_at'), array($request->start, $request->end))
                            ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                            ->select('*');
                    $data = [
                    'detail' => $records->get(),
                    'start'  => $start,
                    'end'    => $end,
                    ];
                }

                // $record = TransMemberPiutang::with('detail')->select('*');
                // if ($tmuk_kode = $tmuk_kode) {
                //     $record->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                // }
                // if(isset($tanggal)){
                //          $record->whereHas('detail',  function($record) use ($tanggal){
                //                 $record->whereBetween('tanggal', $tanggal);
                //          });
                //     }
                // $records = $record->get();

                // $data = [
                // 'detail' => $records,
                // 'start'  => $start,
                // 'end'    => $end,
                // ];
            break;
            case 'daftar-member':

                // dd($records);

                        // dd($records);
                // if ($tmuk_kode = $tmuk_kode) {
                //     $record->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                // }
                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $records = MemberCard::select('*')
                            ->whereBetween(\DB::raw('created_at'), array($request->start, $request->end));
                            // ->select(*);
                // $records = $record->get();

                        // dd($records->toArray());
                $data = [
                'detail' => $records->get(),
                'start'  => $start,
                'end'    => $end,
                ];
                }
            break;
            default:
            break;
        }
        /*return views*/
        return view('modules.laporan.laporan.lists.akun-piutang-member.'.$request->type, $data);
    }

    //  PRINT LAPORAN
    //  PRINT LAPORAN PDF
    //  PEMBELIAN
    public function printAkunPiutangMember(Request $request){
        // dd($request->all());
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start  = '';
        $end    = '';
        $tmuk_kode = $tmuk->kode;
        $record = [];

        switch ($request->laporan) {
            case 'transaksi-belum-lunas':
                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $record = TransMemberPiutang::with('detail')
                                    ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                    ->whereBetween(\DB::raw('date(created_at)'), array($request->start, $request->end));

                    $record         = $record->get();
                    $req            = $request->all();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                    $data = [
                        'request'   => $req,
                        'fiskal'    => $tahun_fiskal,
                        'region'    => $region,
                        'lsi'       => $lsi,
                        'tmuk'      => $tmuk,
                        'records' => $record,
                        // 'detail' => $record->orderBy('updated_at', 'ASC')->get(),
                        'start'  => $start,
                        'end'    => $end,
                        'label_file' => 'Rekap Piutang Member '.$tmuk_kode.' '.$start.' s/d '.$end,
                    ];
                }
            break;
            case 'piutang-member':
                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $record = TransMemberPiutang::with('detail')
                                    ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                    ->whereBetween(\DB::raw('date(created_at)'), array($request->start, $request->end));

                    $record         = $record->get();
                    $req            = $request->all();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                    $data = [
                        'request'   => $req,
                        'fiskal'    => $tahun_fiskal,
                        'region'    => $region,
                        'lsi'       => $lsi,
                        'tmuk'      => $tmuk,
                        'records' => $record,
                        'start'  => $start,
                        'end'    => $end,
                        'label_file' => 'Rekap Pembayaran Member '.$tmuk_kode.' '.$start.' s/d '.$end,
                    ];
                }
            break;
            case 'pembayaran-member':
                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $record = TransMemberPiutang::with('detail')
                                    ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                    ->whereBetween(\DB::raw('date(created_at)'), array($request->start, $request->end));

                    $record         = $record->get();
                    $req            = $request->all();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                    $data = [
                        'request'   => $req,
                        'fiskal'    => $tahun_fiskal,
                        'region'    => $region,
                        'lsi'       => $lsi,
                        'tmuk'      => $tmuk,
                        'records' => $record,
                        'start'  => $start,
                        'end'    => $end,
                        'label_file' => 'Rekap Pembayaran Member '.$tmuk_kode.' '.$start.' s/d '.$end,
                    ];
                }
            break;
            case 'umur-piutang-member':

                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $record = TransJurnal::select(\DB::raw('tmuk_kode, tanggal, idtrans, sum(jumlah) as jml'))->groupBy('tmuk_kode', 'tanggal', 'idtrans')->whereIn('coa_kode', ['1.1.3.1'])
                                ->groupBy('tanggal')->orderBy('tanggal','desc')
                                ->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end))
                                ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                ->where('delete', 0);

                    $record         = $record->get();
                    $req            = $request->all();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                    $data = [
                        'request'   => $req,
                        'fiskal'    => $tahun_fiskal,
                        'region'    => $region,
                        'lsi'       => $lsi,
                        'tmuk'      => $tmuk,
                        'records' => $record,
                        'start'  => $start,
                        'end'    => $end,
                        'label_file' => 'Rekap Pembayaran Member '.$tmuk_kode.' '.$start.' s/d '.$end,
                    ];
                }
            break;
            case 'daftar-member':
                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $record = MemberCard::select('*')
                            ->whereBetween(\DB::raw('created_at'), array($request->start, $request->end));

                    $record         = $record->get();
                    $req            = $request->all();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                    $data = [
                        'request'   => $req,
                        'fiskal'    => $tahun_fiskal,
                        'region'    => $region,
                        'lsi'       => $lsi,
                        'tmuk'      => $tmuk,
                        'records' => $record,
                        'start'  => $start,
                        'end'    => $end,
                        'label_file' => 'Daftar Member '.$tmuk_kode.' '.$start.' s/d '.$end,
                    ];
                }
            break;
            default:

            break;
        }

        $content = view('report.laporan.akun-piutang-member.'.$request->laporan, $data);

        $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
            ];
        // return $content;
        HTML2PDF($content, $data);
    }
    // END LAPORAN PEMBELIAN



    //  PRINT LAPORAN EXEL
    //  PEMBELIAN
    public function exelAkunPiutangMember(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $request = $request;
        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;

        // dd($tanggal);
        switch ($request->laporan) {
            case 'transaksi-belum-lunas':
                    $this->exelTransaksiBelumLunas($tmuk_kode, $request);
                break;
            case 'pembayaran-member':
                    $this->exelPembayaranMember($tmuk_kode, $request);
                break;
            case 'daftar-member':
                    $this->exelDaftarMember($tmuk_kode, $request);
                break;
            case 'umur-piutang-member':
                    $this->exelUmurPiutangMember($tmuk_kode, $request);
                break;
            default:

            break;
        }
    }

    public function exelTransaksiBelumLunas($tmuk_kode, $request)
    {
        // dd($request->start);
        $records = TransMemberPiutang::with('detail')->select('*');
                    if ($tmuk_kode = $tmuk_kode) {
                        $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                ->whereBetween(\DB::raw('date(created_at)'), array($request->start, $request->end));
                    }
        $record = $records->get();

            $start  = Carbon::createFromFormat('Y-m-d', $request->start)->format('d/m/Y');
            $end    = Carbon::createFromFormat('Y-m-d', $request->end)->format('d/m/Y');
        
        $Export = Excel::create('Rekap Piutang Member ', function($excel) use ($record, $request, $start, $end){
            $excel->setTitle('Rekap Piutang Member');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Piutang Member ', function($sheet) use ($record, $request, $start, $end){
                $sheet->row(1, array(
                    'Rekap Piutang Member '
                    ));

                $sheet->row(2, array(
                    'Periode : ' . $start . ' - '. $end
                    
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal Transaksi',
                    'Piutang Member (Rp)',
                    'Nama Member'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    foreach ($val->detail as $data) {
                    $sheet->row($row, array(
                        $no,
                        $data->tanggal,
                        $data->hutang,
                        $data->piutang->nama_member,
                        ));
                    $row=$row;
                    $row++;$no++;
                    }
                }
                $sheet->cells('E4'.':D'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:D'.$row, 'thin');
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:D2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:D3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20));
                $sheet->getStyle('B4:D'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:D'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelPembayaranMember($tmuk_kode, $request)
    {
            // dd($request->all());

         $records = TransMemberPiutang::with('detail')
                            ->whereBetween(\DB::raw('created_at'), array($request->start, $request->end))
                            ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                            ->select('*');
        $record = $records->get();

        $start  = Carbon::createFromFormat('Y-m-d', $request->start)->format('d/m/Y');
        $end    = Carbon::createFromFormat('Y-m-d', $request->end)->format('d/m/Y');
        
        $Export = Excel::create('Rekap Pembayaran Member ', function($excel) use ($record, $request, $start, $end){
            $excel->setTitle('Rekap Pembayaran Member');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Pembayaran Member ', function($sheet) use ($record, $request, $start, $end){
                $sheet->row(1, array(
                    'Rekap Pembayaran Member '
                    ));

                $sheet->row(2, array(
                    'Periode : ' . $start . ' - '. $end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal',
                    'No Member',
                    'Nama Member',
                    'Jumlah Pembayaran (Rp)',
                    'Sisa Piutang (Rp)'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->created_at,
                        $val->no_member,
                        $val->nama_member,
                        '0',
                        '0',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:F2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>30, 'F'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelDaftarMember($tmuk_kode, $request)
    {
            // dd($request->all());

         $records = MemberCard::select('*')
                            ->whereBetween(\DB::raw('created_at'), array($request->start, $request->end));
        $record = $records->get();

        $start  = Carbon::createFromFormat('Y-m-d', $request->start)->format('d/m/Y');
        $end    = Carbon::createFromFormat('Y-m-d', $request->end)->format('d/m/Y');
        
        $Export = Excel::create('Daftar Member ', function($excel) use ($record, $request, $start, $end){
            $excel->setTitle('Daftar Member');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Daftar Member ', function($sheet) use ($record, $request, $start, $end){
                $sheet->row(1, array(
                    'Daftar Member '
                    ));

                $sheet->row(2, array(
                    'Periode : ' . $start . ' - '. $end
                    ));
                $sheet->row(3, array(
                    'No',
                    'No Member',
                    'Nama Member',
                    'Alamat',
                    'Telepon',
                    'Email',
                    'Limit Kredit',
                    'Total Order',
                    'Total pembelian (Rp)',
                    'Last Visit',
                    'Ave Pembelian'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->nomor,
                        $val->nama,
                        $val->alamat,
                        $val->telepon,
                        $val->email,
                        $val->limit_kredit==null ? '-': $val->limit_kredit,
                        '0',
                        '0',
                        '0',
                        '0',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':K'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:K'.$row, 'thin');
                $sheet->mergeCells('A1:K1');
                $sheet->mergeCells('A2:K2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:K2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:K3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>30, 'D'=>30, 'E'=>30, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20, 'K'=>20));
                $sheet->getStyle('B4:K'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:K'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelUmurPiutangMember($tmuk_kode, $request)
    {
            // dd($request->all());

        $records = TransJurnal::select(\DB::raw('tmuk_kode, tanggal, idtrans, sum(jumlah) as jml'))->groupBy('tmuk_kode', 'tanggal', 'idtrans')->whereIn('coa_kode', ['1.1.3.1'])
                            ->groupBy('tanggal')->orderBy('tanggal','desc')
                            ->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end))
                            ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                            ->where('delete', 0);

        $record = $records->get();

        $start  = Carbon::createFromFormat('Y-m-d', $request->start)->format('d/m/Y');
        $end    = Carbon::createFromFormat('Y-m-d', $request->end)->format('d/m/Y');
        
        $Export = Excel::create('Rekap Umur Piutang Member ', function($excel) use ($record, $request, $start, $end){
            $excel->setTitle('Rekap Umur Piutang Member');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Rekap Umur Piutang Member ', function($sheet) use ($record, $request, $start, $end){
                $sheet->row(1, array(
                    'Rekap Umur Piutang Member '
                    ));

                $sheet->row(2, array(
                    'Periode : ' . $start . ' - '. $end
                    ));
                $sheet->row(3, array(
                    'No',
                    'No. Transaksi',
                    'Tanggal Transaksi',
                    'Umur Piutang',
                    'Jumlah',
                    '30 hari [0-30]',
                    '60 hari [31-60]',
                    '90 hari [61-90]',
                    '>90 hari [91-dst]'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->idtrans,
                        $val->tanggal,
                        '0',
                        $val->jml,
                        '0',
                        '0',
                        '0',
                        '0',
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':I'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:I'.$row, 'thin');
                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:I2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:I3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>30, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20));
                $sheet->getStyle('B4:I'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:I'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

}
