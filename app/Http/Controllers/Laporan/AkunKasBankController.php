<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Excel;

use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Coa;

use Carbon\Carbon;

class AkunKasBankController extends Controller
{
    protected $link = 'laporan/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan");
        
    }
    public function grid(Request $request)
    {
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;
        $tgl_opening_tmuk = $tmuk->created_at;
        $data  = [];

        if($request->start and $request->end){
            $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
        }

        switch ($request->type) {
            case 'akun-kas-bank':
            if($request->start and $request->end){
                $start   = $request->start;
                $end     = $request->end;
                $records = TransJurnal::select('*')
                ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
                ->where('tmuk_kode', $tmuk_kode)
                ->where('delete', 0)
                ->whereBetween('tanggal', $tanggal)
                ->orderBy('tanggal');

                $data = [
                    'detail'     => $records->get(),
                    'start'      => Carbon::createFromFormat('d/m/Y', $start)->format('Y/m/d'),
                    'end'        => Carbon::createFromFormat('d/m/Y', $end)->format('Y/m/d'),
                ];
            }
            break;
            case 'arus-kas-per-akun':
            if($request->start){
                $tanggal = $request->start;
                $tgl  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');

                $records = Coa::with(['jurnal' => function($q) use ($tmuk_kode, $tgl){
                   return $q->where('tmuk_kode', $tmuk_kode)->where('delete', 0)->where(\DB::raw('date(tanggal)'),$tgl);
               }])
                ->whereIn('kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])->orderBy('kode');

                $data = [
                    'detail' => $records->get(),
                    'start'  => Carbon::createFromFormat('d/m/Y', $tanggal)->format('Y/m/d'),
                ];
            }
            break;
            case 'mutasi-escrow':
            if($request->start){
                $saldo = 0;
                $start = Carbon::createFromFormat('d/m/Y',$request->start)->format('Y-m-d');
                $end = Carbon::createFromFormat('d/m/Y',$request->end)->format('Y-m-d');
                $tgl_awal = Carbon::parse($tgl_opening_tmuk)->format('Y-m-d');
                $tgl_kemarin = Carbon::createFromFormat('d/m/Y', $request->start)->subDay(1);
                $tgl_akhir = Carbon::parse($tgl_kemarin)->format('Y-m-d');
                $saldo  = TransJurnal::saldoAwalKemarin($tmuk_kode, '1.1.2.1', $tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59');

                $records = TransJurnal::whereBetween('tanggal', $tanggal)
                ->where('tmuk_kode',$tmuk_kode)
                ->whereIn('coa_kode',['1.1.2.1'])
                ->where('delete', 0)
                ->orderBy('tanggal','asc');

                $data = [
                    'detail' => $records->get(),
                    'start'  => $start,
                    'end'  => $end,
                    'saldo'  => $saldo,
                ];
            }
            break;
            default:
            break;
        }
        return view('modules.laporan.laporan.lists.akun-kas-bank.'.$request->type, $data);
    }

    public function printAkunKasBank(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;
        $tgl_opening_tmuk = $tmuk->created_at;
        $data  = [];

        switch ($request->laporan) {
            case 'akun-kas-bank':
            if($request->start and $request->end){
                $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                $tanggal = [$start,$end];
            }

            $records = TransJurnal::with('coa')
            ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
            ->where('tmuk_kode', $tmuk_kode)
            ->where('delete', 0)
            ->whereBetween(\DB::raw('tanggal'), $tanggal)
            ->orderBy('tanggal');

            $req            = $request->all();
            $tahun_fiskal   = TahunFiskal::getTahunFiskal();
            $region         = $tmuk->lsi->region->area;
            $lsi            = $tmuk->lsi->nama;
            $tmuk           = $tmuk->nama;

            $data = [
                'record'   => $records->get(),
                'fiskal'   => $tahun_fiskal,
                'request'  => $req,
                'region'   => $region,
                'lsi'      => $lsi,
                'tmuk'     => $tmuk,
                'label_file' => 'LAPORAN JURNAL KAS & AKUN '.$tmuk_kode.' '.$start.' s/d '.$end,
            ];
            break;
            case 'arus-kas-per-akun':
            // $tanggal = $request->start;
            $tanggal = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $records = Coa::with(['jurnal' => function($q) use ($tmuk_kode, $tanggal){
               return $q->where('tmuk_kode', $tmuk_kode)->where('delete', 0)->where(\DB::raw('DATE(tanggal)'),$tanggal);
           }])
            ->whereIn('kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])->orderBy('kode');

            $req            = $request->all();
            $tahun_fiskal   = TahunFiskal::getTahunFiskal();
            $region         = $tmuk->lsi->region->area;
            $lsi            = $tmuk->lsi->nama;
            $tmuk           = $tmuk->nama;
            $tgl            = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');

            $data = [
                'record'   => $records->get(),
                'fiskal'   => $tahun_fiskal,
                'request'  => $req,
                'region'   => $region,
                'lsi'      => $lsi,
                'tmuk'     => $tmuk,
                'label_file' => 'LAPORAN ARUS KAS PER AKUN '.$tmuk_kode.' '.$tgl,
            ];
            break;
            case 'mutasi-escrow':
            $saldo = 0;
            $start = Carbon::createFromFormat('d/m/Y',$request->start)->format('Y-m-d');
            $end = Carbon::createFromFormat('d/m/Y',$request->end)->format('Y-m-d');


            $tgl_awal = Carbon::parse($tgl_opening_tmuk)->format('Y-m-d');
            $tgl_kemarin = Carbon::createFromFormat('d/m/Y', $request->start)->subDay(1);
            $tgl_akhir = Carbon::parse($tgl_kemarin)->format('Y-m-d');

            $saldo  = TransJurnal::saldoAwalKemarin($tmuk_kode, '1.1.2.1', $tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59');


            $records = TransJurnal::whereBetween(\DB::raw('DATE(tanggal)'),array($start,$end))
            ->where('tmuk_kode',$tmuk_kode)
            ->where('delete', 0)
            ->whereIn('coa_kode',['1.1.2.1'])
            ->orderBy('tanggal','asc');

            $req            = $request->all();
            $tahun_fiskal   = TahunFiskal::getTahunFiskal();
            $region         = $tmuk->lsi->region->area;
            $lsi            = $tmuk->lsi->nama;
            $tmuk           = $tmuk->nama;

            $data = [
                'record'  => $records->get(),
                'fiskal'  => $tahun_fiskal,
                'request' => $req,
                'region'  => $region,
                'lsi'     => $lsi,
                'tmuk'    => $tmuk,
                'saldo'   => $saldo,
                'label_file' => 'LAPORAN MUTASi ESCROW '.$tmuk_kode.' '.$start.' s/d '.$end,
            ];
            break;
            default:

            break;
        }

        // $content = view('report.laporan.akun-kas-bank.'.$request->laporan, $data);

        // HTML2PDF($content, $data);
        $pdf = \PDF::loadView('report.laporan.akun-kas-bank.'.$request->laporan, $data);
            return $pdf->setOption('margin-bottom', 5)
            ->setOption('margin-top', 5)
            ->setOption('margin-left', 3)
            ->setOption('margin-right', 3)
            ->inline();
    }

    public function exelAkunKasBank(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;
        $data  = [];

        $request = $request;

        switch ($request->laporan) {
            case 'akun-kas-bank':
            $this->exelKasBank($tmuk_kode, $request);
            break;
            case 'arus-kas-per-akun':
            $this->exelArusKasPerAkun($tmuk_kode, $request);
            break;
            case 'mutasi-escrow':
            $this->exelMutasiEscrow($tmuk_kode, $request);
            break;
            default:

            break;
        }
    }


    public function exelKasBank($tmuk_kode, $request)
    {
        $records = TransJurnal::select('*')
        ->whereIn('coa_kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])
        ->where('tmuk_kode', $tmuk_kode)
        ->where('delete', 0)
        ->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end))
        ->orderBy('tanggal');
        $record = $records->get();

        $Export = Excel::create('Jurnal Kas & Bank ', function($excel) use ($record, $request){
            $excel->setTitle('Jurnal Kas & Bank');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Jurnal Kas & Bank ', function($sheet) use ($record, $request){
                $sheet->row(1, array(
                    'Jurnal Kas & Bank '
                ));

                $sheet->row(2, array(
                    'Periode : ' . $request->start . ' - '. $request->end
                ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal',
                    'Nama Akun',
                    'Saldo Awal',
                    'Perubahan Debit',
                    'Perubahan Kredit',
                    'Perubahan Bersih',
                    'Saldo Akhir',
                    'Keterangan'
                ));

                $row = 4;
                $no =1;
                $saldo_awal = [];
                $saldo_akhir = [];

                foreach ($record as $val) {


                    if ($no == 1) {
                        $saldo_awal[$no] = 0;
                    }else{
                        $saldo_awal[$no] = $saldo_akhir[$no-1];
                    }

                    $jumlah_debit = $val->posisi=='D' ? $val->jumlah : '0';

                    $jumlah_kredit = $val->posisi=='K' ? $val->jumlah : '0';

                    $posisi_normal = $val->coa->posisi;

                    if ($posisi_normal == 'D') {
                        $bersih = $jumlah_debit - $jumlah_kredit;
                    }else{
                        $bersih = $jumlah_kredit - $jumlah_debit;                                        
                    }

                    $saldo_akhir[$no] = $saldo_awal[$no] + $bersih;


                    $sheet->row($row, array(
                        $no,
                        Carbon::parse($val->tanggal)->format('d/m/Y'),
                        $val->coa->nama,
                        $saldo_awal[$no],
                        $jumlah_debit,
                        $jumlah_kredit,
                        $bersih,
                        $saldo_akhir[$no],
                        $val->deskripsi,
                    ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->setBorder('A3:I'.$row, 'thin');
                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:I2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:I3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>30, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20, 'H'=>20, 'I'=>35));
                $sheet->getStyle('B4:I'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:I'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelArusKasPerAkun($tmuk_kode, $request)
    {
        // $tanggal = $request->start;
        $tanggal = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
        $records = Coa::with(['jurnal' => function($q) use ($tmuk_kode, $tanggal){
           return $q->where('tmuk_kode', $tmuk_kode)->where('delete', 0)->where(\DB::raw('DATE(tanggal)'),$tanggal);
       }])
        ->whereIn('kode', ['1.1.1.0','1.1.1.1','1.1.1.2','1.1.2.0','1.1.2.1','1.1.2.2','1.1.2.3'])->orderBy('kode');
        $record = $records->get();
        
        $Export = Excel::create('Daftar Arus Kas per Akun  ', function($excel) use ($record, $request){
            $excel->setTitle('Daftar Arus Kas per Akun ');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Daftar Arus Kas per Akun  ', function($sheet) use ($record, $request){
                $sheet->row(1, array(
                    'Daftar Arus Kas per Akun  '
                ));

                $sheet->row(2, array(
                    'Periode : ' . $request->start
                ));
                $sheet->row(3, array(
                    'No',
                    'Nama Akun',
                    'Saldo'
                ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->nama,
                        $val->jurnal->sum('jumlah'),
                    ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':C'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:C'.$row, 'thin');
                $sheet->mergeCells('A1:C1');
                $sheet->mergeCells('A2:C2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:C2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:C3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>30));
                $sheet->getStyle('B4:C'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:C'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelMutasiEscrow($tmuk_kode, $request)
    {
        $saldo = 0;
        $start = Carbon::createFromFormat('d/m/Y',$request->start)->format('Y-m-d');
        $end = Carbon::createFromFormat('d/m/Y',$request->end)->format('Y-m-d');

        if($request->start and $request->end){
            $start  = \Carbon\Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = \Carbon\Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start.' 00:00:00',$end.' 23:59:59'];
        }

        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $tmuk_kode = $tmuk->kode;
        $tgl_opening_tmuk = $tmuk->created_at;

        $tgl_awal = Carbon::parse($tgl_opening_tmuk)->format('Y-m-d');
        $tgl_kemarin = Carbon::createFromFormat('d/m/Y', $request->start)->subDay(1);
        $tgl_akhir = Carbon::parse($tgl_kemarin)->format('Y-m-d');
        $saldo  = TransJurnal::saldoAwalKemarin($tmuk_kode, '1.1.2.1', $tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59');

        $records = TransJurnal::whereBetween('tanggal', $tanggal)
        ->where('tmuk_kode',$tmuk_kode)
        ->whereIn('coa_kode',['1.1.2.1'])
        ->where('delete', 0)
        ->orderBy('tanggal','asc');

        $record = $records->get();


        
        $Export = Excel::create('Mutasi Escrow  ', function($excel) use ($record, $start, $end, $saldo){
            $excel->setTitle('Mutasi Escrow ');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('Mutasi Escrow  ', function($sheet) use ($record, $start, $end, $saldo){
                $sheet->row(1, array(
                    'Mutasi Escrow  '
                ));

                $sheet->row(2, array(
                    'Periode : ' . $start . ' - ' . $end
                ));
                $sheet->row(3, array(
                    'No',
                    'Tanggal',
                    'Keterangan',
                    'Debet',
                    'Credit',
                    'Ledger'
                ));

                $row = 4;
                $no =1;

                $kredit=0;
                $debit=0;
                $awal=$saldo;
                $tanggal=[];
                $deskripsi=[];
                $debet=[];
                $credit=[];
                $ledger=[];
                foreach ($record as $val){

                    array_push($tanggal,Carbon::createFromFormat('Y-m-d H:i:s',$val->tanggal)->format('d F Y'));
                    if($val->reduce){
                        array_push($deskripsi,$val->reduce->po_nomor);
                    }else{
                        $strings = '';
                        if($val->pyr){
                            if($val->pyr->vendor_lokal == 0){
                                $tmuk = Tmuk::with('lsi')->where('kode', $val->pyr->tmuk->kode)->first();
                                $strings = ' - '.$tmuk->lsi->nama;
                            }else if($val->pyr->vendor_lokal == 100){
                                $strings = ' - '.$val->pyr->tmuk->nama;
                            }else{
                                $vendor = VendorLokalTmuk::where('id', $val->pyr->vendor_lokal)->first();
                                $strings = ' - '.isset($vendor) ? $vendor->nama : '-';
                            }
                        }
                        $desk = $val->deskripsi.' '.$strings;
                        array_push($deskripsi,$desk);
                    }
                    array_push($debet,$val->posisi=='D' ? $val->jumlah : '');
                    array_push($credit,$val->posisi=='K' ? $val->jumlah : '');

                    if($val->posisi=='D'){
                        $debit+=$val->jumlah;
                    }else{
                        $kredit+=$val->jumlah;
                    }

                    if($val->coa->posisi=='D'){
                        $saldo = ($debit - $kredit) + $awal;

                    }else{
                        $saldo = ($kredit - $debit) + $awal;
                    }
                    array_push($ledger,$saldo);
                }

                krsort($tanggal);
                krsort($deskripsi);
                krsort($debet);
                krsort($credit);
                krsort($ledger);

                foreach ($tanggal as $key => $r){
                    $sheet->row($row, array(
                        $no,
                        $tanggal[$key],
                        $deskripsi[$key],
                        $debet[$key],
                        $credit[$key], 
                        $ledger[$key],
                    ));
                    $row=$row;
                    $row++;$no++;
                }

                $sheet->setBorder('A3:F'.$row, 'thin');
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('A2:F2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:F2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:F3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>15,'C'=>30, 'D'=>20, 'E'=>20, 'F'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );
                $sheet->getStyle('B4:B'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

                $row = $row+2;

                $sheet->row($row, array(
                    '',
                    '',
                    'OPENING BALANCE',
                    'TOTAL DEBET',
                    'TOTAL CREDIT', 
                    'CLOSING BALANCE',
                ));

                $row = $row+1;
                $sheet->row($row, array(
                    '',
                    '',
                    $awal,
                    $debit,
                    $kredit, 
                    $saldo,
                ));

                $mulai = $row-1;

                $sheet->setBorder('C'.$mulai.':F'.$row, 'thin');
                $sheet->cells('C'.$mulai.':F'.$mulai, function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
            });
});
$Export ->download('xls');
}





}
