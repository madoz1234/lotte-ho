<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransAkuisisiAset;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Region;

class AktivaTetapController extends Controller
{
    protected $link = 'laporan/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan");

    }
    public function grid(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $start = '';
        $end   = '';
        $tmuk_kode = $tmuk->kode;
        $data  = [];

        if($request->start and $request->end){
            $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
            $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
            $tanggal = [$start,$end];
        }
        switch ($request->type) {
            case 'aktiva-tetapsss':
                    if($request->start and $request->end){
                        $records = TransAkuisisiAset::select('*')
                                    ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                    ->whereBetween(\DB::raw('created_at'), array($request->start, $request->end));

                        $data = [
                            'detail' => $records->get(),
                            'start'  => $start,
                            'end'    => $end,
                        ];
                    }
                break;
            case 'aktiva-tetap':
                    if($request->start){
                        // $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                        $start   = Carbon::createFromFormat('m/Y', $request->start)->endOfMonth()->format('Y-m-d');
                        
                        $records = TransAkuisisiAset::select(\DB::raw('tmuk_kode,tipe_id,sum(nilai_pembelian) as jumlah'))
                                    ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                    ->where(\DB::raw('tanggal_pembelian'),'<=',$start)
                                    ->groupBy('tipe_id','tmuk_kode')
                                    ->orderBy('tipe_id','asc');

                        $datas = TransAkuisisiAset::where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                    ->where(\DB::raw('created_at'),'<=',$start)
                                    ->orderBy('tipe_id','asc');

                        $data = [
                            'aset' => $records->get(),
                            'data' => $datas->get(),
                            'start'  => $start,
                        ];
                    }
                break;
            case 'aset-listing':
                    if($request->start){
                        $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');

                        $records = TransAkuisisiAset::where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                    ->where('status', 1)
                                    ->where(\DB::raw('tanggal_pembelian'),'<=',$start)
                                    ->orderBy('tanggal_pembelian','desc');

                        $data = [
                            'aset' => $records->get(),
                            'start'  => $start,
                        ];
                    }
                break;
            case 'aktiva-tetap-per-tipe':
                    if($request->start and $request->end){
                        $records = TransJurnal::select('*')->whereIn('coa_kode', ['1.2.1.0','1.2.2.0','1.2.3.0','1.2.4.0','1.2.5.0','1.2.6.0','1.2.7.0'])
                                        ->where('tmuk_kode', $tmuk_kode)
                                        ->where('delete', 0)
                                        ->whereBetween(\DB::raw('tanggal'), $tanggal);

                        $data = [
                            'detail' => $records->get(),
                            'start'  => $start,
                            'end'    => $end,
                        ];
                    }
                break;
            default:
                break;
        }
        /*return views*/
        return view('modules.laporan.laporan.lists.aktiva-tetap.'.$request->type, $data);
    }

    //  PRINT LAPORAN
    //  PRINT LAPORAN PDF
    //  PEMBELIAN
    public function printAktivaTetap(Request $request){
        // dd($request->all());
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $record = [];
        $data   = [];
        $temp   = [];
        $tmuk_kode = $tmuk->kode;

        switch ($request->laporan) {
            case 'aktiva-tetap':
                    if($request->start==true){
                        $start   = Carbon::createFromFormat('m/Y', $request->start)->endOfMonth()->format('Y-m-d');
                        
                        $records = TransAkuisisiAset::select(\DB::raw('tmuk_kode,tipe_id,sum(nilai_pembelian) as jumlah'))
                                    ->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                    ->where(\DB::raw('tanggal_pembelian'),'<=',$start)
                                    ->groupBy('tipe_id','tmuk_kode')
                                    ->orderBy('tipe_id','asc')->get();

                        $req            = $request->all();
                        $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                        $region         = $tmuk->lsi->region->area;
                        $lsi            = $tmuk->lsi->nama;
                        $tmuk           = $tmuk->nama;

                        $content = view('report.laporan.aktiva-tetap.'.$request->laporan, [
                        'request'   => $req,
                        'fiskal'    => $tahun_fiskal,
                        'region'    => $region,
                        'lsi'       => $lsi,
                        'tmuk'      => $tmuk,
                        'aset'      => $records,
                        'start'     => $start,
                        ]);

                        $data = [
                            'oriented'   => 'L',
                            'paper'      => 'A4',
                            'label_file' => 'Daftar Akiva Tetap',
                        ];

                        // return $content;

                        HTML2PDF($content, $data);
                    }

                break;
            case 'aktiva-tetap-per-tipe':
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                    $tanggal = [$start,$end];

                    $records = TransJurnal::select('*')->whereIn('coa_kode', ['1.2.1.0','1.2.2.0','1.2.3.0','1.2.4.0','1.2.5.0','1.2.6.0','1.2.7.0']);
                // dd($records);
                    if ($tmuk_kode = $tmuk_kode) {
                        $records->where('tmuk_kode', $tmuk_kode);
                    }
                    if($request->start==true and $request->end==true){
                        $start = $request->start;
                        $end   = $request->end;
                        $records->whereBetween(\DB::raw('tanggal'), $tanggal);
                    }

                    $record         = $records->get();
                    $req            = $request->all();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                    // dd($region);

                    $data = [
                    'records'   => $record,
                    'request'   => $req,
                    'fiskal'    => $tahun_fiskal,
                    'region'    => $region,
                    'lsi'       => $lsi,
                    'tmuk'      => $tmuk,
                    ];
                break;
            case 'aset-listing':
                    $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');

                    $records = TransAkuisisiAset::where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                                ->where('status', 1)
                                ->where(\DB::raw('tanggal_pembelian'),'<=',$start)
                                ->orderBy('tanggal_pembelian','desc');

                    $record         = $records->get();
                    $req            = $request->all();
                    $tahun_fiskal   = TahunFiskal::getTahunFiskal();
                    $region         = $tmuk->lsi->region->area;
                    $lsi            = $tmuk->lsi->nama;
                    $tmuk           = $tmuk->nama;

                    // dd($region);

                    $data = [
                    'records'   => $record,
                    'request'   => $req,
                    'fiskal'    => $tahun_fiskal,
                    'region'    => $region,
                    'lsi'       => $lsi,
                    'tmuk'      => $tmuk,
                    ];
                break;
            default:

                break;
        }

        $content = view('report.laporan.aktiva-tetap.'.$request->laporan, $data);

        // return $content;
        HTML2PDF($content, $data);
    }
    // END LAPORAN PEMBELIAN



    // PRINT LAPORAN EXEL
    //PEMBELIAN
    public function exelAktivaTetap(Request $request){
        $data = $request->all();
        $tmuk = Tmuk::find($data['tmuk_kode']);

        $record = [];
        $data   = [];
        $temp   = [];
        $tmuk_kode = $tmuk->kode;

        switch ($request->laporan) {
            case 'aktiva-tetap':
                    $tanggal   = Carbon::createFromFormat('m/Y', $request->start)->endOfMonth()->format('Y-m-d');
                    // dd($tanggal);
                    $this->exelAktifaTetap($tmuk_kode, $tanggal);
                break;
            case 'aktiva-tetap-per-tipe':
            if($request->start and $request->end){
                $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
                $tanggal = [$start,$end];
            }
                    $this->exelAktifaTetapperAkun($tmuk_kode, $tanggal);
                break;
            case 'aset-listing':
            $tanggal = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                    $this->exelAsetListing($tmuk_kode, $tanggal);
                break;
            default:

                break;
        }
    }

    public function exelAktifaTetap($tmuk_kode, $tanggal)
    {

        $records = TransAkuisisiAset::from(\DB::raw("(SELECT
                  trans_akuisisi_aset.tmuk_kode,
                  trans_akuisisi_aset.nama,
                  trans_akuisisi_aset.tanggal_pembelian,
                  trans_akuisisi_aset.nilai_pembelian,
                  ref_tipe_aset.tingkat_depresiasi / 12 AS umur_ekonomis,
                  trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi AS penyusutan_perbulan,
                  trans_akuisisi_aset.nilai_pembelian - (trans_akuisisi_aset.nilai_pembelian / ref_tipe_aset.tingkat_depresiasi) AS nilai_buku
                  FROM trans_akuisisi_aset
                  JOIN ref_tipe_aset ON trans_akuisisi_aset.tipe_id = ref_tipe_aset.id)  d"))->where(\DB::raw('tanggal_pembelian'),'<=',$tanggal)->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        $record = $records->get();

        // $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $tanggal = Carbon::createFromFormat('Y-m-d', $tanggal)->format('F Y');

        $Export = Excel::create('AKTIVA TETAP ', function($excel) use ($record, $tanggal){
            $excel->setTitle('AKTIVA TETAP');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('AKTIVA TETAP ', function($sheet) use ($record, $tanggal){
                $sheet->row(1, array(
                    'AKTIVA TETAP '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$tanggal
                    ));
                $sheet->row(3, array(
                    'No',
                    'TMUK',
                    'Keterangan',
                    'Tanggal Perolehan',
                    'Unit',
                    'Harga',
                    'Jumlah',
                    'Umur Ekonomis',
                    'Penyusutan perBulan',
                    'Nilai Buku'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->nama,
                        $val->nama,
                        $val->tanggal_pembelian,
                        '1',
                        $val->nilai_pembelian,
                        $val->nilai_pembelian,
                        $val->umur_ekonomis,
                        $val->penyusutan_perbulan,
                        $val->nilai_buku,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:J'.$row, 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:J3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20,'D'=>50,'E'=>30,'F'=>20, 'G'=>20, 'H'=>20, 'I'=>20, 'J'=>20));
                $sheet->getStyle('B4:J'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:J'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }


    public function exelAktifaTetapperAkun($tmuk_kode, $tanggal){

        // dd($tmuk_kode);
        $records = TransJurnal::select('*')->whereIn('coa_kode', ['1.2.1.0','1.2.2.0','1.2.3.0','1.2.4.0','1.2.5.0','1.2.6.0','1.2.7.0'])
                                    ->where('tmuk_kode', $tmuk_kode)
                                    ->whereBetween(\DB::raw('tanggal'), $tanggal);

        $record = $records->get();

        $start = Carbon::createFromFormat('Y-m-d', $tanggal[0])->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $tanggal[1])->format('d/m/Y');

        $Export = Excel::create('AKTIVA TETAP PER TIPE ', function($excel) use ($record, $start, $end){
            $excel->setTitle('AKTIVA TETAP PER TIPE');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('AKTIVA TETAP PER TIPE ', function($sheet) use ($record, $start, $end){
                $sheet->row(1, array(
                    'AKTIVA TETAP PER TIPE '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$start.' - '.$end
                    ));
                $sheet->row(3, array(
                    'No',
                    'Nama Aktiva',
                    'Harga Perolehan',
                    'Akumulasi Depresiasi',
                    'Book Value',
                    'Depresiasi Tahun Ini',
                    'Tanggal Pembelian'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->coa->nama,
                        $val->jumlah,
                        '',
                        '',
                        '',
                        $val->tanggal,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':G'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:G2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20, 'D'=>20, 'E'=>20, 'F'=>20, 'G'=>20));
                $sheet->getStyle('B4:F'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:F'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }

    public function exelAsetListing($tmuk_kode, $tanggal){
        // dd($tanggal);
        // $tanggal  = Carbon::createFromFormat('d/m/Y', $request->start.' 00:00:00')->format('Y-m-d');

        $records = TransAkuisisiAset::where('tmuk_kode', 'like', '%' . $tmuk_kode . '%')
                    ->where('status', 1)
                    ->where(\DB::raw('tanggal_pembelian'),'<=',$tanggal)
                    ->orderBy('tanggal_pembelian','desc');

        $record = $records->get();

        $Export = Excel::create('ASSET LISTING ', function($excel) use ($record, $tanggal){
            $excel->setTitle('ASSET LISTING');
            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
            $excel->setDescription('Export');

            $excel->sheet('ASSET LISTING ', function($sheet) use ($record, $tanggal){
                $sheet->row(1, array(
                    'ASSET LISTING '
                    ));

                $sheet->row(2, array(
                    'Periode : '.$tanggal
                    ));
                $sheet->row(3, array(
                    'No',
                    'TMUK',
                    'Tipe Aset',
                    'Nama Aset',
                    'Nomor Seri',
                    'Tanggal Pembelian',
                    'Nilai Pembelian'
                    ));

                $row = 4;
                $no =1;
                foreach ($record as $val) {
                    $sheet->row($row, array(
                        $no,
                        $val->tmuk->nama,
                        $val->tipeaset->tipe,
                        $val->nama,
                        $val->no_seri,
                        $val->tanggal_pembelian,
                        $val->nilai_pembelian,
                        ));
                    $row=$row;
                    $row++;$no++;
                }
                $sheet->cells('E4'.':F'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('A3:G'.$row, 'thin');
                $sheet->mergeCells('A1:G1');
                $sheet->mergeCells('A2:G2');
                $sheet->cells('A1:A1', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->cells('A2:B2', function($cells){
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                });
                $sheet->cells('A3:G3', function($cells){
                    $cells->setAlignment('center');
                    $cells->setBackground('#999999');
                    $cells->setFontColor('#FFFFFF');
                });
                $sheet->setWidth(array('A'=>6,'B'=>20,'C'=>20,'D'=>50,'E'=>30,'F'=>20, 'G'=>20));
                $sheet->getStyle('B4:G'.$row)->getAlignment()->setWrapText(true);
                $sheet->getStyle('B4:G'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top')
                );
                $sheet->getStyle('A4:A'.$row)->getAlignment()->applyFromArray(
                    array('vertical' => 'top', 'horizontal' => 'center')
                );

            });
        });
        $Export ->download('xls');
    }






}
