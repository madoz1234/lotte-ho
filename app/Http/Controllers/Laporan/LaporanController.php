<?php

namespace Lotte\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransPyr;
// use Lotte\Models\Trans\TransPoDetail;
// use Lotte\Models\Master\ProdukLsi;

class LaporanController extends Controller
{
    protected $link = 'laporan/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan TMUK");
        $this->setSubtitle("&nbsp;");
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Tmuk' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'fiscal_mulai',
                'name' => 'fiscal_mulai',
                'label' => 'Mulai Tahun Fiskal',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'fiscal_selesai',
                'name' => 'fiscal_selesai',
                'label' => 'Selesai Tahun Fiskal',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => false,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ]
        ]);
    }

    public function index()
    {
      
        return $this->render('modules.laporan.laporan.index');
    }

    public function create()
    {
        return $this->render('modules.laporan.laporan.create');
    }

    public function html2pdf(){
            $content = view('report.purchase-order');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Picking',
            ];

            HTML2PDF($content, $data);
    }


    public function edit($id)
    {
        return $this->render('modules.laporan.laporan.edit');
    }

    public function grid(Request $request)
    {
        // dd($request->all());
        $start = '';
        $end   = '';
        switch ($request->type) {
            // PEMBELIAN
            // case 'lembar-po-stdk':
            //         $records = TransPo::select('*')->where('status', 2);
            //         if ($tmuk_kode = $request->tmuk_kode) {
            //             $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
            //         }
            //         if($request->start==true and $request->end==true){
            //             $start = $request->start;
            //             $end   = $request->end;
            //             $records->whereBetween(\DB::raw('tgl_buat'), array($request->start, $request->end));
            //         }

            //         $records = $records->get();
            //         // dd($records->toArray());
            //         $data = [
            //             'detail' => $records,
            //             'start'  => $start,
            //             'end'    => $end,
            //         ];
            //         return view('modules.laporan.laporan.lists.pembelian.po-stdk', $data);
            //     break;
            //     case 'lembar-po-btdk':
            //         $records = TransPo::select('*')->where('status', 1);
            //         if ($tmuk_kode = $request->tmuk_kode) {
            //             $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
            //         }
            //         if($request->start==true and $request->end==true){
            //             $start = $request->start;
            //             $end   = $request->end;
            //             $records->whereBetween(\DB::raw('tgl_buat'), array($request->start, $request->end));
            //         }
            //         $records = $records->get();
            //         // dd($records->toArray());
            //         $data = [
            //             'detail' => $records,
            //             'start' => $start,
            //             'end' => $end,
            //         ];
            //         return view('modules.laporan.laporan.lists.pembelian.po-btdk', $data);
            //     break;
            // case 'po-barang-trade-ke-lsi':
            //         $records = TransPyr::with('creator')
            //                 ->where('tipe', 001)
            //                 ->select('*');

            //         if ($tmuk_kode = $request->tmuk_kode) {
            //             $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
            //         }
            //         if($request->start and $request->end){
            //             $start = $request->start;
            //             $end   = $request->end;
            //             $records->whereBetween(\DB::raw('tgl_buat'), array($request->start, $request->end));
            //         }
            //         $records = $records->get();

            //         $data = [
            //             'detail' => $records,
            //             'start'  => $start,
            //             'end'    => $end,
            //         ];
            //         return view('modules.laporan.laporan.lists.pembelian.po-barang-trade-ke-lsi', $data);
            //             break;
            // case 'po-barang-non-trade-ke-lsi':
            //         $records = TransPyr::with('creator') 
            //                 // ->where('tipe', 003)
            //                 ->select('*');

            //         if ($tmuk_kode = $request->tmuk_kode) {
            //             $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
            //         }

            //         $records = $records->get();

            //         // dd($records);
            //         $data = [
            //             'detail' => $records,
            //         ];
            //         return view('modules.laporan.laporan.lists.pembelian.po-barang-non-trade-ke-lsi', $data);
            //     break;

            // END PEMBELIAN


                
            // case 'ps':
            //         // Picking Result
            //         // dd($request->all());
            //         $records = TransPr::with('creator')->where('status', 1)
            //              ->select('*');

            //         if ($tmuk_kode = $request->tmuk_kode) {
            //             $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
            //         } 

            //         // $awal   = $request->fiscal_mulai;
            //         // $akhir  = $request->fiscal_selesai;

            //         $records = $records->get();

            //         $data = [
            //             'detail' => $records,
            //         ];

            //         // dd($data->all());
            //         return view('modules.laporan.laporan.lists.pembelian.ps', $data);
            //     break;

            // case 'ldkt':
            //         return view('modules.laporan.laporan.lists.pembelian.ldkt');
            //     break;
            // case 'pembelian-persediaan':
            //         return view('modules.laporan.laporan.lists.pembelian.pembelian-persediaan');
            //     break;
            // // End pembelian
            // case 'umur-persediaan':
            //         return view('modules.laporan.laporan.lists.persediaan.umur-persediaan');
            //     break;
            // case 'nilai-persediaan':
            //         return view('modules.laporan.laporan.lists.persediaan.nilai-persediaan');
            //     break;
            // case 'pergerakan-persediaan':
            //         return view('modules.laporan.laporan.lists.persediaan.pergerakan-persediaan');
            //     break;
            // case 'stok-opname':
            //         return view('modules.laporan.laporan.lists.persediaan.stok-opname');
            //     break;
            // case 'hpp-map':
            //         return view('modules.laporan.laporan.lists.persediaan.hpp-map');
            //     break;
            // case 'penjualan-harian':
            //         return view('modules.laporan.laporan.lists.penjualan.penjualan-harian');
            //     break;
            // case 'penjualan-mingguan':
            //         return view('modules.laporan.laporan.lists.penjualan.penjualan-mingguan');
            //     break;
            // case 'penjualan-bulanan':
            //         return view('modules.laporan.laporan.lists.penjualan.penjualan-bulanan');
            //     break;
            // case 'penjualan-per-barang':
            //         return view('modules.laporan.laporan.lists.penjualan.penjualan-per-barang');
            //     break;
            // case 'lap01':
            //         return view('modules.laporan.laporan.lists.laporan-kantor-pusat.lap01');
            //     break;
            // case 'neraca':
            //         return view('modules.laporan.laporan.lists.keuangan.neraca');
            //     break;
            // case 'neraca-perbandingan-anggaran':
            //         return view('modules.laporan.laporan.lists.keuangan.neraca-perbandingan-anggaran');
            //     break;
            // case 'laba-rugi':
            //         $records = TransJurnal::with('creator')
            //              ->select('*');

            //         if ($tmuk_kode = $request->tmuk_kode) {
            //             $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
            //         }

            //         $records = $records->get();

            //         // dd($records);

            //         $data = [
            //             'detail' => $records,
            //         ];
            //         return view('modules.laporan.laporan.lists.keuangan.laba-rugi', $data);
            //     break;
            // case 'laba-rugi-perbandingan-anggaran':
            //         return view('modules.laporan.laporan.lists.keuangan.laba-rugi-perbandingan-anggaran');
            //     break;
            // case 'laporan-laba-ditahan':
            //         return view('modules.laporan.laporan.lists.keuangan.laporan-laba-ditahan');
            //     break;
            // case 'fokus-keuangan':
            //         return view('modules.laporan.laporan.lists.keuangan.fokus-keuangan');
            //     break;
            // case 'daftar-history-gl':
            //         return view('modules.laporan.laporan.lists.akun-buku-besar.daftar-history-gl');
            //     break;
            // case 'keseluruhan-jurnal':
            //         return view('modules.laporan.laporan.lists.akun-buku-besar.keseluruhan-jurnal');
            //     break;
            // case 'ringkasan-buku-besar':
            //         return view('modules.laporan.laporan.lists.akun-buku-besar.ringkasan-buku-besar');
            //     break;
            // case 'neraca-saldo':
            //         return view('modules.laporan.laporan.lists.akun-buku-besar.neraca-saldo');
            //     break;
            // case 'daftar-akun':
            //         return view('modules.laporan.laporan.lists.akun-buku-besar.daftar-akun');
            //     break;
            // case 'bukti-jurnal-umum':
            //         return view('modules.laporan.laporan.lists.akun-buku-besar.bukti-jurnal-umum');
            //     break;
            // case 'akun-kas-bank':
            //         return view('modules.laporan.laporan.lists.akun-kas-bank.akun-kas-bank');
            //     break;
            // case 'arus-kas-per-akun':
            //         return view('modules.laporan.laporan.lists.akun-kas-bank.arus-kas-per-akun');
            //     break;
            // case 'transaksi-belum-lunas':
            //         return view('modules.laporan.laporan.lists.akun-piutang-member.transaksi-belum-lunas');
            //     break;
            // case 'piutang-member':
            //         return view('modules.laporan.laporan.lists.akun-piutang-member.piutang-member');
            //     break;
            // case 'umur-piutang-member':
            //         return view('modules.laporan.laporan.lists.akun-piutang-member.umur-piutang-member');
            //     break;
            // case 'pembayaran-member':
            //         return view('modules.laporan.laporan.lists.akun-piutang-member.pembayaran-member');
            //     break;
            // case 'daftar-member':
            //         return view('modules.laporan.laporan.lists.akun-piutang-member.daftar-member');
            //     break;
            // case 'aktiva-tetap':
            //         return view('modules.laporan.laporan.lists.aktiva-tetap.aktiva-tetap');
            //     break;
            // case 'aktiva-tetap-per-tipe':
            //     break;

            default:
                    return view('modules.laporan.laporan.lists.aktiva-tetap.aktiva-tetap-per-tipe');
                    return view('modules.laporan.laporan.lists.divisi');
                break;
        }
    }

    //PRINT LAPORAN
    

    //keuangan ==================================================================================================
    public function printNeraca()
    {
            $content = view('report.laporan.keuangan.neraca');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printNeracaPerbandingan()
    {
            $content = view('report.laporan.keuangan.neraca-perbandingan-anggaran');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printLabaRugi()
    {
            $content = view('report.laporan.keuangan.laba-rugi');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printLamaRugiPerbandinganAnggaran()
    {
            $content = view('report.laporan.keuangan.laba-rugi-perbandingan-anggaran');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printLabaDitahan()
    {
            $content = view('report.laporan.keuangan.laba-ditahan');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }


    //akun buku besar ============================================================================
    public function printBuktiJurnalUmum()
    {
            $content = view('report.laporan.akun-buku-besar.bukti-jurnal-umum');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printDaftarAkun()
    {
            $content = view('report.laporan.akun-buku-besar.daftar-akun');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printDaftarHistoryGl()
    {
            $content = view('report.laporan.akun-buku-besar.daftar-history-gl');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printKeseluruhanJurnal()
    {
            $content = view('report.laporan.akun-buku-besar.keseluruhan-jurnal');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printNeracaSaldo()
    {
            $content = view('report.laporan.akun-buku-besar.neraca-saldo');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printRingkasanBukuBesar()
    {
            $content = view('report.laporan.akun-buku-besar.ringkasan-buku-besar');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }


    //akun kas & bank ========================================================================================
    public function printAkunKasBank()
    {
            $content = view('report.laporan.akun-kas-bank.akun-kas-bank');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }
    public function printArusKasPerAkun()
    {
            $content = view('report.laporan.akun-kas-bank.arus-kas-per-akun');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Neraca',
            ];

            HTML2PDF($content, $data);
    }

    
    //penjualan ===============================================================================================
    public function printLaporanPenjualanPersediaan()
    {
            $content = view('report.laporan.penjualan.laporan-penjualan-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Penjualan Persediaan',
            ];

            HTML2PDF($content, $data);
    }


    //akun piutang member ========================================================================================
    public function printLaporanPiutangPelanggan()
    {
            $content = view('report.laporan.akun-piutang-member.laporan-piutang-pelanggan');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Piutang Pelanggan',
            ];

            HTML2PDF($content, $data);
    }
    public function printLaporanUmurPiutangPelanggan()
    {
            $content = view('report.laporan.akun-piutang-member.laporan-umur-piutang-pelanggan');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Umur Piutang Pelanggan',
            ];

            HTML2PDF($content, $data);
    }
    public function printTransaksiBelumLunas()
    {
            $content = view('report.laporan.akun-piutang-member.transaksi-belum-lunas');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Umur Piutang Pelanggan',
            ];

            HTML2PDF($content, $data);
    }
    public function printPembayaranMember()
    {
            $content = view('report.laporan.akun-piutang-member.pembayaran-member');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Umur Piutang Pelanggan',
            ];

            HTML2PDF($content, $data);
    }
    
    public function printDaftarMember()
    {
            $content = view('report.laporan.akun-piutang-member.daftar-member');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Umur Piutang Pelanggan',
            ];

            HTML2PDF($content, $data);
    }

     //pembelian ==========================================================================================================
    public function printRekapPembelianPersediaanTmuk()
    {
            $content = view('report.laporan.pembeliaan.rekap-pembelian-persediaan-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Pembelian Persediaan TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printLembarPoBtdk(Request $request)
    {
        dd($request->all());
        $po = TransPo::with('tmuk', 'tmuk.lsi')->find($id);
        // dd($po->tmuk->lsi->kode);
        $records = TransPo::has('detail')->select(DB::raw('
                            trans_po.nomor as nomor,
                            trans_po.nomor_pr as nomor_pr,

                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            ref_produk.nama as nama,
                            ref_produk.kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            
                            ref_produk_setting.uom1_selling_cek as uom1_selling_cek,

                            ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            ref_produk_lsi.curr_sale_prc as harga_estimasi,

                            trans_po_detail.id as id_detail,
                            trans_po_detail.qty_po as qty_po,
                            trans_po_detail.unit_po as unit_po,
                            trans_po_detail.qty_pr as qty_pr,

                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.unit_pr as unit_pr,

                            trans_po_detail.price as harga_aktual
                        '))
                      ->join('trans_po_detail', 'trans_po.nomor', '=', 'trans_po_detail.po_nomor')
                      ->join('ref_produk', 'trans_po_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->join('trans_pr_detail', function($join) {
                            $join->on('trans_po_detail.produk_kode', '=', 'trans_pr_detail.produk_kode')
                                  ->on('trans_pr_detail.pr_nomor', '=', 'trans_po.nomor_pr');
                      })
                      ->where(DB::raw('trans_po.nomor'), $po->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $po->tmuk->lsi->kode)->get();
        
        $data = [
            'record' => $po,
            'detail' => $records,
        ];

        $content = view('report.laporan.pembeliaan.lembar-po-btdk',$data);

        $data = [
                'oriented'   => 'L',
                'paper'      => 'A4',
                'label_file' => 'Picking',
        ];

        HTML2PDF($content, $data);
    }
    public function printLembarPoStdk(Request $request)
    {

        // dd($request->all());
        $po = TransPo::with('tmuk', 'tmuk.lsi')->find($id);
        // dd($po->tmuk->lsi->kode);
        $records = TransPo::has('detail')->select(DB::raw('
                            trans_po.nomor as nomor,
                            trans_po.nomor_pr as nomor_pr,

                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            ref_produk.nama as nama,
                            ref_produk.kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            
                            ref_produk_setting.uom1_selling_cek as uom1_selling_cek,

                            ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            ref_produk_lsi.curr_sale_prc as harga_estimasi,

                            trans_po_detail.id as id_detail,
                            trans_po_detail.qty_po as qty_po,
                            trans_po_detail.unit_po as unit_po,
                            trans_po_detail.qty_pr as qty_pr,

                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.unit_pr as unit_pr,

                            trans_po_detail.price as harga_aktual
                        '))
                      ->join('trans_po_detail', 'trans_po.nomor', '=', 'trans_po_detail.po_nomor')
                      ->join('ref_produk', 'trans_po_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->join('trans_pr_detail', function($join) {
                            $join->on('trans_po_detail.produk_kode', '=', 'trans_pr_detail.produk_kode')
                                  ->on('trans_pr_detail.pr_nomor', '=', 'trans_po.nomor_pr');
                      })
                      ->where(DB::raw('trans_po.nomor'), $po->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $po->tmuk->lsi->kode)->get();
        
        $data = [
            'record' => $po,
            'escrow' => TransReduceEscrow::where('po_nomor', $po->nomor)->where('tmuk_kode', $po->tmuk_kode)->first(),
            'detail' => $records,
        ];

        $content = view('report.laporan.pembeliaan.lembar-po-stdk',$data);

        $data = [
                'oriented'   => 'L',
                'paper'      => 'A4',
                'label_file' => 'Picking',
        ];

        HTML2PDF($content, $data);
    }
    public function printRekapPoBarangTradeLsi()
    {
            $content = view('report.laporan.pembeliaan.rekap-po-barang-trade-lsi');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap PO Barang Trade LSI',
            ];

            HTML2PDF($content, $data);
    }
    public function printRekapPoBarangNonTradeLsi()
    {
            $content = view('report.laporan.pembeliaan.rekap-po-barang-non-trade-lsi');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap PO Barang Non-Trade LSI',
            ];

            HTML2PDF($content, $data);
    }
    public function printRekapPoBarangTradeVendorLokal()
    {
            $content = view('report.laporan.pembeliaan.rekap-po-barang-trade-vendor-lokal');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap PO Barang Trade Vendor Lokal',
            ];

            HTML2PDF($content, $data);
    }
    public function printLembarPickingResult($id=3)
    {
        // dd($request);

        $pr = TransPr::find($id);

        // if ($nomor = $request->nomor) {
        //     $pr->where('nomor', 'like', '%' . $nomor . '%');
        // }
        // dd($pr->tmuk->lsi->kode);
        $records = TransPr::has('detailpr')->select(DB::raw('ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            trans_pr_detail.produk_kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.qty_pr as qty_pr,
                            trans_pr_detail.unit_pr as unit_pr,
                            trans_pr_detail.qty_pick as qty_pick'
                        ))
                      ->join('trans_pr_detail', 'trans_pr.nomor', '=', 'trans_pr_detail.pr_nomor')
                      ->join('ref_produk', 'trans_pr_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->where(DB::raw('trans_pr.nomor'), $pr->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->get()->toArray();
        $data = [
            'record' => TransPr::find($id),
            'detail' => $records,
        ];

            $content = view('report.laporan.pembeliaan.lembar-picking-result', $data);

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Picking',
            ];

            HTML2PDF($content, $data);
    }
    public function printLembarLabelKontainerTmuk()
    {
            $content = view('report.laporan.pembeliaan.lembar-lebel-kontainer-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Lembar Label Kontainer TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printLembarDaftraMuatanTmuk()
    {
            $content = view('report.laporan.pembeliaan.lembar-daftar-muatan-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Lembar Daftra Muatan TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printLembarSuratJalanTmuk()
    {
            $content = view('report.laporan.pembeliaan.lembar-surat-jalan-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Lembar Surat Jalan TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printRekapDeliveryTmuk()
    {
            $content = view('report.laporan.pembeliaan.rekap-delivery-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Lembar Surat Jalan TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printRekapGrnTmuk()
    {
            $content = view('report.laporan.pembeliaan.rekap-grn-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Lembar Surat Jalan TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printLembarKonfirmasiReturnTmuk()
    {
            $content = view('report.laporan.pembeliaan.lembar-konfirmasi-return-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Lembar Surat Jalan TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printRekapSlLsiTmuk()
    {
            $content = view('report.laporan.pembeliaan.rekap-sl-lsi-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Lembar Surat Jalan TMUK',
            ];

            HTML2PDF($content, $data);
    }


    //penjualan ============================================================================================
    public function printPenjualanBulanan()
    {
            $content = view('report.laporan.penjualan.penjualan-bulanan');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap GRN TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printPenjualanHarian()
    {
            $content = view('report.laporan.penjualan.penjualan-harian');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap GRN TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printPenjualanMingguan()
    {
            $content = view('report.laporan.penjualan.penjualan-mingguan');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap GRN TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printPernjualanPerBarang()
    {
            $content = view('report.laporan.penjualan.penjualan-per-barang');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap GRN TMUK',
            ];

            HTML2PDF($content, $data);
    }

    //aktiva tetap ============================================================================================
    public function printAktivaTetap()
    {
            $content = view('report.laporan.aktiva-tetap.aktiva-tetap');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap GRN TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printAktivaTetapPerTipe()
    {
            $content = view('report.laporan.aktiva-tetap.aktiva-tetap-per-tipe');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap GRN TMUK',
            ];

            HTML2PDF($content, $data);
    }


    //persediaan ====================================================================================================
    public function printPersediaanRekapGrnTmuk()
    {
            $content = view('report.laporan.persediaan.rekap-grn-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap GRN TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printPersediaanRekapPersediaanTmuk()
    {
            $content = view('report.laporan.persediaan.rekap-persediaan-tmuk');

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Persediaan TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printRekapPergerakanPersediaanTmuk()
    {
            $content = view('report.laporan.persediaan.rekap-pergerakan-persediaan-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Pergerakan Persediaan TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printRekapSoTmuk()
    {
            $content = view('report.laporan.persediaan.rekap-so-tmuk');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Stok Opname TMUK',
            ];

            HTML2PDF($content, $data);
    }
    public function printUmurPersediaan()
    {
            $content = view('report.laporan.persediaan.umur-persediaan');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Rekap Stok Opname TMUK',
            ];

            HTML2PDF($content, $data);
    }


// PRINT LAPORAN PDF
//PEMBELIAN
    public function printPembelian(Request $request){
        // dd($request->all());
        $record = [];
        $data   = [];
        switch ($request->laporan) {
            case 'lembar-po-btdk':
                $list_detail_po =function($po){
                $detail = TransPo::has('detail')->select(DB::raw('
                            trans_po.nomor as nomor,
                            trans_po.nomor_pr as nomor_pr,

                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            ref_produk.nama as nama,
                            ref_produk.kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            
                            ref_produk_setting.uom1_selling_cek as uom1_selling_cek,

                            ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            ref_produk_lsi.curr_sale_prc as harga_estimasi,

                            trans_po_detail.id as id_detail,
                            trans_po_detail.qty_po as qty_po,
                            trans_po_detail.unit_po as unit_po,
                            trans_po_detail.qty_pr as qty_pr,

                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.unit_pr as unit_pr,

                            trans_po_detail.price as harga_aktual
                        '))
                      ->join('trans_po_detail', 'trans_po.nomor', '=', 'trans_po_detail.po_nomor')
                      ->join('ref_produk', 'trans_po_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->join('trans_pr_detail', function($join) {
                            $join->on('trans_po_detail.produk_kode', '=', 'trans_pr_detail.produk_kode')
                                  ->on('trans_pr_detail.pr_nomor', '=', 'trans_po.nomor_pr');
                      })
                      ->where(DB::raw('trans_po.nomor'), $po->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $po->tmuk->lsi->kode)->get();
                    $req['total_harga_aktual'] = FormatNumber($detail->sum('harga_aktual'));
                    $array_detail = array_merge($detail->toArray(),$req);
                    return $array_detail;
                };

                $po = TransPo::select('*');

                if($request->status){
                    $po->where('status', $request->status);
                }

                if($request->start and $request->end){
                    $start = $request->start;
                    $end   = $request->end;
                    $po->whereBetween(\DB::raw('tgl_buat'), array($start, $end));
                }

                $pos = $po->get();


                $temp = [];
                foreach ($pos as $record) {
                $detail_rec = $list_detail_po($record);
                $temp[] = [
                    'nomor'     => $record->nomor,
                    'lsi_kode'  => $record->tmuk->lsi->kode,
                    'lsi_nama'  => $record->tmuk->lsi->nama,
                    'tmuk_kode' => $record->tmuk->kode,
                    'tmuk_nama' => $record->tmuk->nama,
                    'tgl_buat'  => $record->tgl_buat?\Carbon\Carbon::parse($record->tgl_buat)->format('Y/M/d')  : '',
                    'tgl_kirim' => $record->pr->tgl_kirim?\Carbon\Carbon::parse($record->pr->tgl_kirim)->format('Y/M/d') : '',
                    'detail' => $detail_rec,

                ];
                }
                $data = [
                'oriented' => 'L',
                'records'   => $temp,
                ];
                break;
            // case '':
                
            //     break;
            default:
                break;
        }
            

        $content = view('report.laporan.pembeliaan.'.$request->laporan, $data);
        // return $content;
        HTML2PDF($content, $data);
    }
// END LAPORAN PEMBELIAN



    // PRINT LAPORAN EXEL
    //PEMBELIAN
    public function exelPembelian(Request $request){
        // dd($request->all());
        switch ($request->laporan) {
            case 'lembar-po-btdk':
                $this->exelPoBtdk();
                break;
    
            default:
                // code...
                break;
        }
    }

    public function exelPoBtdk(){

        $data = [
            'title'     => ['PURCHASE ORDER (STDK)'],
            'header'    => [ 
                    'NO PO',
                    'LSI',
                    'TMUK',
                    'Tanggal Pemesanan',
                    'PO Type',
                    'Expected Delivery Dat',
                    'Kode Produk',
                    'Barcode',
                    'Nama Produk',
                    'Qty/UOM',
                    'UOM',
                    'Order Qty',
                    'Harga Satuan',
                    'Total Harga'
                ],
            // 'total' => $total,
            // 'list_data' => $row_data,
        ];

        $reports = \Excel::create($data['title'][0], function($excel) use ($data){
            $excel->setTitle($data['title'][0]);
            $excel->setCreator('ho.dev')->setCompany('ho.dev');
            $excel->setDescription('Export');
            $excel->sheet($data['title'][0], function($sheet) use ($data){
                // SEET TITTLE
                $sheet->mergeCells('A1:N1');
                $sheet->mergeCells('A2:N2');
                $sheet->cells('A1:N2', function($cells){
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });

                $row =1;
                foreach ($data['title'] as $key=>$title) {
                    $sheet->row($row,array($title));
                    $row++;
                }


                $sheet->cells('A4:N4', function($cells){
                    $cells->setFontSize(12);
                    $cells->setBackground("#80bfff");
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                
                $sheet->row(4, $data['header']);
                $row = 5;

                // foreach ($data['list_data'] as $row_data) {
                //     $sheet->row($row, $row_data);
                //     $row++;
                // }
                $row = $row+1;
                $sheet->mergeCells('B'.$row.':N'.$row);
                $sheet->cells('A'.$row.':N'.$row, function($cells){
                    $cells->setFontSize(12);
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                // $sheet->row($row, $data['total']);

                $range = "A4:N".$row;
                $sheet->setBorder($range, 'thin');

            });
        });

        $reports->export('xls');
    }

}
