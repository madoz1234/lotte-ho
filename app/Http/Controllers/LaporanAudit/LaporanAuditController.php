<?php

namespace Lotte\Http\Controllers\LaporanAudit;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Session;

//Models
use Lotte\Models\Trans\TransLogAudit;
use Lotte\Models\User;
// use Lotte\Models\Trans\TransPo;
// use Lotte\Models\Trans\TransReduceEscrow;
// use Lotte\Models\Trans\TransKontainer;
// use Lotte\Models\Trans\TransTruk;
// use Lotte\Models\Trans\TransJurnal;
// use Lotte\Models\Trans\TransPyr;
// use Lotte\Models\Trans\TransPoDetail;
// use Lotte\Models\Master\ProdukLsi;

class LaporanAuditController extends Controller
{
    protected $link = 'laporan/laporan-audit/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Laporan Audit");
        $this->setSubtitle("&nbsp;");
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Laporan & Rincian' => '#', 'Laporan Audit' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'tanggal',
                'name' => 'tanggal',
                'label' => 'Date',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            // [
            //     'data' => 'tanggal',
            //     'name' => 'tanggal',
            //     'label' => 'Time',
            //     'searchable' => false,
            //     'sortable' => true,
            // ],
            [
                'data' => 'user',
                'name' => 'user',
                'label' => 'User',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'tanggal_transaksi',
                'name' => 'tanggal_transaksi',
                'label' => 'Trans Date',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'type',
                'name' => 'type',
                'label' => 'Type',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            // [
            //     'data' => 'ref',
            //     'name' => 'ref',
            //     'label' => '#',
            //     'searchable' => false,
            //     'sortable' => false,
            //     'className' => "center aligned",
            // ],
            [
                'data' => 'aksi',
                'name' => 'aksi',
                'label' => 'Action',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'amount',
                'name' => 'amount',
                'label' => 'Amount',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],

            // [
            //     'data' => 'action',
            //     'name' => 'action',
            //     'label' => 'Aksi',
            //     'searchable' => false,
            //     'sortable' => false,
            //     'className' => "center aligned",
            //     'width' => '150px',
            // ]
        ]);
    }

    public function index()
    {
      
        return $this->render('modules.laporan.laporan-audit.index',['mockup'=>false]);
    }

    public function create()
    {
        return $this->render('modules.laporan.laporan-audit.create');
    }

    public function html2pdf(){
            $content = view('report.purchase-order');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Picking',
            ];

            HTML2PDF($content, $data);
    }


    public function edit($id)
    {
        return $this->render('modules.laporan.laporan-audit.edit');
    }

    public function grid(Request $request)
    {
        // dd($request->All());

        $tanggal_req = ['2000-01-01 00:00:00', '3000-01-01 23:59:59'];

        if($request->start_date and $request->end_date){
            $start_date  = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d');
            $end_date    = Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d');
            $tanggal_req = [$start_date.' 00:00:00',$end_date.' 23:59:59'];
        }

        // dd($tanggal_req);

        $records = TransLogAudit::with('creator')
                         ->select('*')
                         ->whereBetween('tanggal', $tanggal_req)
                         // ->whereBetween(\DB::raw('tanggal'), array($request->start, $request->end))
                         ->orderBy('tanggal');
        // dd($records->get()->toArray());

        // $id_pengguna = $request->user;
        // $user = User::select('*')->first();

        // dd($id_pengguna);

        if ($user = $request->user) {
            $records->where('user_id',  $user);
        }



        // Filters

        // if ($start = $request->start) {
        //     $records->where('tanggal', 'like', '%' . $start . '%');
        // }
        // if ($end = $request->end) {
        //     $records->where('tanggal', 'like', '%' . $end . '%');
        // }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('user', function ($record) {
                return $record->user->name;
            })
            ->addColumn('amount', function ($record) {
                return rupiah($record->amount);
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
    }



    public function printAudit(Request $request)
    {
        // dd($request->All());

        // $tanggal_req = ['2000-01-01 00:00:00', '3000-01-01 23:59:59'];

        // if($request->start and $request->end){
        //     $start  = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
        //     $end    = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');
        //     $tanggal_req = [$start.' 00:00:00',$end.' 23:59:59'];
        // }

        // $records = TransLogAudit::with('creator')
        //                  ->select('*')
        //                  ->orderBy('tanggal');
        

        $tanggal_req = ['2000-01-01 00:00:00', '3000-01-01 23:59:59'];

        if($request->start_date and $request->end_date){
            $start_date  = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d');
            $end_date    = Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d');
            $tanggal_req = [$start_date.' 00:00:00',$end_date.' 23:59:59'];
        }

        $records = TransLogAudit::with('creator')
                         ->select('*')
                         ->whereBetween('tanggal', $tanggal_req)
                         ->orderBy('tanggal');


        if ($user = $request->user) {
            $records->where('user_id',  $user);
        }

        $rcd = $records->get();

        $user_ = 'All';
        if($user = $request->user){
            $data = User::where('id', $user)->first();
            $user_ =  $data->name;
        }

        // dd($user);

        $data = [
            'record' => $rcd,
            'req'    => $request,
            'user'   => $user_,
        ];

        // dd($data);
        // $content = $this->render('modules.laporan.laporan-audit.print', $data);

        // $data = [
        //         // 'oriented'   => 'L',
        //         'paper'      => 'A4',
        //         'label_file' => 'Picking',
        // ];

        // HTML2PDF($content, $data);
        $pdf = \PDF::loadView('modules.laporan.laporan-audit.print', $data);
            return $pdf->setOption('margin-bottom', 5)
            ->setOption('margin-top', 5)
            ->setOption('margin-left', 3)
            ->setOption('margin-right', 3)
            ->inline();
    }

}
