<?php

namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;

use Lotte\Models\Master\Tmuk;
use Lotte\Models\Planogram\Planogram;

class PlanogramController extends Controller
{
	public function index($tmuk)
	{
		$tmuk = Tmuk::where('kode', $tmuk)
					->first();

		if($tmuk){
			$plano = Planogram::with('detail')
							  ->where('jenis_assortment_id', $tmuk->jenis_assortment_id)
							  ->where('status', 1)
							  ->orderBy('created_at', 'desc')
							  ->first();
			if($plano){
				return response([
					'status' => true,
					'message' => 'Data Plano berhasil ditemukan.',
					'data' => $plano
				]);
			}

			return response([
				'status' => false,
				'message' => 'Data Plano untuk TMUK anda tidak dapat ditemukan.'
			]);
		}

		return response([
			'status' => false,
			'message' => 'Data TMUK tidak dapat ditemukan.'
		]);
	}
}