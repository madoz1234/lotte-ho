<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransHppMap;
use Lotte\Models\Master\HargaTmuk;

class HPPController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	//jenis kustomer
	public function getHpp(Request $request)
	{
		$check = HargaTmuk::where('produk_kode', $request->kode_produk)->where('tmuk_kode', $request->kode_tmuk)->first();
		return response([
			'status'  => 'success',
			'message' => $check,
		], 200);
	}

	public function getMap(Request $request)
	{
		$check = TransHppMap::where('produk_kode', $request->kode_produk)->where('tmuk_kode', $request->tmuk)->where('po_nomor', $request->nomor_pyr)->first();
		return response([
			'status'  => 'success',
			'message' => $check,
		], 200);
	}
}