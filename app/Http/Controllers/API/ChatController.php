<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\Chat;

class ChatController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getChat(Request $request)
	{

		$chat = Chat::orderBy('created_at','asc')->get();
		return response([
			'status'  => 'success',
			'message' => $chat,
		]);
	}

	public function sendChat(Request $request)
	{
		try {
			$chat = new Chat;
			$chat->pesan = $request->pesan;
			$chat->nama = $request->nama;
			$chat->created_by = 1;
			$chat->save();
			return response([
				'status'  => 'success',
				'message' => $chat,
			]);
		} catch (Exception $e) {
			return response([
				'status'  => 'error'
			]);
		}
	}
}