<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;

/* models */
use Lotte\Models\SecTransaksi;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmukBayar;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\PenjualanDetail;
use Lotte\Models\Master\ProdukTmuk;
use Carbon\Carbon;

class PenjualanController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	//jenis kustomer
	public function syncEndShift(Request $request)
	{
		// data
		$tmuk = $request->tmuk;
		$data = $request->data;
		$data['jam'] = '00:00:00';

		// kamus jurnal
		$tahun_fiskal       = TahunFiskal::getTahunFiskal();
		$coa_kas            = '1.1.1.2'; // Kas Kecil
		$coa_balance        = '3.2.0.0'; // OPENING BALANCE EQUITY
		$coa_selisih_lebih  = '7.1.0.0'; // Selisih Kas Lebih
		$coa_selisih_kurang = '8.1.0.0'; // Selisih Kas Kurang

		// Start of Shift:
		$idtrans  = SecTransaksi::generate();
		$dtjurnal = [];

		$dtjurnal[] = [ // kas kecil
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'Start of Shift:',
			'coa_kode'  => $coa_kas,
			'jumlah'    => $data['initial'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // opening balance
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'Start of Shift:',
			'coa_kode'  => $coa_balance,
			'jumlah'    => $data['initial'],
			'posisi'    => 'K',
			'flag'      => '-',
		];

		TransJurnal::insert($dtjurnal); // insert jurnal

		// End of Shift:
		$idtrans  = SecTransaksi::generate();
		$dtjurnal = [];

		$dtjurnal[] = [ // kas kecil
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'End of Shift:',
			'coa_kode'  => $coa_kas,
			'jumlah'    => $data['initial'] + $data['variance'],
			'posisi'    => 'K',
			'flag'      => '-',
		];
		$dtjurnal[] = [ // opening balance
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'End of Shift:',
			'coa_kode'  => $coa_balance,
			'jumlah'    => $data['initial'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // selisih kas lebih
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'End of Shift:',
			'coa_kode'  => $coa_selisih_lebih,
			'jumlah'    => ($data['variance'] > 0) ? $data['variance'] : 0,
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // selisih kas kurang
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'End of Shift:',
			'coa_kode'  => $coa_selisih_kurang,
			'jumlah'    => ($data['variance'] < 0) ? abs($data['variance']) : 0,
			'posisi'    => 'K',
			'flag'      => '-',
		];

		TransJurnal::insert($dtjurnal); // insert jurnal

		return response([
			'status'  => 'success',
			'message' => 'success',
		], 200);
	}

	public function syncPenjualan(Request $request)
	{
		// data
		$tmuk = $request->tmuk;
		$data = $request->data;

		// kode jurnal
		$tahun_fiskal = TahunFiskal::getTahunFiskal();
		$coa_penjualan          = '1.1.1.1'; // hasil penjualan
		$coa_member             = '1.1.3.1'; // Piutang Penjualan Member
		$coa_penjualan_barang   = '4.1.0.0'; //Penjualan Barang Dagang
		$coa_potongan_penjualan = '4.3.0.0'; // potongan penjualan

		// from bos kim 
		// $coa_escrow     = '1.1.2.3'; // rekening escrow
		// $coa_tunai      = '4.1.1.0'; // pembayaran tunai 
		// $coa_piutang    = '4.1.2.0'; // pembayaran piutang
		$coa_point      = '4.1.3.0'; // pembayaran point
		$coa_persediaan = '1.1.4.0'; // persediaan barang dagang
		$coa_hpp        = '5.1.0.0'; // Hpp


		// jurnal penjualan tmuk 01 (J-104)
		$idtrans  = SecTransaksi::generate();
		$dtjurnal = [];

		$dtjurnal[] = [ // kas penjualan
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J104:saat penjualan',
			'coa_kode'  => $coa_penjualan,
			'jumlah'    => $data['total_penjualan'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // piutang penjualan member
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J104:saat penjualan',
			'coa_kode'  => $coa_member,
			'jumlah'    => $data['piutang_member'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // point member
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J104:saat penjualan',
			'coa_kode'  => $coa_point,
			'jumlah'    => $data['point_member'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // potongan penjualan
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J104:saat penjualan',
			'coa_kode'  => $coa_potongan_penjualan,
			'jumlah'    => $data['potongan_penjualan'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // Penjualan Barang Dagang
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J104:saat penjualan',
			'coa_kode'  => $coa_penjualan_barang,
			'jumlah'    => $data['penjualan_barang'],
			'posisi'    => 'K',
			'flag'      => '-',
		];

		TransJurnal::insert($dtjurnal); // insert jurnal


		// jurnal penjualan tmuk 02 (J-105)
		$idtrans  = SecTransaksi::generate();
		$dtjurnal = [];

		$dtjurnal[] = [ // hpp
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J105:saat penjualan',
			'coa_kode'  => $coa_hpp,
			'jumlah'    => $data['hpp'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // persediaan
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J105:saat penjualan',
			'coa_kode'  => $coa_persediaan,
			'jumlah'    => $data['persediaan'],
			'posisi'    => 'K',
			'flag'      => '-',
		];

		TransJurnal::insert($dtjurnal); // insert jurnal

		// response
		return response([
			'status'  => 'success',
			'message' => 'success',
		], 200);
	}

	public function syncReturPenjualan(Request $request)
	{
		// data
		$tmuk = $request->tmuk;
		$data = $request->data;

		// kode jurnal
		$tahun_fiskal         = TahunFiskal::getTahunFiskal();
		$coa_penjualan        = '1.1.1.1'; // hasil penjualan
		$coa_retur_penjualan  = '4.2.0.0'; // Retur Penjualan
		$coa_hpp              = '5.1.0.0'; // Hpp
		$coa_persediaan       = '1.1.4.0'; // persediaan barang dagang
		$coa_piutang_member   = '1.1.3.1'; // piutang penjualan member
		$coa_point            = '4.1.3.0'; // pembayaran point
		$coa_penjualan_barang = '4.1.1.0'; // Penjualan Barang Dagang


		// jurnal retur penjualan tmuk 01 (J-109)
		$idtrans  = SecTransaksi::generate();
		$dtjurnal = [];

		$dtjurnal[] = [ // kas penjualan
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J109:saat retur penjualan (1/4)',
			'coa_kode'  => $coa_penjualan,
			'jumlah'    => $data['total_penjualan'],
			'posisi'    => 'K',
			'flag'      => '-',
		];
		$dtjurnal[] = [ // retur penjualan
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J109:saat retur penjualan (1/4)',
			'coa_kode'  => $coa_retur_penjualan,
			'jumlah'    => $data['retur_penjualan'],
			'posisi'    => 'D',
			'flag'      => '-',
		];

		TransJurnal::insert($dtjurnal); // insert jurnal


		// jurnal retur penjualan tmuk 02 (J-110)
		$idtrans  = SecTransaksi::generate();
		$dtjurnal = [];

		$dtjurnal[] = [ // hpp
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J110:saat retur penjualan (2/4)',
			'coa_kode'  => $coa_hpp,
			'jumlah'    => $data['hpp'],
			'posisi'    => 'K',
			'flag'      => '-',
		];
		$dtjurnal[] = [ // Persediaan Barang Dagang
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J110:saat retur penjualan (2/4)',
			'coa_kode'  => $coa_persediaan,
			'jumlah'    => $data['persediaan'],
			'posisi'    => 'K',
			'flag'      => '-',
		];

		TransJurnal::insert($dtjurnal); // insert jurnal

		// jurnal retur penjualan tmuk 03 (J-111)
		$idtrans  = SecTransaksi::generate();
		$dtjurnal = [];

		$dtjurnal[] = [ // kas penjualan
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J111:saat retur penjualan (3/4)',
			'coa_kode'  => $coa_penjualan,
			'jumlah'    => $data['total_penjualan'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // Piutang Penjualan Member
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J111:saat retur penjualan (3/4)',
			'coa_kode'  => $coa_piutang_member,
			'jumlah'    => $data['piutang_member'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // Point Member
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J111:saat retur penjualan (3/4)',
			'coa_kode'  => $coa_point,
			'jumlah'    => $data['point_member'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // Penjualan Barang Dagang
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J111:saat retur penjualan (3/4)',
			'coa_kode'  => $coa_penjualan_barang,
			'jumlah'    => $data['penjualan_barang'],
			'posisi'    => 'K',
			'flag'      => '+',
		];

		TransJurnal::insert($dtjurnal); // insert jurnal

		// jurnal retur penjualan tmuk 04 (J-112)
		$idtrans  = SecTransaksi::generate();
		$dtjurnal = [];

		$dtjurnal[] = [ // hpp
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J112:saat retur penjualan (4/4)',
			'coa_kode'  => $coa_hpp,
			'jumlah'    => $data['hpp'],
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // Persediaan Barang Dagang
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $data['tanggal'] . ' ' . $data['jam'],
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'J112:saat retur penjualan (4/4)',
			'coa_kode'  => $coa_persediaan,
			'jumlah'    => $data['persediaan'],
			'posisi'    => 'K',
			'flag'      => '-',
		];

		TransJurnal::insert($dtjurnal); // insert jurnal

		// response
		return response([
			'status'  => 'success',
			'message' => 'success',
		], 200);
	}

	public function sendPenjualan(Request $request){
		try {
			$data = json_decode($request->data, true);

			\DB::beginTransaction();
			try {
				// insert ka 3 table
				foreach ($data as $penjualan) {

					// data belum exist
					$exist = TransRekapPenjualanTmuk::where('jual_kode', $penjualan['jual_kode'])->first();
					if (!$exist) {
						$data = new TransRekapPenjualanTmuk();
						$data->fill($penjualan);
						$data->save();

						// ke pembayarannya
						foreach ($penjualan['bayarnya'] as $byr) {
							$pembayarannya = new TransRekapPenjualanTmukBayar();
							$pembayarannya->fill($byr);
							$pembayarannya->save();
						}
						
						// ke detail
						foreach ($penjualan['detailnya'] as $dt) {
							$deetail = new TransRekapPenjualanTmukDetail();
							$deetail->fill($dt);
							$deetail->save();
						}
					}	
				}

				// commit
				\DB::commit();

				// response success
				return response([
					'status'  => 'success',
					'message' => 'success',
				], 200);

			} catch (\Illuminate\Database\QueryException $ex) {
				\DB::rollback();

				return response([
					'status'  => 'error',
					'message' => $ex->getMessage(),
				], 500);
			}
		}catch(\Exception $e){
			return response([
				'status'  => 'error',
				'message' => $e->getMessage(),
			], 500);
		}
	}

	public function sendNilaiPenjualan(Request $request)
	{
		try {
			$data = json_decode($request->data, true);

			\DB::beginTransaction();
			try {
				// insert ka 3 table
				foreach ($data as $penjualan) {
					// data belum exist
					$id = ProdukTmuk::where('tmuk_kode', $request->tmuk)
							->where('produk_kode', $penjualan['stock_id'])
							->pluck('id');
					if(!is_null($id)){
						$detail = PenjualanDetail::where('produk_tmuk_kode', $id)
								->where('date','=', $penjualan['tanggal'])
								->first();
						if(is_null($detail)){
							$detail = new PenjualanDetail();
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $penjualan['qty'];
							$detail->date = $penjualan['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}else{
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $penjualan['qty'];
							$detail->date =$penjualan['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}
					}
				}
				// commit
				\DB::commit();

				// response success
				return response([
					'status'  => 'success',
					'message' => 'success',
				], 200);

			} catch (\Illuminate\Database\QueryException $ex) {
				\DB::rollback();

				return response([
					'status'  => 'error1',
					'message' => $ex->getMessage(),
				], 500);
			}
		}catch(\Exception $e){
			return response([
				'status'  => 'error',
				'message' => $e->getMessage(),
			], 500);
		}
	}

}