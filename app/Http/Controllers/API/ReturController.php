<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransRetur;
use Lotte\Models\Trans\TransReturDetail;

class ReturController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function sendRetur(Request $request)
	{

		$data_list = $request->list;
		$retur = TransRetur::where('nomor_po', $data_list['nomorpo'])->first();
		if(!$retur){
			$data_retur = new TransRetur();
			$data_retur->tmuk_kode = $data_list['tmuk'];
			$data_retur->nomor_po = $data_list['nomorpo'];
			$data_retur->nomor_retur = $data_list['no_retur'];
			$data_retur->created_by = 1;
			$data_retur->created_at = date('Y-m-d H:i:s');
			$data_retur->save();

			foreach ($data_list['detail'] as $value) {
				$retur_detail = new TransReturDetail();
				$retur_detail->retur_id   	= $data_retur->id;
				$retur_detail->produk_kode 	= $value['id'];
				$retur_detail->qty      	= $value['qty'];
				$retur_detail->catatan 		= $value['note'];
				$retur_detail->qty_retur 	= $value['qty'];
				$retur_detail->reason 		= $value['reason'];
				$retur_detail->created_by  	= 1;
				$retur_detail->created_at  	= date('Y-m-d H:i:s');
				$retur_detail->harga_satuan = $value['retur_unit_price'];
				$retur_detail->harga_total 	= $value['retur_price'];
				$retur_detail->save();
			}
		}
		return response([
			'status'  => 'success',
			'message' => true,
		], 200);
	}
}