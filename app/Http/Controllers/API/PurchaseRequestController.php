<?php

namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;

use Lotte\Models\Master\Produk;

use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPrDetail;


class PurchaseRequestController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function sendPr(Request $request)
	{
		// check produk
		foreach ($request->lists as $value) {
			$exist_prod = Produk::where('kode', $value['id'])->first();
			if (!$exist_prod) {
				return response([
					'status'  => 'error',
					'message' => 'Tidak Terdapat Produk dengan kode ' . $value['id'],
				], 404);
			}
		}
		// cek pr
		$pr = TransPr::where('nomor', $request->pr)->first();
		if (!$pr) {
			// insert PR
			$pr = new TransPr();
			$pr->nomor = $request->pr;
			$pr->tmuk_kode = $request->tmuk;
			$pr->tgl_buat = $request->date;
			$pr->tgl_kirim = date('Y-m-d');
			$pr->total = $request->total;
			$pr->keterangan = $request->comments;
			$pr->created_by = 1;
			$pr->created_at = date('Y-m-d H:i:s');
			$pr->save();

			// insert PR Detail
			foreach ($request->lists as $value) {
				$prdetail = new TransPrDetail();
				$prdetail->pr_nomor   	= $request->pr;
				$prdetail->produk_kode 	= $value['id'];
				$prdetail->qty_pr      	= $value['qty'];
				$prdetail->unit_pr 		= $value['unit_order'];
				$prdetail->qty_aktual   = $value['con_pr'];
                $prdetail->qty_pick     = null;
				$prdetail->total_price 	= $value['order_price'];
				$prdetail->qty_order    = $value['qty_pr'];
				$prdetail->unit_order 	= $value['unit_pr'];
				$prdetail->unit_order_price 	= $value['price'];
				$prdetail->qty_sell 	= $value['qty_sell'];
				$prdetail->unit_sell 	= $value['unit_sell'];
				$prdetail->created_by  	= 1;
				$prdetail->created_at  	= date('Y-m-d H:i:s');
				$prdetail->save();
			}
		}
		return response([
			'status'  => 'success',
			'message' => $pr->nomor,
		], 200);
	}
}