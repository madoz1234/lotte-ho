<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Master\DetailVendorLokalTmuk;
use Lotte\Models\Master\VendorLokalTmuk;

class TmukController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getSaldo(Request $request)
	{

		$kode_tmuk = $request->tmuk;
		$tmuk = Tmuk::where('kode', $kode_tmuk)->first();
		return response([
			'status'  => 'success',
			'message' => $tmuk->saldo_deposit,
		], 200);
	}

	public function getOnline(Request $request)
	{
		$online = 0;
		$offline = 0;

		$tmuk = Tmuk::all();
		foreach ($tmuk as $value) {
			if($value->last_online){
				$awal  = date_create($value->last_online);
				$akhir = date_create(); // waktu sekarang
				$diff  = date_diff( $awal, $akhir );

				$menit = (int)$diff->i;
				if($menit<=1){
					$online++;
				}else{
					$offline++;
				}
			}else{
				$offline++;
			}
		}
		return response([
			'online'  => $online,
			'offline' => $offline,
		], 200);
	}

	public function getSaldoEscrow(Request $request)
	{

		$kode_tmuk = $request->tmuk;
		$tmuk = Tmuk::where('kode', $kode_tmuk)->first();
		return response([
			'status'  => 'success',
			'escrow' => $tmuk->saldo_escrow,
			'rekening' => $tmuk->rekeningescrow->nomor_rekening,
		], 200);
	}

	public function getSaldoSCN(Request $request)
	{

		$kode_tmuk = $request->tmuk;
		$tmuk = Tmuk::where('kode', $kode_tmuk)->first();
		return response([
			'status'  => 'success',
			'scn' => $tmuk->saldo_scn,
			'escrow' => $tmuk->saldo_escrow,
		], 200);
	}

	public function getSaldoLocked(Request $request)
	{
		$kode_tmuk = $request->tmuk;
		$locked = TransPr::where('tmuk_kode', $kode_tmuk)
							->where('status', 0)->sum('total');
		$locked2 = TransPr::where('tmuk_kode', $kode_tmuk)
							->whereHas('po', function($q){
								$q->where('status', '!=', 2);
							})->sum('total');
		$data = 0;
		if($locked == NULL){
			if($locked2 !== NULL){
				$data = $locked2;
			}else{
				$data = $locked;
			}
		}else{
			$data = $locked;
		}

		// $data_pyr = TransPyr::where('tmuk_kode', $kode_tmuk)
		// 					->where('status', '!=', 2)->sum('total');

		$total = $data;
		return response([
			'status'  => 'success',
			'locked' => $total,
		], 200);
	}

	public function getVendor(Request $request)
	{

		$tmuk_kode = $request->tmuk;
		$tmuk = Tmuk::with('detailvendor')->where('kode', $tmuk_kode)->first();
		$vendor = DetailVendorLokalTmuk::with('vendor')->where('tmuk_id', $tmuk->id)->get();
		return response([
			'status'  => 'success',
			'vendor' => $vendor,
		], 200);
	}

	public function getTmuk(Request $request)
	{

		$tmuk_kode = $request->tmuk;
		$tmuk = Tmuk::with(['rekeningescrow', 'bankescrow', 'lsi', 'kota', 'kecamatan', 'provinsi'])->where('kode', $tmuk_kode)->first();
		return response([
			'status'  => 'success',
			'tmuk' => $tmuk,
		], 200);
	}

	public function getAktivasi(Request $request)
	{

		$tmuk_kode = $request->tmuk;
		$cek = Tmuk::with(['rekeningescrow', 'bankescrow', 'lsi', 'kota', 'kecamatan', 'provinsi'])->where('kode', $tmuk_kode)->first();
		if($cek['flag_act'] == 0){
			$data = Tmuk::where('kode', $tmuk_kode)->first();
			$data->flag_act = 1;
			$data->save();
		}
		return response([
			'status'  => 'success',
			'tmuk' => $cek
		], 200);
	}
}