<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransPoDetail;
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransMuatanDetail;
use Lotte\Models\Master\Tmuk;

class POController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function sendPO(Request $request)
	{

		$no_pr = $request->nomorpr;
		$pr = TransPr::where('nomor', $no_pr)->first();
		$pr_detail = TransPrDetail::where('pr_nomor', $pr->nomor)->get();
		$po = TransPo::with('detail.produk')->where('nomor_pr', $no_pr)
					   ->where('status', 2)
					   ->get();
		// dd($no_pr);
		return response([
			'status'  => 'success',
			'pr' => $pr,
			'pr_detail' => $pr_detail,
			'po' => $po,
		], 200);
	}

	public function getPO(Request $request)
	{

		$tmuk = $request->tmuk;
		$kode = $request->kode;
		$po = TransPo::with('detail.produk')->where('tmuk_kode', $tmuk)
					   ->where('status', 2)
					   ->where('nomor', $kode)
					   ->get();

		return response([
			'status'  => 'success',
			'po' => $po,
		], 200);
	}

	public function getStock(Request $request)
	{

		$tmuk = $request->tmuk;
		$po = TransPo::with('transPoDetail')->where('status', 1)
					   ->get();
		// dd($no_pr);
		return response([
			'status'  => 'success',
			'po' => $po,
		], 200);
	}

	public function setTerimaPo(Request $request)
	{
		try {
			$po_id = $request->po;
			$po = TransPo::find($po_id);
			if($po){
				$muatan = TransMuatanDetail::where('po_nomor',$po->nomor)->first();
				$muatan->status = 1;
				$muatan->save();
			}	
		} catch (Exception $e) {
			return response([
				'status'  => 'error',
			], 400);
		}
		// dd($no_pr);
		return response([
			'status'  => 'success',
		], 200);
	}

	public function setTerimaOpening(Request $request)
	{
		$tmuk = $request->tmuk;
		$opening = Tmuk::where('kode', $tmuk)
					->get();
		return response([
			'status'  => 'success',
			'opening' => $opening,
		], 200);
	}

	public function setUpdateOpening(Request $request)
	{
		$tmuk = $request->tmuk;
		$tmuk = Tmuk::where('kode', $request->tmuk)->first();
		$tmuk->flag_transaksi = 0;
		$tmuk->save();

		return response([
			'status'  => 'success',
			'message' => true,
		], 200);
	}
}