<?php

namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;

use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukTmukDetail;
use Lotte\Models\Master\GRDetail;
use Lotte\Models\Master\SODetail;
use Lotte\Models\Master\RRDetail;
use Lotte\Models\Master\ADDetail;
use Lotte\Models\Master\PYRDetail;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Promosi;
use Lotte\Models\Master\HargaTmuk;
use Lotte\Models\Trans\TransSync;
use Lotte\Models\Trans\TransSO;
use Lotte\Models\Trans\TransSODetail;
use Lotte\Models\Trans\TransStock;

class ProdukController extends Controller
{

	public function syncProduk(Request $request)
	{
		$tmuk_kode = $request->tmuk;
		$tmuk = Tmuk::with('lsi')->where('kode', $tmuk_kode)->first();
		// get produk in ref_produk_tmuk
		$data = ProdukTmuk::with(['produk.produksetting', 
								'hargaJual' => function($q) use ($tmuk){
									$q->where('tmuk_kode', $tmuk->kode);
							}, 'hargaBeli' => function($q) use ($tmuk){
									$q->where('lsi_kode', $tmuk->lsi->kode);
							}])->where('tmuk_kode', $tmuk_kode)->get();

		$sync = TransSync::where('tmuk_kode',$tmuk_kode)->first();
		if($sync){
			$sync->tmuk_kode = $tmuk_kode;
			$sync->jenis = 'PRODUCT PRICE';
			$sync->updated_at = date('Y-m-d H:i:s');
			$sync->created_at = date('Y-m-d H:i:s');
			$sync->created_by = 1;
			$sync->save();
		}else{
			$new_sync = new TransSync;
			$new_sync->tmuk_kode = $tmuk_kode;
			$new_sync->jenis = 'PRODUCT PRICE';
			$new_sync->created_by = 1;
			$new_sync->created_at = date('Y-m-d H:i:s');
			$new_sync->updated_at = date('Y-m-d H:i:s');
			$new_sync->save();
		}

		return response([
			'status'  => 'success',
			'message' => $data,
		], 200);
	}

	public function getSetting(Request $request)
	{
		$produk_kode = $request->kode_produk;
		$data = ProdukSetting::where('produk_kode', $produk_kode)->get();
		return response([
			'status'  => 'success',
			'message' => $data,
		], 200);
	}

	public function getPrice(Request $request)
	{
		$tmuk_kode = $request->tmuk;
		$kode_produk = $request->kode_produk;
		$tmuk = Tmuk::with('lsi')->where('kode', $tmuk_kode)->first();
		$lsi = Lsi::where('id', $tmuk->lsi_id)->first();
		$data = ProdukLsi::where('lsi_kode', $lsi->kode)
						->where('produk_kode', $kode_produk)
						->first();

		$setting = ProdukSetting::where('produk_kode', $kode_produk)->first();
		if($setting->uom2_conversion !== 0){
			$harga = ($data->curr_sale_prc / $setting->uom2_conversion);
		}else{
			$harga = $data->curr_sale_prc;
		}
		return response([
			'status'  => 'success',
			'message' => $harga,
		], 200);
	}

	public function getPromosi(Request $request)
	{
		$tmuk_kode = $request->tmuk;
		$tgl = \Carbon\Carbon::parse($request->tgl)->format('Y-m-d');
		$tmuk = Tmuk::with('promosi.detailDiskon', 'promosi.detailPromo', 'promosi.detailBuyxgety', 'promosi.detailPurchase')
					->where('kode', $tmuk_kode)
					->first();
		$cek = $tmuk->promosi;
		return response([
			'status'  => 'success',
			'message' => $cek,
		], 200);
		// foreach ($cek as $key => $value) {
		// 	$promosi = Promosi::with(['detailDiskon', 'detailPromo', 'detailBuyxgety', 'detailPurchase.detailList'])->where('id', $value['id'])->get();
		// 	return response([
		// 	'status'  => 'success',
		// 	'message' => $promosi,
		// ], 200);
			
	}

	public function sendChange(Request $request)
	{
		$tmuk_kode = $request->tmuk;
		$produk_kode = $request->stock_id;
		$change = $request->change;

		$harga = HargaTmuk::where('tmuk_kode', $tmuk_kode)
							->where('produk_kode', $produk_kode)->first();
						
		$harga->change_price = $change;
        $harga->save();
		
		return response([
			'status'  => 'success',
			'message' => $harga,
		], 200);
	}

	public function sendNilaiPersediaan(Request $request)
	{
		try {
			$data = json_decode($request->data, true);

			\DB::beginTransaction();
			try {
				// insert ka 3 table
				foreach ($data as $ad) {
					// data belum exist
					$id = ProdukTmuk::where('tmuk_kode', $request->tmuk)
							->where('produk_kode',$ad['stock_id'])
							->pluck('id');
					if(!is_null($id)){
						$detail = ProdukTmukDetail::where('produk_tmuk_kode', $id)
								->where('date','=', $ad['tanggal'])
								->first();
						if(is_null($detail)){
							$detail = new ProdukTmukDetail();
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $ad['qty'];
							$detail->hpp = $ad['map'];
							$detail->nilai_persediaan = $ad['nilai'];
							$detail->date = $ad['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}else{
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $ad['qty'];
							$detail->hpp = $ad['map'];
							$detail->nilai_persediaan = $ad['nilai'];
							$detail->date = $ad['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}
					}
				}
				// commit
				\DB::commit();

				// response success
				return response([
					'status'  => 'success',
					'message' => 'success',
				], 200);

			} catch (\Illuminate\Database\QueryException $ex) {
				\DB::rollback();

				return response([
					'status'  => 'error',
					'message' => $ex->getMessage(),
				], 500);
			}
		}catch(\Exception $e){
			return response([
				'status'  => 'error',
				'message' => $e->getMessage(),
			], 500);
		}
	}

	public function sendPersediaan(Request $request)
	{
		$tmuk = $request->tmuk;
		foreach ($request->lists as $value) {
			$produk_tmuk = ProdukTmuk::where('tmuk_kode', $tmuk)
							->where('produk_kode', $value['stock_id'])
							->first();
			$produk_tmuk->updated_at = date('Y-m-d H:i:s');
			$produk_tmuk->stock_akhir = $value['qty'];
			$produk_tmuk->save();
		}
		return response([
		'status'  => 'success',
		], 200);
	}

	public function sendOpname(Request $request)
	{
		$so = TransSO::where('tmuk_kode', $request->cek['tmuk'])
						->where('tgl_so','=', $request->cek['so_date'])
						->where('type', $request->cek['so_type'])
						->first();

		if($so) {
			if($request->cek['qty_system'] > $request->cek['qty_barcode']){
				$selisih_stok = ($request->cek['qty_system'] - $request->cek['qty_barcode']) * -1;
			}elseif($request->cek['qty_system'] < 0 AND $request->cek['qty_system'] < $request->cek['qty_barcode']){
				$selisih_stok = (($request->cek['qty_system'] * -1) + $request->cek['qty_barcode']);
			}elseif($request->cek['qty_system'] >= 0 AND $request->cek['qty_system'] < $request->cek['qty_barcode']){
				$selisih_stok = $request->cek['qty_barcode'] - $request->cek['qty_system'];
			}
			$so->tmuk_kode = $request->cek['tmuk'];
			$so->tgl_so = $request->cek['so_date'];
			$so->qty_aktual = $request->cek['qty_barcode'];
			$so->qty_system = $request->cek['qty_system'];
			$so->selisih = $selisih_stok;
			$so->type = $request->cek['so_type'];
			$so->flag = 'f';
			$so->created_by = 1;
			$so->created_at = date('Y-m-d H:i:s');
			$so->save();
			foreach ($request->cek['lists'] as $key => $value) {
				$sod->trans_so_id = $so->id;
				$sod->produk_kode = $value['item_code'];
				$sod->qty_barcode = $value['qty_barcode'];
				$sod->qty_system = $value['qty_tersedia'];
				$sod->hpp = $value['map'];
				$sod->created_by = 1;
				$sod->created_at = date('Y-m-d H:i:s');
				$sod->save();
			}
		}else{
			if($request->cek['qty_system'] > $request->cek['qty_barcode']){
				$selisih_stok = ($request->cek['qty_system'] - $request->cek['qty_barcode']) * -1;
			}elseif($request->cek['qty_system'] < 0 AND $request->cek['qty_system'] < $request->cek['qty_barcode']){
				$selisih_stok = (($request->cek['qty_system'] * -1) + $request->cek['qty_barcode']);
			}elseif($request->cek['qty_system'] >= 0 AND $request->cek['qty_system'] < $request->cek['qty_barcode']){
				$selisih_stok = $request->cek['qty_barcode'] - $request->cek['qty_system'];
			}
			$so = new TransSO();
			$so->tmuk_kode = $request->cek['tmuk'];
			$so->tgl_so = $request->cek['so_date'];
			$so->qty_aktual = $request->cek['qty_barcode'];
			$so->qty_system = $request->cek['qty_system'];
			$so->selisih = $selisih_stok;
			$so->type = $request->cek['so_type'];
			$so->flag = 'f';
			$so->created_by = 1;
			$so->created_at = date('Y-m-d H:i:s');
			$so->save();
			foreach ($request->cek['lists'] as $key => $value) {
				$sod = new TransSODetail();
				$sod->trans_so_id = $so->id;
				$sod->produk_kode = $value['item_code'];
				$sod->qty_barcode = $value['qty_barcode'];
				$sod->qty_system = $value['qty_tersedia'];
				$sod->hpp = $value['map'];
				$sod->created_by = 1;
				$sod->created_at = date('Y-m-d H:i:s');
				$sod->save();
			}
		}
		return response([
		'status'  => 'success',
		'message' => 'success',
		], 200);
	}


	public function sendStock(Request $request)
	{
		$tmuk = $request->tmuk;
		foreach ($request->lists as $value) {
			$stock = TransStock::where('tmuk_kode', $tmuk)
						 ->where('tgl_gr', $value['tgl_gr'])
						 ->where('produk_kode', $value['stock_id'])
						 ->first();

			if (!$stock) {
				$stock = new TransStock();
				$stock->tmuk_kode = $tmuk;
				$stock->produk_kode = $value['stock_id'];
				$stock->tgl_gr = $value['tgl_gr'];
				$stock->no_gr = $value['no_gr'];
				$stock->qty = $value['qty'];
				$stock->sisa = $value['sisa'];
				$stock->total = $value['total'];
				$stock->created_by = 1;
				$stock->created_at = date('Y-m-d H:i:s');
				$stock->save();
			}else{
				$stock->tmuk_kode = $tmuk;
				$stock->produk_kode = $value['stock_id'];
				$stock->tgl_gr = $value['tgl_gr'];
				$stock->no_gr = $value['no_gr'];
				$stock->qty = $value['qty'];
				$stock->sisa = $value['sisa'];
				$stock->total = $value['total'];
				$stock->created_by = 1;
				$stock->created_at = date('Y-m-d H:i:s');
				$stock->save();
			}
		}
		return response([
		'status'  => 'success',
		'message' => 'success',
		], 200);
	}

	public function sendNilaiGR(Request $request)
	{
		try {
			$data = json_decode($request->data, true);

			\DB::beginTransaction();
			try {
				// insert ka 3 table
				foreach ($data as $gr) {
					// data belum exist
					$id = ProdukTmuk::where('tmuk_kode', $request->tmuk)
							->where('produk_kode', $gr['stock_id'])
							->pluck('id');
					if(!is_null($id)){
						$detail = GRDetail::where('produk_tmuk_kode', $id)
								->where('date','=', $gr['tanggal'])
								->first();
						if(is_null($detail)){
							$detail = new GRDetail();
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $gr['qty'];
							$detail->date = $gr['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}else{
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $gr['qty'];
							$detail->date =$gr['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}
					}
				}
				// commit
				\DB::commit();

				// response success
				return response([
					'status'  => 'success',
					'message' => 'success',
				], 200);

			} catch (\Illuminate\Database\QueryException $ex) {
				\DB::rollback();

				return response([
					'status'  => 'error',
					'message' => $ex->getMessage(),
				], 500);
			}
		}catch(\Exception $e){
			return response([
				'status'  => 'error',
				'message' => $e->getMessage(),
			], 500);
		}
	}

	public function sendNilaiSO(Request $request)
	{
		try {
			$data = json_decode($request->data, true);

			\DB::beginTransaction();
			try {
				// insert ka 3 table
				foreach ($data as $so) {
					// data belum exist
					$id = ProdukTmuk::where('tmuk_kode', $request->tmuk)
							->where('produk_kode', $so['stock_id'])
							->pluck('id');
					if(!is_null($id)){
						$detail = SODetail::where('produk_tmuk_kode', $id)
								->where('date','=', $so['tanggal'])
								->first();
						if(is_null($detail)){
							$detail = new SODetail();
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $so['qty'];
							$detail->date = $so['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}else{
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $so['qty'];
							$detail->date =$so['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}
					}
				}
				// commit
				\DB::commit();

				// response success
				return response([
					'status'  => 'success',
					'message' => 'success',
				], 200);

			} catch (\Illuminate\Database\QueryException $ex) {
				\DB::rollback();

				return response([
					'status'  => 'error',
					'message' => $ex->getMessage(),
				], 500);
			}
		}catch(\Exception $e){
			return response([
				'status'  => 'error',
				'message' => $e->getMessage(),
			], 500);
		}
	}

	public function sendNilaiRR(Request $request)
	{
		try {
			$data = json_decode($request->data, true);

			\DB::beginTransaction();
			try {
				// insert ka 3 table
				foreach ($data as $rr) {
					// data belum exist
					$id = ProdukTmuk::where('tmuk_kode', $request->tmuk)
							->where('produk_kode', $rr['stock_id'])
							->pluck('id');
					if(!is_null($id)){
						$detail = RRDetail::where('produk_tmuk_kode', $id)
								->where('date','=', $rr['tanggal'])
								->first();
						if(is_null($detail)){
							$detail = new RRDetail();
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $rr['qty'];
							$detail->date = $rr['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}else{
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $rr['qty'];
							$detail->date =$rr['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}
					}
				}
				// commit
				\DB::commit();

				// response success
				return response([
					'status'  => 'success',
					'message' => 'success',
				], 200);

			} catch (\Illuminate\Database\QueryException $ex) {
				\DB::rollback();

				return response([
					'status'  => 'error',
					'message' => $ex->getMessage(),
				], 500);
			}
		}catch(\Exception $e){
			return response([
				'status'  => 'error',
				'message' => $e->getMessage(),
			], 500);
		}
	}

	public function sendNilaiAD(Request $request)
	{
		try {
			$data = json_decode($request->data, true);

			\DB::beginTransaction();
			try {
				// insert ka 3 table
				foreach ($data as $ad) {
					// data belum exist
					$id = ProdukTmuk::where('tmuk_kode', $request->tmuk)
							->where('produk_kode', $ad['stock_id'])
							->pluck('id');
					if(!is_null($id)){
						$detail = ADDetail::where('produk_tmuk_kode', $id)
								->where('date','=', $ad['tanggal'])
								->first();
						if(is_null($detail)){
							$detail = new ADDetail();
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $ad['qty'];
							$detail->date = $ad['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}else{
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $ad['qty'];
							$detail->date =$ad['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}
					}
				}
				// commit
				\DB::commit();

				// response success
				return response([
					'status'  => 'success',
					'message' => 'success',
				], 200);

			} catch (\Illuminate\Database\QueryException $ex) {
				\DB::rollback();

				return response([
					'status'  => 'error',
					'message' => $ex->getMessage(),
				], 500);
			}
		}catch(\Exception $e){
			return response([
				'status'  => 'error',
				'message' => $e->getMessage(),
			], 500);
		}
	}

	public function sendNilaiPYR(Request $request)
	{
		try {
			$data = json_decode($request->data, true);

			\DB::beginTransaction();
			try {
				// insert ka 3 table
				foreach ($data as $pyr) {
					// data belum exist
					$id = ProdukTmuk::where('tmuk_kode', $request->tmuk)
							->where('produk_kode', $pyr['stock_id'])
							->pluck('id');
					if(!is_null($id)){
						$detail = PYRDetail::where('produk_tmuk_kode', $id)
								->where('date','=', $pyr['tanggal'])
								->first();
						if(is_null($detail)){
							$detail = new PYRDetail();
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $pyr['qty'];
							$detail->date = $pyr['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}else{
							$detail->produk_tmuk_kode = $id;
							$detail->qty = $pyr['qty'];
							$detail->date =$pyr['tanggal'];
							$detail->created_at = date('Y-m-d H:i:s');
							$detail->created_by = 1;
							$detail->save();
						}
					}
				}
				// commit
				\DB::commit();

				// response success
				return response([
					'status'  => 'success',
					'message' => 'success',
				], 200);

			} catch (\Illuminate\Database\QueryException $ex) {
				\DB::rollback();

				return response([
					'status'  => 'error',
					'message' => $ex->getMessage(),
				], 500);
			}
		}catch(\Exception $e){
			return response([
				'status'  => 'error',
				'message' => $e->getMessage(),
			], 500);
		}
	}
}