<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\DivisiProduk;

class DivisiController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getDivisi(Request $request)
	{

		$divisi = DivisiProduk::get();
		return response([
			'status'  => 'success',
			'message' => $divisi,
		], 200);
	}
}