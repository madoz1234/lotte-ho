<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\Kategori1;

class CategoryController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getCategory(Request $request)
	{

		$category = Kategori1::get();
		return response([
			'status'  => 'success',
			'message' => $category,
		], 200);
	}
}