<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\JenisKustomer;

class JenisKustomerController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	//jenis kustomer
	public function getKustomer(Request $request)
	{

		$kustomer = JenisKustomer::get();
		return response([
			'status'  => 'success',
			'message' => $kustomer,
		], 200);
	}
}