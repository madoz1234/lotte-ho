<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\SaldoMinimal;
use Lotte\Models\Master\TahunFiskal;

class SaldoMengendapController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getSaldoMengendap()
	{
		$id = TahunFiskal::where('status', 1)->pluck('id');
		$saldo = SaldoMinimal::where('tahun_fiskal_id', $id)->first();
		return response([
			'status'  => 'success',
			'message' => $saldo->saldo_minimal_mengendap,
		], 200);
	}
}