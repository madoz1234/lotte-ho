<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\Tmuk;

class PingController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getSend()
	{
		return response(['status' => 'pong']);
	}

	public function test()
	{
		return response(['status' => 'pong']);
	}

	static public function sentLastOnline(Request $request)
	{
		$tmuk = Tmuk::where('kode', $request->tmuk)->first();
		$tmuk->last_online = date('Y-m-d H:i:s');
		$tmuk->save();

		return response([
			'status'  => 'success',
			'message' => true,
		], 200);
	}

	static public function sentLastOnlineId($id)
	{
		$tmuk = Tmuk::where('kode', $id)->first();
		$tmuk->last_online = date('Y-m-d H:i:s');
		$tmuk->save();

		return response([
			'status'  => 'success',
			'message' => true,
		], 200);
	}
}