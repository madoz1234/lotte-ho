<?php

namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;

use Lotte\Models\Master\Point;
use Lotte\Models\Master\Log\LogPoint;

class PointController extends Controller
{

	public function getPoint(Request $request)
	{

		$point = Point::where('status',1)->orderBy('tgl_berlaku', 'desc')->get();
		return response([
			'status'  => 'success',
			'data' => $point,
		], 200);
	}

	public function kalkulasiPoint(Request $request)
	{

		$point = Point::all();
		return response([
			'status'  => 'success',
			'data' => $point,
		], 200);
	}
}