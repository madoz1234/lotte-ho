<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\RekeningEscrow;

class BankController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getBank(Request $request)
	{

		$bank = RekeningEscrow::get();
		return response([
			'status'  => 'success',
			'message' => $bank,
		], 200);
	}
}