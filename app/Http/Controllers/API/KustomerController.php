<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\Kustomer;

class KustomerController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	//kustomer
	public function getKustomer(Request $request)
	{

		$kustomer = Kustomer::with('tmuk', 'membercard')->get();
		return response([
			'status'  => 'success',
			'message' => $kustomer,
		], 200);
	}
}