<?php

namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;

use Lotte\Models\Master\MemberCard;
use Lotte\Models\Trans\TransMemberPoint;
use Lotte\Models\Trans\TransMemberPointDetail;
use Lotte\Models\Trans\TransMemberPiutang;
use Lotte\Models\Trans\TransMemberPiutangDetail;
use Lotte\Models\Trans\TransRekapMember;
//Model Jurnal
use Lotte\Models\SecTransaksi;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\TahunFiskal;

class MemberController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function sendMember(Request $request)
	{

		// cek
		$member = MemberCard::where('nomor', $request->nomor_member)->first();
		// dd($request->all());
		if (!$member) {
			$member = new MemberCard();
			$member->nomor = $request->nomor;
			$member->nama = $request->nama;
			$member->telepon = $request->telepon;
			$member->email = $request->email;
			$member->jeniskustomer_id = $request->jeniskustomer_id;
			$member->created_by = 1;
			$member->created_at = date('Y-m-d H:i:s');
			// $member->tmuk_id = ;
			$member->alamat = $request->alamat;
			$member->notes = $request->notes;
			$member->limit_kredit = $request->credit_limit;
			$member->tmuk_kode = $request->tmuk_kode;
			$member->save();
		}
		return response([
			'status'  => 'success',
			'message' => $member,
		], 200);
	}

	public function sendMemberPiutangSync(Request $request)
	{

		// cek
		$member = TransMemberPiutang::where('tmuk_kode', $request->tmuk_kode)
										->where('no_member',$request->nomor_member)
										->first();
		// dd($request->all());
		if (!$member || empty($member)) {
			$piutang = new TransMemberPiutang();
			$piutang->tmuk_kode = $request->tmuk_kode;
			$piutang->no_member = $request->nomor_member;
			$piutang->nama_member = $request->nama_member;
			$piutang->created_by = 1;
			$piutang->created_at = date('Y-m-d H:i:s');
			$piutang->save();

			$detail = new TransMemberPiutangDetail();
			$detail->id_trans_member_piutang = $piutang->id;
			$detail->tanggal = $request->tanggal;
			$detail->hutang = $request->total;
			$detail->save();
		}else{
			try {
				$d = TransMemberPiutangDetail::where('tanggal',$request->tanggal)->first();
				if(!$d || empty($d)){
					$detail = new TransMemberPiutangDetail();
					$detail->id_trans_member_piutang = $member->id;
					$detail->tanggal = $request->tanggal;
					$detail->hutang = $request->total;
					$detail->save();
				}

			} catch (Exception $e) {
				
			}
		}

		// data
		$tmuk    = $request->tmuk_kode;
		$tanggal = $request->tanggal;
		$hutang  = $request->total;

		// kamus jurnal
		$tahun_fiskal       = TahunFiskal::getTahunFiskal();
		$coa_penjualan      = '1.1.1.1'; // Kas Hasil Penjualan
		$coa_piutang_member = '1.1.3.1'; // Piutang Penjualan Member

		// Start of Shift:
		$idtrans  = SecTransaksi::generate();
		$dtjurnal = [];

		$dtjurnal[] = [ // kas kecil
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $tanggal,
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'Member Piutang',
			'coa_kode'  => $coa_penjualan,
			'jumlah'    => $hutang,
			'posisi'    => 'D',
			'flag'      => '+',
		];
		$dtjurnal[] = [ // opening balance
			'tahun_fiskal_id'   => $tahun_fiskal->id,
			'idtrans'   => $idtrans,
			'tanggal'   => $tanggal,
			'tmuk_kode' => $tmuk,
			'referensi' => '-',
			'deskripsi' => 'Member Piutang',
			'coa_kode'  => $coa_piutang_member,
			'jumlah'    => $hutang,
			'posisi'    => 'K',
			'flag'      => '-',
		];

		TransJurnal::insert($dtjurnal); // insert jurnal

		return response([
			'status'  => 'success',
			'message' => $member,
		], 200);
	}

	public function sendMemberPointSync(Request $request)
	{

		// cek
		$member = TransMemberPoint::where('tmuk_kode', $request->tmuk_kode)
										->where('no_member',$request->nomor_member)
										->first();
		// dd($request->all());
		if (!$member || empty($member)) {
			$point = new TransMemberPoint();
			$point->tmuk_kode = $request->tmuk_kode;
			$point->no_member = $request->nomor_member;
			$point->nama_member = $request->nama_member;
			$point->created_by = 1;
			$point->created_at = date('Y-m-d H:i:s');
			$point->save();

			$detail = new TransMemberPointDetail();
			$detail->id_trans_member_point = $point->id;
			$detail->tanggal = $request->tanggal;
			$detail->point = $request->point;
			$detail->save();
		}else{
			try {
				$d = TransMemberPointDetail::where('tanggal',$request->tanggal)->first();
				if(!$d || empty($d)){
					$detail = new TransMemberPointDetail();
					$detail->id_trans_member_point = $member->id;
					$detail->tanggal = $request->tanggal;
					$detail->point = $request->point;
					$detail->save();	
				}
			} catch (Exception $e) {
				
			}
		}
		return response([
			'status'  => 'success',
			'message' => $member,
		], 200);
	}

	public function getAll()
	{

		// cek
		$member = MemberCard::get();
		return response([
			'status'  => 'success',
			'message' => $member,
		], 200);
	}

	public function sendMemberPoint(Request $request)
	{

		// cek
		try {
			$tpoin = TransMemberPoint::where('no_member', $request->nomor_member)
											->where('tmuk_kode',$request->tmuk_kode)
											->first();
			if (!$tpoin  || empty($tpoin)) {
				// insert
				$tpoin = new TransMemberPoint();
				$tpoin->no_member = $request->nomor_member;
				$tpoin->nama_member = $request->nama_member;
				$tpoin->tmuk_kode = $request->tmuk_kode;
				$tpoin->save();

				$tpoindetail = new TransMemberPointDetail();
				$tpoindetail->id_trans_member_point = $tpoin->id;
				$tpoindetail->tanggal = $request->tanggal;
				$tpoindetail->point = $request->poin;
				$tpoindetail->save();
			}else{
				$tpoindetail = new TransMemberPointDetail();
				$tpoindetail->id_trans_member_point = $tpoin->id;
				$tpoindetail->tanggal = $request->tanggal;
				$tpoindetail->point = $request->poin;
				$tpoindetail->save();
			}	
		} catch (Exception $e) {
			return response([
				'status'  => 'error'
			], 400);		
		}
	
		return response([
			'status'  => 'success'
		], 200);
	}

	public function sendMemberPiutang(Request $request)
	{

		// cek
		try {
			$tpiutang = TransMemberPiutang::where('no_member', $request->nomor_member)
											->where('tmuk_kode',$request->tmuk_kode)
											->first();
			if (!$tpiutang || empty($tpiutang)) {
				// insert
				$tpiutang = new TransMemberPiutang();
				$tpiutang->no_member = $request->nomor_member;
				$tpiutang->nama_member = $request->nama_member;
				$tpiutang->tmuk_kode = $request->tmuk_kode;
				$tpiutang->save();

				$tpiutangdetail = new TransMemberPiutangDetail();
				$tpiutangdetail->id_trans_member_piutang = $tpiutang->id;
				$tpiutangdetail->tanggal = $request->tanggal;
				$tpiutangdetail->hutang = $request->piutang;
				$tpiutangdetail->save();
			}else{
				$tpiutangdetail = new TransMemberPiutangDetail();
				$tpiutangdetail->id_trans_member_piutang = $tpiutang->id;
				$tpiutangdetail->tanggal = $request->tanggal;
				$tpiutangdetail->hutang = $request->piutang;
				$tpiutangdetail->save();
			}	
		} catch (Exception $e) {
			return response([
				'status'  => 'error'
			], 400);		
		}
	
		return response([
			'status'  => 'success'
		], 200);
	}

	public function syncMemberTmuk(Request $request){
		$cek = TransRekapMember::where('debtor_no',$request->debtor_no)
									->where('nomor_member',$request->nomor_member)
									->first();
		if($cek){
			$cek->fill($request->all());
			$cek->save();
		}else{
			$data = new TransRekapMember;
			$data->fill($request->all());
			$data->save();
		}

		return response([
			'status'  => 'success',
		], 200);
	}
}