<?php

namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransTopupEscrow;
use Lotte\Models\Trans\TransTopupEscrowDetail;
use Lotte\Models\Master\Tmuk;

use Carbon\Carbon;


class TopUpController extends Controller
{

	public function sendTopup(Request $request)
	{
		// $kode_tmuk = $request->tmuk;
		try {
			$tmuk = Tmuk::where('kode',$request->tmuk)->first();
			$topup = new TransTopupEscrow();
			$topup->tanggal = $request->tanggal;
			$topup->tmuk_kode = $request->tmuk;
			$topup->jenis = $request->jenis;
			$topup->jumlah = $request->jumlah;
			$topup->status = $request->status;
			$topup->periode = $request->periode;
			$topup->total_jual = $request->totaljual;
			if($request->jenis==1){
				$topup->saldo_awal = $tmuk->saldo_escrow;
			}else{
				$topup->saldo_awal = $tmuk->saldo_deposit;
			}
			if($request->hasFile('lampiran')){
                $topup->lampiran = uploadFile($request->lampiran, 'uploads/topup/'.$request->tmuk.'/')->file;
            }else{
				$topup->lampiran = null;
            }
			$topup->save();

			if($request->jenis==1){
				if($request->week1!=null){
					$detail = new TransTopupEscrowDetail();
					$detail->id_topup_escrow = $topup->id;
					$detail->week = $request->week1;
					$detail->total = $request->jumlah1;
					$detail->save();
				}
				if($request->week2!=null){
					$detail2 = new TransTopupEscrowDetail();
					$detail2->id_topup_escrow = $topup->id;
					$detail2->week = $request->week2;
					$detail2->total = $request->jumlah2;
					$detail2->save();
				}
				if($request->week3!=null){
					$detail3 = new TransTopupEscrowDetail();
					$detail3->id_topup_escrow = $topup->id;
					$detail3->week = $request->week3;
					$detail3->total = $request->jumlah3;
					$detail3->save();
				}
				if($request->week4!=null){
					$detail4 = new TransTopupEscrowDetail();
					$detail4->id_topup_escrow = $topup->id;
					$detail4->week = $request->week4;
					$detail4->total = $request->jumlah4;
					$detail4->save();
				}
				if($request->week5!=null){
					$detail5 = new TransTopupEscrowDetail();
					$detail5->id_topup_escrow = $topup->id;
					$detail5->week = $request->week5;
					$detail5->total = $request->jumlah5;
					$detail5->save();
				}
			}
		} catch (Exception $e) {
			return response([
				'status'  => 'error',
			], 400);
		}
		
		return response([
			'status'  => 'success',
			'message' => $topup,
		], 200);
	}

	public function sendTopupDetail(Request $request)
	{
		// $kode_tmuk = $request->tmuk;
		// return response("james dosol");
		try {
			$member = $request->member;
	        foreach ($member as $key => $val) {
	            $detail = new TransTopupEscrowDetail();
				$detail->id_topup_escrow = $request->id;
				$detail->member = $key;
				$detail->total = $val;
				$detail->save();
	        }
		} catch (Exception $e) {
			return response([
				'status'  => 'error',
			], 400);
		}
		
		return response([
			'status'  => 'success',
		], 200);
	}

	public function getTopupWeek(Request $request)
	{
		try {
			$pay=0;
			$topup = TransTopupEscrow::where('tmuk_kode',$request->tmuk)->where('jenis',1)->where('status','!=',2)->get();
			foreach ($topup as $value) {
				foreach ($value->detail as $key) {
					if($key->week==$request->week){
						$pay += $key->total;
					}
				}
			}
		} catch (Exception $e) {
			return response([
				'status'  => 'error',
			], 400);
		}
		
		return response([
			'status'  => 'success',
			'message' => $pay,
		], 200);
	}

	public function getSumTunai(Request $request)
	{
		try {
			$pay=0;
			$topup = TransTopupEscrow::where('tmuk_kode',$request->tmuk)->where('jenis',1)->where('status','!=',2)->where('periode',$request->periode)->sum('jumlah');
		} catch (Exception $e) {
			return response([
				'status'  => 'error',
			], 400);
		}
		
		return response([
			'status'  => 'success',
			'message' => $topup,
		], 200);
	}

	public function getSumTunaiAll(Request $request)
	{
		try {
			$pay=0;
			$topup = TransTopupEscrow::where('tmuk_kode',$request->tmuk)->where('jenis',1)->where('status','!=',2)->sum('jumlah');
		} catch (Exception $e) {
			return response([
				'status'  => 'error',
			], 400);
		}
		
		return response([
			'status'  => 'success',
			'message' => $topup,
		], 200);
	}

	public function getSumMember(Request $request)
	{
		try {
			$pay=0;
			$topup = TransTopupEscrow::where('tmuk_kode',$request->tmuk)->where('jenis',2)->where('status','!=',2)->sum('jumlah');
		} catch (Exception $e) {
			return response([
				'status'  => 'error',
			], 400);
		}
		
		return response([
			'status'  => 'success',
			'message' => $topup,
		], 200);
	}

	public function getTopupMember(Request $request)
	{
		try {
			$pay=0;
			$topup = TransTopupEscrow::where('tmuk_kode',$request->tmuk)->where('jenis',2)->where('status','!=',2)->get();
			foreach ($topup as $value) {
				foreach ($value->detail as $key) {
					if($key->member==$request->member){
						$pay += $key->total;
					}
				}
			}
		} catch (Exception $e) {
			return response([
				'status'  => 'error',
			], 400);
		}
		
		return response([
			'status'  => 'success',
			'message' => $pay,
		], 200);
	}

	public function getTopup(Request $request)
	{
		$tmuk = $request->tmuk;
		$from = $request->from;
		$to = $request->to;

		$data = TransTopupEscrow::where('tmuk_kode', $tmuk)
								->where('status', 1)
								->where('jenis','!=','3')
								->whereBetween('tanggal', [$from, $to])
								->get();
		return response([
			'status'  => 'success',
			'message' => $data,
		], 200);
	}

	public function getTopupDeposit(Request $request)
	{
		$tmuk = $request->tmuk;
		$from = $request->from;
		$to = $request->to;

		$data = TransTopupEscrow::where('tmuk_kode', $tmuk)
								->where('status', 1)
								->where('jenis','3')
								->whereBetween('tanggal', [$from, $to])
								->get();
		return response([
			'status'  => 'success',
			'message' => $data,
		], 200);
	}

	public function getTopupYear(Request $request)
	{
		$tmuk = $request->tmuk;
		$year = $request->year;

		$data = TransTopupEscrow::where('tmuk_kode', $tmuk)
								->where('status', 1)
								->where('jenis', 1)
								->whereRaw("date_part('year', tanggal) = '".$year."'")
								->orderBy('created_at','DESC')
								->take(5)
								->get();
		return response([
			'status'  => 'success',
			'message' => $data,
		], 200);
	}

	public function getTopupAll(Request $request)
	{
		$tmuk = $request->tmuk;
		$year = $request->year;

		$data = TransTopupEscrow::where('tmuk_kode', $tmuk)
								->where('status', 1)
								->where('jenis','!=',3)
								->whereRaw("date_part('year', tanggal) = '".$year."'")
								->orderBy('created_at','DESC')
								->take(5)
								->get();
		return response([
			'status'  => 'success',
			'message' => $data,
		], 200);
	}

}