<?php
namespace Lotte\Http\Controllers\API;

use Illuminate\Http\Request;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransPyrDetail;
use Lotte\Models\SecTransaksi;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\TahunFiskal;
use Carbon\Carbon;

class PyrController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	//jenis kustomer
	public function sendPyr(Request $request)
	{
		$data = $request->pyr;
		$detail = $request->detail;
		$pyr = TransPyr::where('nomor_pyr', $data['inv'])->first();
		if (!$pyr) {
			$pyr = new TransPyr();
			$pyr->nomor_pyr = $data['inv'];
			$pyr->tmuk_kode = $data['tmuk'];
			$pyr->vendor_lokal = $data['vendor'];
			$pyr->tgl_buat = $data['tgl'];
			$pyr->tgl_jatuh_tempo = date('Y-m-d');
			$pyr->total = $data['total'];
			$pyr->tipe = $data['tipe'];
			$pyr->bayar = $data['bayar'];
			$pyr->created_by = 1;
			$pyr->created_at = date('Y-m-d H:i:s');
			if ($pyr->save()) {
				// insert detail pyr
				for($i=0; $i<count($detail); $i++){
					$pyrdetail = new TransPyrDetail();
					$pyrdetail->pyr_id  	= $pyr->id;
					$pyrdetail->produk_kode = $detail[$i]['id'];
					$pyrdetail->qty      	= $detail[$i]['qty'];
					$pyrdetail->price 	    = $detail[$i]['price'];
					$pyrdetail->created_by  = 1;
					$pyrdetail->created_at  = date('Y-m-d H:i:s');
					$pyrdetail->save();
				}

				// kamus jurnal
                $tahun_fiskal       = TahunFiskal::getTahunFiskal();
                $coa_utang          = '2.1.1.0'; // Utang Usaha 
                $deskripsi          = '';
                
                $idtrans  = SecTransaksi::generate();
                $dtjurnal = [];
                
                if ($pyr->tipe == '001') {
                  $deskripsi = 'PYR Utang Usaha - Trade Lotte';
                  $dtjurnal[] = [ // Utang Usaha
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => $pyr->tgl_buat,
                    'tmuk_kode'       => $pyr->tmuk_kode,
                    'referensi'       => $pyr->nomor_pyr,
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => $coa_utang,
                    'jumlah'          => $pyr->total,
                    'posisi'          => 'K',
                    'flag'            => '-',
                  ];
                  $dtjurnal[] = [ // Persediaan Barang Dagang
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => $pyr->tgl_buat,
                    'tmuk_kode'       => $pyr->tmuk_kode,
                    'referensi'       => $pyr->nomor_pyr,
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => '1.1.4.0',
                    'jumlah'          => $pyr->total,
                    'posisi'          => 'D',
                    'flag'            => '+',
                  ];
                }else if($pyr->tipe == '002'){
                  $deskripsi = 'PYR Utang Usaha - Trade Non-Lotte';
                  $dtjurnal[] = [ // Utang Usaha
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => $pyr->tgl_buat,
                    'tmuk_kode'       => $pyr->tmuk_kode,
                    'referensi'       => $pyr->nomor_pyr,
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => $coa_utang,
                    'jumlah'          => $pyr->total,
                    'posisi'          => 'K',
                    'flag'            => '-',
                  ];
                  $dtjurnal[] = [ // Persediaan Barang Dagang
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => $pyr->tgl_buat,
                    'tmuk_kode'       => $pyr->tmuk_kode,
                    'referensi'       => $pyr->nomor_pyr,
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => '1.1.4.0',
                    'jumlah'          => $pyr->total,
                    'posisi'          => 'D',
                    'flag'            => '+',
                  ];
                }else if($pyr->tipe == '003'){
                  $deskripsi = 'PYR Utang Usaha - Non Trade Lotte';
                  $dtjurnal[] = [ // Utang Usaha
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => $pyr->tgl_buat,
                    'tmuk_kode'       => $pyr->tmuk_kode,
                    'referensi'       => $pyr->nomor_pyr,
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => $coa_utang,
                    'jumlah'          => $pyr->total,
                    'posisi'          => 'K',
                    'flag'            => '-',
                  ];
                  $dtjurnal[] = [ // Peralatan Toko
                    'tahun_fiskal_id' => $tahun_fiskal->id,
                    'idtrans'         => $idtrans,
                    'tanggal'         => $pyr->tgl_buat,
                    'tmuk_kode'       => $pyr->tmuk_kode,
                    'referensi'       => $pyr->nomor_pyr,
                    'deskripsi'       => $deskripsi,
                    'coa_kode'        => '1.2.6.0',
                    'jumlah'          => $pyr->total,
                    'posisi'          => 'D',
                    'flag'            => '+',
                  ];
                }else if($pyr->tipe == '004'){
	                $deskripsi = 'PYR Utang Usaha - Non Trade Non-Lotte';
                    
                    $b_iklan = 0;
                    $b_pemasaran_lainnya = 0;
                    $komisi_bonus = 0;
                    $b_gaji_lembur_thr = 0;
                    $b_makan_karyawan = 0;
                    $b_tunjangan_kesehatan = 0;
                    $b_asuransi = 0;
                    $b_air = 0;
                    $b_listrik = 0;
                    $b_telp_internet = 0;
                    $b_keamanan = 0;
                    $b_lainnya = 0;
                    $b_distribusi = 0;
                    $plastik = 0;
                    $b_bensin = 0;
                    $b_peralatan_toko = 0;
                    $b_sewa_toko = 0;
                    $b_lain_operasional = 0;
                    $b_lain_nonoperasional = 0;
	                
	                foreach ($pyr->detailPyr as $key => $detail) {
	                    $jenis_barang = $detail->produks->produksetting->jenis_barang_kode;

	                    if ($jenis_barang == '003') { //Biaya Iklan
	                        $b_iklan += $detail->price;
	                    }else if ($jenis_barang == '005') { //Biaya Pemasaran Lainnya
	                        $b_pemasaran_lainnya += $detail->price;
	                    }else if ($jenis_barang == '006') { //Komisi & Bonus
	                        $komisi_bonus += $detail->price;
	                    }else if ($jenis_barang == '007') { //Biaya Gaji, Lembur & THR
	                        $b_gaji_lembur_thr += $detail->price;
	                    }else if ($jenis_barang == '008') { //Biaya Makan Karyawan
	                        $b_makan_karyawan += $detail->price;
	                    }else if ($jenis_barang == '009') { //Biaya Tunjangan Kesehatan
	                        $b_tunjangan_kesehatan += $detail->price;
	                    }else if ($jenis_barang == '010') { //Biaya Asuransi
	                        $b_asuransi += $detail->price;
	                    }else if ($jenis_barang == '011') { //Biaya Air
	                        $b_air += $detail->price;
	                    }else if ($jenis_barang == '012') { //Biaya Listrik
	                        $b_listrik += $detail->price;
	                    }else if ($jenis_barang == '013') { //Biaya Telp & Internet
	                        $b_telp_internet += $detail->price;
	                    }else if ($jenis_barang == '014') { //Biaya Keamanan
	                        $b_keamanan += $detail->price;
	                    }else if ($jenis_barang == '023') { //lainya (sumbangan,dll)
	                        $b_lainnya += $detail->price;
	                    }else if ($jenis_barang == '015') { //Biaya Distribusi (Adm)
	                        $b_distribusi += $detail->price;
	                    }else if ($jenis_barang == '016') { //Plastik Pembungkus
	                        $plastik += $detail->price;
	                    }else if ($jenis_barang == '018') { //Biaya Bensin
	                        $b_bensin += $detail->price;
	                    }else if ($jenis_barang == '022') { //Biaya Perlengkapan Toko
	                        $b_peralatan_toko += $detail->price;
	                    }else if ($jenis_barang == '019') { //Biaya Sewa Toko
	                        $b_sewa_toko += $detail->price;
	                    }else if ($jenis_barang == '020') { //Biaya Lain Operasional
	                        $b_lain_operasional += $detail->price;
	                    }else if ($jenis_barang == '021') { //Biaya Lain Non Operasional
	                        $b_lain_nonoperasional += $detail->price;
	                    }
	                    // else if ($jenis_barang == '017') {
	                    //      $kode_coa = '?', //Promosi
	                    //      $desc = 'Pembayaran PyR - ?';
	                    // }
	                }

                  //Create Non Trade Non Lotte
                  TransJurnal::insert([
                      [
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => $coa_utang, // Utang
                          'jumlah' => $pyr->total,
                          'posisi' => 'K',
                          'flag' => '-',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.1.1.1',        //Biaya Iklan
                          'jumlah' => $b_iklan,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.1.1.2',        //Biaya Pemasaran Lainnya
                          'jumlah' => $b_pemasaran_lainnya,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.1.1.3',        //Komisi & Bonus
                          'jumlah' => $komisi_bonus,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.1.1',        //Biaya Gaji, Lembur & THR
                          'jumlah' => $b_gaji_lembur_thr,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.1.2',        //Biaya Makan Karyawan
                          'jumlah' => $b_makan_karyawan,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.1.3',        //Biaya Tunjangan Kesehatan
                          'jumlah' => $b_tunjangan_kesehatan,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.1.4',        //Biaya Asuransi
                          'jumlah' => $b_asuransi,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.2',        //Biaya Air
                          'jumlah' => $b_air,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.3',        //Biaya Listrik
                          'jumlah' => $b_listrik,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.4',        //Biaya Telp & Internet
                          'jumlah' => $b_telp_internet,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.5',        //Biaya Keamanan
                          'jumlah' => $b_keamanan,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.6',        //Biaya Lainnya (sumbangan, dll)
                          'jumlah' => $b_lainnya,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.10',        //Biaya Distribusi (Adm)
                          'jumlah' => $b_distribusi,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.7',        //Plastik Pembungkus
                          'jumlah' => $plastik,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.11',        //Biaya Bensin
                          'jumlah' => $b_bensin,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.12',        //Biaya Peralatan Toko
                          'jumlah' => $b_peralatan_toko,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.13',        //Biaya Sewa Toko
                          'jumlah' => $b_sewa_toko,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.14',        //Biaya Lain Operasional
                          'jumlah' => $b_lain_operasional,
                          'posisi' => 'D',
                          'flag' => '+',
                      ],[
                          'idtrans' => $idtrans,
                          'tahun_fiskal_id' => $tahun_fiskal->id,
                          'tanggal' => $pyr->tgl_buat,
                          'tmuk_kode' => $pyr->tmuk_kode,
                          'referensi' => $pyr->nomor_pyr,
                          'deskripsi' => $deskripsi,
                          'coa_kode' => '6.2.2.15',        //Biaya Lain Non-Operasional
                          'jumlah' => $b_lain_nonoperasional,
                          'posisi' => 'D',
                          'flag' => '+',
                      ]
                  ]);
                }

                TransJurnal::insert($dtjurnal); // insert jurnal

                $pyr->id_transaksi = $idtrans;
                $pyr->save();

			}
		}
		return response([
			'status'  => 'success',
			'message' => $data,
		], 200);
	}

	public function getPyr(Request $request)
	{
		$nomor = $request->no_pyr;
		$tmuk = $request->tmuk;
		$pyr = TransPyr::with('detailPyr')
						->where('nomor_pyr', $nomor)
						->where('tmuk_kode', $tmuk)
						->where('status', 2)
						->get();

		return response([
			'status'  => 'success',
			'message' => $pyr,
		], 200);
	}

	public function getPyrStatus(Request $request)
	{
		$tmuk = $request->tmuk;
		$pyr = TransPyr::where('tmuk_kode', $tmuk)
						->where('status', 2)
						->get();

		return response([
			'status'  => 'success',
			'message' => $pyr,
		], 200);
	}
}