<?php

namespace Lotte\Http\Controllers\Beranda;

use Illuminate\Http\Request;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\Chat;

class ChatController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	// public function getGrid(Request $request)
	// {
      // $records = Chat::orderBy('created_at', 'desc')->paginate(10);

      //  if ($request->ajax()) {
      //      $view = view('data',compact('records'))->render();
      //      return response()->json(['html'=>$view]);
      //  }
	// 	return view('modules.dashboard.chat', ['records' => $records->reverse()]);
	// }

  //Obrolan
  public function index(Request $request)
  {
        $records = Chat::orderBy('created_at', 'desc')->paginate(10);

        if ($request->ajax()) {
            $view = view('data',compact('records'))->render();
            return response()->json(['html'=>$view]);
        }
    return view('modules.chat.index', ['records' => $records->reverse()]);
  }

  public function ObrolanData(Request $request)
  {
    $records = Chat::orderBy('created_at', 'desc')->paginate(10);
      return view('modules.dashboard.chat.data', ['records' => $records->reverse()]);
  }

  public function ObrolanSave(Request $request)
  {
    $region = new Chat;
      $region->fill($request->all());
      $region->save();

      return response([
        'status' => true,
        'data'  => $region
      ]);
  }

  public function ObrolanDestroy(Request $request)
  {
    $destroy = \App\Models\Ref\Chat::find($request->id)->delete();
      $chat = Chat::all();
      return view('modules.chat.index', ['chat' => $chat]);
  }
//Obrolan
}