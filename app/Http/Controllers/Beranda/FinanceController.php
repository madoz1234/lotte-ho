<?php

namespace Lotte\Http\Controllers\Beranda;
use Lotte\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Datatables;
use Carbon\Carbon;
use Ghunti\HighchartsPHP\Highchart;

//Models
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransTransReduceEscrowDetail;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransTopupEscrow;
use Lotte\Models\Master\Tmuk;

class FinanceController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('modules.dashboard.finance');
	}

	public function getGrid()
	{

		// $reduce = TransReduceEscrow::select('*')->get();
		$now = date('Y-m').'-'.(date('d')-1);
		$old = date('d')-8;

		$record = TransReduceEscrow::select(\DB::raw("extract(days from tanggal) as hari"),'trans_reduce_escrow.saldo_escrow as escrow', 'ref_tmuk.kode as kode', 'ref_tmuk.nama as nama', 'trans_reduce_escrow.tanggal as tanggal')
		->join('ref_tmuk', 'trans_reduce_escrow.tmuk_kode', '=', 'ref_tmuk.kode')
		->orderBy('tanggal', 'DESC')->get()->toArray();

		$temp = [];
		foreach ($record as $val) {
			 	$temp[$val['kode']] = $val;
		}
		//REDUCE ESCROW


		$data_nilai = [];

		//nilai
		foreach ($record as $nilai) {
			$data_nilai[$nilai['kode'].'-'.$nilai['hari']][$nilai['hari']] = $nilai['escrow'];
		}
		// dd($data_nilai);

		$date      = (int)date('d')-1;
		$sparate   = $date-7; 
		$data_temp = [];
		$nilai     = [];
		//header reduce
		$header_reduce_scrow =[];
		for($i=$date; $i>$sparate ;$i--){
			$header_reduce_scrow[] = $i;
		}
		//content
		foreach ($temp as $rows) {
			$hari      = [];
			for($i=$date; $i>$sparate ;$i--){
				$hari[] = isset($data_nilai[$rows['kode'].'-'.$i][$i])? rupiah($data_nilai[$rows['kode'].'-'.$i][$i]):'-';
			}
			$data_temp[] = [
				'nama' => $rows['nama'],
				'kode' => $rows['kode'],
				'nilai'=> $hari
			];		
		}

        $data = [
				'header_reduce' => $header_reduce_scrow,
				'reduce'        => $data_temp,
            ];

		// dd($temp);

		return view('modules.dashboard.finance', $data);
	}

	// RENDER HAIGH CHART

	//Kinerja laba Bersih
	public function postChartmom(Request $request)
	{

		// dd($request->all());

		$from           = \Carbon\Carbon::createFromFormat('M Y', $request->start);
		$to             = \Carbon\Carbon::createFromFormat('M Y', $request->end);
		$diff_in_months = $to->diffInMonths($from);
		$header         = [];

		// dd($from)
		$start          = ($from->format('Y').'-'.$from->format('m').'-1');
		$end            = (($to->format('Y').'-'.$to->format('m').'-'.cal_days($to->format('m'),$to->format('Y'))));
		// dd($start);
		$penjualan = TransRekapPenjualanTmuk::from(\DB::raw("(
			select extract(month from tanggal) as bulan, 
			extract(year from tanggal) as tahun, sum(total) as total , tmuk_kode
			from \"trans_rekap_penjualan\" 
			where tanggal BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
			group by \"bulan\", \"tahun\", \"tmuk_kode\"
		) tbl"))
		->get();

		// dd($penjualan);
		// ===
		foreach ($penjualan as $data) {
			$region = $data->tmuk->lsi->region->area;
			$lsi = $data->tmuk->lsi->nama;
			$tmuk = $data->tmuk->nama;
		}

		// dd($tmuk);
		$arr   = [];
		$nilai_penjualan = [];
		for($i=0; $i<=$diff_in_months; $i++) {
			$header[]  = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ) );
			$arr[date('m-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ))] = $i;
			$nilai_penjualan[]                                                              = 0;
		}
		foreach ($penjualan as $val) {
			$bulan = str_pad($val['bulan'], 2, "0", STR_PAD_LEFT);
			$nilai_penjualan[$arr[$bulan.'-'.$val->tahun]] = (int)$val->total;
		}
// dd($nilai_penjualan);

		// ===

		$chart = new Highchart();
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		$chart->title->text = "Kinerja Laba Bersih TMUK <br> Region Timur / LSI Lotte Pasar Rebo / TMUK Jernih";
		$chart->xAxis->categories = $header;
		$chart->yAxis->min                            = 0;
		$chart->yAxis->title->text                    = "Laba Bersih (Rp)";
		$chart->yAxis->stackLabels->enabled           = 1;
		$chart->credits->enabled                      = false;
		$chart->yAxis->stackLabels->style->fontWeight = "bold";
		$chart->legend->align                         = "right";
		$chart->legend->x                             = - 120;
		$chart->legend->verticalAlign                 = "bottom";
		$chart->legend->shadow                        = false;
		$chart->tooltip = [
				'headerFormat' => '<b>{point.x}</b><br/>',
				'pointFormat' => '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
			];



		$chart->plotOptions->column->stacking = "normal";
		$chart->plotOptions->column->dataLabels->enabled = 1;

		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => $nilai_penjualan
		);

		$chart->series[] = array(
		    'name' => "Laba Kotor",
		    'data' => $nilai_penjualan
		);

		$chart->series[] = array(
		    'name' => "Laba Bersih",
		    'data' => $nilai_penjualan
		);
		return $chart->renderOptions();
	}

	public function postChartqoq(Request $request)
	{
		$chart = new Highchart();
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		$chart->title->text = "Kinerja Laba Bersih TMUK <br> Region Timur / LSI Lotte Pasar Rebo / TMUK Jernih";
		$chart->xAxis->categories = array(
		    'I/15',
		    'II/15',
		    'III/15',
		    'I/16',
		    'II/16',
		    'III/16',
		    'I/17',
		    'II/17',
		    'III/17',
		    'I/18',
		    'II/18',
		    'IV/18',
		);

		$chart->yAxis->min                            = 0;
		$chart->yAxis->title->text                    = "Laba Bersih (Rp)";
		$chart->yAxis->stackLabels->enabled           = 1;
		$chart->yAxis->stackLabels->style->fontWeight = "bold";
		$chart->legend->align                         = "right";
		$chart->legend->x                             = - 120;
		$chart->legend->verticalAlign                 = "bottom";
		$chart->legend->shadow                        = false;
		$chart->credits->enabled                      = false;

		$chart->tooltip = [
				'headerFormat' => '<b>{point.x}</b><br/>',
				'pointFormat' => '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
			];



		$chart->plotOptions->column->stacking = "normal";
		$chart->plotOptions->column->dataLabels->enabled = 1;

		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => array(
		        5,
		        3,
		        4,
		        2,
		        7,
		        7,
		        4,
		        6,
		        3,
		        7,
		        7,
		        2
		    )
		);

		$chart->series[] = array(
		    'name' => "Laba Kotor",
		    'data' => array(
		        2,
		        2,
		        3,
		        2,
		        2,
		        2,
		        2,
		        2,
		        2,
		        2,
		        2,
		        1
		    )
		);

		$chart->series[] = array(
		    'name' => "Laba Bersih",
		    'data' => array(
		        3,
		        4,
		        4,
		        2,
		        2,
		        2,
		        2,
		        2,
		        2,
		        2,
		        2,
		        5
		    )
		);
		return $chart->renderOptions();
	}

	public function postChartyoy(Request $request)
	{

		// dd($request->all());

		$from           = \Carbon\Carbon::createFromFormat('Y', $request->start);
		$to             = \Carbon\Carbon::createFromFormat('Y', $request->end);
		$diff_in_months = $to->diffInYears($from);
		$header         = [];

		$start          = ($from->format('Y').'-01-01');
		$end            = ($to->format('Y').'-12-'.cal_days(12,$to->format('Y')));

		
		$penjualan = TransRekapPenjualanTmuk::from(\DB::raw("(
			select extract(year from tanggal) as tahun, sum(total) as total, tmuk_kode
			from \"trans_rekap_penjualan\" 
			where tanggal BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
			group by \"tahun\", \"tmuk_kode\"
		) tbl"))
		->get();
// dd($penjualan);
		foreach ($penjualan as $data) {
			$region = $data->tmuk->lsi->region->area;
			$lsi = $data->tmuk->lsi->nama;
			$tmuk = $data->tmuk->nama;
		}

		$arr   = [];
		$nilai_penjualan = [];
		for($i=0; $i<=$diff_in_months; $i++) {
			$header[]                                                             = date('Y', strtotime($from->format('Y') . ' -'.$i.'year' ));
			$arr[date('Y', strtotime($from->format('Y') . ' -'.$i.'year' ))] = $i;
			$nilai_penjualan[]                                                              = 0;
		}
		foreach ($penjualan as $val) {
			$nilai_penjualan[$arr[$val['tahun']]] = (int)$val->total;
		}

		$chart = new Highchart();
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		$chart->title->text = "Kinerja Laba Bersih TMUK <br> Region $region / LSI $lsi / TMUK $tmuk";
		$chart->xAxis->categories = $header;
		$chart->yAxis->min                            = 0;
		$chart->yAxis->title->text                    = "Laba Bersih (Rp)";
		$chart->yAxis->stackLabels->enabled           = 1;
		$chart->yAxis->stackLabels->style->fontWeight = "bold";
		$chart->legend->align                         = "right";
		$chart->legend->x                             = - 120;
		$chart->legend->verticalAlign                 = "bottom";
		$chart->legend->shadow                        = false;
		$chart->credits->enabled                      = false;

		$chart->tooltip = [
				'headerFormat' => '<b>{point.x}</b><br/>',
				'pointFormat' => '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
			];



		$chart->plotOptions->column->stacking = "normal";
		$chart->plotOptions->column->dataLabels->enabled = 1;

		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => $nilai_penjualan
		);

		$chart->series[] = array(
		    'name' => "Laba Kotor",
		    'data' => $nilai_penjualan
		);

		$chart->series[] = array(
		    'name' => "Laba Bersih",
		    'data' => $nilai_penjualan
		);
		return $chart->renderOptions();
	}

	//Escrow
	public function postChartreducedod(Request $request)
	{
		// }
		// dd($request->all());
		$from           = \Carbon\Carbon::parse($request->start);
		$to           	= \Carbon\Carbon::parse($request->end);
		$diff_in_days	= $to->diffInDays($from);
		// dd($to);
		$arr = [];
		$arr_a = [];
		$orr = [];
		$nilai_setoran   = [];
		$nilai_penjualan = [];
		$a_i = 0;
		for($i=1; $i<=$diff_in_days; $i++) {
		    $arr[] = date('Y-m-d', strtotime($from->format("Ymd") . ' + '.$i.'days' ));
		    $arr_a[date('Y-m-d', strtotime($from->format("Ymd") . ' + '.$i.'days' ))] = $a_i;
		    $arr_s[date('Y-m-d', strtotime($from->format("Ymd") . ' + '.$i.'days' ))] = $a_i;
			$nilai_setoran[]   = 0;
			$nilai_penjualan[] = 0;
		    $a_i++;
		 }

		// dd($end);
		$penjualan = TransRekapPenjualanTmuk::from(\DB::raw("(
			select to_char(tanggal, 'YYYY-MM-DD') as tanggal, sum(total) as total
			from \"trans_rekap_penjualan\" 
			where to_char(tanggal, 'YYYY-MM-DD') BETWEEN '".$from->format("Y-m-d")."' and '".$to->format("Y-m-d")."' AND  tmuk_kode like '%".$request->tmuk_code."%'
			group by \"tanggal\"
		) tbl"))
		->get();
		// dd($penjualan->toArray());

		$setoran = TransTopupEscrow::from(\DB::raw("(
			select to_char(tanggal, 'YYYY-MM-DD') as tanggal, sum(jumlah) as total
			from \"trans_topup_escrow\" 
			where jenis = 1 and to_char(tanggal, 'YYYY-MM-DD') BETWEEN '".$from->format("Y-m-d")."' and '".$to->format("Y-m-d")."' AND  tmuk_kode like '%".$request->tmuk_code."%'
			group by \"tanggal\"
		) tbl"))
		->get();

		// $region = '';
		// $lsi    = '';
		// $tmuk   = '';
		// $clones = clone $setoran;
		// $dd     = $clones->first();

		// if($dd->tmuk){
		// 	$region = $dd->tmuk->lsi->region->area;
		// 	$lsi    = $dd->tmuk->lsi->nama;
		// 	$tmuk   = $dd->tmuk->nama;
		// }
		$tmuk = TransRekapPenjualanTmuk::from(\DB::raw("(
			SELECT
			tmuk_kode
			FROM 
			trans_rekap_penjualan
			where tmuk_kode like '%".$request->tmuk_code."%'
			GROUP BY tmuk_kode
		) tbl"));
		$dd = $tmuk->first();
		$region = $dd->tmuk->lsi->region->area;
		$lsi    = $dd->tmuk->lsi->nama;
		$tmuk   = $dd->tmuk->nama;



		foreach ($penjualan as $vals) {
			$nilai_setoran[$arr_a[$vals->tanggal]]   = (integer)$vals->total;	
		}
		// dd($nilai_setoran);
		foreach ($setoran as $val) {
			$nilai_penjualan[$arr_s[$val->tanggal]] = (integer)$val->total;	
		}
		// dd($nilai_penjualan);
		$chart = new Highchart();
		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
		$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		$chart->title->text = "Penjualan vs Setoran Penjualan TMUK <br> ".$r_def;
		$chart->xAxis->categories = $arr;


		$chart->legend->x               = 40;
		$chart->legend->y               = 20;
		$chart->yAxis->title->text      = "(Rp)";
		$chart->credits->enabled        = false;


		$chart->plotOptions->column->pointPadding = 0.2;
		// $chart->plotOptions->column->borderWidth = 0;
		$chart->plotOptions->column->dataLabels->enabled = 1;

		$chart->plotOptions->column->borderWidth = 0;
		
		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => $nilai_penjualan
		);
		$chart->series[] = array(
		    'name' => "Setoran",
		    'data' => $nilai_setoran
		);

		return $chart->renderOptions();
	}

	public function postChartreducemom(Request $request)
	{
		// dd($request->all());	
		$from           = \Carbon\Carbon::createFromFormat('M Y', $request->start);
		$to             = \Carbon\Carbon::createFromFormat('M Y', $request->end);
		$diff_in_months = $to->diffInMonths($from);
		$header         = [];

		// dd($from);
		$start          = ($from->format('Y').'-'.$from->format('m').'-1');
		$end            = (($to->format('Y').'-'.$to->format('m').'-'.cal_days($to->format('m'),$to->format('Y'))));

		// dd($start);
		$penjualan = TransRekapPenjualanTmuk::from(\DB::raw("(
			select extract(month from tanggal) as bulan, 
			extract(year from tanggal) as tahun, sum(total) as total
			from \"trans_rekap_penjualan\" 
			where tanggal BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
			group by \"bulan\", \"tahun\"
		) tbl"))
		->get();

		$setoran = TransTopupEscrow::from(\DB::raw("(
			select extract(month from tanggal) as bulan, 
			extract(year from tanggal) as tahun, sum(jumlah) as total
			from \"trans_topup_escrow\" 
			where jenis = 1 AND  tanggal BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
			group by \"bulan\", \"tahun\"
		) tbl"))
		->get();

		// dd($setoran);

		// foreach ($penjualan as $data) {
		// 	$region = $data->tmuk->lsi->region->area;
		// 	$lsi = $data->tmuk->lsi->nama;
		// 	$tmuk = $data->tmuk->nama;
		// }

		$tmuk = TransRekapPenjualanTmuk::from(\DB::raw("(
			SELECT
			tmuk_kode
			FROM 
			trans_rekap_penjualan
			where tmuk_kode like '%".$request->tmuk_code."%'
			GROUP BY tmuk_kode
		) tbl"));
		$dd = $tmuk->first();
		$region = $dd->tmuk->lsi->region->area;
		$lsi    = $dd->tmuk->lsi->nama;
		$tmuk   = $dd->tmuk->nama;

		// dd($penjualan);
		// 
		$arr_penjualan   = [];
		$arr_setoran   = [];
		$nilai_penjualan = [];
		$nilai_setoran = [];

		for($i=0; $i<=$diff_in_months; $i++) {
			$header[]                                                             = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ) );
			$arr_penjualan[date('m-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ))] = $i;
			$nilai_penjualan[]                                                              = 0;
		}
		foreach ($penjualan as $val) {
			$bulan = str_pad($val['bulan'], 2, "0", STR_PAD_LEFT);
			$nilai_penjualan[$arr_penjualan[$bulan.'-'.$val->tahun]] = (int)$val->total;
		}

		for($i=0; $i<=$diff_in_months; $i++){
			$header[]                                                             = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ) );
			$arr_setoran[date('m-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ))] = $i;
			$nilai_setoran[]                                                              = 0;
		}
		foreach ($setoran as $val) {
			$bulan = str_pad($val['bulan'], 2, "0", STR_PAD_LEFT);
			$nilai_setoran[$arr_setoran[$bulan.'-'.$val->tahun]] = (int)$val->total;
		}


		
		$chart = new Highchart();
		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
		$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		// $chart->title->text = "Penjualan vs Setoran Penjualan TMUK <br> Region $region / LSI $lsi / TMUK $tmuk";
		$chart->title->text = "Penjualan vs Setoran Penjualan TMUK <br> ". $r_def;
		$chart->xAxis->categories = $header;


		$chart->legend->x               = 40;
		$chart->legend->y               = 20;
		$chart->yAxis->title->text      = "(Rp)";
		$chart->credits->enabled        = false;


		$chart->plotOptions->column->pointPadding = 0.2;
		// $chart->plotOptions->column->borderWidth = 0;
		$chart->plotOptions->column->dataLabels->enabled = 1;

		$chart->plotOptions->column->borderWidth = 0;
		
		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => $nilai_penjualan
		);

		$chart->series[] = array(
		    'name' => "Setoran",
		    'data' => $nilai_setoran
		);

		return $chart->renderOptions();
	}

	public function postEscrowBalance(Request $request){
		$arr = [];
		for($i=1; $i<=5; $i++) {
		    $arr[] = date('d', strtotime(date('Ymd') . ' - '.$i.'days' ));
		    $orr[date('d', strtotime(date('Ymd') . ' - '.$i.'days' ))] = date('d/m', strtotime(date('Ymd') . ' - '.$i.'days'  ));
		}
		// dd($orr);
		krsort($arr);

// dd($orr);

		$rec = TransReduceEscrow::select(\DB::raw("extract(days from tanggal) as hari"),'trans_reduce_escrow.saldo_escrow as escrow', 'ref_tmuk.kode as kode', 'ref_tmuk.nama as nama', 'trans_reduce_escrow.tanggal as tanggal', 'ref_tmuk.created_at as tgl_tmuk')
		->rightJoin('ref_tmuk', 'trans_reduce_escrow.tmuk_kode', '=', 'ref_tmuk.kode')
		->orderBy('tgl_tmuk', 'ASC')->get();

		// dd($rec);

		$temp = [];
		foreach ($rec->toArray() as $val) {
			$temp[$val['kode']] = $val;
		}

		// dd($rec);
		//REDUCE ESCROW


		$data_nilai = [];

		//nilai
		foreach ($rec->toArray() as $nilai) {
			$data_nilai[$nilai['kode'].'-'.$nilai['hari']][$nilai['hari']] = $nilai['escrow'];
		}

		$data_temp = [];
		$nilai     = [];
		//header reduce
		$header_reduce_scrow =[];

		foreach ($arr as $val) {
			$header_reduce_scrow[] = $orr[$val];
		}
		//content
		foreach ($temp as $rows) {
			$hari = [];
			$prev = $rec->filter(function ($item) use ($arr, $rows){
					    return $item['hari'] < $arr[0] && $item['kode'] == $rows['kode'];
					})->first();
			$n    = $prev ? rupiah($prev->escrow) : 0;
			foreach ($arr as $i) {
				$n = isset($data_nilai[$rows['kode'].'-'.$i][$i])? rupiah($data_nilai[$rows['kode'].'-'.$i][$i]):$n;
				$hari[] = $n;
			}
			$data_temp[] = [
				'nama' => $rows['nama'],
				'kode' => $rows['kode'],
				'nilai'=> $hari
			];		
		}

		$datas = [
			'header_reduce' => $header_reduce_scrow,
			'reduce' => $data_temp,
			'bulan' => $orr
		];
		return $this->render('modules.dashboard.finance.table.escrow-balance', $datas);
	}
}