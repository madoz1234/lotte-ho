<?php

namespace Lotte\Http\Controllers\Beranda;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Master\Tmuk;


class MonitoringController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('modules.dashboard.monitoring');
	}

	public function getGrid()
	{
		$last_sync 		= Tmuk::select('*')
							->orderBy('last_online','desc')
							->get();



		// dd($picking_task);

		return view('modules.dashboard.monitoring', ['tmuk'=>$last_sync]);
	}
}