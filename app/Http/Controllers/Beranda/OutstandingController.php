<?php

namespace Lotte\Http\Controllers\Beranda;
use Lotte\Http\Controllers\Controller;

use Illuminate\Http\Request;

//Libraries;
use DB;
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransSuratJalan;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Master\Tmuk;


class OutstandingController extends Controller
{

	public function index()
	{
        return $this->render('modules.dashboard.outstanding');
	}

	public function getGrid()
	{
		$picking_task 	= TransPr::with('tmuk')->orderBy('status', 'ASC')->where('status', 0)->get();

		$picking_result = TransPr::with('tmuk')->orderBy('status', 'ASC')->where('status', 1)->get();

		$kontainer 		= TransKontainer::select('*')->get();

		$last_sync 		= Tmuk::select('*')->get();



		// dd($picking_task);

		return view('modules.dashboard.outstanding', ['picking_task' => $picking_task, 'picking_result' => $picking_result, 'kontainer' => $kontainer,'tmuk'=>$last_sync]);
	}
}