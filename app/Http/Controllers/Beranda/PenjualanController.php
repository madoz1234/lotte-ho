<?php

namespace Lotte\Http\Controllers\Beranda;
use Lotte\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Datatables;
use Carbon\Carbon;
use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartOptions;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransReduceEscrowDetail;

class PenjualanController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('modules.dashboard.finance');
	}

	public function getGrid()
	{
		// $reduce = TransReduceEscrow::select('*')->get();
		$now = date('Y-m').'-'.(date('d')-1);
		$old = date('d')-8;

		$record = TransReduceEscrow::select(\DB::raw("extract(days from tanggal) as hari"),'trans_reduce_escrow.saldo_escrow as escrow', 'ref_tmuk.kode as kode', 'ref_tmuk.nama as nama', 'trans_reduce_escrow.tanggal as tanggal')
		->join('ref_tmuk', 'trans_reduce_escrow.tmuk_kode', '=', 'ref_tmuk.kode')
		->orderBy('tanggal', 'DESC')->get()->toArray();

		$temp = [];
		foreach ($record as $val) {
			 	$temp[$val['kode']] = $val;
		}
		//REDUCE ESCROW


		$data_nilai = [];

		//nilai
		foreach ($record as $nilai) {
			$data_nilai[$nilai['kode'].'-'.$nilai['hari']][$nilai['hari']] = $nilai['escrow'];
		}
		// dd($data_nilai);

		$date      = (int)date('d')-1;
		$sparate   = $date-7; 
		$data_temp = [];
		$nilai     = [];
		//header reduce
		$header_reduce_scrow =[];
		for($i=$date; $i>$sparate ;$i--){
			$header_reduce_scrow[] = $i;
		}
		//content
		foreach ($temp as $rows) {
			$hari      = [];
			for($i=$date; $i>$sparate ;$i--){
				$hari[] = isset($data_nilai[$rows['kode'].'-'.$i][$i])? rupiah($data_nilai[$rows['kode'].'-'.$i][$i]):'-';
			}
			$data_temp[] = [
				'nama' => $rows['nama'],
				'kode' => $rows['kode'],
				'nilai'=> $hari
			];		
		}

        $data = [
				'header_reduce' => $header_reduce_scrow,
				'reduce'        => $data_temp,
            ];

		// dd($temp);

		return view('modules.dashboard.finance', $data);
	}

	// RENDER HAIGH CHART
	public function postCharpenjualantmom(Request $request)
	{
		// dd($request->all());
		$from           = \Carbon\Carbon::createFromFormat('M Y', $request->start);
		$to             = \Carbon\Carbon::createFromFormat('M Y', $request->end);
		$diff_in_months = $to->diffInMonths($from);
		$header         = [];

		$start          = ($from->format('Y').'-'.$from->format('m').'-1');
		$end            = (($to->format('Y').'-'.$to->format('m').'-'.cal_days($to->format('m'),$to->format('Y'))));
		// dd($start);
		$penjualan = TransRekapPenjualanTmuk::with('tmuk', 'tmuk.lsi')
		->select(\DB::raw("tanggal,
			extract(month from tanggal) as bulan, 
			extract(year from tanggal) as tahun,
			tmuk_kode,
			total 
			"));
		$penjualan->whereBetween('tanggal', [$start,$end]);
		if($request->tmuk_code){
				$penjualan->where('tmuk_kode', $request->tmuk_code);
		}

		$record = $penjualan->get();

		$tmuk = Tmuk::where('kode', $request->tmuk_code);
		$dd     = $tmuk->first();
		$region = 'Data tidak ditemukan';
		$lsi    = 'Data tidak ditemukan';
		$tmuk   = 'Data tidak ditemukan';
		if($dd){
			$region = $dd->lsi->region->area;
			$lsi    = $dd->lsi->nama;
			$tmuk   = $dd->nama;
		}

		// dd($tmuk);
		$arr   = [];
		$nilai = [];
		for($i=0; $i <= $diff_in_months; $i++) {
			$header[] = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ) );
			$arr[date('Y-m', strtotime($from->format('Ymd') . ' +'.$i.'month' ))] = $i;
			$nilai[]= 0;
		}
		foreach ($record as $val) {
			$bulan = str_pad($val['bulan'], 2, "0", STR_PAD_LEFT);
			$nilai[$arr[$val->tahun.'-'.$bulan]] += (int)$val->total;
		}
		$chart = new Highchart();

		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
				$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}

		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		$chart->title->text = "Kinerja Penjualan TMUK <br> ".$r_def;
		$chart->xAxis->categories = $header;
		$chart->yAxis->min         = 0;
		$chart->yAxis->title->text = "Penjualan (Rp)";
		$chart->credits->enabled   = false;
		$chart->legend->enabled    = false;
		// $chart->lang->dataLabels->enabled = 1;
		// $chart->lang->numericSymbols = [ "rb" , "RT" , "JT" , "M" , "T" , "E"];
		$chart->plotOptions->column->pointPadding = 0.2;
		$chart->plotOptions->column->dataLabels->enabled = 1;
	    // $chart->plotOptions->series =[
	    //     'series' => [
	    //         'borderWidth' => 0,
	    //         'dataLabels' => [
	    //             'enabled' => true,
	    //             'format' => '{point.y:.1f}%'
	    //         ]
	    //     ]
	    // ];
		$chart->plotOptions->column->borderWidth = 0;

		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => $nilai,
		    'color' => "#2185d0"
		);

		return $chart->renderOptions();
	}

	public function postCharpenjualantqoq(Request $request)
	{
		// dd($request->all());
		$start     = explode('.', $request->start);
		$end       = explode('.', $request->end);
		$starts    = $start[0].$start[1];
		$ends      = $end[0].$end[1];
		$penjualan = TransRekapPenjualanTmuk::select(\DB::raw("total, 
				extract(year from tanggal) as tahun,
				CEILING(extract(month from tanggal)/3) as triwulan
			"));
		$penjualan->whereBetween(\DB::raw("CAST(CONCAT(extract(year from tanggal), CEILING(extract(month from tanggal)/3)) as numeric(24,0))"), [$starts, $ends]);
		$tmuk_code = $request->tmuk_code==0?"" :$request->tmuk_code;
		if($tmuk_code){
			$penjualan->where('tmuk_kode', 'like' , '%'.$tmuk_code.'%');
		}

		$tmuk   = Tmuk::where('kode', $request->tmuk_code);
		$dd     = $tmuk->first();
		$region = 'Tidak tidak ditemukan';
		$lsi    = 'Tidak tidak ditemukan';
		$tmuk   = 'Tidak tidak ditemukan';
		if($dd){
			$region = $dd->lsi->region->area;
			$lsi    = $dd->lsi->nama;
			$tmuk   = $dd->nama;
		}


		$penjualan = $penjualan->get();
		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
		$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}
		// dd($penjualan);
		// dd($penjualan);
		$header = header_triwulan($request->start,$request->end);
		//nilai
		$nilai = [];
		$headers = [];

		foreach ($header as $key => $value) {
			$headers[]                 = $value['label'];
			$array[$value['triwulan']] = $key;
			$nilai[]               = 0;
		}
		// dd($nilai);
		// dd($header);
		foreach ($penjualan as $row) {
			$nilai[$array[$row->tahun.'.'.$row->triwulan]] += (integer)$row->total;
		}
		// dd($nilai);
		// dd($headers);

		// dd($penjualan->toArray());

		$chart                    = new Highchart();
		$chart->chart->renderTo   = "container";
		$chart->chart->type       = "column";
		$chart->title->text       = "Kinerja Penjualan TMUK <br>". $r_def;
		$chart->xAxis->categories = $headers;
		// $chart->xAxis->categories = ['tes', 'test2'];
		
		
		$chart->yAxis->min         = 0;
		$chart->yAxis->title->text = "Penjualan (Rp)";
		$chart->credits->enabled   = false;
		$chart->legend->enabled    = false;

		$chart->plotOptions->column->pointPadding = 0.2;
		$chart->plotOptions->column->borderWidth = 0;
		$chart->plotOptions->column->dataLabels->enabled = 1;

		// $chart->setOptions(array('lang' => array(
		//         'numericSymbols' => ['rt', 'RB', 'JT', 'M', 'TR'],
		//         'numericSymbolMagnitude' => 100000000
		// )));
		

		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => $nilai,
		    // 'data' => [1,2],
		    'color' => "#2185d0"
		);

		return $chart->renderOptions();
	}

	public function postCharpenjualantyoy(Request $request)
	{

		$from           = \Carbon\Carbon::createFromFormat('Y', $request->start);
		$to             = \Carbon\Carbon::createFromFormat('Y', $request->end);
		$diff_in_months = $to->diffInYears($from);
		$header         = [];

		$start          = ($from->format('Y').'-01-01');
		$end            = ($to->format('Y').'-12-'.cal_days(12,$to->format('Y')));

		
		$penjualan = TransRekapPenjualanTmuk::select(\DB::raw("
			extract(year from tanggal) as tahun, 
			total"));
		$penjualan->whereBetween('tanggal', [$start,$end]);
		if($request->tmuk_code !== 0 || $request->tmuk_code !== ''){
			$penjualan->where('tmuk_kode', 'like', '%'.$request->tmuk_code.'%');
		}

		$penjualan = $penjualan->get();

		$tmuk   = Tmuk::where('kode', $request->tmuk_code);
		$dd     = $tmuk->first();
		$region = 'Maaf data tidak ditemukan';
		$lsi    = 'Maaf data tidak ditemukan';
		$tmuk   = 'Maaf data tidak ditemukan';
		if($dd){
			$region = $dd->lsi->region->area;
			$lsi    = $dd->lsi->nama;
			$tmuk   = $dd->nama;
		}


		$arr   = [];
		$nilai = [];
		for($i=0; $i<=$diff_in_months; $i++) {
			$header[] = date('Y', strtotime($from->format('Y') . ' -'.$i.'year' ));
			$arr[date('Y', strtotime($from->format('Y') . ' -'.$i.'year' ))] = $i;
			$nilai[]= 0;
		}
		foreach ($penjualan as $val) {
			$nilai[$arr[$val['tahun']]] += (int)$val->total;
		}
		// dd($header);

		$chart = new Highchart();
		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
		$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		$chart->title->text = "Kinerja Penjualan TMUK <br> ".$r_def;
		$chart->xAxis->categories = $header;
		
		$chart->yAxis->min         = 0;
		$chart->yAxis->title->text = "Penjualan (Rp)";
		$chart->credits->enabled   = false;
		$chart->legend->enabled    = false;

		$chart->plotOptions->column->pointPadding = 0.2;
		$chart->plotOptions->column->borderWidth = 0;
		$chart->plotOptions->column->dataLabels->enabled = 1;

		// $chart->setOptions(array('lang' => array(
		//         'numericSymbols' => ['rt', 'RB', 'JT', 'M', 'TR'],
		//         'numericSymbolMagnitude' => 100000000
		// )));

		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => $nilai,
		    'color' => "#2185d0"
		);

		return $chart->renderOptions();
	}

	// fungsi pembelian
	public function postCharpembelianmom(Request $request)
	{
		// dd($request->all());
		$from           = \Carbon\Carbon::createFromFormat('M Y', $request->start);
		$to             = \Carbon\Carbon::createFromFormat('M Y', $request->end);
		$diff_in_months = $to->diffInMonths($from);
		$header         = [];

		$start          = ($from->format('Y').'-'.$from->format('m').'-1');
		$end            = (($to->format('Y').'-'.$to->format('m').'-'.cal_days($to->format('m'),$to->format('Y'))));

		$pembelian = TransReduceEscrow::select(\DB::raw("
				tmuk_kode, 
				extract(month from tanggal) as bulan, 
				extract(year from tanggal) as tahun, 
				saldo_dipotong as total
			"));

		$pembelian->whereBetween('tanggal', [$start,$end]);
		if($request->tmuk_code){
				$pembelian->where('tmuk_kode', $request->tmuk_code);
		}

		// $record = $penjualan->get();
		// }
		$record = $pembelian->get();
		
		$tmuk = Tmuk::where('kode' , 'like', '%'.$request->tmuk_code.'%');
		
		$dd     = $tmuk->first();
		$region ='Data tidak ditemukan';
		$lsi    ='Data tidak ditemukan';
		$tmuk   ='Data tidak ditemukan';
		if($dd){
			$region = $dd->lsi->region->area;
			$lsi    = $dd->lsi->nama;
			$tmuk   = $dd->nama;
		}
		
		$arr   = [];
		$nilai = [];
		for($i=0; $i<=$diff_in_months; $i++) {
			$header[]                                                             = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ) );
			$arr[date('m-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ))] = $i;
			$nilai[]                                                              = 0;
		}
		foreach ($record as $val) {
			$bulan = str_pad($val['bulan'], 2, "0", STR_PAD_LEFT);
			$nilai[$arr[$bulan.'-'.$val->tahun]] += (int)$val->total;
		}
		$chart = new Highchart();
		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
		$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		$chart->title->text = "Kinerja Pembelian TMUK <br> ".$r_def;
		$chart->xAxis->categories = $header;
		
		$chart->yAxis->min         = 0;
		$chart->yAxis->title->text = "Pembelian (Rp)";
		$chart->credits->enabled   = false;
		$chart->legend->enabled    = false;

		$chart->plotOptions->column->pointPadding = 0.2;
		$chart->plotOptions->column->borderWidth = 0;
		$chart->plotOptions->column->dataLabels->enabled = 1;
		// $chart->setOptions(array('lang' => array(
		//         'numericSymbols' => ['rt', 'RB', 'JT', 'M', 'TR'],
		//         'numericSymbolMagnitude' => 100000000
		// )));

		$chart->series[] = array(
		    'name' => "Pembelian",
		    'data' => $nilai,
		    'color' => "#79b5e2"
		);

		return $chart->renderOptions();
	}

	public function postCharpembelianqoq(Request $request)
	{
				// dd($request->all());
		$start     = explode('.', $request->start);
		$end       = explode('.', $request->end);
		$starts    = $start[0].$start[1];
		$ends      = $end[0].$end[1];
		$tmuk_code = $request->tmuk_code==0?"" :$request->tmuk_code;
		$pembelian = TransReduceEscrow::select(\DB::raw("
				extract(year from tanggal) as tahun,
				CEILING(extract(month from tanggal)/3) as triwulan,
				saldo_dipotong as total
			"));
		$pembelian->whereBetween(\DB::raw("CAST(CONCAT(extract(year from tanggal), CEILING(extract(month from tanggal)/3)) as numeric(24,0))"), [$starts, $ends]);
		$tmuk_code = $request->tmuk_code==0?"" :$request->tmuk_code;
		if($tmuk_code){
			$pembelian->where('tmuk_kode', 'like' , '%'.$tmuk_code.'%');
		}

		$record = $pembelian->get();
		$tmuk   = Tmuk::where('kode', $request->tmuk_code);
		$dd     = $tmuk->first();

		$region = 'Data tidak ditemukan';
		$lsi    = 'Data tidak ditemukan';
		$tmuk   = 'Data tidak ditemukan';
		if($dd){
			$region = $dd->lsi->region->area;
			$lsi    = $dd->lsi->nama;
			$tmuk   = $dd->nama;
		}

		// dd($pembelian);
		// dd($pembelian);
		$header = header_triwulan($request->start,$request->end);
		//nilai
		$nilai = [];
		$headers = [];

		foreach ($header as $key => $value) {
			$headers[]                 = $value['label'];
			$array[$value['triwulan']] = $key;
			$nilai[$key]               = 0;
		}
		// dd($header);
		foreach ($record as $row) {
			$nilai[$array[$row->tahun.'.'.$row->triwulan]] += (integer)$row->total;
		}


		$chart = new Highchart();
		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
		$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}

		$chart->chart->renderTo   = "container";
		$chart->chart->type       = "column";
		$chart->title->text       = "Kinerja Pembelian TMUK <br>".$r_def;
		$chart->xAxis->categories = $headers;
		
		$chart->yAxis->min         = 0;
		$chart->yAxis->title->text = "Penjualan (Rp)";
		$chart->credits->enabled   = false;
		$chart->legend->enabled    = false;

		$chart->plotOptions->column->pointPadding = 0.2;
		$chart->plotOptions->column->borderWidth = 0;
		$chart->plotOptions->column->dataLabels->enabled = 1;
		// $chart->setOptions(array('lang' => array(
		//         'numericSymbols' => ['rt', 'RB', 'JT', 'M', 'TR'],
		//         'numericSymbolMagnitude' => 100000000
		// )));

		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => $nilai,
		    'color' => "#79b5e2"
		);

		return $chart->renderOptions();
	}

	public function postCharpembelianyoy(Request $request)
	{
		// dd($request->all());
		$from           = \Carbon\Carbon::createFromFormat('Y', $request->start);
		$to             = \Carbon\Carbon::createFromFormat('Y', $request->end);
		$diff_in_months = $to->diffInYears($from);
		$header         = [];

		$start          = ($from->format('Y').'-01-01');
		$end            = ($to->format('Y').'-12-'.cal_days(12,$to->format('Y')));

		// $penjualan = TransRekapPenjualanTmuk::select(\DB::raw("
		// 	extract(year from tanggal) as tahun, 
		// 	total"));
		// $penjualan->whereBetween('tanggal', [$start,$end]);
		// if($request->tmuk_code !== 0 || $request->tmuk_code !== ''){
		// 	$penjualan->where('tmuk_kode', 'like', '%'.$request->tmuk_code.'%');
		// }

		$pembelian = TransReduceEscrow::select(\DB::raw("
			extract(year from tanggal) as tahun, 
			saldo_dipotong as total"));
		$pembelian->whereBetween('tanggal', [$start,$end]);
		if($request->tmuk_code !== 0 || $request->tmuk_code !== ''){
			$pembelian->where('tmuk_kode', 'like', '%'.$request->tmuk_code.'%');
		}

		$pembelian = $pembelian->get();


		$tmuk   = Tmuk::where('kode', $request->tmuk_code);
		$dd     = $tmuk->first();
		$region = 'Maaf data tidak ditemukan';
		$lsi    = 'Maaf data tidak ditemukan';
		$tmuk   = 'Maaf data tidak ditemukan';
		if($dd){
			$region = $dd->lsi->region->area;
			$lsi    = $dd->lsi->nama;
			$tmuk   = $dd->nama;
		}


		$arr   = [];
		$nilai = [];
		for($i=0; $i<=$diff_in_months; $i++) {
			$header[] = date('Y', strtotime($from->format('Y') . ' -'.$i.'year' ));
			$arr[date('Y', strtotime($from->format('Y') . ' -'.$i.'year' ))] = $i;
			$nilai[]= 0;
		}
		foreach ($pembelian as $val) {
			$nilai[$arr[$val['tahun']]] += (int)$val->total;
		}
		// dd($header);

		$chart = new Highchart();
		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
		$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		$chart->title->text = "Kinerja Penjualan TMUK <br> ".$r_def;
		$chart->xAxis->categories = $header;
		
		$chart->yAxis->min         = 0;
		$chart->yAxis->title->text = "Penjualan (Rp)";
		$chart->credits->enabled   = false;
		$chart->legend->enabled    = false;

		$chart->plotOptions->column->pointPadding = 0.2;
		$chart->plotOptions->column->borderWidth = 0;
		$chart->plotOptions->column->dataLabels->enabled = 1;

		// $chart->setOptions(array('lang' => array(
		//         'numericSymbols' => ['rt', 'RB', 'JT', 'M', 'TR'],
		//         'numericSymbolMagnitude' => 100000000
		// )));

		$chart->series[] = array(
		    'name' => "Penjualan",
		    'data' => $nilai,
		    'color' => "#2185d0"
		);

		return $chart->renderOptions();
	}


	public function postChariolevel(Request $request){
		// dd($request->all());
		$from           = \Carbon\Carbon::createFromFormat('M Y', $request->start);
		$to             = \Carbon\Carbon::createFromFormat('M Y', $request->end);
		$diff_in_months = $to->diffInMonths($from);
		$header         = [];

		$start          = ($from->format('Y').'-'.$from->format('m').'-1');
		$end            = (($to->format('Y').'-'.$to->format('m').'-'.cal_days($to->format('m'),$to->format('Y'))));


		// $io_pembelian = TransReduceEscrow::from(\DB::raw("(SELECT
		// 		extract(MONTH from tanggal) as bulan, 
		// 		extract(YEAR from tanggal) as tahun,
		// 		sum(trans_reduce_escrow_detail.total_belanja) as total,
		// 		trans_reduce_escrow.tmuk_kode
		// 		FROM
		// 		trans_reduce_escrow
		// 		INNER JOIN trans_reduce_escrow_detail ON trans_reduce_escrow_detail.reduce_escrow_id=  trans_reduce_escrow.id
		// 		where tanggal BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
		// 		 GROUP BY bulan, tahun, tmuk_kode  )  d"))->get();
		// // ===== 
	
		// $io_penjualan = Tmuk::from(\DB::raw("(SELECT
		// 		extract(MONTH from tanggal) as bulan, 
		// 		extract(YEAR from tanggal) as tahun,
		// 		sum(trans_rekap_penjualan.total) as total,
		// 		trans_rekap_penjualan.tmuk_kode
		// 		FROM
		// 		ref_tmuk
		// 		INNER JOIN trans_rekap_penjualan ON trans_rekap_penjualan.tmuk_kode=  ref_tmuk.kode
		// 		where tanggal BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
		// 		 GROUP BY bulan, tahun, tmuk_kode   )  d"))->get();


		// $rcd = TransReduceEscrow::from(\DB::raw("(SELECT
		// 		trans_reduce_escrow.tmuk_kode, extract(MONTH from trans_reduce_escrow.tanggal) as bulan, extract(YEAR from trans_reduce_escrow.tanggal) as tahun,
		// 		sum(trans_reduce_escrow.saldo_dipotong::decimal) / sum(trans_rekap_penjualan.total::decimal) *100 as total

		// 		from trans_reduce_escrow inner join trans_rekap_penjualan on trans_reduce_escrow.tmuk_kode= trans_rekap_penjualan.tmuk_kode
		// 		inner join ref_tmuk on trans_rekap_penjualan.tmuk_kode = ref_tmuk.kode
		// 		where trans_reduce_escrow.tanggal BETWEEN '".$start."' and '".$end."' AND trans_reduce_escrow.tmuk_kode like '%".$request->tmuk_code."%'
		// 		GROUP BY trans_reduce_escrow.tmuk_kode, bulan, tahun   )  d"))->get();
		// 		sum(trans_reduce_escrow.saldo_dipotong::decimal) / (sum(trans_rekap_penjualan.total::decimal)+sum(trans_reduce_escrow.saldo_dipotong::decimal)) * 100 as total

		$rcd = TransReduceEscrow::from(\DB::raw("(SELECT
				trans_reduce_escrow.tmuk_kode, extract(MONTH from trans_reduce_escrow.tanggal) as bulan, extract(YEAR from trans_reduce_escrow.tanggal) as tahun,
				sum(trans_reduce_escrow.saldo_dipotong::decimal) / (sum(trans_rekap_penjualan.total::decimal)+sum(trans_reduce_escrow.saldo_dipotong::decimal)) * 100 as total

				from trans_reduce_escrow inner join trans_rekap_penjualan on trans_reduce_escrow.tmuk_kode= trans_rekap_penjualan.tmuk_kode
				inner join ref_tmuk on trans_rekap_penjualan.tmuk_kode = ref_tmuk.kode
				where trans_reduce_escrow.tanggal BETWEEN '".$start."' and '".$end."' AND trans_reduce_escrow.tmuk_kode like '%".$request->tmuk_code."%'
				GROUP BY trans_reduce_escrow.tmuk_kode, bulan, tahun   )  d"))->get();

		// dd()

		$tmuk = TransPo::from(\DB::raw("(
			SELECT
			tmuk_kode
			FROM 
			trans_pr
			where tgl_buat BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
			GROUP BY tmuk_kode
		) tbl"));
		$dd = $tmuk->first();
		$region = $dd->tmuk->lsi->region->area;
		$lsi    = $dd->tmuk->lsi->nama;
		$tmuk   = $dd->tmuk->nama;

		
		// dd($rcd);

		$arr   = [];
		$nilai = [];
		for($i=0; $i<=$diff_in_months; $i++) {
			$header[]                                                             = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ) );
			$arr[date('m-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ))] = $i;
			$nilai[]                                                              = 0;
		}
		foreach ($rcd as $val) {
			$bulan = str_pad($val['bulan'], 2, "0", STR_PAD_LEFT);
			$nilai[$arr[$bulan.'-'.$val->tahun]] = (int)$val->total;
		}

		$chart = new Highchart();
		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
		$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		$chart->title->text = "Kinerja I/O Level TMUK <br> ". $r_def;
		// $chart->title->text = "Kinerja I/O TMUK <br> Region $region / LSI $lsi / TMUK $tmuk";
		$chart->xAxis->categories = $header;
		
		$chart->yAxis->min         = 0;
		$chart->yAxis->title->text = "I/O Level (%)";
		$chart->credits->enabled   = false;
		$chart->legend->enabled    = false;

		$chart->plotOptions->column->pointPadding = 0.2;
		$chart->plotOptions->column->borderWidth = 0;
		$chart->plotOptions->column->dataLabels->enabled = 1;
		$chart->plotOptions->column->dataLabels->format = '{point.y:.1f} %';
	    // $chart->plotOptions->series =[
	    //     'series' => [
	    //         'borderWidth' => 0,
	    //         'dataLabels' => [
	    //             'enabled' => true,
	    //             'format' => '{point.y:.1f}%'
	    //         ]
	    //     ]
	    // ];
		$chart->plotOptions->column->borderWidth = 0;
		$chart->series[] = array(
		    'name' => "I/O Level",
		    'data' => $nilai,
		    'color' => '#ed1c23'
		);

		return $chart->renderOptions();
	}

	public function postCharservicelevel(Request $request){
		$from           = \Carbon\Carbon::createFromFormat('M Y', $request->start);
		$to             = \Carbon\Carbon::createFromFormat('M Y', $request->end);
		$diff_in_months = $to->diffInMonths($from);
		$header         = [];

		$start          = ($from->format('Y').'-'.$from->format('m').'-1');
		$end            = (($to->format('Y').'-'.$to->format('m').'-'.cal_days($to->format('m'),$to->format('Y'))));
		// dd($start);

		// $rcd = TransPo::from(\DB::raw("(SELECT
		// 		extract(MONTH from tgl_buat) as bulan, 
		// 		extract(YEAR from tgl_buat) as tahun,
		// 		sum(trans_po_detail.qty_po::decimal) / sum(trans_po_detail.qty_pr::decimal) * 100 as total
		// 		FROM
		// 		trans_po
		// 		INNER JOIN trans_po_detail ON trans_po_detail.po_nomor =  trans_po.nomor
		// 		where tgl_buat BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
		// 		 GROUP BY bulan, tahun )  d"))->get();

		$rcd = TransPo::from(\DB::raw("(SELECT
				extract(MONTH from tgl_buat) as bulan, 
				extract(YEAR from tgl_buat) as tahun,
				sum(trans_po_detail.qty_po::decimal) / sum(trans_po_detail.qty_pr::decimal) * 100 as total,
				trans_po.tmuk_kode
				FROM
				trans_po
				INNER JOIN trans_po_detail ON trans_po_detail.po_nomor =  trans_po.nomor
				where tgl_buat BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
				 GROUP BY bulan, tahun, trans_po.tmuk_kode )  d"))->get();


		$tmuk = TransPo::from(\DB::raw("(
			SELECT
			tmuk_kode
			FROM 
			trans_pr
			where tgl_buat BETWEEN '".$start."' and '".$end."' AND tmuk_kode like '%".$request->tmuk_code."%'
			GROUP BY tmuk_kode
		) tbl"));
		$dd = $tmuk->first();
		$region = $dd->tmuk->lsi->region->area;
		$lsi    = $dd->tmuk->lsi->nama;
		$tmuk   = $dd->tmuk->nama;

		// dd($rcd);
		// foreach ($rcd as $data) {
		// 	$region = $data->tmuk->lsi->region->area;
		// 	$lsi = $data->tmuk->lsi->nama;
		// 	$tmuk = $data->tmuk->nama;
		// 	$data_po = $data->qty_po;
		// }


		// $dd = $rcd->first();
		// $po = $dd->qty_po;
		// $pr = $dd->qty_pr;

		// // dd($po);
		// $ser = $po / $pr;
		// $sl = $ser * 100 ;


		$arr   = [];
		$nilai = [];
		for($i=0; $i<=$diff_in_months; $i++) {
			$header[]                                                             = date('M-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ) );
			$arr[date('m-Y', strtotime($from->format('Ymd') . ' +'.$i.'month' ))] = $i;
			$nilai[]                                                              = 0;
		}
		foreach ($rcd as $val) {
			$bulan = str_pad($val['bulan'], 2, "0", STR_PAD_LEFT);
			$nilai[$arr[$bulan.'-'.$val->tahun]] = (int)$val->total;
		}

		$chart = new Highchart();
		$r_def = '';

		if($request->region_id !== "0" || $request->region_id !== "" ){
		$r_def .= "Region $region";
		}
		if($request->lsi_id !== "0" || $request->lsi_id !== "" ){
		$r_def .= "/ LSI $lsi";
		}
		if($request->tmuk_code !== "0" || $request->tmuk_code !== ""){
			$r_def .= "/ TMUK $tmuk";
		}
		if($request->region_id == "0" || $request->region_id == "" || $request->lsi_id == "0" || $request->lsi_id == "" || $request->tmuk_code == "0" || $request->tmuk_code == ""){
			$r_def = "<br> Semua TMUK";
		}
		$chart->chart->renderTo = "container";
		$chart->chart->type = "column";
		// $chart->title->text = "Kinerja Service Level TMUK <br> Region $region / LSI $lsi / TMUK $tmuk";
		$chart->title->text = "Kinerja Service Level TMUK <br> ".$r_def;
		$chart->xAxis->categories = $header;
		
		$chart->yAxis->min         = 0;
		$chart->yAxis->title->text = "Service Level (%)";
		$chart->credits->enabled   = false;
		$chart->legend->enabled    = false;

		$chart->plotOptions->column->pointPadding = 0.2;
		$chart->plotOptions->column->dataLabels->enabled = 1;
		$chart->plotOptions->column->dataLabels->format = '{point.y:.1f} %';
	    // $chart->plotOptions->series =[
	    //     'series' => [
	    //         'borderWidth' => 0,
	    //         'dataLabels' => [
	    //             'enabled' => true,
	    //             'format' => '{point.y:.1f}%'
	    //         ]
	    //     ]
	    // ];
		$chart->plotOptions->column->borderWidth = 0;

		$chart->series[] = array(
		    'name' => "Service Level",
		    'data' => $nilai,
		    'color' => ' #f4767b'
		);

		return $chart->renderOptions();
	}


}