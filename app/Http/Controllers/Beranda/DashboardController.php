<?php

namespace Lotte\Http\Controllers\Beranda;

use Illuminate\Http\Request;
use DB;
use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransSuratJalan;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Kustomer;
use Lotte\Models\Master\Chat;
use Lotte\Models\Master\Produk;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransTransReduceEscrowDetail;
use Lotte\Models\Trans\TransRekapPenjualanTmuk;
use Lotte\Models\Trans\TransRekapPenjualanTmukDetail;
use Lotte\Models\Master\Kki;
use Lotte\Models\Trans\TransSO;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransMuatanDetail;


class DashboardController extends Controller
{
	protected $link = 'dashboard/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle(auth()->user()->can('dashboard-')?"Dashboard & Statistik TMUK":'Dashboard');
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(auth()->user()->can('dashboard-')?['Dashboard' => '#', 'Monitoring' => '#']:['Dashboard' => '#']);
		$this->setTableStructOutstandingProdukSync([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'tmuk',
				'name' => 'tmuk',
				'label' => 'TMUK',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'last_sync',
				'name' => 'last_sync',
				'label' => 'Last Sync Produk dan Harga',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
		]);

		$this->setTableStructMonitoringOnline([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'tmuk',
				'name' => 'tmuk',
				'label' => 'Nama TMUK',
				'searchable' => false,
				'sortable' => false,
			],
			[
				'data' => 'terakhir',
				'name' => 'terakhir',
				'label' => 'Terakhir Aktif',
				'searchable' => false,
				'sortable' => false,
				'className' => "center aligned",
			],
			[
				'data' => 'status',
				'name' => 'status',
				'label' => 'Status',
				'searchable' => false,
				'sortable' => false,
				'className' => "center aligned",
			],
			[
				'data' => 'sinyal',
				'name' => 'sinyal',
				'label' => 'Sinyal',
				'searchable' => false,
				'sortable' => false,
				'className' => "center aligned",
			],
		]);

		$this->setTableStructOutstandingPr([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'lsi',
				'name' => 'lsi',
				'label' => 'LSI',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'tmuk',
				'name' => 'tmuk',
				'label' => 'TMUK',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'no_pr',
				'name' => 'no_pr',
				'label' => 'No PR',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'qty_pr',
				'name' => 'qty_pr',
				'label' => 'Qty PR',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'nilai_pr',
				'name' => 'nilai_pr',
				'label' => 'Nilai PR (Rp)',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
		]);

		$this->setTableStructOutstandingPicking([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'lsi',
				'name' => 'lsi',
				'label' => 'LSI',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'tmuk',
				'name' => 'tmuk',
				'label' => 'TMUK',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'no_picking',
				'name' => 'no_picking',
				'label' => 'No Picking',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'qty_pr',
				'name' => 'qty_pr',
				'label' => 'Qty PR',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'nilai_pr',
				'name' => 'nilai_pr',
				'label' => 'Nilai PR (Rp)',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
		]);

		$this->setTableStructOutstandingDelivery([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'lsi',
				'name' => 'lsi',
				'label' => 'LSI',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'tmuk',
				'name' => 'tmuk',
				'label' => 'TMUK',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'no_surat_jalan',
				'name' => 'no_surat_jalan',
				'label' => 'No Surat Jalan',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'qty_pr',
				'name' => 'qty_pr',
				'label' => 'Qty PR',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'nilai_dn',
				'name' => 'nilai_dn',
				'label' => 'Nilai Delivery Note (Rp)',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
		]);

		// $this->setTableStructFinanceSetoran([
		// 	[
		// 		'data' => 'num',
		// 		'name' => 'num',
		// 		'label' => '#',
		// 		'orderable' => false,
		// 		'searchable' => false,
		// 		'className' => "center aligned",
		// 		// 'width' => '40px',
		// 	],
		// 	/* --------------------------- */
		// 	[
		// 		'data' => 'tmuk',
		// 		'name' => 'tmuk',
		// 		'label' => 'TMUK',
		// 		'searchable' => false,
		// 		'sortable' => true,
		// 	],
		// 	[
		// 		'data' => 'penjualan',
		// 		'name' => 'penjualan',
		// 		'label' => 'Penjualan (Rp)',
		// 		'searchable' => false,
		// 		'sortable' => true,
		// 		'className' => "center aligned",
		// 	],
		// 	[
		// 		'data' => 'setoran',
		// 		'name' => 'setoran',
		// 		'label' => 'Setoran Penjualan',
		// 		'searchable' => false,
		// 		'sortable' => true,
		// 		'className' => "center aligned",
		// 	],
		// 	[
		// 		'data' => 'outstanding',
		// 		'name' => 'outstanding',
		// 		'label' => 'Outstanding',
		// 		'searchable' => false,
		// 		'sortable' => true,
		// 		'className' => "center aligned",
		// 	],
		// 	[
		// 		'data' => 'last_setor',
		// 		'name' => 'last_setor',
		// 		'label' => 'Last Setor',
		// 		'searchable' => false,
		// 		'sortable' => true,
		// 		'className' => "center aligned",
		// 	],
		// ]);

		$this->setTableStructPenjualanKategori([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			// [
			// 	'data' => 'tmuk',
			// 	'name' => 'tmuk',
			// 	'label' => 'TMUK',
			// 	'searchable' => false,
			// 	'sortable' => true,
			// ],
			[
				'data' => 'kategori_1',
				'name' => 'kategori_1',
				'label' => 'Category 1',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'total',
				'name' => 'total',
				'label' => 'Penjualan (Rp.)',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'sales',
				'name' => 'sales',
				'label' => 'Kontribusi dari Sales (%)',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			// [
			// 	'data' => 'kode',
			// 	'name' => 'kode',
			// 	'label' => 'Kode Produk',
			// 	'searchable' => false,
			// 	'sortable' => true,
			// 	'className' => "center aligned",
			// ],
		]);

		$this->setTableStructDeviasiPenjualan([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'nama',
				'name' => 'nama',
				'label' => 'TMUK',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'ii_penjualan_normal',
				'name' => 'ii_penjualan_normal',
				'label' => 'Penjualan (KKI)',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'penjualan_tmuk',
				'name' => 'penjualan_tmuk',
				'label' => 'Penjualan (Aktual)',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'selisih',
				'name' => 'selisih',
				'label' => 'Selisih (Rp.)',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'terget',
				'name' => 'terget',
				'label' => '% dari Target',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
		]);

		$this->setTableStructLastSoHarian([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'tmuk',
				'name' => 'tmuk',
				'label' => 'TMUK',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'harian_f',
				'name' => 'harian_f',
				'label' => 'So Harian yg Belum Ter-sync',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'harian_t',
				'name' => 'harian_t',
				'label' => 'Last SO Harian',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
		]);

		$this->setTableStructLastSoBulanan([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'tmuk',
				'name' => 'tmuk',
				'label' => 'TMUK',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'bulanan_f',
				'name' => 'bulanan_f',
				'label' => 'So Bulanan yg Belum Ter-sync',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'bulanan_t',
				'name' => 'bulanan_t',
				'label' => 'Last SO Bulanan',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
		]);

		$this->setTableStructStatistikPo([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'tahun',
				'name' => 'tahun',
				'label' => 'Tahun',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'qty',
				'name' => 'qty',
				'label' => 'qty PO/Bulanan',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'total',
				'name' => 'total',
				'label' => 'total PO/Bulan',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
		]);

		$this->setTableStructSetoranPenjualan([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */
			[
				'data' => 'tmuk',
				'name' => 'tmuk',
				'label' => 'Tahun',
				'searchable' => false,
				'sortable' => true,
			],
			[
				'data' => 'nilai_penjualan_tmuk',
				'name' => 'nilai_penjualan_tmuk',
				'label' => 'qty PO/Bulanan',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'nilai_topup',
				'name' => 'nilai_topup',
				'label' => 'total PO/Bulan',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'nilai_outstanding_pr',
				'name' => 'nilai_outstanding_pr',
				'label' => 'total PO/Bulan',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'last_setor',
				'name' => 'last_setor',
				'label' => 'total PO/Bulan',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
		]);

	}

	public function gridOutstandingProdukSync(Request $request)
	{
		$records = Tmuk::with('creator')
		->select('*');
        // $record = RekeningEscrow::select('*');
        // dd($record);

		//Init Sort
		if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
			$records->orderBy('created_at');
		}
		if ($tmuk_kode = $request->tmuk_kode) {
                        $records->where('kode', 'like', '%' . $tmuk_kode . '%');
                    }
		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('tmuk',function($record){
			return $record->nama;
		})
		->addColumn('last_sync', function ($record) {
			$string = '-';
			if($record->sync){
				$string = ''.Carbon::createFromFormat('Y-m-d H:i:s' , $record->sync->updated_at)->format('d/m/Y H:i').'';
			}
			return $string;
		})
		->make(true);
	}

	public function gridMonitoringOnline(Request $request)
	{
		$records = Tmuk::with('creator')
		->orderBy('last_online','desc')
		->select('*');
        // $record = RekeningEscrow::select('*');
        // dd($record);

		//Init Sort
		if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
			$records->orderBy('last_online');
		}
		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('tmuk',function($record){
			return $record->nama;
		})
		->addColumn('terakhir',function($record){
			$string = '-';
			if($record->last_online){
				$string = ''.Carbon::createFromFormat('Y-m-d H:i:s' , $record->last_online)->format('d/m/Y H:i').'';
			}
			return $string;
		})
		->addColumn('status', function ($record) {
			$string = '<button class="ui inverted tiny red active button">Offline</button>';
			if($record->last_online){
				// $string = '<button class="ui inverted tiny green active button">Online</button>';
				$awal  = date_create($record->last_online);
				$akhir = date_create(); // waktu sekarang
				$diff  = date_diff( $awal, $akhir );

				$menit = (int)$diff->i;
				if($menit<=1){
					$string = '<button class="ui inverted tiny green active button">Online</button>';
				}
			}
			return $string;	
		})
		->addColumn('sinyal', function ($record) {
			$string = '<div class="autumn" align="center"><i class="signal icon"></i></div>';
			if($record->last_online){
				$awal  = date_create($record->last_online);
				$akhir = date_create(); // waktu sekarang
				$diff  = date_diff( $awal, $akhir );

				$menit = (int)$diff->i;
				if($menit<=1){
					$string = '<i class="signal icon"></i>';
					// $string = '<button class="ui inverted tiny green active button">Online</button>';
				}
			}
			return $string;
		})
		->make(true);
	}

	public function gridOutstandingPr(Request $request)
	{

		// dd($request->all());
		$records = TransPr::with('creator')
		->where('status', 0) 
		->orderBy('created_at','desc')
		->select('*');
        // $record = RekeningEscrow::select('*');
        // dd($records)->get();

		//Init Sort
		if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
			$records->orderBy('created_at');
		}

		if ($tmuk_kode = $request->tmuk_kode) {
                        $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    }

		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('tmuk',function($record){
			return $record->tmuk->nama;
		})
		->addColumn('lsi',function($record){
			return $record->tmuk->lsi->nama;
		})
		->addColumn('no_pr', function ($record) {
			return $record->nomor;
		})
		->addColumn('qty_pr', function ($record) {
			return $record->detailpr->sum('qty_pr');
		})
		->addColumn('nilai_pr', function ($record) {
			return rupiah($record->total);
		})
		->make(true);
	}

	public function gridOutstandingPicking(Request $request)
	{
		$records = TransPo::with('creator')
		->where('status', 1)
		->orderBy('created_at','desc')
		->select('*');
        // $record = RekeningEscrow::select('*');
        // dd($record);

		//Init Sort
		if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
			$records->orderBy('created_at');
		}
		if ($tmuk_kode = $request->tmuk_kode) {
                        $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    }
		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('tmuk',function($record){
			return $record->pr->tmuk->nama;
		})
		->addColumn('lsi',function($record){
			return $record->pr->tmuk->lsi->nama;
		})
		->addColumn('no_picking', function ($record) {
			return $record->pr->nomor;
		})
		->addColumn('qty_pr', function ($record) {
			return $record->pr->detailpr->sum('qty_pr');
		})
		->addColumn('nilai_pr', function ($record) {
			return rupiah($record->pr->total);
		})
		->make(true);
	}

	public function gridOutstandingDelivery(Request $request)
	{
		$records = TransMuatanDetail::with('creator')
		->where('status',0)
		->select('*');
        // $record = RekeningEscrow::select('*');
        // dd($record);

		//Init Sort
		if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
			$records->orderBy('created_at');
		}
		if ($tmuk_kode = $request->tmuk_kode) {
                        $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    }
		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('tmuk',function($record){
			return $record->tmuk->nama;
		})
		->addColumn('lsi',function($record){
			return $record->tmuk->lsi->nama;
		})
		->addColumn('no_surat_jalan', function ($record) {
			return $record->muatan->suratjalan->nomor;
		})
		->addColumn('qty_pr', function ($record) {
			return $record->po->detail->sum('qty_po');
		})
		->addColumn('nilai_dn', function ($record) {
			return rupiah($record->po->total);
		})
		->make(true);
	}

	public function gridFinanceSetoran(Request $request)
	{
		$records = Tmuk::with('creator')
		->select('*');
        // $record = RekeningEscrow::select('*');
        // dd($record);

		//Init Sort
		if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
			$records->orderBy('created_at');
		}
		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('tmuk',function($record){
			return $record->nama;
		})
		->addColumn('penjualan',function($record){
			$string = '';
			return $string;
		})
		->addColumn('setoran', function ($record) {
			return '';
		})
		->addColumn('outstanding', function ($record) {
			return '';
		})
		->addColumn('last_setor', function ($record) {
			return '';
		})
		->make(true);
	}

	public function gridPenjualanKategori(Request $request)
	{


// SELECT 
// ref_produk.l1_nm as kategori, 
// ref_produk_setting.tipe_produk as tipe,
// (select sum(d.harga) as harga from trans_rekap_penjualan_detail d inner join ref_produk p on d.item_code=p.kode where p.l1_nm=ref_produk.l1_nm) as harga


// FROM ref_produk 
// INNER JOIN ref_produk_setting ON ref_produk_setting.produk_kode= ref_produk.kode where ref_produk_setting.tipe_produk=1

// GROUP BY kategori, tipe;

// select tmuk_pe.tmuk_kode as tmuk_kode, tmuk_dt.rekap_penjualan_kode as rekap_penjualan_kode 
// from trans_rekap_penjualan_detail tmuk_dt right join trans_rekap_penjualan tmuk_pe on tmuk_dt.rekap_penjualan_kode=tmuk_pe.jual_kode GROUP BY tmuk_kode, rekap_penjualan_kode;


// select sum(tmuk_d.harga) as harga from trans_rekap_penjualan_detail tmuk_d inner join trans_rekap_penjualan tmuk_p on tmuk_d.rekap_penjualan_kode=tmuk_p.jual_kode;

// select * from trans_rekap_penjualan_detail where rekap_penjualan_kode = 'CT-2018060206018000020242'


		// $record = TransRekapPenjualanTmuk::from(\DB::raw("(SELECT
  //       		ref_produk.l1_nm as kategori,
		//  		sum(trans_rekap_penjualan_detail.harga) AS harga,
  //       		count(trans_rekap_penjualan_detail.rekap_penjualan_kode) AS total,
  //       		sum(trans_rekap_penjualan_detail.harga::decimal) / (count(trans_rekap_penjualan_detail.rekap_penjualan_kode)+sum(trans_rekap_penjualan_detail.harga::decimal)) * 100 as sales,
		// 		ref_produk_setting.tipe_produk as tipe,
		// 		trans_rekap_penjualan.tmuk_kode as tmuk_kode
		// 		FROM
		//  		ref_produk
  //       		INNER JOIN ref_produk_setting ON ref_produk_setting.produk_kode= ref_produk.kode
		// 		INNER JOIN trans_rekap_penjualan_detail ON trans_rekap_penjualan_detail.item_code= ref_produk.kode
		// 		INNER JOIN trans_rekap_penjualan ON trans_rekap_penjualan_detail.rekap_penjualan_kode = trans_rekap_penjualan.jual_kode
		// 		where ref_produk_setting.tipe_produk = 1 AND tmuk_kode like '%".$request->tmuk_kode."%' GROUP BY kategori, tipe, tmuk_kode)  d"));



		// $record = TransRekapPenjualanTmuk::from(\DB::raw("(SELECT
  //       		ref_produk.l1_nm as kategori,
		//  		sum(trans_rekap_penjualan_detail.harga) AS harga,
  //       		count(trans_rekap_penjualan_detail.rekap_penjualan_kode) AS total,
  //       		trans_rekap_penjualan_detail.harga::decimal / (1+sum(trans_rekap_penjualan_detail.harga::decimal)) * 100 as sales,
		// 		ref_produk_setting.tipe_produk as tipe,
		// 		trans_rekap_penjualan.tmuk_kode as tmuk_kode
		// 		FROM
		//  		ref_produk
  //       		INNER JOIN ref_produk_setting ON ref_produk_setting.produk_kode= ref_produk.kode
		// 		INNER JOIN trans_rekap_penjualan_detail ON trans_rekap_penjualan_detail.item_code= ref_produk.kode
		// 		INNER JOIN trans_rekap_penjualan ON trans_rekap_penjualan_detail.rekap_penjualan_kode = trans_rekap_penjualan.jual_kode
		// 		where ref_produk_setting.tipe_produk = 1 AND tmuk_kode like '%".$request->tmuk_kode."%' GROUP BY kategori, tipe, tmuk_kode, trans_rekap_penjualan_detail.harga)  d"));

		// $record = TransRekapPenjualanTmuk::from(\DB::raw("(SELECT (sum(trans_rekap_penjualan_detail.harga) / (select sum(harga)  from trans_rekap_penjualan_detail ))*100 as sales,
  //       		ref_produk.l1_nm as kategori,
		//  		sum(trans_rekap_penjualan_detail.harga) AS harga,
  //       		count(trans_rekap_penjualan_detail.rekap_penjualan_kode) AS total,
		// 		ref_produk_setting.tipe_produk as tipe,
		// 		trans_rekap_penjualan.tmuk_kode as tmuk_kode
		// 		FROM
		//  		ref_produk
  //       		INNER JOIN ref_produk_setting ON ref_produk_setting.produk_kode= ref_produk.kode
		// 		INNER JOIN trans_rekap_penjualan_detail ON trans_rekap_penjualan_detail.item_code= ref_produk.kode
		// 		INNER JOIN trans_rekap_penjualan ON trans_rekap_penjualan_detail.rekap_penjualan_kode = trans_rekap_penjualan.jual_kode
		// 		where ref_produk_setting.tipe_produk = 1 AND tmuk_kode like '%".$request->tmuk_kode."%' GROUP BY kategori, tipe, tmuk_kode)  d"));

		$record = TransRekapPenjualanTmuk::from(\DB::raw("(SELECT (sum(trans_rekap_penjualan_detail.total) / (select sum(total)  from trans_rekap_penjualan_detail ))*100 as sales,
        		ref_produk.l1_nm as kategori,
		 		sum(trans_rekap_penjualan_detail.total) AS harga,
        		count(trans_rekap_penjualan_detail.rekap_penjualan_kode) AS total
				FROM
		 		ref_produk
        		INNER JOIN ref_produk_setting ON ref_produk_setting.produk_kode= ref_produk.kode
				INNER JOIN trans_rekap_penjualan_detail ON trans_rekap_penjualan_detail.item_code= ref_produk.kode
				INNER JOIN trans_rekap_penjualan ON trans_rekap_penjualan_detail.rekap_penjualan_kode = trans_rekap_penjualan.jual_kode
				where ref_produk_setting.tipe_produk = 1 AND trans_rekap_penjualan.tmuk_kode like '%".$request->tmuk_kode."%'  GROUP BY kategori)  d"));


		// $total = TransRekapPenjualanTmuk::from(\DB::raw("(select 
		// 		sum(trans_rekap_penjualan_detail.harga) as total, 
		// 		trans_rekap_penjualan.tmuk_kode as tmuk
		// 		from trans_rekap_penjualan_detail 
		// 		INNER JOIN trans_rekap_penjualan ON trans_rekap_penjualan.jual_kode = trans_rekap_penjualan_detail.rekap_penjualan_kode
		// 		where tmuk_kode like '%".$request->tmuk_kode."%'
		// 		GROUP BY tmuk_kode)  d"))->get();
		// $data = $total->first();
		// dd($gg->hrg);
		

		$data = clone $record;
		$records = $record->get();

		$data = $data->first();
		$data->tmuk_kode;
		$region ='Semua data';
		$lsi ='Semua data';
		$tmuk ='Semua data';
		if($request->tmuk_kode)
		{
			if($data->tmuk_kode = $request->tmuk_kode){
				$region = $data->tmuk->lsi->region->area;
				$lsi = $data->tmuk->lsi->nama;
				$tmuk = $data->tmuk->nama;
			}else if(!$data->tmuk_kode){
				$region = 'Tidak ada data';
				$lsi = 'Tidak ada data';
				$tmuk = 'Tidak ada data';
			}

		}


		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		// ->addColumn('tmuk', function ($record) {
		// 	return $record->tmuk->nama;
		// })
		->addColumn('kategori_1', function ($record) {
			return $record->kategori;
		})
		->addColumn('total', function ($record) {
			return Rupiah($record->harga);
		})
		->addColumn('sales', function ($record) {
			return FormatNumber($record->sales);

		// 	$target = '-';
		// if ($data->total != 0) {
		// 	$target = $data->harga / ($data->total) *100;
		// 	} else {
		// 		$target = 0;
		// 	}
		// $nilai = $target;

		// 	return $ok;
		})
		// ->addColumn('kode', function ($record) {
		// 	return $record->item_code;
		// })
		->with('region', $region)
		->with('lsi', $lsi)
		->with('tmuk', $tmuk)
		// ->with('tmuk', 'tara')

		->make(true);
	}

	public function gridDeviasiPenjualan(Request $request)
	{
		// dd($request->tmuk_kode);
		$filter = ($request->tmuk_kode) ? "where ref_tmuk.kode like '%".$request->tmuk_kode."%'" : '' ;
        $record = Tmuk::from(\DB::raw("(SELECT 
					case when ref_kki.status > 0 then ref_kki.status else 0 end as status,
					case when ref_kki.ii_penjualan_normal*12 > 0 then ref_kki.ii_penjualan_normal*12 else 0 end as ii_penjualan_normal,
					ref_tmuk.kode as tmuk_kode,
					ref_tmuk.created_at as created_at,
					(SELECT case when SUM(total) > 0 then SUM(total) else 0 end FROM trans_rekap_penjualan WHERE tmuk_kode = ref_tmuk.kode AND extract(YEAR from tanggal) = '".date('Y')."') AS penjualan_tmuk,
					ref_tmuk.nama
			FROM ref_tmuk
			LEFT JOIN ref_kki ON ref_tmuk.id = ref_kki.tmuk_id
			$filter
			) d"))->orderBy('created_at','ASC');
		$records = $record->get();



		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('ii_penjualan_normal', function ($record) {
			return rupiah($record->ii_penjualan_normal*12);
		})
		->addColumn('penjualan_tmuk', function ($record) {
			return rupiah($record->penjualan_tmuk);
		})
		->addColumn('selisih', function ($record) {
			// return rupiah($record->penjualan_tmuk - ($record->ii_penjualan_normal*12));
			return rupiah($record->ii_penjualan_normal*12 - ($record->penjualan_tmuk));
		})
		->addColumn('terget', function ($record) {

			// $data = $record->penjualan_tmuk - ($record->ii_penjualan_normal*12) *100;
			if ($record->ii_penjualan_normal != 0) {
			$target = $record->penjualan_tmuk / ($record->ii_penjualan_normal*12) *100;
			} else {
				$target = 0;
			}


			return FormatNumber($target);
		})
		->addColumn('belum', function ($record) {
			return '-';
		})
		->make(true);
	}

	public function gridLastSoHarian(Request $request)
	{
        $records = TransSO::select('*')->where('type', 'DAY');

        if ($tmuk_kode = $request->tmuk_kode) {
                        $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    }

		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('belum', function ($record) {
			return '-';
		})
		->addColumn('harian_f', function ($record) {
			return $record->flag == 't' ? $record->tgl_so : '-';
		})
		->addColumn('harian_t', function ($record) {
			return $record->flag == 'f' ? $record->tgl_so : '-';
		})
		->addColumn('tmuk', function ($record) {
			return $record->tmuk->nama;
		})
		->make(true);
	}

	public function gridLastSoBulanan(Request $request)
	{
        $records = TransSO::select('*')->where('type', 'MONTH');
        if ($tmuk_kode = $request->tmuk_kode) {
                        $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
                    }
		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('bulanan_f', function ($record) {
			return $record->flag == 't' ? $record->tgl_so : '-';
		})
		->addColumn('bulanan_t', function ($record) {
			return $record->flag == 'f' ? $record->tgl_so : '-';
		})
		->addColumn('tmuk', function ($record) {
			return $record->tmuk->nama;
		})
		->make(true);
	}

	public function gridStatistikPo(Request $request)
	{
        
        $records = TransPo::from(\DB::raw("(SELECT
				extract(YEAR from tgl_buat) as tahun, 
				sum(trans_po.total) as total,
				count(trans_po.nomor) AS qty
				FROM
				trans_po
                WHERE trans_po.status = 2 GROUP BY tahun )  d"))->get();

        

		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('qty', function ($record) {
			return rupiah($record->qty / 12);
		})
		->addColumn('total', function ($record) {
			return rupiah($record->total / 12);
		})
		->make(true);
	}

	public function gridSetoranPenjualan(Request $request)
	{
   //      $records = Tmuk::from(\DB::raw("(SELECT
			//  ref_tmuk.kode, ref_tmuk.nama,
			//  sum(trans_rekap_penjualan.total) as nilai_penjualan_tmuk,
			// (SELECT sum(trans_topup_escrow.jumlah) as jml FROM trans_topup_escrow WHERE tmuk_kode = ref_tmuk.kode AND jenis = 1 GROUP BY tmuk_kode) AS nilai_topup
			// FROM
			//  ref_tmuk
			// INNER JOIN trans_rekap_penjualan ON trans_rekap_penjualan.tmuk_kode=ref_tmuk.kode GROUP BY ref_tmuk.kode, ref_tmuk.nama )  d"))->get();
        

        $records = Tmuk::from(\DB::raw("(SELECT
		ref_tmuk.kode,
		 sum(trans_rekap_penjualan.total) as nilai_penjualan_tmuk,
		(SELECT sum(trans_topup_escrow.jumlah) as jml FROM trans_topup_escrow WHERE tmuk_kode = ref_tmuk.kode AND jenis = 1 GROUP BY tmuk_kode) AS nilai_topup,
        ref_tmuk.nama,
        ref_tmuk.created_at
        FROM ref_tmuk
        LEFT JOIN trans_rekap_penjualan
        ON ref_tmuk.kode = trans_rekap_penjualan.tmuk_kode GROUP BY ref_tmuk.kode, ref_tmuk.nama, ref_tmuk.created_at )  d"))->orderBy('created_at','ASC')->get();
        // dd($records);

        // dd($records);

		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('tmuk', function ($record) {
			return $record->nama;
		})
		->addColumn('nilai_penjualan_tmuk', function ($record) {
			return rupiah($record->nilai_penjualan_tmuk);
		})
		->addColumn('nilai_topup', function ($record) {
			return rupiah($record->nilai_topup);
		})
		->addColumn('nilai_outstanding_pr', function ($record) {
			return rupiah($record->nilai_penjualan_tmuk - $record->nilai_topup);
		})

		->addColumn('last_setor', function ($record) {
			return $record->getLastSetor();
		})
		->make(true);
	}

	public function index()
	{
		$records = Chat::orderBy('created_at', 'desc')->paginate(10);

		$jum_tmuk = Tmuk::count();
		if(auth()->user()->can('dashboard-')){
			$dashboard = 'dashboard.index';
		} else {
			$dashboard = 'dashboard.dash';
		}
		$data = [
			'records'	=> $records->reverse(),
			'record'	=> Tmuk::all(),
            'lsi'		=> Lsi::all(),
            'rombong'	=> Kustomer::all(),
			'jum_tmuk'	=> $jum_tmuk
		];
		return $this->render('modules.'.$dashboard, $data);
	}
}
