<?php

namespace Lotte\Http\Controllers\Transaksi\Kubikasi;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransMuatan;
use Lotte\Models\Trans\TransMuatanDetail;
use Lotte\Models\Trans\TransSuratJalan;
use Lotte\Models\Master\Truk;
use Datatables;
use Carbon\Carbon;

class TrukController extends Controller
{
	protected $link = 'transaksi/kubikasi/truk/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Truk");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen longer");
		$this->setBreadcrumb(['Transaksi' => '#', 'Kubikasi' => '#', 'Truk' => '#']);
		$this->setTableStruct([
			[
				'data' => 'num',
				'name' => 'num',
				'label' => '#',
				'orderable' => false,
				'searchable' => false,
				'className' => "center aligned",
				'width' => '40px',
			],
			/* --------------------------- */

			[
				'data' => 'po.tgl_buat',
				'name' => 'po.tgl_buat',
				'label' => 'Tanggal PO',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'po.nomor',
				'name' => 'po.nomor',
				'label' => 'Nomor PO',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'no_surat_jalan',
				'name' => 'no_surat_jalan',
				'label' => 'Nomor Surat Jalan',
				'searchable' => false,
				'sortable' => false,
				'className' => "center aligned",
			],
			[
				'data' => 'tmuk.kode',
				'name' => 'tmuk.kode',
				'label' => 'TMUK',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'tmuk.alamat',
				'name' => 'tmuk.alamat',
				'label' => 'Alamat',
				'searchable' => false,
				'sortable' => true,
				'className' => "center aligned",
			],
			[
				'data' => 'inner_box',
				'name' => 'inner_box',
				'label' => 'Inner Box / Kontainer Biru',
				'searchable' => false,
				'sortable' => false,
				'className' => "center aligned",
				'width' => '100px',
			],
			[
				'data' => 'outer_box',
				'name' => 'outer_box',
				'label' => 'Outer Box',
				'searchable' => false,
				'sortable' => false,
				'className' => "center aligned",
				'width' => '150px',
			],
			[
				'data' => 'grup',
				'name' => 'grup',
				'label' => 'Grouping',
				'searchable' => false,
				'sortable' => false,
				'className' => "center aligned",
				'width' => '80px',
			],
			[
				'data' => 'action',
				'name' => 'action',
				'label' => 'Aksi',
				'searchable' => false,
				'sortable' => false,
				'className' => "center aligned",
				'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TransTruk::with('creator', 'po','tmuk');
			if(auth()->user()->hasRole('system-administrator')){
                $records->orderBy('group', 'ASC');
            }elseif (isset(auth()->user()->pengguna->lsi_id)) {
                $records->whereHas('tmuk', function($q){
                    $q->where('lsi_id', auth()->user()->pengguna->lsi_id);
                })->orderBy('group', 'ASC');
            }else{
                $records->orderBy('group', 'ASC');
            }

		$truk = Truk::get();

		if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('created_at', 'desc');
        }

        //Filters
		if ($po = $request->po) {
			$records->where('po_nomor', 'ilike', '%' . $po . '%');
		}

		if ($tgl = $request->tgl) {
			$records->whereHas('po', function ($query) use ($tgl) {
				$query->where('tgl_buat',$tgl);
			});
		}

		if ($tmuk = $request->tmuk) {
			$records->where('tmuk_kode',$tmuk);
		}

		return Datatables::of($records)
		->addColumn('num', function ($record) use ($request) {
			return $request->get('start');
		})
		->addColumn('created_at', function ($record) {
			return $record->created_at->diffForHumans();
		})
		->editColumn('po.tgl_buat',function($record){
			return Carbon::createFromFormat('Y-m-d', $record->po->tgl_buat)->format('d/m/y');
		})
		// ->addColumn('po.nomor',function($record){
		// 	return $record->po->nomor;
		// })
		// ->addColumn('alamat',function($record){
		// 	return $record->tmuk->alamat;
		// })
		->addColumn('no_surat_jalan',function($record){
			$string = '-';
			if($record->po->muatandetail){
				$string = $record->po->muatandetail->muatan->suratjalan->nomor;
			}
			return $string;
		})
		->editColumn('tmuk.kode',function($record){
			return $record->tmuk->kode.' - '.$record->tmuk->nama;
		})
		->addColumn('inner_box',function($record){
			$inner = TransKontainer::where('po_nomor',$record->po_nomor)->first();
			return ($inner->detail()->whereNotNull('id_kontainer')->max('no_kontainer') ? $inner->detail()->whereNotNull('id_kontainer')->max('no_kontainer') : 0 );
		})
		->addColumn('outer_box',function($record){
			$outer = TransKontainer::where('po_nomor',$record->po_nomor)->first();
			return $outer->detail()->where('id_kontainer','=',null)->count();
		})
		->editColumn('grup',function($record) use($truk){
			$string = '';
			if($record->group==0){
				$string .= '
				<div class="ui checkbox">
				<input type="checkbox" data-tmuk="'.$record->tmuk_kode.'" class="group-truk" value="'.$record->po_nomor.'">
				</div>';

			}else{
				// $string .= '
				// <div class="ui checkbox">
				// <input type="checkbox" data-tmuk="'.$record->tmuk_kode.'" class="group-truk" value="'.$record->po_nomor.'" checked disabled>
				// </div>';
				$string .= '<span><i class="checkmark icon"></i></span>';
				
			}
				// $string .='
				// <div class="ui custom popup">
				// 	<table>
				// 	<tr>
				// 		<td colspan="3" align="center">Indikasi Muatan</td>
				// 	</tr>';
				// 	foreach ($truk as $value) {
				// 		$string .='
				// 		<tr>
				// 			<td>'.$value->tipe.'</td>
				// 			<td> : Sisa 8 </td>
				// 			<td>&nbsp;Kontainer Biru</td>
				// 		</tr>';
				// 	}
				// $string .='
				// 	</table>
				// </div>
				// ';
			return $string;
		})
		->editColumn('action', function ($record) {
			$btn = '';
			if($record->group==0){
				$btn = '<a target="_blank" href="'.url($this->link).'/print-surat-jalan/'.$record->id.'" class="ui mini default icon print button disabled" data-content="Print Surat Jalan" data-id="1"><i class="print icon"></i> Surat Jalan</a>';

				$btn .= '<a target="_blank" href="'.url($this->link).'/print-daftar-muatan/'.$record->id.'" class="ui mini default icon print button disabled" data-content="Print Daftar Muatan" data-id="1"><i class="print icon"></i> Daftar Muatan</a>';

			}else{
				$btn = '<a target="_blank" href="'.url($this->link).'/print-surat-jalan/'.$record->id.'" class="ui mini default icon print button" data-content="Print Surat Jalan" data-id="1"><i class="print icon"></i> Surat Jalan</a>';

				$btn .= '<a target="_blank" href="'.url($this->link).'/print-daftar-muatan/'.$record->id.'" class="ui mini default icon print button" data-content="Print Daftar Muatan" data-id="1"><i class="print icon"></i> Daftar Muatan</a>';            		
			}
                // //Edit
                // $btn .= $this->makeButton([
                // 	'type' => 'edit',
                // 	'id'   => $record->id
                // ]);
                // // Delete
                // $btn .= $this->makeButton([
                // 	'type' => 'delete',
                // 	'id'   => $record->id
                // ]);
			return $btn;
		})
            // ->rawColumns(['grup', 'action'])
		->make(true);
	}

	public function index()
	{
		return $this->render('modules.transaksi.kubikasi.truk.index',['mockup'=>false]);
	}

	public function create()
	{
		return $this->render('modules.transaksi.kubikasi.truk.create');
	}

	public function edit($id)
	{
		return $this->render('modules.transaksi.kubikasi.truk.edit');
	}

	public function group(Request $request)
	{
    	// dd($request->all());
		if($request->truk){
	    	if(isset(auth()->user()->pengguna->lsi_id)){
	    		$lsi_kode = auth()->user()->pengguna->lsi->kode;
	    	}else{
	    		$lsi_kode = '00000';
	    	}

			$muatan = new TransMuatan;
			$muatan->nomor = TransMuatan::generateCode($lsi_kode);
			$muatan->status = 0;
			$muatan->save();

			foreach ($request->truk as $key => $value) {
				foreach ($value as $k => $v) {
					$muatan_detail = new TransMuatanDetail;
					$muatan_detail->nomor_muatan = $muatan->nomor;
					$muatan_detail->po_nomor = $v;
					$muatan_detail->tmuk_kode = $k;
					$muatan_detail->status = 0;
					$muatan_detail->save();

					$transtruk = TransTruk::where('po_nomor',$v)->first();
					$transtruk->group = 1;
					$transtruk->save();
				}
			}

			$jalan = new TransSuratJalan;
			$jalan->nomor = TransSuratJalan::generateCode($lsi_kode);
			$jalan->nomor_muatan = $muatan->nomor;
			$jalan->supir = $request->supir;
			$jalan->nomor_kendaraan = $request->nomor_kendaraan;
			$jalan->save();
		}else{
			return false;
		}
		return response([
    		'status' => true,
    	]);
	}

	public function printDaftarMuatan($id)
	{
		$truk = TransTruk::find($id);
		$detail = TransMuatanDetail::where('po_nomor',$truk->po_nomor)->first();
		$record = $detail->muatan;
		$detail = $detail->muatan->detail;

		// return $detail;

		$content = view('report.daftar-muatan',['record'=>$record,'detil'=>$detail]);

		$data = [
			'oriented'   => 'L',
			'paper'      => 'A4',
			'label_file' => 'Daftar Muatan '.$record->nomor,
		];

            // return $content;

		HTML2PDF($content, $data);
	}

	public function printSuratJalan($id)
	{
		$truk = TransTruk::find($id);
		$detail = TransMuatanDetail::where('po_nomor',$truk->po_nomor)->first();
		$record = $detail->muatan->suratjalan;
		$content = view('report.surat-jalan',['record'=>$record]);

		$data = [
			'oriented'   => 'L',
			'paper'      => 'A4',
			'label_file' => 'Surat Jalan '.$record->nomor,
		];

            // return $content;

		HTML2PDF($content, $data);
	}
}
