<?php

namespace Lotte\Http\Controllers\Transaksi\Kubikasi;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransKontainer;
use Datatables;
use Carbon\Carbon;

class KontainerController extends Controller
{
	protected $link = 'transaksi/kubikasi/kontainer/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Kontainer");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Transaksi' => '#', 'Kubikasi' => '#', 'Kontainer' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			
			[
			    'data' => 'po.tgl_buat',
			    'name' => 'po.tgl_buat',
			    'label' => 'Tanggal PO',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'po.nomor',
			    'name' => 'po.nomor',
			    'label' => 'Nomor PO',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'tmuk.kode',
			    'name' => 'tmuk.kode',
			    'label' => 'TMUK',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'inner_box',
			    'name' => 'inner_box',
			    'label' => 'Inner Box Kontainer Biru',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '100px',
			],
			[
			    'data' => 'outer_box',
			    'name' => 'outer_box',
			    'label' => 'Outer Box',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TransKontainer::with('creator','po','tmuk');
        	if(auth()->user()->hasRole('system-administrator')){
                $records->orderBy('created_at', 'ASC');
            }elseif (isset(auth()->user()->pengguna->lsi_id)) {
                $records->whereHas('tmuk', function($q){
                    $q->where('lsi_id', auth()->user()->pengguna->lsi_id);
                })->orderBy('created_at', 'ASC');
            }else{
                $records->orderBy('created_at', 'ASC');
            }

		// Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('created_at');
        }

        //Filters
        if ($po = $request->po) {
            $records->where('po_nomor', 'ilike', '%' . $po . '%');
        }

        if ($tgl = $request->tgl) {
            $records->whereHas('po', function ($query) use ($tgl) {
			    $query->where('tgl_buat',$tgl);
			});
        }

        if ($tmuk = $request->tmuk) {
            $records->where('tmuk_kode',$tmuk);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->editColumn('po.tgl_buat',function($record){
                return Carbon::createFromFormat('Y-m-d', $record->po->tgl_buat)->format('d/m/y');
			})
			// ->addColumn('no_po',function($record){
			// 	return $record->po->nomor;
			// })
			->editColumn('tmuk.kode',function($record){
				return $record->tmuk->kode.' - '.$record->tmuk->nama;
			})

			->addColumn('inner_box',function($record){
				$jum_kontainer = $record->detail()->whereNotNull('id_kontainer')->max('no_kontainer');
				$jum_outer = $record->detail->where('id_kontainer',null)->count();

				return $jum_kontainer;
			})
			->addColumn('outer_box',function($record){
				$jum_kontainer = $record->detail()->whereNotNull('id_kontainer')->count();
				$jum_outer = $record->detail->where('id_kontainer',null)->count();
				return $jum_outer;
			})
            ->addColumn('action', function ($record) {
            	$btn = '';
  //               $btn = '<button type="button" class="ui mini orange icon edit button" data-content="Detil Kontainer" data-id='.$record->id.'><i class="edit icon"></i> Detail</button>
		// <a target="_blank" href="{{ url($pageUrl) }}/print-label-kontainer" class="ui mini default icon print button" data-content="Print Detil Kontainer" data-id="1"><i class="print icon"></i> Print</a>';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'detail',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'print',
                	'id'   => $record->id,
                	'class'   => 'icon print label-kontainer'
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.transaksi.kubikasi.kontainer.index',['mockup'=>false]);
    }

    public function create()
    {
        return $this->render('modules.transaksi.kubikasi.kontainer.create');
    }

    public function show($id)
    {
    	$record = TransKontainer::find($id);
        return $this->render('modules.transaksi.kubikasi.kontainer.detail',['record'=>$record]);
    }

    public function edit($id)
    {
        return $this->render('modules.transaksi.kubikasi.kontainer.edit');
    }

    public function printLabelKontainer($id)
    {
            $record = TransKontainer::find($id);

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Label Kontainer '.$record->nomor,
            ];

            $content = view('report.label-kontainer', ['record'=> $record]);
            // return $content;

            HTML2PDF($content, $data);
    }
}
