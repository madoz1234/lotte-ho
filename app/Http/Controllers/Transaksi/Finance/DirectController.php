<?php

namespace Lotte\Http\Controllers\Transaksi\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

use Lotte\Models\Trans\TransDirectInvoice;
use Lotte\Models\Trans\TransDetailDirectInvoice;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Master\ProdukAssortment;
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransPyrDetail;
use Lotte\Models\Trans\TransAkuisisiAset;
use Lotte\Models\SecTransaksi;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\TipeBarang;

use DB;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

class DirectController extends Controller
{
  protected $link = 'transaksi/finance/direct/';

  function __construct()
  {
    $this->setLink($this->link);
    $this->setTitle("Direct Invoice");
    $this->setSubtitle("&nbsp;");
    $this->setModalSize("longer");
    $this->setBreadcrumb(['Transaksi' => '#', 'Finance' => '#', 'Direct Invoice' => '#']);
    $this->setTableStruct([
      [
       'data' => 'num',
       'name' => 'num',
       'label' => '#',
       'orderable' => false,
       'searchable' => false,
       'className' => "center aligned",
       'width' => '40px',
     ],
     /* --------------------------- */
     [
       'data' => 'tgl_buat',
       'name' => 'tgl_buat',
       'label' => 'Tanggal Buat',
       'searchable' => false,
       'sortable' => true,
       'className' => "center aligned",
     ],
     [
       'data' => 'nomor',
       'name' => 'nomor',
       'label' => 'Nomor',
       'searchable' => false,
       'sortable' => true,
       'className' => "center aligned",
     ],
     [
       'data' => 'tmuk',
       'name' => 'tmuk',
       'label' => 'TMUK',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
     ],
     [
       'data' => 'lsi',
       'name' => 'lsi',
       'label' => 'Supplier',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
     ],

     [
       'data' => 'tgl_kirim',
       'name' => 'tgl_kirim',
       'label' => 'Tanggal Kirim',
       'searchable' => false,
       'sortable' => true,
       'className' => "center aligned",
     ],
     [
       'data' => 'action',
       'name' => 'action',
       'label' => 'Aksi',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
       'width' => '150px',
     ]
   ]);
  }

  public function grid(Request $request)
  {
    $records = TransDirectInvoice::with('creator')
    ->select('*');

                         // dd($records);
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
    if($tgl_buat = $request->tgl_buat){
      $dates = \Carbon\Carbon::parse($tgl_buat)->format('Y-m-d');
      $records->where(\DB::raw('DATE(tgl_buat)'),$dates);
    }
        // if ($tgl_buat = $request->tgl_buat) {
        //     $records->where('tgl_buat',$tgl_buat);
        // }
    if ($tmuk_kode = $request->tmuk_kode) {
      $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
      return $request->get('start');
    })
    ->addColumn('lsi', function ($record) {
            // return $record->tmuk->lsi->nama;
      return $record->vendor_lokal_id == null ? $record->tmuk->lsi->nama : $record->vendor->nama ;
    })
    ->addColumn('tmuk', function ($record) {
                // return $record->tmuk.' - '.$record->tmuk->nama;
      return $record->tmuk_kode.' - '.$record->tmuk->nama;
    })
    ->addColumn('created_at', function ($record) {
      return $record->created_at->diffForHumans();
    })

    ->addColumn('action', function ($record) {
      $string = '<button type="button" class="ui mini orange icon detail button" data-content="Detail Direct Invoice" data-id="'.$record->id.'"><i class="edit icon"></i> Detail</button>
      <a target="_blank" href="direct/print/'.$record->nomor.'" class="ui mini default icon print button" data-content="Print Direct Invoice"><i class="print icon"></i> Print</a>';
      return $string;
    })

    ->make(true);
  }

  public function index()
  {
    if (date('d') <= 10) {
        $startdate = Carbon::createFromFormat('Y-m-d', date('Y').'-'.(date('m') -1 ).'-01');
    } else {
        $startdate = Carbon::createFromFormat('Y-m-d', date('Y-m').'-01');
    }
    $enddate = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));

    return $this->render('modules.transaksi.finance.direct.index', [
      'mockup' => false,
      'startdate' => $startdate,
      'enddate' => $enddate,
    ]);
  }

  public function create()
  {
    return $this->render('modules.transaksi.finance.direct.create');
  }

  public function edit($id)
  {

    $data = [
      'record' => TransDirectInvoice::find($id),
    ];
        // dd($data);
    return $this->render('modules.transaksi.finance.direct.edit', $data);
  }

  public function detail($id)
  {

    $data = [
      'record' => TransDirectInvoice::find($id),
    ];

        // dd($data);
    return $this->render('modules.transaksi.finance.direct.detail', $data);
  }

    // public function onChangePopPo($id=null)
    // {
    //     $data = Tmuk::find($id);
    //     // dd($data);
    //     return response()->json([
    //         'kode' => $data->kode,
    //     ]);
    // }

  public function onChangePopPo($id=null)
  {

    $tmuk = Tmuk::find($id);
    $kode = $tmuk->kode;


    $data = TransDirectInvoice::generateCodePr($kode);
    return response()->json([
      'nomor' => $data,
    ]);
  }

  public function onChangePopPyr($id=null)
  {
    $tmuk = Tmuk::find($id);
    $kode = $tmuk->kode;

        // dd($kode);
    $data = TransDirectInvoice::generateCodePyr($kode);
    return response()->json([
      'nomor' => $data,
      'lsi'   => $tmuk->lsi->nama,
    ]);
  }

  public function onChangePopOpening($id=null)
  {
    $tmuk = Tmuk::find($id);
    $kode = $tmuk->kode;

        // dd($kode);
    $data = TransDirectInvoice::generateCodeOpening($kode);
    return response()->json([
      'nomor' => $data,
      'lsi'   => $tmuk->lsi->nama,
    ]);
  }

  public function postimportexcel2(Request $request)
  {
    $data = $request->all();
        // dd($data);

    $tmuk = Tmuk::find($data['tmuk_id']);
    $data['tmuk_kode'] = $tmuk->kode;

    $opening = new TransDirectInvoice;
    $opening->fill($data);
    if ($opening->save()) {
      if($request->hasFile('upload_data_produk')){
        $path = $request->file('upload_data_produk')->getRealPath();
        $data = Excel::load($path, function($reader) {})->get();
        $sukses = 0;
        $gagal = 0;
        $error = [];

                // dd($data->toArray());
        if(!empty($data) && $data->count()){
          foreach ($data->toArray() as $key => $value) {
                        // dd($value);
                        //jika data tidak boleh kosong
            if(!is_null($value['produk_kode'])){ 
                            //check status produk per lsi selain normal dan order stop tidak bisa tersimpan
              $checkStatus = ProdukLsi::where('lsi_kode', $tmuk->lsi->kode)->where('produk_kode', $value['produk_kode'])
              ->whereHas('produk.produksetting', function($query){
                $query->where('tipe_produk', 1);
              })->where(function($q) {
               $q->where('status', 'NORMAL')
               ->orWhere('status', 'ORDER STOP');
             })->first();
              if($checkStatus){
                $pt = ProdukTmuk::where('produk_kode', $value['produk_kode'])->where('tmuk_kode', $tmuk->kode)->first();
                                    // dd($pa);
                if ($pt) {
                  if ($pt->flag == 0) {
                    $record              = new TransDetailDirectInvoice;
                    $record->direct_invoice_id  = $opening->id;
                    $record->produk_kode = $value['produk_kode'];
                    $record->qty_po      = $value['qty'];
                    $record->created_by  = \Auth::user()->id;
                    $record->created_at  = date('Y-m-d H:i:s');
                    if($record->save()){
                      $sukses++;
                    }
                  }else{
                    $gagal++;
                    $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' status non aktif.';
                  }
                }else{
                  $new_pt              = new ProdukTmuk();
                  $new_pt->created_by  = \Auth::user()->id;
                  $new_pt->created_at  = date('Y-m-d H:i:s');
                  $new_pt->tmuk_kode   = $tmuk->kode;
                  $new_pt->produk_kode = $value['produk_kode'];
                  if($new_pt->save()){
                    $record              = new TransDetailDirectInvoice;
                    $record->direct_invoice_id  = $opening->id;
                    $record->produk_kode = $value['produk_kode'];
                    $record->qty_po      = $value['qty'];
                    $record->created_by  = \Auth::user()->id;
                    $record->created_at  = date('Y-m-d H:i:s');
                    if($record->save()){
                      $sukses++;
                    }
                  }
                }
              }else{
                $gagal++;
                $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' tidak terdaftar di system.';
              }

            }
          }

                    // dd("ok");

          if ($sukses != 0) {
                        //masukin data ke trans PR dan detail PR
            $op = TransDirectInvoice::with('tmuk', 'tmuk.lsi')->find($opening->id);
            $records = TransDirectInvoice::has('detaildirect')->select(DB::raw('
              ref_produk.nama as nama,

              ref_produk_lsi.stok_gmd as stok_gmd,
              ref_produk_lsi.curr_sale_prc as curr_sale_prc,

              ref_produk_setting.uom1_barcode as uom1_barcode,
              ref_produk_setting.uom1_satuan as uom1_satuan,
              ref_produk_setting.uom2_satuan as uom2_satuan,
              ref_produk_setting.uom2_conversion as uom2_conversion,

              trans_detail_direct_invoice.produk_kode as produk_kode,
              trans_detail_direct_invoice.qty_po as qty_po,
              (trans_detail_direct_invoice.qty_po*ref_produk_lsi.curr_sale_prc)as total_harga'
            ))
            ->join('trans_detail_direct_invoice', 'trans_direct_invoice.id', '=', 'trans_detail_direct_invoice.direct_invoice_id')
            ->join('ref_produk', 'trans_detail_direct_invoice.produk_kode', '=', 'ref_produk.kode')
            ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
            ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
            ->where(DB::raw('trans_direct_invoice.id'), $op->id)
            ->where(DB::raw('ref_produk_lsi.lsi_kode'), $op->tmuk->lsi->kode)->get();
                                      // ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->get()->toArray();
            $sub_total = $records->sum('total_harga');

                        //input ke trans pr
            $pr = new TransPr;
            $pr->nomor       = $opening->nomor;
            $pr->tmuk_kode   = $opening->tmuk_kode;
            $pr->tgl_buat    = $opening->tgl_buat;
            $pr->tgl_kirim   = $opening->tgl_kirim;
            $pr->total       = $sub_total;
            $pr->status      = 0;
            $pr->created_by  = \Auth::user()->id;
            $pr->created_at  = date('Y-m-d H:i:s');
            if($pr->save()){
              foreach ($records as $key => $value) {
                                //konversi
                $qty_pr = $value['qty_po']/$value['uom2_conversion'];
                if (is_float($qty_pr)) {
                  $qty_pr = ceil($qty_pr);
                  $qty_aktual = $qty_pr * $value['uom2_conversion'];
                }else{
                  $qty_aktual = $qty_pr;
                }

                $pr_detail              = new TransPrDetail;
                $pr_detail->pr_nomor    = $pr->nomor;
                $pr_detail->produk_kode = $value['produk_kode'];
                $pr_detail->qty_order   = $value['qty_po'];
                $pr_detail->unit_order  = $value['uom1_satuan'];
                $pr_detail->unit_order_price    = $value['curr_sale_prc'];
                $pr_detail->qty_sell    = $value['uom2_conversion'];
                                // $pr_detail->unit_sell   = $value['uom1_satuan'];
                $pr_detail->unit_sell   = $value['uom2_satuan'];
                $pr_detail->qty_pr      = $qty_pr;
                $pr_detail->unit_pr     = $value['uom2_satuan'];
                $pr_detail->total_price = $value['total_harga'];
                $pr_detail->qty_pick    = null;
                $pr_detail->qty_aktual  = $qty_aktual;
                $pr_detail->created_by  = \Auth::user()->id;
                $pr_detail->created_at  = date('Y-m-d H:i:s');
                $pr_detail->save();
              }
            }
          }else{
                        //jika gagal lebih banyak dari data sukses terupload maka data di database di hapus
            TransDetailDirectInvoice::where('direct_invoice_id', $opening->id)->delete();
            TransDirectInvoice::find($opening->id)->delete();
          }

          if ($error) {
                        //create log 
            $record = $error;
            $filename = $tmuk->kode.date("YmdHis");
            $errorlog = Excel::create($filename, function($excel) use ($record, $tmuk){
              $excel->setTitle('Error Log Direct PR');
              $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
              $excel->setDescription('Error Log Direct PR');

              $excel->sheet('Error Log Direct PR ', function($sheet) use ($record, $tmuk){
                $sheet->row(1, array(
                  'Error Log Direct PR '.$tmuk->nama.'-'.$tmuk->kode,
                ));

                $row = 2;
                foreach ($record as $val) {
                  $sheet->row($row, array(
                    $val,
                  ));
                  $row=$row;
                  $row++;
                }
                $sheet->setWidth(array('A'=>50));
                $sheet->cells('A1', function($cells){
                  $cells->setAlignment('center');
                  $cells->setBackground('#999999');
                  $cells->setFontColor('#FFFFFF');
                });

              });
            })->store('xls', storage_path('uploads/log-direct-pr'));

            return response([
              'status' => true,
              'upload' => true,
              'sukses' => $sukses,
              'gagal'  => $gagal,
              'log' => '<a href="'.asset('storage/uploads/log-direct-pr/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
            ]);
          }else{
            return response([
              'status' => true,
              'upload' => true,
              'sukses' => $sukses,
              'gagal'  => $gagal
            ]);
          }

        }
      }
    }else{
      return back()->with('error','Please Check your file, Something is wrong there.');
    }
  }

  public function postimportexcel1(Request $request)
  {
    $data = $request->all();

    $tmuk = Tmuk::find($data['tmuk_id']);
    $tipe_pyr = TipeBarang::find($data['tipe_pyr']);
    $data['tmuk_kode'] = $tmuk->kode;
    $vendor = $data['vendor_lokal_id'];

    $opening = new TransDirectInvoice;

    $check = Lsi::where('kode', $vendor)->first();
    if ($check) {
      $suplier = '0';
    }else{
      $suplier = $vendor;
    }

    $data['vendor_lokal_id'] = $suplier;
    $opening->fill($data);
    if ($opening->save()) {
      if($request->hasFile('upload_data_produk')){
        $path = $request->file('upload_data_produk')->getRealPath();
        $data = Excel::load($path, function($reader) {})->get();
        $sukses = 0;
        $gagal = 0;
        $error = [];

                // dd($data->toArray());
        if(!empty($data) && $data->count()){
          foreach ($data->toArray() as $key => $value) {
                        //jika data tidak boleh kosong
            if(!is_null($value['produk_kode'])){ 
              if (!empty($value['qty'])) {
                if (!empty($value['harga'])) {
                                    //check status produk per lsi selain normal dan order stop tidak bisa tersimpan
                  $checkStatus = Produk::where('kode', $value['produk_kode'])
                  ->whereHas('produksetting', function($q){
                    $q->whereIn('tipe_barang_kode', ['004', '003', '002']);
                  })->first();

                  if($checkStatus){
                    $pt = ProdukTmuk::where('produk_kode', $value['produk_kode'])->where('tmuk_kode', $tmuk->kode)->first();

                    if ($pt) {
                      if ($pt->flag == 0) {
                        $record                    = new TransDetailDirectInvoice;
                        $record->direct_invoice_id = $opening->id;
                        $record->produk_kode       = $value['produk_kode'];
                        $record->qty_po            = $value['qty'];
                        $record->price             = $value['harga'];
                        $record->created_by        = \Auth::user()->id;
                        $record->created_at        = date('Y-m-d H:i:s');
                        if($record->save()){
                          $sukses++;
                        }
                      }else{
                        $gagal++;
                        $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' status non aktif.';
                      }
                    }else{
                      $new_pt              = new ProdukTmuk();
                      $new_pt->created_by  = \Auth::user()->id;
                      $new_pt->created_at  = date('Y-m-d H:i:s');
                      $new_pt->tmuk_kode   = $tmuk->kode;
                      $new_pt->produk_kode = $value['produk_kode'];
                      if($new_pt->save()){
                        $record                    = new TransDetailDirectInvoice;
                        $record->direct_invoice_id = $opening->id;
                        $record->produk_kode       = $value['produk_kode'];
                        $record->qty_po            = $value['qty'];
                        $record->price             = $value['harga'];
                        $record->created_by        = \Auth::user()->id;
                        $record->created_at        = date('Y-m-d H:i:s');
                        if($record->save()){
                          $sukses++;
                        }
                      }
                    }
                  }else{
                    $gagal++;
                    $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' Harap cek data excell yang di upload.';
                  }
                }else{
                  $gagal++;
                  $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' Harap cek data excell yang di upload.';
                }

              }else{
                $gagal++;
                $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' Harap cek data excell yang di upload.';
                // $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' tidak terdaftar di system.';
              }
            }
          }

          if ($sukses != 0) {
            $records = TransDirectInvoice::with('tmuk','detaildirect')->find($opening->id);
            $total = $records->detaildirect->sum('price');
                        //input ke trans pr
            $pyr = new TransPyr;
            $pyr->nomor_pyr         = $opening->nomor;
            $pyr->tmuk_kode         = $opening->tmuk_kode;
            $pyr->vendor_lokal      = $opening->vendor_lokal_id;
            $pyr->tgl_buat          = $opening->tgl_buat;
            $pyr->tgl_jatuh_tempo   = $opening->tgl_kirim;
            $pyr->total             = $total;
            $pyr->status            = 0;
            $pyr->tipe              = isset($tipe_pyr) ? $tipe_pyr->kode : 0;
            $pyr->created_by        = \Auth::user()->id;
            $pyr->created_at        = date('Y-m-d H:i:s');
            if($pyr->save()){
              foreach ($records->detaildirect as $key => $detail) {
                $pyr_detail              = new TransPyrDetail;
                $pyr_detail->pyr_id      = $pyr->id;
                $pyr_detail->produk_kode = $detail['produk_kode'];
                $pyr_detail->qty         = $detail['qty_po'];
                $pyr_detail->price       = $detail['price'];
                $pyr_detail->created_by  = \Auth::user()->id;
                $pyr_detail->created_at  = date('Y-m-d H:i:s');
                $pyr_detail->save();
              }

                            // kamus jurnal
              $tahun_fiskal       = TahunFiskal::getTahunFiskal();
                            $coa_utang          = '2.1.1.0'; // Utang Usaha 
                            $deskripsi          = '';
                            
                            $idtrans  = SecTransaksi::generate();
                            $dtjurnal = [];
                            
                            if ($pyr->tipe == '001') {
                              $deskripsi = 'PYR Utang Usaha - Trade Lotte';
                              $dtjurnal[] = [ // Utang Usaha
                              'tahun_fiskal_id' => $tahun_fiskal->id,
                              'idtrans'         => $idtrans,
                              'tanggal'         => $pyr->tgl_buat,
                              'tmuk_kode'       => $pyr->tmuk_kode,
                              'referensi'       => $pyr->nomor_pyr,
                              'deskripsi'       => $deskripsi,
                              'coa_kode'        => $coa_utang,
                              'jumlah'          => $pyr->total,
                              'posisi'          => 'K',
                              'flag'            => '-',
                            ];
                              $dtjurnal[] = [ // Persediaan Barang Dagang
                              'tahun_fiskal_id' => $tahun_fiskal->id,
                              'idtrans'         => $idtrans,
                              'tanggal'         => $pyr->tgl_buat,
                              'tmuk_kode'       => $pyr->tmuk_kode,
                              'referensi'       => $pyr->nomor_pyr,
                              'deskripsi'       => $deskripsi,
                              'coa_kode'        => '1.1.4.0',
                              'jumlah'          => $pyr->total,
                              'posisi'          => 'D',
                              'flag'            => '+',
                            ];
                          }else if($pyr->tipe == '002'){
                            $deskripsi = 'PYR Utang Usaha - Trade Non-Lotte';
                              $dtjurnal[] = [ // Utang Usaha
                              'tahun_fiskal_id' => $tahun_fiskal->id,
                              'idtrans'         => $idtrans,
                              'tanggal'         => $pyr->tgl_buat,
                              'tmuk_kode'       => $pyr->tmuk_kode,
                              'referensi'       => $pyr->nomor_pyr,
                              'deskripsi'       => $deskripsi,
                              'coa_kode'        => $coa_utang,
                              'jumlah'          => $pyr->total,
                              'posisi'          => 'K',
                              'flag'            => '-',
                            ];
                              $dtjurnal[] = [ // Persediaan Barang Dagang
                              'tahun_fiskal_id' => $tahun_fiskal->id,
                              'idtrans'         => $idtrans,
                              'tanggal'         => $pyr->tgl_buat,
                              'tmuk_kode'       => $pyr->tmuk_kode,
                              'referensi'       => $pyr->nomor_pyr,
                              'deskripsi'       => $deskripsi,
                              'coa_kode'        => '1.1.4.0',
                              'jumlah'          => $pyr->total,
                              'posisi'          => 'D',
                              'flag'            => '+',
                            ];
                          }else if($pyr->tipe == '004'){
                            $deskripsi = 'PYR Utang Usaha - Non Trade Non-Lotte';

                            $b_iklan = 0;
                            $b_pemasaran_lainnya = 0;
                            $komisi_bonus = 0;
                            $b_gaji_lembur_thr = 0;
                            $b_makan_karyawan = 0;
                            $b_tunjangan_kesehatan = 0;
                            $b_asuransi = 0;
                            $b_air = 0;
                            $b_listrik = 0;
                            $b_telp_internet = 0;
                            $b_keamanan = 0;
                            $b_lainnya = 0;
                            $b_distribusi = 0;
                            $plastik = 0;
                            $b_bensin = 0;
                            $b_peralatan_toko = 0;
                            $b_sewa_toko = 0;
                            $b_lain_operasional = 0;
                            $b_lain_nonoperasional = 0;

                            foreach ($pyr->detailPyr as $key => $detail) {
                              $jenis_barang = $detail->produks->produksetting->jenis_barang_kode;

                                if ($jenis_barang == '003') { //Biaya Iklan
                                  $b_iklan += $detail->price;
                                }else if ($jenis_barang == '005') { //Biaya Pemasaran Lainnya
                                  $b_pemasaran_lainnya += $detail->price;
                                }else if ($jenis_barang == '006') { //Komisi & Bonus
                                  $komisi_bonus += $detail->price;
                                }else if ($jenis_barang == '007') { //Biaya Gaji, Lembur & THR
                                  $b_gaji_lembur_thr += $detail->price;
                                }else if ($jenis_barang == '008') { //Biaya Makan Karyawan
                                  $b_makan_karyawan += $detail->price;
                                }else if ($jenis_barang == '009') { //Biaya Tunjangan Kesehatan
                                  $b_tunjangan_kesehatan += $detail->price;
                                }else if ($jenis_barang == '010') { //Biaya Asuransi
                                  $b_asuransi += $detail->price;
                                }else if ($jenis_barang == '011') { //Biaya Air
                                  $b_air += $detail->price;
                                }else if ($jenis_barang == '012') { //Biaya Listrik
                                  $b_listrik += $detail->price;
                                }else if ($jenis_barang == '013') { //Biaya Telp & Internet
                                  $b_telp_internet += $detail->price;
                                }else if ($jenis_barang == '014') { //Biaya Keamanan
                                  $b_keamanan += $detail->price;
                                }else if ($jenis_barang == '023') { //lainya (sumbangan,dll)
                                  $b_lainnya += $detail->price;
                                }else if ($jenis_barang == '015') { //Biaya Distribusi (Adm)
                                  $b_distribusi += $detail->price;
                                }else if ($jenis_barang == '016') { //Plastik Pembungkus
                                  $plastik += $detail->price;
                                }else if ($jenis_barang == '018') { //Biaya Bensin
                                  $b_bensin += $detail->price;
                                }else if ($jenis_barang == '022') { //Biaya Perlengkapan Toko
                                  $b_peralatan_toko += $detail->price;
                                }else if ($jenis_barang == '019') { //Biaya Sewa Toko
                                  $b_sewa_toko += $detail->price;
                                }else if ($jenis_barang == '020') { //Biaya Lain Operasional
                                  $b_lain_operasional += $detail->price;
                                }else if ($jenis_barang == '021') { //Biaya Lain Non Operasional
                                  $b_lain_nonoperasional += $detail->price;
                                }
                                // else if ($jenis_barang == '017') {
                                //      $kode_coa = '?', //Promosi
                                //      $desc = 'Pembayaran PyR - ?';
                                // }
                              }

                              //Create Non Trade Non Lotte
                              TransJurnal::insert([
                                [
                                  'idtrans' => $idtrans,
                                  'tahun_fiskal_id' => $tahun_fiskal->id,
                                  'tanggal' => $pyr->tgl_buat,
                                  'tmuk_kode' => $pyr->tmuk_kode,
                                  'referensi' => $pyr->nomor_pyr,
                                  'deskripsi' => $deskripsi,
                                      'coa_kode' => $coa_utang, // Utang
                                      'jumlah' => $pyr->total,
                                      'posisi' => 'K',
                                      'flag' => '-',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.1.1.1',        //Biaya Iklan
                                      'jumlah' => $b_iklan,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.1.1.2',        //Biaya Pemasaran Lainnya
                                      'jumlah' => $b_pemasaran_lainnya,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.1.1.3',        //Komisi & Bonus
                                      'jumlah' => $komisi_bonus,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.1.1',        //Biaya Gaji, Lembur & THR
                                      'jumlah' => $b_gaji_lembur_thr,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.1.2',        //Biaya Makan Karyawan
                                      'jumlah' => $b_makan_karyawan,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.1.3',        //Biaya Tunjangan Kesehatan
                                      'jumlah' => $b_tunjangan_kesehatan,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.1.4',        //Biaya Asuransi
                                      'jumlah' => $b_asuransi,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.2',        //Biaya Air
                                      'jumlah' => $b_air,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.3',        //Biaya Listrik
                                      'jumlah' => $b_listrik,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.4',        //Biaya Telp & Internet
                                      'jumlah' => $b_telp_internet,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.5',        //Biaya Keamanan
                                      'jumlah' => $b_keamanan,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.6',        //Biaya Lainnya (sumbangan, dll)
                                      'jumlah' => $b_lainnya,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.10',        //Biaya Distribusi (Adm)
                                      'jumlah' => $b_distribusi,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.7',        //Plastik Pembungkus
                                      'jumlah' => $plastik,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.11',        //Biaya Bensin
                                      'jumlah' => $b_bensin,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.12',        //Biaya Peralatan Toko
                                      'jumlah' => $b_peralatan_toko,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.13',        //Biaya Sewa Toko
                                      'jumlah' => $b_sewa_toko,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.14',        //Biaya Lain Operasional
                                      'jumlah' => $b_lain_operasional,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ],[
                                      'idtrans' => $idtrans,
                                      'tahun_fiskal_id' => $tahun_fiskal->id,
                                      'tanggal' => $pyr->tgl_buat,
                                      'tmuk_kode' => $pyr->tmuk_kode,
                                      'referensi' => $pyr->nomor_pyr,
                                      'deskripsi' => $deskripsi,
                                      'coa_kode' => '6.2.2.15',        //Biaya Lain Non-Operasional
                                      'jumlah' => $b_lain_nonoperasional,
                                      'posisi' => 'D',
                                      'flag' => '+',
                                    ]
                                  ]);
}

                            TransJurnal::insert($dtjurnal); // insert jurnal

                            $pyr->id_transaksi = $idtrans;
                            $pyr->save();
                          }
                        }else{
                        //jika gagal lebih banyak dari data sukses terupload maka data di database di hapus
                          TransDetailDirectInvoice::where('direct_invoice_id', $opening->id)->delete();
                          TransDirectInvoice::find($opening->id)->delete();
                        }

                        if ($error) {
                        //create log 
                          $record = $error;
                          $filename = $tmuk->kode.date("YmdHis");
                          $errorlog = Excel::create($filename, function($excel) use ($record, $tmuk){
                            $excel->setTitle('Error Log Direct PYR');
                            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
                            $excel->setDescription('Error Log Direct PYR');

                            $excel->sheet('Error Log Direct PYR ', function($sheet) use ($record, $tmuk){
                              $sheet->row(1, array(
                                'Error Log Direct PYR '.$tmuk->nama.'-'.$tmuk->kode,
                              ));

                              $row = 2;
                              foreach ($record as $val) {
                                $sheet->row($row, array(
                                  $val,
                                ));
                                $row=$row;
                                $row++;
                              }
                              $sheet->setWidth(array('A'=>60));
                              $sheet->cells('A1', function($cells){
                                $cells->setAlignment('center');
                                $cells->setBackground('#999999');
                                $cells->setFontColor('#FFFFFF');
                              });

                            });
                          })->store('xls', storage_path('uploads/log-direct-pyr'));

                          return response([
                            'status' => true,
                            'upload' => true,
                            'sukses' => $sukses,
                            'gagal'  => $gagal,
                            'log' => '<a href="'.asset('storage/uploads/log-direct-pyr/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
                          ]);
                        }else{
                          return response([
                            'status' => true,
                            'upload' => true,
                            'sukses' => $sukses,
                            'gagal'  => $gagal
                          ]);
                        }

                      }
                    }
                  }else{
                    return back()->with('error','Please Check your file, Something is wrong there.');
                  }
                }

                public function postimportexcelopening(Request $request)
                {
                  $data = $request->all();

                  $tmuk = Tmuk::find($data['tmuk_id']);
                  $tipe_pyr = TipeBarang::find($data['tipe_pyr']);

                  $data['tmuk_kode'] = $tmuk->kode;
                  $vendor = $data['vendor_lokal_id'];

                  $opening = new TransDirectInvoice;

                  $check = Lsi::where('kode', $vendor)->first();
                  if ($check) {
                    $suplier = '0';
                  }else{
                    $suplier = $vendor;
                  }

                  $data['vendor_lokal_id'] = $suplier;
                  $opening->fill($data);
                  if ($opening->save()) {
                    if($request->hasFile('upload_data_produk')){
                      $path = $request->file('upload_data_produk')->getRealPath();
                      $data = Excel::load($path, function($reader) {})->get();
                      $sukses = 0;
                      $gagal = 0;
                      $error = [];

                // dd($data->toArray());
                      if(!empty($data) && $data->count()){
                        foreach ($data->toArray() as $key => $value) {
                        //jika data tidak boleh kosong
                          if(!is_null($value['produk_kode'])){ 
                            $checkStatus = Produk::where('kode', $value['produk_kode'])
                            ->whereHas('produksetting', function($q){
                              $q->where('tipe_barang_kode', '003');
                            })->first();

                            if($checkStatus){

                              // dd($data->toArray());
                              $cekstatusketerangan = array("9", "10", "11");
                              if (in_array($value['keterangan_pengeluaran'], $cekstatusketerangan)) {

                                  //save akuisisi aset
                                $uom1_nama = ProdukSetting::where('produk_kode', $value['produk_kode'])->first();
                                $asetakuisisi = new TransAkuisisiAset;
                                $asetakuisisi->tmuk_kode          = $opening->tmuk_kode;
                                $asetakuisisi->tipe_id            = $value['tipe_aset'];
                                $asetakuisisi->nama               = $uom1_nama->uom1_produk_description;
                                $asetakuisisi->no_seri            = $value['nomor_seri'];
                                $asetakuisisi->tanggal_pembelian  = $opening->tgl_buat;
                                $asetakuisisi->nilai_pembelian    = $value['harga'];
                                $asetakuisisi->kondisi            = $value['kondisi_barang'];
                                $asetakuisisi->status             = 0;
                                $asetakuisisi->produk_kode        = $value['produk_kode'];
                                $asetakuisisi->nomor_pyr          = $opening->nomor;
                                $asetakuisisi->save();
                              }

                              $pt = ProdukTmuk::where('produk_kode', $value['produk_kode'])->where('tmuk_kode', $tmuk->kode)->first();

                              if ($pt) {
                                if ($pt->flag == 0) {
                                  $record                    = new TransDetailDirectInvoice;
                                  $record->direct_invoice_id = $opening->id;
                                  $record->produk_kode       = $value['produk_kode'];
                                  $record->qty_po            = $value['qty'];
                                  $record->price             = $value['harga'];
                                  $record->keterangan        = $value['keterangan_pengeluaran'];
                                  $record->created_by        = \Auth::user()->id;
                                  $record->created_at        = date('Y-m-d H:i:s');
                                  if($record->save()){
                                    $sukses++;
                                  }
                                }else{
                                  $gagal++;
                                  $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' status non aktif.';
                                }
                              }else{
                                $new_pt              = new ProdukTmuk();
                                $new_pt->created_by  = \Auth::user()->id;
                                $new_pt->created_at  = date('Y-m-d H:i:s');
                                $new_pt->tmuk_kode   = $tmuk->kode;
                                $new_pt->produk_kode = $value['produk_kode'];
                                if($new_pt->save()){
                                  $record                    = new TransDetailDirectInvoice;
                                  $record->direct_invoice_id = $opening->id;
                                  $record->produk_kode       = $value['produk_kode'];
                                  $record->qty_po            = $value['qty'];
                                  $record->price             = $value['harga'];
                                  $record->keterangan        = $value['keterangan_pengeluaran'];
                                  $record->created_by        = \Auth::user()->id;
                                  $record->created_at        = date('Y-m-d H:i:s');
                                  if($record->save()){
                                    $sukses++;
                                  }
                                }
                              }
                            }else{
                              $gagal++;
                              $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' tidak terdaftar di system.';
                            }
                          }
                        }

                        if ($sukses != 0) {
                          $records = TransDirectInvoice::with('tmuk','detaildirect')->find($opening->id);
                          $total = $records->detaildirect->sum('price');
                        //input ke trans pr
                          $pyr = new TransPyr;
                          $pyr->nomor_pyr         = $opening->nomor;
                          $pyr->tmuk_kode         = $opening->tmuk_kode;
                          $pyr->vendor_lokal      = $opening->vendor_lokal_id;
                          $pyr->tgl_buat          = $opening->tgl_buat;
                          $pyr->tgl_jatuh_tempo   = $opening->tgl_kirim;
                          $pyr->total             = $total;
                          $pyr->status            = 0;
                          $pyr->tipe              = isset($tipe_pyr) ? $tipe_pyr->kode : 0;
                          $pyr->created_by        = \Auth::user()->id;
                          $pyr->created_at        = date('Y-m-d H:i:s');
                          if($pyr->save()){
                            foreach ($records->detaildirect as $key => $detail) {
                              $pyr_detail              = new TransPyrDetail;
                              $pyr_detail->pyr_id      = $pyr->id;
                              $pyr_detail->produk_kode = $detail['produk_kode'];
                              $pyr_detail->qty         = $detail['qty_po'];
                              $pyr_detail->price       = $detail['price'];
                              $pyr_detail->created_by  = \Auth::user()->id;
                              $pyr_detail->created_at  = date('Y-m-d H:i:s');
                              $pyr_detail->keterangan  = $detail['keterangan'];
                              $pyr_detail->save();
                            }

                            // kamus jurnal
                            $tahun_fiskal       = TahunFiskal::getTahunFiskal();
                            $coa_utang          = '2.1.1.0'; // Utang Usaha 
                            $deskripsi          = '';
                            
                            $idtrans  = SecTransaksi::generate();
                            $dtjurnal = [];
                            
                            if($pyr->tipe == '003'){
                              $deskripsi = 'PYR Utang Usaha - Non Trade Lotte';
                              $dtjurnal[] = [ // Utang Usaha
                              'tahun_fiskal_id' => $tahun_fiskal->id,
                              'idtrans'         => $idtrans,
                              'tanggal'         => $pyr->tgl_buat,
                              'tmuk_kode'       => $pyr->tmuk_kode,
                              'referensi'       => $pyr->nomor_pyr,
                              'deskripsi'       => $deskripsi,
                              'coa_kode'        => $coa_utang,
                              'jumlah'          => $pyr->total,
                              'posisi'          => 'K',
                              'flag'            => '-',
                            ];
                              $dtjurnal[] = [ // Peralatan Toko
                              'tahun_fiskal_id' => $tahun_fiskal->id,
                              'idtrans'         => $idtrans,
                              'tanggal'         => $pyr->tgl_buat,
                              'tmuk_kode'       => $pyr->tmuk_kode,
                              'referensi'       => $pyr->nomor_pyr,
                              'deskripsi'       => $deskripsi,
                              'coa_kode'        => '1.2.6.0',
                              'jumlah'          => $pyr->total,
                              'posisi'          => 'D',
                              'flag'            => '+',
                            ];
                          }

                            TransJurnal::insert($dtjurnal); // insert jurnal

                            $pyr->id_transaksi = $idtrans;
                            $pyr->save();
                          }
                        }else{
                        //jika gagal lebih banyak dari data sukses terupload maka data di database di hapus
                          TransDetailDirectInvoice::where('direct_invoice_id', $opening->id)->delete();
                          TransDirectInvoice::find($opening->id)->delete();
                        }

                        if ($error) {
                        //create log 
                          $record = $error;
                          $filename = $tmuk->kode.date("YmdHis");
                          $errorlog = Excel::create($filename, function($excel) use ($record, $tmuk){
                            $excel->setTitle('Error Log Direct PYR');
                            $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
                            $excel->setDescription('Error Log Direct PYR');

                            $excel->sheet('Error Log Direct PYR ', function($sheet) use ($record, $tmuk){
                              $sheet->row(1, array(
                                'Error Log Direct PYR '.$tmuk->nama.'-'.$tmuk->kode,
                              ));

                              $row = 2;
                              foreach ($record as $val) {
                                $sheet->row($row, array(
                                  $val,
                                ));
                                $row=$row;
                                $row++;
                              }
                              $sheet->setWidth(array('A'=>50));
                              $sheet->cells('A1', function($cells){
                                $cells->setAlignment('center');
                                $cells->setBackground('#999999');
                                $cells->setFontColor('#FFFFFF');
                              });

                            });
                          })->store('xls', storage_path('uploads/log-direct-pyr'));

                          return response([
                            'status' => true,
                            'upload' => true,
                            'sukses' => $sukses,
                            'gagal'  => $gagal,
                            'log' => '<a href="'.asset('storage/uploads/log-direct-pyr/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
                          ]);
                        }else{
                          return response([
                            'status' => true,
                            'upload' => true,
                            'sukses' => $sukses,
                            'gagal'  => $gagal
                          ]);
                        }

                      }
                    }
                  }else{
                    return back()->with('error','Please Check your file, Something is wrong there.');
                  }
                }

                public function importexcel()
                {
                  return $this->render('modules.transaksi.finance.direct.import');
                }

                public function importexcel2()
                {
                  return $this->render('modules.transaksi.finance.direct.import2');
                }

                public function importexcelopening()
                {
                  return $this->render('modules.transaksi.finance.direct.opening');
                }

                public function printDirect($nomor)
                {
                  $pyr = TransPyr::where('nomor_pyr',$nomor)->first();
                  if ($pyr) {
                    if($pyr->vendor_lokal == 0){
                      $tmuk = Tmuk::with('lsi')->where('kode', $pyr->tmuk->kode)->first();
                      $vendor = $tmuk->lsi->nama;
                      $alamat_vendor = $tmuk->lsi->alamat;
                      $tlp_vendor = $tmuk->lsi->telepon;
                    }else if($pyr->vendor_lokal == 100){
                      $tmuk = Tmuk::with('lsi')->where('kode', $pyr->tmuk->kode)->first();
                      $vendor = $tmuk->nama;
                      $alamat_vendor = $tmuk->alamat;
                      $tlp_vendor = $tmuk->telepon;
                    }else{
                      $lokal = VendorLokalTmuk::where('id', $pyr->vendor_lokal)->first();
                      $vendor = isset($lokal) ? $lokal->nama : '-';
                      $alamat_vendor = $lokal->alamat;
                      $tlp_vendor = $lokal->telepon;
                    }

                    $data = [
                      'record'        => $pyr,
                      'vendor'        => $vendor,
                      'alamat_vendor' => $alamat_vendor,
                      'tlp_vendor'    => $tlp_vendor,
                    ];
                    $content = view('report.direct-invoice-pyr', $data);
                    $oriented = 'P';
                  }else{
                    $pr = [
                      'record' => TransPr::where('nomor', $nomor)->first(),
                    ];
                    $content = view('report.direct-invoice-pr', $pr);
                    $oriented = 'L';
                  }


                  $data = [
                    'oriented'   => $oriented,
                    'paper'      => 'A4',
                    'label_file' => 'Direct Invoice '.$nomor,
                  ];

                  HTML2PDF($content, $data);
                }
              }
