<?php

namespace Lotte\Http\Controllers\Transaksi\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\SecTransaksi;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Coa;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Http\Requests\Transaksi\Finance\JurnalManualRequest;
use Lotte\Models\Trans\TransLogAudit;

use Datatables;
use Carbon\Carbon;

class JurnalManualController extends Controller
{
	protected $link = 'transaksi/finance/jurnal-manual/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Transaksi Jurnal Manual");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen longer");
		$this->setBreadcrumb(['Transaksi' => '#', 'Finance' => '#', 'Jurnal Manual' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */

			[
			    'data' => 'tanggal',
			    'name' => 'tanggal',
			    'label' => 'Tanggal Jurnal',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'idtrans',
			    'name' => 'idtrans',
			    'label' => 'No Jurnal',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'tmuk_kode',
			    'name' => 'tmuk_kode',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '50px',
			]
		]);
	}

	public function grid(Request $request)
	{
		// $records = TransJurnal::select('tmuk_kode','timestamp(tanggal)', 'idtrans')->groupBy('tmuk_kode', 'timestamp(tanggal)', 'idtrans')->where('delete', 0)->orderBy('tanggal', 'DESC');

        $records = TransJurnal::from(\DB::raw(" (select
                    tanggal ::date,
                    tmuk_kode,
                    idtrans
                    FROM
                    trans_jurnal
                    where delete = 0 
                    GROUP BY tmuk_kode, idtrans, tanggal ::date) d"));

        // CAST(tanggal, <datetime>)

// AND -> whereBetween("time", '2016-04-12 00:00:00', '2016-04-12 23:59:59')
        // dd($records);
		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', $kode);
        }
        if ($no_jurnal = $request->no_jurnal) {
            $records->where('idtrans', 'like', '%' . $no_jurnal . '%');
        }
        if($tanggal = $request->tanggal){
            $dates = \Carbon\Carbon::parse($tanggal)->format('Y-m-d');
            $records->where(\DB::raw('DATE(tanggal)'),$dates);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tmuk_kode', function ($record){
            	return $record->tmuk->nama;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type'    => 'edit',
                    'tooltip' => 'Lihat Data',
                    'id'      => $record->idtrans
                ]);
                // Delete
                // $btn .= $this->makeButton([
                // 	'type' => 'delete',
                // 	'id'   => $record->idtrans
                // ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        if (date('d') <= 10) {
            $startdate = Carbon::createFromFormat('Y-m-d', date('Y').'-'.(date('m') -1 ).'-01');
        } else {
            $startdate = Carbon::createFromFormat('Y-m-d', date('Y-m').'-01');
        }
        $enddate = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
        // dump($startdate->month);
        // dump($startdate);
        // dump($enddate);
        // dd('s');

        return $this->render('modules.transaksi.finance.jurnal-manual.index', [
            'mockup' => false,
            'startdate' => $startdate,
            'enddate' => $enddate,
        ]);
    }

    public function create()
    {
        // $startdate = Carbon::createFromFormat('Y-m-d', '2018-05-05');
        // $enddate = Carbon::createFromFormat('Y-m-d', '2018-05-10');

        return $this->render('modules.transaksi.finance.jurnal-manual.create');
    }

    public function store(JurnalManualRequest $request)
    {
        // dd($request->all());
        $jumlah_debit = array_sum(decimal_for_save($request->jumlah_debit));
        $jumlah_kredit = array_sum(decimal_for_save($request->jumlah_kredit));

        if ($jumlah_debit != $jumlah_kredit) {
            return response(['message' => 'Total Debit dan Total Kredit tidak sama'], 404);
        }else{
            $l = sizeof($request->coa_kode_kode);

            $idtrans     = SecTransaksi::generate();
            $th_fiskal   = TahunFiskal::getTahunFiskal();
            $coa_ekuitas = array('3.1.0.0', '3.1.1.0', '3.1.2.0', '3.2.0.0', '3.3.0.0', '3.3.0.0', '3.4.0.0');

            for($x=0; $x < $l; $x++){
              $jurnal = new TransJurnal;
                $jurnal->idtrans         = $idtrans;
                $jurnal->tanggal         = $request->tanggal;
                $jurnal->tmuk_kode       = $request->tmuk_kode;
                $jurnal->coa_kode        = $request->coa_kode_kode[$x];
                $jurnal->deskripsi       = $request->deskripsi_memo[$x];
                $jurnal->jumlah          = $request->jumlah_debit[$x] != 0 ? decimal_for_save($request->jumlah_debit[$x]) : decimal_for_save($request->jumlah_kredit[$x]);
                $jurnal->posisi          = $request->jumlah_debit[$x] != 0 ? 'D' : 'K';

                if (in_array($request->coa_kode_kode[$x], $coa_ekuitas)){
                    $jurnal->flag            = $request->jumlah_debit[$x] != 0 ? '-' : '+';
                }else{
                    $jurnal->flag            = $request->jumlah_debit[$x] != 0 ? '+' : '-';
                }

                $jurnal->tahun_fiskal_id = $th_fiskal->id;
                $jurnal->save();
            }

            //SALDO BERTAMBAH
                //check coa deposit => 1.1.6.1
                $check_deposit = TransJurnal::where('idtrans', $idtrans)
                                            ->where('tmuk_kode', $jurnal->tmuk_kode)
                                            ->where('posisi', 'D')
                                            ->where('coa_kode', '1.1.6.1')
                                            ->first();
                if ($check_deposit) {
                    $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    $tmuk->saldo_deposit = $tmuk->saldo_deposit + $check_deposit->jumlah;
                    $tmuk->save();
                }

                //check coa escrow => 1.1.2.1
                $check_escrow = TransJurnal::where('idtrans', $idtrans)
                                            ->where('tmuk_kode', $jurnal->tmuk_kode)
                                            ->where('posisi', 'D')
                                            ->where('coa_kode', '1.1.2.1')
                                            ->first();

                if ($check_escrow) {
                    $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    $tmuk->saldo_escrow = $tmuk->saldo_escrow + $check_escrow->jumlah;
                    $tmuk->save();
                }

            //SALDO BERKURANG
                //check coa deposit => 1.1.6.1
                $check_deposit = TransJurnal::where('idtrans', $idtrans)
                                            ->where('tmuk_kode', $jurnal->tmuk_kode)
                                            ->where('posisi', 'K')
                                            ->where('coa_kode', '1.1.6.1')
                                            ->first();
                if ($check_deposit) {
                    $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    $tmuk->saldo_deposit = $tmuk->saldo_deposit - $check_deposit->jumlah;
                    $tmuk->save();
                }

                //check coa escrow => 1.1.2.1
                $check_escrow_k = TransJurnal::where('idtrans', $idtrans)
                                            ->where('tmuk_kode', $jurnal->tmuk_kode)
                                            ->where('posisi', 'K')
                                            ->where('coa_kode', '1.1.2.1')
                                            ->first();

                if ($check_escrow_k) {
                    $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    $tmuk->saldo_escrow = $tmuk->saldo_escrow - $check_escrow_k->jumlah;
                    $tmuk->save();
                }

                //check coa Biaya Distribusi (Adm) => 6.2.2.10
                $check_biaya = TransJurnal::where('idtrans', $idtrans)
                                            ->where('tmuk_kode', $jurnal->tmuk_kode)
                                            ->where('posisi', 'D')
                                            ->where('coa_kode', '6.2.2.10')
                                            ->first();

                if ($check_biaya) {
                    $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    $tmuk->saldo_escrow = $tmuk->saldo_escrow - $check_biaya->jumlah;
                    $tmuk->save();
                }

                //check coa Biaya Pengiriman (Adm) => 6.2.2.1
                $check_biaya_pengiriman = TransJurnal::where('idtrans', $idtrans)
                                            ->where('tmuk_kode', $jurnal->tmuk_kode)
                                            ->where('posisi', 'D')
                                            ->where('coa_kode', '6.2.2.1')
                                            ->first();

                if ($check_biaya_pengiriman) {
                    $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    $tmuk->saldo_escrow = $tmuk->saldo_escrow - $check_biaya_pengiriman->jumlah;
                    $tmuk->save();
                }

                //check coa Biaya Admin Bank => 8.3.0.0
                $check_admin_bank = TransJurnal::where('idtrans', $idtrans)
                                            ->where('tmuk_kode', $jurnal->tmuk_kode)
                                            ->where('posisi', 'D')
                                            ->where('coa_kode', '8.3.0.0')
                                            ->first();

                if ($check_admin_bank) {
                    $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    $tmuk->saldo_escrow = $tmuk->saldo_escrow - $check_admin_bank->jumlah;
                    $tmuk->save();
                }

                // insert to log audit
                TransLogAudit::setLog([
                    'tanggal_transaksi' => date('Y-m-d'),
                    'type'              => 'Jurnal Manual',
                    'ref'               => '',
                    'aksi'              => 'Dibuat',
                    'amount'            =>  $jumlah_debit + $jumlah_kredit ,
                ]);

                return response([
                    'status' => true,
                    'data'  => $jurnal
                ]);
        }
    }

    public function edit($idtrans)
    {
        $record = TransJurnal::where('idtrans', $idtrans)->where('delete', 0)->orderBy('id', 'ASC')->get();

        return $this->render('modules.transaksi.finance.jurnal-manual.edit', ['record' => $record]);
    }

    // public function update(Request $request, $id)
    // {
    //     $data = $request->all();
        
    //     $array = array_filter($data['id_jurnal']);

    //     foreach ($array as $key => $value) {
    //         $jurnal = TransJurnal::find($data['id_jurnal'][$key]);

    //         if (!is_null($jurnal)) {
    //             $jurnal->deskripsi  = $data['deskripsi_memo'][$key];
    //             $jurnal->jumlah     = ($data['jumlah_debit'][$key] != 0) ? decimal_for_save($data['jumlah_debit'][$key]) : decimal_for_save($data['jumlah_kredit'][$key]);
    //             $jurnal->posisi     = ($data['jumlah_debit'][$key] != 0) ? 'D' : 'K';
    //             $jurnal->flag       = ($data['jumlah_debit'][$key] != 0) ? '+' : '-';
    //             $jurnal->save();
    //         }
    //     }

    //     // $check = TransJurnal::where('idtrans', $idtrans)->where('tmuk_kode', $jurnal->tmuk_kode)->where('posisi', 'D')->where('coa_kode', '1.1.6.1')->first();
    //     // if ($check) {
    //     //     $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
    //     //     $tmuk->saldo_deposit = $check->jumlah;
    //     //     $tmuk->save();
    //     // }
        
    //     return response([
    //         'status' => true,
    //         'data'  => $jurnal
    //     ]);
    // }

    public function destroy($idtrans)
    {
        $check = TransJurnal::where('idtrans', $idtrans)->get();

        foreach ($check as $value) {
            $jurnal = TransJurnal::find($value['id']);
            $jurnal->delete = 1;
            $jurnal->save();   
        }

        return response([
            'status' => true,
        ]);
    }
}
