<?php

namespace Lotte\Http\Controllers\Transaksi\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

class BudgetEntriController extends Controller
{
	protected $link = 'transaksi/finance/budget-entri/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("KKI TMUK");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen longer");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'KKI TMUK' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'tmuk',
			    'name' => 'tmuk',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '500px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'KKI',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			]
		]);
	}

    public function index()
    {
        return $this->render('modules.transaksi.finance.budget-entri.index');
    }

    public function create()
    {
        return $this->render('modules.transaksi.finance.budget-entri.create');
    }

    public function edit($id)
    {
        return $this->render('modules.transaksi.finance.budget-entri.edit');
    }
}
