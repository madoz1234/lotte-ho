<?php

namespace Lotte\Http\Controllers\Transaksi\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\SecTransaksi;
use Lotte\Models\Master\Coa;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransLogAudit;

use Datatables;
use Carbon\Carbon;

class PembatalanJurnalController extends Controller
{
	protected $link = 'transaksi/finance/pembatalan-jurnal/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Pembatalan Jurnal");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen longer");
		$this->setBreadcrumb(['Transaksi' => '#', 'Finance' => '#', 'Pembatalan Jurnal' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '20px',
			],
			/* --------------------------- */
			[
			    'data' => 'tanggal',
			    'name' => 'tanggal',
			    'label' => 'Tanggal Jurnal',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'idtrans',
			    'name' => 'idtrans',
			    'label' => 'No Jurnal',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'tmuk_kode',
			    'name' => 'tmuk_kode',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Detail',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '50px',
			],
			[
                'data' => 'batalkan',
                'name' => 'batalkan',
                'label' => 'Pembatalan',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '50px',
            ],
		]);
	}

	public function grid(Request $request)
	{

		$records = TransJurnal::select('tmuk_kode', 'tanggal', 'idtrans', 'delete')->groupBy('tmuk_kode', 'tanggal', 'idtrans', 'delete');

        //Filters
        if ($kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', $kode);
        }
        if ($coa = $request->coa_kode) {
            $records->where('coa_kode', $coa);
        }
        if ($tanggal = $request->tanggal) {
            $end    = Carbon::createFromFormat('M d, Y', $tanggal)->format('Y-m-d');

            $records->whereBetween('tanggal', [$end . ' 00:00:00' , $end . ' 23:59:59']);
        }
        if ($no_jurnal = $request->no_jurnal) {
            $records->where('idtrans', $no_jurnal);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tmuk_kode', function ($record){
            	return $record->tmuk->nama;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'detail',
                	'id'   => $record->idtrans
                ]);
                return $btn;
            })

            ->addColumn('batalkan', function ($record) {
                $string = '';
                if ($record->delete == 0) {
                    $string = '<button type="button" class="ui circular mini green icon toggle button" id="pembatalan" data-content="Batalkan" data-id="'.$record->idtrans.'">Batalkan</button>';
                } else{
                    $string = '<button type="button" class="ui circular mini red icon toggle button" id="pembatalan" data-content="Dibatalkan" disabled="">Dibatalkan</button>';
                }
                return $string;
            })
            ->make(true);
	}

	//Pembatalan
    public function onChangePopPembatalan($idtrans)
    {

        $record = TransJurnal::where('idtrans', $idtrans)->get();

        foreach ($record as $value) {
            $jurnal = TransJurnal::find($value['id']);

            //deposit
            if ($jurnal->coa_kode == '1.1.6.1') {
                $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    if ($jurnal->posisi == 'D') {
                        $tmuk->saldo_deposit = $tmuk->saldo_deposit - $jurnal->jumlah;
                    }else{
                        $tmuk->saldo_deposit = $tmuk->saldo_deposit + $jurnal->jumlah;
                    }
                $tmuk->save();
            }

            //escrow
            if($jurnal->coa_kode == '1.1.2.1'){
                $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    if ($jurnal->posisi == 'D') {
                        $tmuk->saldo_escrow = $tmuk->saldo_escrow - $jurnal->jumlah;
                    }else{
                        $tmuk->saldo_escrow = $tmuk->saldo_escrow + $jurnal->jumlah;
                    }
                $tmuk->save();
            }

            //check coa Biaya Distribusi (Adm) => 6.2.2.10
            if($jurnal->coa_kode == '6.2.2.10'){
                $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    if ($jurnal->posisi == 'D') {
                        $tmuk->saldo_escrow = $tmuk->saldo_escrow + $jurnal->jumlah;
                    }
                $tmuk->save();
            }

            //check coa Biaya Pengiriman (Adm) => 6.2.2.1
            if($jurnal->coa_kode == '6.2.2.1'){
                $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    if ($jurnal->posisi == 'D') {
                        $tmuk->saldo_escrow = $tmuk->saldo_escrow + $jurnal->jumlah;
                    }
                $tmuk->save();
            }

            //check coa Biaya Admin Bank => 8.3.0.0
            if($jurnal->coa_kode == '8.3.0.0'){
                $tmuk = Tmuk::where('kode', $jurnal->tmuk_kode)->first();
                    if ($jurnal->posisi == 'D') {
                        $tmuk->saldo_escrow = $tmuk->saldo_escrow + $jurnal->jumlah;
                    }
                $tmuk->save();
            }

            $jurnal->delete = 1;
            $jurnal->save();   
        }

        // insert to log audit
            TransLogAudit::setLog([
                'tanggal_transaksi' => date('Y-m-d'),
                'type'              => 'Pembatalan Jurnal ',
                'ref'               => '',
                'aksi'              => 'Dibatalkan',
                'amount'            => $jurnal->jumlah,
            ]);

        return response([
            'status' => true,
        ]);
    }

    public function index()
    {
        return $this->render('modules.transaksi.finance.pembatalan-jurnal.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.transaksi.finance.pembatalan-jurnal.create');
    }

    public function detail($idtrans)
    {
    	$record = TransJurnal::where('idtrans', $idtrans)->get();

        return $this->render('modules.transaksi.finance.pembatalan-jurnal.detail', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $array = array_filter($data['id_jurnal']);

        foreach ($array as $key => $value) {
            $jurnal = TransJurnal::find($data['id_jurnal'][$key]);

            if (!is_null($jurnal)) {
                $jurnal->deskripsi  = $data['deskripsi_memo'][$key];
                $jurnal->jumlah     = ($data['jumlah_debit'][$key] != 0) ? decimal_for_save($data['jumlah_debit'][$key]) : decimal_for_save($data['jumlah_kredit'][$key]);
                $jurnal->posisi     = ($data['jumlah_debit'][$key] != 0) ? 'D' : 'K';
                $jurnal->flag       = ($data['jumlah_debit'][$key] != 0) ? '+' : '-';
                $jurnal->delete 	= $data['delete'][$key];
                $jurnal->save();
            }
        }

        return response([
            'status' => true,
            'data'  => $jurnal
        ]);
    }
}
