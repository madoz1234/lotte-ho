<?php

namespace Lotte\Http\Controllers\Transaksi\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\SecTransaksi;
use Lotte\Models\Master\Coa;

use Datatables;
use Carbon\Carbon;

class ReportJurnalController extends Controller
{
	protected $link = 'transaksi/finance/jurnal-manual/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Report Jurnal");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen longer");
		$this->setBreadcrumb(['Transaksi' => '#', 'Finance' => '#', 'Report Jurnal' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */

			[
			    'data' => 'tanggal',
			    'name' => 'tanggal',
			    'label' => 'Tanggal Jurnal',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'idtrans',
			    'name' => 'idtrans',
			    'label' => 'No Jurnal',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'tmuk_kode',
			    'name' => 'tmuk_kode',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '50px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TransJurnal::select('tmuk_kode', 'tanggal', 'idtrans', 'posisi', 'jumlah')->where('delete', 0);

        //Filters
        if ($kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', $kode);
        }
        if ($no_jurnal = $request->no_jurnal) {
            $records->where('idtrans', 'like', '%' . $no_jurnal . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tmuk_kode', function ($record){
            	return $record->tmuk->nama;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->idtrans
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->idtrans
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        $record = TransJurnal::get();
        $temp =[];

        foreach($record as $row){
            $temp[$row['idtrans']][] = [
                'tmuk'      => $row->tmuk->nama,
                'idtrans'  => $row->idtrans,
                'tanggal'   => $row->tanggal,
                'coa_kode' => $row->coa_kode,
                'coa_nama' => $row->coa->nama,
                'jumlah'   => $row->jumlah,
                'posisi'   => $row->posisi,
            ];
        }

        $header = TransJurnal::select('idtrans', 'tanggal', 'coa_kode', 'jumlah')->groupBy('idtrans', 'tanggal', 'coa_kode', 'jumlah')->where('delete',0)->orderBy('idtrans')->get();

        return $this->render('modules.transaksi.finance.report-jurnal.index', ['record' => $record , 'temp' => $temp , 'data' => $header ]);
    }

    public function cari(Request $request)
    {
        dd('TARA');

        $record = TransJurnal::get();
        $temp =[];

        foreach($record as $row){
            $temp[$row['idtrans']][] = [
                'idtrans'  => $row->idtrans,
                'tanggal'   => $row->tanggal,
                'coa_kode' => $row->coa_kode,
                'coa_nama' => $row->coa->nama,
                'jumlah'   => $row->jumlah,
                'posisi'   => $row->posisi,
            ];
        }

        $header = TransJurnal::select('idtrans', 'tanggal', 'coa_kode', 'jumlah')->groupBy('idtrans', 'tanggal', 'coa_kode', 'jumlah')->where('delete',0)->orderBy('idtrans')->get();

        return $this->render('modules.transaksi.finance.report-jurnal.index', ['record' => $record , 'temp' => $temp , 'data' => $header ]);
    }



    public function create()
    {
        return $this->render('modules.transaksi.finance.jurnal-manual.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
    	$l = sizeof($request->coa_kode_kode);

    	$idtrans = SecTransaksi::generate();
		
		for($x=0; $x < $l; $x++){
			$jurnal = new TransJurnal;
            $jurnal->idtrans 	= $idtrans;
            $jurnal->tanggal 	= $request->tanggal;
            $jurnal->tmuk_kode 	= $request->tmuk_kode;
            $jurnal->coa_kode 	= $request->coa_kode_kode[$x];
            $jurnal->deskripsi 	= $request->deskripsi_memo[$x];
            $jurnal->jumlah     = $request->jumlah_debit[$x] != 0 ? decimal_for_save($request->jumlah_debit[$x]) : decimal_for_save($request->jumlah_kredit[$x]);
            $jurnal->posisi     = $request->jumlah_debit[$x] != 0 ? 'D' : 'K';
            $jurnal->flag       = $request->jumlah_debit[$x] != 0 ? '+' : '-';
            $jurnal->save();
		}

    	return response([
    		'status' => true,
    		'data'	=> $jurnal
    	]);
    }

    public function edit($idtrans)
    {
    	$record = TransJurnal::where('idtrans', $idtrans)->get();

        return $this->render('modules.transaksi.finance.jurnal-manual.edit', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $array = array_filter($data['id_jurnal']);

        foreach ($array as $key => $value) {
            $jurnal = TransJurnal::find($data['id_jurnal'][$key]);

            if (!is_null($jurnal)) {
                $jurnal->deskripsi  = $data['deskripsi_memo'][$key];
                $jurnal->jumlah     = ($data['jumlah_debit'][$key] != 0) ? decimal_for_save($data['jumlah_debit'][$key]) : decimal_for_save($data['jumlah_kredit'][$key]);
                $jurnal->posisi     = ($data['jumlah_debit'][$key] != 0) ? 'D' : 'K';
                $jurnal->flag       = ($data['jumlah_debit'][$key] != 0) ? '+' : '-';
                $jurnal->save();
            }
        }

        return response([
            'status' => true,
            'data'  => $jurnal
        ]);
    }

    public function destroy($idtrans)
    {
        $check = TransJurnal::where('idtrans', $idtrans)->get();

        foreach ($check as $value) {
            $jurnal = TransJurnal::find($value['id']);
            $jurnal->delete = 1;
            $jurnal->save();   
        }

        return response([
            'status' => true,
        ]);
    }
}
