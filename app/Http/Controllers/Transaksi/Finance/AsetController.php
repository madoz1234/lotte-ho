<?php

namespace Lotte\Http\Controllers\Transaksi\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;


use Lotte\Models\Trans\TransAkuisisiAset;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TipeAset;
use Lotte\Models\Trans\TransLogAudit;
// use Lotte\Http\Requests\Transaksi\Finance\AkuisisiAsetRequest;

use DB;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

class AsetController extends Controller
{
	protected $link = 'transaksi/finance/aset/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Akuisisi Aset");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("large");
		$this->setBreadcrumb(['Transaksi' => '#', 'Master Data Finance ' => '#', 'Akuisisi Aset' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'tmuk_kode',
			    'name' => 'tmuk_kode',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'tipe_id',
			    'name' => 'tipe_id',
			    'label' => 'Tipe Aset',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama Aset',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'no_seri',
			    'name' => 'no_seri',
			    'label' => 'Nomor Seri',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'tanggal_pembelian',
			    'name' => 'tanggal_pembelian',
			    'label' => 'Tanggal Pembelian',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nilai_pembelian',
			    'name' => 'nilai_pembelian',
			    'label' => 'Nilai Pembelian',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'kondisi',
			    'name' => 'kondisi',
			    'label' => 'Kondisi',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{

		// dd($request->all());
		$records = TransAkuisisiAset::whereIn('status', ['1']);
		// $records = TransAkuisisiAset::with('creator')
		// 				 ->select('*');

		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }
       if ($tipe_id = $request->tipe_id) {
            $records->where('tipe_id', $tipe_id);
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike',  '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('tmuk_kode', function ($record) {
                return $record->tmuk->kode .' - '. $record->tmuk->nama;
            })
            ->addColumn('tipe_id', function ($record) {
                return $record->tipe_id == null ? '-' : $record->tipeaset->tipe;
            })
            ->addColumn('nilai_pembelian', function ($record) {
                return rupiah($record->nilai_pembelian).',00';
            })
            ->addColumn('kondisi',function($record){
                if($record->kondisi == 1){
                    return 'Baru';
                }else if($record->kondisi == 2){
                    return 'Bekas';
                }
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.transaksi.finance.aset.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.transaksi.finance.aset.create');
    }

    public function store(Request $request)
    {
    	$data = $request->all();
    	// dd($data);
    	$aset = new TransAkuisisiAset;
    	$data['nilai_pembelian'] = decimal_for_save($data['nilai_pembelian']);
    	$aset->fill($data);
    	$aset->save();


    	// insert to log audit
	    TransLogAudit::setLog([
	        'tanggal_transaksi' => date('Y-m-d'),
	        'type'              => 'Akuisisi Asset ',
	        'ref'               => '',
	        'aksi'              => 'Dibuat',
	        'amount'            => $data['nilai_pembelian'],
	    ]);

    	return response([
    		'status' => true,
    		'data'	=> $aset
    	]);
    }

    public function edit($id)
    {
        $record = TransAkuisisiAset::find($id);

        return $this->render('modules.transaksi.finance.aset.edit', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
    	$data = $request->all();
    	// dd($request->all);

    	$aset = TransAkuisisiAset::find($id);
    	// $data['nilai_pembelian'] = decimal_for_save($data['nilai_pembelian']);
    	$data['no_seri'] = ($data['no_seri']);
    	$data['kondisi'] = ($data['kondisi']);
    	// dd($data['nilai_pembelian']);
    	// $aset->fill($data);
    	$aset->fill($data);
    	$aset->save();

    	// insert to log audit
	    TransLogAudit::setLog([
	        'tanggal_transaksi' => date('Y-m-d'),
	        'type'              => 'Akuisisi Asset ',
	        'ref'               => '',
	        'aksi'              => 'Diubah',
	        // 'amount'            => $data['nilai_pembelian'],
	    ]);

    	return response([
    		'status' => true,
    		'data'	=> $aset
    	]);
    }

    public function destroy($id)
    {
    	$aset = TransAkuisisiAset::find($id);
    	$data['status'] = 2;
    	$aset->fill($data);

    	$aset->save();
    	// $aset->delete();


    	// insert to log audit
	    TransLogAudit::setLog([
	        'tanggal_transaksi' => date('Y-m-d'),
	        'type'              => 'Akuisisi Asset ',
	        'ref'               => '',
	        'aksi'              => 'Dihapus',
	        'amount'            => decimal_for_save($aset['nilai_pembelian']),
	    ]);

    	return response([
    		'status' => true,
    	]);
    }
}
