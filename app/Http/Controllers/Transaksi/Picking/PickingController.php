<?php

namespace Lotte\Http\Controllers\Transaksi\Picking;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
//Libraries;
use DB;
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransPoDetail;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Trans\TransLogAudit;

class PickingController extends Controller
{
	protected $link = 'transaksi/picking/list-picking/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Transaksi Picking");
		$this->setSubtitle("&nbsp");
		$this->setModalSize("fullscreen longer");
		$this->setBreadcrumb(['Transaksi' => '#', 'Pengambilan (Picking)' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			],
			/* --------------------------- */

			[
			    'data' => 'tgl_buat',
			    'name' => 'tgl_buat',
			    'label' => 'Tanggal PR',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nomor',
			    'name' => 'nomor',
			    'label' => 'Nomor PR',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'tmuk',
			    'name' => 'tmuk',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'picking_task',
			    'name' => 'picking_task',
			    'label' => 'Picking Task',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'picking_result',
			    'name' => 'picking_result',
			    'label' => 'Picking Result',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TransPr::with('tmuk');
            if(auth()->user()->hasRole('system-administrator')){
                $records->orderBy('status', 'ASC');
            }elseif (isset(auth()->user()->pengguna->lsi_id)) {
                $records->whereHas('tmuk', function($q){
                    $q->where('lsi_id', auth()->user()->pengguna->lsi_id);
                })->orderBy('status', 'ASC');
            }else{
                $records->orderBy('status', 'ASC');
            }
                    
        // $records = TransPr::with('creator')
        //                  ->select('*');
		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($tgl_buat = $request->tgl_buat) {
            $records->where('tgl_buat',$tgl_buat);
        }
        if ($nomor_pr = $request->nomor_pr) {
            $records->where('nomor', 'like', '%' . $nomor_pr . '%');
        }
        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            // ->addColumn('region_id',function($record){
            //     return $record->region->area;
            // })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('tmuk', function ($record) {
                return $record->tmuk_kode.' - '.$record->tmuk->nama;
                // return $record->tmuk->nama;
            })
            ->addColumn('picking_task', function ($record) {
                $string = '<button type="button" class="ui mini orange icon detail button" data-content="Detil Picking" data-id="'.$record->id.'"><i class="edit icon"></i> Detil</button>
		        <a target="_blank" href="list-picking/picking-task/'.$record->id.'" class="ui mini default icon print button" data-content="Print Picking"><i class="print icon"></i> Print</a>';
				return $string;
            })
            // ->addColumn('gmd_price', function ($record) {
            //     $string = '<button type="button" class="ui mini orange icon add button" data-content="Cek Data" data-id="'.$record->id.'"> Cek</button>';
            //     return $string;
            // })
            ->addColumn('picking_result', function ($record) {
                $string = '';
                $dt = $record->created_at;
                $dt2 = Carbon::now();
                    // ($dt);
                $diff = $dt->diff($dt2)->days;
                    if ($record->status == 0) {
                        if($diff > 7){
                           return '<button type="button" data-content="Tolak" id="btn-tolak" data-id="'.$record->id.'" class="ui mini red icon tolak button"><i class="minus circle icon"></i></button>';
                        }else{
                            $string = '<button type="button" class="ui mini primary icon edit button" data-content="Buat Picking" data-id="'.$record->id.'"><i class="add icon"></i> Buat</button>';
                        }
                    }else{
                        if ($record->status == 3) {
                            return '<i style="font-size: 15px; color: red;" data-content="Melebihi Batas Waktu">Ditolak</i>';
                        }else{
                            $string = '<a target="_blank" href="list-picking/picking-result/'.$record->id.'" class="ui mini default icon print button" data-content="Print Picking Result"><i class="print icon"></i> Print</a>';
                        }
                        $string = '<a target="_blank" href="list-picking/picking-result/'.$record->id.'" class="ui mini default icon print button" data-content="Print Picking Result"><i class="print icon"></i> Print</a>';
                    }
                return $string;
            })
            // fitur penolakan lebih 7 hari
            // ->addColumn('picking_result', function ($record) {
            //     $string = '';
            //     $dt = $record->created_at;
            //     $dt2 = Carbon::now();
            //     $diff = $dt->diff($dt2)->days;
            //         if ($record->status == 0) {
            //             if($diff > 7){
            //                return '<i style="font-size: 15px; color: red;" data-content="Melebihi Batas Waktu">Ditolak</i>';
            //             }else{
            //             $string = '<button type="button" class="ui mini primary icon edit button" data-content="Buat Picking" data-id="'.$record->id.'"><i class="add icon"></i> Buat</button>';
            //             }
            //         }else{
            //             $string = '<a target="_blank" href="list-picking/picking-result/'.$record->id.'" class="ui mini default icon print button" data-content="Print Picking Result"><i class="print icon"></i> Print</a>';
            //         }
            //         return $string;
            // })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.transaksi.picking.list-picking.index', ['mockup' => false]);
    }

    public function detail($id)
    {
        $pr = TransPr::with('tmuk', 'tmuk.lsi')->find($id);
        // dd($pr->tmuk->lsi->kode);
        $records = TransPr::has('detailpr')->select(DB::raw('ref_produk_lsi.lsi_kode as lsi_kode,
                            trans_pr_detail.id as id_detail,
                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            trans_pr_detail.produk_kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.qty_pr as qty_pr,
                            trans_pr_detail.unit_pr as unit_pr,
                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.qty_aktual as qty_aktual'
                        ))
                      ->join('trans_pr_detail', 'trans_pr.nomor', '=', 'trans_pr_detail.pr_nomor')
                      ->join('ref_produk', 'trans_pr_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->where(DB::raw('trans_pr.nomor'), $pr->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get()->toArray();
        $data = [
            'record' => TransPr::find($id),
            'detail' => $records,
        ];
        return $this->render('modules.transaksi.picking.list-picking.detail', $data);
    }

    public function edit($id)
    {
        $pr = TransPr::with('tmuk', 'tmuk.lsi')->find($id);
        // dd($pr->tmuk->lsi->kode);
        $records = TransPr::has('detailpr')->select(DB::raw('ref_produk_lsi.lsi_kode as lsi_kode,
                            trans_pr_detail.id as id_detail,
                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            trans_pr_detail.produk_kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.qty_pr as qty_pr,
                            trans_pr_detail.unit_pr as unit_pr,
                            trans_pr_detail.qty_pick as qty_pick,
                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.qty_aktual as qty_aktual'
                        ))
                      ->join('trans_pr_detail', 'trans_pr.nomor', '=', 'trans_pr_detail.pr_nomor')
                      ->join('ref_produk', 'trans_pr_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->where(DB::raw('trans_pr.nomor'), $pr->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get()->toArray();
        // dd($records);
        $data = [
            'record' => $pr,
            'detail' => $records,
        ];
        
        return $this->render('modules.transaksi.picking.list-picking.edit', $data);
    }

    public function update(Request $request)
    {
        // update status pr
        // dd($request->produk['kode'])
        $nomorpr = TransPr::where('nomor', $request->nomor)->first();
        $nomorpr->status = 1;
        $nomorpr->save();

        // update qty pick tiap barang
        foreach ($request->produk['id_detail'] as $produk_kode => $value) {
            $picking = TransPrDetail::find($value);
            $picking->qty_pick = $request->produk['qty_pick'][$produk_kode];
            $picking->save();
        }

        $initial_stock = 0;
        $no_opening = substr($nomorpr->nomor, -4);
        if ($no_opening == '0000') {
            $initial_stock = 1;
        }

        // insert po
        $dt_po = [
            'nomor'         => TransPo::generateCode($nomorpr->tmuk_kode),
            'nomor_pr'      => $nomorpr->nomor,
            'tmuk_kode'     => $nomorpr->tmuk_kode,
            'tgl_buat'      => date('Y-m-d'),
            'total'         => 0,
            'keterangan'    => '-',
            'status'        => 0, // new
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => auth()->user()->id,
            'initial_stock' => $initial_stock,
        ];
        $po = new TransPo();
        $po->fill($dt_po);
        $po->save();

        // insert to detail
        $produk = ProdukLsi::with([   // ambil dulu produk lsi nya berdasarkan detail pr
            'lsi',
            'produk'
        ])->where('lsi_kode', $request->lsi_kode)->whereIn('produk_kode', $request->produk['kode'])->get();

        $total = 0;
        foreach ($produk as $val) {
            $podetail = new TransPoDetail();
            $podetail->fill([
                'po_nomor'    => $po->nomor,
                'produk_kode' => $val->produk_kode,
                'qty_po'      => $request->produk['qty_pick'][$val->produk_kode],
                'unit_po'     => $val->produk->produksetting->uom2_satuan,
                'qty_pr'      => $request->produk['qty_aktual'][$val->produk_kode],
                'unit_pr'     => $val->produk->produksetting->uom1_satuan,
                'price'       => priceBblm($val, $request->produk['qty_pick'][$val->produk_kode]),
                'created_at'  => date('Y-m-d H:i:s'),
                'created_by'  => auth()->user()->id,
            ]);
            $podetail->save();

            $total += $podetail->price;
        }

        // udpate total di trans po
        $po->total = $total;
        $po->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Transaksi Picking ',
            'ref'               => '',
            'aksi'              => 'Picking Result',
            'amount'            => $total,
        ]);

        return response([
            'status' => true,
            'data'  => 'success'
        ]);
    }

    public function hasilPengambilan()
    {
            $content = view('report.hasil-pengambilan');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Picking-Task',
            ];

            HTML2PDF($content, $data);
    }

    public function pickingTask($id)
    {
        $pr = TransPr::find($id);
        // dd($pr->tmuk->lsi->kode);
        $records = TransPr::has('detailpr')->select(DB::raw('ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            trans_pr_detail.produk_kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.qty_pr as qty_pr,
                            trans_pr_detail.unit_pr as unit_pr,
                            trans_pr_detail.qty_aktual as qty_aktual'
                        ))
                      ->join('trans_pr_detail', 'trans_pr.nomor', '=', 'trans_pr_detail.pr_nomor')
                      ->join('ref_produk', 'trans_pr_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->where(DB::raw('trans_pr.nomor'), $pr->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get()->toArray();
        $data = [
            'record' => $pr,
            'detail' => $records,
        ];

        // $content = view('report.picking-task', $data);

        // $data = [
        //         'oriented'   => 'L',
        //         'paper'      => 'A4',
        //         'label_file' => 'Picking Task '.$pr->nomor,
        // ];

        // HTML2PDF($content, $data);
        $pdf = \PDF::loadView('report.picking-task', $data);
            return $pdf->setOption('margin-bottom', 5)
            ->setOption('margin-top', 5)
            ->setOption('margin-left', 3)
            ->setOption('margin-right', 3)
            ->inline();
    }

    public function pickingResult($id)
    {
        $pr = TransPr::find($id);
        // dd($pr->tmuk->lsi->kode);
        $records = TransPr::has('detailpr')->take(5)->select(DB::raw('ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            trans_pr_detail.produk_kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.qty_pr as qty_pr,
                            trans_pr_detail.unit_pr as unit_pr,
                            trans_pr_detail.qty_pick as qty_pick,
                            trans_pr_detail.qty_aktual as qty_aktual'
                        ))
                      ->join('trans_pr_detail', 'trans_pr.nomor', '=', 'trans_pr_detail.pr_nomor')
                      ->join('ref_produk', 'trans_pr_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->where(DB::raw('trans_pr.nomor'), $pr->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get()->toArray();
        $data = [
            'record' => $pr,
            'detail' => $records,
        ];

        // $content = view('report.picking-result', $data);

        // $data = [
        //         'oriented'   => 'L',
        //         'paper'      => 'A4',
        //         'label_file' => 'Picking Result '.$pr->nomor,
        // ];

        // HTML2PDF($content, $data);
        $pdf = \PDF::loadView('report.picking-result', $data);
            return $pdf->setOption('margin-bottom', 5)
            ->setOption('margin-top', 5)
            ->setOption('margin-left', 3)
            ->setOption('margin-right', 3)
            ->inline();
    }


    //Tolak
    public function onChangePopTolak($id)
    {
        $statusreduce = TransPr::find($id);
        $statusreduce->status = 3;
        $statusreduce->save();
        // if($statusreduce->save())
        //     {
        //         $statuspo = TransPo::where('nomor', $statusreduce->po_nomor)->first();
        //         $statuspo->status  = 3;
        //         // $statuspo->save()
        //         if($statuspo->save())
        //             {
        //               $statuspr = TransPr::where('nomor', $statuspo->nomor_pr)->first();
        //               $statuspr->status  = 3;
        //               $statuspr->save();  
        //             }
        //     }
        
        return response([
            'status' => true,
            'data'  => $statusreduce
        ]);
    }
}
