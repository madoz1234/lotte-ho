<?php

namespace Lotte\Http\Controllers\Transaksi\Pembelian;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Transaksi\PyrRequest;
use Lotte\Http\Controllers\Controller;
//Libraries;
use DB;
use Datatables;
use Carbon\Carbon;
use Excel;

//model
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransPyrPembayaran;
use Lotte\Models\Trans\TransPyrDetail;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\SecTransaksi;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Master\TipeAset;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransHppMap;
use Lotte\Models\Trans\TransAkuisisiAset;
use Lotte\Models\Trans\TransLogAudit;
use Lotte\Models\Master\DetailVendorLokalTmuk;
use Lotte\Models\Master\ProdukHarga;

class PembayaranPemasokController extends Controller
{
    protected $link = 'transaksi/pembelian/pembayaran-pemasok/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Pembayaran PyR");
        $this->setSubtitle("&nbsp;");
        $this->setModalSize("fullscreen");
        $this->setBreadcrumb(['Transaksi' => '#', 'Pembelian (PO)' => '#', 'Pembayaran PyR' => '#']);
        $this->setTableStruct([
            [
             'data' => 'num',
             'name' => 'num',
             'label' => '#',
             'orderable' => false,
             'searchable' => false,
             'className' => "center aligned",
             'width' => '40px',
         ],
         /* --------------------------- */
         [
             'data' => 'tgl_buat',
             'name' => 'tgl_buat',
             'label' => 'Tanggal PYR',
             'searchable' => false,
             'sortable' => false,
             'className' => "center aligned",
         ],
         [
             'data' => 'nomor_pyr',
             'name' => 'nomor_pyr',
             'label' => 'No PYR',
             'searchable' => false,
             'sortable' => true,
             'className' => "center aligned",
         ],
         [
             'data' => 'vendor',
             'name' => 'vendor',
             'label' => 'Vendor',
             'searchable' => false,
             'sortable' => false,
             'className' => "center aligned",
         ],
         [
             'data' => 'tgl_tempo',
             'name' => 'tgl_tempo',
             'label' => 'Tanggal Jatuh Tempo',
             'searchable' => false,
             'sortable' => false,
             'className' => "center aligned",
         ],
         [
             'data' => 'price',
             'name' => 'price',
             'label' => 'Jumlah Tagihan (Rp)',
             'searchable' => false,
             'sortable' => false,
             'className' => "center aligned",
         ],
         [
             'data' => 'saldo_tmuk',
             'name' => 'saldo_tmuk',
             'label' => 'Saldo Escrow Saat Ini (Rp)',
             'searchable' => false,
             'sortable' => false,
             'className' => "center aligned",
         ],
         [
             'data' => 'bank',
             'name' => 'bank',
             'label' => 'Nama Bank',
             'searchable' => false,
             'sortable' => false,
             'className' => "center aligned",
         ],
         [
             'data' => 'biaya_admin',
             'name' => 'biaya_admin',
             'label' => 'Biaya Administrasi Bank (Rp)',
             'searchable' => false,
             'sortable' => false,
             'className' => "center aligned",
         ],
         [
             'data' => 'total_bayar',
             'name' => 'total_bayar',
             'label' => 'Total (Rp)',
             'searchable' => false,
             'sortable' => false,
             'className' => "center aligned",
             'width' => '150px',
         ],
         [
            'data' => 'verifikasi1_date',
            'name' => 'verifikasi1_date',
            'label' => 'Persetujuan 1',
            'searchable' => false,
            'sortable' => false,
            'className' => "center aligned",
        ],
        [
            'data' => 'verifikasi2_date',
            'name' => 'verifikasi2_date',
            'label' => 'Persetujuan 2',
            'searchable' => false,
            'sortable' => false,
            'className' => "center aligned",
        ],
        [
            'data' => 'verifikasi3_date',
            'name' => 'verifikasi3_date',
            'label' => 'Persetujuan 3',
            'searchable' => false,
            'sortable' => false,
            'className' => "center aligned",
        ],
        [
            'data' => 'verifikasi4_date',
            'name' => 'verifikasi4_date',
            'label' => 'Persetujuan 4',
            'searchable' => false,
            'sortable' => false,
            'className' => "center aligned",
        ],
        [
            'data' => 'action',
            'name' => 'action',
            'label' => 'Aksi',
            'searchable' => false,
            'sortable' => false,
            'className' => "center aligned",
        ],
        // [
        //     'data' => 'keterangan',
        //     'name' => 'keterangan',
        //     'label' => 'Keterangan',
        //     'searchable' => false,
        //     'sortable' => false,
        //     'className' => "center aligned",
        // ],
    ]);
    }

    public function grid(Request $request)
    {
        $records = TransPyrPembayaran::with('creator','groups')
        ->whereNull('verifikasi4_date')
        // ->where(function($query){
        //     $query->where(function($qu){
        //         $qu->where('status',0)
        //             ->where('group',null);
        //     })->orWhere(function($qu){
        //         $qu->where('status',0)
        //             ->where(\DB::raw('trans_pyr_pembayaran.nomor_pyr'),\DB::raw('trans_pyr_pembayaran.group'));
        //     });
        // })
        ->orderBy('created_at', 'DESC')
        ->select('*');

        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if($tgl_buat = $request->tgl_buat){
            $dates = \Carbon\Carbon::parse($tgl_buat)->format('Y-m-d');
            $records->whereHas('pyr', function($q) use ($dates){
              $q->where(\DB::raw('DATE(tgl_buat)'),$dates);  
          });
        }
        if ($nomor_pyr = $request->nomor_pyr) {
            // $records->where('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
            $records->where(function($query) use ($nomor_pyr){
                $query->whereHas('groups', function ($qu) use ($nomor_pyr) {
                    $qu->where('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
                })->orWhere('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
              });
        }
        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }
        if ($vendor_lokal = $request->vendor_lokal) {
            $records->whereHas('pyr', function ($query) use ($vendor_lokal) {
                $query->where('vendor_lokal',$vendor_lokal);
            });
        }

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('tgl_buat', function ($record) {
            return $record->pyr->tgl_buat;
        })
        ->addColumn('nomor_pyr', function ($record) {
                // $string = '';
                // $string = '<button type="button" class="circular ui icon button detail-pyr" data-content="Detail PYR" data-id="'.$record->id.'">'.$record->nomor_pyr.'</button>';
                // return $string;
            $string='';
            if(count($record->groups) > 0){
                $string .= '
                <div class="ui pyr button">'.$record->nomor_pyr.'</div>
                <div class="ui pyr popup top left transition hidden">
                <div class="ui one column grid">
                <div class="column">';
                foreach ($record->groups as $value) {
                  $string .= $value->nomor_pyr.'<br>';
              }
              $string .='
              </div>
              </div>
              </div>
              ';
          }else{
            $string .= '
            <div class="ui pyr button">'.$record->nomor_pyr.'</div>
            <div class="ui pyr popup top left transition hidden">
            <div class="ui one column grid">
            <div class="column">'.$record->nomor_pyr.'
            </div>
            </div>
            </div>
            ';
        }
        return $string;
    })
        ->addColumn('tmuk', function ($record) {
            return $record->pyr->tmuk->kode.' - '.$record->pyr->tmuk->nama;
        })
        ->addColumn('vendor', function ($record) {
                // return isset($record->pyr->vendorlokal->nama) ? $record->pyr->vendorlokal->nama : '-';
            if($record->pyr->vendor_lokal == 0){
                $tmuk = Tmuk::with('lsi')->where('kode', $record->pyr->tmuk->kode)->first();
                return $tmuk->lsi->nama;
            }else if($record->pyr->vendor_lokal == 100){
                return $record->pyr->tmuk->nama;
            }else{
                $vendor = VendorLokalTmuk::where('id', $record->pyr->vendor_lokal)->first();
                return isset($vendor) ? $vendor->nama : '-';
            }
        })
        ->addColumn('tgl_tempo', function ($record) {
            return $record->pyr->tgl_jatuh_tempo;
        })
        ->addColumn('price', function ($record) {
            $string='';
          if(count($record->groups) > 0){
            $sum = 0;
            foreach($record->groups as $value) {
              $sum += isset($value->pyr->detailPyr) ? $value->pyr->detailPyr->sum('price') : 0;
            }
            $string = $sum;
          }else{
            $string = isset($record->pyr->detailPyr) ? $record->pyr->detailPyr->sum('price') : 0;
          }
          return FormatNumber($string);
        })
        ->addColumn('saldo_tmuk', function ($record) {
            return FormatNumber($record->pyr->tmuk->saldo_escrow);
        })
        ->addColumn('bank', function ($record) {
                return $record->pyr->tmuk->bankescrow->nama;
            // if($record->pyr->vendor_lokal == 0 || $record->pyr->vendor_lokal == 100){
            //     $tmuk = Tmuk::with('lsi')->where('kode', $record->pyr->tmuk->kode)->first();
            //     return $tmuk->bankescrow->nama;
            // }else{
            //     $vendor = VendorLokalTmuk::where('id', $record->pyr->vendor_lokal)->first();
            //     return isset($vendor) ? $vendor->bankescrow->nama : '-';
            // }
        })
        ->addColumn('biaya_admin', function ($record) {
            $string = '';  
            if($record->verifikasi1_date != ''){
                return FormatNumber($record->biaya_admin);
            }else{
                if ($record->status == 0) {
                    // if (auth()->user()->hasRole('cso-persetujuan1')) {
                        $disabled = '';
                    // } else {
                    //     $disabled = 'readonly=""';
                    // }
                    return '<div class="ui fluid input">
                    <input type="text" name="biaya_admin_'.$record->id.'" placeholder="Biaya Admin" onkeypress="return isNumberKey(event)" value="0" '.$disabled.'>
                    </div>';
                }else{
                    return FormatNumber($record->biaya_admin);
                }
            }
            return $string;
        })
        ->addColumn('total_bayar', function ($record) {
            $string='';
          if(count($record->groups) > 0){
            $sum = 0;
            foreach($record->groups as $value) {
              $sum += isset($value->pyr->detailPyr) ? $value->pyr->detailPyr->sum('price') : 0;
            }
            $string = $sum;
          }else{
            $string = isset($record->pyr->detailPyr) ? $record->pyr->detailPyr->sum('price') : 0;
          }
          return FormatNumber($string);
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('verifikasi1_date', function ($record) {
            $disabled = 'disabled';
            $string = '';
            if ($record->verifikasi1_date == '') {
                if ($record->status == 1) {
                    $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->updated_at.'</i>';
                }else{
                    if (auth()->user()->hasRole('cso-persetujuan1')) {
                        $disabled = '';
                    } else {
                        $disabled = 'disabled';
                    }
                    $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-1" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
                }
            } else {
                $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi1_date.'</i>';
            }
            return $string;
        })
        ->addColumn('verifikasi2_date', function ($record) {
            $string = '';
            $disabled = 'disabled';
            if ($record->verifikasi2_date == '') {
                if ($record->status == 1) {
                    $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->updated_at.'</i>';
                }else{
                    if($record->verifikasi1_date){
                        if (auth()->user()->hasRole('manager-cso-persetujuan2')) {
                            $disabled = '';
                        }else{
                            $disabled = 'disabled';
                        }
                    }
                    $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-2" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
                }
            } else{
                $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi2_date.'</i>';
            }
            return $string;
        })
        ->addColumn('verifikasi3_date', function ($record) {
            $string = '';
            $disabled = 'disabled';
            if ($record->verifikasi3_date == '') {
                if ($record->status == 1) {
                    $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->updated_at.'</i>';
                }else{
                    if($record->verifikasi2_date){
                        if (auth()->user()->hasRole('director-cso-persetujuan3')) {
                            $disabled = '';
                        }else{
                            $disabled='disabled';
                        }
                    }
                    $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-3" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
                }
            } else{
                $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi3_date.'</i>';
            }
            return $string;
        })
        ->addColumn('verifikasi4_date', function ($record) {
            $string = '';
            $disabled = 'disabled';
            if ($record->statush2h == 1) {
             $string = '<i style="font-size: 12px; color: orange;" data-content="Sedang Proses">Proses H2H</i>';
                } else{
                    if ($record->verifikasi4_date == '') {
                        if ($record->status == 1) {
                            $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->updated_at.'</i>';
                        }else{
                            if($record->verifikasi3_date){
                                if (auth()->user()->hasRole('finance-tmuk-persetujuan4') || auth()->user()->hasRole('system-administrator')) {
                                    $disabled = '';
                                }else{
                                    $disabled='disabled';
                                }
                            }
                            $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-4" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
                        }
                    } else{
                        $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi4_date.'</i>';
                    }
            }
            return $string;
        })
        //test
        
        //test
        ->addColumn('action', function ($record) {
            // $string = '';
            // $disabled = 'disabled';
            // $string = '<a target="_blank" href="pembayaran-pemasok/print/'.$record->id.'" class="ui mini default icon print button '.($record->verifikasi4_date?'':$disabled).'" data-content="Print Pembayaran PYR"><i class="print icon"></i> Print</a>';
            // return $string;
            $string = '';
                $string = '<button type="button" class="ui mini orange icon detail button" data-content="Detail PYR" data-id="'.$record->id.'"><i class="eye icon"></i>  Detail</button>';
                return $string;
        })
        // ->addColumn('keterangan', function ($record) {
        //     return $record->keterangan;
        // })
        ->make(true);
    }

    public function gridHistori(Request $request)
    {
        $records = TransPyrPembayaran::with('creator')
        ->where(function($query){
            $query->where(function($qu){
                $qu->where('status',1)
                    ->where('group',null);
            })->orWhere(function($qu){
                $qu->where('status',1)
                    ->where(\DB::raw('trans_pyr_pembayaran.nomor_pyr'),\DB::raw('trans_pyr_pembayaran.group'));
            });
        })
        ->orderBy('verifikasi4_date', 'DESC')
        ->select('*');
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if($tgl_buat = $request->tgl_buat){
            $dates = \Carbon\Carbon::parse($tgl_buat)->format('Y-m-d');
            $records->whereHas('pyr', function($q) use ($dates){
              $q->where(\DB::raw('DATE(tgl_buat)'),$dates);  
          });
        }
        if ($nomor_pyr = $request->nomor_pyr) {
            // $records->where('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
            $records->where(function($query) use ($nomor_pyr){
                $query->whereHas('groups', function ($qu) use ($nomor_pyr) {
                    $qu->where('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
                })->orWhere('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
              });
        }
        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }
        if ($vendor_lokal = $request->vendor_lokal) {
            $records->whereHas('pyr', function ($query) use ($vendor_lokal) {
                $query->where('vendor_lokal',$vendor_lokal);
            });
        }

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('tgl_buat', function ($record) {
            return $record->pyr->tgl_buat;
        })
        ->addColumn('nomor_pyr', function ($record) {
            // $string = '';
            // $string = '<button type="button" class="circular ui icon button detail-pyr" data-content="Detail PYR" data-id="'.$record->id.'">'.$record->nomor_pyr.'</button>';
            // return $string;
            $string='';
            if(count($record->groups) > 0){
                $string .= '
                <div class="ui pyr button">'.$record->nomor_pyr.'</div>
                <div class="ui pyr popup top left transition hidden">
                <div class="ui one column grid">
                <div class="column">';
                foreach ($record->groups as $value) {
                  $string .= $value->nomor_pyr.'<br>';
              }
              $string .='
              </div>
              </div>
              </div>
              ';
          }else{
            $string .= '
            <div class="ui pyr button">'.$record->nomor_pyr.'</div>
            <div class="ui pyr popup top left transition hidden">
            <div class="ui one column grid">
            <div class="column">'.$record->nomor_pyr.'
            </div>
            </div>
            </div>
            ';
        }
        return $string;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->pyr->tmuk->kode.' - '.$record->pyr->tmuk->nama;
        })
        ->addColumn('vendor', function ($record) {
                // return $record->pyr->vendorlokal->nama;
            if($record->pyr->vendor_lokal == 0){
                $tmuk = Tmuk::with('lsi')->where('kode', $record->pyr->tmuk->kode)->first();
                return $tmuk->lsi->nama;
            }else if($record->pyr->vendor_lokal == 100){
                return $record->pyr->tmuk->nama;
            }else{
                $vendor = VendorLokalTmuk::where('id', $record->pyr->vendor_lokal)->first();
                return isset($vendor) ? $vendor->nama : '-';
            }
        })
        ->addColumn('tgl_tempo', function ($record) {
            return $record->pyr->tgl_jatuh_tempo;
        })
        ->addColumn('price', function ($record) {
            $string='';
          if(count($record->groups) > 0){
            $sum = 0;
            foreach($record->groups as $value) {
              $sum += isset($value->pyr->detailPyr) ? $value->pyr->detailPyr->sum('price') : 0;
            }
            $string = $sum;
          }else{
            $string = isset($record->pyr->detailPyr) ? $record->pyr->detailPyr->sum('price') : 0;
          }
          return FormatNumber($string);
        })
        ->addColumn('saldo_tmuk', function ($record) {
            return FormatNumber($record->pyr->tmuk->saldo_escrow);
        })
        ->addColumn('bank', function ($record) {
                return $record->pyr->tmuk->bankescrow->nama;
            // if($record->pyr->vendor_lokal == 0 || $record->pyr->vendor_lokal == 100){
            //     $tmuk = Tmuk::with('lsi')->where('kode', $record->pyr->tmuk->kode)->first();
            //     return $tmuk->bankescrow->nama;
            // }else{
            //     $vendor = VendorLokalTmuk::where('id', $record->pyr->vendor_lokal)->first();
            //     return isset($vendor) ? $vendor->bankescrow->nama : '-';
            // }
        })
        ->addColumn('biaya_admin', function ($record) {
            $string = '';  
            if($record->verifikasi1_date != ''){
                return FormatNumber($record->biaya_admin);
            }else{
                if ($record->status == 0) {
                    if (auth()->user()->hasRole('cso-persetujuan1')) {
                        $disabled = '';
                    } else {
                        $disabled = 'readonly=""';
                    }
                    return '<div class="ui fluid input">
                    <input type="text" name="biaya_admin_'.$record->id.'" placeholder="Biaya Admin" onkeypress="return isNumberKey(event)" value="0" '.$disabled.'>
                    </div>';
                }else{
                    return FormatNumber($record->biaya_admin);
                }
            }
            return $string;
        })
        ->addColumn('total_bayar', function ($record) {
            $string='';
          if(count($record->groups) > 0){
            $sum = 0;
            foreach($record->groups as $value) {
              $sum += isset($value->pyr->detailPyr) ? $value->pyr->detailPyr->sum('price') : 0;
            }
            $string = $sum;
          }else{
            $string = isset($record->pyr->detailPyr) ? $record->pyr->detailPyr->sum('price') : 0;
          }
          return FormatNumber($string);
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('verifikasi1_date', function ($record) {
            $disabled = 'disabled';
            $string = '';
            if ($record->verifikasi1_date == '') {
                if ($record->status == 1) {
                    $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->updated_at.'</i>';
                }else{
                    if (auth()->user()->hasRole('cso-persetujuan1')) {
                        $disabled = '';
                    } else {
                        $disabled = 'disabled';
                    }
                    $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-1" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
                }
            } else {
                $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi1_date.'</i>';
            }
            return $string;
        })
        ->addColumn('verifikasi2_date', function ($record) {
            $string = '';
            $disabled = 'disabled';
            if ($record->verifikasi2_date == '') {
                if ($record->status == 1) {
                    $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->updated_at.'</i>';
                }else{
                    if($record->verifikasi1_date){
                        if (auth()->user()->hasRole('manager-cso-persetujuan2')) {
                            $disabled = '';
                        }else{
                            $disabled = 'disabled';
                        }
                    }
                    $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-2" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
                }
            } else{
                $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi2_date.'</i>';
            }
            return $string;
        })
        ->addColumn('verifikasi3_date', function ($record) {
            $string = '';
            $disabled = 'disabled';
            if ($record->verifikasi3_date == '') {
                if ($record->status == 1) {
                    $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->updated_at.'</i>';
                }else{
                    if($record->verifikasi2_date){
                        if (auth()->user()->hasRole('director-cso-persetujuan3')) {
                            $disabled = '';
                        }else{
                            $disabled='disabled';
                        }
                    }
                    $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-3" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
                }
            } else{
                $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi3_date.'</i>';
            }
            return $string;
        })
        ->addColumn('verifikasi4_date', function ($record) {
            $string = '';
            $disabled = 'disabled';
            if ($record->verifikasi4_date == '') {
                if ($record->status == 1) {
                    $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->updated_at.'</i>';
                }else{
                    if($record->verifikasi3_date){
                        if (auth()->user()->hasRole('finance-tmuk-persetujuan4')) {
                            $disabled = '';
                        }else{
                            $disabled='disabled';
                        }
                    }
                    $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-4" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
                }
            } else{
                $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi4_date.'</i>';
            }
            return $string;
        })
        ->addColumn('action', function ($record) {
            // $string = '';
            // $string = '<a target="_blank" href="pembayaran-pemasok/print/'.$record->id.'" class="ui mini default icon print button" data-content="Print Pembayaran PYR"><i class="print icon"></i> Print</a>';
            // return $string;
            $string = '';
                $string = '
                <a target="_blank" href="pembayaran-pemasok/print/'.$record->id.'" class="ui mini default icon print button {{ $record->cekapprovalpyr($record->nomor_pyr) }}" data-content="Print Pembayaran PYR"><i class="print icon"></i> Print</a> <br>
                <button type="button" class="ui mini orange icon detail button" data-content="Detail PYR" data-id="'.$record->id.'"><i class="eye icon"></i>  Detail</button>
                ';
                return $string;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.transaksi.pembelian.pembayaran-pemasok.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.transaksi.pembelian.pembayaran-pemasok.create');
    }

    public function edit($id)
    {
        return $this->render('modules.transaksi.pembelian.pembayaran-pemasok.edit');
    }

    public function printPemasok()
    {
        $content = view('report.pembayaran-pemasok');

        $data = [
            'oriented'   => 'L',
            'paper'      => 'A4',
            'label_file' => 'Vendor Lokal Tmuk',
        ];

        HTML2PDF($content, $data);
    }

    public function detailPyr($id)
    {
        $pembayaran = TransPyrPembayaran::find($id);
        $pyr = TransPyr::with('detailPyr','tmuk', 'tmuk.lsi')->find($pembayaran->pyr->id);

        if($pyr->vendor_lokal == 0){
            $tmuk = Tmuk::with('lsi')->where('kode', $pyr->tmuk->kode)->first();
            $vendor = $tmuk->lsi->nama;
        }else{
            $lokal = VendorLokalTmuk::where('id', $pyr->vendor_lokal)->first();
            $vendor = isset($lokal) ? $lokal->nama : '-';
        }

        $data = [
            'record' => $pyr,
            'vendor' => $vendor,
        ];

        return $this->render('modules.transaksi.pembelian.pembayaran-pemasok.detail', $data);
    }

    public function detailPyr2($id)
    {
        // $pembayaran = TransPyrPembayaran::find($id);
        $pyr = TransPyr::with('detailPyr','tmuk', 'tmuk.lsi')->where('nomor_pyr',$id)->first();

        if($pyr->vendor_lokal == 0){
            $tmuk = Tmuk::with('lsi')->where('kode', $pyr->tmuk->kode)->first();
            $vendor = $tmuk->lsi->nama;
        }else{
            $lokal = VendorLokalTmuk::where('id', $pyr->vendor_lokal)->first();
            $vendor = isset($lokal) ? $lokal->nama : '-';
        }

        $data = [
            'record' => $pyr,
            'vendor' => $vendor,
        ];

        return $this->render('modules.transaksi.pembelian.pembayaran-pemasok.detail-pyr', $data);
    }

    //Approval 1
    public function onChangePopApproval1($id, $biaya)
    {
        $approval1 = TransPyrPembayaran::find($id);
        if(count($approval1->groups)>0){
            foreach ($approval1->groups as $value) {
                $approval1 = TransPyrPembayaran::find($value->id);
                $approval1->biaya_admin      = $biaya;
                $approval1->verifikasi1_date = date('Y-m-d h:i:s');
                $approval1->verifikasi1_user = auth()->user()->id;
                $approval1->save();
            }
        }else{
            $approval1->biaya_admin      = $biaya;
            $approval1->verifikasi1_date = date('Y-m-d h:i:s');
            $approval1->verifikasi1_user = auth()->user()->id;
            $approval1->save();
        }
        
        return response([
            'status' => true,
            'data'  => $approval1
        ]);
    }

    //Approval 2
    public function onChangePopApproval2($id)
    {
        
        $approval2 = TransPyrPembayaran::find($id);
        if(count($approval2->groups)>0){
            foreach ($approval2->groups as $value) {
                $approval2 = TransPyrPembayaran::find($value->id);
                $approval2->verifikasi2_date = date('Y-m-d h:i:s');
                $approval2->verifikasi2_user = auth()->user()->id;
                $approval2->save();
            }
        }else{
            $approval2->verifikasi2_date = date('Y-m-d h:i:s');
            $approval2->verifikasi2_user = auth()->user()->id;
            $approval2->save();
        }
        
        return response([
            'status' => true,
            'data'  => $approval2
        ]);
    }

    //Approval 3
    public function onChangePopApproval3($id)
    {
        $approval3 = TransPyrPembayaran::find($id);
        if(count($approval3->groups)>0){
            foreach ($approval3->groups as $value) {
                $approval3 = TransPyrPembayaran::find($value->id);
                $approval3->verifikasi3_date = date('Y-m-d h:i:s');
                $approval3->verifikasi3_user = auth()->user()->id;
                $approval3->save();
            }
        }else{
            $approval3->verifikasi3_date = date('Y-m-d h:i:s');
            $approval3->verifikasi3_user = auth()->user()->id;
            $approval3->save();
        }
        
        return response([
            'status' => true,   
            'data'  => $approval3
        ]);
    }

    //Approval 4
    public function onChangePopApproval4($id)
    {
        //h2h
        // $pembayaran = TransPyrPembayaran::find($id);
        // // dd(substr($pembayaran->pyr->total, 0,-3));
        // $filename = $pembayaran->nomor_pyr . date('YmdHis');
        // \Lotte\Libraries\H2H::createCsv([
        //     'CustRefNo'            => $filename,
        //     'ValueDate'            => date('YmdHi'),
        //     'Amount'               => $pembayaran->pyr->total,
        //     'TrxRemark'            => '',
        //     'RateVoucherCode'      => '',
        //     'ChargeType'           => '',
        //     'DebitAccount'         => $pembayaran->tmuk->rekeningpemilik->nomor_rekening,
        //     'SenderName'           => $pembayaran->tmuk->bankescrow->nama,
        //     'SenderAddress'        => $pembayaran->tmuk->bankescrow->kota->nama,
        //     'SenderCountryCode'    => '',
        //     'CreditAccount'        => $pembayaran->tmuk->rekeningpemilik->nomor_rekening,
        //     'BeneficiaryName'      => $pembayaran->tmuk->rekeningpemilik->nama_pemilik,
        //     'BeneficiaryAddress'   => $pembayaran->tmuk->rekeningpemilik->bankescrow->alamat,
        //     'BenBankIdentifier'    => '',
        //     'BenBankName'          => '',
        //     'BenBankAddress'       => '',
        //     'BenBankCountryCode'   => '',
        //     'InterBankIdentifier'  => '',
        //     'InterBankName'        => '',
        //     'InterBankAddress'     => '',
        //     'InterBankCountryCode' => '',
        //     'Notification'         => '',
        //     'BeneficiaryCategory'  => '',
        //     'BeneficiaryRelation'  => '',
        //     'BITransaction Code'   => '',
        //     'TemplateCode'         => '',
        // ]);

        // $pembayaran->verifikasi4_date = date('Y-m-d h:i:s');
        // $pembayaran->verifikasi4_user = auth()->user()->id;

        // // 0:awal, 1:proses upload, 2:proses bank, 3:sukses, 4:gagal
        // $pembayaran->statush2h = 1;
        // $pembayaran->keterangan = 'Proses Upload H2H';
        // $pembayaran->nomorh2h = $filename;
        // $pembayaran->save();

        // return response([
        //     'status' => true,
        //     'data'  => $pembayaran
        // ]);
        //h2h
        $pembayaran = TransPyrPembayaran::find($id);
        if(count($pembayaran->groups)==0){
            $biaya = $pembayaran->biaya_admin;
            $total_price = isset($pembayaran->pyr->detailPyr) ? $pembayaran->pyr->detailPyr->sum('price') : '0';

            $tmuk_saldo = Tmuk::where('kode', $pembayaran->tmuk_kode)->first();
            if (auth()->user()->hasRole('finance-tmuk-persetujuan4')) {
                if($biaya !== 0){
                    if (substr($pembayaran->nomor_pyr, 0, -24) == 'OPENING') {
                        if ($pembayaran->pyr->vendor_lokal == 0) {
                            //skema pemotongan di mulai dari deposit, escrow
                            // dd('opening');
                            $awal_saldo_deposit = $tmuk_saldo->saldo_deposit;
                            $awal_saldo_escrow = $tmuk_saldo->saldo_escrow;

                            $saldo_deposit = $awal_saldo_deposit - ($total_price + $biaya);
                            if ($saldo_deposit < 0) {
                            // dd('escrow');
                                
                                $saldo_escrow = $awal_saldo_escrow + $saldo_deposit;
                                
                                $tmuk_saldo->saldo_deposit = 0;
                                $tmuk_saldo->saldo_escrow = $saldo_escrow;
                            }else{
                            // dd('deposit');
                                $tmuk_saldo->saldo_deposit = $saldo_deposit;
                            }
                        }else{
                            $tmuk_saldo->saldo_escrow = ($tmuk_saldo->saldo_escrow - $total_price) - $biaya;                            
                        }
                    }else{
                        // dd('bukan opening');
                        $tmuk_saldo->saldo_escrow = ($tmuk_saldo->saldo_escrow - $total_price) - $biaya;
                    }
                }
                $tmuk_saldo->save();

                // update Pyr pemesanan, biaya_admin, saldo deposit
                $pembayaran->saldo_escrow = $tmuk_saldo->saldo_escrow;
            }

            $pembayaran->verifikasi4_date = date('Y-m-d h:i:s');
            $pembayaran->verifikasi4_user = auth()->user()->id;
            $pembayaran->status = 1;
            $pembayaran->save();

            $pyr = TransPyr::where('nomor_pyr', $pembayaran->nomor_pyr)->first();
            $pyr->status = 2;
            $pyr->save();

            //jika tipe pyr 001 masuk hpp map
            if($pyr->tipe == '001' || $pyr->tipe == '002'){
                //insert hpp map
                foreach ($pyr->detailPyr as $key => $detail) {
                    $tipe_produk = $detail->produks->produksetting->tipe_produk;
                    if ($tipe_produk != 0) {
                        $conversi_uom2 = $detail->produks->produksetting->uom2_conversion;

                        $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                        $qty   = $detail->qty * $conversi_uom2;
                        $price = $detail->price / $qty;
                        $total = $qty * $price;

                        if ($check->count() != 0) {
                            $jml = 0;
                            foreach ($check as $key => $data) {
                                $jml += $data->qty * $data->price;
                            }
                            $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                            $hpp_map = new TransHppMap;
                            $hpp_map->fill([
                                'tanggal'       => date('Y-m-d H:i:s'),
                                'po_nomor'      => $pyr->nomor_pyr,
                                'produk_kode'   => $detail->produk_kode,
                                'tmuk_kode'     => $pyr->tmuk_kode,
                                'qty'           => $qty,
                                'price'         => $price,
                                'map'           => $new_map,
                                'created_at'    => date('Y-m-d H:i:s'),
                                'created_by'    => auth()->user()->id,
                            ]);
                            $hpp_map->save();
                        }else{
                            $hpp_map = new TransHppMap;
                            $hpp_map->fill([
                                'tanggal'       => date('Y-m-d H:i:s'),
                                'po_nomor'      => $pyr->nomor_pyr,
                                'produk_kode'   => $detail->produk_kode,
                                'tmuk_kode'     => $pyr->tmuk_kode,
                                'qty'           => $qty,
                                'price'         => $price,
                                'map'           => $price,
                                'created_at'    => date('Y-m-d H:i:s'),
                                'created_by'    => auth()->user()->id,
                            ]);
                            $hpp_map->save();
                        }
                    }
                }
            }

            //JURNAL
            $idtrans = SecTransaksi::generate();
            $th_fiskal = TahunFiskal::getTahunFiskal();
            $tanggal = $pembayaran->pyr->tgl_buat;

            $tanggal_aset = date('Y-m-d');
            $last_day = date("Y-m-t", strtotime($tanggal));
            $deposit = '1.1.6.1'; //deposit
            $rekening = '1.1.2.1'; //escrow
            
            $tmuk_kode = $pembayaran->tmuk_kode;
            $persediaan = $total_price + $biaya;
            $tipe = $pembayaran->pyr->tipe;
            $nomor_pyr = $pembayaran->pyr->nomor_pyr;

            //PYR OPENING
            if (substr($nomor_pyr, 0, -24) == 'OPENING') {
                //insert hpp map jika produk tipe trade (001)
                foreach ($pyr->detailPyr as $key => $detail) {
                    $tipe_barang_kode = $detail->produks->produksetting->tipe_barang_kode;
                    if ($tipe_barang_kode == '001') {
                        $conversi_uom2 = $detail->produks->produksetting->uom2_conversion;

                        $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                        // $qty   = $detail->qty * $conversi_uom2;
                        $qty   = $detail->qty;
                        $price = $detail->price / $qty;
                        $total = $qty * $price;

                        if ($check->count() != 0) {
                            $jml = 0;
                            foreach ($check as $key => $data) {
                                $jml += $data->qty * $data->price;
                            }
                            $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                            $hpp_map = new TransHppMap;
                            $hpp_map->fill([
                                'tanggal'       => date('Y-m-d H:i:s'),
                                'po_nomor'      => $pyr->nomor_pyr,
                                'produk_kode'   => $detail->produk_kode,
                                'tmuk_kode'     => $pyr->tmuk_kode,
                                'qty'           => $qty,
                                'price'         => $price,
                                'map'           => $new_map,
                                'created_at'    => date('Y-m-d H:i:s'),
                                'created_by'    => auth()->user()->id,
                            ]);
                            $hpp_map->save();
                        }else{
                            $hpp_map = new TransHppMap;
                            $hpp_map->fill([
                                'tanggal'       => date('Y-m-d H:i:s'),
                                'po_nomor'      => $pyr->nomor_pyr,
                                'produk_kode'   => $detail->produk_kode,
                                'tmuk_kode'     => $pyr->tmuk_kode,
                                'qty'           => $qty,
                                'price'         => $price,
                                'map'           => $price,
                                'created_at'    => date('Y-m-d H:i:s'),
                                'created_by'    => auth()->user()->id,
                            ]);
                            $hpp_map->save();
                        }
                    }
                }

                $check = TransAkuisisiAset::where('nomor_pyr', $nomor_pyr)->get();
                if ($check) {
                    foreach ($check as $detail) {
                        $akuisisi = TransAkuisisiAset::find($detail->id);
                        $akuisisi->status = 1;
                        if($akuisisi->save()){
                            //depresiasi
                            if (!is_null($akuisisi->tipe_id)) {
                                $depresiasi = TipeAset::find($akuisisi->tipe_id);
                                $depresiasi_x = $detail->nilai_pembelian / $depresiasi->tingkat_depresiasi;
                                for($x = 0; $x < $depresiasi->tingkat_depresiasi; $x++){
                                    if($x == 0){
                                        TransJurnal::insert([
                                            [
                                                'idtrans'         => $idtrans,
                                                'tahun_fiskal_id' => $th_fiskal->id,
                                                'tanggal'         => $tanggal,
                                                'tmuk_kode'       => $tmuk_kode,
                                                'referensi'       => $nomor_pyr,
                                                'deskripsi'       => $detail->produk_kode.'Depresiasi Bulan Ke-'.$x,
                                                'coa_kode'        => '6.3.3.0',
                                                'jumlah'          => 0,
                                                'posisi'          => 'D',
                                                'flag'            => '+',
                                            ],
                                            [
                                                'idtrans'         => $idtrans,
                                                'tahun_fiskal_id' => $th_fiskal->id,
                                                'tanggal'         => $tanggal,
                                                'tmuk_kode'       => $tmuk_kode,
                                                'referensi'       => $nomor_pyr,
                                                'deskripsi'       => $detail->produk_kode.'Depresiasi Bulan Ke-'.$x,
                                                'coa_kode'        => '1.2.7.0',
                                                'jumlah'          => 0,
                                                'posisi'          => 'K',
                                                'flag'            => '-',
                                            ]
                                        ]);
                                    }else{
                                        $day_next = Carbon::now()->addMonths($x)->endOfMonth()->format('Y-m-d');
                                        $nilai_depresiasi = $depresiasi_x * $x;
                                        $idtrans_next = SecTransaksi::generate();

                                        TransJurnal::insert([
                                            [
                                                'idtrans' => $idtrans_next,
                                                'tahun_fiskal_id' => $th_fiskal->id,
                                                'tanggal' => $day_next.' 00:00:00',
                                                'tmuk_kode' => $tmuk_kode,
                                                'referensi' => $nomor_pyr,
                                                'deskripsi' => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                'coa_kode' => '6.3.3.0',
                                                'jumlah' => $nilai_depresiasi,
                                                'posisi' => 'D',
                                                'flag' => '+',
                                            ],
                                            [
                                                'idtrans' => $idtrans_next,
                                                'tahun_fiskal_id' => $th_fiskal->id,
                                                'tanggal' => $day_next.' 00:00:00',
                                                'tmuk_kode' => $tmuk_kode,
                                                'referensi' => $nomor_pyr,
                                                'deskripsi' => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                'coa_kode' => '1.2.7.0',
                                                'jumlah' => $nilai_depresiasi,
                                                'posisi' => 'K',
                                                'flag' => '-',
                                            ]
                                        ]);
                                    }
                                }
                            }

                        }
                    }
                }
            }

            if($tipe == '001'){
                //Trade Lotte
                TransJurnal::insert([
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Pembayaran PyR - Trade Lotte',
                        'coa_kode' => $rekening, // escrow
                        'jumlah' => $total_price,
                        'posisi' => 'K',
                        'flag' => '-',
                    ],
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Pembayaran PyR - Trade Lotte',
                        'coa_kode' => '2.1.1.0', //Utang Usaha
                        'jumlah' => $total_price,
                        'posisi' => 'D',
                        'flag' => '+',
                    ]
                ]);
            }else if($tipe == '002'){
                //Trade Non-Lotte
                TransJurnal::insert([
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Pembayaran PyR - Trade Non Lotte',
                        'coa_kode' => $rekening, // escrow
                        'jumlah' => $total_price,
                        'posisi' => 'K',
                        'flag' => '-',
                    ],
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Pembayaran PyR - Trade Non Lotte',
                        'coa_kode' => '2.1.1.0', //Utang Usaha
                        'jumlah' => $total_price,
                        'posisi' => 'D',
                        'flag' => '+',
                    ]
                ]);
            }else if($tipe == '003'){
                //Non Trade Lotte
                TransJurnal::insert([
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Pembayaran PyR - Non Trade Lotte',
                        'coa_kode' => $rekening, // escrow
                        'jumlah' => $total_price,
                        'posisi' => 'K',
                        'flag' => '-',
                    ],
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Pembayaran PyR - Non Trade Lotte',
                        'coa_kode' => '2.1.1.0', //Utang Usaha
                        'jumlah' => $total_price,
                        'posisi' => 'D',
                        'flag' => '+',
                    ]
                ]);
            }else if($tipe == '004'){
                //Non Trade Non Lotte
                TransJurnal::insert([
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Pembayaran PyR - Non Trade Non Lotte',
                        'coa_kode' => $rekening, // escrow
                        'jumlah' => $total_price,
                        'posisi' => 'K',
                        'flag' => '-',
                    ],
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Pembayaran PyR - Non Trade Non Lotte',
                        'coa_kode' => '2.1.1.0', //Utang Usaha
                        'jumlah' => $total_price,
                        'posisi' => 'D',
                        'flag' => '+',
                    ]
                ]);
            }

            //update id_transaksi di pyr
            $pyr->id_transaksi = $idtrans;
            $pyr->save();

            //Biaya Admin
            TransJurnal::insert([
                [
                    'idtrans' => $idtrans,
                    'tahun_fiskal_id' => $th_fiskal->id,
                    'tanggal' => $tanggal,
                    'tmuk_kode' => $tmuk_kode,
                    'referensi' => $nomor_pyr,
                    'deskripsi' => 'Biaya Admin',
                    'coa_kode' => '1.1.2.1', //escrow
                    'jumlah' => $biaya,
                    'posisi' => 'K',
                    'flag' => '-',
                ],
                [
                    'idtrans' => $idtrans,
                    'tahun_fiskal_id' => $th_fiskal->id,
                    'tanggal' => $tanggal,
                    'tmuk_kode' => $tmuk_kode,
                    'referensi' => $nomor_pyr,
                    'deskripsi' => 'Biaya Admin',
                    'coa_kode' => '8.3.0.0', //Biaya Admin Bank
                    'jumlah' => $biaya,
                    'posisi' => 'D',
                    'flag' => '+',
                ]
            ]);
        
        //satuan bukan dalam pilihan
        }else{
            foreach ($pembayaran->groups as $value) {
                $pembayaran2 = TransPyrPembayaran::find($value->id);
                $biaya = $pembayaran2->biaya_admin;
                $total_price = isset($pembayaran2->pyr->detailPyr) ? $pembayaran2->pyr->detailPyr->sum('price') : '0';

                //skema pemotongan di mulai dari deposit
                $tmuk_saldo = Tmuk::where('kode', $pembayaran2->tmuk_kode)->first();
                if (auth()->user()->hasRole('finance-tmuk-persetujuan4')) {
                    if($biaya !== 0){
                        if (substr($pembayaran2->nomor_pyr, 0, -24) == 'OPENING') {
                            if ($pembayaran2->pyr->vendor_lokal == 0) {
                                //skema pemotongan di mulai dari deposit, escrow
                                // dd('opening');
                                $awal_saldo_deposit = $tmuk_saldo->saldo_deposit;
                                $awal_saldo_escrow = $tmuk_saldo->saldo_escrow;

                                $saldo_deposit = $awal_saldo_deposit - ($total_price + $biaya);
                                if ($saldo_deposit < 0) {
                                // dd('escrow');
                                    
                                    $saldo_escrow = $awal_saldo_escrow + $saldo_deposit;
                                    
                                    $tmuk_saldo->saldo_deposit = 0;
                                    $tmuk_saldo->saldo_escrow = $saldo_escrow;
                                }else{
                                // dd('deposit');
                                    $tmuk_saldo->saldo_deposit = $saldo_deposit;
                                }
                            }else{
                                $tmuk_saldo->saldo_escrow = ($tmuk_saldo->saldo_escrow - $total_price) - $biaya;                            
                            }
                        }else{
                            // dd('bukan opening');
                            $tmuk_saldo->saldo_escrow = ($tmuk_saldo->saldo_escrow - $total_price) - $biaya;
                        }
                    }
                    $tmuk_saldo->save();
                    
                    // update Pyr pemesanan, biaya_admin, saldo deposit
                    $pembayaran2->saldo_escrow = $tmuk_saldo->saldo_escrow;
                }
                $pembayaran2->verifikasi4_date = date('Y-m-d h:i:s');
                $pembayaran2->verifikasi4_user = auth()->user()->id;
                $pembayaran2->status = 1;
                $pembayaran2->save();

                $pyr = TransPyr::where('nomor_pyr', $pembayaran2->nomor_pyr)->first();
                $pyr->status = 2;
                $pyr->save();

                //jika tipe pyr 001 masuk hpp map
                if($pyr->tipe == '001' || $pyr->tipe == '002'){
                    //insert hpp map
                    foreach ($pyr->detailPyr as $key => $detail) {
                        $tipe_produk = $detail->produks->produksetting->tipe_produk;
                        if ($tipe_produk != 0) {
                            $conversi_uom2 = $detail->produks->produksetting->uom2_conversion;

                            $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                            $qty   = $detail->qty * $conversi_uom2;
                            $price = $detail->price / $qty;
                            $total = $qty * $price;

                            if ($check->count() != 0) {
                                $jml = 0;
                                foreach ($check as $key => $data) {
                                    $jml += $data->qty * $data->price;
                                }
                                $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                                $hpp_map = new TransHppMap;
                                $hpp_map->fill([
                                    'tanggal'       => date('Y-m-d H:i:s'),
                                    'po_nomor'      => $pyr->nomor_pyr,
                                    'produk_kode'   => $detail->produk_kode,
                                    'tmuk_kode'     => $pyr->tmuk_kode,
                                    'qty'           => $qty,
                                    'price'         => $price,
                                    'map'           => $new_map,
                                    'created_at'    => date('Y-m-d H:i:s'),
                                    'created_by'    => auth()->user()->id,
                                ]);
                                $hpp_map->save();
                            }else{
                                $hpp_map = new TransHppMap;
                                $hpp_map->fill([
                                    'tanggal'       => date('Y-m-d H:i:s'),
                                    'po_nomor'      => $pyr->nomor_pyr,
                                    'produk_kode'   => $detail->produk_kode,
                                    'tmuk_kode'     => $pyr->tmuk_kode,
                                    'qty'           => $qty,
                                    'price'         => $price,
                                    'map'           => $price,
                                    'created_at'    => date('Y-m-d H:i:s'),
                                    'created_by'    => auth()->user()->id,
                                ]);
                                $hpp_map->save();
                            }
                        }

                        //test harga pembentukan harga non lotte
                        else {
                           $cek_produk_harga = substr($detail->produk_kode, -10,1);
                           if ($cek_produk_harga == '5' || $cek_produk_harga == '2') {
                                $conversi_uom2 = $detail->produks->produksetting->uom2_conversion ? $detail->produks->produksetting->uom2_conversion : 1;

                                    $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                                    $qty   = $detail->qty * $conversi_uom2;
                                    $price = $detail->price / $qty;
                                    $total = $qty * $price;

                                    if ($check->count() != 0) {
                                        $jml = 0;
                                        foreach ($check as $key => $data) {
                                            $jml += $data->qty * $data->price;
                                        }
                                        $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                                        $hpp_map = new TransHppMap;
                                        $hpp_map->fill([
                                            'tanggal'       => date('Y-m-d H:i:s'),
                                            'po_nomor'      => $pyr->nomor_pyr,
                                            'produk_kode'   => $detail->produk_kode,
                                            'tmuk_kode'     => $pyr->tmuk_kode,
                                            'qty'           => $qty,
                                            'price'         => $price,
                                            'map'           => $new_map,
                                            'created_at'    => date('Y-m-d H:i:s'),
                                            'created_by'    => auth()->user()->id,
                                        ]);
                                        $hpp_map->save();
                                    }else{
                                        $hpp_map = new TransHppMap;
                                        $hpp_map->fill([
                                            'tanggal'       => date('Y-m-d H:i:s'),
                                            'po_nomor'      => $pyr->nomor_pyr,
                                            'produk_kode'   => $detail->produk_kode,
                                            'tmuk_kode'     => $pyr->tmuk_kode,
                                            'qty'           => $qty,
                                            'price'         => $price,
                                            'map'           => $price,
                                            'created_at'    => date('Y-m-d H:i:s'),
                                            'created_by'    => auth()->user()->id,
                                        ]);
                                        $hpp_map->save();
                                    }
                           }
                        }
                        //test harga pembentukan harga non lotte
                        
                        // $test = $detail->price / $detail->qty;
                        // dd($detail->produk_kode);
                        //test
                        //insert ProdukHarga
                        //test array harga
                            // $cek_produk_harga = substr($detail->produk_kode, -10,1);
                            // dd($cek_produk_harga);
                            // if ($cek_produk_harga == '5' || $cek_produk_harga == '2') {
                                // foreach ($pyr->detailPyr as $key => $hasildata) {
                                //         dd($hasildata->produk_kode);
                                //     }
                            // }
                            // dd($detail->produk_kode);
                
                            $cek_produk_harga = substr($detail->produk_kode, -10,1);
                            if ($cek_produk_harga == '5' || $cek_produk_harga == '2') {
                                //test array harga 
                                $produk_harga = ProdukHarga::with('produksetting')
                                                    ->where('produk_kode', $detail->produk_kode)
                                                    ->where('tmuk_kode', $pyr->tmuk_kode)->first();
                                    // dd($detail->produks->produksetting->margin);
                                    $margin_produk = isset($detail->produks->produksetting->margin) ? $detail->produks->produksetting->margin : 1;
                                    $hpp           = $detail->price / $detail->qty;
                                    $margin_persen = ($margin_produk * $hpp) / 100;
                                    $hitung_margin = $margin_persen + $hpp ;
                                    $roundup       = round(round($hitung_margin)/100) * 100;
                                    $hargaawal     = $detail->price / $detail->qty;
                                    $marginpersen  = $margin_produk / 100;
                                    $suggest_price = ($hargaawal * $marginpersen) + $hargaawal;
                                    $suggest_total =  round($suggest_price,-2);
                                    $amount = $roundup - $hpp;
                                if ($produk_harga) {
                                    //cek kondisi change_price
                                    if ($produk_harga->change_price == 0) {
                                        // $margin_produk = isset($detail->produks->produksetting->margin) ? $detail->produks->produksetting->margin : 1;
                                        // $hpp           = $produk_harga->map;
                                        // $margin_persen = ($margin_produk * $hpp) / 100;
                                        // $hitung_margin = $margin_persen + $hpp ;
                                        // $roundup       = round(round($hitung_margin)/100) * 100;
                                        // $hargaawal     = $detail->price / $detail->qty;
                                        // $marginpersen  = $margin_produk / 100;
                                        // $suggest_price    = ($hargaawal * $marginpersen) + $hargaawal;

                                        // $amount = $roundup - $hpp;

                                        $produk_harga->fill([
                                            'cost_price'    => $hargaawal,
                                            'suggest_price' => $suggest_total,
                                            'change_price'  => 0,
                                            'margin_amount' => $roundup - $hpp,
                                            'map'           => $produk_harga->map,
                                            'gmd_price'     => $detail->price / $detail->qty,
                                            'created_at'    => date('Y-m-d H:i:s'),
                                            'created_by'    => auth()->user()->id,
                                        ]);
                                        $produk_harga->save();
                                    }else{
                                        $produk_harga->fill([
                                            'cost_price'    => $hargaawal,
                                        // 'suggest_price' => 0,
                                        // 'change_price'  => 0,
                                            'margin_amount' => $produk_harga->change_price - $hpp_map->map,
                                            'map'           => $hpp_map->map,
                                            'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
                                            'updated_at'    => date('Y-m-d H:i:s'),
                                            'updated_by'    => auth()->user()->id,
                                        ]);
                                        $produk_harga->save();
                                    }
                                }else{
                                    // $margin_produk = isset($detail->produks->produksetting->margin) ? $detail->produks->produksetting->margin : 1;
                                    // $hpp           = $detail->price / $detail->qty;
                                    // $margin_persen = ($margin_produk * $hpp) / 100;
                                    // $hitung_margin = $margin_persen + $hpp ;
                                    // $roundup       = round(round($hitung_margin)/100) * 100;
                                    // $hargaawal     = $detail->price / $detail->qty;
                                    // $marginpersen  = $margin_produk / 100;
                                    // $suggest_price = ($hargaawal * $marginpersen) + $hargaawal;
                                    // $suggest_total =  round($suggest_price);
                                    // $amount = $roundup - $hpp;

                                    $new_produk_harga = new ProdukHarga;
                                    $new_produk_harga->fill([
                                        'tmuk_kode'     => $pyr->tmuk_kode,
                                        'produk_kode'   => $detail->produk_kode,
                                        'cost_price'    => $hargaawal,
                                        'suggest_price' => $suggest_total,
                                        'change_price'  => 0,
                                        'margin_amount' => $roundup - $hpp,
                                        'map'           => $hargaawal,
                                        'gmd_price'     => $hargaawal,
                                        'created_at'    => date('Y-m-d H:i:s'),
                                        'created_by'    => auth()->user()->id,
                                    ]);
                                    $new_produk_harga->save();
                                }
                                //test

                            }
                            
                    }
                }
                
                //JURNAL
                $idtrans = SecTransaksi::generate();
                $th_fiskal = TahunFiskal::getTahunFiskal();
                $tanggal = $pembayaran2->pyr->tgl_buat;

                $tanggal_aset = date('Y-m-d');
                $last_day = date("Y-m-t", strtotime($tanggal));
                $deposit = '1.1.6.1'; //deposit
                $rekening = '1.1.2.1'; //escrow
                
                $tmuk_kode = $pembayaran2->tmuk_kode;
                $persediaan = $total_price + $biaya;
                $tipe = $pembayaran2->pyr->tipe;
                $nomor_pyr = $pembayaran2->pyr->nomor_pyr;

                //PYR OPENING
                if (substr($nomor_pyr, 0, -24) == 'OPENING') {
                    //insert hpp map jika produk tipe trade (001)
                    foreach ($pyr->detailPyr as $key => $detail) {
                        $tipe_barang_kode = $detail->produks->produksetting->tipe_barang_kode;
                        if ($tipe_barang_kode == '001') {
                            $conversi_uom2 = $detail->produks->produksetting->uom2_conversion;

                            $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $pyr->tmuk_kode)->get();

                            // $qty   = $detail->qty * $conversi_uom2;
                            $qty   = $detail->qty;
                            $price = $detail->price / $qty;
                            $total = $qty * $price;

                            if ($check->count() != 0) {
                                $jml = 0;
                                foreach ($check as $key => $data) {
                                    $jml += $data->qty * $data->price;
                                }
                                $new_map = ($jml + $total) / ($check->sum('qty') + $qty);

                                $hpp_map = new TransHppMap;
                                $hpp_map->fill([
                                    'tanggal'       => date('Y-m-d H:i:s'),
                                    'po_nomor'      => $pyr->nomor_pyr,
                                    'produk_kode'   => $detail->produk_kode,
                                    'tmuk_kode'     => $pyr->tmuk_kode,
                                    'qty'           => $qty,
                                    'price'         => $price,
                                    'map'           => $new_map,
                                    'created_at'    => date('Y-m-d H:i:s'),
                                    'created_by'    => auth()->user()->id,
                                ]);
                                $hpp_map->save();
                            }else{
                                $hpp_map = new TransHppMap;
                                $hpp_map->fill([
                                    'tanggal'       => date('Y-m-d H:i:s'),
                                    'po_nomor'      => $pyr->nomor_pyr,
                                    'produk_kode'   => $detail->produk_kode,
                                    'tmuk_kode'     => $pyr->tmuk_kode,
                                    'qty'           => $qty,
                                    'price'         => $price,
                                    'map'           => $price,
                                    'created_at'    => date('Y-m-d H:i:s'),
                                    'created_by'    => auth()->user()->id,
                                ]);
                                $hpp_map->save();
                            }
                        }
                    }

                    $check = TransAkuisisiAset::where('nomor_pyr', $nomor_pyr)->get();
                    if ($check) {
                        foreach ($check as $detail) {
                            $akuisisi = TransAkuisisiAset::find($detail->id);
                            $akuisisi->status = 1;
                            if($akuisisi->save()){
                                if (!is_null($akuisisi->tipe_id)) {
                                    $depresiasi = TipeAset::find($akuisisi->tipe_id);
                                    $depresiasi_x = $detail->nilai_pembelian / $depresiasi->tingkat_depresiasi;
                                    for($x = 0; $x < $depresiasi->tingkat_depresiasi; $x++){
                                        if($x == 0){
                                            TransJurnal::insert([
                                                [
                                                    'idtrans'         => $idtrans,
                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                    'tanggal'         => $tanggal,
                                                    'tmuk_kode'       => $tmuk_kode,
                                                    'referensi'       => $nomor_pyr,
                                                    'deskripsi'       => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                    'coa_kode'        => '6.3.3.0',
                                                    'jumlah'          => 0,
                                                    'posisi'          => 'D',
                                                    'flag'            => '+',
                                                ],
                                                [
                                                    'idtrans'         => $idtrans,
                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                    'tanggal'         => $tanggal,
                                                    'tmuk_kode'       => $tmuk_kode,
                                                    'referensi'       => $nomor_pyr,
                                                    'deskripsi'       => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                    'coa_kode'        => '1.2.7.0',
                                                    'jumlah'          => 0,
                                                    'posisi'          => 'K',
                                                    'flag'            => '-',
                                                ]
                                            ]);
                                        }else{
                                            $day_next = Carbon::createFromFormat('Y-m-d', $tanggal)->addMonths($x)->endOfMonth()->format('Y-m-d');
                                            // $day_next = Carbon::now()->addMonths($x)->endOfMonth()->format('Y-m-d');
                                            $nilai_depresiasi = $depresiasi_x * $x;
                                            $idtrans_next = SecTransaksi::generate();

                                            TransJurnal::insert([
                                                [
                                                    'idtrans' => $idtrans_next,
                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                    'tanggal' => $day_next.' 00:00:00',
                                                    'tmuk_kode' => $tmuk_kode,
                                                    'referensi' => $nomor_pyr,
                                                    'deskripsi' => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                    'coa_kode' => '6.3.3.0',
                                                    'jumlah' => $nilai_depresiasi,
                                                    'posisi' => 'D',
                                                    'flag' => '+',
                                                ],
                                                [
                                                    'idtrans' => $idtrans_next,
                                                    'tahun_fiskal_id' => $th_fiskal->id,
                                                    'tanggal' => $day_next.' 00:00:00',
                                                    'tmuk_kode' => $tmuk_kode,
                                                    'referensi' => $nomor_pyr,
                                                    'deskripsi' => $detail->produk_kode.' Depresiasi Bulan Ke-'.$x,
                                                    'coa_kode' => '1.2.7.0',
                                                    'jumlah' => $nilai_depresiasi,
                                                    'posisi' => 'K',
                                                    'flag' => '-',
                                                ]
                                            ]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if($tipe == '001'){
                    //Trade Lotte
                    TransJurnal::insert([
                        [
                            'idtrans' => $idtrans,
                            'tahun_fiskal_id' => $th_fiskal->id,
                            'tanggal' => $tanggal,
                            'tmuk_kode' => $tmuk_kode,
                            'referensi' => $nomor_pyr,
                            'deskripsi' => 'Pembayaran PyR - Trade Lotte',
                            'coa_kode' => $rekening, // escrow
                            'jumlah' => $total_price,
                            'posisi' => 'K',
                            'flag' => '-',
                        ],
                        [
                            'idtrans' => $idtrans,
                            'tahun_fiskal_id' => $th_fiskal->id,
                            'tanggal' => $tanggal,
                            'tmuk_kode' => $tmuk_kode,
                            'referensi' => $nomor_pyr,
                            'deskripsi' => 'Pembayaran PyR - Trade Lotte',
                            'coa_kode' => '2.1.1.0', //Utang Usaha
                            'jumlah' => $total_price,
                            'posisi' => 'D',
                            'flag' => '+',
                        ]
                    ]);
                }else if($tipe == '002'){
                    //Trade Non-Lotte
                    TransJurnal::insert([
                        [
                            'idtrans' => $idtrans,
                            'tahun_fiskal_id' => $th_fiskal->id,
                            'tanggal' => $tanggal,
                            'tmuk_kode' => $tmuk_kode,
                            'referensi' => $nomor_pyr,
                            'deskripsi' => 'Pembayaran PyR - Trade Non Lotte',
                            'coa_kode' => $rekening, // escrow
                            'jumlah' => $total_price,
                            'posisi' => 'K',
                            'flag' => '-',
                        ],
                        [
                            'idtrans' => $idtrans,
                            'tahun_fiskal_id' => $th_fiskal->id,
                            'tanggal' => $tanggal,
                            'tmuk_kode' => $tmuk_kode,
                            'referensi' => $nomor_pyr,
                            'deskripsi' => 'Pembayaran PyR - Trade Non Lotte',
                            'coa_kode' => '2.1.1.0', //Utang Usaha
                            'jumlah' => $total_price,
                            'posisi' => 'D',
                            'flag' => '+',
                        ]
                    ]);
                }else if($tipe == '003'){
                    //Non Trade Lotte
                    TransJurnal::insert([
                        [
                            'idtrans' => $idtrans,
                            'tahun_fiskal_id' => $th_fiskal->id,
                            'tanggal' => $tanggal,
                            'tmuk_kode' => $tmuk_kode,
                            'referensi' => $nomor_pyr,
                            'deskripsi' => 'Pembayaran PyR - Non Trade Lotte',
                            'coa_kode' => $rekening, // escrow
                            'jumlah' => $total_price,
                            'posisi' => 'K',
                            'flag' => '-',
                        ],
                        [
                            'idtrans' => $idtrans,
                            'tahun_fiskal_id' => $th_fiskal->id,
                            'tanggal' => $tanggal,
                            'tmuk_kode' => $tmuk_kode,
                            'referensi' => $nomor_pyr,
                            'deskripsi' => 'Pembayaran PyR - Non Trade Lotte',
                            'coa_kode' => '2.1.1.0', //Utang Usaha
                            'jumlah' => $total_price,
                            'posisi' => 'D',
                            'flag' => '+',
                        ]
                    ]);
                }else if($tipe == '004'){
                    //Non Trade Non Lotte
                    TransJurnal::insert([
                        [
                            'idtrans' => $idtrans,
                            'tahun_fiskal_id' => $th_fiskal->id,
                            'tanggal' => $tanggal,
                            'tmuk_kode' => $tmuk_kode,
                            'referensi' => $nomor_pyr,
                            'deskripsi' => 'Pembayaran PyR - Non Trade Non Lotte',
                            'coa_kode' => $rekening, // escrow
                            'jumlah' => $total_price,
                            'posisi' => 'K',
                            'flag' => '-',
                        ],
                        [
                            'idtrans' => $idtrans,
                            'tahun_fiskal_id' => $th_fiskal->id,
                            'tanggal' => $tanggal,
                            'tmuk_kode' => $tmuk_kode,
                            'referensi' => $nomor_pyr,
                            'deskripsi' => 'Pembayaran PyR - Non Trade Non Lotte',
                            'coa_kode' => '2.1.1.0', //Utang Usaha
                            'jumlah' => $total_price,
                            'posisi' => 'D',
                            'flag' => '+',
                        ]
                    ]);
                }

                //update id_transaksi di pyr
                $pyr->id_transaksi = $idtrans;
                $pyr->save();

                //Biaya Admin
                TransJurnal::insert([
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Biaya Admin',
                        'coa_kode' => '1.1.2.1', //escrow
                        'jumlah' => $biaya,
                        'posisi' => 'K',
                        'flag' => '-',
                    ],
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $nomor_pyr,
                        'deskripsi' => 'Biaya Admin',
                        'coa_kode' => '8.3.0.0', //Biaya Admin Bank
                        'jumlah' => $biaya,
                        'posisi' => 'D',
                        'flag' => '+',
                    ]
                ]);
            }
        }

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Pembayaran PyR',
            'ref'               => '',
            'aksi'              => 'Disetujui',
            'amount'            => $total_price,
        ]);

        return response([
            'status' => true,
            'data'  => $pembayaran
        ]);
    }

    public function prints($id)
    {

        $pembayaranpyr = TransPyrPembayaran::find($id);

        if($pembayaranpyr->pyr->vendor_lokal == 0){
            $tmuk = Tmuk::with('lsi')->where('kode', $pembayaranpyr->pyr->tmuk->kode)->first();
            // return $tmuk->lsi->nama;
            $data = [
                'record'           => $pembayaranpyr,
                'nama_pemilik'     => $tmuk->rekeningescrow->nama_pemilik,
                'no_rek_asal'      => $tmuk->rekeningescrow->nomor_rekening,
                'nama_penerima'    => $tmuk->lsi->pajak->nama,
                'nama_bank_tujuan' => $tmuk->bankescrow->nama,
                'no_rek_tujuan'    => $tmuk->lsi->rekeningescrow->nomor_rekening,
            ];
        }else if($pembayaranpyr->pyr->vendor_lokal == 100){
            $tmuk = Tmuk::with('lsi')->where('kode', $pembayaranpyr->pyr->tmuk->kode)->first();
            // return $tmuk->lsi->nama;
            $data = [
                'record'           => $pembayaranpyr,
                'nama_penerima'    => $tmuk->rekeningpemilik->nama_pemilik,
                'no_rek_tujuan'    => $tmuk->rekeningpemilik->nomor_rekening,
                'nama_bank_tujuan' => $tmuk->bankpemilik->nama,
            ];
        }else{
            // $vendor = VendorLokalTmuk::where('id', $pembayaranpyr->pyr->vendor_lokal)->first();
            // // return isset($vendor) ? $vendor->nama : '-';
            // $data = [
            //     'record'           => $pembayaranpyr,
            //     'nama_pemilik'     => $vendor->rekeningescrow->nama_pemilik,
            //     'no_rek_asal'      => $vendor->rekeningescrow->nomor_rekening,
            //     'nama_penerima'    => $vendor->rekeningescrow->nama_pemilik,
            //     'nama_bank_tujuan' => $vendor->bankescrow->nama,
            //     'no_rek_tujuan'    => $vendor->rekeningescrow->nomor_rekening,
            // ];
         
            $tmuk_vendor = DetailVendorLokalTmuk::where('vendor_tmuk_id', $pembayaranpyr->pyr->vendor_lokal)->first();
            // dd($data_tmuk->rekeningescrow->nama_pemilik);
            $data = [
                'record'           => $pembayaranpyr,
                'nama_penerima'    => $tmuk_vendor->vendor->rekeningescrow->nama_pemilik,
                'no_rek_tujuan'    => $tmuk_vendor->vendor->rekeningescrow->nomor_rekening,
                'nama_bank_tujuan' => $tmuk_vendor->vendor->rekeningescrow->bankescrow->nama,
            ];
        }


        $content = view('report.pembayaran-pyr', $data);

        $data = [
            'oriented'   => 'L',
            'paper'      => 'A4',
            'label_file' => 'Pembayaran PYR '.$pembayaranpyr->nomor_pyr,
        ];

        HTML2PDF($content, $data);
    }

}
