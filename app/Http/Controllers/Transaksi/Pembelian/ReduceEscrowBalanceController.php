<?php

namespace Lotte\Http\Controllers\Transaksi\Pembelian;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
//Libraries;
use DB;
use Datatables;
use Carbon\Carbon;
use Lotte\Libraries\ApiGmd;
use Excel;

//Models
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransKontainer;
use Lotte\Models\Trans\TransKontainerDetail;
use Lotte\Models\Trans\TransTruk;
use Lotte\Models\Trans\TransMuatan;
use Lotte\Models\Trans\TransMuatanDetail;
use Lotte\Models\Trans\TransSuratJalan;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransReduceEscrowDetail;
use Lotte\Models\Trans\TransHppMap;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Kontainer;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\ProdukHarga;
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\SecTransaksi;
use Lotte\Models\Trans\TransLogAudit;

class ReduceEscrowBalanceController extends Controller
{
    protected $link = 'transaksi/pembelian/reduce-escrow-balance/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Reduce Escrow Balance");
        $this->setSubtitle("&nbsp;");
        $this->setModalSize("fullscreen");
        // $this->setModalSize("large");
        $this->setBreadcrumb(['Transaksi' => '#', 'Pembelian (PO)' => '#', 'Reduce Escrow Balance' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => 'No',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'tanggal',
                'name' => 'tanggal',
                'label' => 'Tanggal PO',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'po_nomor',
                'name' => 'po_nomor',
                'label' => 'Nomor PO',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'tmuk',
                'name' => 'tmuk',
                'label' => 'TMUK',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'po_btdk_rp',
                'name' => 'po_btdk_rp',
                'label' => 'PO-BTDK (Rp)',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'nomor_struk',
                'name' => 'nomor_struk',
                'label' => 'Nomor Struk',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'saldo_dipotong',
                'name' => 'saldo_dipotong',
                'label' => 'Saldo Dipotong (Rp)',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'awal_saldo_deposit',
                'name' => 'awal_saldo_deposit',
                'label' => 'Awal (Rp) deposit',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'akhir_saldo_deposit',
                'name' => 'akhir_saldo_deposit',
                'label' => 'Akhir (Rp) deposit',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'awal_saldo_escrow',
                'name' => 'awal_saldo_escrow',
                'label' => 'Awal (Rp)escrow',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'akhir_saldo_escrow',
                'name' => 'akhir_saldo_escrow',
                'label' => 'Akhir (Rp)escrow',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'verifikasi1_date',
                'name' => 'verifikasi1_date',
                'label' => 'Persetujuan 1',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'verifikasi2_date',
                'name' => 'verifikasi2_date',
                'label' => 'Persetujuan 2',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'verifikasi3_date',
                'name' => 'verifikasi3_date',
                'label' => 'Persetujuan 3',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'verifikasi4_date',
                'name' => 'verifikasi4_date',
                'label' => 'Persetujuan 4',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            // [
            //     'data' => 'ceklist',
            //     'name' => 'ceklist',
            //     'label' => 'Setujui',
            //     'orderable' => false,
            //     'searchable' => false,
            //     'className' => "center aligned",
            //     'width' => '40px',
            //     'className' => "center aligned",
            // ]
        ]);

}

public function grid(Request $request)
{
    $records = TransReduceEscrow::with('creator')->whereNull('verifikasi4_date')->orderBy('tanggal', 'DESC')
    ->select('*');
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
    if($tanggal = $request->tanggal){
        $dates = \Carbon\Carbon::parse($tanggal)->format('Y-m-d');
        $records->where(\DB::raw('DATE(tanggal)'),$dates);
    }

    if ($po_nomor = $request->po_nomor) {
        $records->where('po_nomor', 'like', '%' . $po_nomor . '%');
    }
    if ($tmuk_kode = $request->tmuk_kode) {
        $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
        return $request->get('start');
    })
    ->addColumn('nomor', function ($record) {
        $string = '';
        $string = '<a href="" class="ui mini orange icon detail" data-content="Print PO BTDK" data-id="'.$record->id.'">'.$record->po_nomor.'</a>';
                    // $string = '<button type="button" class="ui mini orange icon detail button" data-content="Detail Struk" data-id="'.$record->id.'">Detail Struk</button>';
        return $string;
    })
    ->addColumn('po_nomor', function ($record) {
        $string = '';
        $string = '<button type="button" class="circular ui icon button detail-po" data-content="Detail PO" data-id="'.$record->id.'">'.$record->po_nomor.'</button>';
        return $string;
    })
    ->addColumn('tmuk', function ($record) {
        return $record->tmuk->kode.' - '.$record->tmuk->nama;
    })
    ->addColumn('po_btdk_rp', function ($record) {
        return FormatNumber($record->transpo->total);
    })
    ->addColumn('nomor_struk', function ($record) {
        $string = '';
        if ($record->verifikasi1_date == '') {
            if($record->detailreduce){
                $string = '<button type="button" class="ui mini blue icon edit button" data-content="Edit Struk" data-id="'.$record->id.'">Edit Struk</button>';
            }else{
                $string = '<button type="button" class="ui mini blue icon edit button" data-content="Tambah Struk" data-id="'.$record->id.'">Entri Struk</button>';
            }
        } else{
            $string = '<button type="button" class="ui mini orange icon detail button" data-content="Detail Struk" data-id="'.$record->id.'">Detail Struk</button>';
        }
        return $string;
    })
    ->addColumn('saldo_dipotong', function ($record) {
        $string = isset($record->detailreduce) ? $record->detailreduce->sum('total_belanja') : '0';
        return FormatNumber($string);
    })
    ->addColumn('awal_saldo_deposit', function ($record) {
        $string = (!is_null($record->saldo_awal_deposit)) ? $record->saldo_awal_deposit : $record->tmuk->saldo_deposit;
        return FormatNumber($string);
    })
    ->addColumn('akhir_saldo_deposit', function ($record) {
        $string = (!is_null($record->saldo_deposit)) ? $record->saldo_deposit : '0';
        return FormatNumber($string);
    })
    ->addColumn('awal_saldo_escrow', function ($record) {
        $string = (!is_null($record->saldo_awal_escrow)) ? $record->saldo_awal_escrow : $record->tmuk->saldo_escrow;
        return FormatNumber($string);
    })
    ->addColumn('akhir_saldo_escrow', function ($record) {
        $string = (!is_null($record->saldo_escrow)) ? $record->saldo_escrow : '0';
        return FormatNumber($string);

                // $salpot = isset($record->detailreduce) ? $record->detailreduce->sum('total_belanja') : '0';
                // $salmuk = (!is_null($record->saldo_escrow)) ? $record->saldo_escrow : $record->tmuk->saldo_escrow;
                // $akhir = $salmuk - $salpot;
                // return ($akhir <= 0) ? '<button class="ui negative basic button" data-content="Saldo Tidak Mencukupi, Silahkan Topup!">Saldo Tidak Cukup</button>' : FormatCurrency($akhir);

    })
    ->addColumn('created_at', function ($record) {
        return $record->created_at->diffForHumans();
    })
    ->addColumn('verifikasi1_date', function ($record) {
        $disabled = 'disabled';
        $string = '';
        if ($record->verifikasi1_date == '') {
            if($record->detailreduce->sum('total_belanja')){
                $disabled = '';
            }
            if (auth()->user()->hasRole('cso-persetujuan1')) {
                $disabled = '';
            } else {
                $disabled = 'disabled';
            }
            $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-1" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
        } else {
                    // $string = '<button type="button" class="ui toggle button active" data-content="Tervalidasi">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">'.$record->verifikasi1_date.'</i>';
            $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi1_date.'</i>';
        }
        return $string;
    })
    ->addColumn('verifikasi2_date', function ($record) {
        $string = '';
        $disabled = 'disabled';
        if ($record->verifikasi2_date == '') {
            if($record->verifikasi1_date){
                if (auth()->user()->hasRole('manager-cso-persetujuan2')) {
                    $disabled = '';
                }else{
                    $disabled = 'disabled';
                }
            }
            $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-2" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
        } else{
                    // $string = '<button type="button" class="ui toggle button active" data-content="Tervalidasi">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">'.$record->verifikasi2_date.'</i>';
            $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi2_date.'</i>';
        }
        return $string;
    })
    ->addColumn('verifikasi3_date', function ($record) {
        $string = '';
        $disabled = 'disabled';
        if ($record->verifikasi3_date == '') {
            if($record->verifikasi2_date){
                if (auth()->user()->hasRole('director-cso-persetujuan3')) {
                    $disabled = '';
                }else{
                    $disabled='disabled';
                }
            }
            $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-3" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
        } else{
                    // $string = '<button type="button" class="ui toggle button active" data-content="Tervalidasi">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">'.$record->verifikasi3_date.'</i>';
            $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi3_date.'</i>';
        }
        return $string;
    })
    ->addColumn('verifikasi4_date', function ($record) {
        $string = '';
        $disabled = 'disabled';
        if ($record->statush2h == 1) {
             $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b>';
        } else{
            if ($record->verifikasi4_date == '') {
                if($record->verifikasi3_date){
                    if (auth()->user()->hasRole('finance-tmuk-persetujuan4')) {
                        $disabled = '';
                    }else{
                        $disabled='disabled';
                    }
                }
                $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-4" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
            } else{
                        // $string = '<button type="button" class="ui toggle button active" data-content="Tervalidasi">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">'.$record->verifikasi4_date.'</i>';
                $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi4_date.'</i>';
            }
        }
        return $string;
    })
    ->addColumn('keterangan', function ($record) {
        $string = '';
        if ($record->statush2h == 1) {
            $string = '<i style="font-size: 12px; color: orange;" data-content="Sedang Proses">Proses H2H</i>';
        }elseif ($record->statush2h == 4){
            $string = '<b style="font-size: 15px; color: red;" data-content="Gagal">Gagal</b><br><i style="font-size: 12px; color: red;" data-content="Sudah Tervalidasi">'.$record->keterangan.'</i>';
        }
        return $string;

    })
    ->addColumn('ceklist', function ($record) {
        $string = '<input type="checkbox" class="select pyr">';
        return $string;
    })
    ->addColumn('action', function ($record) {
        $string = '';
        $disabled = 'disabled';
        $string = '<a target="_blank" href="reduce-escrow-balance/print/'.$record->id.'" class="ui mini default icon print button '.($record->verifikasi4_date?'':$disabled).'" data-content="Print Reduce Escrow"><i class="print icon"></i> Print</a>';
        return $string;
    })
    ->make(true);
}

public function gridHistori(Request $request)
{
    $records = TransReduceEscrow::with('creator')->whereNotNull('verifikasi4_date')->orderBy('verifikasi4_date', 'DESC')
    ->select('*');
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
    if($tanggal = $request->tanggal){
        $dates = \Carbon\Carbon::parse($tanggal)->format('Y-m-d');
        $records->where(\DB::raw('DATE(tanggal)'),$dates);
    }

    if ($po_nomor = $request->po_nomor) {
        $records->where('po_nomor', 'like', '%' . $po_nomor . '%');
    }
    if ($tmuk_kode = $request->tmuk_kode) {
        $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
        return $request->get('start');
    })
    ->addColumn('nomor', function ($record) {
        $string = '';
        $string = '<a href="" class="ui mini orange icon detail" data-content="Print PO BTDK" data-id="'.$record->id.'">'.$record->po_nomor.'</a>';
                    // $string = '<button type="button" class="ui mini orange icon detail button" data-content="Detail Struk" data-id="'.$record->id.'">Detail Struk</button>';
        return $string;
    })
    ->addColumn('po_nomor', function ($record) {
        $string = '';
        $string = '<button type="button" class="circular ui icon button detail-po" data-content="Detail PO" data-id="'.$record->id.'">'.$record->po_nomor.'</button>';
        return $string;
    })
    ->addColumn('tmuk', function ($record) {
        return $record->tmuk->kode.' - '.$record->tmuk->nama;
    })
    ->addColumn('po_btdk_rp', function ($record) {
        return FormatNumber($record->transpo->total);
    })
    ->addColumn('nomor_struk', function ($record) {
        $string = '';
        if ($record->verifikasi1_date == '') {
            if($record->detailreduce){
                $string = '<button type="button" class="ui mini blue icon edit button" data-content="Edit Struk" data-id="'.$record->id.'">Edit Struk</button>';
            }else{
                $string = '<button type="button" class="ui mini blue icon edit button" data-content="Tambah Struk" data-id="'.$record->id.'">Entri Struk</button>';
            }
        } else{
            $string = '<button type="button" class="ui mini orange icon detail button" data-content="Detail Struk" data-id="'.$record->id.'">Detail Struk</button>';
        }
        return $string;
    })
    ->addColumn('saldo_dipotong', function ($record) {
        $string = isset($record->detailreduce) ? $record->detailreduce->sum('total_belanja') : '0';
        return FormatNumber($string);
    })
    ->addColumn('awal_saldo_deposit', function ($record) {
        $string = (!is_null($record->saldo_awal_deposit)) ? $record->saldo_awal_deposit : $record->tmuk->saldo_deposit;
        return FormatNumber($string);
    })
    ->addColumn('akhir_saldo_deposit', function ($record) {
        $string = (!is_null($record->saldo_deposit)) ? $record->saldo_deposit : '0';
        return FormatNumber($string);
    })
    ->addColumn('awal_saldo_escrow', function ($record) {
        $string = (!is_null($record->saldo_awal_escrow)) ? $record->saldo_awal_escrow : $record->tmuk->saldo_escrow;
        return FormatNumber($string);
    })
    ->addColumn('akhir_saldo_escrow', function ($record) {
        $string = (!is_null($record->saldo_escrow)) ? $record->saldo_escrow : '0';
        return FormatNumber($string);

                // $salpot = isset($record->detailreduce) ? $record->detailreduce->sum('total_belanja') : '0';
                // $salmuk = (!is_null($record->saldo_escrow)) ? $record->saldo_escrow : $record->tmuk->saldo_escrow;
                // $akhir = $salmuk - $salpot;
                // return ($akhir <= 0) ? '<button class="ui negative basic button" data-content="Saldo Tidak Mencukupi, Silahkan Topup!">Saldo Tidak Cukup</button>' : FormatCurrency($akhir);

    })
    ->addColumn('created_at', function ($record) {
        return $record->created_at->diffForHumans();
    })
    ->addColumn('verifikasi1_date', function ($record) {
        $disabled = 'disabled';
        $string = '';
        if ($record->verifikasi1_date == '') {
            if($record->detailreduce->sum('total_belanja')){
                $disabled = '';
            }
            if (auth()->user()->hasRole('cso-persetujuan1')) {
                $disabled = '';
            } else {
                $disabled = 'disabled';
            }
            $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-1" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
        } else {
                    // $string = '<button type="button" class="ui toggle button active" data-content="Tervalidasi">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">'.$record->verifikasi1_date.'</i>';
            $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi1_date.'</i>';
        }
        return $string;
    })
    ->addColumn('verifikasi2_date', function ($record) {
        $string = '';
        $disabled = 'disabled';
        if ($record->verifikasi2_date == '') {
            if($record->verifikasi1_date){
                if (auth()->user()->hasRole('manager-cso-persetujuan2')) {
                    $disabled = '';
                }else{
                    $disabled = 'disabled';
                }
            }
            $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-2" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
        } else{
                    // $string = '<button type="button" class="ui toggle button active" data-content="Tervalidasi">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">'.$record->verifikasi2_date.'</i>';
            $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi2_date.'</i>';
        }
        return $string;
    })
    ->addColumn('verifikasi3_date', function ($record) {
        $string = '';
        $disabled = 'disabled';
        if ($record->verifikasi3_date == '') {
            if($record->verifikasi2_date){
                if (auth()->user()->hasRole('director-cso-persetujuan3')) {
                    $disabled = '';
                }else{
                    $disabled='disabled';
                }
            }
            $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-3" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
        } else{
                    // $string = '<button type="button" class="ui toggle button active" data-content="Tervalidasi">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">'.$record->verifikasi3_date.'</i>';
            $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi3_date.'</i>';
        }
        return $string;
    })
    ->addColumn('verifikasi4_date', function ($record) {
        $string = '';
        $disabled = 'disabled';
        if ($record->verifikasi4_date == '') {
            if($record->verifikasi3_date){
                if (auth()->user()->hasRole('finance-tmuk-persetujuan4')) {
                    $disabled = '';
                }else{
                    $disabled='disabled';
                }
            }
            $string = '<button type="button" class="ui circular mini red icon toggle button" id="btn-approval-4" data-content="Validasikan" data-id="'.$record->id.'" '.$disabled.'>Validasi</button>';
        } else{
                    // $string = '<button type="button" class="ui toggle button active" data-content="Tervalidasi">Tervalidasi</button><br><i style="font-size: 12px; color: blue;">'.$record->verifikasi4_date.'</i>';
            $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Tervalidasi</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tervalidasi">'.$record->verifikasi4_date.'</i>';
        }
        return $string;
    })
    ->addColumn('action', function ($record) {
        $string = '';
        $disabled = 'disabled';
        if ($record->keterangan != '') {
            if ($record->statush2h == 3) {
                $string = '<a target="_blank" href="reduce-escrow-balance/print/'.$record->id.'" class="ui mini default icon print button '.($record->verifikasi4_date?'':$disabled).'" data-content="Print Reduce Escrow"><i class="print icon"></i> Print</a>';
            }
        }else{
            $string = '<a target="_blank" href="reduce-escrow-balance/print/'.$record->id.'" class="ui mini default icon print button '.($record->verifikasi4_date?'':$disabled).'" data-content="Print Reduce Escrow"><i class="print icon"></i> Print</a>';
        }
        return $string;
    })
    ->addColumn('ceklist', function ($record) {
        $string = '<input type="checkbox" class="select pyr">';
        return $string;
    })
    ->make(true);
}

public function index()
{
    return $this->render('modules.transaksi.pembelian.reduce-escrow-balance.index', ['mockup' => false]);
}

public function onChangePopStruk($id_escrow=null,$nomor=null)
{
        //get data by po nomor
    $escrow = TransReduceEscrow::find($id_escrow);
        // get data from GMD
        $response = ApiGmd::getStruk($nomor); // edited

        // ngetes struk di lokal
        // $arr = array('data' => [
        //       [
        //          'PROD_CD' => '1002929000',
        //          'PROD_NM' => 'GOOD DAY KOPI CHOC ORANGE 10X30 G',
        //          'SALE_QTY' => '10',
        //          'SALE_AMT' => '16950',
        //          'TOT_PRICE' => '169500',
        //          'DC_AMT' => '0'
        //       ]
        //       ,[
        //          'PROD_CD' => '1019605000',
        //          'PROD_NM' => 'NESTLE KOKO KRUNCH COMBO 4X(20+12) G',
        //          'SALE_QTY' => '10',
        //          'SALE_AMT' => '26300',
        //          'TOT_PRICE' => '78900',
        //          'DC_AMT' => '0'
        //       ]
        //       ,[
        //         'PROD_CD' => '1065039000',
        //         'PROD_NM' => 'ICHI OCHA GREEN TEA 500ML',
        //         'SALE_QTY' => '5',
        //         'SALE_AMT' => '4900',
        //         'TOT_PRICE' => '24500',
        //         'DC_AMT' => '0'
        //       ]
        //    ],'status' => true);
        // $response = json_encode($arr);
        
        // cek is json // ke deui
        $json = json_decode($response, true);

        $total = 0;
        $error = [];
        if ($json['status'] == true) { // jika terdapat info dari gmd
            foreach ($json['data'] as $val) {
                //cek sesuai po 
                $check = $escrow->transpo->detail->where('produk_kode', $val['PROD_CD'])->first();

                if ($check) {
                    $total += (float) $val['TOT_PRICE'];
                }else{
                    $error[$val['PROD_CD']] = 'Barang '.$val['PROD_CD'].' - '.$val['PROD_NM'].' tidak terdaftar di system.';
                }
            }
        }else{
            return response()->json([
                'status' => ['Nomor struk Tidak ditemukan.'],
            ],402);
        }

        if ($error) {
            //create log 
            $record = $error;
            $filename = $nomor.date("YmdHis");
            $errorlog = Excel::create($filename, function($excel) use ($record, $nomor){
                $excel->setTitle('Error Log Struk');
                $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
                $excel->setDescription('Error Log Struk');

                $excel->sheet('Error Log Struk ', function($sheet) use ($record, $nomor){
                    $sheet->row(1, array(
                        'Error Log Struk No.'.$nomor,
                    ));

                    $row = 2;
                    foreach ($record as $val) {
                        $sheet->row($row, array(
                            $val,
                        ));
                        $row=$row;
                        $row++;
                    }
                    $sheet->setWidth(array('A'=>80));
                    $sheet->cells('A1', function($cells){
                        $cells->setAlignment('center');
                        $cells->setBackground('#999999');
                        $cells->setFontColor('#FFFFFF');
                    });

                });
            })->store('xls', storage_path('uploads/log-struk'));

            return response()->json([
                'status' => $error,
                'total' => $total,
                'log' => '<a href="'.asset('storage/uploads/log-struk/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
            ],402);
        }else{
            return response()->json([
                'total' => $total,
            ]);
        }

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $no_struk = (isset($data['escrow_nomor_struk'])) ? array_filter($data['escrow_nomor_struk']) : [];

        if (empty($no_struk)) {
            return response([
                'status' => 'Nomor struk dan total belanja tidak boleh kosong',
            ],402);
        }else{
            $total_belanja = (isset($data['escrow_total_belanja'])) ? array_filter($data['escrow_total_belanja']) : [];
            if (empty($total_belanja)) {
                return response([
                    'status' => 'Nomor struk dan total belanja tidak boleh kosong',
                ],402);
            }

            $TransReduceEscrow = TransReduceEscrow::find($id);
            $TransReduceEscrow->updated_at = date('Y-m-d H:i:s');
            $TransReduceEscrow->updated_by = auth()->user()->id;
            $TransReduceEscrow->save();

            foreach ($no_struk as $key => $value) {
                if (empty($data['escrow_detail_id'][$key])) {
                    $data_escrow = new TransReduceEscrowDetail;
                    $data_escrow->reduce_escrow_id = $TransReduceEscrow->id;
                    $data_escrow->nomor_struk = $data['escrow_nomor_struk'][$key];
                    $data_escrow->total_belanja = decimal_for_save($data['escrow_total_belanja'][$key]);
                    $data_escrow->save();
                }else{
                    //delete
                    TransReduceEscrowDetail::where('reduce_escrow_id', $TransReduceEscrow->id)->whereNotIn('id', $request->escrow_detail_id)->delete();
                    
                    $data_escrow = TransReduceEscrowDetail::find($data['escrow_detail_id'][$key]);

                    if (!is_null($data_escrow)) {
                        $data_escrow->nomor_struk = $data['escrow_nomor_struk'][$key];
                        $data_escrow->total_belanja = decimal_for_save($data['escrow_total_belanja'][$key]);
                        $data_escrow->save();
                    }
                }
            }

            // insert to log audit
            TransLogAudit::setLog([
                'tanggal_transaksi' => date('Y-m-d'),
                'type'              => 'Reduce Escrow Balance ',
                'ref'               => '',
                'aksi'              => 'Entri Struk',
                'amount'            => $data_escrow->total_belanja,
            ]);

            return response([
                'status' => true,
                'data'  => $data_escrow
            ]);
        }

    }

    //Tolak
    public function onChangePopTolak($id)
    {
        $approval1 = TransReduceEscrow::find($id);
        $approval1->verifikasi1_date = date('Y-m-d h:i:s');
        $approval1->verifikasi1_user = auth()->user()->id;
        $approval1->save();
        
        return response([
            'status' => true,
            'data'  => $approval1
        ]);
    }

    //Approval 1
    public function onChangePopApproval1($id)
    {
        $approval1 = TransReduceEscrow::find($id);
        $approval1->verifikasi1_date = date('Y-m-d h:i:s');
        $approval1->verifikasi1_user = auth()->user()->id;
        $approval1->save();
        
        return response([
            'status' => true,
            'data'  => $approval1
        ]);
    }

    //Approval 2
    public function onChangePopApproval2($id)
    {
        $approval2 = TransReduceEscrow::find($id);
        $approval2->verifikasi2_date = date('Y-m-d h:i:s');
        $approval2->verifikasi2_user = auth()->user()->id;
        $approval2->save();
        
        return response([
            'status' => true,
            'data'  => $approval2
        ]);
    }

    //Approval 3
    public function onChangePopApproval3($id)
    {
        $approval3 = TransReduceEscrow::find($id);
        $approval3->verifikasi3_date = date('Y-m-d h:i:s');
        $approval3->verifikasi3_user = auth()->user()->id;
        $approval3->save();
        
        return response([
            'status' => true,   
            'data'  => $approval3
        ]);
    }

    //Approval 4
    public function onChangePopApproval4($id)
    {
        $cekh2h = TransReduceEscrow::find($id);
        // dd($cekh2h);
        if ($cekh2h->tmuk_kode == '0600100047') {
        // if ($cekh2h->po_nomor == 'PO-20110115060010004700001') {
            //h2h
            $approval4 = TransReduceEscrow::find($id);
            // dd((str)$approval4->po_nomor);
                $filename = $approval4->po_nomor . date('YmdHis');
                \Lotte\Libraries\H2H::createCsv([
                    'SequenceNumber'       => $approval4->po_nomor,
                    'CustRefNo'            => $filename,
                    'ValueDate'            => date('YmdHi'),
                    'Amount'               => (int)$approval4->detailreduce->sum('total_belanja'),
                    'TrxRemark'            => $approval4->po_nomor,
                    'RateVoucherCode'      => '',
                    'ChargeType'           => 'OUR',
                    'DebitAccount'         => $approval4->tmuk->rekeningescrow->nomor_rekening,
                    'SenderName'           => $approval4->tmuk->rekeningpemilik->nama_pemilik,
                    'SenderAddress'        => $approval4->tmuk->rekeningpemilik->bankescrow->alamat,
                    'SenderCountryCode'    => 'ID',
                    'CreditAccount'        => '052201000291300',
                    'BeneficiaryName'      => 'PT LOTTE SHOPPING INDONESIA',
                    'BeneficiaryAddress'   => $approval4->tmuk->bankescrow->kota->nama,
                    'BenBankIdentifier'    => '002',
                    'BenBankName'          => 'BRI',
                    'BenBankAddress'       => $approval4->tmuk->bankescrow->kota->nama,
                    'BenBankCountryCode'   => 'ID',
                    'InterBankIdentifier'  => '',
                    'InterBankName'        => '',
                    'InterBankAddress'     => '',
                    'InterBankCountryCode' => '',
                    'Notification'         => 'dina.murdaningrum@lottemart.co.id',
                    'BeneficiaryCategory'  => 'A0',
                    'BeneficiaryRelation'  => 'N',
                    'BITransaction Code'   => '299',
                    'TemplateCode'         => '1',
                ]);

                $approval4->verifikasi4_date = date('Y-m-d h:i:s');
                $approval4->verifikasi4_user = auth()->user()->id;

                // 0:awal, 1:proses upload, 2:proses bank, 3:sukses, 4:gagal
                $approval4->statush2h = 1;
                $approval4->keterangan = 'Proses Upload H2H';
                $approval4->nomorh2h = $filename;
                $approval4->save();

                return response([
                    'status' => true,
                    'data'  => $approval4
                ]);
            //h2h
        } else {
            //reduce escrow
               $approval4 = TransReduceEscrow::find($id);
                $approval4->verifikasi4_date = date('Y-m-d h:i:s');
                $approval4->verifikasi4_user = auth()->user()->id;
                $approval4->save();

                //update saldo trans_reduce_escrow_detail
                $detail = TransReduceEscrowDetail::where('reduce_escrow_id', $approval4->id)->get(); 
                $tonja = $detail->sum('total_belanja');

                //skema pemotongan di mulai dari deposit, scn, escrow
                $tmuk_saldo = Tmuk::where('kode', $approval4->tmuk_kode)->first();
                $awal_saldo_deposit = $tmuk_saldo->saldo_deposit;
                $awal_saldo_escrow = $tmuk_saldo->saldo_escrow;

                $saldo_deposit = $awal_saldo_deposit - $tonja;
                if ($saldo_deposit < 0) {
                    $saldo_escrow = $awal_saldo_escrow + $saldo_deposit;
                    
                    $tmuk_saldo->saldo_deposit = 0;
                    $tmuk_saldo->saldo_escrow = $saldo_escrow;
                }else{
                    $tmuk_saldo->saldo_deposit = $saldo_deposit;
                }

                if (isset($approval4->transpo->nomor_pr)) {
                    $no_po = substr($approval4->transpo->nomor_pr, -4);
                    if ($no_po == '0000') {
                        $tmuk_saldo->nomor_transaksi = $approval4->po_nomor;
                        $tmuk_saldo->flag_transaksi  = 1;
                    }
                }
                
                $tmuk_saldo->save();

                //update saldo dipotong trans_reduce_escrow
                $detail_escrow = TransReduceEscrow::where('id', $approval4->id)->first();
                $detail_escrow->saldo_dipotong       = $tonja;
                $detail_escrow->saldo_awal_deposit   = $awal_saldo_deposit;
                $detail_escrow->saldo_awal_escrow    = $awal_saldo_escrow;
                $detail_escrow->saldo_deposit        = $tmuk_saldo->saldo_deposit;
                $detail_escrow->saldo_escrow         = $tmuk_saldo->saldo_escrow;
                if($detail_escrow->save()){
                    //update status 2 di trnas_po
                    $po = TransPo::where('nomor', $approval4->po_nomor)->first();
                    $po->status = 2;
                    if($po->save()){
                        foreach ($po->detail as $key => $detail) {
                            //check qty_po != 0
                            if ($detail->qty_po != 0) {
                                //insert hpp map
                                $conversi_uom2 = isset($detail->produk->produksetting->uom2_conversion) ? $detail->produk->produksetting->uom2_conversion : 1;

                                $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $po->tmuk_kode)->get();

                                $po_qty = $detail->qty_po * $conversi_uom2; //ngambil hasil konversi
                                $po_price = $detail->price / $po_qty;
                                $total = $po_qty * $po_price;

                                if ($check->count() != 0) {
                                    $jml = 0;
                                    foreach ($check as $key => $data) {
                                        $jml += $data->qty * $data->price;
                                    }
                                    $new_map = ($jml + $total) / ($check->sum('qty') + $po_qty);

                                    $hpp_map = new TransHppMap;
                                    $hpp_map->fill([
                                        'tanggal'       => date('Y-m-d H:i:s'),
                                        'po_nomor'      => $detail->po_nomor,
                                        'produk_kode'   => $detail->produk_kode,
                                        'tmuk_kode'     => $po->tmuk_kode,
                                        'qty'           => $po_qty,
                                        'price'         => $po_price,
                                        'map'           => $new_map,
                                        'created_at'    => date('Y-m-d H:i:s'),
                                        'created_by'    => auth()->user()->id,
                                    ]);
                                    $hpp_map->save();
                                }else{
                                    $hpp_map = new TransHppMap;
                                    $hpp_map->fill([
                                        'tanggal'       => date('Y-m-d H:i:s'),
                                        'po_nomor'      => $detail->po_nomor,
                                        'produk_kode'   => $detail->produk_kode,
                                        'tmuk_kode'     => $po->tmuk_kode,
                                        'qty'           => $po_qty,
                                        'price'         => $po_price,
                                        'map'           => $po_price,
                                        'created_at'    => date('Y-m-d H:i:s'),
                                        'created_by'    => auth()->user()->id,
                                    ]);
                                    $hpp_map->save();
                                }

                                //insert ProdukHarga
                                $produk_harga = ProdukHarga::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $po->tmuk_kode)->first();

                                if ($produk_harga) {
                                    //cek kondisi change_price
                                    if ($produk_harga->change_price == 0) {
                                        $margin_produk = isset($detail->produk->produksetting->margin) ? $detail->produk->produksetting->margin : 1;
                                        $hpp           = $hpp_map->map;
                                        $margin_persen = ($margin_produk * $hpp) / 100;
                                        $hitung_margin = $margin_persen + $hpp ;
                                        $roundup       = round(round($hitung_margin)/100) * 100;

                                        $amount = $roundup - $hpp;

                                        $produk_harga->fill([
                                            'cost_price'    => $hpp_map->price,
                                            'suggest_price' => $roundup,
                                            'change_price'  => 0,
                                            'margin_amount' => $roundup - $hpp,
                                            'map'           => $hpp_map->map,
                                            'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
                                            'created_at'    => date('Y-m-d H:i:s'),
                                            'created_by'    => auth()->user()->id,
                                        ]);
                                        $produk_harga->save();
                                    }else{
                                        $produk_harga->fill([
                                            'cost_price'    => $hpp_map->price,
                                        // 'suggest_price' => 0,
                                        // 'change_price'  => 0,
                                            'margin_amount' => $produk_harga->change_price - $hpp_map->map,
                                            'map'           => $hpp_map->map,
                                            'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
                                            'updated_at'    => date('Y-m-d H:i:s'),
                                            'updated_by'    => auth()->user()->id,
                                        ]);
                                        $produk_harga->save();
                                    }
                                }else{
                                    $margin_produk = isset($detail->produk->produksetting->margin) ? $detail->produk->produksetting->margin : 1;
                                    $hpp           = $hpp_map->map;
                                    $margin_persen = ($margin_produk * $hpp) / 100;
                                    $hitung_margin = $margin_persen + $hpp ;
                                    $roundup       = round(round($hitung_margin)/100) * 100;

                                    $amount = $roundup - $hpp;

                                    $new_produk_harga = new ProdukHarga;
                                    $new_produk_harga->fill([
                                        'tmuk_kode'     => $po->tmuk_kode,
                                        'produk_kode'   => $detail->produk_kode,
                                        'cost_price'    => $hpp_map->price,
                                        'suggest_price' => $roundup,
                                        'change_price'  => 0,
                                        'margin_amount' => $roundup - $hpp,
                                        'map'           => $hpp_map->map,
                                        'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
                                        'created_at'    => date('Y-m-d H:i:s'),
                                        'created_by'    => auth()->user()->id,
                                    ]);
                                    $new_produk_harga->save();
                                }

                                //insert or update ProdukTmuk
                                $produk_tmuk = ProdukTmuk::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $po->tmuk_kode)->first();

                                if ($produk_tmuk) {
                                    //cek kondisi stock_awal
                                    if ($produk_tmuk->stock_awal != NULL ) {
                                        $produk_tmuk->stock_awal = $produk_tmuk->stock_akhir + $detail->qty_pr;
                                        $produk_tmuk->updated_at = date('Y-m-d H:i:s');
                                        $produk_tmuk->updated_by = auth()->user()->id;
                                        $produk_tmuk->save();
                                    }else{
                                        $produk_tmuk->stock_awal = $detail->qty_pr;
                                        $produk_tmuk->updated_at = date('Y-m-d H:i:s');
                                        $produk_tmuk->updated_by = auth()->user()->id;
                                        $produk_tmuk->save();
                                    }
                                }
                            }
                        }

                    }
                }

                // insrt to jurnal
                $th_fiskal      = TahunFiskal::getTahunFiskal();
                $idtrans        = SecTransaksi::generate(); // sementara
                // $tanggal        = date('Y-m-d H:i:s');
                $tanggal        = $approval4->verifikasi1_date;
                $tmuk_kode      = $approval4->tmuk_kode;
                
                $no_pr          = $detail_escrow->transpo->nomor_pr;

                $saldo_potong   = $detail_escrow->saldo_dipotong;
                $deposit        = $detail_escrow->saldo_awal_deposit;
                $escrow         = $detail_escrow->saldo_awal_escrow;

                $check = $deposit - $saldo_potong;
                if ($check <= 0) {
                    $selisih_deposit = $deposit;
                    $selisih_escrow = $escrow - ($check + $escrow);
                }else{
                    $selisih_deposit = $saldo_potong;
                    $selisih_escrow = 0;
                }
                //Jurnal
                $jurnal = TransJurnal::insert([
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $approval4->po_nomor,
                        'deskripsi' => 'Reduce Escrow',
                        'coa_kode' => '1.1.6.1', // deposit
                        'jumlah' => $selisih_deposit,
                        'posisi' => 'K',
                        'flag' => '-',
                    ],
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $approval4->po_nomor,
                        'deskripsi' => 'Reduce Escrow',
                        'coa_kode' => '1.1.2.1', // escrow
                        'jumlah' => $selisih_escrow,
                        'posisi' => 'K',
                        'flag' => '-',
                    ],
                    [
                        'idtrans' => $idtrans,
                        'tahun_fiskal_id' => $th_fiskal->id,
                        'tanggal' => $tanggal,
                        'tmuk_kode' => $tmuk_kode,
                        'referensi' => $approval4->po_nomor,
                        'deskripsi' => 'Reduce Escrow',
                        'coa_kode' => '1.1.4.0', // persediaan barang dagang
                        'jumlah' => $saldo_potong,
                        'posisi' => 'D',
                        'flag' => '+',
                    ],
                ]);

                //update trans reduce escrow id_transaksi
                $approval4->id_transaksi = $idtrans;
                $approval4->save();
                //JAMES KONTAINER
                $kontainer = new TransKontainer;
                $kontainer->nomor = TransKontainer::generateCode($approval4->tmuk_kode);
                $kontainer->po_nomor = $approval4->po_nomor;
                $kontainer->tmuk_kode = $approval4->tmuk_kode;
                $kontainer->save();

                $voulume=0;

                $kont = Kontainer::where('tipe_box',1)->get();

                foreach ($kont as $value) {
                    $volume = $value->tinggi_dalam * $value->lebar_bawah * $value->panjang_bawah;
                    $vol=0;
                    $i = 1;
                    foreach ($kontainer->po->detail as $key) {
                        $jumlah_produk=0;
                        if($key->produk->produksetting->jenis_barang_kode=='001'){
                            if($key->produk->produksetting->uom2_boxtype==0){
                                for ($xx=1; $xx <= $key->qty_po ; $xx++) { 
                                    // var_dump($i);
                                    $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
                                    if($vol>=$volume){
                                        $vol = 0;
                                        // $i++;
                                        $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
                                    }
                                    if($vol<$volume){
                                        $jumlah_produk++;
                                        $temp_vol = ($key->produk->height/10) * ($key->produk->width/10) * ($key->produk->length/10);
                                        $temp = $vol + $temp_vol;
                                        if($temp>=$volume && $xx==$key->qty_po){
                                            $kont_detail = new TransKontainerDetail;
                                            $kont_detail->id_kontainer = $value->id;
                                            $kont_detail->id_trans_kontainer = $kontainer->id;
                                            $kont_detail->produk_kode = $key->produk->kode;
                                            $kont_detail->no_kontainer = $i;
                                            $kont_detail->jumlah_produk = $jumlah_produk;
                                            $kont_detail->save();

                                            $i++;
                                            $vol=0;
                                        }else if($temp<$volume && $xx==$key->qty_po){
                                            $kont_detail = new TransKontainerDetail;
                                            $kont_detail->id_kontainer = $value->id;
                                            $kont_detail->id_trans_kontainer = $kontainer->id;
                                            $kont_detail->produk_kode = $key->produk->kode;
                                            $kont_detail->no_kontainer = $i;
                                            $kont_detail->jumlah_produk = $jumlah_produk;
                                            $kont_detail->save();
                                            $jumlah_produk=0;
                                        }else if($temp>=$volume && $xx<$key->qty_po){
                                            $kont_detail = new TransKontainerDetail;
                                            $kont_detail->id_kontainer = $value->id;
                                            $kont_detail->id_trans_kontainer = $kontainer->id;
                                            $kont_detail->produk_kode = $key->produk->kode;
                                            $kont_detail->no_kontainer = $i;
                                            $kont_detail->jumlah_produk = $jumlah_produk;
                                            $kont_detail->save();

                                            $i++;
                                            $vol=0;
                                            $jumlah_produk=0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $maxkont=TransKontainerDetail::where('id_trans_kontainer',$kontainer->id)->max('no_kontainer');

                foreach ($kont as $value) {
                    $volume = $value->tinggi_dalam * $value->lebar_bawah * $value->panjang_bawah;
                    $vol=0;
                    $i = $maxkont;
                    $i++;
                    foreach ($kontainer->po->detail as $key) {
                        $jumlah_produk=0;
                        if($key->produk->produksetting->jenis_barang_kode=='002'){
                            if($key->produk->produksetting->uom2_boxtype==0){
                                for ($xx=1; $xx <= $key->qty_po ; $xx++) { 
                                   $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
                                   if($vol>=$volume){
                                    $vol = 0;
                                        // $i++;
                                    $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
                                }
                                if($vol<$volume){
                                    $jumlah_produk++;
                                    $temp_vol = ($key->produk->height/10) * ($key->produk->width/10) * ($key->produk->length/10);
                                    $temp = $vol + $temp_vol;
                                    if($temp>=$volume && $xx==$key->qty_po){
                                        $kont_detail = new TransKontainerDetail;
                                        $kont_detail->id_kontainer = $value->id;
                                        $kont_detail->id_trans_kontainer = $kontainer->id;
                                        $kont_detail->produk_kode = $key->produk->kode;
                                        $kont_detail->no_kontainer = $i;
                                        $kont_detail->jumlah_produk = $jumlah_produk;
                                        $kont_detail->save();

                                        $i++;
                                        $vol=0;
                                    }else if($temp<$volume && $xx==$key->qty_po){
                                        $kont_detail = new TransKontainerDetail;
                                        $kont_detail->id_kontainer = $value->id;
                                        $kont_detail->id_trans_kontainer = $kontainer->id;
                                        $kont_detail->produk_kode = $key->produk->kode;
                                        $kont_detail->no_kontainer = $i;
                                        $kont_detail->jumlah_produk = $jumlah_produk;
                                        $kont_detail->save();
                                        $jumlah_produk=0;
                                    }else if($temp>=$volume && $xx<$key->qty_po){
                                        $kont_detail = new TransKontainerDetail;
                                        $kont_detail->id_kontainer = $value->id;
                                        $kont_detail->id_trans_kontainer = $kontainer->id;
                                        $kont_detail->produk_kode = $key->produk->kode;
                                        $kont_detail->no_kontainer = $i;
                                        $kont_detail->jumlah_produk = $jumlah_produk;
                                        $kont_detail->save();

                                        $i++;
                                        $vol=0;
                                        $jumlah_produk=0;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $max=TransKontainerDetail::where('id_trans_kontainer',$kontainer->id)->max('no_kontainer');

            foreach ($kontainer->po->detail as $key) {
                if($key->qty_po>0){
                    if($key->produk->produksetting->uom2_boxtype==1){
                        $max+=1;
                        $kont_detail = new TransKontainerDetail;
                        $kont_detail->id_kontainer = null;
                        $kont_detail->id_trans_kontainer = $kontainer->id;
                        $kont_detail->produk_kode = $key->produk->kode;
                        $kont_detail->no_kontainer = $max;
                        $kont_detail->jumlah_produk = $key->qty_po;
                        $kont_detail->save();
                    }
                }
            }

            $truk = new TransTruk;
            $truk->po_nomor = $approval4->po_nomor;
            $truk->tmuk_kode = $approval4->tmuk_kode;
            $truk->save();

            // insert to log audit
            TransLogAudit::setLog([
                'tanggal_transaksi' => date('Y-m-d'),
                'type'              => 'Reduce Escrow Balance ',
                'ref'               => '',
                'aksi'              => 'Disetujui',
                'amount'            => $saldo_potong,
            ]);

            return response([
                'status' => true,
                'data'  => $approval4
            ]);
            //reduce escrow
        }
    }
    //h2h
    // {
    //     $approval4 = TransReduceEscrow::find($id);
    //     $filename = $approval4->po_nomor . date('YmdHis');
    //     \Lotte\Libraries\H2H::createCsv([
    //         'CustRefNo'            => $filename,
    //         'ValueDate'            => date('YmdHi'),
    //         'Amount'               => $approval4->transpo->total,
    //         'TrxRemark'            => '',
    //         'RateVoucherCode'      => '',
    //         'ChargeType'           => '',
    //         'DebitAccount'         => $approval4->tmuk->rekeningpemilik->nomor_rekening,
    //         'SenderName'           => $approval4->tmuk->bankescrow->nama,
    //         'SenderAddress'        => $approval4->tmuk->bankescrow->kota->nama,
    //         'SenderCountryCode'    => '',
    //         'CreditAccount'        => $approval4->tmuk->rekeningpemilik->nomor_rekening,
    //         'BeneficiaryName'      => $approval4->tmuk->rekeningpemilik->nama_pemilik,
    //         'BeneficiaryAddress'   => $approval4->tmuk->rekeningpemilik->bankescrow->alamat,
    //         'BenBankIdentifier'    => '',
    //         'BenBankName'          => '',
    //         'BenBankAddress'       => '',
    //         'BenBankCountryCode'   => '',
    //         'InterBankIdentifier'  => '',
    //         'InterBankName'        => '',
    //         'InterBankAddress'     => '',
    //         'InterBankCountryCode' => '',
    //         'Notification'         => '',
    //         'BeneficiaryCategory'  => '',
    //         'BeneficiaryRelation'  => '',
    //         'BITransaction Code'   => '',
    //         'TemplateCode'         => '',
    //     ]);

    //     $approval4->verifikasi4_date = date('Y-m-d h:i:s');
    //     $approval4->verifikasi4_user = auth()->user()->id;

    //     // 0:awal, 1:proses upload, 2:proses bank, 3:sukses, 4:gagal
    //     $approval4->statush2h = 1;
    //     $approval4->keterangan = 'Proses Upload H2H';
    //     $approval4->nomorh2h = $filename;
    //     $approval4->save();

    //     return response([
    //         'status' => true,
    //         'data'  => $approval4
    //     ]);
    // }
    //h2h
    
    //taplak
    // {
    //     $approval4 = TransReduceEscrow::find($id);
    //     $approval4->verifikasi4_date = date('Y-m-d h:i:s');
    //     $approval4->verifikasi4_user = auth()->user()->id;
    //     $approval4->save();

    //     //update saldo trans_reduce_escrow_detail
    //     $detail = TransReduceEscrowDetail::where('reduce_escrow_id', $approval4->id)->get(); 
    //     $tonja = $detail->sum('total_belanja');

    //     //skema pemotongan di mulai dari deposit, scn, escrow
    //     $tmuk_saldo = Tmuk::where('kode', $approval4->tmuk_kode)->first();
    //     $awal_saldo_deposit = $tmuk_saldo->saldo_deposit;
    //     $awal_saldo_escrow = $tmuk_saldo->saldo_escrow;

    //     $saldo_deposit = $awal_saldo_deposit - $tonja;
    //     if ($saldo_deposit < 0) {
    //         $saldo_escrow = $awal_saldo_escrow + $saldo_deposit;
            
    //         $tmuk_saldo->saldo_deposit = 0;
    //         $tmuk_saldo->saldo_escrow = $saldo_escrow;
    //     }else{
    //         $tmuk_saldo->saldo_deposit = $saldo_deposit;
    //     }

    //     if (isset($approval4->transpo->nomor_pr)) {
    //         $no_po = substr($approval4->transpo->nomor_pr, -4);
    //         if ($no_po == '0000') {
    //             $tmuk_saldo->nomor_transaksi = $approval4->po_nomor;
    //             $tmuk_saldo->flag_transaksi  = 1;
    //         }
    //     }
        
    //     $tmuk_saldo->save();

    //     //update saldo dipotong trans_reduce_escrow
    //     $detail_escrow = TransReduceEscrow::where('id', $approval4->id)->first();
    //     $detail_escrow->saldo_dipotong       = $tonja;
    //     $detail_escrow->saldo_awal_deposit   = $awal_saldo_deposit;
    //     $detail_escrow->saldo_awal_escrow    = $awal_saldo_escrow;
    //     $detail_escrow->saldo_deposit        = $tmuk_saldo->saldo_deposit;
    //     $detail_escrow->saldo_escrow         = $tmuk_saldo->saldo_escrow;
    //     if($detail_escrow->save()){
    //         //update status 2 di trnas_po
    //         $po = TransPo::where('nomor', $approval4->po_nomor)->first();
    //         $po->status = 2;
    //         if($po->save()){
    //             foreach ($po->detail as $key => $detail) {
    //                 //check qty_po != 0
    //                 if ($detail->qty_po != 0) {
    //                     //insert hpp map
    //                     $conversi_uom2 = isset($detail->produk->produksetting->uom2_conversion) ? $detail->produk->produksetting->uom2_conversion : 1;

    //                     $check = TransHppMap::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $po->tmuk_kode)->get();

    //                     $po_qty = $detail->qty_po * $conversi_uom2; //ngambil hasil konversi
    //                     $po_price = $detail->price / $po_qty;
    //                     $total = $po_qty * $po_price;

    //                     if ($check->count() != 0) {
    //                         $jml = 0;
    //                         foreach ($check as $key => $data) {
    //                             $jml += $data->qty * $data->price;
    //                         }
    //                         $new_map = ($jml + $total) / ($check->sum('qty') + $po_qty);

    //                         $hpp_map = new TransHppMap;
    //                         $hpp_map->fill([
    //                             'tanggal'       => date('Y-m-d H:i:s'),
    //                             'po_nomor'      => $detail->po_nomor,
    //                             'produk_kode'   => $detail->produk_kode,
    //                             'tmuk_kode'     => $po->tmuk_kode,
    //                             'qty'           => $po_qty,
    //                             'price'         => $po_price,
    //                             'map'           => $new_map,
    //                             'created_at'    => date('Y-m-d H:i:s'),
    //                             'created_by'    => auth()->user()->id,
    //                         ]);
    //                         $hpp_map->save();
    //                     }else{
    //                         $hpp_map = new TransHppMap;
    //                         $hpp_map->fill([
    //                             'tanggal'       => date('Y-m-d H:i:s'),
    //                             'po_nomor'      => $detail->po_nomor,
    //                             'produk_kode'   => $detail->produk_kode,
    //                             'tmuk_kode'     => $po->tmuk_kode,
    //                             'qty'           => $po_qty,
    //                             'price'         => $po_price,
    //                             'map'           => $po_price,
    //                             'created_at'    => date('Y-m-d H:i:s'),
    //                             'created_by'    => auth()->user()->id,
    //                         ]);
    //                         $hpp_map->save();
    //                     }

    //                     //insert ProdukHarga
    //                     $produk_harga = ProdukHarga::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $po->tmuk_kode)->first();

    //                     if ($produk_harga) {
    //                         //cek kondisi change_price
    //                         if ($produk_harga->change_price == 0) {
    //                             $margin_produk = isset($detail->produk->produksetting->margin) ? $detail->produk->produksetting->margin : 1;
    //                             $hpp           = $hpp_map->map;
    //                             $margin_persen = ($margin_produk * $hpp) / 100;
    //                             $hitung_margin = $margin_persen + $hpp ;
    //                             $roundup       = round(round($hitung_margin)/100) * 100;

    //                             $amount = $roundup - $hpp;

    //                             $produk_harga->fill([
    //                                 'cost_price'    => $hpp_map->price,
    //                                 'suggest_price' => $roundup,
    //                                 'change_price'  => 0,
    //                                 'margin_amount' => $roundup - $hpp,
    //                                 'map'           => $hpp_map->map,
    //                                 'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
    //                                 'created_at'    => date('Y-m-d H:i:s'),
    //                                 'created_by'    => auth()->user()->id,
    //                             ]);
    //                             $produk_harga->save();
    //                         }else{
    //                             $produk_harga->fill([
    //                                 'cost_price'    => $hpp_map->price,
    //                             // 'suggest_price' => 0,
    //                             // 'change_price'  => 0,
    //                                 'margin_amount' => $produk_harga->change_price - $hpp_map->map,
    //                                 'map'           => $hpp_map->map,
    //                                 'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
    //                                 'updated_at'    => date('Y-m-d H:i:s'),
    //                                 'updated_by'    => auth()->user()->id,
    //                             ]);
    //                             $produk_harga->save();
    //                         }
    //                     }else{
    //                         $margin_produk = isset($detail->produk->produksetting->margin) ? $detail->produk->produksetting->margin : 1;
    //                         $hpp           = $hpp_map->map;
    //                         $margin_persen = ($margin_produk * $hpp) / 100;
    //                         $hitung_margin = $margin_persen + $hpp ;
    //                         $roundup       = round(round($hitung_margin)/100) * 100;

    //                         $amount = $roundup - $hpp;

    //                         $new_produk_harga = new ProdukHarga;
    //                         $new_produk_harga->fill([
    //                             'tmuk_kode'     => $po->tmuk_kode,
    //                             'produk_kode'   => $detail->produk_kode,
    //                             'cost_price'    => $hpp_map->price,
    //                             'suggest_price' => $roundup,
    //                             'change_price'  => 0,
    //                             'margin_amount' => $roundup - $hpp,
    //                             'map'           => $hpp_map->map,
    //                             'gmd_price'     => $detail->produk->produklsi->curr_sale_prc,
    //                             'created_at'    => date('Y-m-d H:i:s'),
    //                             'created_by'    => auth()->user()->id,
    //                         ]);
    //                         $new_produk_harga->save();
    //                     }

    //                     //insert or update ProdukTmuk
    //                     $produk_tmuk = ProdukTmuk::where('produk_kode', $detail->produk_kode)->where('tmuk_kode', $po->tmuk_kode)->first();

    //                     if ($produk_tmuk) {
    //                         //cek kondisi stock_awal
    //                         if ($produk_tmuk->stock_awal != NULL ) {
    //                             $produk_tmuk->stock_awal = $produk_tmuk->stock_akhir + $detail->qty_pr;
    //                             $produk_tmuk->updated_at = date('Y-m-d H:i:s');
    //                             $produk_tmuk->updated_by = auth()->user()->id;
    //                             $produk_tmuk->save();
    //                         }else{
    //                             $produk_tmuk->stock_awal = $detail->qty_pr;
    //                             $produk_tmuk->updated_at = date('Y-m-d H:i:s');
    //                             $produk_tmuk->updated_by = auth()->user()->id;
    //                             $produk_tmuk->save();
    //                         }
    //                     }
    //                 }
    //             }

    //         }
    //     }

    //     // insrt to jurnal
    //     $th_fiskal      = TahunFiskal::getTahunFiskal();
    //     $idtrans        = SecTransaksi::generate(); // sementara
    //     // $tanggal        = date('Y-m-d H:i:s');
    //     $tanggal        = $approval4->verifikasi1_date;
    //     $tmuk_kode      = $approval4->tmuk_kode;
        
    //     $no_pr          = $detail_escrow->transpo->nomor_pr;

    //     $saldo_potong   = $detail_escrow->saldo_dipotong;
    //     $deposit        = $detail_escrow->saldo_awal_deposit;
    //     $escrow         = $detail_escrow->saldo_awal_escrow;

    //     $check = $deposit - $saldo_potong;
    //     if ($check <= 0) {
    //         $selisih_deposit = $deposit;
    //         $selisih_escrow = $escrow - ($check + $escrow);
    //     }else{
    //         $selisih_deposit = $saldo_potong;
    //         $selisih_escrow = 0;
    //     }
    //     //Jurnal
    //     $jurnal = TransJurnal::insert([
    //         [
    //             'idtrans' => $idtrans,
    //             'tahun_fiskal_id' => $th_fiskal->id,
    //             'tanggal' => $tanggal,
    //             'tmuk_kode' => $tmuk_kode,
    //             'referensi' => '-',
    //             'deskripsi' => 'Reduce Escrow',
    //             'coa_kode' => '1.1.6.1', // deposit
    //             'jumlah' => $selisih_deposit,
    //             'posisi' => 'K',
    //             'flag' => '-',
    //         ],
    //         [
    //             'idtrans' => $idtrans,
    //             'tahun_fiskal_id' => $th_fiskal->id,
    //             'tanggal' => $tanggal,
    //             'tmuk_kode' => $tmuk_kode,
    //             'referensi' => '-',
    //             'deskripsi' => 'Reduce Escrow',
    //             'coa_kode' => '1.1.2.1', // escrow
    //             'jumlah' => $selisih_escrow,
    //             'posisi' => 'K',
    //             'flag' => '-',
    //         ],
    //         [
    //             'idtrans' => $idtrans,
    //             'tahun_fiskal_id' => $th_fiskal->id,
    //             'tanggal' => $tanggal,
    //             'tmuk_kode' => $tmuk_kode,
    //             'referensi' => '-',
    //             'deskripsi' => 'Reduce Escrow',
    //             'coa_kode' => '1.1.4.0', // persediaan barang dagang
    //             'jumlah' => $saldo_potong,
    //             'posisi' => 'D',
    //             'flag' => '+',
    //         ],
    //     ]);

    //     //update trans reduce escrow id_transaksi
    //     $approval4->id_transaksi = $idtrans;
    //     $approval4->save();
    //     //JAMES KONTAINER
    //     $kontainer = new TransKontainer;
    //     $kontainer->nomor = TransKontainer::generateCode($approval4->tmuk_kode);
    //     $kontainer->po_nomor = $approval4->po_nomor;
    //     $kontainer->tmuk_kode = $approval4->tmuk_kode;
    //     $kontainer->save();

    //     $voulume=0;

    //     $kont = Kontainer::where('tipe_box',1)->get();

    //     foreach ($kont as $value) {
    //         $volume = $value->tinggi_dalam * $value->lebar_bawah * $value->panjang_bawah;
    //         $vol=0;
    //         $i = 1;
    //         foreach ($kontainer->po->detail as $key) {
    //             $jumlah_produk=0;
    //             if($key->produk->produksetting->jenis_barang_kode=='001'){
    //                 if($key->produk->produksetting->uom2_boxtype==0){
    //                     for ($xx=1; $xx <= $key->qty_po ; $xx++) { 
    //                         // var_dump($i);
    //                         $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
    //                         if($vol>=$volume){
    //                             $vol = 0;
    //                             // $i++;
    //                             $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
    //                         }
    //                         if($vol<$volume){
    //                             $jumlah_produk++;
    //                             $temp_vol = ($key->produk->height/10) * ($key->produk->width/10) * ($key->produk->length/10);
    //                             $temp = $vol + $temp_vol;
    //                             if($temp>=$volume && $xx==$key->qty_po){
    //                                 $kont_detail = new TransKontainerDetail;
    //                                 $kont_detail->id_kontainer = $value->id;
    //                                 $kont_detail->id_trans_kontainer = $kontainer->id;
    //                                 $kont_detail->produk_kode = $key->produk->kode;
    //                                 $kont_detail->no_kontainer = $i;
    //                                 $kont_detail->jumlah_produk = $jumlah_produk;
    //                                 $kont_detail->save();

    //                                 $i++;
    //                                 $vol=0;
    //                             }else if($temp<$volume && $xx==$key->qty_po){
    //                                 $kont_detail = new TransKontainerDetail;
    //                                 $kont_detail->id_kontainer = $value->id;
    //                                 $kont_detail->id_trans_kontainer = $kontainer->id;
    //                                 $kont_detail->produk_kode = $key->produk->kode;
    //                                 $kont_detail->no_kontainer = $i;
    //                                 $kont_detail->jumlah_produk = $jumlah_produk;
    //                                 $kont_detail->save();
    //                                 $jumlah_produk=0;
    //                             }else if($temp>=$volume && $xx<$key->qty_po){
    //                                 $kont_detail = new TransKontainerDetail;
    //                                 $kont_detail->id_kontainer = $value->id;
    //                                 $kont_detail->id_trans_kontainer = $kontainer->id;
    //                                 $kont_detail->produk_kode = $key->produk->kode;
    //                                 $kont_detail->no_kontainer = $i;
    //                                 $kont_detail->jumlah_produk = $jumlah_produk;
    //                                 $kont_detail->save();

    //                                 $i++;
    //                                 $vol=0;
    //                                 $jumlah_produk=0;
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }

    //     $maxkont=TransKontainerDetail::where('id_trans_kontainer',$kontainer->id)->max('no_kontainer');

    //     foreach ($kont as $value) {
    //         $volume = $value->tinggi_dalam * $value->lebar_bawah * $value->panjang_bawah;
    //         $vol=0;
    //         $i = $maxkont;
    //         $i++;
    //         foreach ($kontainer->po->detail as $key) {
    //             $jumlah_produk=0;
    //             if($key->produk->produksetting->jenis_barang_kode=='002'){
    //                 if($key->produk->produksetting->uom2_boxtype==0){
    //                     for ($xx=1; $xx <= $key->qty_po ; $xx++) { 
    //                        $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
    //                        if($vol>=$volume){
    //                         $vol = 0;
    //                             // $i++;
    //                         $vol+= ($key->produk->produksetting->uom2_height/10) * ($key->produk->produksetting->uom2_width/10) * ($key->produk->produksetting->uom2_length/10);
    //                     }
    //                     if($vol<$volume){
    //                         $jumlah_produk++;
    //                         $temp_vol = ($key->produk->height/10) * ($key->produk->width/10) * ($key->produk->length/10);
    //                         $temp = $vol + $temp_vol;
    //                         if($temp>=$volume && $xx==$key->qty_po){
    //                             $kont_detail = new TransKontainerDetail;
    //                             $kont_detail->id_kontainer = $value->id;
    //                             $kont_detail->id_trans_kontainer = $kontainer->id;
    //                             $kont_detail->produk_kode = $key->produk->kode;
    //                             $kont_detail->no_kontainer = $i;
    //                             $kont_detail->jumlah_produk = $jumlah_produk;
    //                             $kont_detail->save();

    //                             $i++;
    //                             $vol=0;
    //                         }else if($temp<$volume && $xx==$key->qty_po){
    //                             $kont_detail = new TransKontainerDetail;
    //                             $kont_detail->id_kontainer = $value->id;
    //                             $kont_detail->id_trans_kontainer = $kontainer->id;
    //                             $kont_detail->produk_kode = $key->produk->kode;
    //                             $kont_detail->no_kontainer = $i;
    //                             $kont_detail->jumlah_produk = $jumlah_produk;
    //                             $kont_detail->save();
    //                             $jumlah_produk=0;
    //                         }else if($temp>=$volume && $xx<$key->qty_po){
    //                             $kont_detail = new TransKontainerDetail;
    //                             $kont_detail->id_kontainer = $value->id;
    //                             $kont_detail->id_trans_kontainer = $kontainer->id;
    //                             $kont_detail->produk_kode = $key->produk->kode;
    //                             $kont_detail->no_kontainer = $i;
    //                             $kont_detail->jumlah_produk = $jumlah_produk;
    //                             $kont_detail->save();

    //                             $i++;
    //                             $vol=0;
    //                             $jumlah_produk=0;
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     }

    //     $max=TransKontainerDetail::where('id_trans_kontainer',$kontainer->id)->max('no_kontainer');

    //     foreach ($kontainer->po->detail as $key) {
    //         if($key->qty_po>0){
    //             if($key->produk->produksetting->uom2_boxtype==1){
    //                 $max+=1;
    //                 $kont_detail = new TransKontainerDetail;
    //                 $kont_detail->id_kontainer = null;
    //                 $kont_detail->id_trans_kontainer = $kontainer->id;
    //                 $kont_detail->produk_kode = $key->produk->kode;
    //                 $kont_detail->no_kontainer = $max;
    //                 $kont_detail->jumlah_produk = $key->qty_po;
    //                 $kont_detail->save();
    //             }
    //         }
    //     }

    //     $truk = new TransTruk;
    //     $truk->po_nomor = $approval4->po_nomor;
    //     $truk->tmuk_kode = $approval4->tmuk_kode;
    //     $truk->save();

    //     // insert to log audit
    //     TransLogAudit::setLog([
    //         'tanggal_transaksi' => date('Y-m-d'),
    //         'type'              => 'Reduce Escrow Balance ',
    //         'ref'               => '',
    //         'aksi'              => 'Disetujui',
    //         'amount'            => $saldo_potong,
    //     ]);

    //     return response([
    //         'status' => true,
    //         'data'  => $approval4
    //     ]);
    // }
    // taplak 


public function create()
{
    return $this->render('modules.transaksi.pembelian.reduce-escrow-balance.create');
}

public function edit($id)
{
    $transreduceescrow = TransReduceEscrow::find($id);

    if (auth()->user()->hasRole('cso-persetujuan1')) {
        $disabled = '';
    } else {
        $disabled = 'readonly=""';
    }

    $data = [
        'record' => $transreduceescrow,
        'role' => $disabled,
    ];
        // dd($data);
    return $this->render('modules.transaksi.pembelian.reduce-escrow-balance.edit', $data);
}

public function details($id)
{
    $transreduceescrow = TransReduceEscrow::find($id);
    $detail = TransReduceEscrowDetail::where('reduce_escrow_id', $transreduceescrow->id)->get(); 

    $data = [
        'record' => $transreduceescrow,
        'detail' => $detail,
        'total' => $detail->sum('total_belanja'),
    ];
        // dd($data);
    return $this->render('modules.transaksi.pembelian.reduce-escrow-balance.detail', $data);
}

public function detail($id)
{
    $transreduceescrow = TransReduceEscrow::find($id);
    $detail = TransReduceEscrowDetail::where('reduce_escrow_id', $transreduceescrow->id)->get(); 

    $data = [
        'record' => $transreduceescrow,
        'detail' => $detail,
        'total' => $detail->sum('total_belanja'),
    ];
        // dd($data);
    return $this->render('modules.transaksi.pembelian.reduce-escrow-balance.detail', $data);
}

public function detailPo($id)
{
    $transreduceescrow = TransReduceEscrow::find($id);
    
    $po = TransPo::with('tmuk', 'tmuk.lsi')->find($transreduceescrow->transpo->id);
        // dd($po->tmuk->lsi->kode);
    $records = TransPo::has('detail')->select(DB::raw('
        trans_po.nomor as nomor,
        trans_po.nomor_pr as nomor_pr,

        ref_produk.bumun_nm as bumun_nm,
        ref_produk.l1_nm as l1_nm,
        ref_produk.nama as nama,
        ref_produk.kode as produk_kode,
        ref_produk_setting.uom1_barcode as uom1_barcode,
        ref_produk.nama as nama,
        
        ref_produk_setting.uom1_selling_cek as uom1_selling_cek,

        ref_produk_lsi.lsi_kode as lsi_kode,
        ref_produk_lsi.stok_gmd as stok_gmd,
        ref_produk_lsi.curr_sale_prc as harga_estimasi,

        trans_po_detail.id as id_detail,
        trans_po_detail.qty_po as qty_po,
        trans_po_detail.unit_po as unit_po,
        trans_po_detail.qty_pr as qty_pr,
        trans_po_detail.unit_pr as unit_pr,

        trans_pr_detail.qty_order as qty_order,
        trans_pr_detail.unit_order as unit_order,
        trans_pr_detail.qty_sell as qty_sell,
        trans_pr_detail.unit_sell as unit_sell,
        trans_pr_detail.qty_pr as qty_from_pr,

        trans_po_detail.price as harga_aktual
        '))
    ->join('trans_po_detail', 'trans_po.nomor', '=', 'trans_po_detail.po_nomor')
    ->join('ref_produk', 'trans_po_detail.produk_kode', '=', 'ref_produk.kode')
    ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
    ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
    ->join('trans_pr_detail', function($join) {
        $join->on('trans_po_detail.produk_kode', '=', 'trans_pr_detail.produk_kode')
        ->on('trans_pr_detail.pr_nomor', '=', 'trans_po.nomor_pr');
    })
    ->where(DB::raw('trans_po.nomor'), $po->nomor)
    ->where(DB::raw('ref_produk_lsi.lsi_kode'), $po->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get();

    $data = [
        'record' => $po,
        'detail' => $records,
    ];

    return $this->render('modules.transaksi.pembelian.reduce-escrow-balance.detail-po', $data);
}

public function prints($id)
{

    $transreduceescrow = TransReduceEscrow::find($id);

    $data = [
        'record' => $transreduceescrow,
    ];

    $content = view('report.reduce-escrow', $data);

    $data = [
        'oriented'   => 'L',
        'paper'      => 'A4',
        'label_file' => 'Reduce Escrow '.$transreduceescrow->po_nomor,
    ];

    // return $content;
    HTML2PDF($content, $data);
}
}
