<?php

namespace Lotte\Http\Controllers\Transaksi\Pembelian;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
//Libraries;
use DB;
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPrDetail;
use Lotte\Models\Trans\TransPo;
use Lotte\Models\Trans\TransPoDetail;
use Lotte\Models\Trans\TransReduceEscrow;
use Lotte\Models\Trans\TransReduceEscrowDetail;
use Lotte\Models\Trans\TransLogAudit;

class PemesananPoController extends Controller
{
	protected $link = 'transaksi/pembelian/pemesanan-po/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Purchase Order (PO)");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen");
		$this->setBreadcrumb(['Transaksi' => '#', 'Pembelian (PO)' => '#', 'Purchase Order (PO)' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => 'No',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
            [
                'data' => 'nomor_pr',
                'name' => 'nomor_pr',
                'label' => 'Nomor PR',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
			[
			    'data' => 'tmuk',
			    'name' => 'tmuk',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'lsi',
			    'name' => 'lsi',
			    'label' => 'LSI',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'tgl_buat',
			    'name' => 'tgl_buat',
			    'label' => 'Tanggal Picking Result',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
            [
                'data' => 'jumlah',
                'name' => 'jumlah',
                'label' => 'Jumlah Picking (Rp)',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'picking_result',
                'name' => 'picking_result',
                'label' => 'Picking Result',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '120px',
            ],
			[
			    'data' => 'po',
			    'name' => 'po',
			    'label' => 'PO (BTDK)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '100px',
			],
            [
                'data' => 'po_stdk',
                'name' => 'po_stdk',
                'label' => 'PO (STDK)',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
		]);
	}

	public function grid(Request $request)
	{
		$records = TransPo::with('tmuk');
            if(auth()->user()->hasRole('system-administrator')){
                $records->orderBy('status', 'ASC');
            }elseif (isset(auth()->user()->pengguna->lsi_id)) {
                $records->whereHas('tmuk', function($q){
                    $q->where('lsi_id', auth()->user()->pengguna->lsi_id);
                })->orderBy('status', 'ASC');
            }else{
                $records->orderBy('status', 'ASC');
            }
		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if($tgl_buat = $request->tgl_buat){
            $dates = \Carbon\Carbon::parse($tgl_buat)->format('Y-m-d');
            $records->where(\DB::raw('DATE(tgl_buat)'),$dates);
        }
        if ($nomor_pr = $request->nomor_pr) {
            $records->where('nomor_pr', 'like', '%' . $nomor_pr . '%');
        }
        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('tmuk', function ($record) {
                return $record->tmuk_kode.' - '.$record->tmuk->nama;
            })
            ->addColumn('lsi', function ($record) {
                return $record->tmuk->lsi->nama;
            })
            ->addColumn('picking_result', function ($record) {
                $string = '
                            <button type="button" class="ui mini orange icon detail button" data-content="View Data" data-id="'.$record->id.'"><i class="edit icon"></i>View</button>
                            <a target="_blank" href="pemesanan-po/print-picking-result/'.$record->id.'" class="ui mini default icon print button" data-content="Print Picking Result"><i class="print icon"></i> Print</a>
                          ';
                return $string;
            })
            ->addColumn('jumlah', function ($record) {
				return FormatCurrency($record->total);
            })
            ->addColumn('po', function ($record) {
                // $string = '<button type="button" class="ui mini primary icon add button" data-content="Buat Picking" data-id="'.$record->id.'"><i class="add icon"></i> Buat</button>';
                $string = '';
                if ($record->status == 0) {
                    $string = '<button type="button" class="ui mini primary icon edit button" data-content="Buat PO" data-id="'.$record->id.'"><i class="add icon"></i> Buat</button>';
                }else{
                    $string = '<a target="_blank" href="pemesanan-po/print-po-btdk/'.$record->id.'" class="ui mini default icon print button" data-content="Print PO BTDK"><i class="print icon"></i> Print</a>';
                }
                return $string;
            })
            ->addColumn('po_stdk', function ($record) {
                // $string = '<button type="button" class="ui mini primary icon add button" data-content="Buat Picking" data-id="'.$record->id.'"><i class="add icon"></i> Buat</button>';
                $string = '';
                if ($record->status == 2) {
                    $string = '<a target="_blank" href="pemesanan-po/print-po-stdk/'.$record->id.'" class="ui mini default icon print button" data-content="Print PO STDK"><i class="print icon"></i> Print</a>';
                }elseif ($record->status == 3) {
                    $string = '<i style="font-size: 15px; color: red;" data-content="Melebihi Batas Waktu">Ditolak</i>';
                }else{
                    $string = '<a target="_blank" href="pemesanan-po/print-po-stdk/'.$record->id.'" class="ui mini default icon print button disabled" data-content="Print PO STDK"><i class="print icon"></i> Print</a>';
                }
                return $string;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.transaksi.pembelian.pemesanan-po.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.transaksi.pembelian.pemesanan-po.create');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        // dd($data);
        
        $po = TransPo::find($id);

        if($po){
            if ($po->status == 0) {
                $array = array_filter($data['detail_id']);

                foreach ($array as $key => $value) {
                    //trans_po_detail
                    $detail = TransPoDetail::find($value);

                    if ($detail->qty_po == 0) {

                        $detail->qty_po = $data['detail_qty_po'][$key];
                        $detail->price = priceBblm($detail->produk->produklsi, $data['detail_qty_po'][$key]); //parse to bblm
                    }else{
                        $detail->qty_po = $data['detail_qty_po'][$key];
                        $detail->price = decimal_for_save($data['detail_price'][$key]); //parse to desimal for save
                    }
                    $detail->updated_at = date('Y-m-d H:i:s');
                    $detail->updated_by = auth()->user()->id;
                    $detail->save();
                }

                $total = decimal_for_save($data['detail_price']); //parse to desimal for save
                //save trans_po
                $po->status = 1;
                $po->total = array_sum($total);
                if($po->save()){
                    $reduceescrow = new TransReduceEscrow;
                    $reduceescrow->tanggal          = $po->updated_at;
                    $reduceescrow->po_nomor         = $po->nomor;
                    $reduceescrow->tmuk_kode        = $po->tmuk_kode;
                    $reduceescrow->created_at       = date('Y-m-d H:i:s');
                    $reduceescrow->created_by       = auth()->user()->id;
                    $reduceescrow->save();
                }

                // insert to log audit
                TransLogAudit::setLog([
                    'tanggal_transaksi' => date('Y-m-d'),
                    'type'              => 'Purchase Order (PO)',
                    'ref'               => '',
                    'aksi'              => 'PO (BTDK)',
                    'amount'            => $po->total,
                ]);
            
                return response([
                    'status' => true,
                    'data'  => 'success'
                ]);
            }else{
                return response([
                    'status' => 'Data sudah di proses sebelumnya, silahkan cek kembali.',
                ], 402);
            }
        }

    }

    public function detail($id)
    {
        $po = TransPo::find($id);

        $pr = TransPr::with('tmuk', 'tmuk.lsi')->find($po->pr->id);
        // dd($pr->tmuk->lsi->kode);
        $records = TransPr::has('detailpr')->select(DB::raw('ref_produk_lsi.lsi_kode as lsi_kode,
                            trans_pr_detail.id as id_detail,
                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            trans_pr_detail.produk_kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.qty_pr as qty_pr,
                            trans_pr_detail.unit_pr as unit_pr,
                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.qty_pick as qty_pick,
                            trans_pr_detail.qty_aktual as qty_aktual'
                        ))
                      ->join('trans_pr_detail', 'trans_pr.nomor', '=', 'trans_pr_detail.pr_nomor')
                      ->join('ref_produk', 'trans_pr_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->where(DB::raw('trans_pr.nomor'), $pr->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get()->toArray();
        $data = [
            'record' => $pr,
            'detail' => $records,
        ];
        return $this->render('modules.transaksi.pembelian.pemesanan-po.detail', $data);

    }

    public function edit($id)
    {
    	$po = TransPo::with('tmuk', 'tmuk.lsi')->find($id);
        // dd($po->tmuk->lsi->kode);
        $records = TransPo::has('detail')->select(DB::raw('
                            trans_po.nomor as nomor,
                            trans_po.nomor_pr as nomor_pr,

                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            ref_produk.nama as nama,
                            ref_produk.kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            
                            ref_produk_setting.uom1_selling_cek as uom1_selling_cek,

                            ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            ref_produk_lsi.curr_sale_prc as harga_estimasi,

                            trans_po_detail.id as id_detail,
                            trans_po_detail.qty_po as qty_po,
                            trans_po_detail.unit_po as unit_po,
                            trans_po_detail.qty_pr as qty_pr,
                            trans_po_detail.unit_pr as unit_pr,

                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.qty_pr as qty_from_pr,

                            trans_po_detail.price as harga_aktual
                        '))
                      ->join('trans_po_detail', 'trans_po.nomor', '=', 'trans_po_detail.po_nomor')
                      ->join('ref_produk', 'trans_po_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->join('trans_pr_detail', function($join) {
                            $join->on('trans_po_detail.produk_kode', '=', 'trans_pr_detail.produk_kode')
                                  ->on('trans_pr_detail.pr_nomor', '=', 'trans_po.nomor_pr');
                      })
                      ->where(DB::raw('trans_po.nomor'), $po->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $po->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get();

        $data = [
            'record' => $po,
            'detail' => $records,
        ];
        return $this->render('modules.transaksi.pembelian.pemesanan-po.edit', $data);
    }

    public function printPickingResult($id)
    {
        $po = TransPo::find($id);

        $pr = TransPr::find($po->pr->id);
        // dd($pr->tmuk->lsi->kode);
        $records = TransPr::has('detailpr')->select(DB::raw('ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            trans_pr_detail.produk_kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.qty_pr as qty_pr,
                            trans_pr_detail.unit_pr as unit_pr,
                            trans_pr_detail.qty_pick as qty_pick'
                        ))
                      ->join('trans_pr_detail', 'trans_pr.nomor', '=', 'trans_pr_detail.pr_nomor')
                      ->join('ref_produk', 'trans_pr_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->where(DB::raw('trans_pr.nomor'), $pr->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get()->toArray();
        $data = [
            'record' => $pr,
            'detail' => $records,
        ];
            // $content = view('report.picking-result', $data);

            // $data = [
            //         'oriented'   => 'L',
            //         'paper'      => 'A4',
            //         'label_file' => 'Picking Result '. $pr->nomor,
            // ];

            // HTML2PDF($content, $data);
            $pdf = \PDF::loadView('report.picking-result', $data);
            return $pdf->setOption('margin-bottom', 5)
            ->setOption('margin-top', 5)
            ->setOption('margin-left', 3)
            ->setOption('margin-right', 3)
            ->inline();
    }

    public function printPoBtdk($id)
    {
        $po = TransPo::with('tmuk', 'tmuk.lsi')->find($id);
        // dd($po->tmuk->lsi->kode);
        $records = TransPo::has('detail')->select(DB::raw('
                            trans_po.nomor as nomor,
                            trans_po.nomor_pr as nomor_pr,

                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            ref_produk.nama as nama,
                            ref_produk.kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            
                            ref_produk_setting.uom1_selling_cek as uom1_selling_cek,

                            ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            ref_produk_lsi.curr_sale_prc as harga_estimasi,

                            trans_po_detail.id as id_detail,
                            trans_po_detail.qty_po as qty_po,
                            trans_po_detail.unit_po as unit_po,
                            trans_po_detail.qty_pr as qty_pr,

                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.unit_pr as unit_pr,

                            trans_po_detail.price as harga_aktual
                        '))
                      ->join('trans_po_detail', 'trans_po.nomor', '=', 'trans_po_detail.po_nomor')
                      ->join('ref_produk', 'trans_po_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->join('trans_pr_detail', function($join) {
                            $join->on('trans_po_detail.produk_kode', '=', 'trans_pr_detail.produk_kode')
                                  ->on('trans_pr_detail.pr_nomor', '=', 'trans_po.nomor_pr');
                      })
                      ->where(DB::raw('trans_po.nomor'), $po->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $po->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get();
        
        $data = [
            'record' => $po,
            'detail' => $records,
        ];

        // $content = view('report.purchase-order-btdk',$data);

        // $data = [
        //         'oriented'   => 'L',
        //         'paper'      => 'A4',
        //         'label_file' => 'PO BTDK '.$po->nomor,
        // ];

        // HTML2PDF($content, $data);
        $pdf = \PDF::loadView('report.purchase-order-btdk', $data);
            return $pdf->setOption('margin-bottom', 5)
            ->setOption('margin-top', 5)
            ->setOption('margin-left', 3)
            ->setOption('margin-right', 3)
            ->inline();
    }

    public function printPoStdk($id)
    {
        $po = TransPo::with('tmuk', 'tmuk.lsi')->find($id);
        // dd($po->tmuk->lsi->kode);
        $records = TransPo::has('detail')->select(DB::raw('
                            trans_po.nomor as nomor,
                            trans_po.nomor_pr as nomor_pr,

                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.l1_nm as l1_nm,
                            ref_produk.nama as nama,
                            ref_produk.kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            
                            ref_produk_setting.uom1_selling_cek as uom1_selling_cek,

                            ref_produk_lsi.lsi_kode as lsi_kode,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            ref_produk_lsi.curr_sale_prc as harga_estimasi,

                            trans_po_detail.id as id_detail,
                            trans_po_detail.qty_po as qty_po,
                            trans_po_detail.unit_po as unit_po,
                            trans_po_detail.qty_pr as qty_pr,

                            trans_pr_detail.qty_order as qty_order,
                            trans_pr_detail.unit_order as unit_order,
                            trans_pr_detail.qty_sell as qty_sell,
                            trans_pr_detail.unit_sell as unit_sell,
                            trans_pr_detail.unit_pr as unit_pr,

                            trans_po_detail.price as harga_aktual
                        '))
                      ->join('trans_po_detail', 'trans_po.nomor', '=', 'trans_po_detail.po_nomor')
                      ->join('ref_produk', 'trans_po_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->join('trans_pr_detail', function($join) {
                            $join->on('trans_po_detail.produk_kode', '=', 'trans_pr_detail.produk_kode')
                                  ->on('trans_pr_detail.pr_nomor', '=', 'trans_po.nomor_pr');
                      })
                      ->where(DB::raw('trans_po.nomor'), $po->nomor)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $po->tmuk->lsi->kode)->orderBy('l1_nm', 'ASC')->orderBy('nama', 'ASC')->get();
        
        $data = [
            'record' => $po,
            'escrow' => TransReduceEscrow::where('po_nomor', $po->nomor)->where('tmuk_kode', $po->tmuk_kode)->first(),
            'detail' => $records,
        ];

        // $content = view('report.purchase-order-stdk',$data);

        // $data = [
        //         'oriented'   => 'L',
        //         'paper'      => 'A4',
        //         'label_file' => 'PO STDK '.$po->nomor,
        // ];

        // HTML2PDF($content, $data);
        $pdf = \PDF::loadView('report.purchase-order-stdk', $data);
            return $pdf->setOption('margin-bottom', 5)
            ->setOption('margin-top', 5)
            ->setOption('margin-left', 3)
            ->setOption('margin-right', 3)
            ->inline();
    }
}
