<?php

namespace Lotte\Http\Controllers\Transaksi\Pembelian;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Transaksi\PyrRequest;
use Lotte\Http\Controllers\Controller;
//Libraries;
use DB;
use Datatables;
use Carbon\Carbon;
use Excel;

//model
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransPyrDetail;
use Lotte\Models\Trans\TransPyrPembayaran;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\SecTransaksi;

class DirectInvoiceController extends Controller
{
	protected $link = 'transaksi/pembelian/direct-invoice/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Payment Request (PyR)");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen");
		$this->setBreadcrumb(['Transaksi' => '#', 'Pembelian (PO)' => '#', 'Payment Request (PyR)' => '#']);
		$this->setTableStruct([
			[
       'data' => 'num',
       'name' => 'num',
       'label' => '#',
       'orderable' => false,
       'searchable' => false,
       'className' => "center aligned",
       'width' => '40px',
     ],
     /* --------------------------- */
     [
       'data' => 'tgl_buat',
       'name' => 'tgl_buat',
       'label' => 'Tanggal PYR',
       'searchable' => false,
       'sortable' => true,
       'className' => "center aligned",
     ],
     [
       'data' => 'nomor_pyr',
       'name' => 'nomor_pyr',
       'label' => 'Nomor PYR',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
     ],
     [
       'data' => 'tmuk',
       'name' => 'tmuk',
       'label' => 'TMUK',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
     ],
     [
       'data' => 'vendor',
       'name' => 'vendor',
       'label' => 'Vendor',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
     ],
     [
       'data' => 'tgl_jatuh_tempo',
       'name' => 'tgl_jatuh_tempo',
       'label' => 'Tanggal Jatuh Tempo',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
     ],
     [
       'data' => 'total',
       'name' => 'total',
       'label' => 'Jumlah Pembelian (Rp)',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
     ],
     [
       'data' => 'saldo_tmuk',
       'name' => 'saldo_tmuk',
       'label' => 'Saldo Escrow Saat Ini (Rp)',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
     ],
     [
       'data' => 'po_dari_pyr',
       'name' => 'po_dari_pyr',
       'label' => 'PYR',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
       'width' => '150px',
     ],
			// [
   //              'data' => 'action',
   //              'name' => 'action',
   //              'label' => 'Persetujuan',
   //              'orderable' => false,
   //              'searchable' => false,
   //              'className' => "center aligned",
   //              'width' => '40px',
   //          ]
     [
      'data' => 'grup',
      'name' => 'grup',
      'label' => 'Group',
      'searchable' => false,
      'sortable' => false,
      'className' => "center aligned",
      'width' => '80px',
    ],
  ]);

	}

	public function grid(Request $request)
	{
    $records = TransPyr::with('creator','groups')
    ->where(function ($query) {
        $query->where('group',null)
              ->orWhere(\DB::raw('trans_pyr.nomor_pyr'),\DB::raw('trans_pyr.group'));
    })
    ->where('status',0)
    ->orderBy('status', 'ASC')
    ->orderBy('tgl_buat', 'DESC')
    ->select('*');

    // dd($records->get());
		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
    if($tgl_buat = $request->tgl_buat){
      $dates = \Carbon\Carbon::parse($tgl_buat)->format('Y-m-d');
      $records->where(\DB::raw('DATE(tgl_buat)'),$dates);
    }
    if ($nomor_pyr = $request->nomor_pyr) {
      $records->where('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
    }
    if ($tmuk_kode = $request->tmuk_kode) {
      $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
    }
    if ($vendor_lokal = $request->vendor_lokal) {
      $records->where('vendor_lokal',$vendor_lokal);
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
      return $request->get('start');
    })
    ->addColumn('tmuk', function ($record) {
      return $record->tmuk->kode.' - '.$record->tmuk->nama;
    })
    ->editColumn('nomor_pyr', function ($record) {
        // dd($record->groups);z
      $string='';
      if(count($record->groups) > 0){
        $string .= '
        <div class="ui pyr button">'.$record->nomor_pyr.'</div>
        <div class="ui pyr popup top left transition hidden">
          <div class="ui one column grid">
            <div class="column">';
            foreach ($record->groups as $value) {
              $string .= $value->nomor_pyr.'<br>';
            }
        $string .='
            </div>
          </div>
        </div>
        ';
      }else{
        $string .= '
        <div class="ui pyr button">'.$record->nomor_pyr.'</div>
        <div class="ui pyr popup top left transition hidden">
          <div class="ui one column grid">
            <div class="column">'.$record->nomor_pyr.'
            </div>
          </div>
        </div>
        ';
      }
      return $string;
    })
    ->addColumn('total', function ($record) {
      $string='';
      if(count($record->groups) > 0){
        $sum = 0;
        foreach($record->groups as $value) {
          $sum += isset($value->detailPyr) ? $value->detailPyr->sum('price') : 0;
        }
        $string = $sum;
      }else{
        $string = isset($record->detailPyr) ? $record->detailPyr->sum('price') : 0;
      }
      return FormatNumber($string);
    })
    ->addColumn('saldo_tmuk', function ($record) {
      return FormatNumber($record->tmuk->saldo_escrow);
    })
    ->addColumn('vendor', function ($record) {
      if($record->vendor_lokal == 0){
        $tmuk = Tmuk::with('lsi')->where('kode', $record->tmuk->kode)->first();
        return $tmuk->lsi->nama;
      }else if($record->vendor_lokal == 100){
        $tmuk = Tmuk::with('lsi')->where('kode', $record->tmuk->kode)->first();
        return $tmuk->nama;
      }else{
        $vendor = VendorLokalTmuk::where('id', $record->vendor_lokal)->first();
        return isset($vendor) ? $vendor->nama : '-';
      }
    })
    ->addColumn('created_at', function ($record) {
      return $record->created_at->diffForHumans();
    })
    ->addColumn('po_dari_pyr', function ($record) {
      if ($record->status == 0) {
        $string = '
        <button type="button" class="ui mini blue icon edit button" data-content="Edit Pyr" data-id="'.$record->id.'"><i class="edit icon"></i>  Edit</button>
        ';
      }else{
        $string = '
        <button type="button" class="ui mini orange icon detail button" data-content="Detail Pyr" data-id="'.$record->id.'"><i class="eye icon"></i>  Detail</button>
        ';
      }

      return $string;
    })
            // ->addColumn('action', function ($record) {
            //     $btn = '';
            //     //Edit
            //     if ($record->status == 0) {
            //         $btn .= $this->makeButton([
            //             'type' => 'verify',
            //             'class' => 'green icon btn-update',
            //             'id'   => $record->id
            //         ]);
            //         // Delete
            //         $btn .= $this->makeButton([
            //             'type' => 'tolak',
            //             'class' => 'red icon btn-tolak',
            //             'id'   => $record->id
            //         ]);
            //     }else if($record->status == 1){
            //         $btn = '<i style="font-size: 15px; color: blue;" data-content="Sudah Diproses">Proses</i>';
            //     }else if($record->status == 2){
            //         $btn = '<i style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Lunas</i>';
            //     }else if($record->status == 3){
            //         $btn = '<i style="font-size: 15px; color: red;" data-content="Tidak Disetujui">Ditolak</i>';
            //     }

            //     return $btn;
            // })
    ->editColumn('grup',function($record){
      $vendors = '';
      if($record->vendor_lokal == 0){
        $tmuk = Tmuk::with('lsi')->where('kode', $record->tmuk->kode)->first();
        $vendors =  $tmuk->lsi->nama;
      }else{
        $vendor = VendorLokalTmuk::where('id', $record->vendor_lokal)->first();
        $vendors =  isset($vendor) ? $vendor->nama : '-';
      }
      $string = '';
      if($record->status==0){
        if(is_null($record->group)){
          $bayar = isset($record->detailPyr) ? $record->detailPyr->sum('price') : '0';
          $string .= '
          <div class="ui checkbox">
          <input type="checkbox" data-tmuk="'.$record->tmuk->kode.'" data-vendor="'.$vendors.'" class="group-pyr" value="'.$record->nomor_pyr.'" data-beli="'.$bayar.'">
          </div>';
        }
      }else if($record->status == 1){
        $string.= '<i style="font-size: 15px; color: blue;" data-content="Sudah Diproses">Proses</i>';
      }else if($record->status == 2){
        $string.= '<i style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Lunas</i>';
      }else if($record->status == 3){
        $string.= '<i style="font-size: 15px; color: red;" data-content="Tidak Disetujui">Ditolak</i>';
      }
      return $string;
    })
    ->make(true);
  }

  public function gridHistori(Request $request)
  {
    $records = TransPyr::with('creator','groups')
    ->where(function ($query) {
        $query->where('group',null)
              ->orWhere(\DB::raw('trans_pyr.nomor_pyr'),\DB::raw('trans_pyr.group'));
    })
    ->where('status','!=',0)
    ->orderBy('status', 'ASC')
    ->orderBy('updated_at', 'DESC')
    ->select('*');

    // dd($records->get());
    //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
    if($tgl_buat = $request->tgl_buat){
      $dates = \Carbon\Carbon::parse($tgl_buat)->format('Y-m-d');
      $records->where(\DB::raw('DATE(tgl_buat)'),$dates);
    }
    if ($nomor_pyr = $request->nomor_pyr) {
      $records->where(function($query) use ($nomor_pyr){
        $query->whereHas('groups', function ($qu) use ($nomor_pyr) {
            $qu->where('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
        })->orWhere('nomor_pyr', 'like', '%' . $nomor_pyr . '%');
      });
    }
    if ($tmuk_kode = $request->tmuk_kode) {
      $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
    }
    if ($vendor_lokal = $request->vendor_lokal) {
      $records->where('vendor_lokal',$vendor_lokal);
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
      return $request->get('start');
    })
    ->addColumn('tmuk', function ($record) {
      return $record->tmuk->kode.' - '.$record->tmuk->nama;
    })
    ->editColumn('nomor_pyr', function ($record) {
        // dd($record->groups);z
      $string='';
      if(count($record->groups) > 0){
        $string .= '
        <div class="ui pyr button">'.$record->nomor_pyr.'</div>
        <div class="ui pyr popup top left transition hidden">
          <div class="ui one column grid">
            <div class="column">';
            foreach ($record->groups as $value) {
              $string .= $value->nomor_pyr.'<br>';
            }
        $string .='
            </div>
          </div>
        </div>
        ';
      }else{
        $string .= '
        <div class="ui pyr button">'.$record->nomor_pyr.'</div>
        <div class="ui pyr popup top left transition hidden">
          <div class="ui one column grid">
            <div class="column">'.$record->nomor_pyr.'
            </div>
          </div>
        </div>
        ';
      }
      return $string;
    })
    ->addColumn('total', function ($record) {
      $string='';
      if(count($record->groups) > 0){
        $sum = 0;
        foreach($record->groups as $value) {
          $sum += isset($value->detailPyr) ? $value->detailPyr->sum('price') : 0;
        }
        $string = $sum;
      }else{
        $string = isset($record->detailPyr) ? $record->detailPyr->sum('price') : 0;
      }
      return FormatNumber($string);
    })
    ->addColumn('saldo_tmuk', function ($record) {
      return FormatNumber($record->tmuk->saldo_escrow);
    })
    ->addColumn('vendor', function ($record) {
      if($record->vendor_lokal == 0){
        $tmuk = Tmuk::with('lsi')->where('kode', $record->tmuk->kode)->first();
        return $tmuk->lsi->nama;
      }else if($record->vendor_lokal == 100){
        $tmuk = Tmuk::with('lsi')->where('kode', $record->tmuk->kode)->first();
        return $tmuk->nama;
      }else{
        $vendor = VendorLokalTmuk::where('id', $record->vendor_lokal)->first();
        return isset($vendor) ? $vendor->nama : '-';
      }
    })
    ->addColumn('created_at', function ($record) {
      return $record->created_at->diffForHumans();
    })
    ->addColumn('po_dari_pyr', function ($record) {
      if ($record->status == 0) {
        $string = '
        <button type="button" class="ui mini blue icon edit button" data-content="Edit Pyr" data-id="'.$record->id.'"><i class="edit icon"></i>  Edit</button>
        ';
      }else{
        $string = '
        <button type="button" class="ui mini orange icon detail button" data-content="Detail Pyr" data-id="'.$record->id.'"><i class="eye icon"></i>  Detail</button>
        ';
      }

      return $string;
    })
            // ->addColumn('action', function ($record) {
            //     $btn = '';
            //     //Edit
            //     if ($record->status == 0) {
            //         $btn .= $this->makeButton([
            //             'type' => 'verify',
            //             'class' => 'green icon btn-update',
            //             'id'   => $record->id
            //         ]);
            //         // Delete
            //         $btn .= $this->makeButton([
            //             'type' => 'tolak',
            //             'class' => 'red icon btn-tolak',
            //             'id'   => $record->id
            //         ]);
            //     }else if($record->status == 1){
            //         $btn = '<i style="font-size: 15px; color: blue;" data-content="Sudah Diproses">Proses</i>';
            //     }else if($record->status == 2){
            //         $btn = '<i style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Lunas</i>';
            //     }else if($record->status == 3){
            //         $btn = '<i style="font-size: 15px; color: red;" data-content="Tidak Disetujui">Ditolak</i>';
            //     }

            //     return $btn;
            // })
    ->editColumn('grup',function($record){
      $vendors = '';
      if($record->vendor_lokal == 0){
        $tmuk = Tmuk::with('lsi')->where('kode', $record->tmuk->kode)->first();
        $vendors =  $tmuk->lsi->nama;
      }else{
        $vendor = VendorLokalTmuk::where('id', $record->vendor_lokal)->first();
        $vendors =  isset($vendor) ? $vendor->nama : '-';
      }
      $string = '';
      if($record->status==0){
        if(is_null($record->group)){
          $bayar = isset($record->detailPyr) ? $record->detailPyr->sum('price') : '0';
          $string .= '
          <div class="ui checkbox">
          <input type="checkbox" data-tmuk="'.$record->tmuk->kode.'" data-vendor="'.$vendors.'" class="group-pyr" value="'.$record->nomor_pyr.'" data-beli="'.$bayar.'">
          </div>';
        }
      }else if($record->status == 1){
        $string.= '<i style="font-size: 15px; color: blue;" data-content="Sudah Diproses">Proses</i>';
      }else if($record->status == 2){
        $string.= '<i style="font-size: 15px; color: green;" data-content="Sudah Tervalidasi">Lunas</i>';
      }else if($record->status == 3){
        $string.= '<i style="font-size: 15px; color: red;" data-content="Tidak Disetujui">Ditolak</i>';
      }
      return $string;
    })
    ->make(true);
  }

  public function index()
  {
    return $this->render('modules.transaksi.pembelian.direct-invoice.index', ['mockup' => false]);
  }

  public function importpyrexcel()
  {
    return $this->render('modules.transaksi.pembelian.direct-invoice.import');
  }

    //Function list template Produk
  public function postImportPyrExcel(PyrRequest $request)
  {
    $data = $request->all();

    $kode = [];
    foreach (TransPyrDetail::get() as $row) {
      $kode[$row->produk_kode] = $row->id;
    }

    $pyr = new TransPyr;
    $pyr->fill($data);
    $pyr->tipe = "003";
    if ($pyr->save()) {
     if($request->hasFile('upload_data_produk')){
      $path = $request->file('upload_data_produk')->getRealPath();
      $data = Excel::load($path, function($reader) {})->get();
      $sukses = 0;
      $gagal = 0;
	            // dd($data->toArray());
      if(!empty($data) && $data->count()){
       foreach ($data->toArray() as $key => $value) {
	                    // dd($value);
         if(!is_null($value['produk_kode'])){
                            //cek
          $check = Produk::with('produksetting')->where('kode', $value['produk_kode'])->first();

          $tipe_barang = (isset($check->produksetting->tipe_barang_kode)) ? $check->produksetting->tipe_barang_kode : 0;

          try {
                                //check tipe barang
            if ($tipe_barang == '003') {
                                    // ins/updt
              $record                 = new TransPyrDetail;
              $record->pyr_id         = $pyr->id;
              $record->produk_kode    = $value['produk_kode'];
              $record->qty            = $value['qty'];
              $record->price          = $value['harga'];
              $record->created_by     = \Auth::user()->id;
              $record->created_at     = date('Y-m-d H:i:s');
              if($record->save()){
                $sukses++;
              }
            }else{
              $gagal++;
            }
          }catch (Exception $e) {
            $gagal++;
          }

        }
      }

      $total = TransPyrDetail::where('pyr_id', $pyr->id)->sum('price');
      if (!is_null($total)) {
        $pyr->total = $total;
        $pyr->save();
      }

      if ($sukses < $gagal) {
        TransPyrDetail::where('pyr_id', $pyr->id)->delete();
        TransPyr::find($pyr->id)->delete();
      }

      return response([
        'status' => true,
        'upload' => true,
        'sukses'  => $sukses,
        'gagal'  => $gagal
      ]);
    }
  }
}else{
 return back()->with('error','Please Check your file, Something is wrong there.');
}
}

public function create()
{
  return $this->render('modules.transaksi.pembelian.direct-invoice.create');
}

public function detail($id)
{
        // $pr = TransPr::find($id);
  $pyr = TransPyr::with('detailPyr','tmuk', 'tmuk.lsi')->find($id);

  $pyr = TransPyr::with('detailPyr','tmuk', 'tmuk.lsi')->find($id);

  if($pyr->vendor_lokal == 0){
    $tmuk = Tmuk::with('lsi')->where('kode', $pyr->tmuk->kode)->first();
    $vendor = $tmuk->lsi->nama;
  }else if($pyr->vendor_lokal == 100){
    $tmuk = Tmuk::with('lsi')->where('kode', $pyr->tmuk->kode)->first();
    $vendor = $tmuk->nama;
  }else{
    $lokal = VendorLokalTmuk::where('id', $pyr->vendor_lokal)->first();
    $vendor = isset($lokal) ? $lokal->nama : '-';
  }

  $data = [
    'record' => $pyr,
    'vendor' => $vendor,
  ];

  return $this->render('modules.transaksi.pembelian.direct-invoice.detail', $data);
}

public function detail2($id)
{
        // $pr = TransPr::find($id);
  $pyr = TransPyr::with('detailPyr','tmuk', 'tmuk.lsi')->find($id);

  $pyr = TransPyr::with('detailPyr','tmuk', 'tmuk.lsi')->find($id);

  if($pyr->vendor_lokal == 0){
    $tmuk = Tmuk::with('lsi')->where('kode', $pyr->tmuk->kode)->first();
    $vendor = $tmuk->lsi->nama;
  }else{
    $lokal = VendorLokalTmuk::where('id', $pyr->vendor_lokal)->first();
    $vendor = isset($lokal) ? $lokal->nama : '-';
  }

  $data = [
    'record' => $pyr,
    'vendor' => $vendor,
  ];

  return $this->render('modules.transaksi.pembelian.direct-invoice.detail2', $data);
}

public function edit($id)
{
    	// $pr = TransPr::find($id);
  $pyr = TransPyr::with('detailPyr','tmuk', 'tmuk.lsi')->find($id);

  if($pyr->vendor_lokal == 0){
    $tmuk = Tmuk::with('lsi')->where('kode', $pyr->tmuk->kode)->first();
    $vendor = $tmuk->lsi->nama;
  }else if($pyr->vendor_lokal == 100){
    $tmuk = Tmuk::with('lsi')->where('kode', $pyr->tmuk->kode)->first();
    $vendor = $tmuk->nama;
  }else{
    $lokal = VendorLokalTmuk::where('id', $pyr->vendor_lokal)->first();
    $vendor = isset($lokal) ? $lokal->nama : '-';
  }

  $data = [
    'record' => $pyr,
    'vendor' => $vendor,
  ];

  return $this->render('modules.transaksi.pembelian.direct-invoice.edit', $data);
}

public function update(Request $request, $id)
{
  $data = $request->all();
  $record = TransPyr::find($id);
  // $tgl_pyr = Carbon::parse($data['tgl_pyr'])->format('Y-m-d');

  if ($record) {
    //Perubahan Tanggal PYR
    // if ($record->tgl_buat != $tgl_pyr) {
    //   $record->tgl_buat = $tgl_pyr;
    // }

    $array = $data['detail_id'];

    foreach ($array as $key => $value) {
      $detail = TransPyrDetail::find($value);
      $detail->qty   = decimal_for_save($data['detail_qty'][$key]);
      $detail->price = decimal_for_save($data['detail_price'][$key]);
      $detail->save();
    }

    $record->total = $record->detailPyr->sum('price');
    if($record->save()){
      if ($record->tipe != '004') {
        //cek jurnal
        $jurnal = TransJurnal::where('idtrans', $record->idtransaksi)
                              ->orWhere('referensi', $record->nomor_pyr)
                              ->get();
        if ($jurnal) {
          foreach ($jurnal as $data) {
            //update jumlah
            $update_jurnal = TransJurnal::find($data->id);
            // if ($update_jurnal->tanggal != $record->tgl_buat) {
            //   $update_jurnal->tanggal = $record->tgl_buat.' 00:00:00';
            // }
            $update_jurnal->jumlah = $record->total;
            $update_jurnal->save();
          }
        }
      }else{
        //cek jurnal
        $jurnal = TransJurnal::where('idtrans', $record->idtransaksi)
                              ->orWhere('referensi', $record->nomor_pyr)
                              ->get();
        if ($jurnal) {
          $b_iklan = 0;
          $b_pemasaran_lainnya = 0;
          $komisi_bonus = 0;
          $b_gaji_lembur_thr = 0;
          $b_makan_karyawan = 0;
          $b_tunjangan_kesehatan = 0;
          $b_asuransi = 0;
          $b_air = 0;
          $b_listrik = 0;
          $b_telp_internet = 0;
          $b_keamanan = 0;
          $b_lainnya = 0;
          $b_distribusi = 0;
          $plastik = 0;
          $b_bensin = 0;
          $b_peralatan_toko = 0;
          $b_sewa_toko = 0;
          $b_lain_operasional = 0;
          $b_lain_nonoperasional = 0;

          foreach ($array as $key => $value) {
            $detail = TransPyrDetail::find($value);
            $jenis_barang = $detail->produks->produksetting->jenis_barang_kode;

            if ($jenis_barang == '003') { //Biaya Iklan
                $b_iklan += $detail->price;
            }else if ($jenis_barang == '005') { //Biaya Pemasaran Lainnya
                $b_pemasaran_lainnya += $detail->price;
            }else if ($jenis_barang == '006') { //Komisi & Bonus
                $komisi_bonus += $detail->price;
            }else if ($jenis_barang == '007') { //Biaya Gaji, Lembur & THR
                $b_gaji_lembur_thr += $detail->price;
            }else if ($jenis_barang == '008') { //Biaya Makan Karyawan
                $b_makan_karyawan += $detail->price;
            }else if ($jenis_barang == '009') { //Biaya Tunjangan Kesehatan
                $b_tunjangan_kesehatan += $detail->price;
            }else if ($jenis_barang == '010') { //Biaya Asuransi
                $b_asuransi += $detail->price;
            }else if ($jenis_barang == '011') { //Biaya Air
                $b_air += $detail->price;
            }else if ($jenis_barang == '012') { //Biaya Listrik
                $b_listrik += $detail->price;
            }else if ($jenis_barang == '013') { //Biaya Telp & Internet
                $b_telp_internet += $detail->price;
            }else if ($jenis_barang == '014') { //Biaya Keamanan
                $b_keamanan += $detail->price;
            }else if ($jenis_barang == '023') { //lainya (sumbangan,dll)
                $b_lainnya += $detail->price;
            }else if ($jenis_barang == '015') { //Biaya Distribusi (Adm)
                $b_distribusi += $detail->price;
            }else if ($jenis_barang == '016') { //Plastik Pembungkus
                $plastik += $detail->price;
            }else if ($jenis_barang == '018') { //Biaya Bensin
                $b_bensin += $detail->price;
            }else if ($jenis_barang == '022') { //Biaya Perlengkapan Toko
                $b_peralatan_toko += $detail->price;
            }else if ($jenis_barang == '019') { //Biaya Sewa Toko
                $b_sewa_toko += $detail->price;
            }else if ($jenis_barang == '020') { //Biaya Lain Operasional
                $b_lain_operasional += $detail->price;
            }else if ($jenis_barang == '021') { //Biaya Lain Non Operasional
                $b_lain_nonoperasional += $detail->price;
            }
          }

          foreach ($jurnal as $data) {
            //update jumlah
            $update_jurnal = TransJurnal::find($data->id);
            $coa = $update_jurnal->coa_kode;
            
            if($coa == '2.1.1.0'){
              $update_jurnal->jumlah = $record->total;
            }else if ($coa == '6.1.1.1') {
              $update_jurnal->jumlah = $b_iklan;
            }else if ($coa == '6.1.1.2') {
              $update_jurnal->jumlah = $b_pemasaran_lainnya;
            }else if ($coa == '6.1.1.3') {
              $update_jurnal->jumlah = $komisi_bonus;
            }else if ($coa == '6.2.1.1') {
              $update_jurnal->jumlah = $b_gaji_lembur_thr;
            }else if ($coa == '6.2.1.2') {
              $update_jurnal->jumlah = $b_makan_karyawan;
            }else if ($coa == '6.2.1.3') {
              $update_jurnal->jumlah = $b_tunjangan_kesehatan;
            }else if ($coa == '6.2.1.4') {
              $update_jurnal->jumlah = $b_asuransi;
            }else if ($coa == '6.2.2.2') {
              $update_jurnal->jumlah = $b_air;
            }else if ($coa == '6.2.2.3') {
              $update_jurnal->jumlah = $b_listrik;
            }else if ($coa == '6.2.2.4') {
              $update_jurnal->jumlah = $b_telp_internet;
            }else if ($coa == '6.2.2.5') {
              $update_jurnal->jumlah = $b_keamanan;
            }else if ($coa == '6.2.2.6') {
              $update_jurnal->jumlah = $b_lainnya;
            }else if ($coa == '6.2.2.10') {
              $update_jurnal->jumlah = $b_distribusi;
            }else if ($coa == '6.2.2.7') {
              $update_jurnal->jumlah = $plastik;
            }else if ($coa == '6.2.2.11') {
              $update_jurnal->jumlah = $b_bensin;
            }else if ($coa == '6.2.2.12') {
              $update_jurnal->jumlah = $b_peralatan_toko;
            }else if ($coa == '6.2.2.13') {
              $update_jurnal->jumlah = $b_sewa_toko;
            }else if ($coa == '6.2.2.14') {
              $update_jurnal->jumlah = $b_lain_operasional;
            }else if ($coa == '6.2.2.15') {
              $update_jurnal->jumlah = $b_lain_nonoperasional;
            }
            
            $update_jurnal->save();
          }
        }
      }

      return response([
        'status' => true,
        'data'  => $detail
      ]);
    }
  }else{
    return response([
      'status' => 'Data Pyr Tidak ditemukan.',
    ], 402);
  }
}
    //Update Status
public function onChangePopStatus($id)
{
  $updatestatus = TransPyr::find($id);
  $updatestatus->status = 1;
        // $updatestatus->save();

        //Insert to table trans_pyr_pembayaran
  if($updatestatus->save()){
    // $pyrpembayaran = new TransPyrPembayaran;
    // $pyrpembayaran->nomor_pyr = $updatestatus->nomor_pyr;
    // $pyrpembayaran->tmuk_kode = $updatestatus->tmuk_kode;
    //             // $pyrpembayaran->fill($request->all());
    // $pyrpembayaran->save();
    if ($updatestatus->status == 0) {
      $pyrpembayaran = new TransPyrPembayaran;
      $pyrpembayaran->nomor_pyr = $updatestatus->nomor_pyr;
      $pyrpembayaran->tmuk_kode = $updatestatus->tmuk_kode;
      $pyrpembayaran->save();
    }else{
      return response([
          'status' => 'Data sudah di proses sebelumnya, silahkan cek kembali.',
      ], 402);
    }
  }

  return response([
    'status' => true,
    'data'  => $updatestatus
  ]);
}

// public function onChangePopTolak($id)
// {
//   $updatestatus = TransPyr::find($id);
//   $updatestatus->status = 3;
//   $updatestatus->save();

//   return response([
//     'status' => true,
//     'data'  => $updatestatus
//   ]);
// }

public function groupProses(Request $request)
{
  $group = '';
  $count=0;
    // dd($request->all());
  foreach ($request->pyr as $key => $value) {
    $break = false;
    foreach ($value as $k => $v){
      $cek = TransPyr::where('nomor_pyr',$v)->first();
      if($cek->status==1 || $cek->status==3){
        $break = true;
        break;
      }
    }
    if($break==true){
      return response([
          'status' => 'Beberapa Data sudah di proses sebelumnya, silahkan cek kembali untuk melanjutkan proses.',
      ], 402);
      break;
    }
  }

  foreach ($request->pyr as $key => $value) {
    foreach ($value as $k => $v) {
      if($count==0){
        $group = $v;
      }
      $updatestatus = TransPyr::where('nomor_pyr',$v)->first();
      $updatestatus->status = $request->status;
      $updatestatus->group = $group;
      $updatestatus->save();
      $count++;

      if($request->status==1){
        $pyrpembayaran = new TransPyrPembayaran;
        $pyrpembayaran->nomor_pyr = $updatestatus->nomor_pyr;
        $pyrpembayaran->tmuk_kode = $updatestatus->tmuk_kode;
        $pyrpembayaran->group = $group;
        $pyrpembayaran->save();
      }else{
        if($request->status==3){
          $check_jurnal = TransJurnal::where('coa_kode', '2.1.1.0')
                              ->where('referensi', $v)
                              ->orWhere('idtrans', $updatestatus->id_transaksi)
                              ->first();
          // dd($check_jurnal);
          if ($check_jurnal) {
            // kamus jurnal
            $tahun_fiskal = $check_jurnal->tahun_fiskal_id;
            $coa_utang    = '2.1.1.0'; // Utang Usaha 
            $deskripsi    = '';
            $idtrans      = SecTransaksi::generate();
            $dtjurnal     = [];
            
            if ($check_jurnal->tipe == '001') {
              $deskripsi = 'PYR Utang Usaha Ditolak - Trade Lotte';
              $dtjurnal[] = [ // Utang Usaha
                'tahun_fiskal_id' => $tahun_fiskal,
                'idtrans'         => $idtrans,
                'tanggal'         => $updatestatus->tgl_buat,
                'tmuk_kode'       => $updatestatus->tmuk_kode,
                'referensi'       => $updatestatus->nomor_pyr,
                'deskripsi'       => $deskripsi,
                'coa_kode'        => $coa_utang,
                'jumlah'          => $updatestatus->total,
                'posisi'          => 'D',
                'flag'            => '+',
              ];
              $dtjurnal[] = [ // Persediaan Barang Dagang
                'tahun_fiskal_id' => $tahun_fiskal,
                'idtrans'         => $idtrans,
                'tanggal'         => $updatestatus->tgl_buat,
                'tmuk_kode'       => $updatestatus->tmuk_kode,
                'referensi'       => $updatestatus->nomor_pyr,
                'deskripsi'       => $deskripsi,
                'coa_kode'        => '1.1.4.0',
                'jumlah'          => $updatestatus->total,
                'posisi'          => 'K',
                'flag'            => '-',
              ];
            }else if($updatestatus->tipe == '002'){
              $deskripsi = 'PYR Utang Usaha Ditolak - Trade Non-Lotte';
              $dtjurnal[] = [ // Utang Usaha
                'tahun_fiskal_id' => $tahun_fiskal,
                'idtrans'         => $idtrans,
                'tanggal'         => $updatestatus->tgl_buat,
                'tmuk_kode'       => $updatestatus->tmuk_kode,
                'referensi'       => $updatestatus->nomor_pyr,
                'deskripsi'       => $deskripsi,
                'coa_kode'        => $coa_utang,
                'jumlah'          => $updatestatus->total,
                'posisi'          => 'D',
                'flag'            => '+',
              ];
              $dtjurnal[] = [ // Persediaan Barang Dagang
                'tahun_fiskal_id' => $tahun_fiskal,
                'idtrans'         => $idtrans,
                'tanggal'         => $updatestatus->tgl_buat,
                'tmuk_kode'       => $updatestatus->tmuk_kode,
                'referensi'       => $updatestatus->nomor_pyr,
                'deskripsi'       => $deskripsi,
                'coa_kode'        => '1.1.4.0',
                'jumlah'          => $updatestatus->total,
                'posisi'          => 'K',
                'flag'            => '-',
              ];
            }else if($updatestatus->tipe == '003'){
              $deskripsi = 'PYR Utang Usaha Ditolak - Non Trade Lotte';
              $dtjurnal[] = [ // Utang Usaha
                'tahun_fiskal_id' => $tahun_fiskal,
                'idtrans'         => $idtrans,
                'tanggal'         => $updatestatus->tgl_buat,
                'tmuk_kode'       => $updatestatus->tmuk_kode,
                'referensi'       => $updatestatus->nomor_pyr,
                'deskripsi'       => $deskripsi,
                'coa_kode'        => $coa_utang,
                'jumlah'          => $updatestatus->total,
                'posisi'          => 'D',
                'flag'            => '+',
              ];
              $dtjurnal[] = [ // Peralatan Toko
                'tahun_fiskal_id' => $tahun_fiskal,
                'idtrans'         => $idtrans,
                'tanggal'         => $updatestatus->tgl_buat,
                'tmuk_kode'       => $updatestatus->tmuk_kode,
                'referensi'       => $updatestatus->nomor_pyr,
                'deskripsi'       => $deskripsi,
                'coa_kode'        => '1.2.6.0',
                'jumlah'          => $updatestatus->total,
                'posisi'          => 'K',
                'flag'            => '-',
              ];
            }else if($updatestatus->tipe == '004'){
              $deskripsi = 'PYR Utang Usaha Ditolak - Non Trade Non-Lotte';
              
              $b_iklan = 0;
              $b_pemasaran_lainnya = 0;
              $komisi_bonus = 0;
              $b_gaji_lembur_thr = 0;
              $b_makan_karyawan = 0;
              $b_tunjangan_kesehatan = 0;
              $b_asuransi = 0;
              $b_air = 0;
              $b_listrik = 0;
              $b_telp_internet = 0;
              $b_keamanan = 0;
              $b_lainnya = 0;
              $b_distribusi = 0;
              $plastik = 0;
              $b_bensin = 0;
              $b_peralatan_toko = 0;
              $b_sewa_toko = 0;
              $b_lain_operasional = 0;
              $b_lain_nonoperasional = 0;
              
              foreach ($updatestatus->detailPyr as $key => $detail) {
                $jenis_barang = $detail->produks->produksetting->jenis_barang_kode;

                if ($jenis_barang == '003') { //Biaya Iklan
                    $b_iklan += $detail->price;
                }else if ($jenis_barang == '005') { //Biaya Pemasaran Lainnya
                    $b_pemasaran_lainnya += $detail->price;
                }else if ($jenis_barang == '006') { //Komisi & Bonus
                    $komisi_bonus += $detail->price;
                }else if ($jenis_barang == '007') { //Biaya Gaji, Lembur & THR
                    $b_gaji_lembur_thr += $detail->price;
                }else if ($jenis_barang == '008') { //Biaya Makan Karyawan
                    $b_makan_karyawan += $detail->price;
                }else if ($jenis_barang == '009') { //Biaya Tunjangan Kesehatan
                    $b_tunjangan_kesehatan += $detail->price;
                }else if ($jenis_barang == '010') { //Biaya Asuransi
                    $b_asuransi += $detail->price;
                }else if ($jenis_barang == '011') { //Biaya Air
                    $b_air += $detail->price;
                }else if ($jenis_barang == '012') { //Biaya Listrik
                    $b_listrik += $detail->price;
                }else if ($jenis_barang == '013') { //Biaya Telp & Internet
                    $b_telp_internet += $detail->price;
                }else if ($jenis_barang == '014') { //Biaya Keamanan
                    $b_keamanan += $detail->price;
                }else if ($jenis_barang == '023') { //lainya (sumbangan,dll)
                    $b_lainnya += $detail->price;
                }else if ($jenis_barang == '015') { //Biaya Distribusi (Adm)
                    $b_distribusi += $detail->price;
                }else if ($jenis_barang == '016') { //Plastik Pembungkus
                    $plastik += $detail->price;
                }else if ($jenis_barang == '018') { //Biaya Bensin
                    $b_bensin += $detail->price;
                }else if ($jenis_barang == '022') { //Biaya Perlengkapan Toko
                    $b_peralatan_toko += $detail->price;
                }else if ($jenis_barang == '019') { //Biaya Sewa Toko
                    $b_sewa_toko += $detail->price;
                }else if ($jenis_barang == '020') { //Biaya Lain Operasional
                    $b_lain_operasional += $detail->price;
                }else if ($jenis_barang == '021') { //Biaya Lain Non Operasional
                    $b_lain_nonoperasional += $detail->price;
                }
                // else if ($jenis_barang == '017') {
                //      $kode_coa = '?', //Promosi
                //      $desc = 'Pembayaran PyR - ?';
                // }
              }

              //Create Non Trade Non Lotte
              TransJurnal::insert([
                  [
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => $coa_utang, // Utang
                      'jumlah' => $updatestatus->total,
                      'posisi' => 'D',
                      'flag' => '+',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.1.1.1',        //Biaya Iklan
                      'jumlah' => $b_iklan,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.1.1.2',        //Biaya Pemasaran Lainnya
                      'jumlah' => $b_pemasaran_lainnya,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.1.1.3',        //Komisi & Bonus
                      'jumlah' => $komisi_bonus,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.1.1',        //Biaya Gaji, Lembur & THR
                      'jumlah' => $b_gaji_lembur_thr,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.1.2',        //Biaya Makan Karyawan
                      'jumlah' => $b_makan_karyawan,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.1.3',        //Biaya Tunjangan Kesehatan
                      'jumlah' => $b_tunjangan_kesehatan,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.1.4',        //Biaya Asuransi
                      'jumlah' => $b_asuransi,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.2',        //Biaya Air
                      'jumlah' => $b_air,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.3',        //Biaya Listrik
                      'jumlah' => $b_listrik,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.4',        //Biaya Telp & Internet
                      'jumlah' => $b_telp_internet,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.5',        //Biaya Keamanan
                      'jumlah' => $b_keamanan,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.6',        //Biaya Lainnya (sumbangan, dll)
                      'jumlah' => $b_lainnya,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.10',        //Biaya Distribusi (Adm)
                      'jumlah' => $b_distribusi,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.7',        //Plastik Pembungkus
                      'jumlah' => $plastik,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.11',        //Biaya Bensin
                      'jumlah' => $b_bensin,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.12',        //Biaya Peralatan Toko
                      'jumlah' => $b_peralatan_toko,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.13',        //Biaya Sewa Toko
                      'jumlah' => $b_sewa_toko,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.14',        //Biaya Lain Operasional
                      'jumlah' => $b_lain_operasional,
                      'posisi' => 'K',
                      'flag' => '-',
                  ],[
                      'idtrans' => $idtrans,
                      'tahun_fiskal_id' => $tahun_fiskal,
                      'tanggal' => $updatestatus->tgl_buat,
                      'tmuk_kode' => $updatestatus->tmuk_kode,
                      'referensi' => $updatestatus->nomor_pyr,
                      'deskripsi' => $deskripsi,
                      'coa_kode' => '6.2.2.15',        //Biaya Lain Non-Operasional
                      'jumlah' => $b_lain_nonoperasional,
                      'posisi' => 'K',
                      'flag' => '-',
                  ]
              ]);
            }

            TransJurnal::insert($dtjurnal); // insert jurnal

            $updatestatus->id_transaksi = $idtrans;
            $updatestatus->save();
          }
        }
      }
    }
  }

  return response([
    'status' => true
  ]);
}
}
