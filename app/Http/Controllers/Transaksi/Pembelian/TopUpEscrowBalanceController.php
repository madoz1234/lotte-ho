<?php

namespace Lotte\Http\Controllers\Transaksi\Pembelian;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransTopupEscrow;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\Trans\TransMemberPiutangDetail;
use Lotte\Models\Trans\TransMemberPiutang;
use Lotte\Models\SecTransaksi;
use Lotte\Models\Trans\TransLogAudit;

use Datatables;
use Carbon\Carbon;


class TopUpEscrowBalanceController extends Controller
{
	protected $link = 'transaksi/pembelian/topup-escrow-balance/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Top Up Escrow Balance");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("lounge");
		$this->setBreadcrumb(['Transaksi' => '#', 'Pembelian (PO)' => '#', 'Top Up Escrow Balance' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => 'No',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'tmuk_kode',
                'name' => 'tmuk_kode',
                'label' => 'TMUK',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'riwayat',
                'name' => 'riwayat',
                'label' => 'Riwayat Top Up',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'jenis',
                'name' => 'jenis',
                'label' => 'Jenis Top Up',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'tanggal',
                'name' => 'tanggal',
                'label' => 'Tanggal Top Up',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'jumlah',
                'name' => 'jumlah',
                'label' => 'Jumlah Top Up (Rp)',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'lampiran',
                'name' => 'lampiran',
                'label' => 'Lampiran',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Persetujuan',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);

	}

    public function grid(Request $request)
    {
        $record = TransTopupEscrow::with('creator')
                        ->orderBy('status','ASC')
                        ->orderBy('tanggal','desc')
                         ->select('*');
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $record->->sort();
        //     $record->orderBy('kode');
        // }

        //Filters
        if ($tanggal = $request->tanggal) {
            $record->where('tanggal', $tanggal);
        }
        if ($tmuk_kode = $request->tmuk_kode) {
            $record->where('tmuk_kode',$tmuk_kode);
        }

        return Datatables::of($record)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tmuk_kode', function ($record) {
                $kode_tmuk = $record->tmuk->kode;
                $nama_tmuk = $record->tmuk->nama;
                return $kode_tmuk.' - '.$nama_tmuk;
            })
            ->addColumn('jenis', function ($record) {
                $string = '';
                if ($record->jenis == 1) {
                    $string = 'Setoran Penjualan Tunai';
                }else if($record->jenis == 2){
                    $string = 'Setoran Penjualan Kredit';
                }else{
                    $string = 'Setoran Modal';
                }
                
                return $string;
            })
            ->addColumn('tanggal', function ($record) {
                return Carbon::createFromFormat('Y-m-d', $record->tanggal)->format('d/m/Y');
            })
            ->addColumn('jumlah', function ($record) {
                return 'Rp '. number_format($record->jumlah,2,',','.').'';
            })
            ->addColumn('lampiran', function ($record) {
                $string = '';
                if($record->lampiran!=null){
                    $string = '<a href="'.asset('storage').'/'.$record->lampiran.'" target="_blank"><img src="'.asset('storage').'/'.$record->lampiran.'" style="width: 100px; height: 100px; object-fit:cover"></a>';
                }
                return $string;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'verify',
                    'class' => 'green icon verify '.($record->status!=0  ? 'disabled' : ''),
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'tolak',
                    'class' => 'red icon tolak '.($record->status!=0  ? 'disabled' : ''),
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->addColumn('riwayat', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'detail',
                    'id'   => $record->tmuk_kode
                ]);

                return $btn;
            })
            ->addColumn('status', function ($record) {
                $status = '';
                if($record->status==0){
                    $status = "<div class='ui tiny orange label'>Menunggu Persetujuan</div>";
                }else{
                    if($record->status==1){
                        $status = "<div class='ui tiny green label'>Terverifikasi</div>";
                    }else{
                        $status = "<div class='ui tiny red label'>Ditolak</div>";
                    }
                }
                return $status;
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.transaksi.pembelian.topup-escrow-balance.index',['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.transaksi.pembelian.topup-escrow-balance.create');
    }

    public function edit($id)
    {
        return $this->render('modules.transaksi.pembelian.topup-escrow-balance.edit');
    }

    public function show($id)
    {
        $data = Tmuk::where('kode',$id)->first();
        $topup = TransTopupEscrow::where('tmuk_kode',$id)
                                    ->where('status','!=',0)
                                    ->orderBy('tanggal', 'DESC')
                                    ->get();
        return $this->render('modules.transaksi.pembelian.topup-escrow-balance.detail',['record'=>$data,'topup'=>$topup]);
    }

    public function verify($id)
    {
        $data = TransTopupEscrow::find($id);
        $data->status = 1;
        $data->save();

        // if($data->jenis==3){
        //     $saldo = $data->tmuk->saldo_deposit;
        //     $data->tmuk->saldo_deposit = $saldo + $data->jumlah;
        //     $data->tmuk->save();
        // }else{
            $saldo = $data->tmuk->saldo_escrow;
            $data->tmuk->saldo_escrow = $saldo + $data->jumlah;
            $data->tmuk->save();
        // }

        if($data->jenis==2){
            try {
                foreach ($data->detail as $value) {
                    $piutang = TransMemberPiutang::where('no_member',$value->member)->first();
                    if($piutang){
                        $piutangdetail = new TransMemberPiutangDetail;
                        $piutangdetail->id_trans_member_piutang = $piutang->id;
                        $piutangdetail->tanggal = date('Y-m-d');
                        $piutangdetail->hutang = $value->total * -1;
                        $piutangdetail->save();
                    }
                }
            } catch (Exception $e) {
                
            }
        }


        // insrt to jurnal
        $idtrans = SecTransaksi::generate(); // sementara
        $th_fiskal = TahunFiskal::getTahunFiskal();
        $tanggal = $data->tanggal.' 00:00:00';
        $tmuk_kode = $data->tmuk_kode;
        $escrow = $data->jumlah;

        if($data->jenis==3){
            $coa = '3.1.2.0';
            $desc = 'Top Up Escrow Balance - Setoran Modal';
        }else if($data->jenis==2){
            $coa = '1.1.1.1';
            $desc = 'Top Up Escrow Balance - Setoran Penjualan Kredit';
        }else{
            $coa = '1.1.1.1';
            $desc = 'Top Up Escrow Balance - Setoran Penjualan Tunai';
        }

        TransJurnal::insert([
            [
                'idtrans' => $idtrans,
                'tahun_fiskal_id' => $th_fiskal->id,
                'tanggal' => $tanggal,
                'tmuk_kode' => $tmuk_kode,
                'referensi' => '-',
                'deskripsi' => $desc,
                'coa_kode' => '1.1.2.1',
                'jumlah' => $escrow,
                'posisi' => 'D',
                'flag' => '+',
            ],
            [
                'idtrans' => $idtrans,
                'tahun_fiskal_id' => $th_fiskal->id,
                'tanggal' => $tanggal,
                'tmuk_kode' => $tmuk_kode,
                'referensi' => '-',
                'deskripsi' => $desc,
                'coa_kode' => $coa,
                'jumlah' => $escrow,
                'posisi' => 'K',
                'flag' => '-',
            ]
        ]);

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Top Up Ecsrow Balance ',
            'ref'               => '',
            'aksi'              => 'Disetujui',
            'amount'            => $escrow,
        ]);

        

        return response([
            'status' => true,
        ]);
    }

    public function tolak($id)
    {
        $data = TransTopupEscrow::find($id);
        $data->status = 2;
        $data->save();

        return response([
            'status' => true,
        ]);
    }


}
