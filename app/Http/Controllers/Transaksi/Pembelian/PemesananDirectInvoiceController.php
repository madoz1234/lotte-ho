<?php

namespace Lotte\Http\Controllers\Transaksi\Pembelian;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Transaksi\OpeningTokoRequest;
use Lotte\Http\Controllers\Controller;

//model
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukAssortment;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransOpening;
use Lotte\Models\Trans\TransDetailOpening;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransPr;
use Lotte\Models\Trans\TransPrDetail;
use DB;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

class PemesananDirectInvoiceController extends Controller
{
	protected $link = 'transaksi/pembelian/pemesanan-direct-invoice/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Pemesanan Direct Invoice");
		$this->setSubtitle("&nbsp");
		$this->setModalSize("longer");
		$this->setBreadcrumb(['Transaksi' => '#', 'Opening Toko' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			],
			/* --------------------------- */

			[
			    'data' => 'lsi',
			    'name' => 'lsi',
			    'label' => 'LSI',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'tmuk',
			    'name' => 'tmuk',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
                'data' => 'tgl_buat',
                'name' => 'tgl_buat',
                'label' => 'Tanggal Opening',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
			    'data' => 'tgl_kirim',
			    'name' => 'tgl_kirim',
			    'label' => 'Tanggal Kirim',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'po',
			    'name' => 'po',
			    'label' => 'PO',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'pyr',
			    'name' => 'pyr',
			    'label' => 'PYR',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '75px',
            ]
		]);
	}

	public function grid(Request $request)
	{
        $records = TransOpening::with('creator')
                         ->select('*');
		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if($tgl_buat = $request->tgl_buat){
            $dates = \Carbon\Carbon::parse($tgl_buat)->format('Y-m-d');
            $records->where(\DB::raw('DATE(tgl_buat)'),$dates);
        }
        // if ($tgl_buat = $request->tgl_buat) {
        //     $records->where('tgl_buat',$tgl_buat);
        // }
        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('lsi', function ($record) {
                return $record->tmuk->lsi->nama;
            })
            ->addColumn('tmuk', function ($record) {
                // return $record->tmuk.' - '.$record->tmuk->nama;
                return $record->tmuk_kode.' - '.$record->tmuk->nama;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('po', function ($record) {
                $string = '
							<button type="button" class="ui mini orange icon edit button" data-content="View Data" data-id="'.$record->id.'"><i class="edit icon"></i>View</button>
                            <a target="_blank" href="opening-toko/print-opening-toko/'.$record->id.'" class="ui mini default icon print button" data-content="Print PO"><i class="print icon"></i> Print</a>
		        		  ';
				return $string;
            })
            ->addColumn('pyr', function ($record) {
                $check = TransPyr::where('tmuk_kode', $record->tmuk_kode)->first();

                if ($check) {
                    $string = '
    					<button type="button" class="ui mini orange icon detail-pyr button" data-content="View Data" data-id="'.$record->id.'"><i class="edit icon"></i>View</button>';
                }else{
                    $string = '
                        <a href="'.asset('template\produk-pyr.xls').'" class="ui mini grey button" target="_blank"><i class="download icon icon"></i>Template</a>
                        <button type="button" class="ui mini importpyrexcel blue icon button" data-content="Upload Data" data-id="'.$record->id.'"><i class="upload icon"></i>Upload</button>';
                }

				return $string;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                $pr = TransPr::where('nomor', $record->nomor_pr)->where('status', '0')->first();

                if(!is_null($pr)){
                    // Delete
                    $btn .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id
                    ]);
                }else{
                    // Delete
                    $btn .= $this->makeButton([
                        'disabled' => 'disabled',
                    	'type' => 'delete',
                    	'id'   => $record->id
                    ]);
                }

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.transaksi.pembelian.pemesanan-direct-invoice.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.transaksi.pembelian.pemesanan-direct-invoice.create');
    }

    public function edit($id)
    {
        $data = [
            'record' => TransOpening::find($id),
        ];
        return $this->render('modules.transaksi.pembelian.pemesanan-direct-invoice.detail-po', $data);
    }

    public function detailPyr($id)
    {

        $opening = TransOpening::find($id);
        
        $pyr = TransPyr::with('tmuk', 'tmuk.lsi')->where('tmuk_kode', $opening->tmuk_kode)
                                                 ->where('tgl_buat', $opening->tgl_buat)
                                                 ->where('tipe', '003')
                                                 ->first();

        $data = [
            'record' => $pyr,
        ];

        return $this->render('modules.transaksi.pembelian.pemesanan-direct-invoice.edit', $data);
    }

    public function importexcel()
    {
        return $this->render('modules.transaksi.pembelian.pemesanan-direct-invoice.import');
    }

    public function onChangePopTmuk($id=null)
    {
        $data = Tmuk::find($id);
        // dd($data);
        return response()->json([
            'kode' => $data->kode,
        ]);
    }

    //Function list template Produk
    public function postImportExcel(OpeningTokoRequest $request)
	{
        $data = $request->all();
        // dd($data);

        $tmuk = Tmuk::find($data['tmuk_id']);
        $data['tmuk_kode'] = $tmuk->kode;

        //cek duplikasi tmuk
        $check = TransOpening::where('tmuk_kode', $tmuk->kode)->first();
        if($check){
            return response([
                'status' => 'Data "'.$tmuk->kode.' - '.$tmuk->nama.'" sudah ada sebelumnya di database',
            ], 402);
        }

        $opening = new TransOpening;
        $opening->fill($data);
        if ($opening->save()) {
			if($request->hasFile('upload_data_produk')){
				$path = $request->file('upload_data_produk')->getRealPath();
	            $data = Excel::load($path, function($reader) {})->get();
                $sukses = 0;
                $gagal = 0;

	            // dd($data->toArray());
	            if(!empty($data) && $data->count()){
	                foreach ($data->toArray() as $key => $value) {
	                    // dd($value);
                        //jika data tidak boleh kosong
	                    if(!is_null($value['produk_kode'])){ 
                            //check status produk per lsi selain normal dan order stop tidak bisa tersimpan
                            $checkStatus = ProdukLsi::where('lsi_kode', $tmuk->lsi->kode)->where('produk_kode', $value['produk_kode'])
                                                    ->where(function($q) {
                                                         $q->where('status', 'NORMAL')
                                                           ->orWhere('status', 'ORDER STOP');
                                                     })->first();
                            if($checkStatus){
                                // ins/updt
                                try {
                                    $pa = ProdukAssortment::where('produk_kode', $value['produk_kode'])->where('assortment_type_id', $tmuk->assortment->id)->first();
                                    // dd($pa);
                                    if ($pa) {
                                        $record              = new TransDetailOpening;
                                        $record->opening_id  = $opening->id;
                                        $record->produk_kode = $value['produk_kode'];
                                        $record->qty_po      = $value['qty'];
                                        $record->created_by  = \Auth::user()->id;
                                        $record->created_at  = date('Y-m-d H:i:s');
                                        if($record->save()){
                                            $sukses++;
                                        }
                                    }else{
                                        $gagal++;
                                    }
                                }catch (Exception $e) {
                                    $gagal++;
                                }
                            }else{
                                $gagal++;
                            }

	                    }
	                }

                    if ($sukses > $gagal) {
    	                //masukin data ke trans PR dan detail PR
    	                $op = TransOpening::with('tmuk', 'tmuk.lsi')->find($opening->id);
    			        $records = TransOpening::has('detailOpening')->select(DB::raw('
                                            ref_produk.nama as nama,

                                            ref_produk_lsi.stok_gmd as stok_gmd,
                                            ref_produk_lsi.curr_sale_prc as curr_sale_prc,
                                            
                                            ref_produk_setting.uom1_barcode as uom1_barcode,
                                            ref_produk_setting.uom1_satuan as uom1_satuan,
                                            ref_produk_setting.uom2_satuan as uom2_satuan,
                                            ref_produk_setting.uom2_conversion as uom2_conversion,
                                            
    			                            trans_detail_opening.produk_kode as produk_kode,
                                            trans_detail_opening.qty_po as qty_po,
    			                            (trans_detail_opening.qty_po*ref_produk_lsi.curr_sale_prc)as total_harga'
    			                        ))
    			                      ->join('trans_detail_opening', 'trans_opening.id', '=', 'trans_detail_opening.opening_id')
    			                      ->join('ref_produk', 'trans_detail_opening.produk_kode', '=', 'ref_produk.kode')
    			                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
    			                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
    			                      ->where(DB::raw('trans_opening.id'), $op->id)
    			                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $op->tmuk->lsi->kode)->get();
    			                      // ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->get()->toArray();
    			                        $sub_total = $records->sum('total_harga');

    			        //input ke trans pr
    			        $pr = new TransPr;
            			$pr->nomor  	 = $opening->nomor_pr;
            			$pr->tmuk_kode 	 = $opening->tmuk_kode;
            			$pr->tgl_buat  	 = $opening->tgl_buat;
            			$pr->tgl_kirim 	 = $opening->tgl_kirim;
            			$pr->total     	 = $sub_total;
            			$pr->status    	 = 0;
            			$pr->created_by  = \Auth::user()->id;
                        $pr->created_at  = date('Y-m-d H:i:s');
            			if($pr->save()){
            				foreach ($records as $key => $value) {
                                //konversi
                                $qty_pr = $value['qty_po']/$value['uom2_conversion'];
                                if (is_float($qty_pr)) {
                                    $qty_pr = ceil($qty_pr);
                                    $qty_aktual = $qty_pr * $value['uom2_conversion'];
                                }else{
                                    $qty_aktual = $qty_pr;
                                }

                                $pr_detail              = new TransPrDetail;
                                $pr_detail->pr_nomor    = $pr->nomor;
                                $pr_detail->produk_kode = $value['produk_kode'];
                                $pr_detail->qty_order   = $value['qty_po'];
                                $pr_detail->unit_order  = $value['uom1_satuan'];
                                $pr_detail->unit_order_price    = $value['curr_sale_prc'];
                                $pr_detail->qty_sell    = $value['uom2_conversion'];
                                $pr_detail->unit_sell   = $value['uom1_satuan'];
                                $pr_detail->qty_pr      = $qty_pr;
                                $pr_detail->unit_pr     = $value['uom2_satuan'];
                                $pr_detail->total_price = $value['total_harga'];
                                $pr_detail->qty_pick    = null;
                                $pr_detail->qty_aktual  = $qty_aktual;
                                $pr_detail->created_by  = \Auth::user()->id;
                                $pr_detail->created_at  = date('Y-m-d H:i:s');
    							$pr_detail->save();
    		                }
            			}
                    }else{
                        //jika gagal lebih banyak dari data sukses terupload maka data di database di hapus
                        TransDetailOpening::where('opening_id', $opening->id)->delete();
                        TransOpening::find($opening->id)->delete();
                    }

                    return response([
                        'status' => true,
                        'upload' => true,
                        'sukses'  => $sukses,
                        'gagal'  => $gagal
                    ]);

	            }
			}
        }else{
			return back()->with('error','Please Check your file, Something is wrong there.');
		}
	}

    //untuk form modal upload data pyr
    public function importpyrexcel($id)
    {
        $data = [
            'record' => TransOpening::find($id),
        ];

        return $this->render('modules.transaksi.opening-toko.importpyr', $data);
    }

    public function printOpeningToko($id)
    {
        // $pr = TransPr::find($id);
        $pr = TransOpening::with('tmuk', 'tmuk.lsi')->find($id);

        // dd($pr->tmuk->lsi->kode);
        $records = TransOpening::has('detailOpening')->select(DB::raw('
                            trans_detail_opening.produk_kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            ref_produk_lsi.stok_gmd as stok_gmd,
                            ref_produk_setting.uom1_satuan as uom1_satuan,
                            trans_detail_opening.qty_po as qty_po,
                            ref_produk_lsi.curr_sale_prc as curr_sale_prc,
                            (trans_detail_opening.qty_po*ref_produk_lsi.curr_sale_prc)as total_harga'
                        ))
                      ->join('trans_detail_opening', 'trans_opening.id', '=', 'trans_detail_opening.opening_id')
                      ->join('ref_produk', 'trans_detail_opening.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->where(DB::raw('trans_opening.id'), $pr->id)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->get();
                      // ->where(DB::raw('ref_produk_lsi.lsi_kode'), $pr->tmuk->lsi->kode)->get()->toArray();
                        $sub_total = $records->sum('total_harga');
        // dd($records);

        $data = [
            'record' => TransOpening::find($id),
            'detail' => $records,
            'sub_total' => $sub_total,
        ];
            $content = view('report.opening-toko', $data);

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Picking',
            ];

            HTML2PDF($content, $data);
    }

    public function destroy($id)
    {
        $opening = TransOpening::find($id);
        if($opening){
            $pr = TransPr::where('nomor', $opening->nomor_pr)->where('status', '0')->first();
            if (!is_null($pr)) {
                $detailpr = TransPrDetail::where('pr_nomor', $pr->nomor)->delete();
                if($detailpr){
                    $pr->delete();
                    if($opening->delete()){
                        return response([
                            'status' => true,
                        ]);
                    }
                }else{
                    return response([
                        'status' => false,
                    ], 402);
                }
            }else{
                return response([
                    'status' => false,
                ], 402);
            }
        }
    }

}
