<?php

namespace Lotte\Http\Controllers\Transaksi\KonfirmasiRetur;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\ProdukSettingRequest;
use Lotte\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

//Libraries
use DB;
use Excel;
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Trans\TransRetur;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Trans\TransJurnal;
use Lotte\Models\SecTransaksi;

class KonfirmasiReturController extends Controller
{
	protected $link = 'transaksi/konfirmasiretur/konfirmasi-retur/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Konfirmasi Retur");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen");
		$this->setBreadcrumb(['Menu Transaksi' => '#', 'Konfirmasi Retur' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */

			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal RR',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '100px',
			],
			[
			    'data' => 'nomor_po',
			    'name' => 'nomor_po',
			    'label' => 'Nomor PO',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '100px',
			],
			[
			    'data' => 'nomor_retur',
			    'name' => 'nomor_retur',
			    'label' => 'Nomor RR',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '100px',
			],
			[
			    'data' => 'tmuk',
			    'name' => 'tmuk',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '100px',
			],
			[
			    'data' => 'nilai_retur',
			    'name' => 'nilai_retur',
			    'label' => 'Nilai Retur (Rp)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'persetujuan',
			    'name' => 'persetujuan',
			    'label' => 'Persetujuan',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

    public function grid(Request $request)
    {
        $records = TransRetur::orderBy('status', 'ASC');
        // ->select('*');

        //Filters
        if($created_at = $request->created_at){
            $dates = \Carbon\Carbon::parse($created_at)->format('Y-m-d');
            $records->where(\DB::raw('DATE(created_at)'),$dates);
        }

        if ($nomor_retur = $request->nomor_retur) {
            $records->where('nomor_retur', 'like', '%' . $nomor_retur . '%');
        }

        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode', 'like', '%' . $tmuk_kode . '%');
        }

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('lsi', function ($record) {
          return $record->vendor_lokal_id == null ? $record->tmuk->lsi->nama : $record->vendor->nama ;
        })
        ->addColumn('tmuk', function ($record) {
            return $record->tmuk_kode.' - '.$record->tmuk->nama;
        })
        ->addColumn('nomor_po', function ($record) {
            return $record->po->nomor;
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->format('Y-m-d');
        })
        ->addColumn('nilai_retur', function ($record) {
            $string = isset($record->retur_detail) ? $record->retur_detail->sum('harga_total') : '0';
                return FormatNumber($string);
        })
        ->addColumn('persetujuan', function ($record) {
            // $string = '<button type="button" class="ui mini primary icon edit button" data-content="Buat Persetujuan" data-id="'.$record->id.'"><i class="add icon"></i> Buat</button>';
            $string = '';
                if ($record->status == 0) {
                    $string = '<button type="button" class="ui mini primary icon edit button" data-content="Buat Persetujuan" data-id="'.$record->id.'"><i class="add icon"></i> Buat</button>';
                }else{
                    $string = '<b style="font-size: 15px; color: green;" data-content="Sudah Tersetujui">Tersetujui</b><br><i style="font-size: 12px; color: green;" data-content="Sudah Tersetujui">'.$record->updated_at.'</i>';
                }
			return $string;
        })

        ->make(true);
    }

    public function index()
    {
        return $this->render('modules.transaksi.konfirmasi-retur.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.transaksi.konfirmasi-retur.create');
    }

    public function edit($id)
    {
    	// $record = TransRetur::find($id);
    	$po = TransRetur::with('tmuk', 'tmuk.lsi')->find($id);
        // dd($po->tmuk->lsi->kode);
        $records = TransRetur::has('retur_detail')->select(DB::raw('
                            trans_retur.id as id,
                            trans_retur_detail.catatan as catatan,
                            ref_produk.bumun_nm as bumun_nm,
                            ref_produk.kode as produk_kode,
                            ref_produk_setting.uom1_barcode as uom1_barcode,
                            ref_produk.nama as nama,
                            trans_retur_detail.qty as qty,
                            ref_produk_setting.uom1_satuan as uom1_satuan,
                            trans_retur_detail.harga_satuan as harga_satuan,
                            trans_retur_detail.harga_total as harga_total,
                            trans_retur_detail.reason as reason
                        '))
                      ->join('trans_retur_detail', 'trans_retur.id', '=', 'trans_retur_detail.retur_id')
                      ->join('ref_produk', 'trans_retur_detail.produk_kode', '=', 'ref_produk.kode')
                      ->join('ref_produk_setting', 'ref_produk.kode', '=', 'ref_produk_setting.produk_kode')
                      ->join('ref_produk_lsi', 'ref_produk.kode', '=', 'ref_produk_lsi.produk_kode')
                      ->where(DB::raw('trans_retur.id'), $po->id)
                      ->where(DB::raw('ref_produk_lsi.lsi_kode'), $po->tmuk->lsi->kode)->get();

        $data = [
            'record' => $po,
            'detail' => $records,
        ];

        return $this->render('modules.transaksi.konfirmasi-retur.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $updateretur = TransRetur::find($id);
        $updateretur->status = 1;
        if($updateretur->save()){
            $tmuk = Tmuk::where('kode', $request->tmuk_kode)->first();
            $tmuk->saldo_deposit = $tmuk->saldo_deposit + $updateretur->retur_detail->sum('harga_total');
            $tmuk->save();
        }

        // insrt to jurnal
        $th_fiskal = TahunFiskal::getTahunFiskal();
        $idtrans = SecTransaksi::generate(); // sementara
        $tanggal = date('Y-m-d H:i:s');
        $tmuk_kode = $tmuk->kode;
        $deposit = $tmuk->saldo_deposit;
        TransJurnal::insert([
            [
                'idtrans' => $idtrans,
                'tahun_fiskal_id' => $th_fiskal->id,
                'tanggal' => $tanggal,
                'tmuk_kode' => $tmuk_kode,
                'referensi' => '-',
                'deskripsi' => 'Konfirmasi Retur',
                'coa_kode' => '1.1.6.1', // deposit
                'jumlah' => $deposit,
                'posisi' => 'D',
                'flag' => '+',
            ],
            [
                'idtrans' => $idtrans,
                'tahun_fiskal_id' => $th_fiskal->id,
                'tanggal' => $tanggal,
                'tmuk_kode' => $tmuk_kode,
                'referensi' => '-',
                'deskripsi' => 'Konfirmasi Retur',
                'coa_kode' => '1.1.4.0', // persediaan barang dagang
                'jumlah' => $deposit,
                'posisi' => 'K',
                'flag' => '-',
            ]
        ]);

        return response([
            'status' => true,
            'data'  => $updateretur
        ]);
    }

    public function printKonfirmasiReturn()
    {
            $content = view('report.konfirmasi-return');

            $data = [
                    'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Picking',
            ];

            HTML2PDF($content, $data);
    }
}
