<?php

namespace Lotte\Http\Controllers\Transaksi\Member;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransMemberPiutang;
use Lotte\Models\Master\MemberCard;
use Lotte\Models\Trans\TransRekapMember;
use Datatables;


class PiutangController extends Controller
{
	protected $link = 'transaksi/member/piutang/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Piutang");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Transaksi' => '#', 'Member' => '#', 'Piutang' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'debtor_no',
			    'name' => 'debtor_no',
			    'label' => 'TMUK',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'nomor_member',
			    'name' => 'nomor_member',
			    'label' => 'No Member Card',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'branch_ref',
			    'name' => 'branch_ref',
			    'label' => 'Jenis Kustomer',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'br_name',
			    'name' => 'br_name',
			    'label' => 'Nama Member',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'piutang',
			    'name' => 'piutang',
			    'label' => 'Jumlah Piutang',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '100px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
    {
        $record = TransRekapMember::with('tmuk')
                         ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $record->->sort();
            $record->orderBy('created_at');
        }

        //Filters
        
        if ($tmuk_kode = $request->tmuk_kode) {
            $record->where('debtor_no',$tmuk_kode);
        }

        if ($nomor = $request->nomor) {
            $record->where('nomor_member','ilike','%'.$nomor.'%');
        }

        if ($nama = $request->nama) {
            $record->where('br_name','ilike','%'.$nama.'%');
        }

        return Datatables::of($record)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('debtor_no', function ($record) {
                if($record->tmuk){
	                $kode_tmuk = $record->tmuk->kode;
	                $nama_tmuk = $record->tmuk->nama;
	                return $kode_tmuk.' - '.$nama_tmuk;
            	}else{
            		return '';
            	}
            })
            ->addColumn('action', function ($record) {
                $btn = '<button type="button" data-content="Detail Data" data-tmuk="'.$record->debtor_no.'" data-nomor="'.$record->nomor_member.'" class="ui mini blue icon detail-custom button"><i class="eye icon"></i></button>';
                //Edit
                // $btn .= $this->makeButton([
                //     'type' => 'detail',
                //     'id'   => $record->nomor_member
                // ]);

                return $btn;
            })
            ->addColumn('piutang', function ($record) {
            	// if($record->point){
	            //     return $record->piutang->detail->sum('hutang');
            	// }else{
            	// 	return 0;
            	// }

            	$rec = TransMemberPiutang::where('tmuk_kode',$record->debtor_no)
    								->where('no_member',$record->nomor_member)
    								->first();
            	if($rec){
	                return $rec->detail->sum('hutang');
            	}else{
            		return 0;
            	}
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.transaksi.member.piutang.index',['mockup'=>false]);
    }

    public function show($tmuk,$nomor)
    {
    	$record = TransMemberPiutang::where('tmuk_kode',$tmuk)
    								->where('no_member',$nomor)
    								->first();
    	$tmuk = TransRekapMember::where('debtor_no',$tmuk)
    								->where('nomor_member',$nomor)
    								->first();
        return $this->render('modules.transaksi.member.piutang.detail',['record'=>$record,'tmuk'=>$tmuk]);
    }
}
