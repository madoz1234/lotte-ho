<?php

namespace Lotte\Http\Controllers\Transaksi\Member;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransMemberPoint;
use Lotte\Models\Trans\TransRekapMember;
use Lotte\Models\Master\MemberCard;
use Datatables;


class PointController extends Controller
{
	protected $link = 'transaksi/member/point/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Point");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Transaksi' => '#', 'Member' => '#', 'Point' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'debtor_no',
			    'name' => 'debtor_no',
			    'label' => 'TMUK',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'nomor_member',
			    'name' => 'nomor_member',
			    'label' => 'No Member Card',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'branch_ref',
			    'name' => 'branch_ref',
			    'label' => 'Jenis Kustomer',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'br_name',
			    'name' => 'br_name',
			    'label' => 'Nama Member',
			    'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'point',
			    'name' => 'point',
			    'label' => 'Saldo Point',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '100px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
    {
        $record = TransRekapMember::with('tmuk')
                         ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $record->->sort();
            $record->orderBy('created_at');
        }

        //Filters
        
        if ($tmuk_kode = $request->tmuk_kode) {
            $record->where('debtor_no',$tmuk_kode);
        }

        if ($nomor = $request->nomor) {
            $record->where('nomor_member','ilike','%'.$nomor.'%');
        }

        if ($nama = $request->nama) {
            $record->where('br_name','ilike','%'.$nama.'%');
        }

        return Datatables::of($record)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('debtor_no', function ($record) {
            	if($record->tmuk){
	                $kode_tmuk = $record->tmuk->kode;
	                $nama_tmuk = $record->tmuk->nama;
	                return $kode_tmuk.' - '.$nama_tmuk;
            	}else{
            		return '';
            	}
            	// return $record->tmukkode->nama;
            })
            ->addColumn('action', function ($record) {
                $btn = '<button type="button" data-content="Detail Data" data-tmuk="'.$record->debtor_no.'" data-nomor="'.$record->nomor_member.'" class="ui mini blue icon detail-custom button"><i class="eye icon"></i></button>';
                //Edit
                // $btn .= $this->makeButton([
                //     'type' => 'detail',
                //     'id'   => $record->nomor_member
                // ]);

                return $btn;
            })
            ->addColumn('point', function ($record) {
            	$rec = TransMemberPoint::where('tmuk_kode',$record->debtor_no)
    								->where('no_member',$record->nomor_member)
    								->first();
            	if($rec){
	                return $rec->detail->sum('point');
            	}else{
            		return 0;
            	}
            	return '';
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.transaksi.member.point.index',['mockup'=>false]);
    }

    public function show($tmuk,$nomor)
    {
    	$record = TransMemberPoint::where('tmuk_kode',$tmuk)
    								->where('no_member',$nomor)
    								->first();
    	$tmuk = TransRekapMember::where('debtor_no',$tmuk)
    								->where('nomor_member',$nomor)
    								->first();
        return $this->render('modules.transaksi.member.point.detail',['record'=>$record,'tmuk'=>$tmuk]);
    }
}
