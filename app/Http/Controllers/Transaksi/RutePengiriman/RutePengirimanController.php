<?php

namespace Lotte\Http\Controllers\Transaksi\RutePengiriman;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Trans\TransSuratJalan;
use Datatables;


class RutePengirimanController extends Controller
{
	protected $link = 'transaksi/rutepengiriman/rute-pengiriman/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Transaksi Rute Pengiriman");
		$this->setSubtitle("&nbsp");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Menu Transaksi' => '#', 'Rute Pengiriman' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */

			[
			    'data' => 'nomor',
			    'name' => 'nomor',
			    'label' => 'No Surat Jalan',
			    'searchable' => false,
			    'sortable' => false,
			],
			// [
			//     'data' => 'tujuan',
			//     'name' => 'tujuan',
			//     'label' => 'Tujuan',
			//     'searchable' => false,
			//     'sortable' => true,
			// ],
			[
			    'data' => 'status',
			    'name' => 'status',
			    'label' => 'Status',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TransSuratJalan::with('creator')
						 ->select('*');

        // $record = Kontainer::select('*');
        // dd($record);

		// Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('created_at');
        }

        //Filters
        if ($surat = $request->surat) {
            $records->where('nomor', 'ilike', '%' . $surat . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
			// ->addColumn('tujuan',function($record){
			// 	return 'bandung';
			// })
			->addColumn('status',function($record){
				$terkirim = false;
				foreach ($record->muatan->detail as $value) {
					if($value->status==0){
						$terkirim = false;
						break;
					}else{
						$terkirim = true;
					}
				}
				$string = 'On Proses';
				if($terkirim==true){
					$string = 'Terkirim';
				}
				return $string;
			})
            ->editColumn('action', function ($record) {

            	$btn='';
                $btn .= $this->makeButton([
                	'type' => 'detail',
                	'id'   => $record->id
                ]);
                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.transaksi.rute-pengiriman.index',['mockup'=>false]);
    }

    public function create()
    {
        return $this->render('modules.transaksi.rute-pengiriman.create');
    }

    public function edit($id)
    {
        return $this->render('modules.transaksi.rute-pengiriman.edit');
    }

    public function show($id)
    {
    	$record = TransSuratJalan::find($id);
        return $this->render('modules.transaksi.rute-pengiriman.detail',['record'=>$record]);
    }

    public function printRute($id)
    {
    	$record = TransSuratJalan::find($id);
    	$content = view('report.rute-pengiriman',['record'=>$record]);

		$data = [
			'oriented'   => 'P',
			'paper'      => 'A4',
			'label_file' => 'Rute Pengiriman '.$record->nomor,
		];

        // return $data;

		return HTML2PDF($content, $data);
    }
}
