<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\JenisAssortmentRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use DB;
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\JenisAssortment;
use Lotte\Models\Master\AssortmentTypeProduk;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\Log\LogJenisAssortment;
use Lotte\Models\Trans\TransLogAudit;

class JenisAssortmentController extends Controller
{
	protected $link = 'utama/produk/jenis-assortment/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Assortment Type");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Assortment Type' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Assortment Type',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		// $records = JenisAssortment::with('creator')
		// 				 ->select('*')->orderBy('nama', 'asc');

        $records = JenisAssortment::with('creator')
                        ->select('*')
                        ->orderByRaw('LENGTH(nama)')
                        ->orderBy('nama', 'asc');
		// Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     $records->sort();
        //     $records->orderBy('nama', 'DESC');
        // }
        // if(request()->order[0]['column'] == '0'){
        //   $record->orderBy('id','desc');
        // }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Detail
                // $btn .= $this->makeButton([
                //     'type' => 'detail',
                //     'id'   => $record->id
                // ]);
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.jenis-assortment.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.jenis-assortment.create');
    }

    public function store(JenisAssortmentRequest $request)
    {
    	$jenisassortment = new JenisAssortment;
    	$jenisassortment->fill($request->all());
    	$jenisassortment->save();

        //log insert
        $log = new LogJenisAssortment;
        $log->jenis_assortment_id = $jenisassortment->id;
        $log->nama = $request->nama;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Assortment Type',
            'ref'               => '',
            'aksi'              => 'Tambah Data Assortment Type',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $jenisassortment
    	]);
    }

    public function upload()
    {
        return $this->render('modules.utama.produk.jenis-assortment.upload');
    }

    public function edit($id)
    {
    	$record = JenisAssortment::find($id);
        return $this->render('modules.utama.produk.jenis-assortment.edit', ['record' => $record]);
    }

    public function update(JenisAssortmentRequest $request, $id)
    {
    	$jenisassortment = JenisAssortment::find($id);
    	$jenisassortment->fill($request->all());
    	$jenisassortment->save();

        //log update
        $log = new LogJenisAssortment;
        $log->jenis_assortment_id = $jenisassortment->id;
        $log->nama = $jenisassortment->nama;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Assortment Type',
            'ref'               => '',
            'aksi'              => 'Ubah Data Assortment Type',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $jenisassortment
    	]);
    }

    public function destroy($id)
    {
    	$jenisassortment = JenisAssortment::find($id);
    	$jenisassortment->delete();

        //log delete
        $log = new LogJenisAssortment;
        $log->jenis_assortment_id = $jenisassortment->id;
        $log->nama = $jenisassortment->nama;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Assortment Type',
            'ref'               => '',
            'aksi'              => 'Hapus Data Assortment Type',
        ]);

    	return response([
    		'status' => true,
    	]);
    }

    /* detail */
    public function detail($id)
    {
        $jenisassortment = JenisAssortment::find($id);
        $produk = produk::all();

        // get produk yg terceklis
        $ceklis = [];
        $res = AssortmentTypeProduk::where('assortment_type_id', $id)->get();
        foreach ($res as $val) { // transform to array
            $ceklis[$val->produk_kode] = $val->produk_kode;
        }

        return $this->render('modules.utama.produk.jenis-assortment.detail', [
            'assortment' => $jenisassortment, 
            'produk' => $produk,
            'ceklis' => $ceklis,
        ]);
    }

    public function saveAssortmentProduk(Request $request)
    {
        $ceklis      = $request->ceklis;
        $type_id     = $request->type_id;
        $produk_kode = $request->produk_kode;

        if ($ceklis == 'true') {
            $produk = AssortmentTypeProduk::where('assortment_type_id', $type_id)->where('produk_kode', $produk_kode)->first();
            if (!$produk) {
                $prod = new AssortmentTypeProduk();
                $prod->assortment_type_id = $type_id;
                $prod->produk_kode        = $produk_kode;
                $prod->save();
            }
        } else {
            $produk = AssortmentTypeProduk::where('assortment_type_id', $type_id)->where('produk_kode', $produk_kode)->first();
            if ($produk) {
                $produk->delete();
            }
        }

        return response(['status' => 'success']);
    }

}
