<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\UnitUomRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Master\UnitUom;
use Lotte\Models\Master\Log\LogUnitUom;
use Lotte\Models\Trans\TransLogAudit;

class UnitUomController extends Controller
{
	protected $link = 'utama/produk/unit-uom/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Unit UoM");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Unit UoM' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Unit UoM',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

		public function grid(Request $request)
	{
		$records = UnitUom::with('creator')
						 ->select('*');
        // $record = UnitUom::select('*');
        // dd($record);

		// Init Sort
  //       if (!isset(request()->order[0]['column'])) {
  //           // $records->->sort();
  //           $records->orderBy('kode');
  //       }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
   //          ->addColumn('generate',function($record){
			// 	return $record->generate->kode;
			// })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.unit-uom.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.unit-uom.create');
    }

    public function store(UnitUomRequest $request)
    {
    	$unituom = new UnitUom;
    	$unituom->fill($request->all());
    	$unituom->save();

        //log insert
        $log = new LogUnitUom;
        $log->unit_uom_id = $unituom->id;
        $log->nama = $request->nama;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Unit UOM',
            'ref'               => '',
            'aksi'              => 'Tambah Data Unit UOM',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $unituom
    	]);
    }

    public function upload()
    {
        return $this->render('modules.utama.produk.unit-uom.upload');
    }

    public function edit($id)
    {
    	$record = UnitUom::find($id);
        return $this->render('modules.utama.produk.unit-uom.edit', ['record' => $record]);
    }

    public function update(UnitUomRequest $request, $id)
    {
    	$unituom = UnitUom::find($id);
    	$unituom->fill($request->all());
    	$unituom->save();

        //log update
        $log = new LogUnitUom;
        $log->unit_uom_id = $unituom->id;
        $log->nama = $unituom->nama;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Unit UOM',
            'ref'               => '',
            'aksi'              => 'Ubah Data Unit UOM',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $unituom
    	]);
    }

    public function destroy($id)
    {
    	$unituom = UnitUom::find($id);
    	$unituom->delete();

         //log delete
        $log = new LogUnitUom;
        $log->unit_uom_id = $unituom->id;
        $log->nama = $unituom->nama;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Unit UOM',
            'ref'               => '',
            'aksi'              => 'Hapus Data Unit UOM',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
}
