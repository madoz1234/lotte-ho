<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\RakRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\Rak;
use Lotte\Models\Master\Log\LogRak;
use Lotte\Models\Trans\TransLogAudit;

class RakController extends Controller
{
	protected $link = 'utama/produk/rak/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Rak");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Rak' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'tipe_rak',
			    'name' => 'tipe_rak',
			    'label' => 'Tipe Rak',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'shelving',
			    'name' => 'shelving',
			    'label' => 'Shelving (Tingkat)',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'tinggi_rak',
			    'name' => 'tinggi_rak',
			    'label' => 'Dimensi Rak',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'hanger',
			    'name' => 'hanger',
			    'label' => 'Hanger',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Rak::with('creator')
						 ->select('*');
        // $record = TipeBarang::select('*');
        // dd($record);

		// Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('tipe_rak');
        }

        //Filters
        if ($tipe_rak = $request->tipe_rak) {
            $records->where('tipe_rak', 'ilike', '%' . $tipe_rak . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('hanger',function($record){
				if($record->hanger == 1){
					return 'Ya';
				}else if($record->hanger == 0){
					return 'Tidak';
				}
			})
   //          ->addColumn('generate',function($record){
			// 	return $record->generate->kode;
			// })
			// 
			->addColumn('tinggi_rak', function ($record) {
                return $record->panjang_rak. 'cm'. '&nbsp;X&nbsp;' .$record->lebar_rak.'cm'. '&nbsp;X&nbsp;' .$record->tinggi_rak. 'cm';
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                // $btn .= $this->makeButton([
                // 	'type' => 'edit',
                // 	'id'   => $record->id
                // ]);
                // // Delete
                // $btn .= $this->makeButton([
                // 	'type' => 'delete',
                // 	'id'   => $record->id
                // ]);
                //Detail
                $btn .= $this->makeButton([
                    'type' => 'detail',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.rak.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.rak.create');
    }

    public function store(RakRequest $request)
    {
    	$rak = new Rak;
    	$rak->fill($request->all());
    	$rak->save();

    	//log insert
        $log = new LogRak;
        $log->rak_id = $rak->id;
        $log->tipe_rak = $request->tipe_rak;
        $log->shelving = $request->shelving;
        $log->tinggi_rak = $request->tinggi_rak;
        $log->panjang_rak = $request->panjang_rak;
        $log->lebar_rak = $request->lebar_rak;
        $log->hanger = $request->hanger;
        $log->tinggi_hanger = $request->tinggi_hanger;
        $log->panjang_hanger = $request->panjang_hanger;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Rak',
            'ref'               => '',
            'aksi'              => 'Tambah Data Rak',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $rak
    	]);
    }

    public function show($id)
    {
        $record = Rak::find($id);

        return response($record);
    }

    public function edit($id)
    {
    	$record = Rak::find($id);

        return $this->render('modules.utama.produk.rak.edit', ['record' => $record]);
    }

    public function detail($id)
    {
        $record = Rak::find($id);

        return $this->render('modules.utama.produk.rak.detail', ['record' => $record]);
    }

    public function update(RakRequest $request, $id)
    {
    	$rak = Rak::find($id);
    	$rak->fill($request->all());
    	$rak->save();

    	// log update
        $log = new LogRak;
        $log->rak_id = $rak->id;
        $log->tipe_rak = $rak->tipe_rak;
        $log->shelving = $rak->shelving;
        $log->tinggi_rak = $rak->tinggi_rak;
        $log->panjang_rak = $rak->panjang_rak;
        $log->lebar_rak = $rak->lebar_rak;
        $log->hanger = $rak->hanger;
        $log->tinggi_hanger = $rak->tinggi_hanger;
        $log->panjang_hanger = $rak->panjang_hanger;

        $log->save();

    	return response([
    		'status' => true,
    		'data'	=> $rak
    	]);
    }

    public function destroy($id)
    {
    	$rak = Rak::find($id);
    	$rak->delete();

    	//log delete
        $log = new LogRak;
        $log->rak_id = $rak->id;
        $log->tipe_rak = $rak->tipe_rak;
        $log->shelving = $rak->shelving;
        $log->tinggi_rak = $rak->tinggi_rak;
        $log->panjang_rak = $rak->panjang_rak;
        $log->lebar_rak = $rak->lebar_rak;
        $log->hanger = $rak->hanger;
        $log->tinggi_hanger = $rak->tinggi_hanger;
        $log->panjang_hanger = $rak->panjang_hanger;

        $log->save();

    	return response([
    		'status' => true,
    	]);
    }


}
