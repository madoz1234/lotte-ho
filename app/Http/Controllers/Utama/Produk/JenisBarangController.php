<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\JenisBarangRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\JenisBarang;
use Lotte\Models\Master\Log\LogJenisBarang;
use Lotte\Models\Trans\TransLogAudit;

class JenisBarangController extends Controller
{
    protected $link = 'utama/produk/jenis-barang/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Jenis Barang");
        $this->setSubtitle("&nbsp;");
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Jenis Barang' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            // [
            //     'data' => 'kode',
            //     'name' => 'kode',
            //     'label' => 'Kode',
            //     'searchable' => false,
            //     'sortable' => true,
            //     'className' => "center aligned",
            //     'width' => '150px',
            // ],
            [
                'data' => 'jenis',
                'name' => 'jenis',
                'label' => 'Jenis Barang',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = JenisBarang::with('creator')
                         ->select('*');
        // $record = TipeBarang::select('*');
        // dd($record);

        // Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('kode');
        }

        //Filters
        // if ($kode = $request->kode) {
        //     $records->where('kode', 'like', '%' . $kode . '%');
        // }
        if ($jenis = $request->jenis) {
            $records->where('jenis', 'ilike', '%' . $jenis . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
   //          ->addColumn('generate',function($record){
            //  return $record->generate->kode;
            // })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                // $btn .= $this->makeButton([
                //     'type' => 'edit',
                //     'id'   => $record->id
                // ]);
                // // Delete
                // $btn .= $this->makeButton([
                //     'type' => 'delete',
                //     'id'   => $record->id
                // ]);
                //Detail
                $btn .= $this->makeButton([
                    'type' => 'detail',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.utama.produk.jenis-barang.index', ['mockup' => false]);
    }

    public function create()
    {
        $code = JenisBarang::generateCode();
        return $this->render('modules.utama.produk.jenis-barang.create', ['code' => $code]);
    }

    public function store(JenisBarangRequest $request)
    {
        $jenisbarang = new JenisBarang;
        $jenisbarang->fill($request->all());
        $jenisbarang->save();

        //log insert
        $log = new LogJenisBarang;
        $log->jenis_barang_id = $jenisbarang->id;
        $log->kode = $request->kode;
        $log->jenis = $request->jenis;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Jenis Barang',
            'ref'               => '',
            'aksi'              => 'Tambah Data Jenis Barang',
        ]);

        return response([
            'status' => true,
            'data'  => $jenisbarang
        ]);
    }

    public function edit($id)
    {
        $record = JenisBarang::find($id);

        return $this->render('modules.utama.produk.jenis-barang.edit', ['record' => $record]);
    }

    public function detail($id)
    {
        $record = JenisBarang::find($id);

        return $this->render('modules.utama.produk.jenis-barang.detail', ['record' => $record]);
    }

    public function update(JenisBarangRequest $request, $id)
    {
        $jenisbarang = JenisBarang::find($id);
        $jenisbarang->fill($request->all());
        $jenisbarang->save();

        //log insert
        $log = new LogJenisBarang;
        $log->jenis_barang_id = $jenisbarang->id;
        $log->kode = $jenisbarang->kode;
        $log->jenis = $jenisbarang->jenis;

        $log->save();

        return response([
            'status' => true,
            'data'  => $jenisbarang
        ]);
    }

    public function destroy($id)
    {
        $jenisbarang = JenisBarang::find($id);
        $jenisbarang->delete();

        //log insert
        $log = new LogJenisBarang;
        $log->jenis_barang_id = $jenisbarang->id;
        $log->kode = $jenisbarang->kode;
        $log->jenis = $jenisbarang->jenis;

        $log->save();

        return response([
            'status' => true,
        ]);
    }
}
