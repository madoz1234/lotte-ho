<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\ProdukRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Master\Lsi;

class UploadProdukController extends Controller
{
	protected $link = 'utama/produk/upload-produk/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Upload Produk GMD");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Upload Produk dari GMD' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => 'No',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'kode',
			    'name' => 'kode',
			    'label' => 'Kode Produk',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Product Description',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'tipe_produk',
			    'name' => 'tipe_produk',
			    'label' => 'Tipe Produk',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'bumun_nm',
			    'name' => 'bumun_nm',
			    'label' => 'Division',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'l1_nm',
			    'name' => 'l1_nm',
			    'label' => 'Category 1',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'l2_nm',
			    'name' => 'l2_nm',
			    'label' => 'Category 2',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'l3_nm',
			    'name' => 'l3_nm',
			    'label' => 'Category 3',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'l4_nm',
			    'name' => 'l4_nm',
			    'label' => 'Category 4',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'width',
			    'name' => 'width',
			    'label' => 'Width',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'length',
			    'name' => 'length',
			    'label' => 'Length',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'height',
			    'name' => 'height',
			    'label' => 'Height',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'wg',
			    'name' => 'wg',
			    'label' => 'Weight',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'gmd_price_status',
			    'name' => 'gmd_price_status',
			    'label' => 'GMD',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			// [
			//     'data' => 'gmd_status',
			//     'name' => 'gmd_status',
			//     'label' => 'GMD Status',
			//     'searchable' => false,
			//     'sortable' => false,
			//     'className' => "center aligned",
			// ],
		]);
	}

	public function grid(Request $request)
	{
		// $records = Produk::with('creator')
		// 				 ->select('*');
		$records = Produk::with('creator')->whereHas('produksetting', function($query){
												$query->where('tipe_produk', 1);
											})->get();

		// Init Sort
  //       if (!isset(request()->order[0]['column'])) {
  //           // $records->->sort();
  //           $records->orderBy('kode');
  //       }

        //Filters
        if ($kode = $request->kode) {
           //  $records = Produk::with('creator')->whereHas('produksetting', function($query){
											// 	$query->where('tipe_produk', 1);
											// })->get()->where('kode', 'like', '%' . $kode . '%');
            $records = Produk::where('kode', 'like', '%' . $kode . '%')
                                            ->whereHas('produksetting', function($q){
                                                $q->where('tipe_produk', 1);
                                            })->get();
        }
        if ($nama = $request->nama) {
            $records = Produk::with('creator')->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($bumun_nm = $request->bumun_nm) {
            $records = Produk::with('creator')->where('bumun_nm',$bumun_nm);
        }
        if ($l1_nm = $request->l1_nm) {
            $records = Produk::with('creator')->where('l1_nm',$l1_nm);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('bumun_nm', function ($record) {
                $string = (!is_null($record->bumun_nm)) ? $record->bumun_nm : '-';
                return $string;
            })
            ->addColumn('tipe_produk', function ($record) {
                $string = 'Produk GMD';
                return $string;
            })
            ->addColumn('l1_nm', function ($record) {
                $string = (!is_null($record->l1_nm)) ? $record->l1_nm : '-';
                return $string;
            })
            ->addColumn('l2_nm', function ($record) {
                $string = (!is_null($record->l2_nm)) ? $record->l2_nm : '-';
                return $string;
            })
            ->addColumn('l3_nm', function ($record) {
                $string = (!is_null($record->l3_nm)) ? $record->l3_nm : '-';
                return $string;
            })
            ->addColumn('l4_nm', function ($record) {
                $string = (!is_null($record->l4_nm)) ? $record->l4_nm : '-';
                return $string;
            })
            ->addColumn('width', function ($record) {
                $string = (!is_null($record->width)) ? $record->width : '-';
                return $string;
            })
            ->addColumn('length', function ($record) {
                $string = (!is_null($record->length)) ? $record->length : '-';
                return $string;
            })
            ->addColumn('height', function ($record) {
                $string = (!is_null($record->height)) ? $record->height : '-';
                return $string;
            })
            ->addColumn('wg', function ($record) {
                $string = (!is_null($record->wg)) ? $record->wg : '-';
                return $string;
            })
            ->addColumn('gmd_price_status', function ($record) {
                $string = '<button type="button" class="ui mini orange icon cekprice button" data-content="Cek Data" data-id="'.$record->kode.'"> Cek Price $ Status</button>';
                // <button type="button" class="ui mini orange icon cekstatus button" data-content="Cek Data" data-id="'.$record->kode.'"> Cek Status</button>';
				return $string;
            })
   //          ->addColumn('gmd_status', function ($record) {
   //              $string = '<button type="button" class="ui mini orange icon cekstatus button" data-content="Cek Data" data-id="'.$record->kode.'"> Cek</button>';
			// return $string;
   //          })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

	public function create()
    {
        return $this->render('modules.utama.produk.upload-produk.create');
    }

    public function store(ProdukRequest $request)
    {
        //save produk ref_produk
        // dd($request->all());
    	$data=$request->all();

        $nongmd = new Produk;
        $nongmd->fill($data);
        // $nongmd->save();
        if($nongmd->save()){
                $produksetting = new ProdukSetting;
                $produksetting->produk_kode          		= $nongmd->kode;
                $produksetting->uom1_produk_description     = $nongmd->deskripsi_uom_1;
                $produksetting->tipe_barang_kode          	= $nongmd->kode_tipe_barang;
                $produksetting->jenis_barang_kode          	= $nongmd->kode_jenis_barang;
                $produksetting->locked_price          		= $nongmd->locked_price;
                $produksetting->uom1_barcode          		= $nongmd->barcode_uom_1;
                $produksetting->uom1_satuan          		= $nongmd->satuan_uom_1;
                $produksetting->uom2_satuan          		= $nongmd->satuan_uom_2;
                $produksetting->uom1_boxtype          		= $nongmd->box_type_uom_1;
                $produksetting->uom2_boxtype          		= $nongmd->box_type_uom_2;
                $produksetting->uom2_conversion          	= $nongmd->conversion_uom_2;
                $produksetting->margin          			= $nongmd->margin;
                $produksetting->created_at       			= date('Y-m-d H:i:s');
                $produksetting->created_by       			= auth()->user()->id;
                $produksetting->save();
            }

    	return response([
    		'status' => true,
    		'data'	=> $nongmd
    	]);
    }

    public function index()
    {
        return $this->render('modules.utama.produk.upload-produk.index', ['mockup' => false]);
    }

    public function cekprice($kode)
    {
    	$this->data['produklsi'] = ProdukLsi::where('produk_kode', $kode)->get();
    	$this->data['lsi'] = Lsi::all();

        return $this->render('modules.utama.produk.upload-produk.price', $this->data);
    }

    public function cekstatus($kode)
    {
    	$this->data['produklsi'] = ProdukLsi::where('produk_kode', $kode)->get();
    	$this->data['lsi'] = Lsi::all();

        return $this->render('modules.utama.produk.upload-produk.status', $this->data);
    }

    //Function download template Produk
    public function postExportTabel(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\upload_produk_gmd.xlsx', function($reader) use ($filter) {
            // get data logsheet
      
        })->download('xls');
    }
    //Function download template Produk

    public function importexcel()
    {
        return $this->render('modules.utama.produk.upload-produk.import');
    }

    //Function upload template Produk
    public function postImportExcel(Request $request)
	{
		if($request->hasFile('upload_data_produk')){
			// upload file to tmp folder
			$file = uploadFile($request->file('upload_data_produk'), 'upload-produk/tmp')->filename;

			// run artisan command in background
			//call_in_background('upload:produk --filename={$file}');
			$exitCode = \Artisan::call('upload:produk', [
		            '--filename' => $file,
		    	]);
			
			return back()->with('success','Insert Record successfully.');
		}


		return back()->with('error','Please Check your file, Something is wrong there.');
	}
    //Function upload template Produk
}
