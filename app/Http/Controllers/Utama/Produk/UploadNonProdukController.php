<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\ProdukRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Master\Lsi;

class UploadNonProdukController extends Controller
{
	protected $link = 'utama/produk/upload-non-produk/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Upload Produk Non GMD");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Upload Produk dari GMD' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => 'No',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'produk_kode',
			    'name' => 'produk_kode',
			    'label' => 'Kode Produk',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Product Description',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "left aligned",
			],
			[
			    'data' => 'tipe_produk',
			    'name' => 'tipe_produk',
			    'label' => 'Tipe Produk',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'tipe_barang_kode',
			    'name' => 'tipe_barang_kode',
			    'label' => 'Tipe Barang',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'jenis_barang_kode',
			    'name' => 'jenis_barang_kode',
			    'label' => 'Jenis Barang',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			// [
			//     'data' => 'detail',
			//     'name' => 'detail',
			//     'label' => 'Aksi',
			//     'searchable' => false,
			//     'sortable' => false,
			//     'className' => "center aligned",
			// ],
		]);
	}

	public function grid(Request $request)
	{
		$records = ProdukSetting::whereIn('tipe_produk', ['0']);
		// $records = ProdukSetting::whereIn('tipe_barang_kode', ['004', '003', '002']);

		// Init Sort
  //       if (!isset(request()->order[0]['column'])) {
  //           // $records->->sort();
  //           $records->orderBy('kode');
  //       }

        //Filters
        if ($produk_kode = $request->produk_kode) {
            $records->where('produk_kode', 'like', '%' . $produk_kode . '%');
        }
        if ($produk_nama = $request->produk_nama) {
            $records->where('produk_kode', 'like', '%' . $produk_nama . '%');
        }
        if ($tipe_barang_kode = $request->tipe_barang_kode) {
            $records->where('tipe_barang_kode',$tipe_barang_kode);
        }
        if ($jenis_barang_kode = $request->jenis_barang_kode) {
            $records->where('jenis_barang_kode',$jenis_barang_kode);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('tipe_produk', function ($record) {
                $string = 'Produk Non GMD';
				return $string;
            })
            ->addColumn('nama', function ($record) {
                return $record->produk->nama;
            })
            ->addColumn('tipe_barang_kode', function ($record) {
                return $record->tipebarang->nama;
            })
            ->addColumn('jenis_barang_kode', function ($record) {
                return $record->jenisbarang->jenis;
            })
            ->addColumn('detail', function ($record) {
                $string = '<button type="button" class="ui mini orange icon edit button" data-content="Detail Data" data-id="'.$record->id.'"><i class="edit icon"></i> Detail</button>';
				return $string;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

	public function create()
    {
        return $this->render('modules.utama.produk.upload-non-produk.create');
    }

    public function store(ProdukRequest $request)
    {
        //save produk ref_produk
        // dd($request->all());
    	$data=$request->all();

        $nongmd = new Produk;
        $nongmd->fill($data);
        // $nongmd->save();
        if($nongmd->save()){
                $produksetting = new ProdukSetting;
                $produksetting->produk_kode          = $nongmd->kode;
                $produksetting->created_at       = date('Y-m-d H:i:s');
                $produksetting->created_by       = auth()->user()->id;
                $produksetting->save();
            }

    	return response([
    		'status' => true,
    		'data'	=> $nongmd
    	]);
    }

    public function index()
    {
        return $this->render('modules.utama.produk.upload-non-produk.index', ['mockup' => false]);
    }

    public function edit($id)
    {
    	$record = ProdukSetting::find($id);

        return $this->render('modules.utama.produk.upload-non-produk.edit', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
        $nongmd = ProdukSetting::find($id);
        $nongmd->fill($request->all());
        $nongmd->save();

        return response([
            'status' => true,
            'data'  => $nongmd
        ]);
    }

    public function importexcel()
    {
        return $this->render('modules.utama.produk.upload-non-produk.import');
    }

    //Function upload template Produk Non GMD
    public function postImportExcel(Request $request)
	{
		if($request->hasFile('upload_produk_non_gmd')){
			$path = $request->file('upload_produk_non_gmd')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();
            $sukses = 0;
            $gagal = 0;
        	$error = [];

            // dd($data->toArray());
            if(!empty($data) && $data->count()){
                foreach ($data->toArray() as $key => $value) {
                    // dd($value);
                    if(!is_null($value['kode'])){ //jika data tidak boleh kosong tambahkan dengan &&
                        $check = Produk::where('kode', $value['kode'])->first();
                        // ins/updt
                        if (!$check) {
							$record             = new Produk;
							$record->kode       = $value['kode'];
							$record->nama       = $value['nama'];
							$record->created_by = \Auth::user()->id;
							$record->created_at = date('Y-m-d H:i:s');
							if($record->save())
							{
								$recorddetail              = new ProdukSetting;
	                            // $recorddetail->tipe_barang_kode     = '002';
	                            // $recorddetail->ppob     = '1';
                                $recorddetail->produk_kode       = $record->kode;
								$recorddetail->uom1_produk_description       = $record->nama;
								$recorddetail->tipe_barang_kode  = $value['kode_tipe_barang'];
                                $recorddetail->jenis_barang_kode = $value['kode_jenis_barang'];
                                $recorddetail->locked_price = $value['locked_price'];
                                $recorddetail->uom1_satuan = $value['satuan_uom_1'];
								$recorddetail->uom2_satuan = $value['satuan_uom_2'];
                                $recorddetail->uom1_barcode              = $value['barcode_uom_1'];
                                $recorddetail->uom2_barcode              = $value['barcode_uom_1'];
                                $recorddetail->uom1_boxtype              = $value['box_type_uom_1'];
                                $recorddetail->uom2_boxtype              = $value['box_type_uom_2'];
                                $recorddetail->uom2_conversion              = $value['conversion_uom_2'];
                                $recorddetail->margin              = $value['margin'];
                                // $recorddetail->ppob              = $value['kode_pulsa'];
                                $recorddetail->ppob              = 0;
                                $recorddetail->tipe_produk       = 0;
								$recorddetail->created_by        = $record->created_by;
								$recorddetail->created_at        = $record->created_at;
								if($recorddetail->save()){
									$sukses++;
								}
							}
						}else{
							$gagal++;
                            $error[$value['kode']] = 'Produk Kode '.$value['kode'].' sudah terdaftar dalam system.';
						}
                    }
                }

                // if(!empty($insert)){
                // 	// dd('tara');
                //     // Produk::insert($insert);
                //     return back()->with('success','Insert Record successfully.');
                // }
                if ($error) {
                    //create log 
                    $record = $error;
                    $filename = date("YmdHis");
                    $errorlog = Excel::create($filename, function($excel) use ($record){
                        $excel->setTitle('Error Log Upload Non Gmd');
                        $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
                        $excel->setDescription('Error Log Upload Non Gmd');

                        $excel->sheet('Error Log Upload Non Gmd ', function($sheet) use ($record){
                            $sheet->row(1, array(
                                'Error Log Upload Non Gmd '.date("Y-m-d H:i:s"),
                            ));

                            $row = 2;
                            foreach ($record as $val) {
                                $sheet->row($row, array(
                                    $val,
                                ));
                                $row=$row;
                                $row++;
                            }
                            $sheet->setWidth(array('A'=>50));
                            $sheet->cells('A1', function($cells){
                                $cells->setAlignment('center');
                                $cells->setBackground('#999999');
                                $cells->setFontColor('#FFFFFF');
                            });

                        });
                    })->store('xls', storage_path('uploads/log-upload-produk-non-gmd'));

                    return response([
                        'status' => true,
                        'upload' => true,
                        'sukses' => $sukses,
                        'gagal'  => $gagal,
                        'log' => '<a href="'.asset('storage/uploads/log-upload-produk-non-gmd/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
                    ]);
                }else{
                    return response([
                        'status' => true,
                        'upload' => true,
                        'sukses' => $sukses,
                        'gagal'  => $gagal
                    ]);
                }
            }
		}
	}
    //Function upload template Produk Non GMD
}
