<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\ProdukSettingRequest;
use Lotte\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\Rak;

class ProdukController extends Controller
{
	protected $link = 'utama/produk/list-produk/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Produk");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Produk' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => 'No',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'produk_kode',
			    'name' => 'produk_kode',
			    'label' => 'Kode Produk',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'produk_nama',
			    'name' => 'produk_nama',
			    'label' => 'Nama Produk',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
            [
                'data' => 'tipe_produk',
                'name' => 'tipe_produk',
                'label' => 'Tipe Produk',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'tipe_barang_kode',
                'name' => 'tipe_barang_kode',
                'label' => 'Tipe Barang',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
			[
			    'data' => 'jenis_barang_kode',
			    'name' => 'jenis_barang_kode',
			    'label' => 'Jenis Barang',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'divisi',
			    'name' => 'divisi',
			    'label' => 'Division',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'category1',
			    'name' => 'category1',
			    'label' => 'Category 1',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'category2',
			    'name' => 'category2',
			    'label' => 'Category 2',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			// [
			//     'data' => 'status',
			//     'name' => 'status',
			//     'label' => 'Status',
			//     'searchable' => false,
			//     'sortable' => false,
			//     'className' => "center aligned",
			// ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
            [
                'data' => 'detail',
                'name' => 'detail',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ],
            // [
            //     'data' => 'action',
            //     'name' => 'action',
            //     'label' => 'Aksi',
            //     'searchable' => false,
            //     'sortable' => false,
            //     'className' => "center aligned",
            //     'width' => '150px',
            // ]
		]);
	}

	public function grid(Request $request)
	{
		$records = ProdukSetting::with('creator', 'produk', 'tipebarang');
        // $records = ProdukSetting::whereIn('tipe_barang_kode', ['004', '003', '002']);
		// $record = Produk::with('creator')->select('*');
        // Filters
        if ($produk_kode = $request->produk_kode) {
            $records->where('produk_kode', 'like', '%' . $produk_kode . '%');
        }

        if ($nama = $request->nama) {
        	$records->whereHas('produk', function ($query) use ($nama){
        		$query->where('nama', 'ilike', '%' . $nama . '%');
        	});
        }

        // // if ($tipe_barang_kode = $request->tipe_barang_kode) {
        // // 	$records->whereHas('tipebarang', function ($query) use ($tipe_barang_kode){
        // // 		$query->where('tipe_barang_kode', 'like', '%' . $tipe_barang_kode . '%');
        // // 	});
        // // }

        if ($bumun_nm = $request->bumun_nm) {
        	$records->whereHas('produk', function ($query) use ($bumun_nm){
        		$query->where('bumun_nm', 'like', '%' . $bumun_nm . '%');
        	});
        }

        if ($l1_nm = $request->l1_nm) {
            $records->whereHas('produk', function ($query) use ($l1_nm){
                $query->where('l1_nm', 'like', '%' . $l1_nm . '%');
            });
        }

        if ($tipe_barang_kode = $request->tipe_barang_kode) {
            $records->where('tipe_barang_kode',$tipe_barang_kode);
        }

        if ($jenis_barang_kode = $request->jenis_barang_kode) {
            $records->where('jenis_barang_kode',$jenis_barang_kode);
        }

        // dd($request->tipe_produk);
        $tipe_produk = $request->tipe_produk;
        if ($tipe_produk != '') {
            $records->where('tipe_produk', $tipe_produk);
        }

        // if ($status_prod = $request->status_prod) {
        // 	$records->whereHas('produk', function ($query) use ($status_prod){
        // 		$query->where('status_prod', 'like', '%' . $status_prod . '%');
        // 	});
        // }

        // dd($records->take(10)->get());

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('produk_nama',function($record){
                // return ($record->produk) ? $record->produk->nama : '-';
                if($record->tipe_produk == 1){
                    $string = $record->produk->nama;
                }else if($record->tipe_produk == 0){
                    $string = $record->uom1_produk_description;
                }
                return $string;
            })
            ->addColumn('divisi',function($record){
                return ($record->produk) ? $record->produk->bumun_nm : $record->produk->nama;
            })

            ->addColumn('tipe_produk',function($record){
                if($record->tipe_produk == 1){
                    return 'Produk GMD';
                }else if($record->tipe_produk == 0){
                    return 'Produk Non GMD';
                }
            })

            ->addColumn('tipe_barang_kode',function($record){
                return ($record->tipebarang) ?  $record->tipebarang->nama : '-';
                // return ($produksetting = $record->unit) ? $produksetting->unit : '-';
            })
            ->addColumn('jenis_barang_kode',function($record){
                return ($record->jenisbarang) ?  $record->jenisbarang->jenis : '-';
                // return ($produksetting = $record->unit) ? $produksetting->unit : '-';
            })
            ->addColumn('status',function($record){
                return ($record->produk) ? $record->produk->status_prod : '-';
            })
            ->addColumn('category1',function($record){
                return ($record->produk) ? $record->produk->l1_nm : '-';
            })
            ->addColumn('category2',function($record){
                return ($record->produk) ? $record->produk->l2_nm : '-';
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('detail', function ($record) {
                $string = '';
                if ($record->tipe_produk == 0) {
                    // data non gmd
                    $string = '<button type="button" class="ui mini orange icon edit-non-gmd button" data-content="Detail Data" data-id="'.$record->id.'"><i class="edit icon"></i> Detail</button>';
                }else{
                    //data gmd
                    $string = '<button type="button" class="ui mini orange icon edit button" data-content="Detail Data" data-id="'.$record->id.'"><i class="edit icon"></i> Detail</button>';
                }
				return $string;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                // $btn .= $this->makeButton([
                // 	'type' => 'edit',
                // 	'id'   => $record->id
                // ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.list-produk.index', ['mockup' => false]);
    }

    public function onChangePopRak($id=null)
    {
        $data = Rak::find($id);
        return response()->json([
            'shelving1' => $data->shelving,
            'shelving2' => $data->shelving,
            'shelving3' => $data->shelving,
            'shelving4' => $data->shelving,
        ]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.list-produk.create');
    }

    public function store(Request $request)
    {
    	// dd($request->all());
    	$data=$request->all();
    	// $data['uom1_selling_cek'] = 0;
    	// $data['uom1_order_cek'] = 0;
    	// $data['uom1_receiving_cek'] = 0;
    	// $data['uom1_return_cek'] = 0;
    	// $data['uom1_inventory_cek'] = 0;
    	// $data['uom2_selling_cek'] = 0;
    	// $data['uom2_order_cek'] = 0;
    	// $data['uom2_receiving_cek'] = 0;
    	// $data['uom2_return_cek'] = 0;
    	// $data['uom2_inventory_cek'] = 0;
    	// $data['uom3_selling_cek'] = 0;
    	// $data['uom3_order_cek'] = 0;
    	// $data['uom3_receiving_cek'] = 0;
    	// $data['uom3_return_cek'] = 0;
    	// $data['uom3_inventory_cek'] = 0;
    	// $data['uom4_selling_cek'] = 0;
    	// $data['uom4_order_cek'] = 0;
    	// $data['uom4_receiving_cek'] = 0;
    	// $data['uom4_return_cek'] = 0;
    	// $data['uom4_inventory_cek'] = 0;

    	$produksetting = new ProdukSetting;
    	$produksetting->fill($data);
    	$produksetting->save();

    	return response([
    		'status' => true,
    		'data'	=> $produksetting
    	]);
    }

    public function show($id)
    {
        $record = Produk::with('produksetting')->find($id);

        return response($record);
    }

    public function edit($id)
    {
        $record = ProdukSetting::find($id);

        return $this->render('modules.utama.produk.list-produk.edit', ['record' => $record]);
    }
    
    public function editNonGmd($id)
    {
    	$record = ProdukSetting::find($id);

        return $this->render('modules.utama.produk.list-produk.edit-non-gmd', ['record' => $record]);
    }

    public function update(ProdukSettingRequest $request, $id)
    {
        $data = $request->all();
        // dd($data);
        
        $produksetting = ProdukSetting::find($id);

        if($produksetting){
            // cek request
            $data['margin'] = ($data['margin'] != "") ? decimal_for_save($data['margin']) : '0';
            $data['uom1_width'] = ($data['uom1_width'] != "") ? decimal_for_save($data['uom1_width']) : '0';
            $data['uom1_length'] = ($data['uom1_length'] != "") ? decimal_for_save($data['uom1_length']) : '0';
            $data['uom1_height'] = ($data['uom1_height'] != "") ? decimal_for_save($data['uom1_height']) : '0';
            $data['uom1_weight'] = ($data['uom1_weight'] != "") ? decimal_for_save($data['uom1_weight']) : '0';

            // $data['uom2_width'] = ($data['uom2_width'] != "") ? decimal_for_save($data['uom2_width']) : '0';
            // $data['uom2_length'] = ($data['uom2_length'] != "") ? decimal_for_save($data['uom2_length']) : '0';
            // $data['uom2_height'] = ($data['uom2_height'] != "") ? decimal_for_save($data['uom2_height']) : '0';
            // $data['uom2_weight'] = ($data['uom2_weight'] != "") ? decimal_for_save($data['uom2_weight']) : '0';

            // $data['uom3_width'] = ($data['uom3_width'] != "") ? decimal_for_save($data['uom3_width']) : '0';
            // $data['uom3_length'] = ($data['uom3_length'] != "") ? decimal_for_save($data['uom3_length']) : '0';
            // $data['uom3_height'] = ($data['uom3_height'] != "") ? decimal_for_save($data['uom3_height']) : '0';
            // $data['uom3_weight'] = ($data['uom3_weight'] != "") ? decimal_for_save($data['uom3_weight']) : '0';

            // $data['uom4_width'] = ($data['uom4_width'] != "") ? decimal_for_save($data['uom4_width']) : '0';
            // $data['uom4_length'] = ($data['uom4_length'] != "") ? decimal_for_save($data['uom4_length']) : '0';
            // $data['uom4_height'] = ($data['uom4_height'] != "") ? decimal_for_save($data['uom4_height']) : '0';
            // $data['uom4_weight'] = ($data['uom4_weight'] != "") ? decimal_for_save($data['uom4_weight']) : '0';

            if($request->hasFile('uom1_file')){
                $data['uom1_file'] = uploadFile($request->uom1_file, 'upload-produk/produk/')->file;
            }else if($produksetting['uom1_file'] != ""){
                $data['uom1_file'] = $produksetting['uom1_file'];    
            }

            if($request->hasFile('uom2_file')){
                $data['uom2_file'] = uploadFile($request->uom2_file, 'upload-produk/produk/')->file;
            }else if($produksetting['uom2_file'] != ""){
                $data['uom2_file'] = $produksetting['uom2_file'];    
            }

            if($request->hasFile('uom3_file')){
                $data['uom3_file'] = uploadFile($request->uom3_file, 'upload-produk/produk/')->file;
            }else if($produksetting['uom3_file'] != ""){
                $data['uom3_file'] = $produksetting['uom3_file'];    
            }

            if($request->hasFile('uom4_file')){
                $data['uom4_file'] = uploadFile($request->uom4_file, 'upload-produk/produk/')->file;
            }else if($produksetting['uom4_file'] != ""){
                $data['uom4_file'] = $produksetting['uom4_file'];    
            }

            // check
            // $request['uom1_selling_cek'] = $request->uom1_selling_cek == 'on' ? 1 : 0;
            // $request['uom1_order_cek'] = $request->uom1_order_cek == 'on' ? 1 : 0;
            // $request['uom1_receiving_cek'] = $request->uom1_receiving_cek == 'on' ? 1 : 0;
            // $request['uom1_return_cek'] = $request->uom1_return_cek == 'on' ? 1 : 0;
            // $request['uom1_inventory_cek'] = $request->uom1_inventory_cek == 'on' ? 1 : 0;
            // $request['uom2_selling_cek'] = $request->uom2_selling_cek == 'on' ? 1 : 0;
            // $request['uom2_order_cek'] = $request->uom2_order_cek == 'on' ? 1 : 0;
            // $request['uom2_receiving_cek'] = $request->uom2_receiving_cek == 'on' ? 1 : 0;
            // $request['uom2_return_cek'] = $request->uom2_return_cek == 'on' ? 1 : 0;
            // $request['uom2_inventory_cek'] = $request->uom2_inventory_cek == 'on' ? 1 : 0;

            // dd($data);
            //save data
            $produksetting->fill($data);
            $produksetting->save();

            //return
            return response([
                'status' => true,
                'data'  => $produksetting
            ]);
        }

    }

    public function importexcel()
    {
        return $this->render('modules.utama.produk.list-produk.import');
    }

    //Function list template Produk
 //    public function postImportExcel(Request $request)
	// {

	// 	if($request->hasFile('upload_data_produk')){
	// 		$path = $request->file('upload_data_produk')->getRealPath();
	// 		$data = Excel::load($path, function($reader) {})->get();

	// 		if(!empty($data) && $data->count()){
	// 			foreach ($data->toArray() as $key => $value) {
	// 				if(!is_null($value['kode'])){ //jika data tidak boleh kosong tambahkan dengan &&
	// 					$insert[] = [
	// 							'kode' => $value['kode'],
	// 							'created_at' => $value['created_at'],
	// 							'created_by' => $value['created_by'],
	// 							'nama' => $value['nama'],
	// 							'bumun_cd' => $value['bumun_cd'],
	// 							'bumun_nm' => $value['bumun_nm'],
	// 							'l1_cd' => $value['l1_cd'],
	// 							'l1_nm' => $value['l1_nm'],
	// 							'l2_cd' => $value['l2_cd'],
	// 							'l2_nm' => $value['l2_nm'],
	// 							'l3_cd' => $value['l3_cd'],
	// 							'l3_nm' => $value['l3_nm'],
	// 							'l4_cd' => $value['l4_cd'],
	// 							'l4_nm' => $value['l4_nm'],
	// 							'width' => $value['width'],
	// 							'length' => $value['length'],
	// 							'height' => $value['height'],
	// 							'wg' => $value['wg'],
	// 							'inner_width' => $value['inner_width'],
	// 							'inner_length' => $value['inner_length'],
	// 							'inner_height' => $value['inner_height'],
	// 							'inner_wg' => $value['inner_wg']
	// 						];
	// 				}
	// 			}

	// 			if(!empty($insert)){
	// 				Produk::insert($insert);
	// 				return back()->with('success','Insert Record successfully.');
	// 			}

	// 		}

	// 	}


	// 	return back()->with('error','Please Check your file, Something is wrong there.');
	// }
    //Function upload template Produk
    public function postImportExcel(Request $request)
    {
        if($request->hasFile('upload_data_produk')){
            // upload file to tmp folder
            $file = uploadFile($request->file('upload_data_produk'), 'upload-produk/tmp')->filename;

            // run artisan command in background
            //call_in_background('upload:produk --filename={$file}');
            $exitCode = \Artisan::call('upload:produk', [
                    '--filename' => $file,
                ]);
            
            return back()->with('success','Insert Record successfully.');
        }


        return back()->with('error','Please Check your file, Something is wrong there.');
    } 
}
