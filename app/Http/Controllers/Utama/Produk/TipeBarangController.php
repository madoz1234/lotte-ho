<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\TipeBarangRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\TipeBarang;
use Lotte\Models\Master\Log\LogTipeBarang;
use Lotte\Models\Trans\TransLogAudit;

class TipeBarangController extends Controller
{
	protected $link = 'utama/produk/tipe-barang/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Tipe Barang");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Tipe Barang' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			// [
			//     'data' => 'kode',
			//     'name' => 'kode',
			//     'label' => 'Kode',
			//     'searchable' => false,
			//     'sortable' => true,
			//     'className' => "center aligned",
			//     'width' => '150px',
			// ],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Tipe Barang',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TipeBarang::with('creator')
						 ->select('*');
        // $record = TipeBarang::select('*');
        // dd($record);

		// Init Sort
  //       if (!isset(request()->order[0]['column'])) {
  //           // $records->->sort();
  //           $records->orderBy('kode');
  //       }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($jenis = $request->jenis) {
            $records->where('jenis', 'like', '%' . $jenis . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
   //          ->addColumn('generate',function($record){
			// 	return $record->generate->kode;
			// })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Detail
                $btn .= $this->makeButton([
                	'type' => 'detail',
                	'id'   => $record->id
                ]);
                // // Delete
                // $btn .= $this->makeButton([
                // 	'type' => 'delete',
                // 	'id'   => $record->id
                // ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.tipe-barang.index', ['mockup' => false]);
    }

    public function create()
    {
        $code = TipeBarang::generateCode();
        return $this->render('modules.utama.produk.tipe-barang.create', ['code' => $code]);
    }

    public function store(TipeBarangRequest $request)
    {
    	$tipebarang = new TipeBarang;
    	$tipebarang->fill($request->all());
    	$tipebarang->save();

        //log insert
        $log = new LogTipeBarang;
        $log->tipebarang_id = $tipebarang->id;
        $log->nama = $request->nama;
        $log->kode = $request->kode;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Tipe Barang',
            'ref'               => '',
            'aksi'              => 'Tambah Data Tipe Barang',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $tipebarang
    	]);
    }

    public function edit($id)
    {
    	$record = TipeBarang::find($id);

        return $this->render('modules.utama.produk.tipe-barang.edit', ['record' => $record]);
    }


    public function detail($id)
    {
        $record = TipeBarang::find($id);

        return $this->render('modules.utama.produk.tipe-barang.edit', ['record' => $record]);
    }

    public function update(TipeBarangRequest $request, $id)
    {
    	$tipebarang = TipeBarang::find($id);
    	$tipebarang->fill($request->all());
    	$tipebarang->save();

        //log update
        $log = new LogTipeBarang;
        $log->tipebarang_id = $tipebarang->id;
        $log->nama = $tipebarang->nama;
        $log->kode = $tipebarang->kode;

        $log->save();

    	return response([
    		'status' => true,
    		'data'	=> $tipebarang
    	]);
    }

    public function destroy($id)
    {
    	$tipebarang = TipeBarang::find($id);
    	$tipebarang->delete();

        //log delete
        $log = new LogTipeBarang;
        $log->tipebarang_id = $tipebarang->id;
        $log->nama = $tipebarang->nama;
        $log->kode = $tipebarang->kode;

        $log->save();

    	return response([
    		'status' => true,
    	]);
    }
}
