<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Master\ProdukAssortment;
use Lotte\Models\Master\JenisAssortment;

class AktivasiProdukAssortmentController extends Controller
{
	protected $link = 'utama/produk/aktivasi-produk-assortment/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Aktivasi Produk Assortment");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Aktivasi Produk Assortment' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => 'No',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
            [
                'data' => 'assortment.nama',
                'name' => 'assortment.nama',
                'label' => 'Assortment Type',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'produk.kode',
                'name' => 'produk.kode',
                'label' => 'Kode Produk',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
			[
			    'data' => 'produk_nama',
			    'name' => 'produk_nama',
			    'label' => 'Nama Produk',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
            [
                'data' => 'tipe_produk',
                'name' => 'tipe_produk',
                'label' => 'Tipe Produk',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'tipe_barang',
                'name' => 'tipe_barang',
                'label' => 'Tipe Barang',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'jenis_barang',
                'name' => 'jenis_barang',
                'label' => 'Jenis Barang',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Detil',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
        // dd($request->All());
		$records = ProdukAssortment::with('creator', 'produk', 'assortment', 'produksetting')
                         ->select('ref_produk_assortment.*');
						 // ->select('*');

        //Filters
        if ($produk_kode = $request->produk_kode) {
            $records->where('produk_kode', 'like', '%' . $produk_kode . '%');
        }
        // if ($produk_kode = $request->produk_kode) {
        //     $records->where('produk_kode',$produk_kode);
        // }
        
        if ($nama = $request->nama) {
            $records->whereHas('produk', function ($query) use ($nama){
                $query->where('nama', 'ilike', '%' . $nama . '%');
            });
        }
        if ($assortment_type_id = $request->assortment_type_id) {
            $records->where('assortment_type_id',$assortment_type_id);
        }

        if ($tipe_barang_kode = $request->tipe_barang_kode) {
            $records->whereHas('produksetting', function ($query) use ($tipe_barang_kode){
                $query->where('tipe_barang_kode', 'like', '%' . $tipe_barang_kode . '%');
            });
        }

        if ($jenis_barang_kode = $request->jenis_barang_kode) {
            $records->whereHas('produksetting', function ($query) use ($jenis_barang_kode){
                $query->where('jenis_barang_kode', 'like', '%' . $jenis_barang_kode . '%');
            });
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('produk_nama', function ($record) {
                return ($record->produk) ? $record->produk->nama : '-';
            })
            ->addColumn('tipe_barang', function ($record) {

                return ($record->produk->produksetting->tipebarang) ? $record->produk->produksetting->tipebarang->nama : '-';
                // return ($record->produk) ? $record->produk->produksetting->tipebarang->nama : '-';
            })
            ->addColumn('jenis_barang', function ($record) {
                return ($record->produk->produksetting->jenisbarang) ? $record->produk->produksetting->jenisbarang->jenis : '-';
            })
            ->addColumn('tipe_produk',function($record){
                if($record->produk->produksetting->tipe_produk == 1){
                    return 'Produk GMD';
                }else if($record->produk->produksetting->tipe_produk == 0){
                    return 'Produk Non GMD';
                }
            })
            ->addColumn('detil', function ($record) {
                $string = '<button type="button" class="ui mini orange icon add button" data-content="Detil" data-id="'.$record->id.'"> Detil</button>';
				return $string;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.aktivasi-produk-assortment.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.aktivasi-produk-assortment.create');
    }

    public function edit($id)
    {
    	$record = ProdukAssortment::find($id);

        return $this->render('modules.utama.produk.aktivasi-produk-assortment.edit', ['record' => $record]);
    }

    public function destroy($id)
    {
    	$rak = ProdukAssortment::find($id);
    	$rak->delete();

    	return response([
    		'status' => true,
    	]);
    }

    public function update(Request $request, $id)
    {
    	$rak = ProdukAssortment::find($id);
    	$rak->fill($request->all());
    	$rak->save();

    	return response([
    		'status' => true,
    		'data'	=> $rak
    	]);
    }

    //Function download template Produk
    public function importexcel()
    {
        return $this->render('modules.utama.produk.aktivasi-produk-assortment.import');
    }

    //Function upload template Produk
    public function postImportExcel(Request $request)
    {
        if($request->hasFile('produk_assortment')){
            $path = $request->file('produk_assortment')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();
            $sukses = 0;
            $gagal = 0;
            $error = [];

            if(!empty($data) && $data->count()){
                foreach ($data->toArray() as $key => $value) {
                    // dd($data->toArray());
                    if(!is_null($value['assortment_type'])){ //jika data tidak boleh kosong tambahkan dengan &&
                        // cek assortment
                            $id_assortment = $value['assortment_type'];

                            // ins/updt
                            $check = ProdukAssortment::whereHas('assortment', function($q) use ($id_assortment) { 
                                                            $q->where('nama', $id_assortment);
                                                        })->where('produk_kode', $value['produk_kode'])->first();

                            if (!$check) {
                                $type = JenisAssortment::where('nama', $id_assortment)->first();
                            
                                $assortment = new ProdukAssortment();
                                $assortment->assortment_type_id = $type->id;
                                $assortment->produk_kode = $value['produk_kode'];
                                $assortment->created_by  = \Auth::user()->id;
                                $assortment->created_at  = date('Y-m-d H:i:s');
                                if($assortment->save()){
                                    $sukses++;
                                }
                            }else{
                                $gagal++;
                                $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' sudah terdaftar dalam system.';
                            }
                    }
                }

                // if(!empty($insert)){
                //     ProdukAssortment::insert($insert);
                //     return back()->with('success','Insert Record successfully.');
                // }
                
                if ($error) {
                    //create log 
                    $record = $error;
                    $filename = date("YmdHis");
                    $errorlog = Excel::create($filename, function($excel) use ($record){
                        $excel->setTitle('Error Log Aktivasi Assortment');
                        $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
                        $excel->setDescription('Error Log Aktivasi Assortment');

                        $excel->sheet('Error Log Aktivasi Assortment ', function($sheet) use ($record){
                            $sheet->row(1, array(
                                'Error Log Aktivasi Assortment '.date("Y-m-d H:i:s"),
                            ));

                            $row = 2;
                            foreach ($record as $val) {
                                $sheet->row($row, array(
                                    $val,
                                ));
                                $row=$row;
                                $row++;
                            }
                            $sheet->setWidth(array('A'=>50));
                            $sheet->cells('A1', function($cells){
                                $cells->setAlignment('center');
                                $cells->setBackground('#999999');
                                $cells->setFontColor('#FFFFFF');
                            });

                        });
                    })->store('xls', storage_path('uploads/log-aktivasi-produk-assortment'));

                    return response([
                        'status' => true,
                        'upload' => true,
                        'sukses' => $sukses,
                        'gagal'  => $gagal,
                        'log' => '<a href="'.asset('storage/uploads/log-aktivasi-produk-assortment/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
                    ]);
                }else{
                    return response([
                        'status' => true,
                        'upload' => true,
                        'sukses' => $sukses,
                        'gagal'  => $gagal
                    ]);
                }
            }
        }

        return back()->with('error','Please Check your file, Something is wrong there.');
    }
    //Function upload template Produk
}

