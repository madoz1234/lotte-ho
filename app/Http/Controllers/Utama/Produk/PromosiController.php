<?php

namespace Lotte\Http\Controllers\Utama\produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\PromosiRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\ProdukLsi;
use Lotte\Models\Master\Promosi;
use Lotte\Models\Master\BuyXGetY;
use Lotte\Models\Master\Diskon;
use Lotte\Models\Master\Promo;
use Lotte\Models\Master\Purchase;
use Lotte\Models\Master\PurchaseList;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Trans\TransLogAudit;

class PromosiController extends Controller
{
    protected $link = 'utama/produk/promosi/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Promosi");
        $this->setSubtitle("&nbsp;");
        $this->setModalSize("fullscreen");
        $this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Promosi' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'kode_promo',
                'name' => 'kode_promo',
                'label' => 'Kode Promosi',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Promosi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'tipe',
                'name' => 'tipe',
                'label' => 'Tipe Promosi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'tgl_awal',
                'name' => 'tgl_awal',
                'label' => 'Mulai',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'tgl_akhir',
                'name' => 'tgl_akhir',
                'label' => 'Mulai',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Promosi::with('creator')
                         ->where('status', 0)
                         ->where('tgl_akhir', '>=', date("Y-m-d"))
                         ->orderBy('created_at', 'DESC')
                         ->select('*');
        // $records = Promosi::with('creator')
        //                  ->where('tgl_akhir', '>=', date("Y-m-d"))
        //                  ->orderBy('created_at', 'DESC')
        //                  ->select('*');
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($kode_promosi = $request->kode) {
            $records->where('kode_promo', 'like', '%' . $kode_promosi . '%');
        }
        if ($nama_promosi = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama_promosi . '%');
        }
        
        $tipe_promo = $request->tipe_promo;
        if ($tipe_promo != '') {
            $records->where('tipe_promo', $tipe_promo);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('tipe',function($record){
                if($record->tipe_promo == 1){
                    return 'Diskon';
                }else if($record->tipe_promo == 2){
                    return 'Promo Paket';
                }
                if ($record->tipe_promo == 3){
                    return 'Buy X Get Y';
                }else if ($record->tipe_promo == 4) {
                    return 'Purchase with Purchase';
                }
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
    }

    public function gridHistori(Request $request)
    {
        $records = Promosi::with('creator')
                         ->where('tgl_akhir', '<', date("Y-m-d"))
                         ->orderBy('created_at', 'DESC')
                         ->select('*');
        //Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($kode_promosi = $request->kode) {
            $records->where('kode_promo', 'like', '%' . $kode_promosi . '%');
        }
        if ($nama_promosi = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama_promosi . '%');
        }
        
        $tipe_promo = $request->tipe_promo;
        if ($tipe_promo != '') {
            $records->where('tipe_promo', $tipe_promo);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('tipe',function($record){
                if($record->tipe_promo == 1){
                    return 'Diskon';
                }else if($record->tipe_promo == 2){
                    return 'Promo Paket';
                }
                if ($record->tipe_promo == 3){
                    return 'Buy X Get Y';
                }else if ($record->tipe_promo == 4) {
                    return 'Purchase with Purchase';
                }
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit',
                    'id'   => $record->id
                ]);
                // Delete
                // $btn .= $this->makeButton([
                //     'type' => 'delete',
                //     'id'   => $record->id
                // ]);

                return $btn;
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.utama.produk.promosi.index', ['mockup' => false]);
    }

    public function create()
    {
        $produk = ProdukLsi::all();

        return $this->render('modules.utama.produk.promosi.create', [
            // 'produk' => $produk,
            'produk' => '',
            // 'ceklis' => $ceklis,
        ]);
    }

    public function onChangePopProduk($id=null)
    {
        $data = ProdukLsi::find($id);

        $price_gmd = isset($data->curr_sale_prc) ? $data->curr_sale_prc : 0;
        $uom2_conversion = isset($data->produk->produksetting->uom2_conversion) ? $data->produk->produksetting->uom2_conversion : 0;

        if ($price_gmd != 0 && $uom2_conversion != 0) {
            $price_conversi = round($price_gmd / $uom2_conversion);
        }else{
            $price_conversi = 0;
        }

        return response()->json([
            'kode' => isset($data->produk->kode) ? $data->produk->kode : '-',
            'nama' => isset($data->produk->nama) ? $data->produk->nama : '-',
            'harga' => FormatNumber($price_conversi),
            'locked_price' => isset($data->produk->produksetting->locked_price) ? $data->produk->produksetting->locked_price : 1,
        ]);
    }

    public function onChangePopProdukByKode($kode=null,$lsi_id=null)
    {
        $lsi = Lsi::find($lsi_id);

        $data = ProdukLsi::where('lsi_kode', $lsi->kode)->where('produk_kode',$kode)->whereIn('status', ['NORMAL','ORDER STOP'])->first();
        
        if(!is_null($data)){
            //conversi uom 2
            $price_gmd = isset($data->curr_sale_prc) ? $data->curr_sale_prc : 0;
            $uom2_conversion = isset($data->produk->produksetting->uom2_conversion) ? $data->produk->produksetting->uom2_conversion : 0;

            if ($price_gmd != 0 && $uom2_conversion != 0) {
                $price_conversi = round($price_gmd / $uom2_conversion);
            }else{
                $price_conversi = 0;
            }

            return response()->json([
                'kode' => isset($data->produk->kode) ? $data->produk->kode : '-',
                'nama' => isset($data->produk->nama) ? $data->produk->nama : '-',
                'harga' => FormatNumber($price_conversi),
                'locked_price' => isset($data->produk->produksetting->locked_price) ? $data->produk->produksetting->locked_price : 1,
            ]);
        }else{
            return response([
                'status' => 'Data Tidak Ditemukan',
            ], 402);
        }
    }

    public function store(PromosiRequest $request)
    {
        $data = $request->all();
        // dd($data);
        $promosi = new Promosi;
        $promosi->fill($data);
        if ($promosi->save()) {
            if ($request->tipe_promo == '1') { // untuk diskon
                $array = array_filter($data['diskon_kode_produk']);

                foreach ($array as $key => $value) {
                    $diskon = new Diskon;
                    $diskon->promosi_id = $promosi->id;
                    $diskon->kode_produk = $value;
                    $diskon->nama_produk = $data['diskon_nama_produk'][$key];
                    $diskon->cost_price = decimal_for_save($data['diskon_cost_price'][$key]);
                    $diskon->harga_terdiskon = decimal_for_save($data['diskon_harga_terdiskon'][$key]);
                    $diskon->diskon = decimal_for_save($data['diskon_diskon'][$key]);
                    $diskon->harga_awal = decimal_for_save($data['harga_awal'][$key]);
                    $diskon->save();
                }
            }else if ($request->tipe_promo == '2') { //untuk promo
                $array = array_filter($data['promo_kode_produk']);

                foreach ($array as $key => $value) {
                    $promo = new Promo;
                    $promo->promosi_id = $promosi->id;
                    $promo->kode_produk = $value;
                    $promo->nama_produk = $data['promo_nama_produk'][$key];
                    $promo->selling_price = decimal_for_save($data['promo_selling_price'][$key]);
                    $promo->harga_terdiskon = decimal_for_save($data['promo_harga_terdiskon'][$key]);
                    $promo->diskon = decimal_for_save($data['promo_diskon'][$key]);
                    $promo->save();
                }
            }else if ($request->tipe_promo == '3') { //for buyxgety
                $buyxgety = new BuyXGetY;
                $buyxgety->promosi_id = $promosi->id;
                $buyxgety->kode_produk_x = $request['kode_produk_x'];
                $buyxgety->nama_produk_x = $request['nama_produk_x'];
                $buyxgety->harga_jual_x = decimal_for_save($request['harga_jual_x']);
                $buyxgety->diskon_x = decimal_for_save($request['diskon_x']);
                $buyxgety->harga_diskon_x = decimal_for_save($request['harga_diskon_x']);
                $buyxgety->kode_produk_y = $request['kode_produk_y'];
                $buyxgety->nama_produk_y = $request['nama_produk_y'];
                $buyxgety->harga_jual_y = decimal_for_save($request['harga_jual_y']);
                $buyxgety->diskon_y = decimal_for_save($request['diskon_y']);
                $buyxgety->harga_diskon_y = decimal_for_save($request['harga_diskon_y']);
                $buyxgety->total_diskon = decimal_for_save($request['total_diskon']);
                $buyxgety->save();      
            }else if ($request->tipe_promo == '4') { //for purchase with purchase
                $purchase = new Purchase;
                $purchase->promosi_id = $promosi->id;
                $purchase->kode_produk = $data['purchase_kode_produk'];
                $purchase->nama_produk = $data['purchase_nama_produk'];
                $purchase->selling_price = decimal_for_save($data['purchase_selling_price']);
                $purchase->harga_terdiskon = decimal_for_save($data['purchase_harga_terdiskon']);
                $purchase->diskon = decimal_for_save($data['purchase_diskon']);
                if($purchase->save()){
                    $array = array_filter($data['purchase_list_kode_produk']);

                    foreach ($array as $key => $value) {
                        $purchaselist = new PurchaseList;
                        $purchaselist->purchase_id = $purchase->id;
                        $purchaselist->kode_produk = $value;
                        $purchaselist->nama_produk = $data['purchase_list_nama_produk'][$key];
                        $purchaselist->selling_price = decimal_for_save($data['purchase_list_selling_price'][$key]);
                        $purchaselist->save();
                    }
                }

            }

            foreach ($data['tmuk_id'] as $key => $id) {
                $tmuk[$key] = ['promosi_id' => $promosi->id,'tmuk_id' => $id]; 
            }
            $promosi->detailTmuk()->sync($tmuk);


            // insert to log audit
            TransLogAudit::setLog([
                'tanggal_transaksi' => date('Y-m-d'),
                'type'              => 'Master Data Promosi',
                'ref'               => '',
                'aksi'              => 'Tambah Data Promosi',
            ]);

            return response([
                'status' => true,
                'data'  => $promosi
            ]);
        }      
    }

    public function edit($id)
    {
        $promosi = Promosi::with('detailDiskon', 'detailPromo', 'detailBuyxgety', 'detailPurchase')->find($id);

        return $this->render('modules.utama.produk.promosi.edit', [
            'promosi' => $promosi,
            // 'ceklis' => $ceklis,
        ]);
    }

    public function update(PromosiRequest $request, $id)
    {
        $data = $request->all();
        // dd($data);
        $promosi = Promosi::find($data['id']);
        $promosi->fill($data);
        if ($promosi->save()) {
            if ($request->tipe_promo == '1') { // untuk diskon
                $array = array_filter($data['diskon_kode_produk']);

                Diskon::where('promosi_id', $promosi->id)->whereNotIn('id', $data['diskon_id'])->update(['status' => 1]);

                foreach ($array as $key => $value) {
                    if (empty($data['diskon_id'][$key])) {
                        $new_diskon = new Diskon;
                        $new_diskon->promosi_id      = $promosi->id;
                        $new_diskon->kode_produk     = $value;
                        $new_diskon->nama_produk     = $data['diskon_nama_produk'][$key];
                        $new_diskon->cost_price      = decimal_for_save($data['diskon_cost_price'][$key]);
                        $new_diskon->harga_terdiskon = decimal_for_save($data['diskon_harga_terdiskon'][$key]);
                        $new_diskon->diskon          = decimal_for_save($data['diskon_diskon'][$key]);
                        $new_diskon->harga_awal      = decimal_for_save($data['harga_awal'][$key]);
                        $new_diskon->save();
                    }else{

                        $diskon = Diskon::find($data['diskon_id'][$key]);

                        if (!is_null($diskon)) {
                            $diskon->kode_produk     = $value;
                            $diskon->nama_produk     = $data['diskon_nama_produk'][$key];
                            $diskon->cost_price      = decimal_for_save($data['diskon_cost_price'][$key]);
                            $diskon->harga_terdiskon = decimal_for_save($data['diskon_harga_terdiskon'][$key]);
                            $diskon->diskon          = decimal_for_save($data['diskon_diskon'][$key]);
                            $diskon->save();
                        }
                    }
                }
            }else if ($request->tipe_promo == '2') { //untuk promo
                $array = array_filter($data['promo_kode_produk']);

                Promo::where('promosi_id', $promosi->id)->whereNotIn('id', $data['promo_id'])->update(['status' => 1]);;

                foreach ($array as $key => $value) {
                    if (empty($data['promo_id'][$key])) {
                        $new_promo = new Promo;
                        $new_promo->promosi_id      = $promosi->id;
                        $new_promo->kode_produk     = $value;
                        $new_promo->nama_produk     = $data['promo_nama_produk'][$key];
                        $new_promo->selling_price   = decimal_for_save($data['promo_selling_price'][$key]);
                        $new_promo->harga_terdiskon = decimal_for_save($data['promo_harga_terdiskon'][$key]);
                        $new_promo->diskon          = decimal_for_save($data['promo_diskon'][$key]);
                        $new_promo->save();
                    }else{
                        $promo = Promo::find($data['promo_id'][$key]);

                        if (!is_null($promo)) {
                            $promo->kode_produk     = $value;
                            $promo->nama_produk     = $data['promo_nama_produk'][$key];
                            $promo->selling_price   = decimal_for_save($data['promo_selling_price'][$key]);
                            $promo->harga_terdiskon = decimal_for_save($data['promo_harga_terdiskon'][$key]);
                            $promo->diskon          = decimal_for_save($data['promo_diskon'][$key]);
                            $promo->save();
                        }
                    }
                    
                }
            }else if ($request->tipe_promo == '3') { //for buyxgety
                $buyxgety = BuyXGetY::find($data['buy_id']);

                if (!is_null($buyxgety)) {
                    $buyxgety->kode_produk_x  = $request['kode_produk_x'];
                    $buyxgety->nama_produk_x  = $request['nama_produk_x'];
                    $buyxgety->harga_jual_x   = decimal_for_save($request['harga_jual_x']);
                    $buyxgety->diskon_x       = decimal_for_save($request['diskon_x']);
                    $buyxgety->harga_diskon_x = decimal_for_save($request['harga_diskon_x']);
                    $buyxgety->kode_produk_y  = $request['kode_produk_y'];
                    $buyxgety->nama_produk_y  = $request['nama_produk_y'];
                    $buyxgety->harga_jual_y   = decimal_for_save($request['harga_jual_y']);
                    $buyxgety->diskon_y       = decimal_for_save($request['diskon_y']);
                    $buyxgety->harga_diskon_y = decimal_for_save($request['harga_diskon_y']);
                    $buyxgety->total_diskon   = decimal_for_save($request['total_diskon']);
                    $buyxgety->save();      
                }
            }else if ($request->tipe_promo == '4') { //for purchase with purchase
                $purchase = Purchase::find($data['purchase_id']);

                if (!is_null($purchase)) {
                    $purchase->kode_produk     = $data['purchase_kode_produk'];
                    $purchase->nama_produk     = $data['purchase_nama_produk'];
                    $purchase->selling_price   = decimal_for_save($data['purchase_selling_price']);
                    $purchase->harga_terdiskon = decimal_for_save($data['purchase_harga_terdiskon']);
                    $purchase->diskon          = decimal_for_save($data['purchase_diskon']);
                    if($purchase->save()){
                        $array = array_filter($data['purchase_list_kode_produk']);

                        if(isset($data['purchase_list_id'])){
                            PurchaseList::where('purchase_id', $purchase->id)->whereNotIn('id', $data['purchase_list_id'])->update(['status' => 1]);
                        }

                        foreach ($array as $key => $value) {
                            if (empty($data['purchase_list_id'][$key])) {
                                $purchaselist = new PurchaseList;
                                $purchaselist->purchase_id   = $purchase->id;
                                $purchaselist->kode_produk   = $value;
                                $purchaselist->nama_produk   = $data['purchase_list_nama_produk'][$key];
                                $purchaselist->selling_price = decimal_for_save($data['purchase_list_selling_price'][$key]);
                                $purchaselist->save();
                            }else{
                                $new_purchaselist = PurchaseList::find($data['purchase_list_id'][$key]);

                                if (!is_null($new_purchaselist)) {
                                    $new_purchaselist->purchase_id   = $purchase->id;
                                    $new_purchaselist->kode_produk   = $value;
                                    $new_purchaselist->nama_produk   = $data['purchase_list_nama_produk'][$key];
                                    $new_purchaselist->selling_price = decimal_for_save($data['purchase_list_selling_price'][$key]);
                                    $new_purchaselist->save();
                                }
                            }
                            
                        }
                    }
                }

            }

            foreach ($data['tmuk_id'] as $key => $id) {
                $tmuk[$key] = ['promosi_id' => $promosi->id,'tmuk_id' => $id]; 
            }
            $promosi->detailTmuk()->sync($tmuk);

            // insert to log audit
            TransLogAudit::setLog([
                'tanggal_transaksi' => date('Y-m-d'),
                'type'              => 'Master Data Promosi',
                'ref'               => '',
                'aksi'              => 'Ubah Data Promosi',
            ]);

            return response([
                'status' => true,
                'data'  => $promosi
            ]);
        } 
    }

    public function destroy($id)
    {
        $promosi = Promosi::find($id);
            if (!is_null($promosi)) {
                if ($promosi->tipe_promo == 1) {
                    $diskon = Diskon::where('promosi_id', $promosi->id);
                    $diskon->update(['status' => 1]);
                }else if ($promosi->tipe_promo == 2) {
                    $promo = Promo::where('promosi_id', $promosi->id);
                    $promo->update(['status' => 1]);
                }else if ($promosi->tipe_promo == 3) {
                    $buyxgety = BuyXGetY::where('promosi_id', $promosi->id)->first();
                    $buyxgety->update(['status' => 1]);
                }else if ($promosi->tipe_promo == 4) {
                    $purchase = Purchase::where('promosi_id', $promosi->id)->first();
                    if (!is_null($purchase)) {
                        $purchaselist = PurchaseList::where('purchase_id', $purchase->id);
                        if ($purchaselist->update(['status' => 1])) {
                            // $purchase->delete();
                            $purchase->update(['status' => 1]);
                        }
                    }
                }

                $data['status'] = 1;
                $promosi->fill($data);
                $promosi->save();

            //asli
            // $promosi = Promosi::find($id);
            // if (!is_null($promosi)) {
            //     if ($promosi->tipe_promo == 1) {
            //         $diskon = Diskon::where('promosi_id', $promosi->id);
            //         $diskon->delete();
            //     }else if ($promosi->tipe_promo == 2) {
            //         $promo = Promo::where('promosi_id', $promosi->id);
            //         $promo->delete();
            //     }else if ($promosi->tipe_promo == 3) {
            //         $buyxgety = BuyXGetY::where('promosi_id', $promosi->id)->first();
            //         $buyxgety->delete();
            //     }else if ($promosi->tipe_promo == 4) {
            //         $purchase = Purchase::where('promosi_id', $promosi->id)->first();
            //         if (!is_null($purchase)) {
            //             $purchaselist = PurchaseList::where('purchase_id', $purchase->id);
            //             if ($purchaselist->delete()) {
            //                 $purchase->delete();
            //             }
            //         }
            //     }
                
            //     $promosi->delete();

            // insert to log audit
            TransLogAudit::setLog([
                'tanggal_transaksi' => date('Y-m-d'),
                'type'              => 'Master Data Promosi',
                'ref'               => '',
                'aksi'              => 'Hapus Data Promosi',
            ]);



            return response([
                'status' => true,
            ]);
        }
    }

    public function AllDiskon()
    {
        return $this->render('modules.utama.produk.promosi.all-diskon');
    }

    public function AllPromo()
    {
        return $this->render('modules.utama.produk.promosi.all-promo');
    }

    //post alldiskon
    public function PostAllDiskon(Request $request)
    {
        $data = $request->all();
        $lsi_id = [];
        $tmuk_id = [];
        // dd($data['tmuk_id']);
        if (isset($data['tmuk_id'])) {
            foreach ($data['tmuk_id'] as $key => $value) {
                $tmuk = Tmuk::find($value);
                $lsi_id[$key] = $tmuk->lsi_id;
                $tmuk_id[$tmuk->lsi_id][$key] = $tmuk->id;
            }
            // dd($tmuk_id);
        }else{
            // dd('tmuk kosong');
            return response(['responseJSON' => ['Harap Pilih TMUK']], 404);
        }

        foreach (array_unique($lsi_id) as $key => $value) {
            $lsi = Lsi::find($value);

            //save di table promosi
            $alldiskon = new Promosi;
            $alldiskon->region_id  = $lsi->region_id;
            $alldiskon->lsi_id     = $lsi->id;
            $alldiskon->tipe_promo = '1';
            $alldiskon->nama = $data['nama'];
            $alldiskon->kode_promo = $data['kode_promo'].$lsi->kode;
            $alldiskon->tgl_awal = $data['tgl_awal'];
            $alldiskon->tgl_akhir = $data['tgl_akhir'];
            if ($alldiskon->save()) {
                // dd($tmuk_id[$alldiskon->lsi_id]);
                $tmuks = [];

                foreach ($tmuk_id[$alldiskon->lsi_id] as $key => $id) {
                    $tmuks[$key] = ['promosi_id' => $alldiskon->id,'tmuk_id' => $id]; 
                }
                // dd($tmuks);
                $alldiskon->detailTmuk()->sync($tmuks);

                if($request->hasFile('upload_data_all_diskon_tmuk')){
                    $path = $request->file('upload_data_all_diskon_tmuk')->getRealPath();
                    $data_excel = Excel::load($path, function($reader) {})->get();
                    $sukses = 0;
                    $gagal = 0;
                    $error = [];

                    // dd($data_excel->toArray());
                    if(!empty($data_excel) && $data_excel->count()){
                        //save di table detail diskon
                        foreach ($data_excel->toArray() as $key => $value) {
                            if(!is_null($value['produk_kode'])){
                                $cek_produk = Produk::where('kode', $value['produk_kode'])->first();
                                    if ($cek_produk ) {
                                        //insert data
                                        $diskon = new Diskon;
                                        $diskon->promosi_id      = $alldiskon->id;
                                        $diskon->kode_produk     = $value['produk_kode'];
                                        $diskon->nama_produk     = $cek_produk->nama;
                                        $diskon->cost_price      = 0;
                                        $diskon->harga_awal      = $value['harga_awal'];
                                        $diskon->harga_terdiskon = $value['harga_terdiskon'];
                                        $diskon->diskon          = $value['harga_awal'] - $value['harga_terdiskon'];

                                        $diskon->created_by = \Auth::user()->id;
                                        $diskon->created_at = date('Y-m-d H:i:s');
                                        // $diskon->save();
                                        if($diskon->save()){
                                            $sukses++;
                                        }
                                    } else {
                                        $gagal++;
                                        $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' tidak terdaftar di system.';
                                    } 
                            }
                        }

                    }
                }
            } else {
                return back()->with('error','Please Check your file, Something is wrong there.');
            }
        }

        //popup alert excell
        if ($error) {
            //create log 
            $record = $error;
            $filename = date("YmdHis");
            $errorlog = Excel::create($filename, function($excel) use ($record){
                $excel->setTitle('Error Log Promosi All Tmuk');
                $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
                $excel->setDescription('Error Log Promosi All Tmuk');

                $excel->sheet('Error Log Promosi All Tmuk ', function($sheet) use ($record){
                    $sheet->row(1, array(
                        'Error Log Promosi All Tmuk '.date("Y-m-d H:i:s"),
                    ));

                    $row = 2;
                    foreach ($record as $val) {
                        $sheet->row($row, array(
                            $val,
                        ));
                        $row=$row;
                        $row++;
                    }
                    $sheet->setWidth(array('A'=>80));
                    $sheet->cells('A1', function($cells){
                        $cells->setAlignment('center');
                        $cells->setBackground('#999999');
                        $cells->setFontColor('#FFFFFF');
                    });

                });
            })->store('xls', storage_path('uploads/log-promosi/diskon'));

            return response([
                'status' => true,
                'upload' => true,
                'sukses' => $sukses,
                'gagal'  => $gagal,
                'log' => '<a href="'.asset('storage/uploads/log-promosi/diskon/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
            ]);
        }else{
            return response([
                'status' => true,
                'upload' => true,
                'sukses' => $sukses,
                'gagal'  => $gagal
            ]);
        }
    }

    //post alldiskon
    public function PostAllPromo(Request $request)
    {
        $data = $request->all();
        $lsi_id = [];
        $tmuk_id = [];

        // dd($data['tmuk_id']);
        if (isset($data['tmuk_id'])) {
            foreach ($data['tmuk_id'] as $key => $value) {
                $tmuk = Tmuk::find($value);
                $lsi_id[$key] = $tmuk->lsi_id;
                $tmuk_id[$tmuk->lsi_id][$key] = $tmuk->id;
            }
            // dd(array_unique($lsi_id));
        }else{
            // dd('tmuk kosong');
            return response(['responseJSON' => ['Harap Pilih TMUK']], 404);
        }

        foreach (array_unique($lsi_id) as $key => $value) {
            $lsi = Lsi::find($value);

            //save di table promosi
            $alldiskon = new Promosi;
            $alldiskon->region_id  = $lsi->region_id;
            $alldiskon->lsi_id     = $lsi->id;
            $alldiskon->tipe_promo = '2';
            $alldiskon->nama = $data['nama'];
            $alldiskon->kode_promo = $data['kode_promo'].$lsi->kode;
            $alldiskon->tgl_awal = $data['tgl_awal'];
            $alldiskon->tgl_akhir = $data['tgl_akhir'];
            if ($alldiskon->save()) {
                // dd($tmuk_id[$alldiskon->lsi_id]);
                $tmuks = [];

                foreach ($tmuk_id[$alldiskon->lsi_id] as $key => $id) {
                    $tmuks[$key] = ['promosi_id' => $alldiskon->id,'tmuk_id' => $id]; 
                }
                // dd($tmuks);
                $alldiskon->detailTmuk()->sync($tmuks);
                
                if($request->hasFile('upload_data_all_promo_tmuk')){
                    $path = $request->file('upload_data_all_promo_tmuk')->getRealPath();
                    $data_excel = Excel::load($path, function($reader) {})->get();
                    $sukses = 0;
                    $gagal = 0;
                    $error = [];

                    // dd($data_excel->toArray());
                    if(!empty($data_excel) && $data_excel->count()){
                        //save di table detail diskon
                        foreach ($data_excel->toArray() as $key => $value) {
                            if(!is_null($value['produk_kode'])){
                                $cek_produk = Produk::where('kode', $value['produk_kode'])->first();
                                    if ($cek_produk ) {
                                        //insert data
                                        $diskon = new Promo;
                                        $diskon->promosi_id      = $alldiskon->id;
                                        $diskon->kode_produk     = $value['produk_kode'];
                                        $diskon->nama_produk     = $cek_produk->nama;
                                        $diskon->selling_price   = $value['harga_awal'];
                                        $diskon->harga_terdiskon = $value['harga_jual'];
                                        $diskon->diskon          = $diskon->selling_price - $diskon->harga_terdiskon;

                                        $diskon->created_by = \Auth::user()->id;
                                        $diskon->created_at = date('Y-m-d H:i:s');
                                        // $diskon->save();
                                        if($diskon->save()){
                                            $sukses++;
                                        }
                                    } else {
                                        $gagal++;
                                        $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' tidak terdaftar di system.';
                                    } 
                            }
                        }

                    }
                }
            } else {
                return back()->with('error','Please Check your file, Something is wrong there.');
            }
        }

        //popup alert excell
        if ($error) {
            //create log 
            $record = $error;
            $filename = date("YmdHis");
            $errorlog = Excel::create($filename, function($excel) use ($record){
                $excel->setTitle('Error Log Promosi All Tmuk');
                $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
                $excel->setDescription('Error Log Promosi All Tmuk');

                $excel->sheet('Error Log Promosi All Tmuk ', function($sheet) use ($record){
                    $sheet->row(1, array(
                        'Error Log Promosi All Tmuk '.date("Y-m-d H:i:s"),
                    ));

                    $row = 2;
                    foreach ($record as $val) {
                        $sheet->row($row, array(
                            $val,
                        ));
                        $row=$row;
                        $row++;
                    }
                    $sheet->setWidth(array('A'=>80));
                    $sheet->cells('A1', function($cells){
                        $cells->setAlignment('center');
                        $cells->setBackground('#999999');
                        $cells->setFontColor('#FFFFFF');
                    });

                });
            })->store('xls', storage_path('uploads/log-promosi/diskon'));

            return response([
                'status' => true,
                'upload' => true,
                'sukses' => $sukses,
                'gagal'  => $gagal,
                'log' => '<a href="'.asset('storage/uploads/log-promosi/diskon/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
            ]);
        }else{
            return response([
                'status' => true,
                'upload' => true,
                'sukses' => $sukses,
                'gagal'  => $gagal
            ]);
        }
    }

}
