<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

class RakAssortmentController extends Controller
{
	protected $link = 'utama/produk/rak-assortment/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Rak by Assortment Type");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Rak by Assortment Type' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'assortment_type',
			    'name' => 'assortment_type',
			    'label' => 'Assortment Type',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'rak_lorong',
			    'name' => 'rak_lorong',
			    'label' => 'Jumlah Rak Lorong',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'rak_dinding',
			    'name' => 'rak_dinding',
			    'label' => 'Jumlah Rak Dinding',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'rak_kasir',
			    'name' => 'rak_kasir',
			    'label' => 'Jumlah Rak Kasir',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.rak-assortment.index');
    }

    public function create()
    {
        return $this->render('modules.utama.produk.rak-assortment.create');
    }

    public function edit($id)
    {
        return $this->render('modules.utama.produk.rak-assortment.edit');
    }
}
