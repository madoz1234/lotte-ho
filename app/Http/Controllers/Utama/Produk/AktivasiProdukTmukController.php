<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\ProdukAssortment;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\JenisAssortment;

class AktivasiProdukTmukController extends Controller
{
    protected $link = 'utama/produk/aktivasi-produk-tmuk/';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("Aktivasi Produk TMUK");
        $this->setSubtitle("&nbsp;");
        $this->setModalSize("small");
        $this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Aktivasi Produk TMUK' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => 'No',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'tmuk.nama',
                'name' => 'tmuk.nama',
                'label' => 'TMUK',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'produk.kode',
                'name' => 'produk.kode',
                'label' => 'Kode Produk',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'produk.nama',
                'name' => 'produk.nama',
                'label' => 'Nama Produk',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'tipe_produk',
                'name' => 'tipe_produk',
                'label' => 'Tipe Produk',
                'searchable' => false,
                'sortable' => false,
                'className' => "left aligned",
            ],
            [
                'data' => 'tipe_barang',
                'name' => 'tipe_barang',
                'label' => 'Tipe Barang',
                'searchable' => false,
                'sortable' => false,
                'className' => "left aligned",
            ],
            [
                'data' => 'jenis_barang',
                'name' => 'jenis_barang',
                'label' => 'Jenis Barang',
                'searchable' => false,
                'sortable' => false,
                'className' => "left aligned",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Detil',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = ProdukTmuk::with('creator','produk', 'tmuk', 'produksetting')
                        ->whereIn('flag', ['0','1']);
                         // ->select('*');

        //Filters
        if ($produk_kode = $request->produk_kode) {
            $records->where('produk_kode', 'like', '%' . $produk_kode . '%');
        }
        // if ($produk_kode = $request->produk_kode) {
        //     $records->where('produk_kode',$produk_kode);
        // }
        
        if ($nama = $request->nama) {
            $records->whereHas('produk', function ($query) use ($nama){
                $query->where('nama', 'ilike', '%' . $nama . '%');
            });
        }
        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode',$tmuk_kode);
        }

        if ($tipe_barang_kode = $request->tipe_barang_kode) {
            $records->whereHas('produksetting', function ($query) use ($tipe_barang_kode){
                $query->where('tipe_barang_kode', 'like', '%' . $tipe_barang_kode . '%');
            });
        }

        if ($jenis_barang_kode = $request->jenis_barang_kode) {
            $records->whereHas('produksetting', function ($query) use ($jenis_barang_kode){
                $query->where('jenis_barang_kode', 'like', '%' . $jenis_barang_kode . '%');
            });
        }

        $flag = $request->flag;
        if ($flag != '') {
            $records->where('flag', $flag);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('status',function($record){
                if($record->flag == 1){
                    return '<b style="font-size: 15px; color: red;" data-content="Produk Tidak Aktif"> Non Aktif </b>';
                }else if($record->flag == 0){
                    return '<b style="font-size: 15px; color: green;" data-content="Produk Aktif"> Aktif </b>';
                }
            })
            // ->addColumn('tmuk_kode', function ($record) {
            //     return $record->tmuk->nama;
            // })
            // ->addColumn('nama_produk', function ($record) {
            //     return $record->produk->nama;
            //     // return ($record->produk) ? $record->produk->nama : '-';
            // })
            ->addColumn('tipe_barang', function ($record) {
                return $record->produk->produksetting->tipebarang->nama;
            })
            ->addColumn('jenis_barang', function ($record) {
                return $record->produk->produksetting->jenisbarang->jenis;
            })
            ->addColumn('tipe_produk',function($record){
                if($record->produk->produksetting->tipe_produk == 1){
                    return 'Produk GMD';
                }else if($record->produk->produksetting->tipe_produk == 0){
                    return 'Produk Non GMD';
                }
            })
            ->addColumn('detil', function ($record) {
                $string = '<button type="button" class="ui mini orange icon add button" data-content="Detil" data-id="'.$record->id.'"> Detil</button>';
                return $string;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit',
                    'id'   => $record->id
                ]);
                // Delete
                // $btn .= $this->makeButton([
                //  'type' => 'delete',
                //  'id'   => $record->id
                // ]);

                return $btn;
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.utama.produk.aktivasi-produk-tmuk.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.aktivasi-produk-tmuk.create');
    }

    public function edit($id)
    {
        $record = ProdukTmuk::find($id);

        return $this->render('modules.utama.produk.aktivasi-produk-tmuk.edit', ['record' => $record]);
    }

    public function destroy($id)
    {
        $rak = ProdukTmuk::find($id);
        $rak->delete();

        return response([
            'status' => true,
        ]);
    }

    public function update(Request $request, $id)
    {
        $rak = ProdukTmuk::find($id);
        $rak->fill($request->all());
        $rak->save();

        return response([
            'status' => true,
            'data'  => $rak
        ]);
    }

    //Function download template Produk
    public function postExportTabel(Request $request)
    {
        // dd($request->all());
        $filter = $request->filter;

        \Excel::load(public_path() . '\template\produk-assortment.xls', function($reader) use ($filter) {
            // get data logsheet
        })->download('xls');
    }

    //Function download template Produk
    public function importexcel()
    {
        return $this->render('modules.utama.produk.aktivasi-produk-tmuk.import');
    }

    //Function upload template Produk
    public function postImportExcel(Request $request)
    {
        $kode = [];
        foreach (ProdukTmuk::get() as $row) {
            $kode[$row->produk_kode] = $row->id;
        }

        // dd($request->hasFile('produk_assortment'));
        if($request->hasFile('produk_tmuk')){
            $path = $request->file('produk_tmuk')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();
            $sukses = 0;
            $gagal = 0;
            $error = [];

            if(!empty($data) && $data->count()){
                foreach ($data->toArray() as $key => $value) {
                    if(!is_null($value['produk_kode'])){ //jika data tidak boleh kosong tambahkan dengan &&

                        //cek tmuk
                        $cek_tmuk = Tmuk::where('kode', $value['tmuk_kode'])->first();
                        if ($cek_tmuk) { 
                            //cek produk
                            $cek_produk = ProdukSetting::where('produk_kode', $value['produk_kode'])->first();
                            if ($cek_produk) {
                                // ins/updt
                                $kode_tmuk = $value['tmuk_kode'];
                                $check = ProdukTmuk::where('tmuk_kode', $kode_tmuk)
                                                    ->where('produk_kode', $value['produk_kode'])->first();
                                // cek assortment
                                if (!$check) {
                                    $record              = new ProdukTmuk();
                                    $record->created_by  = \Auth::user()->id;
                                    $record->created_at  = date('Y-m-d H:i:s');
                                    $record->tmuk_kode   = $kode_tmuk;
                                    $record->produk_kode = $value['produk_kode'];
                                    if($record->save()){
                                        $sukses++;
                                    }
                                }else{
                                    $gagal++;
                                    $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' sudah terdaftar dalam system.';
                                }
                                
                            } else {
                                $gagal++;
                                $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' tidak terdaftar dalam system.';
                            }
                        } else {
                            $gagal++;
                            $error[$value['tmuk_kode']] = 'Tmuk Kode '.$value['tmuk_kode'].' tidak terdaftar dalam system.';
                        }
                    }
                }

                // if(!empty($insert)){
                //     ProdukTmuk::insert($insert);
                //     return back()->with('success','Insert Record successfully.');
                // }

                if ($error) {
                    //create log 
                    $record = $error;
                    $filename = date("YmdHis");
                    $errorlog = Excel::create($filename, function($excel) use ($record){
                        $excel->setTitle('Error Log Aktivasi Tmuk');
                        $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
                        $excel->setDescription('Error Log Aktivasi Tmuk');

                        $excel->sheet('Error Log Aktivasi Tmuk ', function($sheet) use ($record){
                            $sheet->row(1, array(
                                'Error Log Aktivasi Tmuk '.date("Y-m-d H:i:s"),
                            ));

                            $row = 2;
                            foreach ($record as $val) {
                                $sheet->row($row, array(
                                    $val,
                                ));
                                $row=$row;
                                $row++;
                            }
                            $sheet->setWidth(array('A'=>50));
                            $sheet->cells('A1', function($cells){
                                $cells->setAlignment('center');
                                $cells->setBackground('#999999');
                                $cells->setFontColor('#FFFFFF');
                            });

                        });
                    })->store('xls', storage_path('uploads/log-aktivasi-produk-tmuk'));

                    return response([
                        'status' => true,
                        'upload' => true,
                        'sukses' => $sukses,
                        'gagal'  => $gagal,
                        'log' => '<a href="'.asset('storage/uploads/log-aktivasi-produk-tmuk/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
                    ]);
                }else{
                    return response([
                        'status' => true,
                        'upload' => true,
                        'sukses' => $sukses,
                        'gagal'  => $gagal
                    ]);
                }
            }
        }

        return back()->with('error','Please Check your file, Something is wrong there.');
    }
    //Function upload template Produk
    
    //Aktivasi Produk Tmuk
    public function aktivasiProdukForm()
    {
        return $this->render('modules.utama.produk.aktivasi-produk-tmuk.form-aktivasi');
    }

    //Aktivasi Produk Tmuk Proses
    public function aktivasiProduk(Request $request)
    {
        $kode = [];

        // get tmuk
        $tmuk = Tmuk::with('assortment')->find($request->aktivasi_tmuk);
        // dd($tmuk);
        // cek tmuk
        if (!$tmuk) {
            return response(['responseJSON' => ['Tidak terdapat TMUK']], 404);
        }

        // get produk assorment by tmuk assortment
        $produk = ProdukAssortment::where('assortment_type_id', $tmuk->assortment->id)->get();

        // check produk
        if (count($produk) <= 0) {
            return response(['responseJSON' => ['Tidak terdapat produk pada assortment type ' . $tmuk->assortment->nama]], 404);
        } 

        // loop produk assortment
        foreach($produk as $p)
        {

            $check = ProdukTmuk::where('tmuk_kode', $tmuk->kode)->where('produk_kode', $p->produk_kode)->first();

            if(!$check)
            {
                $save = new ProdukTmuk();
                $save->created_by  = \Auth::user()->id;
                $save->created_at  = date('Y-m-d H:i:s');
                $save->tmuk_kode   = $tmuk->kode;
                $save->produk_kode = $p->produk_kode;
                $save->save();
            }
        }

        return response(['status' => 'success'], 200);
    }
}