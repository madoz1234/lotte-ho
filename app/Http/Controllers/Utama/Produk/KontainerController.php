<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\KontainerRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\Kontainer;
use Lotte\Models\Master\Log\LogKontainer;
use Lotte\Models\Trans\TransLogAudit;

class KontainerController extends Controller
{
	protected $link = 'utama/produk/kontainer/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Kontainer");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Kontainer' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Tipe Kontainer',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'tinggi_luar',
			    'name' => 'tinggi_luar',
			    'label' => 'Tinggi Luar (cm)',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'tinggi_dalam',
			    'name' => 'tinggi_dalam',
			    'label' => 'Tinggi Dalam (cm)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'panjang_atas',
			    'name' => 'panjang_atas',
			    'label' => 'Panjang Atas (cm)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'panjang_bawah',
			    'name' => 'panjang_bawah',
			    'label' => 'Panjang Bawah (cm)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'lebar_atas',
			    'name' => 'lebar_atas',
			    'label' => 'Lebar Atas (cm)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'lebar_bawah',
			    'name' => 'lebar_bawah',
			    'label' => 'Lebar Bawah (cm)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'volume',
			    'name' => 'volume',
			    'label' => 'Volume (L)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'tipe_box',
			    'name' => 'tipe_box',
			    'label' => 'Tipe Boks ',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '20px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Kontainer::with('creator')
						 ->select('*');
        // $record = Kontainer::select('*');
        // dd($record);

		// Init Sort
  //       if (!isset(request()->order[0]['column'])) {
  //           // $records->->sort();
  //           $records->orderBy('kode');
  //       }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('tipe_box',function($record){
				if($record->tipe_box == 1){
					return 'InnerBox';
				}else if($record->tipe_box == 0){
					return 'OutBox';
				}
			})
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                // $btn .= $this->makeButton([
                // 	'type' => 'delete',
                // 	'id'   => $record->id
                // ]);
                //detail
                // $btn .= $this->makeButton([
                //     'type' => 'detail',
                //     'id'   => $record->id
                // ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.kontainer.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.kontainer.create');
    }

    public function store(KontainerRequest $request)
    {
    	$kontainer = new Kontainer;
    	$kontainer->fill($request->all());
    	$kontainer->save();

    	//log insert
        $log = new LogKontainer;
        $log->kontainer_id = $kontainer->id;
        $log->nama = $request->nama;
        $log->tinggi_luar = $request->tinggi_luar;
        $log->tinggi_dalam = $request->tinggi_dalam;
        $log->panjang_atas = $request->panjang_atas;
        $log->panjang_bawah = $request->panjang_bawah;
        $log->lebar_atas = $request->lebar_atas;
        $log->lebar_bawah = $request->lebar_bawah;
        $log->volume = $request->volume;
        $log->tipe_box = $request->tipe_box;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Kontainer',
            'ref'               => '',
            'aksi'              => 'Tambah Data Kontainer',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $kontainer
    	]);
    }

    public function edit($id)
    {
    	$record = Kontainer::find($id);

        return $this->render('modules.utama.produk.kontainer.edit', ['record' => $record]);
    }

    public function detail($id)
    {
        $record = Kontainer::find($id);

        return $this->render('modules.utama.produk.kontainer.detail', ['record' => $record]);
    }

    public function update(KontainerRequest $request, $id)
    {
    	$kontainer = Kontainer::find($id);
    	$kontainer->fill($request->all());
    	$kontainer->save();

    	//log update
        $log = new LogKontainer;
        $log->kontainer_id = $kontainer->id;
        $log->nama = $kontainer->nama;
        $log->tinggi_luar = $kontainer->tinggi_luar;
        $log->tinggi_dalam = $kontainer->tinggi_dalam;
        $log->panjang_atas = $kontainer->panjang_atas;
        $log->panjang_bawah = $kontainer->panjang_bawah;
        $log->lebar_atas = $kontainer->lebar_atas;
        $log->lebar_bawah = $kontainer->lebar_bawah;
        $log->volume = $kontainer->volume;
        $log->tipe_box = $kontainer->tipe_box;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Kontainer',
            'ref'               => '',
            'aksi'              => 'Ubah Data Kontainer',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $kontainer
    	]);
    }

    public function destroy($id)
    {
    	$kontainer = Kontainer::find($id);
    	$kontainer->delete();

    	//log delete
        $log = new LogKontainer;
        $log->kontainer_id = $kontainer->id;
        $log->nama = $kontainer->nama;
        $log->tinggi_luar = $kontainer->tinggi_luar;
        $log->tinggi_dalam = $kontainer->tinggi_dalam;
        $log->panjang_atas = $kontainer->panjang_atas;
        $log->panjang_bawah = $kontainer->panjang_bawah;
        $log->lebar_atas = $kontainer->lebar_atas;
        $log->lebar_bawah = $kontainer->lebar_bawah;
        $log->volume = $kontainer->volume;
        $log->tipe_box = $kontainer->tipe_box;

        $log->save();

    	return response([
    		'status' => true,
    	]);
    }
}
