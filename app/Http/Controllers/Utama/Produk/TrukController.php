<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Produk\TrukRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use DB;
use Excel;

//Models
use Lotte\Models\Master\Truk;
use Lotte\Models\Master\Log\LogTruk;
use Lotte\Models\Trans\TransLogAudit;

class TrukController extends Controller
{
	protected $link = 'utama/produk/truk/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Truk");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Truk' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'tipe',
			    'name' => 'tipe',
			    'label' => 'Tipe Truk',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'tinggi',
			    'name' => 'tinggi',
			    'label' => 'Tinggi (cm)',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'panjang',
			    'name' => 'panjang',
			    'label' => 'Panjang (cm)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'lebar',
			    'name' => 'lebar',
			    'label' => 'Lebar (cm)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'kapasitas',
			    'name' => 'kapasitas',
			    'label' => 'Kapasitas (ton)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'volume',
			    'name' => 'volume',
			    'label' => 'Volume (L)',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Truk::with('creator')
						 ->select('*');
        // $record = Truk::select('*');
        // dd($record);

		// Init Sort
  //       if (!isset(request()->order[0]['column'])) {
  //           // $records->->sort();
  //           $records->orderBy('kode');
  //       }

        //Filters
        if ($tipe = $request->tipe) {
            $records->where('tipe', 'ilike', '%' . $tipe . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.truk.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.truk.create');
    }

    public function store(TrukRequest $request)
    {
    	$truk = new Truk;
    	$truk->fill($request->all());
    	$truk->save();

    	//log insert
        $log = new LogTruk;
        $log->truk_id = $truk->id;
        $log->tipe = $request->tipe;
        $log->tinggi = $request->tinggi;
        $log->panjang = $request->panjang;
        $log->lebar = $request->lebar;
        $log->kapasitas = $request->kapasitas;
        $log->volume = $request->volume;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Truk',
            'ref'               => '',
            'aksi'              => 'Tambah Data Truk',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $truk
    	]);
    }

    public function edit($id)
    {
    	$record = Truk::find($id);
        return $this->render('modules.utama.produk.truk.edit', ['record' => $record]);
    }

    public function update(TrukRequest $request, $id)
    {
    	$truk = Truk::find($id);
    	$truk->fill($request->all());
    	$truk->save();

    	//log insert
        $log = new LogTruk;
        $log->truk_id = $truk->id;
        $log->tipe = $truk->tipe;
        $log->tinggi = $truk->tinggi;
        $log->panjang = $truk->panjang;
        $log->lebar = $truk->lebar;
        $log->kapasitas = $truk->kapasitas;
        $log->volume = $truk->volume;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Truk',
            'ref'               => '',
            'aksi'              => 'Ubah Data Truk',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $truk
    	]);
    }

    public function destroy($id)
    {
    	$truk = Truk::find($id);
    	$truk->delete();

    	//log insert
        $log = new LogTruk;
        $log->truk_id = $truk->id;
        $log->tipe = $truk->tipe;
        $log->tinggi = $truk->tinggi;
        $log->panjang = $truk->panjang;
        $log->lebar = $truk->lebar;
        $log->kapasitas = $truk->kapasitas;
        $log->volume = $truk->volume;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Truk',
            'ref'               => '',
            'aksi'              => 'Hapus Data Truk',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
    
    public function importexcel()
    {
        return $this->render('modules.utama.produk.truk.upload');
    }

	public function postImportExcel(Request $request)
	{

		if($request->hasFile('upload_data_produk')){
			$path = $request->file('upload_data_produk')->getRealPath();
			// $path = $request->upload_data_produk->move(storage_path('upload-produk'), $request->upload_data_produk->getClientOriginalName());
			$data = Excel::load($path, function($reader) {})->get();

			if(!empty($data) && $data->count()){
				foreach ($data->toArray() as $key => $value) {
					if(!is_null($value['tipe'])){
						$insert[] = [
								'tipe' => $value['tipe'], 
								'tinggi' => $value['tinggi'], 
								'panjang' => $value['panjang'], 
								'lebar' => $value['lebar'], 
								'kapasitas' => $value['kapasitas'], 
								'volume' => $value['volume'], 
								'created_at' => $value['created_at'], 
								'created_by' => $value['created_by']
							];
					}
				}

				if(!empty($insert)){
					Truk::insert($insert);
					return back()->with('success','Insert Record successfully.');
				}

			}

		}
		return back()->with('error','Please Check your file, Something is wrong there.');
	}
    
}
