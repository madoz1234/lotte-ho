<?php

namespace Lotte\Http\Controllers\Utama\produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

//model
use Lotte\Models\Master\AssortmentTypeProduk;
use Lotte\Models\Master\ProdukHarga;
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Master\ProdukSetting;
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Produk;
use Lotte\Models\Master\ProdukLsi;

//Libraries
use Datatables;
use Carbon\Carbon;
use Lotte\Libraries\ApiGmd;
use Excel;

class HargaController extends Controller
{
	protected $link = 'utama/produk/harga/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Harga");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Harga' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '20px',
			],
			/* --------------------------- */
			[
			    'data' => 'produk_kode',
			    'name' => 'produk_kode',
			    'label' => 'Kode Produk',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'produk',
			    'name' => 'produk',
			    'label' => 'Nama Produk',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'tmuk_kode',
			    'name' => 'tmuk_kode',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'hpp',
			    'name' => 'hpp',
			    'label' => 'Cost Price (Rp)',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'suggest_price',
			    'name' => 'suggest_price',
			    'label' => 'Suggested Rounded <br> Selling Price (Rp)',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'change_price',
			    'name' => 'change_price',
			    'label' => 'Changed Selling Price (Rp)',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'margin_amount',
			    'name' => 'margin_amount',
			    'label' => 'Cost Price (Rp)',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    // 'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = ProdukHarga::with('creator', 'produk')
						 ->select('*');
        // dd($record);

		// Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($produk_kode = $request->produk_kode) {
            $records->where('produk_kode', 'like', '%' . $produk_kode . '%');
        }
        // if ($produk_kode = $request->produk_kode) {
        //     $records->where('produk_kode',$produk_kode);
        // }
        if ($tmuk_kode = $request->tmuk_kode) {
            $records->where('tmuk_kode',$tmuk_kode);
        }
        
        if ($nama = $request->nama) {
            $records->whereHas('produk', function ($query) use ($nama){
                $query->where('nama', 'ilike', '%' . $nama . '%');
            });
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tmuk_kode', function ($record) {
                return $record->tmuk->nama;
            })
           ->addColumn('produk', function ($record) {
                return $record->produk->nama;
            })
           ->addColumn('hpp', function ($record) {
                return FormatNumber($record->map);
            })
           ->addColumn('suggest_price', function ($record) {
                return FormatNumber($record->suggest_price);
            })
           ->addColumn('change_price', function ($record) {
                return FormatNumber($record->change_price);
            })
           ->addColumn('margin_amount', function ($record) {
                return FormatNumber($record->margin_amount);
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                // //Edit
                // $btn .= $this->makeButton([
                // 	'type' => 'edit',
                // 	'id'   => $record->id
                // ]);
                // // Delete
                // $btn .= $this->makeButton([
                // 	'type' => 'delete',
                // 	'id'   => $record->id
                // ]);
                $btn .= $this->makeButton([
                    'type' => 'detail',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.produk.harga.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.harga.create');
    }

    public function detail($id)
    {
        $record = ProdukHarga::find($id);
        return $this->render('modules.utama.produk.harga.edit', ['record' => $record]);
    }

    public function edit($id)
    {
        $record = ProdukHarga::find($id);
        return $this->render('modules.utama.produk.harga.edit', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
        $produkharga = ProdukHarga::find($id);
        $produkharga->fill($request->all());
        $produkharga->save();

        return response([
            'status' => true,
            'data'  => $produkharga
        ]);
    }

    public function destroy($id)
    {
    	$rak = ProdukHarga::find($id);
    	$rak->delete();

    	return response([
    		'status' => true,
    	]);
    }

    public function generateHargaForm()
    {
    	return $this->render('modules.utama.produk.harga.generate-form');
    }

    public function generateHarga(Request $request)
    {
    	$tmuk_kode = $request->tmuk_kode;

    	// get tmuk
    	$tmuk = Tmuk::where('kode', $tmuk_kode)->first();

    	if ($tmuk) {
    		// get produk by assortment type
    		$res_produks = AssortmentTypeProduk::where('assortment_type_id', $tmuk->assortment_type_id)->get();
    		// transform to array
    		$produks = [];
    		foreach ($res_produks as $row) {
    			$produks[] = $row->produk_kode;
    		}
            // dd($produks);

    		// get data from GMD
    		$response = ApiGmd::getProduct($lsi_kode, $produks); // edited

    		// cek is json // ke deui
            $json = json_decode($response, true);

            if ($json['status'] == true) { // jika terdapat info dari gmd
            	foreach ($json['data'] as $val) {
            		// insert/update harga produk
            		$prod_exist = ProdukHarga::where('tmuk_kode', $tmuk_kode)->where('produk_kode', $val['PROD_CD'])->first();
            		if ($prod_exist) { // maka update

            		} else { // maka insert
            			$produk = new ProdukHarga();
            		}
            	}
            }

    	}

    }

    public function uploadHarga()
    {
        return $this->render('modules.utama.produk.harga.upload');
    }
    
    public function postUploadHarga(Request $request)
    {
        if($request->hasFile('upload_harga_tmuk')){
            $path = $request->file('upload_harga_tmuk')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();
            $sukses = 0;
            $gagal = 0;
            $error = [];

            // dd($data->toArray());
            if(!empty($data) && $data->count()){
                foreach ($data->toArray() as $key => $value) {
                    // dd($value);
                    if(!is_null($value['produk_kode'])){ //jika data tidak boleh kosong tambahkan dengan &&
                        //test
                        $cek_tmuk = Tmuk::where('kode', $value['tmuk_kode'])->first();
                        if ($cek_tmuk) {
                            $cek_produk_gmd = ProdukTmuk::with('hargaBeli')->where('produk_kode', $value['produk_kode'])->first();
                            // Cek Produk Ada Tidak
                        if ($cek_produk_gmd) {
                                //Produk Lsi
                            $cekproduklsi = ProdukLsi::where('produk_kode', $value['produk_kode'])->first();
                            if ($cekproduklsi ) {
                                $setting = ProdukHarga::where('tmuk_kode', $value['tmuk_kode'])->where('produk_kode', $value['produk_kode'])->first();
                                if (!$setting ) {
                                    //insert data
                                    $setting = new ProdukHarga();
                                    $setting->tmuk_kode      = $value['tmuk_kode'];
                                    $setting->produk_kode    = $value['produk_kode'];
                                    $setting->cost_price     = $value['cost_price'];
                                    $setting->suggest_price  = $value['suggest_price'];
                                    $setting->change_price   = $value['change_price'];
                                    $setting->margin_amount  = $value['margin'];
                                    $setting->map            = $value['hpp'];
                                    $setting->gmd_price      = $cek_produk_gmd->hargaBeli ? $cek_produk_gmd->hargaBeli->curr_sale_prc : 0;

                                    $setting->created_by = \Auth::user()->id;
                                    $setting->created_at = date('Y-m-d H:i:s');
                                    // var_dump($value['produk_kode'].'->'.'Produk Tersimpan !');
                                } else {
                                    //update data
                                    $setting->cost_price     = $value['cost_price'];
                                    $setting->suggest_price  = $value['suggest_price'];
                                    $setting->change_price   = $value['change_price'];
                                    $setting->margin_amount  = $value['margin'];
                                    $setting->map            = $value['hpp'];
                                    $setting->gmd_price      = $cek_produk_gmd->hargaBeli ? $cek_produk_gmd->hargaBeli->curr_sale_prc : 0;

                                    $setting->updated_at      = date('Y-m-d H:i:s');
                                    $setting->updated_by      = 1;
                                    // var_dump($value['produk_kode'].'->'.'Produk Sudah Ada !');
                                }       
                                $setting->save();
                                        $sukses++;
                                
                            } else {
                                //Produk Lsi Belum Terdaftar
                                $setting = ProdukHarga::where('tmuk_kode', $value['tmuk_kode'])->where('produk_kode', $value['produk_kode'])->first();
                                if (!$setting ) {
                                    //insert data
                                    $setting = new ProdukHarga();
                                    $setting->tmuk_kode      = $value['tmuk_kode'];
                                    $setting->produk_kode    = $value['produk_kode'];
                                    $setting->cost_price     = $value['cost_price'];
                                    $setting->suggest_price  = $value['suggest_price'];
                                    $setting->change_price   = $value['change_price'];
                                    $setting->margin_amount  = $value['margin'];
                                    $setting->map            = $value['hpp'];
                                    $setting->gmd_price      = $cek_produk_gmd->hargaBeli ? $cek_produk_gmd->hargaBeli->curr_sale_prc : 0;

                                    $setting->created_by = \Auth::user()->id;
                                    $setting->created_at = date('Y-m-d H:i:s');
                                    // var_dump($value['produk_kode'].'->'.'Produk Tersimpan !');
                                } else {
                                    //update data
                                    $setting->cost_price     = $value['cost_price'];
                                    $setting->suggest_price  = $value['suggest_price'];
                                    $setting->change_price   = $value['change_price'];
                                    $setting->margin_amount  = $value['margin'];
                                    $setting->map            = $value['hpp'];
                                    $setting->gmd_price      = $cek_produk_gmd->hargaBeli ? $cek_produk_gmd->hargaBeli->curr_sale_prc : 0;

                                    $setting->updated_at      = date('Y-m-d H:i:s');
                                    $setting->updated_by      = 1;
                                    // var_dump($value['produk_kode'].'->'.'Produk Sudah Ada !');
                                }       
                                $setting->save();
                                        $sukses++;

                                // if($setting->save())
                                //     {
                                //         $sukses++;
                                //     }
                                // var_dump($value['produk_kode'].'->'.'Produk Belum Terdaftar DiLsi !');
                            }
                        }
                        else{
                            // var_dump($value['produk_kode'].'->'.'Produk Tidak Ada Disystem !');
                            $gagal++;
                                $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' Belum Teraktivasi.';
                        }
                        } else {
                            // var_dump($value['tmuk_kode'].'->'.'Tmuk Tidak Ada Disystem !');
                            $gagal++;
                                $error[$value['tmuk_kode']] = 'Tmuk Kode '.$value['tmuk_kode'].' Tidak Ada Disystem.';
                        }
                        
                        //test
                        //cek kode tmuk terdaftar di master tmuk
                        // $cektmuk = Tmuk::where('kode', $value['tmuk_kode'])->first();
                        // if ($cektmuk) {
                        //     //cek tipe produk gmd
                        //     $tipeproduk = Produk::with('produksetting','produklsi')->where('kode', $value['produk_kode'])->first();
                        //     if ($tipeproduk->produksetting->tipe_produk == 1) {
                        //         //cek produk sudah teraktivasi blum di produk tmuk
                        //         $check = ProdukTmuk::where('produk_kode', $value['produk_kode'])->first();
                        //             if ($check) {
                        //                 //cek produk dan harga table harga_tmuk
                        //                 $produkharga = Produkharga::where('tmuk_kode', $value['tmuk_kode'])->where('produk_kode', $value['produk_kode'])->first();
                        //                 if ($produkharga) {
                        //                     $gagal++;
                        //                     $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' sudah terdaftar dalam Tmuk '.$value['tmuk_kode'];
                        //                 } else {
                        //                     $record                 = new ProdukHarga;
                        //                     $record->tmuk_kode      = $value['tmuk_kode'];
                        //                     $record->produk_kode    = $value['produk_kode'];
                        //                     $record->cost_price     = $value['cost_price'];
                        //                     $record->suggest_price  = $value['suggest_price'];
                        //                     $record->change_price   = $value['change_price'];
                        //                     $record->margin_amount  = $value['margin'];
                        //                     $record->map            = $value['hpp'];
                        //                     $record->gmd_price      = $tipeproduk->produklsi->curr_sale_prc;

                        //                     $record->created_by = \Auth::user()->id;
                        //                     $record->created_at = date('Y-m-d H:i:s');
                        //                     if($record->save())
                        //                     {
                        //                         $sukses++;
                        //                     }
                        //                 }
                                        
                        //             }else{
                        //                 $gagal++;
                        //                 $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' tidak terdaftar dalam produk tmuk.';
                        //             }
                        //     } else {
                        //         //bukan tipe produk gmd
                                
                        //         //cek produk sudah teraktivasi blum di produk tmuk
                        //         $check = ProdukTmuk::where('produk_kode', $value['produk_kode'])->first();
                        //         if ($check) {
                        //             //cek produk dan harga table harga_tmuk
                        //             $produkharga = Produkharga::where('tmuk_kode', $value['tmuk_kode'])->where('produk_kode', $value['produk_kode'])->first();

                        //             if ($produkharga) {
                        //                 $gagal++;
                        //                 $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' sudah terdaftar dalam Tmuk '.$value['tmuk_kode'];
                        //             } else {
                        //                 $record                 = new ProdukHarga;
                        //                 $record->tmuk_kode      = $value['tmuk_kode'];
                        //                 $record->produk_kode    = $value['produk_kode'];
                        //                 $record->cost_price     = $value['cost_price'];
                        //                 $record->suggest_price  = $value['suggest_price'];
                        //                 $record->change_price   = $value['change_price'];
                        //                 $record->margin_amount  = $value['margin'];
                        //                 $record->map            = $value['hpp'];

                        //                 $record->created_by = \Auth::user()->id;
                        //                 $record->created_at = date('Y-m-d H:i:s');
                        //                 if($record->save())
                        //                 {
                        //                     $sukses++;
                        //                 }
                        //             }
                                    
                        //         }else{
                        //             $gagal++;
                        //             $error[$value['produk_kode']] = 'Produk Kode '.$value['produk_kode'].' tidak terdaftar dalam produk tmuk.';
                        //         }
                        //     }
                            
                        // } else {
                        //     $gagal++;
                        //         $error[$value['tmuk_kode']] = 'Tmuk Kode '.$value['tmuk_kode'].' tidak terdaftar dalam system.';
                        // }
                        
                    }
                }

                if ($error) {
                    //create log 
                    $record = $error;
                    $filename = date("YmdHis");
                    $errorlog = Excel::create($filename, function($excel) use ($record){
                        $excel->setTitle('Error Log Harga Tmuk');
                        $excel->setCreator('Aplikasi Akuntansi')->setCompany('Lotte');
                        $excel->setDescription('Error Log Harga Tmuk');

                        $excel->sheet('Error Log Harga Tmuk ', function($sheet) use ($record){
                            $sheet->row(1, array(
                                'Error Log Harga Tmuk '.date("Y-m-d H:i:s"),
                            ));

                            $row = 2;
                            foreach ($record as $val) {
                                $sheet->row($row, array(
                                    $val,
                                ));
                                $row=$row;
                                $row++;
                            }
                            $sheet->setWidth(array('A'=>80));
                            $sheet->cells('A1', function($cells){
                                $cells->setAlignment('center');
                                $cells->setBackground('#999999');
                                $cells->setFontColor('#FFFFFF');
                            });

                        });
                    })->store('xls', storage_path('uploads/log-harga-tmuk'));

                    return response([
                        'status' => true,
                        'upload' => true,
                        'sukses' => $sukses,
                        'gagal'  => $gagal,
                        'log' => '<a href="'.asset('storage/uploads/log-harga-tmuk/'.$errorlog->filename.'.xls').'" target="_blank">Download Log</a>',
                    ]);
                }else{
                    return response([
                        'status' => true,
                        'upload' => true,
                        'sukses' => $sukses,
                        'gagal'  => $gagal
                    ]);
                }
            }
        }
    }

}
