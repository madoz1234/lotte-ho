<?php

namespace Lotte\Http\Controllers\Utama\Produk;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

//Models
use Lotte\Models\Master\DivisiProduk;
use Lotte\Models\Master\Kategori1;
use Lotte\Models\Master\Kategori2;
use Lotte\Models\Master\Kategori3;
use Lotte\Models\Master\Kategori4;

use Datatables;

class KategoriController extends Controller
{
    protected $link = 'utama/produk/kategori/';
    protected $array_table = [
            'struct_divisi'=>[
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'kode',
                    'name' => 'kode',
                    'label' => 'Kode',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'nama',
                    'name' => 'nama',
                    'label' => 'Divisi',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'created_at',
                    'name' => 'created_at',
                    'label' => 'Dibuat pada ',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                ]
            ],
            'struct_kat1'=>[
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'divisi_kode',
                    'name' => 'divisi_kode',
                    'label' => 'Kode Divisi',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'kode',
                    'name' => 'kode',
                    'label' => 'Kode',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'nama',
                    'name' => 'nama',
                    'label' => 'Nama',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'created_at',
                    'name' => 'created_at',
                    'label' => 'Dibuat pada ',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                ]
            ],

            'struct_kat2'=>[
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'divisi_kode',
                    'name' => 'divisi_kode',
                    'label' => 'Divisi Kode',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'kategori1_kode',
                    'name' => 'kategori1_kode',
                    'label' => 'Kode Kategori 1',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'kode',
                    'name' => 'kode',
                    'label' => 'Kode',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'nama',
                    'name' => 'nama',
                    'label' => 'Nama',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'created_at',
                    'name' => 'created_at',
                    'label' => 'Dibuat pada ',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                ]
            ],

            'struct_kat3'=>[
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'divisi_kode',
                    'name' => 'divisi_kode',
                    'label' => 'Divisi Kode',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'kategori2_kode',
                    'name' => 'kategori2_kode',
                    'label' => 'Kode Kategori 2',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'kode',
                    'name' => 'kode',
                    'label' => 'KODE',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'nama',
                    'name' => 'nama',
                    'label' => 'Nama',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'created_at',
                    'name' => 'created_at',
                    'label' => 'Dibuat pada ',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                ]
            ],

            'struct_kat4'=>[
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => "center aligned",
                    'width' => '40px',
                ],
                /* --------------------------- */
                [
                    'data' => 'divisi_kode',
                    'name' => 'divisi_kode',
                    'label' => 'Divisi Kode',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'kategori3_kode',
                    'name' => 'kategori3_kode',
                    'label' => 'Kode Kategori 3',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'kode',
                    'name' => 'kode',
                    'label' => 'Kode',
                    'searchable' => false,
                    'sortable' => true,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'nama',
                    'name' => 'nama',
                    'label' => 'Nama',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                ],
                [
                    'data' => 'created_at',
                    'name' => 'created_at',
                    'label' => 'Dibuat pada ',
                    'searchable' => false,
                    'sortable' => false,
                    'className' => "center aligned",
                    'width' => '150px',
                ]
            ],
        ];

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Kategori Produk");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("mini");
        $this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Produk' => '#', 'Kategori Produk' => '#']);
	}

    // GRID VIEW
    public function gridKat1(Request $request)
    {
        $records = Kategori1::select('*');

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('divisi_kode',function($record){
                return $record->divisi->nama;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->make(true);
    }

    public function gridKat2(Request $request)
    {
        $records = Kategori2::select('*');

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('kategori1_kode',function($record){
                return $record->kategori1->nama;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->make(true);
    }

    public function gridKat3(Request $request)
    {
        $records = Kategori3::select('*');

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('kategori2_kode',function($record){
                return $record->kategori2->nama;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->make(true);
    }

    public function gridKat4(Request $request)
    {
        $records = Kategori4::select('*');

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('kategori3_kode',function($record){
                return $record->kategori3->nama;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->make(true);
    }

    public function gridDivisi(Request $request)
    {
        $records = DivisiProduk::select('*');

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->make(true);
    }

    // public function grid(Request $request){
    //     $type = explode("_", $request->type);
    //     if($type[0] !== ''){
    //         return view('modules.utama.produk.kategori.lists.parent');
    //     }
    // }
    public function grid(Request $request){
        // $type = explode("_", $request->type);
        // if($type[0] !== ''){
        //     return view('modules.utama.produk.kategori.lists.parent');
        // }
        $table = $this->array_table;
        $type = explode("_", $request->type);
        switch ($type[0]) {
            case 'kat1':
                    $data['tableStruct'] = $table['struct_kat1'];
                    return view('modules.utama.produk.kategori.lists.kategori1', $data);
                break;
            case 'kat2':
                    $data['tableStruct'] = $table['struct_kat2'];
                    return view('modules.utama.produk.kategori.lists.kategori2', $data);
                break;
            case 'kat3':
                    $data['tableStruct'] = $table['struct_kat3'];
                    return view('modules.utama.produk.kategori.lists.kategori3', $data);
                break;
            case 'kat4':
                    $data['tableStruct'] = $table['struct_kat4'];
                    return view('modules.utama.produk.kategori.lists.kategori4', $data);
                break;
            case 'data_divisi':
                    $data['tableStruct'] = $table['struct_divisi'];
                    return view('modules.utama.produk.kategori.lists.divisi', $data);
                break;
            default:
                    $data['tableStruct'] = $table['struct_divisi'];
                    return view('modules.utama.produk.kategori.lists.divisi', $data);
                break;
        }
    } 

    public function index()
    {
        $data_divisi = DivisiProduk::with('kategori1', 'kategori1.kategori2', 'kategori1.kategori2.kategori3', 'kategori1.kategori2.kategori3.kategori4')->get();
        // dd($data);
        return $this->render('modules.utama.produk.kategori.index', [
            'data_div'  => $data_divisi, 
        ]);
    }

    public function create()
    {
        return $this->render('modules.utama.produk.kategori.create');
    }

    public function edit($id)
    {
        return $this->render('modules.utama.produk.kategori.edit');
    }

    
}
