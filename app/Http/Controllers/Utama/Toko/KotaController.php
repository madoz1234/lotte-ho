<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\KotaRequest;
use Lotte\Http\Controllers\Controller;
use Excel;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\Kota;
// use Lotte\Models\Master\Log\LogRegion;
// use Lotte\Models\Trans\TransLogAudit;

class KotaController extends Controller
{
	protected $link = 'utama/toko/kota/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Kota");
		$this->setSubtitle("&nbsp;");
		// $this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'Kota' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
                'data' => 'provinsi',
                'name' => 'provinsi',
                'label' => 'Provinsi',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama Kota',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Kota::with('creator')
						 ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('id');
        }

        // Filters
        if ($provinsi_id = $request->provinsi_id) {
            $records->where('provinsi_id',$provinsi_id);
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('provinsi',function($record){
                return $record->provinsi->nama;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.kota.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.toko.kota.create');
    }

    public function store(KotaRequest $request)
    {
        // dd($request->All());
    	$region = new Kota;
    	$region->fill($request->all());
    	$region->save();

        // //log insert
        // $log = new LogRegion;
        // $log->region_id = $region->id;
        // $log->kode = $request->kode;
        // $log->area = $request->area;

        // $log->save();

        // insert to log audit
        // TransLogAudit::setLog([
        //     'tanggal_transaksi' => date('Y-m-d'),
        //     'type'              => 'Master Data Region',
        //     'ref'               => '',
        //     'aksi'              => 'Tambah Data Region',
        // ]);

    	return response([
    		'status' => true,
    		'data'	=> $region
    	]);
    }

    public function edit($id)
    {
    	$record = Kota::find($id);

        return $this->render('modules.utama.toko.kota.edit', ['record' => $record]);
    }

    public function update(KotaRequest $request, $id)
    {
    	$region = Kota::find($id);
    	$region->fill($request->all());
    	$region->save();

    	return response([
    		'status' => true,
    		'data'	=> $region
    	]);
    }

    public function destroy($id)
    {
    	$region = Kota::find($id);
    	$region->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
