<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\TmukRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\MemberCard;
use Lotte\Models\Master\JenisAssortment;
use Lotte\Models\Master\Pajak;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\Log\LogTmuk;
use Lotte\Models\Trans\TransLogAudit;

class TmukController extends Controller
{
	protected $link = 'utama/toko/tmuk/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Toko Mitra Usaha Kita-kita (TMUK)");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen longer");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'TMUK' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'kode',
			    'name' => 'kode',
			    'label' => 'Kode TMUK',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama TMUK',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'jenis_assortment',
			    'name' => 'jenis_assortment',
			    'label' => 'Assortment Type',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'alamat',
			    'name' => 'alamat',
			    'label' => 'Alamat',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Tmuk::with('creator')
						 ->select('*');
		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($lsi_id = $request->lsi_id) {
            $records->where('lsi_id', 'like', '%' . $lsi_id . '%');
        }
        if ($kode = $request->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('jenis_assortment',function($record){
				return $record->assortment->nama;
			})
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.tmuk.index', ['mockup' => false]);
    }

    public function onChangePopPajak($id=null)
    {
        $data = Pajak::find($id);
        return response()->json([
            'nama_npwp'     => $data->npwp,
            'nama_alamat'   => $data->alamat_npwp,
        ]);
    }

    public function onChangePopRekeningEscrow($id=null)
    {
        $data = RekeningEscrow::find($id);
        return response()->json([
            'nomor_rekening'    => $data->nomor_rekening,
            'bank_escrow_id'    => $data->bank_escrow_id,
            'nama_bank'         => $data->bankescrow->nama,
            'kode_bank'         => $data->bankescrow->kode_bank,
        ]);
    }

    public function onChangePopRekeningPemilik($id=null)
    {
        $data = RekeningEscrow::find($id);
        return response()->json([
            'nomor_rekening_pemilik' => $data->nomor_rekening,
            'bank_pemilik_id'        => $data->bank_escrow_id,
            'nama_bank_pemilik'      => $data->bankescrow->nama,
            'kode_bank_pemilik'      => $data->bankescrow->kode_bank,
        ]);
    }

    // public function onChangePopBankEscrow($id=null)
    // {
    //     $data = BankEscrow::find($id);
    //     return response()->json([
    //         'kode_bank' => $data->kode_bank,
    //     ]);
    // }

    public function create()
    {
        return $this->render('modules.utama.toko.tmuk.create');
    }

    public function onChangePop($id=null)
    {
        $data = MemberCard::find($id);
        return response()->json([
            'nama' => $data->nama,
            'jeniskustomer' => $data->kustomer->jenis,
        ]);
    }

    public function store(TmukRequest $request)
    {
        // dd($request->all());
        $request['aktual_pembukaan'] = $request->aktual_pembukaan == "" ? NULL : $request->aktual_pembukaan;
    	$tmuk = new Tmuk;
    	$tmuk->fill($request->all());

        //update status rekening escrow
        if($tmuk->save()){
                $statusrekening = RekeningEscrow::where('id', $tmuk->rekening_escrow_id)->first();
                $statusrekening->status_rekening = 1; //No rekening sudah dipakai
                $statusrekening->save();
            }

        //log insert
        $log = new LogTmuk;
        $log->tmuk_id = $tmuk->id;
        $log->lsi_id = $request->lsi_id;
        $log->kota_id = $request->kota_id;
        $log->kecamatan_id = $request->kecamatan_id;
        $log->membercard_id = $request->membercard_id;
        $log->jenis_assortment_id = $request->jenis_assortment_id;
        $log->rekening_escrow_id = $request->rekening_escrow_id;
        $log->bank_escrow_id = $request->bank_escrow_id;
        $log->kode= $request->kode;
        $log->nama = $request->nama;
        $log->alamat = $request->alamat;
        $log->kode_pos = $request->kode_pos;
        $log->telepon = $request->telepon;
        $log->email = $request->email;
        $log->asn = $request->asn;
        $log->auto_approve = $request->auto_approve;
        $log->longitude = $request->longitude;
        $log->latitude = $request->latitude;
        $log->gross_area = $request->gross_area;
        $log->selling_area = $request->selling_area;
        $log->nama_cde = $request->nama_cde;
        $log->email_cde = $request->email_cde;
        $log->rencana_pembukaan = $request->rencana_pembukaan;
        $log->aktual_pembukaan = $request->aktual_pembukaan;
        $log->pajak_id = $request->pajak_id;
        $log->provinsi_id = $request->provinsi_id;
        $log->rekening_pemilik_id = $request->rekening_pemilik_id;
        $log->bank_pemilik_id = $request->bank_pemilik_id;

        $log->save();


        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data TMUK',
            'ref'               => '',
            'aksi'              => 'Tambah Data TMUK',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $tmuk
    	]);
    }

    public function edit($id)
    {
        $record = Tmuk::find($id);
        return $this->render('modules.utama.toko.tmuk.edit', ['record' => $record]);
    }

    public function update(TmukRequest $request, $id)
    {
        // dd($request->all());
        $request['aktual_pembukaan'] = $request->aktual_pembukaan == "" ? NULL : $request->aktual_pembukaan;
    	$tmuk = Tmuk::find($id);
    	$tmuk->fill($request->all());
    	$tmuk->save();

        //log insert
        $log = new LogTmuk;
        $log->tmuk_id = $tmuk->id;
        $log->lsi_id = $tmuk->lsi_id;
        $log->kota_id = $tmuk->kota_id;
        $log->kecamatan_id = $tmuk->kecamatan_id;
        $log->membercard_id = $tmuk->membercard_id;
        $log->jenis_assortment_id = $tmuk->jenis_assortment_id;
        $log->rekening_escrow_id = $tmuk->rekening_escrow_id;
        $log->bank_escrow_id = $tmuk->bank_escrow_id;
        $log->kode= $tmuk->kode;
        $log->nama = $tmuk->nama;
        $log->alamat = $tmuk->alamat;
        $log->kode_pos = $tmuk->kode_pos;
        $log->telepon = $tmuk->telepon;
        $log->email = $tmuk->email;
        $log->asn = $tmuk->asn;
        $log->auto_approve = $tmuk->auto_approve;
        $log->longitude = $tmuk->longitude;
        $log->latitude = $tmuk->latitude;
        $log->gross_area = $tmuk->gross_area;
        $log->selling_area = $tmuk->selling_area;
        $log->nama_cde = $tmuk->nama_cde;
        $log->email_cde = $tmuk->email_cde;
        $log->rencana_pembukaan = $tmuk->rencana_pembukaan;
        $log->aktual_pembukaan = $tmuk->aktual_pembukaan;
        $log->pajak_id = $tmuk->pajak_id;
        $log->provinsi_id = $tmuk->provinsi_id;
        $log->rekening_pemilik_id = $tmuk->rekening_pemilik_id;
        $log->bank_pemilik_id = $tmuk->bank_pemilik_id;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data TMUK',
            'ref'               => '',
            'aksi'              => 'Ubah Data TMUK',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $tmuk
    	]);
    }

    public function destroy($id)
    {
    	$tmuk = Tmuk::find($id);
    	$tmuk->delete();

        //log insert
        $log = new LogTmuk;
        $log->tmuk_id = $tmuk->id;
        $log->lsi_id = $tmuk->lsi_id;
        $log->kota_id = $tmuk->kota_id;
        $log->kecamatan_id = $tmuk->kecamatan_id;
        $log->membercard_id = $tmuk->membercard_id;
        $log->jenis_assortment_id = $tmuk->jenis_assortment_id;
        $log->rekening_escrow_id = $tmuk->rekening_escrow_id;
        $log->bank_escrow_id = $tmuk->bank_escrow_id;
        $log->kode= $tmuk->kode;
        $log->nama = $tmuk->nama;
        $log->alamat = $tmuk->alamat;
        $log->kode_pos = $tmuk->kode_pos;
        $log->telepon = $tmuk->telepon;
        $log->email = $tmuk->email;
        $log->asn = $tmuk->asn;
        $log->auto_approve = $tmuk->auto_approve;
        $log->longitude = $tmuk->longitude;
        $log->latitude = $tmuk->latitude;
        $log->gross_area = $tmuk->gross_area;
        $log->selling_area = $tmuk->selling_area;
        $log->nama_cde = $tmuk->nama_cde;
        $log->email_cde = $tmuk->email_cde;
        $log->rencana_pembukaan = $tmuk->rencana_pembukaan;
        $log->aktual_pembukaan = $tmuk->aktual_pembukaan;
        $log->pajak_id = $tmuk->pajak_id;
        $log->provinsi_id = $tmuk->provinsi_id;
        $log->rekening_pemilik_id = $tmuk->rekening_pemilik_id;
        $log->bank_pemilik_id = $tmuk->bank_pemilik_id;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data TMUK',
            'ref'               => '',
            'aksi'              => 'Hapus Data TMUK',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
}
