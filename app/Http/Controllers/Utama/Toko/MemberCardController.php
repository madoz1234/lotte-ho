<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\MemberCardRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\MemberCard;
use Lotte\Models\Master\Log\LogMember;
use Lotte\Models\Trans\TransLogAudit;

class MemberCardController extends Controller
{
	protected $link = 'utama/toko/member-card/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Kartu Anggota");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("longer");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'Kartu Anggota' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
            [
                'data' => 'nomor',
                'name' => 'nomor',
                'label' => 'No Member Card',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama Pemilik',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'kustomer',
			    'name' => 'kustomer',
			    'label' => 'Jenis Kustomer',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'telepon',
			    'name' => 'telepon',
			    'label' => 'Telepon',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'email',
			    'name' => 'email',
			    'label' => 'Email',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = MemberCard::with('creator')
						 ->select('*')
                         ->orderBy('created_at', 'desc');
        // $record = MemberCard::select('*');
        // dd($record);

		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        if ($jeniskustomer_id = $request->jeniskustomer_id) {
            $records->where('jeniskustomer_id',$jeniskustomer_id);
        }
        // dd($request->jeniskustomer_id);

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('kustomer',function($record){
				// return ucfirst($kustomer = $record->jenis) ? $kustomer->jenis : '-';
                return $record->kustomer->jenis;
				// return 'a';
			})
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                    // 'disabled' => $record->tmuk_kode ==  null ? '' : 'disabled' ,
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                    // 'disabled' => $record->tmuk_kode ==  null ? '' : 'disabled' ,
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.member-card.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.toko.member-card.create');
    }

    public function store(MemberCardRequest $request)
    {
    	$membercard = new MemberCard;
    	$membercard->fill($request->all());
    	$membercard->save();

        // dd($membercard);
    	//log insert
        $log = new LogMember;
        $log->membercard_id = $membercard->id;
        $log->nomor = $request->nomor;
        $log->nama = $request->nama;
        $log->telepon = $request->telepon;
        $log->email = $request->email;
        $log->jeniskustomer_id = $request->jeniskustomer_id;
        $log->tmuk_id = $request->tmuk_id;
        $log->alamat = $request->alamat;
        $log->notes = $request->notes;
        // $log->limit_kredit = $request->limit_kredit;
        // $log->tmuk_kode = $request->tmuk_kode;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Kartu Anggota',
            'ref'               => '',
            'aksi'              => 'Tambah Data Kartu Anggota',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $membercard
    	]);
    }

    public function edit($id)
    {
    	$record = MemberCard::find($id);

        return $this->render('modules.utama.toko.member-card.edit', ['record' => $record]);
    }

    public function update(MemberCardRequest $request, $id)
    {
    	$membercard = MemberCard::find($id);
    	$membercard->fill($request->all());
    	$membercard->save();

    	//log update
        $log = new LogMember;
        $log->membercard_id = $membercard->id;
        $log->nomor = $membercard->nomor;
        $log->nama = $membercard->nama;
        $log->telepon = $membercard->telepon;
        $log->email = $membercard->email;
        $log->jeniskustomer_id = $membercard->jeniskustomer_id;
        $log->tmuk_id = $membercard->tmuk_id;
        $log->alamat = $membercard->alamat;
        $log->notes = $membercard->notes;
        // $log->limit_kredit = $membercard->limit_kredit;
        // $log->tmuk_kode = $membercard->tmuk_kode;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Kartu Anggota',
            'ref'               => '',
            'aksi'              => 'Ubah Data Kartu Anggota',
        ]);


    	return response([
    		'status' => true,
    		'data'	=> $membercard
    	]);
    }

    public function destroy($id)
    {
    	$membercard = MemberCard::find($id);
    	$membercard->delete();

    	//log update
        $log = new LogMember;
        $log->membercard_id = $membercard->id;
        $log->nomor = $membercard->nomor;
        $log->nama = $membercard->nama;
        $log->telepon = $membercard->telepon;
        $log->email = $membercard->email;
        $log->jeniskustomer_id = $membercard->jeniskustomer_id;
        $log->tmuk_id = $membercard->tmuk_id;
        $log->alamat = $membercard->alamat;
        $log->notes = $membercard->notes;
        // $log->limit_kredit = $membercard->limit_kredit;
        // $log->tmuk_kode = $membercard->tmuk_kode;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Kartu Anggota',
            'ref'               => '',
            'aksi'              => 'Hapus Data Kartu Anggota',
        ]);


    	return response([
    		'status' => true,
    	]);
    }
}
