<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\KustomerRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Master\Kustomer;
use Lotte\Models\Master\JenisKustomer;
use Lotte\Models\Master\MemberCard;
use Lotte\Models\Master\Pajak;
use Lotte\Models\Master\Log\LogKustomer;
use Lotte\Models\Trans\TransLogAudit;

class KustomerController extends Controller
{
	protected $link = 'utama/toko/kustomer/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Kostumer");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen longer");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'Kostumer' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'tmuk_id',
			    'name' => 'tmuk_id',
			    'label' => 'TMUK Induk',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama Kostumer',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'jenis_kustomer',
			    'name' => 'jenis_kustomer',
			    'label' => 'Jenis Kostumer',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'alamat',
			    'name' => 'alamat',
			    'label' => 'Alamat',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Kustomer::with('creator')
						 ->select('*');
        // $record = Kustomer::select('*');
        // dd($record);

		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('kode');
        }

        //Filters
        if ($tmuk_id = $request->tmuk_id) {
            $records->where('tmuk_id',$tmuk_id);
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($jenis = $request->jenis) {
            $records->where('jenis', 'like', '%' . $jenis . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tmuk_id',function($record){
				return $record->tmuk->nama;
			})
            ->addColumn('jenis_kustomer',function($record){
				return $record->kustomer->jenis;
			})
            ->addColumn('nama',function($record){
				return $record->membercard->nama;
			})
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.kustomer.index', ['mockup' => false]);
    }

    public function onChangePopPajak($id=null)
    {
        $data = Pajak::find($id);
        return response()->json([
            // 'nama' => $data->nama,
            'nama_npwp' => $data->npwp,
        ]);
    }

    public function onChangePopMemberCard($id=null)
    {
        $data = MemberCard::find($id);
        return response()->json([
            'nama' => $data->nama,
            'telepon' => $data->telepon,
            'email' => $data->email,
            'jeniskustomer' => $data->kustomer->jenis,
            'jeniskustomer_id' => $data->kustomer->id,
        ]);
    }

    public function create()
    {
    	$code = Kustomer::generateCode();
        return $this->render('modules.utama.toko.kustomer.create', ['code' => $code]);
    }

    public function store(KustomerRequest $request)
    {
        $data = $request->all();

        $jk = JenisKustomer::find($data['jeniskustomer_id']);

        if (strtoupper($jk->jenis) == "MEMBER") {
            $data['longitude'] = 0;
            $data['latitude'] = 0;
        }

        // $request['pajak_id']= ($request->pajak_id != 0) ? $request->pajak_id : null;
        // dd($request['pajak_id']);

    	$kustomer = new Kustomer;
        $data['limit_kredit'] = decimal_for_save($data['limit_kredit']);
        $data['pajak_id']= ($request->pajak_id != 0) ? $request->pajak_id : null;
        $kustomer->fill($data);
    	if($kustomer->save()){
            $data['kustomer_id'] = $kustomer->id;

            //log insert
            $log = new LogKustomer;
            $log->fill($data);
            $log->save();


        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Kustomer',
            'ref'               => '',
            'aksi'              => 'Tambah Data Kustomer',
        ]);

            return response([
                'status' => true,
                'data'  => $kustomer
            ]);
        }
    }

    public function edit($id)
    {
    	$record = Kustomer::find($id);
        return $this->render('modules.utama.toko.kustomer.edit', ['record' => $record]);
    }

    public function importexcel()
    {
        return $this->render('modules.utama.toko.kustomer.import');
    }

    public function update(KustomerRequest $request, $id)
    {
        $data = $request->all();

        $jk = JenisKustomer::find($data['jeniskustomer_id']);

        if (strtoupper($jk->jenis) == "MEMBER") {
            $data['longitude'] = 0;
            $data['latitude'] = 0;
        }

        $kustomer = Kustomer::find($id);
        $data['limit_kredit'] = decimal_for_save($data['limit_kredit']);
        $data['pajak_id']= ($request->pajak_id != 0) ? $request->pajak_id : null;
        $kustomer->fill($data);
        if($kustomer->save()){
            $data['kustomer_id'] = $kustomer->id;

            //log insert
            $log = new LogKustomer;
            $log->fill($data);
            $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Kustomer',
            'ref'               => '',
            'aksi'              => 'Ubah Data Kustomer',
        ]);

            return response([
                'status' => true,
                'data'  => $kustomer
            ]);
        }
    }

    public function destroy($id)
    {
    	$kustomer = Kustomer::find($id);
    	$kustomer->delete();

        //log update
        $log = new LogKustomer;
        $log->kustomer_id = $kustomer->id;
        $log->tmuk_id = $kustomer->tmuk_id;
        $log->kota_id = $kustomer->kota_id;
        $log->kecamatan_id = $kustomer->kecamatan_id;
        $log->pajak_id = $kustomer->pajak_id;
        $log->jeniskustomer_id = $kustomer->jeniskustomer_id;
        $log->membercard_id = $kustomer->membercard_id;
        $log->kode = $kustomer->kode;
        $log->alamat = $kustomer->alamat;
        $log->kode_pos = $kustomer->kode_pos;
        $log->longitude = $kustomer->longitude;
        $log->latitude = $kustomer->latitude;
        $log->limit_kredit = decimal_for_save($kustomer->limit_kredit);
        $log->persen_diskon = $kustomer->persen_diskon;
        $log->cara_pembayaran = $kustomer->cara_pembayaran;
        $log->status_kredit = $kustomer->status_kredit;
        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Kustomer',
            'ref'               => '',
            'aksi'              => 'Hapus Data Kustomer',
        ]);

    	return response([
    		'status' => true,
    	]);
    }

    //Function upload template kustomer
    public function postImportExcel(Request $request)
    {
        $tmuk = [];
        foreach (Kustomer::get() as $row) {
            $tmuk[$row->tmuk_id] = $row->id;
        }

        // dd($request->hasFile('kustomer'));
        if($request->hasFile('kustomer')){
            $path = $request->file('kustomer')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();
// dd($data->all());
            if(!empty($data) && $data->count()){
                foreach ($data->toArray() as $key => $value) {
                    if(!is_null($value['tmuk_id'])){ //jika data tidak boleh kosong tambahkan dengan &&
                        // cek data
                        // dd($value['tmuk_id']);
                        // if (!isset($tmuk[$value['tmuk_id']])) {

                                $record                     = new Kustomer();
                                $record->tmuk_id            = $value['tmuk_id'];
                                $record->kota_id            = $value['kota_id'];
                                $record->kecamatan_id       = $value['kecamatan_id'];
                                $record->pajak_id           = $value['pajak_id'];
                                $record->jeniskustomer_id   = $value['jeniskustomer_id'];
                                $record->membercard_id      = $value['membercard_id'];
                                $record->kode               = $value['kode'];
                                $record->alamat             = $value['alamat'];
                                $record->kode_pos           = $value['kode_pos'];
                                $record->longitude          = $value['longitude'];
                                $record->latitude           = $value['latitude'];
                                $record->limit_kredit       = $value['limit_kredit'];
                                $record->persen_diskon      = $value['persen_diskon'];
                                $record->cara_pembayaran      = $value['cara_pembayaran'];
                                $record->status_kredit      = $value['status_kredit'];
                                $record->created_by         = \Auth::user()->id;
                                $record->created_at         = date('Y-m-d H:i:s');
                                $record->save();

                        // }
                    }
                }

                if(!empty($insert)){
                    Kustomer::insert($insert);
                    return back()->with('success','Insert Record successfully.');
                }
            }
        }

        return back()->with('error','Please Check your file, Something is wrong there.');
    }
}
