<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\LsiRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\Lsi;
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Pajak;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\Log\LogLsi;
use Lotte\Models\Trans\TransLogAudit;

class LsiController extends Controller
{
	protected $link = 'utama/toko/lsi/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("LSI");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'LSI' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'region_id',
			    'name' => 'region_id',
			    'label' => 'Region',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'kode',
			    'name' => 'kode',
			    'label' => 'Kode LSI',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama LSI',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Lsi::with('creator')
						 ->select('*');
		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($region_id = $request->region_id) {
            $records->where('region_id',$region_id);
        }
        if ($kode = $request->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('region_id',function($record){
                return $record->region->area;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.lsi.index', ['mockup' => false]);
    }

    public function onChangePopPajak($id=null)
    {
        $data = Pajak::find($id);
        return response()->json([
            'nama_npwp' => $data->npwp,
        ]);
    }

    public function onChangePopRekeningEscrow($id=null)
    {
        $data = RekeningEscrow::find($id);
        return response()->json([
            'nomor_rekening'    => $data->nomor_rekening,
            'bank_escrow_id'    => $data->bank_escrow_id,
            'nama_bank'    => $data->bankescrow->nama,
            'kode_bank'    => $data->bankescrow->kode_bank,
        ]);
    }

    // public function onChangePopBankEscrow($id=null)
    // {
    //     $data = BankEscrow::find($id);
    //     return response()->json([
    //         'kode_bank' => $data->kode_bank,
    //     ]);
    // }

    public function create()
    {
        return $this->render('modules.utama.toko.lsi.create');
    }

    public function store(LsiRequest $request)
    {
    	$lsi = new Lsi;
    	$lsi->fill($request->all());
    	$lsi->save();

        //log insert
        $log = new LogLsi;
        $log->lsi_id = $lsi->id;
        $log->region_id = $request->region_id;
        $log->kota_id = $request->kota_id;
        $log->rekening_escrow_id = $request->rekening_escrow_id;
        $log->bank_escrow_id = $request->bank_escrow_id;
        $log->kode = $request->kode;
        $log->nama = $request->nama;
        $log->alamat = $request->alamat;
        $log->kode_pos= $request->kode_pos;
        $log->telepon = $request->telepon;
        $log->email = $request->email;
        $log->longitude = $request->longitude;
        $log->latitude = $request->latitude;
        $log->pajak_id = $request->pajak_id;

        $log->save();


        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data LSI',
            'ref'               => '',
            'aksi'              => 'Tambah Data LSI',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $lsi
    	]);
    }

    public function edit($id)
    {

        $record = Lsi::find($id);
        return $this->render('modules.utama.toko.lsi.edit', ['record' => $record]);
    }

    public function update(LsiRequest $request, $id)
    {
    	$lsi = Lsi::find($id);
    	$lsi->fill($request->all());
    	$lsi->save();

        //log update
        $log = new LogLsi;
        $log->lsi_id = $lsi->id;
        $log->region_id = $lsi->region_id;
        $log->kota_id = $lsi->kota_id;
        $log->rekening_escrow_id = $lsi->rekening_escrow_id;
        $log->bank_escrow_id = $lsi->bank_escrow_id;
        $log->kode = $lsi->kode;
        $log->nama = $lsi->nama;
        $log->alamat = $lsi->alamat;
        $log->kode_pos= $lsi->kode_pos;
        $log->telepon = $lsi->telepon;
        $log->email = $lsi->email;
        $log->longitude = $lsi->longitude;
        $log->latitude = $lsi->latitude;
        $log->pajak_id = $lsi->pajak_id;
        
        $log->save();


        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data LSI',
            'ref'               => '',
            'aksi'              => 'Ubah Data LSI',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $lsi
    	]);
    }

    public function destroy($id)
    {
    	$lsi = Lsi::find($id);
    	$lsi->delete();

        //log delete
        $log = new LogLsi;
        $log->lsi_id = $lsi->id;
        $log->region_id = $lsi->region_id;
        $log->kota_id = $lsi->kota_id;
        $log->rekening_escrow_id = $lsi->rekening_escrow_id;
        $log->bank_escrow_id = $lsi->bank_escrow_id;
        $log->kode = $lsi->kode;
        $log->nama = $lsi->nama;
        $log->alamat = $lsi->alamat;
        $log->kode_pos= $lsi->kode_pos;
        $log->telepon = $lsi->telepon;
        $log->email = $lsi->email;
        $log->longitude = $lsi->longitude;
        $log->latitude = $lsi->latitude;
        $log->pajak_id = $lsi->pajak_id;
        
        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data LSI',
            'ref'               => '',
            'aksi'              => 'Hapus Data LSI',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
}
