<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\KecamatanRequest;
use Lotte\Http\Controllers\Controller;
use Excel;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\Kecamatan;
// use Lotte\Models\Master\Log\LogRegion;
// use Lotte\Models\Trans\TransLogAudit;

class KecamatanController extends Controller
{
	protected $link = 'utama/toko/kecamatan/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Kecamatan");
		$this->setSubtitle("&nbsp;");
		// $this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'Kecamatan' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
                'data' => 'provinsi',
                'name' => 'provinsi',
                'label' => 'Provinsi',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'kota',
                'name' => 'kota',
                'label' => 'Kota',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama Kecamatan',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Kecamatan::with('creator')
						 ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('id');
        }

        // Filters
        if ($kota_id = $request->kota_id) {
            $records->where('kota_id',$kota_id);
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('provinsi',function($record){
                return $record->kota->provinsi->nama;
            })
            ->addColumn('kota',function($record){
                return $record->kota->nama;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.kecamatan.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.toko.kecamatan.create');
    }

    public function store(KecamatanRequest $request)
    {
    	$region = new Kecamatan;
    	$region->fill($request->all());
    	$region->save();

        // //log insert
        // $log = new LogRegion;
        // $log->region_id = $region->id;
        // $log->kode = $request->kode;
        // $log->area = $request->area;

        // $log->save();

        // insert to log audit
        // TransLogAudit::setLog([
        //     'tanggal_transaksi' => date('Y-m-d'),
        //     'type'              => 'Master Data Region',
        //     'ref'               => '',
        //     'aksi'              => 'Tambah Data Region',
        // ]);

    	return response([
    		'status' => true,
    		'data'	=> $region
    	]);
    }

    public function edit($id)
    {
    	$record = Kecamatan::find($id);

        return $this->render('modules.utama.toko.kecamatan.edit', ['record' => $record]);
    }

    public function update(KecamatanRequest $request, $id)
    {
    	$region = Kecamatan::find($id);
    	$region->fill($request->all());
    	$region->save();

    	return response([
    		'status' => true,
    		'data'	=> $region
    	]);
    }

    public function destroy($id)
    {
    	$region = Kecamatan::find($id);
    	$region->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
