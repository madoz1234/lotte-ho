<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\JenisKustomerRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\JenisKustomer;
use Lotte\Models\Master\Log\LogJenisKustomer;
use Lotte\Models\Trans\TransLogAudit;

class JenisKustomerController extends Controller
{
	protected $link = 'utama/toko/jenis-kustomer/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Jenis Kustomer");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'Jenis Kustomer' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			// [
			//     'data' => 'kode',
			//     'name' => 'kode',
			//     'label' => 'Kode',
			//     'searchable' => false,
			//     'sortable' => true,
			//     'className' => "center aligned",
			//     'width' => '150px',
			// ],
			[
			    'data' => 'jenis',
			    'name' => 'jenis',
			    'label' => 'Nama Jenis',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = JenisKustomer::with('creator')
						 ->select('*')
                         ->orderBy('created_at', 'desc');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('kode');
        }

        //Filters
        // if ($kode = $request->kode) {
        //     $records->where('kode', 'like', '%' . $kode . '%');
        // }
        if ($jenis = $request->jenis) {
            $records->where('jenis', 'ilike', '%' . $jenis . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'detail',
                	'id'   => $record->id
                ]);
                // Delete
                // $btn .= $this->makeButton([
                // 	'type' => 'delete',
                // 	'id'   => $record->id
                // ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.jenis-kustomer.index', ['mockup' => false]);
    }

    public function create()
    {
    	$code = JenisKustomer::generateCode();
        return $this->render('modules.utama.toko.jenis-kustomer.create', ['code' => $code]);
    }

    public function store(JenisKustomerRequest $request)
    {
    	$jeniskustomer = new JenisKustomer;
    	$jeniskustomer->fill($request->all());
    	$jeniskustomer->save();

        //log insert
        $log = new LogJenisKustomer;
        $log->jeniskustomer_id = $jeniskustomer->id;
        $log->kode = $request->kode;
        $log->jenis = $request->jenis;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Jenis Kustomer',
            'ref'               => '',
            'aksi'              => 'Tambah Data Jenis Kustomer',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $jeniskustomer
    	]);
    }

    public function edit($id)
    {

    	$record = JenisKustomer::find($id);

        return $this->render('modules.utama.toko.jenis-kustomer.edit', ['record' => $record]);
    }

    public function detail($id)
    {
        $record = JenisKustomer::find($id);
        return $this->render('modules.utama.toko.jenis-kustomer.detail',['record'=>$record]);
    }

    public function update(JenisKustomerRequest $request, $id)
    {
    	$jeniskustomer = JenisKustomer::find($id);
    	$jeniskustomer->fill($request->all());
    	$jeniskustomer->save();

        //log update
        $log = new LogJenisKustomer;
        $log->jeniskustomer_id = $jeniskustomer->id;
        $log->kode = $jeniskustomer->kode;
        $log->jenis = $jeniskustomer->jenis;

        $log->save();

    	return response([
    		'status' => true,
    		'data'	=> $jeniskustomer
    	]);
    }

    public function destroy($id)
    {
    	$jeniskustomer = JenisKustomer::find($id);
    	$jeniskustomer->delete();

        //log delete
        $log = new LogJenisKustomer;
        $log->jeniskustomer_id = $jeniskustomer->id;
        $log->kode = $jeniskustomer->kode;
        $log->jenis = $jeniskustomer->jenis;

        $log->save();

    	return response([
    		'status' => true,
    	]);
    }
}
