<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\VendorLokalTmukRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\VendorLokalTmuk;
use Lotte\Models\Master\Pajak;
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\Log\LogVendorLokalTmuk;
use Lotte\Models\Trans\TransPyr;
use Lotte\Models\Trans\TransLogAudit;

class VendorLokalTmukController extends Controller
{
	protected $link = 'utama/toko/vendor-lokal-tmuk/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Vendor Lokal TMUK");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'Vendor Lokal TMUK' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'kode',
			    'name' => 'kode',
			    'label' => 'Kode Vendor Lokal',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama Vendor Lokal',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'alamat',
			    'name' => 'alamat',
			    'label' => 'Alamat Vendor Lokal',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = VendorLokalTmuk::with('creator')
						 ->select('*');
		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }
        
        // $vendor_pyr = TransPyr::select('*')->first();
        // $id_vendor = $vendor_pyr->vendor_lokal;


        //Filters
        if ($lsi_id = $request->lsi_id) {
            $records->where('lsi_id', 'like', '%' . $lsi_id . '%');
        }
        if ($kode = $request->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('nama', function ($record) {
                return 'Vendor Lokal - ' . $record->nama;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);

                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);


                // if ($record->id = $id_vendor){
                //     $btn .= '<button type="button" class="ui mini red icon delete button" id="pembatalan" data-content="Hapus Data" data-id="'.$record->id.'"><i class="trash icon"></i></button>';
                // } else {
                //     $btn .= $this->makeButton([
                //     'type' => 'delete',
                //     'id'   => $record->id
                //     ]);
                // }


                // $record->id = $id_vendor  ? '<button type="button" class="ui mini red icon delete button" id="pembatalan" data-content="Hapus Data" data-id="'.$record->id.'"><i class="trash icon"></i></button>' : 'xxx'; 
                // $btn .= '<button type="button" class="ui mini red icon delete button" id="pembatalan" data-content="Hapus Data" data-id="'.$record->id.'"><i class="trash icon"></i></button>';

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.vendor-lokal-tmuk.index', ['mockup' => false]);
    }

    public function onChangePopPajak($id=null)
    {
        $data = Pajak::find($id);
        return response()->json([
            // 'nama' => $data->nama,
            'nama_npwp' => $data->npwp,
        ]);
    }

    public function onChangePopRekeningEscrow($id=null)
    {
        $data = RekeningEscrow::find($id);
        return response()->json([
            // 'nomor_rekening' => $data->nomor_rekening,

            'nomor_rekening' => $data->nomor_rekening,
            'bank_escrow_id'    => $data->bank_escrow_id,
            'nama_bank'    => $data->bankescrow->nama,
            'kode_bank'    => $data->bankescrow->kode_bank,
        ]);
    }

    public function onChangePopBankEscrow($id=null)
    {
        $data = BankEscrow::find($id);
        return response()->json([
            'kode_bank' => $data->kode_bank,
        ]);
    }

    public function create()
    {
        // $code = VendorLokalTmuk::generateCode();
        // return $this->render('modules.utama.toko.vendor-lokal-tmuk.create', ['code' => $code]);
        return $this->render('modules.utama.toko.vendor-lokal-tmuk.create');
    }


    // public function store(VendorLokalTmukRequest $request)
    public function store(Request $request)
    {
// dd($request->rekening_escrow_id);
// dd($request->bank_escrow_id);


        $generateDays = function($val){
            switch ($val) {
                case 1:
                     $days = 'order_senin';  
                    break;
                case 2:
                     $days = 'order_selasa';  
                       
                    break;
                case 3:
                     $days = 'order_rabu';  
                       
                    break;
                case 4:
                     $days = 'order_kamis';  
                       
                    break;
                case 5:
                     $days = 'order_jumat';  
                       
                    break;
                case 6:
                     $days = 'order_sabtu';  
                       
                    break;
                case 7:
                     $days = 'order_minggu';  
                       
                    break;
            }

            return $days;
        };

        $request['order_senin']  = 0;
        $request['order_selasa'] = 0;
        $request['order_rabu']   = 0;
        $request['order_kamis']  = 0;
        $request['order_jumat']  = 0;
        $request['order_sabtu']  = 0;
        $request['order_minggu'] = 0;

        foreach ($request->jadwal_order as $key=>$val) {
            $request[$generateDays($val)] = $val;
        }

        $request['pajak_id']= ($request->pajak_id != 0) ? $request->pajak_id : null;
        $request['rekening_escrow_id']= ($request->rekening_escrow_id != 0) ? $request->rekening_escrow_id : null;
        $request['bank_escrow_id']= ($request->bank_escrow_id != 0) ? $request->bank_escrow_id : null;

        // dd($request['bank_escrow_id']);
    	$vendorlokaltmuk = new VendorLokalTmuk;
    	$vendorlokaltmuk->fill($request->all());
    	$vendorlokaltmuk->save();

        $vendorlokaltmuk->detailVendor()->sync($request->tmuk_id);

        //log insert
        $log = new LogVendorLokalTmuk;
        $log->vendor_lokal_tmuk_id = $vendorlokaltmuk->id;
        $log->kota_id = $request->kota_id;
        $log->kecamatan_id = $request->kecamatan_id;
        // $log->rekening_escrow_id = $request->rekening_escrow_id;
        // $log->bank_escrow_id = $request->bank_escrow_id;
        $log->rekening_escrow_id = ($request->rekening_escrow_id != 0) ? null : $request->rekening_escrow_id;
        $log->bank_escrow_id = ($request->bank_escrow_id != 0) ? null : $request->bank_escrow_id;
        $log->pajak_id = ($request->pajak_id != 0) ? null : $request->pajak_id;
        $log->kode = $request->kode;
        $log->nama = $request->nama;
        $log->alamat = $request->alamat;
        $log->kode_pos= $request->kode_pos;
        $log->telepon = $request->telepon;
        $log->email = $request->email;
        $log->top = $request->top;
        $log->lead_time = $request->lead_time;
        $log->order_senin = $request->order_senin;
        $log->order_selasa = $request->order_selasa;
        $log->order_rabu = $request->order_rabu;
        $log->order_kamis = $request->order_kamis;
        $log->order_jumat = $request->order_jumat;
        $log->order_sabtu = $request->order_sabtu;
        $log->order_minggu = $request->order_minggu;
        $log->contact_person = $request->contact_person;
        $log->provinsi_id = $request->provinsi_id;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Vendor Lokal',
            'ref'               => '',
            'aksi'              => 'Tambah Data Vendor Lokal',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $vendorlokaltmuk
    	]);
    }

    public function edit($id)
    {
    	$record = VendorLokalTmuk::find($id);
        return $this->render('modules.utama.toko.vendor-lokal-tmuk.edit', ['record' => $record]);
    }

    public function update(VendorLokalTmukRequest $request, $id)
    {
        // dd($request->all());
        $request['pajak_id']= ($request->pajak_id != 0) ? $request->pajak_id : null;
        $request['rekening_escrow_id']= ($request->rekening_escrow_id != 0) ? $request->rekening_escrow_id : null;
        $request['bank_escrow_id']= ($request->bank_escrow_id != 0) ? $request->bank_escrow_id : null;


        $vendorlokaltmuk = VendorLokalTmuk::find($id);
        $vendorlokaltmuk->fill($request->all());
        $vendorlokaltmuk->save();

        // sync tmuk
        $vendorlokaltmuk->detailVendor()->sync($request->tmuk_id);

        // sync hari
        $vendorlokaltmuk->order_senin = 0;
        $vendorlokaltmuk->order_selasa = 0;
        $vendorlokaltmuk->order_rabu = 0;
        $vendorlokaltmuk->order_kamis = 0;
        $vendorlokaltmuk->order_jumat = 0;
        $vendorlokaltmuk->order_sabtu = 0;
        $vendorlokaltmuk->order_minggu = 0;

        foreach ($request->jadwal_order as $val) {
            if ($val == 1) {
                $vendorlokaltmuk->order_senin = 1;

            } else if($val == 2) {
                $vendorlokaltmuk->order_selasa = 1;

            } else if($val == 3) {
                $vendorlokaltmuk->order_rabu = 1;

            } else if($val == 4) {
                $vendorlokaltmuk->order_kamis = 1;

            } else if($val == 5) {
                $vendorlokaltmuk->order_jumat = 1;

            } else if($val == 6) {
                $vendorlokaltmuk->order_sabtu = 1;

            } else if($val == 7) {
                $vendorlokaltmuk->order_minggu = 1;

            }
        }

        $vendorlokaltmuk->save();

        //log insert
        $log = new LogVendorLokalTmuk;
        $log->vendor_lokal_tmuk_id = $vendorlokaltmuk->id;
        $log->kota_id = $vendorlokaltmuk->kota_id;
        $log->kecamatan_id = $vendorlokaltmuk->kecamatan_id;
        // $log->rekening_escrow_id = $vendorlokaltmuk->rekening_escrow_id;
        // $log->bank_escrow_id = $vendorlokaltmuk->bank_escrow_id;
        // $log->pajak_id = $vendorlokaltmuk->pajak_id;
        $log->rekening_escrow_id = ($request->rekening_escrow_id != 0) ? null : $request->rekening_escrow_id;
        $log->bank_escrow_id = ($request->bank_escrow_id != 0) ? null : $request->bank_escrow_id;
        $log->pajak_id = ($request->pajak_id != 0) ? null : $request->pajak_id;
        $log->kode = $vendorlokaltmuk->kode;
        $log->nama = $vendorlokaltmuk->nama;
        $log->alamat = $vendorlokaltmuk->alamat;
        $log->kode_pos= $vendorlokaltmuk->kode_pos;
        $log->telepon = $vendorlokaltmuk->telepon;
        $log->email = $vendorlokaltmuk->email;
        $log->top = $vendorlokaltmuk->top;
        $log->lead_time = $vendorlokaltmuk->lead_time;
        $log->order_senin = $vendorlokaltmuk->order_senin;
        $log->order_selasa = $vendorlokaltmuk->order_selasa;
        $log->order_rabu = $vendorlokaltmuk->order_rabu;
        $log->order_kamis = $vendorlokaltmuk->order_kamis;
        $log->order_jumat = $vendorlokaltmuk->order_jumat;
        $log->order_sabtu = $vendorlokaltmuk->order_sabtu;
        $log->order_minggu = $vendorlokaltmuk->order_minggu;
        $log->contact_person = $vendorlokaltmuk->contact_person;
        $log->provinsi_id = $vendorlokaltmuk->provinsi_id;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Vendor Lokal',
            'ref'               => '',
            'aksi'              => 'Ubah Data Vendor Lokal',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $vendorlokaltmuk
    	]);
    }

    public function destroy($id)
    {
        // dd($id);

        $vendor_pyr = TransPyr::where('vendor_lokal', $id)->get();

    	$vendorlokaltmuk = VendorLokalTmuk::find($id);

        
        if ($vendor_pyr->count() == 0){
            $vendorlokaltmuk->delete();
        } else {
            return response([
                'status' => false,
            ], 500);
        }
 
        //log insert
        $log = new LogVendorLokalTmuk;
        $log->vendor_lokal_tmuk_id = $vendorlokaltmuk->id;
        $log->kota_id = $vendorlokaltmuk->kota_id;
        $log->kecamatan_id = $vendorlokaltmuk->kecamatan_id;
        $log->rekening_escrow_id = $vendorlokaltmuk->rekening_escrow_id;
        $log->bank_escrow_id = $vendorlokaltmuk->bank_escrow_id;
        $log->pajak_id = $vendorlokaltmuk->pajak_id;
        $log->kode = $vendorlokaltmuk->kode;
        $log->nama = $vendorlokaltmuk->nama;
        $log->alamat = $vendorlokaltmuk->alamat;
        $log->kode_pos= $vendorlokaltmuk->kode_pos;
        $log->telepon = $vendorlokaltmuk->telepon;
        $log->email = $vendorlokaltmuk->email;
        $log->top = $vendorlokaltmuk->top;
        $log->lead_time = $vendorlokaltmuk->lead_time;
        $log->order_senin = $vendorlokaltmuk->order_senin;
        $log->order_selasa = $vendorlokaltmuk->order_selasa;
        $log->order_rabu = $vendorlokaltmuk->order_rabu;
        $log->order_kamis = $vendorlokaltmuk->order_kamis;
        $log->order_jumat = $vendorlokaltmuk->order_jumat;
        $log->order_sabtu = $vendorlokaltmuk->order_sabtu;
        $log->order_minggu = $vendorlokaltmuk->order_minggu;
        $log->contact_person = $vendorlokaltmuk->contact_person;
        $log->provinsi_id = $vendorlokaltmuk->provinsi_id;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Vendor Lokal',
            'ref'               => '',
            'aksi'              => 'Hapus Data Vendor Lokal',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
}
