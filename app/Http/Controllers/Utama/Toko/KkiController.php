<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\KkiRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Excel;

//Models
use Lotte\Models\Master\Tmuk;
use Lotte\Models\Master\Kki;
use Lotte\Models\Master\ProdukTmuk;
use Lotte\Models\Trans\TransLogAudit;

class KkiController extends Controller
{
	protected $link = 'utama/toko/kki/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("KKI TMUK");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen longer");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'KKI TMUK' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
                'data' => 'nomor',
                'name' => 'nomor',
                'label' => 'Nomor KKI',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '500px',
            ],
            [
			    'data' => 'tmuk_id',
			    'name' => 'tmuk_id',
			    'label' => 'TMUK',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '500px',
			],
            [
                'data' => 'lsi',
                'name' => 'lsi',
                'label' => 'LSI',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '500px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'print',
                'name' => 'print',
                'label' => 'Print',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ]
		]);
	}

	public function grid(Request $request)
	{
		$records = Kki::with('creator')
                         ->select('*');

       //Filters
        if ($nomor = $request->nomor) {
            $records->where('nomor', 'like', '%' . $nomor . '%');
        }

        if ($tmuk_id = $request->tmuk_id) {
            $records->where('tmuk_id',$tmuk_id);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })

            ->addColumn('tmuk_id', function ($record) {
                return $record->tmuk->nama;
            })

            ->addColumn('lsi', function ($record) {
                return $record->tmuk->lsi->nama;
            })

            ->addColumn('status',function($record){
                if($record->status == 1){
                    return 'Berjalan';
                }else if($record->status == 0){
                    return 'Selesai';
                }
            })

            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('detil', function ($record) {
                $string = '<button type="button" class="ui mini orange icon add button" data-content="Detil" data-id="1"> Detil</button>';
				return $string;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })

            ->addColumn('print', function ($record) {
                $string = '
                            <a target="_blank" href="kki/print-data-kki/'.$record->id.'" class="ui mini default icon print button" data-content="Print KKI"><i class="print icon"></i> Print</a>
                          ';
                return $string;
            })

            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.kki.index', ['mockup' => false]);
    }

    public function importexcel()
    {
        return $this->render('modules.utama.toko.kki.import');
    }

    public function create()
    {
        // return $this->render('modules.utama.toko.kki.create');

        // $code = Kki::generateCode();
        return $this->render('modules.utama.toko.kki.create');
    }

    public function edit($id)
    {

    	$record = Kki::find($id);

        return $this->render('modules.utama.toko.kki.edit', ['record' => $record]);
    }

     //Function upload template Produk
    public function postImportExcel(KkiRequest $request)
    {
        $import = [];
        foreach (ProdukTmuk::get() as $row) {
            $import[$row->lsi_id] = $row->id;
        }

        // dd($request->hasFile('produk_assortment'));
        if($request->hasFile('kki')){
            $path = $request->file('kki')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();

            if(!empty($data) && $data->count()){
                foreach ($data->toArray() as $key => $value) {
                    if(!is_null($value['lsi_id'])){ //jika data tidak boleh kosong tambahkan dengan &&
                        // cek assortment
                        if (!isset($import[$value['lsi_id']])) {
                                $record              = new Kki();
                                $record->lsi_id   = $value['lsi_id'];
                                $record->tmuk_id   = $value['tmuk_id'];
                                $record->tanggal_submit   = date('Y-m-d H:i:s');
                                $record->i_satu   = $value['i_satu'];
                                $record->i_dua   = $value['i_dua'];
                                $record->i_tiga_a   = $value['i_tiga_a'];
                                $record->i_tiga_b   = $value['i_tiga_b'];
                                $record->i_tiga_c   = $value['i_tiga_c'];
                                $record->i_tiga_d   = $value['i_tiga_d'];
                                $record->i_tiga_e   = $value['i_tiga_e'];
                                $record->i_tiga_f   = $value['i_tiga_f'];
                                $record->i_empat_a   = $value['i_empat_a'];
                                $record->i_empat_b   = $value['i_empat_b'];
                                $record->i_empat_c   = $value['i_empat_c'];
                                $record->i_lima_a   = $value['i_lima_a'];
                                $record->i_lima_b   = $value['i_lima_b'];
                                $record->i_sub_total   = $value['i_sub_total'];
                                $record->i_sewa_ruko   = $value['i_sewa_ruko'];
                                $record->i_total_inves1_1   = $value['i_total_inves1_1'];
                                $record->i_ppn   = $value['i_ppn'];
                                $record->i_total_inves1_2   = $value['i_total_inves1_2'];

                                $record->ii_struk_terburuk   = $value['ii_struk_terburuk'];
                                $record->ii_average_terburuk   = $value['ii_average_terburuk'];
                                $record->ii_sales_terburuk   = $value['ii_sales_terburuk'];
                                $record->ii_penjualan_terburuk   = $value['ii_penjualan_terburuk'];
                                $record->ii_struk_normal   = $value['ii_struk_normal'];
                                $record->ii_average_normal   = $value['ii_average_normal'];
                                $record->ii_sales_normal   = $value['ii_sales_normal'];
                                $record->ii_penjualan_normal   = $value['ii_penjualan_normal'];
                                $record->ii_struk_terbaik   = $value['ii_struk_terbaik'];
                                $record->ii_average_terbaik   = $value['ii_average_terbaik'];
                                $record->ii_sales_terbaik   = $value['ii_sales_terbaik'];
                                $record->ii_penjualan_terbaik   = $value['ii_penjualan_terbaik'];

                                $record->iii_labakotor_estimasi1   = $value['iii_labakotor_estimasi1'];
                                $record->iii_pendapatanlain2_estimasi1   = $value['iii_pendapatanlain2_estimasi1'];
                                $record->iii_pendapatansewa_estimasi1   = $value['iii_pendapatansewa_estimasi1'];
                                $record->iii_total_estimasi1   = $value['iii_total_estimasi1'];
                                $record->iii_labakotor_estimasi2   = $value['iii_labakotor_estimasi2'];
                                $record->iii_pendapatanlain2_estimasi2   = $value['iii_pendapatanlain2_estimasi2'];
                                $record->iii_pendapatansewa_estimasi2   = $value['iii_pendapatansewa_estimasi2'];
                                $record->iii_total_estimasi2   = $value['iii_total_estimasi2'];
                                $record->iii_labakotor_estimasi3   = $value['iii_labakotor_estimasi3'];
                                $record->iii_pendapatanlain2_estimasi3   = $value['iii_pendapatanlain2_estimasi3'];
                                $record->iii_pendapatansewa_estimasi3   = $value['iii_pendapatansewa_estimasi3'];
                                $record->iii_total_estimasi3   = $value['iii_total_estimasi3'];

                                $record->iv_satu_a_estimasi1   = $value['iv_satu_a_estimasi1'];
                                $record->iv_satu_b_estimasi1   = $value['iv_satu_b_estimasi1'];
                                $record->iv_satu_c_estimasi1   = $value['iv_satu_c_estimasi1'];
                                $record->iv_satu_d_estimasi1   = $value['iv_satu_d_estimasi1'];
                                $record->iv_satu_e_estimasi1   = $value['iv_satu_e_estimasi1'];
                                $record->iv_satu_f_estimasi1   = $value['iv_satu_f_estimasi1'];
                                $record->iv_satu_g_estimasi1   = $value['iv_satu_g_estimasi1'];
                                $record->iv_satu_h_estimasi1   = $value['iv_satu_h_estimasi1'];
                                $record->iv_satu_i_estimasi1   = $value['iv_satu_i_estimasi1'];
                                $record->iv_dua_estimasi1   = $value['iv_dua_estimasi1'];
                                $record->iv_tiga_a_estimasi1   = $value['iv_tiga_a_estimasi1'];
                                $record->iv_tiga_b_estimasi1   = $value['iv_tiga_b_estimasi1'];
                                $record->iv_empat_a_estimasi1   = $value['iv_empat_a_estimasi1'];
                                $record->iv_empat_b_estimasi1   = $value['iv_empat_b_estimasi1'];
                                $record->iv_empat_c_estimasi1   = $value['iv_empat_c_estimasi1'];
                                $record->iv_empat_d_estimasi1   = $value['iv_empat_d_estimasi1'];
                                $record->iv_total_estimasi1   = $value['iv_total_estimasi1'];
                                $record->iv_satu_a_estimasi2   = $value['iv_satu_a_estimasi2'];
                                $record->iv_satu_b_estimasi2   = $value['iv_satu_b_estimasi2'];
                                $record->iv_satu_c_estimasi2   = $value['iv_satu_c_estimasi2'];
                                $record->iv_satu_d_estimasi2   = $value['iv_satu_d_estimasi2'];
                                $record->iv_satu_e_estimasi2   = $value['iv_satu_e_estimasi2'];
                                $record->iv_satu_f_estimasi2   = $value['iv_satu_f_estimasi2'];
                                $record->iv_satu_g_estimasi2   = $value['iv_satu_g_estimasi2'];
                                $record->iv_satu_h_estimasi2   = $value['iv_satu_h_estimasi2'];
                                $record->iv_satu_i_estimasi2   = $value['iv_satu_i_estimasi2'];
                                $record->iv_dua_estimasi2   = $value['iv_dua_estimasi2'];
                                $record->iv_tiga_a_estimasi2   = $value['iv_tiga_a_estimasi2'];
                                $record->iv_tiga_b_estimasi2   = $value['iv_tiga_b_estimasi2'];
                                $record->iv_empat_a_estimasi2   = $value['iv_empat_a_estimasi2'];
                                $record->iv_empat_b_estimasi2   = $value['iv_empat_b_estimasi2'];
                                $record->iv_empat_c_estimasi2   = $value['iv_empat_c_estimasi2'];
                                $record->iv_empat_d_estimasi2   = $value['iv_empat_d_estimasi2'];
                                $record->iv_total_estimasi2   = $value['iv_total_estimasi2'];
                                $record->iv_satu_a_estimasi3   = $value['iv_satu_a_estimasi3'];
                                $record->iv_satu_b_estimasi3   = $value['iv_satu_b_estimasi3'];
                                $record->iv_satu_c_estimasi3   = $value['iv_satu_c_estimasi3'];
                                $record->iv_satu_d_estimasi3   = $value['iv_satu_d_estimasi3'];
                                $record->iv_satu_e_estimasi3   = $value['iv_satu_e_estimasi3'];
                                $record->iv_satu_f_estimasi3   = $value['iv_satu_f_estimasi3'];
                                $record->iv_satu_g_estimasi3   = $value['iv_satu_g_estimasi3'];
                                $record->iv_satu_h_estimasi3   = $value['iv_satu_h_estimasi3'];
                                $record->iv_satu_i_estimasi3   = $value['iv_satu_i_estimasi3'];
                                $record->iv_dua_estimasi3   = $value['iv_dua_estimasi3'];
                                $record->iv_tiga_a_estimasi3   = $value['iv_tiga_a_estimasi3'];
                                $record->iv_tiga_b_estimasi3   = $value['iv_tiga_b_estimasi3'];
                                $record->iv_empat_a_estimasi3   = $value['iv_empat_a_estimasi3'];
                                $record->iv_empat_b_estimasi3   = $value['iv_empat_b_estimasi3'];
                                $record->iv_empat_c_estimasi3   = $value['iv_empat_c_estimasi3'];
                                $record->iv_empat_d_estimasi3   = $value['iv_empat_d_estimasi3'];
                                $record->iv_total_estimasi3   = $value['iv_total_estimasi3'];

                                $record->v_satu_estimasi1   = $value['v_satu_estimasi1'];
                                $record->v_dua_estimasi1   = $value['v_dua_estimasi1'];
                                $record->v_satu_estimasi2   = $value['v_satu_estimasi2'];
                                $record->v_dua_estimasi2   = $value['v_dua_estimasi2'];
                                $record->v_satu_estimasi3   = $value['v_satu_estimasi3'];
                                $record->v_dua_estimasi3   = $value['v_dua_estimasi3'];
                                $record->created_by  = \Auth::user()->id;
                                $record->created_at  = date('Y-m-d H:i:s');

                                $record->save();

                        }
                    }
                }

                if(!empty($insert)){
                    ProdukTmuk::insert($insert);
                    return back()->with('success','Insert Record successfully.');
                }
            }
        }

        return back()->with('error','Please Check your file, Something is wrong there.');
    }

    public function onChangePopLsi($id=null)
    {
        $data = Tmuk::find($id);
        return response()->json([
            'lsi_id' => $data->lsi->nama,
        ]);
    }

    public function onChangePopNomor($id=null)
    {
        $getLast = Kki::select('nomor','id')->where('tmuk_id', $id)->orderBy('id','desc')->first();
        // dd($getLast->nomor);
        $n = 0;
        if(isset($getLast)){
            $n = (int) $getLast->nomor;
        }
        $data = Kki::generateCode($n);
        return response()->json([
            'nomor' => $data,
        ]);
    }

    public function store(KkiRequest $request)
    {
        $data = $request->all();

        // dd($data);

        $record = $request->all(); 
        $check = Kki::where('status', 1)->where('tmuk_id', $request->tmuk_id)->first();
        // dd($check);
        if (!is_null($check)) {
            return response([
                'error' => ['Hanya diperbolehkan 1 Status Berjalan untuk satu TMUK'],

            ], 422);
        } else {

        // dd($data);
        $record = new Kki;
        // $record->fill($request->all());

                                // $record              = new Kki();
                                
                                $record->tmuk_id        = $data['tmuk_id'];
                                $record->tanggal_submit = $data['tanggal_submit'];
                                $record->nomor          = $data['nomor'];
                                $record->status         = $data['status'];
                                $record->i_satu             = '0.00';
                                $record->i_dua              = '0.00';
                                $record->i_tiga_a           = '0.00';
                                $record->i_tiga_b           = '0.00';
                                $record->i_tiga_c           = '0.00';
                                $record->i_tiga_d           = '0.00';
                                $record->i_tiga_e           = '0.00';
                                $record->i_tiga_f           = '0.00';
                                $record->i_empat_a          = '0.00';
                                $record->i_empat_b          = '0.00';
                                $record->i_empat_c          = '0.00';
                                $record->i_lima_a           = '0.00';
                                $record->i_lima_b           = '0.00';
                                $record->i_sub_total        = '0.00';
                                $record->i_sewa_ruko        = '0.00';
                                $record->i_total_inves1_1   = '0.00';
                                $record->i_ppn              = '0.00';
                                $record->i_total_inves1_2   = '0.00';
                                $record->i_bila_ada_biaya_tambahan   = '0.00';
                                $record->i_perkiraan_stok_ideal      = '0.00';

                                $record->ii_struk_terburuk      = '0.00';
                                $record->ii_average_terburuk    = '0.00';
                                $record->ii_sales_terburuk      = '0.00';
                                $record->ii_penjualan_terburuk  = '0.00';
                                $record->ii_struk_normal        = '0.00';
                                $record->ii_average_normal      = '0.00';
                                $record->ii_sales_normal        = '0.00';
                                $record->ii_penjualan_normal    = '0.00';
                                $record->ii_struk_terbaik       = '0.00';
                                $record->ii_average_terbaik     = '0.00';
                                $record->ii_sales_terbaik       = '0.00';
                                $record->ii_penjualan_terbaik   = '0.00';

                                $record->iii_labakotor_estimasi1         = '0.00';
                                $record->iii_pendapatanlain2_estimasi1   = '0.00';
                                $record->iii_pendapatansewa_estimasi1    = '0.00';
                                $record->iii_total_estimasi1             = '0.00';
                                $record->iii_labakotor_estimasi2         = '0.00';
                                $record->iii_pendapatanlain2_estimasi2   = '0.00';
                                $record->iii_pendapatansewa_estimasi2    = '0.00';
                                $record->iii_total_estimasi2             = '0.00';
                                $record->iii_labakotor_estimasi3         = '0.00';
                                $record->iii_pendapatanlain2_estimasi3   = '0.00';
                                $record->iii_pendapatansewa_estimasi3    = '0.00';
                                $record->iii_total_estimasi3             = '0.00';

                                $record->iv_satu_a_estimasi1    = '0.00';
                                $record->iv_satu_b_estimasi1    = '0.00';
                                $record->iv_satu_c_estimasi1    = '0.00';
                                $record->iv_satu_d_estimasi1    = '0.00';
                                $record->iv_satu_e_estimasi1    = '0.00';
                                $record->iv_satu_f_estimasi1    = '0.00';
                                $record->iv_satu_g_estimasi1    = '0.00';
                                $record->iv_satu_h_estimasi1    = '0.00';
                                $record->iv_satu_i_estimasi1    = '0.00';
                                $record->iv_dua_estimasi1       = '0.00';
                                $record->iv_tiga_a_estimasi1    = '0.00';
                                $record->iv_tiga_b_estimasi1    = '0.00';
                                $record->iv_empat_a_estimasi1   = '0.00';
                                $record->iv_empat_b_estimasi1   = '0.00';
                                $record->iv_empat_c_estimasi1   = '0.00';
                                $record->iv_empat_d_estimasi1   = '0.00';
                                $record->iv_total_estimasi1     = '0.00';
                                $record->iv_satu_a_estimasi2    = '0.00';
                                $record->iv_satu_b_estimasi2    = '0.00';
                                $record->iv_satu_c_estimasi2    = '0.00';
                                $record->iv_satu_d_estimasi2    = '0.00';
                                $record->iv_satu_e_estimasi2    = '0.00';
                                $record->iv_satu_f_estimasi2    = '0.00';
                                $record->iv_satu_g_estimasi2    = '0.00';
                                $record->iv_satu_h_estimasi2    = '0.00';
                                $record->iv_satu_i_estimasi2    = '0.00';
                                $record->iv_dua_estimasi2       = '0.00';
                                $record->iv_tiga_a_estimasi2    = '0.00';
                                $record->iv_tiga_b_estimasi2    = '0.00';
                                $record->iv_empat_a_estimasi2   = '0.00';
                                $record->iv_empat_b_estimasi2   = '0.00';
                                $record->iv_empat_c_estimasi2   = '0.00';
                                $record->iv_empat_d_estimasi2   = '0.00';
                                $record->iv_total_estimasi2     = '0.00';
                                $record->iv_satu_a_estimasi3    = '0.00';
                                $record->iv_satu_b_estimasi3    = '0.00';
                                $record->iv_satu_c_estimasi3    = '0.00';
                                $record->iv_satu_d_estimasi3    = '0.00';
                                $record->iv_satu_e_estimasi3    = '0.00';
                                $record->iv_satu_f_estimasi3    = '0.00';
                                $record->iv_satu_g_estimasi3    = '0.00';
                                $record->iv_satu_h_estimasi3    = '0.00';
                                $record->iv_satu_i_estimasi3    = '0.00';
                                $record->iv_dua_estimasi3       = '0.00';
                                $record->iv_tiga_a_estimasi3    = '0.00';
                                $record->iv_tiga_b_estimasi3    = '0.00';
                                $record->iv_empat_a_estimasi3   = '0.00';
                                $record->iv_empat_b_estimasi3   = '0.00';
                                $record->iv_empat_c_estimasi3   = '0.00';
                                $record->iv_empat_d_estimasi3   = '0.00';
                                $record->iv_total_estimasi3     = '0.00';
                                $record->iv_tambahan_satu       = '0.00';
                                $record->iv_tambahan_satu_b     = '0.00';
                                $record->iv_tambahan_satu_c     = '0.00';
                                $record->iv_tambahan_dua        = '0.00';
                                $record->iv_tambahan_dua_b      = '0.00';
                                $record->iv_tambahan_dua_c      = '0.00';
                                $record->iv_tambahan_tiga       = '0.00';
                                $record->iv_tambahan_tiga_b     = '0.00';
                                $record->iv_tambahan_tiga_c     = '0.00';
                                $record->iv_tambahan_empat      = '0.00';
                                $record->iv_tambahan_empat_b    = '0.00';
                                $record->iv_tambahan_empat_c    = '0.00';

                                $record->v_satu_estimasi1   = '0.00';
                                $record->v_dua_estimasi1    = '0.00';
                                $record->v_satu_estimasi2   = '0.00';
                                $record->v_dua_estimasi2    = '0.00';
                                $record->v_satu_estimasi3   = '0.00';
                                $record->v_dua_estimasi3    = '0.00';
                                $record->created_by         = \Auth::user()->id;
                                $record->created_at         = date('Y-m-d H:i:s');


        $record->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data KKI',
            'ref'               => '',
            'aksi'              => 'Tambah Data KKI',
        ]);

        return response([
            'status' => true,
            'data'  => $record
        ]);
        }
    }

    public function update(Request $request, $id)
    {
// dd($request->all());
        $data = $request->all();
        $kki = Kki::find($id);

        

        $data['id']                 = $data['id'];
        $data['tmuk_id']            = $data['tmuk_id'];
        $data['tanggal_submit']     = $data['tanggal_submit'];
        $data['nomor']              = $data['nomor'];
        $data['status']             = $data['status'];
        $data['i_satu']             = decimal_for_save($data['i_satu']);
        $data['i_dua']              = decimal_for_save($data['i_dua']);
        $data['i_tiga_a']           = decimal_for_save($data['i_tiga_a']);
        $data['i_tiga_b']           = decimal_for_save($data['i_tiga_b']);
        $data['i_tiga_c']           = decimal_for_save($data['i_tiga_c']);
        $data['i_tiga_d']           = decimal_for_save($data['i_tiga_d']);
        $data['i_tiga_e']           = decimal_for_save($data['i_tiga_e']);
        $data['i_tiga_f']           = decimal_for_save($data['i_tiga_f']);
        $data['i_empat_a']          = decimal_for_save($data['i_empat_a']);
        $data['i_empat_b']          = decimal_for_save($data['i_empat_b']);
        $data['i_empat_c']          = decimal_for_save($data['i_empat_c']);
        $data['i_lima_a']           = decimal_for_save($data['i_lima_a']);
        $data['i_lima_b']           = decimal_for_save($data['i_lima_b']);
        $data['i_sub_total']        = decimal_for_save($data['i_sub_total']);
        $data['i_sewa_ruko']        = decimal_for_save($data['i_sewa_ruko']);
        $data['i_total_inves1_1']   = decimal_for_save($data['i_total_inves1_1']);
        $data['i_ppn']              = decimal_for_save($data['i_ppn']);
        $data['i_total_inves1_2']   = decimal_for_save($data['i_total_inves1_2']);
        $data['i_bila_ada_biaya_tambahan']   = decimal_for_save($data['i_bila_ada_biaya_tambahan']);
        $data['i_perkiraan_stok_ideal']      = decimal_for_save($data['i_perkiraan_stok_ideal']);

        $data['ii_struk_terburuk']      = decimal_for_save($data['ii_struk_terburuk']);
        $data['ii_average_terburuk']    = decimal_for_save($data['ii_average_terburuk']);
        $data['ii_sales_terburuk']      = decimal_for_save($data['ii_sales_terburuk']);
        $data['ii_penjualan_terburuk']  = decimal_for_save($data['ii_penjualan_terburuk']);
        $data['ii_struk_normal']        = decimal_for_save($data['ii_struk_normal']);
        $data['ii_average_normal']      = decimal_for_save($data['ii_average_normal']);
        $data['ii_sales_normal']        = decimal_for_save($data['ii_sales_normal']);
        $data['ii_penjualan_normal']    = decimal_for_save($data['ii_penjualan_normal']);
        $data['ii_struk_terbaik']       = decimal_for_save($data['ii_struk_terbaik']);
        $data['ii_average_terbaik']     = decimal_for_save($data['ii_average_terbaik']);
        $data['ii_sales_terbaik']       = decimal_for_save($data['ii_sales_terbaik']);
        $data['ii_penjualan_terbaik']   = decimal_for_save($data['ii_penjualan_terbaik']);

        $data['iii_labakotor_estimasi1']        = decimal_for_save($data['iii_labakotor_estimasi1']);
        $data['iii_pendapatanlain2_estimasi1']  = decimal_for_save($data['iii_pendapatanlain2_estimasi1']);
        $data['iii_pendapatansewa_estimasi1']   = decimal_for_save($data['iii_pendapatansewa_estimasi1']);
        $data['iii_total_estimasi1']            = decimal_for_save($data['iii_total_estimasi1']);
        $data['iii_labakotor_estimasi2']        = decimal_for_save($data['iii_labakotor_estimasi2']);
        $data['iii_pendapatanlain2_estimasi2']  = decimal_for_save($data['iii_pendapatanlain2_estimasi2']);
        $data['iii_pendapatansewa_estimasi2']   = decimal_for_save($data['iii_pendapatansewa_estimasi2']);
        $data['iii_total_estimasi2']            = decimal_for_save($data['iii_total_estimasi2']);
        $data['iii_labakotor_estimasi3']        = decimal_for_save($data['iii_labakotor_estimasi3']);
        $data['iii_pendapatanlain2_estimasi3']  = decimal_for_save($data['iii_pendapatanlain2_estimasi3']);
        $data['iii_pendapatansewa_estimasi3']   = decimal_for_save($data['iii_pendapatansewa_estimasi3']);
        $data['iii_total_estimasi3']            = decimal_for_save($data['iii_total_estimasi3']);


        
        $data['iv_satu_a_estimasi1']            = decimal_for_save($data['iv_satu_a_estimasi1']);
        $data['iv_satu_b_estimasi1']            = decimal_for_save($data['iv_satu_b_estimasi1']);
        $data['iv_satu_c_estimasi1']            = decimal_for_save($data['iv_satu_c_estimasi1']);
        $data['iv_satu_d_estimasi1']            = decimal_for_save($data['iv_satu_d_estimasi1']);
        $data['iv_satu_e_estimasi1']            = decimal_for_save($data['iv_satu_e_estimasi1']);
        $data['iv_satu_f_estimasi1']            = decimal_for_save($data['iv_satu_f_estimasi1']);
        $data['iv_satu_g_estimasi1']            = decimal_for_save($data['iv_satu_g_estimasi1']);
        $data['iv_satu_h_estimasi1']            = decimal_for_save($data['iv_satu_h_estimasi1']);
        $data['iv_satu_i_estimasi1']            = decimal_for_save($data['iv_satu_i_estimasi1']);
        $data['iv_dua_estimasi1']               = decimal_for_save($data['iv_dua_estimasi1']);
        $data['iv_tiga_a_estimasi1']            = decimal_for_save($data['iv_tiga_a_estimasi1']);
        $data['iv_tiga_b_estimasi1']            = decimal_for_save($data['iv_tiga_b_estimasi1']);
        $data['iv_empat_a_estimasi1']           = decimal_for_save($data['iv_empat_a_estimasi1']);
        $data['iv_empat_b_estimasi1']           = decimal_for_save($data['iv_empat_b_estimasi1']);
        $data['iv_empat_c_estimasi1']           = decimal_for_save($data['iv_empat_c_estimasi1']);
        $data['iv_empat_d_estimasi1']           = decimal_for_save($data['iv_empat_d_estimasi1']);
        $data['iv_total_estimasi1']             = decimal_for_save($data['iv_total_estimasi1']);
        $data['iv_satu_a_estimasi2']            = decimal_for_save($data['iv_satu_a_estimasi2']);
        $data['iv_satu_b_estimasi2']            = decimal_for_save($data['iv_satu_b_estimasi2']);
        $data['iv_satu_c_estimasi2']            = decimal_for_save($data['iv_satu_c_estimasi2']);
        $data['iv_satu_d_estimasi2']            = decimal_for_save($data['iv_satu_d_estimasi2']);
        $data['iv_satu_e_estimasi2']            = decimal_for_save($data['iv_satu_e_estimasi2']);
        $data['iv_satu_f_estimasi2']            = decimal_for_save($data['iv_satu_f_estimasi2']);
        $data['iv_satu_g_estimasi2']            = decimal_for_save($data['iv_satu_g_estimasi2']);
        $data['iv_satu_h_estimasi2']            = decimal_for_save($data['iv_satu_h_estimasi2']);
        $data['iv_satu_i_estimasi2']            = decimal_for_save($data['iv_satu_i_estimasi2']);
        $data['iv_dua_estimasi2']               = decimal_for_save($data['iv_dua_estimasi2']);
        $data['iv_tiga_a_estimasi2']            = decimal_for_save($data['iv_tiga_a_estimasi2']);
        $data['iv_tiga_b_estimasi2']            = decimal_for_save($data['iv_tiga_b_estimasi2']);
        $data['iv_empat_a_estimasi2']           = decimal_for_save($data['iv_empat_a_estimasi2']);
        $data['iv_empat_b_estimasi2']           = decimal_for_save($data['iv_empat_b_estimasi2']);
        $data['iv_empat_c_estimasi2']           = decimal_for_save($data['iv_empat_c_estimasi2']);
        $data['iv_empat_d_estimasi2']           = decimal_for_save($data['iv_empat_d_estimasi2']);
        $data['iv_total_estimasi2']             = decimal_for_save($data['iv_total_estimasi2']);
        $data['iv_satu_a_estimasi3']            = decimal_for_save($data['iv_satu_a_estimasi3']);
        $data['iv_satu_b_estimasi3']            = decimal_for_save($data['iv_satu_b_estimasi3']);
        $data['iv_satu_c_estimasi3']            = decimal_for_save($data['iv_satu_c_estimasi3']);
        $data['iv_satu_d_estimasi3']            = decimal_for_save($data['iv_satu_d_estimasi3']);
        $data['iv_satu_e_estimasi3']            = decimal_for_save($data['iv_satu_e_estimasi3']);
        $data['iv_satu_f_estimasi3']            = decimal_for_save($data['iv_satu_f_estimasi3']);
        $data['iv_satu_g_estimasi3']            = decimal_for_save($data['iv_satu_g_estimasi3']);
        $data['iv_satu_h_estimasi3']            = decimal_for_save($data['iv_satu_h_estimasi3']);
        $data['iv_satu_i_estimasi3']            = decimal_for_save($data['iv_satu_i_estimasi3']);
        $data['iv_dua_estimasi3']               = decimal_for_save($data['iv_dua_estimasi3']);
        $data['iv_tiga_a_estimasi3']            = decimal_for_save($data['iv_tiga_a_estimasi3']);
        $data['iv_tiga_b_estimasi3']            = decimal_for_save($data['iv_tiga_b_estimasi3']);
        $data['iv_empat_a_estimasi3']           = decimal_for_save($data['iv_empat_a_estimasi3']);
        $data['iv_empat_b_estimasi3']           = decimal_for_save($data['iv_empat_b_estimasi3']);
        $data['iv_empat_c_estimasi3']           = decimal_for_save($data['iv_empat_c_estimasi3']);
        $data['iv_empat_d_estimasi3']           = decimal_for_save($data['iv_empat_d_estimasi3']);
        $data['iv_total_estimasi3']             = decimal_for_save($data['iv_total_estimasi3']);
        $data['iv_tambahan_satu']               = decimal_for_save($data['iv_tambahan_satu']);
        $data['iv_tambahan_satu_b']             = decimal_for_save($data['iv_tambahan_satu_b']);
        $data['iv_tambahan_satu_c']             = decimal_for_save($data['iv_tambahan_satu_c']);
        $data['iv_tambahan_dua']                = decimal_for_save($data['iv_tambahan_dua']);
        $data['iv_tambahan_dua_b']              = decimal_for_save($data['iv_tambahan_dua_b']);
        $data['iv_tambahan_dua_c']              = decimal_for_save($data['iv_tambahan_dua_c']);
        $data['iv_tambahan_tiga']               = decimal_for_save($data['iv_tambahan_tiga']);
        $data['iv_tambahan_tiga_b']             = decimal_for_save($data['iv_tambahan_tiga_b']);
        $data['iv_tambahan_tiga_c']             = decimal_for_save($data['iv_tambahan_tiga_c']);
        $data['iv_tambahan_empat']              = decimal_for_save($data['iv_tambahan_empat']);
        $data['iv_tambahan_empat_b']            = decimal_for_save($data['iv_tambahan_empat_b']);
        $data['iv_tambahan_empat_c']            = decimal_for_save($data['iv_tambahan_empat_c']);


        $data['v_satu_estimasi1']               = decimal_for_save($data['v_satu_estimasi1']);
        $data['v_dua_estimasi1']                = decimal_for_save($data['v_dua_estimasi1']);
        $data['v_satu_estimasi2']               = decimal_for_save($data['v_satu_estimasi2']);
        $data['v_dua_estimasi2']                = decimal_for_save($data['v_dua_estimasi2']);
        $data['v_satu_estimasi3']               = decimal_for_save($data['v_satu_estimasi3']);
        $data['v_dua_estimasi3']                = decimal_for_save($data['v_dua_estimasi3']);
        
        $data['update_at']                      = date('Y-m-d H:i:s');
        $data['update_by']                      = \Auth::user()->id;


        $kki->fill($data);
        if($kki->save()){
            $data['kustomer_id'] = $kki->id;


            // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data KKI',
            'ref'               => '',
            'aksi'              => 'Ubah Data KKI',
        ]);

        return response([
            'status' => true,
            'data'  => $kki
        ]);
        }
    }

    // public function update(Request $request, $id)
    // {

    //     $kki = Kki::find($id);
    //     $kki->fill($request->all());
    //     $kki->save();

    //     return response([
    //         'status' => true,
    //         'data'  => $kki
    //     ]);
    // }
    public function printDataKki($id)
    {

        // dd($id)
        // dd($id)

        $records = Kki::find($id);
                        
        // $records = $records->get();

// dd($records);
        $data = [
            'row' => $records,
        ];
            $content = view('modules.utama.toko.kki.print-data-kki', $data);

            $data = [
                    // 'oriented'   => 'L',
                    'paper'      => 'A4',
                    'label_file' => 'Data KKI',
            ];

            HTML2PDF($content, $data);
    }

    public function destroy($id)
    {
        $kki = Kki::find($id);
        $kki->delete();


        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data KKI',
            'ref'               => '',
            'aksi'              => 'Hapus Data KKI',
        ]);

        return response([
            'status' => true,
        ]);
    }
}
