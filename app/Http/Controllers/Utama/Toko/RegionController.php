<?php

namespace Lotte\Http\Controllers\Utama\Toko;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\RegionRequest;
use Lotte\Http\Controllers\Controller;
use Excel;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\Region;
use Lotte\Models\Master\Log\LogRegion;
use Lotte\Models\Trans\TransLogAudit;

class RegionController extends Controller
{
	protected $link = 'utama/toko/region/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Region");
		$this->setSubtitle("&nbsp;");
		// $this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Toko' => '#', 'Region (Area)' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'area',
			    'name' => 'area',
			    'label' => 'Region',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Region::with('creator')
						 ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('kode');
        }

        //Filters
        if ($kode = $request->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($area = $request->area) {
            $records->where('area', 'ilike', '%' . $area . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.toko.region.index', ['mockup' => false]);
    }

    public function create()
    {
    	$code = Region::generateCode();
        return $this->render('modules.utama.toko.region.create', ['code' => $code]);
    }

    public function store(RegionRequest $request)
    {
    	$region = new Region;
    	$region->fill($request->all());
    	$region->save();

        //log insert
        $log = new LogRegion;
        $log->region_id = $region->id;
        $log->kode = $request->kode;
        $log->area = $request->area;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Region',
            'ref'               => '',
            'aksi'              => 'Tambah Data Region',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $region
    	]);
    }

    public function edit($id)
    {
    	$record = Region::find($id);

        return $this->render('modules.utama.toko.region.edit', ['record' => $record]);
    }

    public function update(RegionRequest $request, $id)
    {
    	$region = Region::find($id);
    	$region->fill($request->all());
    	$region->save();

        //log update
        $log = new LogRegion;
        $log->region_id = $region->id;
        $log->kode = $region->kode;
        $log->area = $region->area;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Region',
            'ref'               => '',
            'aksi'              => 'Ubah Data Region',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $region
    	]);
    }

    public function destroy($id)
    {
    	$region = Region::find($id);
    	$region->delete();

        //log update
        $log = new LogRegion;
        $log->region_id = $region->id;
        $log->kode = $region->kode;
        $log->area = $region->area;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Region',
            'ref'               => '',
            'aksi'              => 'Hapus Data Region',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
}
