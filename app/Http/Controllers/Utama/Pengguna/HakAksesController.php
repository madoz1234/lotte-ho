<?php

namespace Lotte\Http\Controllers\Utama\Pengguna;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;
use Lotte\Models\Entrust\Permission;
use Lotte\Models\Entrust\Role;
use Lotte\Http\Requests\Hakakses\HakAksesRequest;
//Libraries
use Datatables;
use Carbon\Carbon;

class HakAksesController extends Controller
{
	protected $link = 'utama/pengguna/hak-akses';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Hak Akses");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("large");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Pengguna' => '#', 'Hak Akses' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'display_name',
			    'name' => 'display_name',
			    'label' => 'Nama Role',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'description',
			    'name' => 'description',
			    'label' => 'Deskripsi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request){
		$records = Role::select('*');
		
		if($request->name){
            $records->where('name', 'like', '%'.$request->name.'%');
		}

		if (!isset(request()->order[0]['column'])) {
            $records->orderBy('id', 'DESC');
        }
        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
    	$data = [
    		'mockup'=> false, 
    	];
        return $this->render('modules.utama.pengguna.hak-akses.index',$data);
    }

    public function create()
    {
    	$data = Permission::get()->toArray();
    	$menu = [
            'menu-utama-',
            'transaksi-',
            'laporan-rincian-',
            'dashboard-'
    	];
        $temp2 = [];
        $temp  = [];
        $menus = $menu;
        foreach ($data as $val) { 
            $ext  = explode('-', $val['name']);
            $menu ='';
            foreach ($ext as $vals) {
                $menu = $vals;
            }

            if($menu == 'access'){
                $temp2[] = $val;
            }
            if($menu !== 'access'){
                $temp[] = $val;
            }
        }

        $result_data = [];
        $result      = [];
        $data        = [];
        $result_data2 = [];
        foreach ($menus as $input) {
            $result_data= array_filter($temp, function ($item) use ($input) {
                if (stripos($item['name'], $input) !== false) {
                    return true;
                }
                return false;
            });

            $rest_array = array_values($result_data);
            $data['child'] =[];
            foreach ($rest_array as $key => $val) {
                $input2         = str_replace(' ', '',$val['name']);
                $data2['child'] = [];
                if($key !== 0){
                    $result_data2 = array_filter($temp2, function ($item) use ($input2) {
                        if (stripos($item['name'], $input2) !== false) {
                            return true;
                        }
                        return false;
                    });
                    $rest_array2  = array_values($result_data2);
                    foreach ($rest_array2 as $key2 => $val2) {
                        // if($key2 !== 0){
                            $data2['child'][] = $val2;
                        // }
                    }
                    $data['child'][] = array_merge($val,$data2);
                }
            }
            $result[] = array_merge($rest_array[0], $data);
        }

    	$data=[
    		'record' => $result,
    	];
        return $this->render('modules.utama.pengguna.hak-akses.create' ,$data);
    }

    public function edit($id)
    {
        $role_data = Role::find($id);
        $data      = Permission::get()->toArray();
        $menu = [
            'menu-utama-',
            'transaksi-',
            'laporan-rincian-',
            'dashboard-'
        ];
        $temp2 = [];
        $temp  = [];
        $menus = $menu;
        foreach ($data as $val) { 
            $ext  = explode('-', $val['name']);
            $menu ='';
            foreach ($ext as $vals) {
                $menu = $vals;
            }

            if($menu == 'access'){
                $temp2[] = $val;
            }
            if($menu !== 'access'){
                $temp[] = $val;
            }
        }

        $result_data = [];
        $result      = [];
        $data        = [];
        $result_data2 = [];
        foreach ($menus as $input) {
            $result_data= array_filter($temp, function ($item) use ($input) {
                if (stripos($item['name'], $input) !== false) {
                    return true;
                }
                return false;
            });

            $rest_array = array_values($result_data);
            $data['child'] =[];
            foreach ($rest_array as $key => $val) {
                $input2         = str_replace(' ', '',$val['name']);
                $data2['child'] = [];
                if($key !== 0){
                    $result_data2 = array_filter($temp2, function ($item) use ($input2) {
                        if (stripos($item['name'], $input2) !== false) {
                            return true;
                        }
                        return false;
                    });
                    $rest_array2  = array_values($result_data2);
                    foreach ($rest_array2 as $key2 => $val2) {
                        // if($key2 !== 0){
                            $data2['child'][] = $val2;
                        // }
                    }
                    $data['child'][] = array_merge($val,$data2);
                }
            }
            $result[] = array_merge($rest_array[0], $data);
        }

    	$data=[
            'record'      => $result,
            'record_role' => $role_data,
            'checked'     => $role_data->cachedPermissions()
    	];
        return $this->render('modules.utama.pengguna.hak-akses.edit', $data);
    }

    public function store(HakAksesRequest $request){
    	if($request->id){
    		$role = Role::find($request->id);
    	}else{
    		$role = new Role();
    	}
		$role->name         = str_replace(' ','-',strtolower($request->name));
		$role->display_name = $request->name;
		$role->description  = $request->description;
        $role->save();
        $role->perms()->sync($request->role);
        // $roles->sync($request->role);
    }

    public function destroy($id){

		$role = Role::find($id);
        if($role->users->count() !== 0){
            return response([
        		'status' => false,
            ], 500);
        } else {
            $role->users()->sync([]); // Delete relationship data
            $role->perms()->sync([]); // Delete relationship data
            $role->forceDelete();
            return response([
                'status' => true,
            ]);
        }
    }

    public function smallify($arr, $numberOfSlices){
	  $sliceLength = sizeof($arr) /$numberOfSlices;
	  for($i=1; $i<=$numberOfSlices; $i++){

	       $arr1 = array_chunk($arr, $sliceLength*$i);
	       return $arr1;
	       // unset($arr1);

	   }
	}

    public function recursive($temp, $input=''){

    }
}
