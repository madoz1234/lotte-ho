<?php

namespace Lotte\Http\Controllers\Utama\Pengguna;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Toko\RegionRequest;
use Lotte\Http\Controllers\Controller;
use Excel;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\Backup;

class BackupController extends Controller
{
	protected $link = 'utama/pengguna/backup/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Backup Database");
		$this->setSubtitle("&nbsp;");
		// $this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Pengguna' => '#', 'Backup Database' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Backup::with('creator')
						 ->select('*');
		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($kode = $request->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if($created_at = $request->created_at){
            $dates = \Carbon\Carbon::parse($created_at)->format('Y-m-d');
            $records->where(\DB::raw('DATE(created_at)'),$dates);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $string = '<a href="'.url('utama/pengguna/backup/download/'.$record->id).'" class="ui mini icon blue button" data-content="Download Database"> <i class="download icon"></i></a>';
                return $string;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.pengguna.backup.index', ['mockup' => false]);
    }

     public function download($id)
    {
        $file = Backup::find($id);
        return redirect(url('storage/'.$file->nama));
    }

}
