<?php

namespace Lotte\Http\Controllers\Utama\Pengguna;

use Illuminate\Http\Request;

use Lotte\Http\Requests\Hakakses\PenggunaRequest;
use Lotte\Models\User;
use Lotte\Models\Entrust\Role;
use Lotte\Models\Entrust\Pengguna;
use Lotte\Models\Master\Lsi;
use Lotte\Http\Controllers\Controller;
use Datatables;

class PersetujuanController extends Controller
{
	protected $link = 'utama/pengguna/persetujuan/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Persetujuan Escrow");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Pengguna' => '#', 'Persetujuan' => '#']);
		// $this->setTableStruct([
		// 	[
		// 	    'data' => 'num',
		// 	    'name' => 'num',
		// 	    'label' => '#',
		// 	    'orderable' => false,
		// 	    'searchable' => false,
		// 	    'className' => "center aligned",
		// 	    'width' => '40px',
		// 	],
		// 	/* --------------------------- */
		// 	[
		// 	    'data' => 'nama_lengkap',
		// 	    'name' => 'nama_lengkap',
		// 	    'label' => 'Nama',
		// 	    'searchable' => false,
		// 	    'sortable' => true,
		// 	    'className' => "center aligned",
		// 	],
		// 	[
		// 	    'data' => 'email',
		// 	    'name' => 'email',
		// 	    'label' => 'Email',
		// 	    'searchable' => false,
		// 	    'sortable' => true,
		// 	    'className' => "center aligned",
		// 	],
		// 	[
		// 	    'data' => 'setuju',
		// 	    'name' => 'setuju',
		// 	    'label' => 'Persetujuan',
		// 	    'searchable' => false,
		// 	    'sortable' => false,
		// 	    'className' => "center aligned",
		// 	],
		// 	[
		// 	    'data' => 'login_terakhir',
		// 	    'name' => 'login_terakhir',
		// 	    'label' => 'Terakhir Login',
		// 	    'searchable' => false,
		// 	    'sortable' => false,
		// 	    'className' => "center aligned",
		// 	],
		// 	[
		// 	    'data' => 'created_at',
		// 	    'name' => 'created_at',
		// 	    'label' => 'Dibuat Pada',
		// 	    'searchable' => false,
		// 	    'sortable' => false,
		// 	    'className' => "center aligned",
		// 	],
		// 	[
		// 	    'data' => 'action',
		// 	    'name' => 'action',
		// 	    'label' => 'Aksi',
		// 	    'searchable' => false,
		// 	    'sortable' => false,
		// 	    'className' => "center aligned",
		// 	    'width' => '150px',
		// 	]
		// ]);
	}

    public function index()
    {

    	$persetujuan1 = Pengguna::with('user')->whereHas('user.roles', function($q){
    												$q->where('role_id', 11); // cso-persetujuan1
										    	})->get();
    	$persetujuan2 = Pengguna::with('user')->whereHas('user.roles', function($q){
    												$q->where('role_id', 12); // manager-cso-persetujuan2
										    	})->get();
    	$persetujuan3 = Pengguna::with('user')->whereHas('user.roles', function($q){
    												$q->where('role_id', 13); // director-cso-persetujuan3
										    	})->get();
    	$persetujuan4 = Pengguna::with('user')->whereHas('user.roles', function($q){
    												$q->where('role_id', 14); // finance-tmuk-persetujuan4
										    	})->get();
    	$data = [
    		'mockup' => false,
    		'persetujuan1' => $persetujuan1,
    		'persetujuan2' => $persetujuan2,
    		'persetujuan3' => $persetujuan3,
    		'persetujuan4' => $persetujuan4,
    	];
        return $this->render('modules.utama.pengguna.persetujuan.index', $data);
    }

    public function grid(Request $request){
    	// dd($request->all());
		// $records = Pengguna::->whereHas('produksetting', function($query){
		// 										$query->where('tipe_produk', 1);
		// 									})->get();
		$records = Pengguna::with('user','lsi')->select('*');
		
		if($request->name){
            $records->where('nama_lengkap', 'like', '%'.$request->name.'%');
		}

		// if (!isset(request()->order[0]['column'])) {
  //           $records->orderBy('id', 'DESC');
  //       }
        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('login_terakhir', function ($record) {
                return $record->user->updated_at->diffForHumans();
            })
            ->addColumn('setuju', function ($record) {
                // dd($record->user->roles()->name);
            	// return $record->user->roles;
                	 
                $string = '';
                // dd($record->user->users()->roles());
                foreach ($record->user->roles as $val) {
                	$string .= $val->display_name;
	                	if ($val->display_name == 'cso-persetujuan1') {
	                		return 'Persetujuan 1';
	                	}else if($val->display_name == 'manager-cso-persetujuan2'){
	                		return 'Persetujuan 2';
	                	}else if($val->display_name == 'director-cso-persetujuan3'){
	                		return 'Persetujuan 3';
	                	}else if($val->display_name == 'finance-tmuk-persetujuan4'){
	                		return 'Persetujuan 4';
	                	}
                    // return $string;
                }
                // dd($record->user->roles());
                // return $string;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            // ->rawColumns('role', 'lokasi', 'action');
            ->make(true);
    }

    public function store(PenggunaRequest $request)
    {
    	if($request->id){
			$pengguna = Pengguna::find($request->id);
			$user     = User::find($pengguna->user_id);
    	}else{
			$user     = new User();
			$pengguna = new Pengguna();
    	}
		$user->name     = $request->nama_lengkap;
		$user->email    = $request->email;
		$user->password = bcrypt($request->password);
		$users = $user->save();

		$user->roles()->sync($request->role);

    	if(!$request->id){
			$pengguna->user_id  = $user->id;
		}
		$pengguna->nama_lengkap = $request->nama_lengkap;
		$pengguna->telepon      = $request->telepon;
		$pengguna->email        = $request->email;
		$pengguna->lsi_id       = $request->lsi_code;

		if (request()->hasFile('ttd')) {
           	$foto = request()->file('ttd');
           	$filename = uploadFile($foto,'uploads/ttd/');
        }

        $pengguna->ttd       	= isset($filename->file) ? $filename->file : null;
		$pengguna->save();

    }

    

    public function create()
    {
    	$data = [
			'lsi_options'        => Lsi::select('id','kode', 'nama')->get(),
			'hak_aksess_options' => Role::select('id', 'display_name')->get()
    	];
        return $this->render('modules.utama.pengguna.pengguna.create', $data);
    }

    public function edit($id)
    {
    	$data_pengguna = Pengguna::find($id); 
    	// dd(json_encode($data_pengguna->user->roles->toArray()));
    	$checked =[];
    	foreach ($data_pengguna->user->roles as $val) {
    		$checked[]= $val->pivot->role_id;
    	}

    	$data = [
			'record'             => $data_pengguna,
			'checked'            => $checked,
			'lsi_options'        => Lsi::select('id','kode', 'nama')->get(),
			'hak_aksess_options' => Role::select('id', 'display_name')->get()

    	];
        return $this->render('modules.utama.pengguna.pengguna.edit', $data);
    }

    public function destroy($id){
		$pengguna = Pengguna::find($id);
		$user = User::find($pengguna->user_id);
		if($user){
	        $user->roles()->sync([]); // Delete relationship data
        	$pengguna->delete();
	        $user->forceDelete();
		}
        return response([
    		'status' => true,
    	]);
    }
}
