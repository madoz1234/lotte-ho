<?php

namespace Lotte\Http\Controllers\Utama\Pengguna;

use Illuminate\Http\Request;

use Lotte\Http\Requests\Hakakses\PenggunaRequest;
use Lotte\Models\User;
use Lotte\Models\Entrust\Role;
use Lotte\Models\Entrust\Pengguna;
use Lotte\Models\Master\Lsi;
use Lotte\Http\Controllers\Controller;
use Datatables;
class PenggunaController extends Controller
{
	protected $link = 'utama/pengguna/pengguna/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Pengguna");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Pengguna' => '#', 'Pengguna' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama_lengkap',
			    'name' => 'nama_lengkap',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'email',
			    'name' => 'email',
			    'label' => 'Email',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'lokasi',
			    'name' => 'lokasi',
			    'label' => 'Lokasi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'role',
			    'name' => 'role',
			    'label' => 'Role',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'login_terakhir',
			    'name' => 'login_terakhir',
			    'label' => 'Terakhir Login',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

    public function index()
    {
    	$data = [
    		'mockup' => false
    	];
        return $this->render('modules.utama.pengguna.pengguna.index', $data);
    }

    public function grid(Request $request){
		$records = Pengguna::with('user','lsi')->select('*');
		
		if($request->name){
            $records->where('nama_lengkap', 'ilike', '%'.$request->name.'%');
		}

		if($request->lsi){
            $records->where('lsi_id', $request->lsi);
		}

		if (!isset(request()->order[0]['column'])) {
            $records->orderBy('id', 'DESC');
        }
        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('login_terakhir', function ($record) {
                return $record->user->updated_at->diffForHumans();
            })
            ->addColumn('role', function ($record) {
                $string = '';
                // dd($record->user->users()->roles());
                foreach ($record->user->roles as $val) {
                	$string .= '<div class="ui horizontal label" style="padding:5px; margin:2px;">'.$val->display_name.'</div><br>';
                }
                // dd($record->user->roles());
                return $string;
            })
            ->addColumn('lokasi', function ($record) {
            	// dd($record->lsi);
                return $record->lsi->nama;
            })
            ->addColumn('action', function ($record) {
	            if (auth()->user()->hasRole('system-administrator')) {
	                $disabled = '';
	            } else {
	                $disabled = 'disabled';
	            }

                $btn = '';
                //Edit
                $btn .= $this->makeButton([
					'type'     => 'edit',
					'id'       => $record->id,
					'disabled' => $disabled
                ]);
                // Delete
                $btn .= $this->makeButton([
					'type'     => 'delete',
					'id'       => $record->id,
					'disabled' => $disabled
                ]);

                return $btn;
            })
            // ->rawColumns('role', 'lokasi', 'action');
            ->make(true);
    }

    public function store(PenggunaRequest $request)
    {
    	if($request->id){
			$pengguna = Pengguna::find($request->id);
			$user     = User::find($pengguna->user_id);
    	}else{
			$user     = new User();
			$pengguna = new Pengguna();
    	}
		$user->name     = $request->nama_lengkap;
		$user->email    = $request->email;
		$user->password = bcrypt($request->password);
		$users = $user->save();

		$user->roles()->sync($request->role);

    	if(!$request->id){
			$pengguna->user_id  = $user->id;
		}
		$pengguna->nama_lengkap = $request->nama_lengkap;
		$pengguna->telepon      = $request->telepon;
		$pengguna->email        = $request->email;
		$pengguna->lsi_id       = $request->lsi_code;

		if (request()->hasFile('ttd')) {
           	$foto = request()->file('ttd');
           	$filename = uploadFile($foto,'uploads/ttd/');
        }

        $pengguna->ttd       	= isset($filename->file) ? $filename->file : null;
		$pengguna->save();

    }

    

    public function create()
    {
    	$data = [
			'lsi_options'        => Lsi::select('id','kode', 'nama')->get(),
			'hak_aksess_options' => Role::select('id', 'display_name')->get()
    	];
        return $this->render('modules.utama.pengguna.pengguna.create', $data);
    }

    public function edit($id)
    {
    	$data_pengguna = Pengguna::find($id); 
    	// dd(json_encode($data_pengguna->user->roles->toArray()));
    	$ttd = '';
    	$checked =[];
    	foreach ($data_pengguna->user->roles as $val) {
    		$checked[]= $val->pivot->role_id;

    		// 11 = cso-persetujuan1 , 12 = manager-cso-persetujuan2, 13 = director-cso-persetujuan3, 14 = finance-tmuk-persetujuan4
    		if ($val->pivot->role_id == 11 || $val->pivot->role_id == 12 || $val->pivot->role_id == 13 || $val->pivot->role_id == 14) {
    			$ttd = 'true';
    		}
    	}

    	$data = [
			'record'             => $data_pengguna,
			'checked'            => $checked,
			'has_role'           => $ttd,
			'lsi_options'        => Lsi::select('id','kode', 'nama')->get(),
			'hak_aksess_options' => Role::select('id', 'display_name')->get()

    	];
        return $this->render('modules.utama.pengguna.pengguna.edit', $data);
    }

    public function update(PenggunaRequest $request, $id){
		$pengguna    = Pengguna::find($request->id);
		$user        = User::find($pengguna->user_id);

		$user->name  = $request->nama_lengkap;
		$user->email = $request->email;
		$users       = $user->save();

		$user->roles()->sync($request->role);

		$pengguna->nama_lengkap = $request->nama_lengkap;
		$pengguna->telepon      = $request->telepon;
		$pengguna->email        = $request->email;
		$pengguna->lsi_id       = $request->lsi_code;

		if (request()->hasFile('ttd')) {
			$foto     = request()->file('ttd');
			$filename = uploadFile($foto,'uploads/ttd/');
        }
        
        $pengguna->ttd = isset($filename->file) ? $filename->file : null;
		$pengguna->save();

        return response([
            'status' => true,
            'data'  => $pengguna
        ]);
    }

    public function destroy($id){
		$pengguna = Pengguna::find($id);
		$user = User::find($pengguna->user_id);
		if($user){
	        $user->roles()->sync([]); // Delete relationship data
        	$pengguna->delete();
	        $user->forceDelete();
		}
        return response([
    		'status' => true,
    	]);
    }
}
