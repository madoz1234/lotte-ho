<?php

namespace Lotte\Http\Controllers\Utama\Planogram;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

use Lotte\Models\Master\Rak;
use Lotte\Models\Master\JenisAssortment;
use Lotte\Models\Master\Produk;
use Lotte\Models\Planogram\Planogram;
use Lotte\Models\Planogram\Detail;
use Lotte\Models\Master\TahunFiskal;

use Datatables;
use Storage;

class RencanaPlanogramController extends Controller
{
	protected $link = 'utama/planogram/rencana-planogram/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Planogram");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Planogram' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'kode',
			    'name' => 'kode',
			    'label' => 'Kode Planogram',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama Planogram',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'assortment.nama',
			    'name' => 'assortment.nama',
			    'label' => 'Assortment Type',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'status',
			    'name' => 'status',
			    'label' => 'Status',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$link = $this->link;
		$records = Planogram::with('creator', 'assortment')
						 	->select('ref_planogram.*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('kode');
        }

        //Filters
        if ($kode = $request->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'like', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->editColumn('status', function ($record) {
                return $record->status
                	 ? 'Aktif'
                	 : 'Non-Aktif';
            })
            ->addColumn('action', function ($record) use ($link){
                $btn = '';
                //View Data
                $btn .= $this->makeButton([
                    'type'      => 'url',
                    'class'     => 'orange icon view',
                    'label'     => '<i class="search icon"></i>',
                    'tooltip'   => 'View Data',
                    'url'       => url($link.$record->id.'/edit'),
                    'id'        => $record->id
                ]);
                //Print Data
                $btn .= $this->makeButton([
                	'type' 		=> 'url',
                	'class' 	=> 'blue icon view',
                	'label'   	=> '<i class="print icon"></i>',
                	'tooltip' 	=> 'Print Data',
                	'url'   	=> url($link.$record->id.'/print'),
                    'id'        => $record->id,
                    'attributes' => [
                        'target' => '_blank'
                    ]
                ]);
                //Analisis Data
                // $btn .= $this->makeButton([
                // 	'type' 		=> 'url',
                // 	'class' 	=> 'green icon analisis',
                // 	'label'   	=> '<i class="file text icon"></i>',
                // 	'tooltip' 	=> 'Analisis Data',
                // 	'url'   	=> url($link.$record->id),
                // 	'id'   		=> $record->id
                // ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
    	$assortment = JenisAssortment::orderByRaw('nama DESC')->get();
        return $this->render('modules.utama.planogram.rencana-planogram.index', [
        	'mockup' => false,
        	'assortment' => $assortment
        ]);
    }

    public function create()
    {
    	$assortment = JenisAssortment::orderByRaw('nama DESC')->get();
        return $this->render('modules.utama.planogram.rencana-planogram.create', [
        	'assortment' => $assortment
        ]);
    }

    public function products()
    {
        return $this->render('modules.utama.planogram.rencana-planogram.products');
    }

    public function productsGrid(Request $request)
    {
		$records = Produk::with('produksetting', 'produkAssortment')
						 ->select('*');
        //Filters
		if($assort = $request->assortment){
			$records->whereHas('produkAssortment', function($a) use ($assort){
				$a->where('assortment_type_id', $assort);
			});
		}
        if ($kode = $request->produk_kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($nama = $request->nama) {
        	$records->where('nama', 'like', '%' . $nama . '%');
        }
        if ($bumun_nm = $request->bumun_nm) {
        	$records->where('bumun_nm', 'like', '%' . $bumun_nm . '%');
        }
        if ($l1_nm = $request->l1_nm) {
            $records->where('l1_nm', 'like', '%' . $l1_nm . '%');
        }
        if ($l2_nm = $request->l2_nm) {
            $records->where('l2_nm', 'like', '%' . $l2_nm . '%');
        }
        if ($l3_nm = $request->l3_nm) {
        	$records->where('l3_nm', 'like', '%' . $l3_nm . '%');
        }
        if ($status_prod = $request->status_prod) {
        	$records->where('status_prod', 'like', '%' . $status_prod . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'class' => 'blue icon insert product',
                	'label' => '<i class="plus icon"></i>',
                	'tooltip' => 'Tambahkan Produk',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
    }

    public function store(Request $request)
    {
    	$record = new Planogram;
    	$record->fill($request->all());
    	$record->save();

    	return response([
    		'success' => true,
    		'data' => $record,
    	]);
    }

    public function edit($id)
    {
    	$this->setTitle('Rancang Data Planogram');
    	$this->setSubtitle('Manajemen penataan produk');
    	$this->pushBreadcrumb(['Planogram Utama' => '#']);

    	$data['record'] = Planogram::find($id);
    	$data['rak'] = Rak::with(['planogram' => function($q) use ($id){
    						$q->where('planogram_id', $id)
                              ->orderBy('nomor_rak');
					      }])
                          ->where('shelving', '>', 0)
    					  ->orderBy('tipe_rak', 'desc')
    					  ->get();

        return $this->render('modules.utama.planogram.rencana-planogram.edit', $data);
    }

    public function show($id)
    {
        return $this->render('modules.utama.planogram.rencana-planogram.view');
	}
	
	public function save(Request $request)
	{
		$req = $request->detail;
		$record = ($req['id'] == '') ? new Detail : Detail::find($req['id']);
		$record->fill($req);
		$record->save();
        $file = $this->generate($record->planogram->id, true);
        // dd($file);

		return response([
			'status' => true,
			'data'	=> $record
		]);
	}

    public function destroy($id)
    {
    	$region = Planogram::find($id);
    	$region->delete();

    	return response([
    		'status' => true,
    	]);
    }

    public function add(Request $request)
    {
        $id = $request->planogram_id;
        $rak = $request->rak_id;
        $plano = Planogram::find($id);
        $nomor = $plano->detail()
                       ->where('rak_id', $rak)
                       ->orderBy('nomor_rak', 'desc')
                       ->first();

        $detail = new Detail();
        $detail->rak_id = $rak;
        $detail->nomor_rak = $nomor->nomor_rak + 1;
        $detail->planogram_json = json_encode([]);
        $plano->detail()->save($detail);

        return redirect()->back();
    }

    public function download($id)
    {
        return $this->generate($id);
    }

    public function generate($id, $ret=false)
    {
        $data['oriented']       = 'L';
        $data['fiskal']         = TahunFiskal::getTahunFiskal();
        $data['record']         = Planogram::find($id);
        $data['label_file']     = $data['record']->kode;
        $data['rak']            = Rak::with(['planogram' => function($q) use ($id){
                                       $q->where('planogram_id', $id)
                                         ->orderBy('nomor_rak');
                                     }])
                                     ->orderBy('tipe_rak', 'desc')
                                     ->get();

        $content = view('modules.utama.planogram.rencana-planogram.print', $data);
        if($ret){
            // echo $content;
            return HTML2PDF($content, $data, true);
        }else{
            return HTML2PDF($content, $data);
        }
    }
}
