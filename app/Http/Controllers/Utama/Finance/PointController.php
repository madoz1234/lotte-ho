<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Finance\PointRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Helpers\Untilities;

//Models
use Lotte\Models\Master\Point;
use Lotte\Models\Master\Log\LogPoint;
use Lotte\Models\Trans\TransLogAudit;

class PointController extends Controller
{
	protected $link = 'utama/finance/point/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Point");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Point' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
                'data' => 'tgl_berlaku',
                'name' => 'tgl_berlaku',
                'label' => 'Tanggal Berlaku',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
			    'data' => 'konversi',
			    'name' => 'konversi',
			    'label' => 'Kelipatan (Rp)',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
            [
                'data' => 'faktor_konversi',
                'name' => 'faktor_konversi',
                'label' => 'Faktor Konversi',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'faktor_reedem',
                'name' => 'faktor_reedem',
                'label' => 'Faktor Reedem (Point/Rp)',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Point::with('creator')
						 ->select('*');
        // $record = Pajak::select('*');
        // dd($record);

		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        // if ($npwp = $request->npwp) {
        //     $records->where('npwp', 'like', '%' . $npwp . '%');
        // }
        // if ($nama = $request->nama) {
        //     $records->where('nama', 'like', '%' . $nama . '%');
        // }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('konversi',function($record){
                return 'Rp ' . rupiah($record->konversi);
            })
            ->addColumn('faktor_konversi',function($record){
                return ($record->faktor_konversi) . ' %';
            })

            ->addColumn('faktor_reedem',function($record){
                return rupiah($record->faktor_reedem);
            })

           ->addColumn('status',function($record){
                if($record->status == 1){
                    return 'Aktif';
                }else if($record->status == 0){
                    return 'Non Aktif';
                }
            })

            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.point.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.finance.point.create');
    }

    public function store(PointRequest $request)
    {
    	// $point = new Point;
    	// $point->fill($request->all());
    	// $point->save();

        $point = $request->all(); 
        $check = Point::where('status', 1)->first();
        // dd($check);
        if (!is_null($check)) {
            return response([
                'error' => ['Hanya diperbolehkan 1 Status Aktif yang boleh berlaku'],

            ], 422);
        } else {
            // return TahunFiskal::where(['status' => 1])->update($tahunfiskal);
            // dd($tahunfiskal);
            $point = new Point;
            $point->fill($request->all());
            $point->save(); 

        //log insert
        $log = new LogPoint;
        $log->point_id = $point->id;
        $log->tgl_berlaku = $request->tgl_berlaku;
        $log->konversi = $request->konversi;
        $log->status = $request->status;
        $log->faktor_konversi = $request->faktor_konversi;
        $log->faktor_reedem = $request->faktor_reedem;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Point',
            'ref'               => '',
            'aksi'              => 'Tambah Data Point',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $point
    	]);
        }
    }

    public function edit($id)
    {
    	$record = Point::find($id);

        return $this->render('modules.utama.finance.point.edit', ['record' => $record]);
    }

    public function update(PointRequest $request, $id)
    {
    	// $point = Point::find($id);
    	// $point->fill($request->all());
    	// $point->save();
        $data = $request->all();
        $point = Point::find($id);

        if (!is_null($point)) {
            $point->tgl_berlaku = $data['tgl_berlaku'];
            $point->konversi = $data['konversi'];
            $point->faktor_konversi = $data['faktor_konversi'];
            $point->faktor_reedem = $data['faktor_reedem'];
            if ($point['status'] != 0) {
                $point->status = $data['status'];
            }
            $point->save(); 

        //log update
        $log = new LogPoint;
        $log->point_id = $id;
        $log->tgl_berlaku = $point->tgl_berlaku;
        $log->konversi = $point->konversi;
        $log->status = $point->status;
        $log->faktor_konversi = $point->faktor_konversi;
        $log->faktor_reedem = $point->faktor_reedem;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Point',
            'ref'               => '',
            'aksi'              => 'Ubah Data Point',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $point
    	]);
        }
    }

    public function destroy($id)
    {
    	$point = Point::find($id);
    	$point->delete();

        // log delete
        $log = new LogPoint;
        $log->point_id = $id;
        $log->tgl_berlaku = $point->tgl_berlaku;
        $log->konversi = $point->konversi;
        $log->status = $point->status;
        $log->faktor_konversi = $point->faktor_konversi;
        $log->faktor_reedem = $point->faktor_reedem;
        
        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Point',
            'ref'               => '',
            'aksi'              => 'Hapus Data Point',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
}
