<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

class AkunGlController extends Controller
{
	protected $link = 'utama/finance/akun-gl/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Akun GL");
		$this->setSubtitle("Data Akun GL Finance");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Akun GL' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'akun_code',
			    'name' => 'akun_code',
			    'label' => 'Akun Code',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'akun_nama',
			    'name' => 'akun_nama',
			    'label' => 'Nama Akun',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'akun_group',
			    'name' => 'akun_group',
			    'label' => 'Akun Group',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.akun-gl.index');
    }

    public function create()
    {
        return $this->render('modules.utama.finance.akun-gl.create');
    }

    public function edit($id)
    {
        return $this->render('modules.utama.finance.akun-gl.edit');
    }
}
