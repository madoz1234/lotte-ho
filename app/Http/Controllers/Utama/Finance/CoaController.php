<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Finance\CoaRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\TipeCoa;
// use Lotte\Models\Master\Coa1;
// use Lotte\Models\Master\Coa2;
// use Lotte\Models\Master\Coa3;
use Lotte\Models\Master\Coa;
use Lotte\Models\Trans\TransLogAudit;

class CoaController extends Controller
{
    protected $link = 'utama/finance/coa/';
    protected $array_table = [
    'struct_type_coa'=>[
    [
    'data' => 'num',
    'name' => 'num',
    'label' => '#',
    'orderable' => false,
    'searchable' => false,
    'className' => "center aligned",
    'width' => '40px',
    ],
    /* --------------------------- */
    [
    'data' => 'tipe_coa_id',
    'name' => 'tipe_coa_id',
    'label' => 'TIPE',
    'searchable' => false,
    'sortable' => true,
    'className' => "center aligned",
    ],
    [
    'data' => 'kode',
    'name' => 'kode',
    'label' => 'KODE',
    'searchable' => false,
    'sortable' => true,
    'className' => "center aligned",
    ],
    [
    'data' => 'nama',
    'name' => 'nama',
    'label' => 'NAMA',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'status',
    'name' => 'status',
    'label' => 'Status',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'created_at',
    'name' => 'created_at',
    'label' => 'Dibuat Pada',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ],
    [
    'data' => 'action',
    'name' => 'action',
    'label' => 'Aksi',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ]
    ],
    'struct_tipe'=>[
    [
    'data' => 'num',
    'name' => 'num',
    'label' => '#',
    'orderable' => false,
    'searchable' => false,
    'className' => "center aligned",
    'width' => '40px',
    ],
    /* --------------------------- */
    [
    'data' => 'kode',
    'name' => 'kode',
    'label' => 'KODE',
    'searchable' => false,
    'sortable' => true,
    'className' => "center aligned",
    ],
    [
    'data' => 'nama',
    'name' => 'nama',
    'label' => 'NAMA',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'status',
    'name' => 'status',
    'label' => 'Status',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'created_at',
    'name' => 'created_at',
    'label' => 'Dibuat Pada',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ],
    [
    'data' => 'action',
    'name' => 'action',
    'label' => 'Aksi',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ]
    ],
    'struct_coa1'=>[
    [
    'data' => 'num',
    'name' => 'num',
    'label' => '#',
    'orderable' => false,
    'searchable' => false,
    'className' => "center aligned",
    'width' => '40px',
    ],
    /* --------------------------- */
    [
    'data' => 'kode',
    'name' => 'kode',
    'label' => 'Kode',
    'searchable' => false,
    'sortable' => true,
    'className' => "center aligned",
    ],
    [
    'data' => 'nama',
    'name' => 'nama',
    'label' => 'NAMA',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'status',
    'name' => 'status',
    'label' => 'Status',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'created_at',
    'name' => 'created_at',
    'label' => 'Dibuat Pada',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ],
    [
    'data' => 'action',
    'name' => 'action',
    'label' => 'Aksi',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ]
    ],

    'struct_coa2'=>[
    [
    'data' => 'num',
    'name' => 'num',
    'label' => '#',
    'orderable' => false,
    'searchable' => false,
    'className' => "center aligned",
    'width' => '40px',
    ],
    /* --------------------------- */
    [
    'data' => 'kode',
    'name' => 'kode',
    'label' => 'KODE',
    'searchable' => false,
    'sortable' => true,
    'className' => "center aligned",
    ],
    [
    'data' => 'nama',
    'name' => 'nama',
    'label' => 'NAMA',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'status',
    'name' => 'status',
    'label' => 'Status',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'created_at',
    'name' => 'created_at',
    'label' => 'Dibuat Pada',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ],
    [
    'data' => 'action',
    'name' => 'action',
    'label' => 'Aksi',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ]
    ],

    'struct_coa3'=>[
    [
    'data' => 'num',
    'name' => 'num',
    'label' => '#',
    'orderable' => false,
    'searchable' => false,
    'className' => "center aligned",
    'width' => '40px',
    ],
    /* --------------------------- */
    [
    'data' => 'kode',
    'name' => 'kode',
    'label' => 'KODE',
    'searchable' => false,
    'sortable' => true,
    'className' => "center aligned",
    ],
    [
    'data' => 'nama',
    'name' => 'nama',
    'label' => 'NAMA',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'status',
    'name' => 'status',
    'label' => 'Status',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    ],
    [
    'data' => 'created_at',
    'name' => 'created_at',
    'label' => 'Dibuat Pada',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ],
    [
    'data' => 'action',
    'name' => 'action',
    'label' => 'Aksi',
    'searchable' => false,
    'sortable' => false,
    'className' => "center aligned",
    'width' => '150px',
    ]
    ],
    ];

    function __construct()
    {
        $this->setLink($this->link);
        $this->setTitle("COA");
        $this->setSubtitle("&nbsp;");
        $this->setModalSize("mini");
        $this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'COA' => '#']);
        // $this->setTableStruct($this->array_table);
    }

    // GRID VIEW
    public function gridCoa(Request $request)
    {
        // dd($request->all());
        $records = Coa::select('*');
        if($request->kode){
            $records->where('parent_kode', $request->kode);
        }

        if($request->tipe_id){
            $records->where('tipe_coa_id', $request->tipe_id);

        }
        if($request->kode){
            $records->where('parent_kode', $request->kode);
        }else{
            $records->whereNotNull('tipe_coa_id');
        }

        if($request->searchbox){
            $records->where('nama', 'ilike', '%'.$request->searchbox.'%');
        }

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->diffForHumans();
        })
        ->addColumn('status',function($record){
            if($record->status == 1){
                return 'Aktif';
            }else if($record->status == 0){
                return 'Non-Aktif';
            }
        })
            // ->addColumn('tipe_coa_id', function ($record) {
            //     return $record->type->nama;
            // }) 
        ->addColumn('action', function ($record) {
            $btn = '';
            $btn .= '<button class="ui mini orange icon edit button disabled" type="button" data-content="Ubah Data"><i class="edit icon"></i></button>';
                // Delete
            $btn .= '<button class="ui mini red icon delete button disabled" type="button" data-content="Hapus Data"><i class="trash icon"></i></button>';

            return $btn;
        })
        // ->addColumn('action', function ($record) {
        //     $btn = '';
        //     $btn .= $this->makeButton([
        //         'type' => 'other',
        //         'id'   => $record->id,
        //         'event' => "onclick='showFormEdit({$record->id})'",
        //         ]);
        //         // Delete
        //     $btn .= $this->makeButton([
        //         'type' => 'delete',
        //         'id'   => $record->id
        //         ]);

        //     return $btn;
        // })
        ->make(true);
    }

    public function index()
    {

        // $array_menu = Coa::with('coaParent')->where('parent_kode' , NULL)->get();

        $array_menu = TipeCoa::with('coa')->get();

        // dd($array_menu);

        $tipe_coa = [];

        foreach ($array_menu as $tipe) {
            // dd($tipe);

            $temp_menu =[];
            foreach ($tipe->coa as $coa) {
            // dd($coa);

                $child1 =[];
                foreach ($coa->coaParent as $coa2) {
                // dd($coa2);

                    $child2 =[];
                    foreach($coa2->coaParent as $coa3){
                    // dd($coa3);
                        $child2[] = [
                        'tipe' => 'coa3',
                        // 'tipe_coa_id' => $coa->tipe_coa_id,
                        'parent_kode' => $coa3->parent_kode,
                        'kode'      => $coa3->kode,
                        'nama'      => $coa3->nama,
                        ];
                    }
                    $child1[] =[
                    'table' => 'coa2',
                    // 'tipe_coa_id' => $coa->tipe_coa_id,
                    'parent_kode' => $coa2->parent_kode,
                    'kode'      => $coa2->kode,
                    'nama'      => $coa2->nama,
                    'count'       => count($child2),
                    'child'     => $child2,
                    ];
                }
                $temp_menu[] =[
                'table' => 'coa1',
                'tipe_coa_id' => $coa->tipe_coa_id,
                'kode'        => $coa->kode,
                'nama'        => $coa->nama,
                'count'       => count($child1),
                'child'       => $child1,
                ];
            }
            $tipe_coa[] =[
            'table' => 'tipe',
            'tipe_coa_id' => $tipe->id,
            'kode'        => $tipe->kode,
            'nama'        => $tipe->nama,
            'count'       => count($temp_menu),
            'child'       => $temp_menu,
            ];
        }

        $data['coa'] = $tipe_coa;

        // dd($data['coa']);

        return $this->render('modules.utama.finance.coa.index', $data);
    }

    // public function index()
    // {

    //     $array_menu = Coa::with('coaParent')->where('parent_kode' , NULL)->get();

    //     $temp_menu = [];


    //     foreach ($array_menu as $coa) {
    //         // dd($coa);

    //         $child1 =[];
    //         foreach ($coa->coaParent as $coa2) {
    //             // dd($coa2);

    //             $child2 =[];
    //             foreach($coa2->coaParent as $coa3){
    //                 // dd($coa3);
    //                 $child2[] = [
    //                     'table' => 'coa3',
    //                     'parent_kode' => $coa3->parent_kode,
    //                     'kode'      => $coa3->kode,
    //                     'nama'      => $coa3->nama,
    //                 ];
    //             }
    //             $child1[] =[
    //                 'table' => 'coa2',
    //                 'parent_kode' => $coa2->parent_kode,
    //                 'kode'      => $coa2->kode,
    //                 'nama'      => $coa2->nama,
    //                 'count'       => count($child2),
    //                 'child'     => $child2,
    //             ];
    //         }
    //         $temp_menu[] =[
    //             'table' => 'coa1',
    //             'tipe_coa_id' => $coa->tipe_coa_id,
    //             'kode'        => $coa->kode,
    //             'nama'        => $coa->nama,
    //             'count'       => count($child1),
    //             'child'       => $child1,
    //          ];
    //     }
    //     // dd($temp_menu);

    //     $data['coa'] = $temp_menu;

    //     return $this->render('modules.utama.finance.coa.index', $data);
    // }

    public function create($type, $parent_kode='')
    {

        // dd($tipe_coa_id);
        $parent = Coa::where('kode', $parent_kode)->first();

        $data = [];

        if($parent)
        {
            $data['parent_kode'] = (string)$parent_kode;
            $data['parent_nama'] = (string)$parent->nama;
        }

        switch ($type) {
            // case 1:
            //     return $this->render('modules.utama.finance.coa.form.tipe', $data);
            //     break;
            case 1:
            return $this->render('modules.utama.finance.coa.form.parent', $data);
            break;
            case 2:
            return $this->render('modules.utama.finance.coa.form.child', $data);
            break;
            case 3:
            return $this->render('modules.utama.finance.coa.form.divisi', $data);
            break;
            case 4:
            return $this->render('modules.utama.finance.coa.form.coak', $data);
            break;
            case 5:
            return $this->render('modules.utama.finance.coa.form.tipe', $data);
            break;
        }

    }

    public function edit($type,$id, $parent_kode='', $parent='')
    {
        $data = Coa::find($id);
        if($data->parent)
        {
            $data['parent_kode'] = (string)$data->parent_kode;
            $data['parent_nama'] = (string)$data->parent->nama;
        }

        return $this->render('modules.utama.finance.coa.form.'.$type, ['record' => $data]);
    }

    public function grid(Request $request){
        $table = $this->array_table;
        $type = explode("_", $request->type);
        switch ($type[0]) {
            case 'tipe':
            $data =[
            'pageUrl'     => $this->link,
            'tableStruct' => $data = $table['struct_tipe'],
            'kode'        => $request->kode,
            'tipe_id'     => $request->tipe_id,
            'type_form'   => 'tipe'
            ];

                    // dd($data['tableStruct']);
            return view('modules.utama.finance.coa.lists.tipe', $data);
            break;
            case 'parent':
            $data =[
            'pageUrl'     => $this->link,
            'tableStruct' => $data = $table['struct_coa1'],
            'kode'        => $request->kode,
            'type_form'   => 'parent'
            ];

                     // dd($data);
            return view('modules.utama.finance.coa.lists.parent', $data);
            break;
            case 'child':
            $data =[
            'pageUrl'     => $this->link,
            'tableStruct' => $data = $table['struct_coa2'],
            'kode'        => $request->kode,
            'type_form'   => 'child'
            ];
            return view('modules.utama.finance.coa.lists.child', $data);
            break;
            case 'coak':
            $data =[
            'pageUrl'     => $this->link,
            'tableStruct' => $data = $table['struct_type_coa'],
            'kode'        => $request->kode,
            'type_form'   => 'coak'
            ];
            return view('modules.utama.finance.coa.lists.coak', $data);
            break;
            default:
            $data =[
            'pageUrl'     => $this->link,
            'tableStruct' => $data = $table['struct_coa3'],
            'kode'        => $request->kode,
            'type_form'   => 'divisi'
            ];
            return view('modules.utama.finance.coa.lists.divisi', $data);
            break;
        }
    }
    public function store(Request $request)
    {
        // dd($request->all());
        if($request->id){
            $coa = Coa::find($request->id);
        }else{
            $coa = new Coa;
        }
        $coa->fill($request->all());
        $coa->save();


        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data COA',
            'ref'               => '',
            'aksi'              => 'Tambah Data COA',
        ]);

        return response([
            'status' => true,
            'data'  => $coa
            ]);
    }

    public function destroy($id)
    {
        $coa = Coa::find($id);
        $coa->delete();

        return response([
            'status' => true,
            ]);
    }
}