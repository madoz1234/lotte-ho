<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

class AkunGrupController extends Controller
{
	protected $link = 'utama/finance/akun-grup/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Akun Grup GL");
		$this->setSubtitle("Data Akun Grup GL Finance");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Akun Grup' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'grup_id',
			    'name' => 'grup_id',
			    'label' => 'Group ID',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'nama_grup',
			    'name' => 'nama_grup',
			    'label' => 'Nama Grup',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'sub_grup',
			    'name' => 'sub_grup',
			    'label' => 'Sub_Grup',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'tipe',
			    'name' => 'tipe',
			    'label' => 'Tipe',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.akun-grup.index');
    }

    public function create()
    {
        return $this->render('modules.utama.finance.akun-grup.create');
    }

    public function edit($id)
    {
        return $this->render('modules.utama.finance.akun-grup.edit');
    }
}
