<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Finance\RekeningEscrowRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\RekeningEscrow;
use Lotte\Models\Master\Log\LogRekeningBank;
use Lotte\Models\Trans\TransLogAudit;

class RekeningEscrowController extends Controller
{
	protected $link = 'utama/finance/rekening-escrow/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Rekening Bank");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Menu Utama' => '#','Master Data Finance' => '#', 'Rekening Bank' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'bank_escrow_id',
			    'name' => 'bank_escrow_id',
			    'label' => 'Bank Bank',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nama_pemilik',
			    'name' => 'nama_pemilik',
			    'label' => 'Nama Pemilik Rekening',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'nomor_rekening',
			    'name' => 'nomor_rekening',
			    'label' => 'Nomor Rekening',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			// [
			//     'data' => 'akun_gl',
			//     'name' => 'akun_gl',
			//     'label' => 'Akun Jurnal',
			//     'searchable' => false,
			//     'sortable' => false,
			//     'className' => "center aligned",
			// ],
			[
			    'data' => 'status',
			    'name' => 'status',
			    'label' => 'Status',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = RekeningEscrow::with('creator')
						 ->select('*');
        // $record = RekeningEscrow::select('*');
        // dd($record);

		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($nama_pemilik = $request->nama_pemilik) {
            $records->where('nama_pemilik', 'ilike', '%' . $nama_pemilik . '%');
        }
        if ($nomor_rekening = $request->nomor_rekening) {
            $records->where('nomor_rekening', 'like', '%' . $nomor_rekening . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('bank_escrow_id',function($record){
				return $record->bankescrow->nama;
			})
			->addColumn('status',function($record){
				if($record->status == 1){
					return 'Aktif';
				}else if($record->status == 0){
					return 'Non-Aktif';
				}
			})
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.rekening-escrow.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.finance.rekening-escrow.create');
    }

    public function store(RekeningEscrowRequest $request)
    {
    	// dd($request->all());
    	$rekeningescrow = new RekeningEscrow;
    	$rekeningescrow->fill($request->all());
    	$rekeningescrow->save();
        // dd($rekeningescrow);
        //log insert
        $log = new LogRekeningBank;
        $log->bank_escrow_id = $rekeningescrow->id;
        $log->rekening_escrow_id = $rekeningescrow->id;
        $log->nama_pemilik = $request->nama_pemilik;
        $log->nomor_rekening = $request->nomor_rekening;
        $log->status = $request->status;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Rekening',
            'ref'               => '',
            'aksi'              => 'Tambah Data Rekening',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $rekeningescrow
    	]);
    }

    public function edit($id)
    {
    	$record = RekeningEscrow::find($id);
        return $this->render('modules.utama.finance.rekening-escrow.edit', ['record' => $record]);
    }

    public function update(RekeningEscrowRequest $request, $id)
    {
    	$rekeningescrow = RekeningEscrow::find($id);
    	$rekeningescrow->fill($request->all());
    	$rekeningescrow->save();

    	//log upate
        $log = new LogRekeningBank;
        $log->bank_escrow_id = $rekeningescrow->id;
        $log->rekening_escrow_id = $rekeningescrow->id;
        $log->nama_pemilik = $rekeningescrow->nama_pemilik;
        $log->nomor_rekening = $rekeningescrow->nomor_rekening;
        $log->status = $rekeningescrow->status;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Rekening',
            'ref'               => '',
            'aksi'              => 'Ubah Data Rekening',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $rekeningescrow
    	]);
    }

    public function destroy($id)
    {
    	$rekeningescrow = RekeningEscrow::find($id);
    	$rekeningescrow->delete();

    	//log delete
        $log = new LogRekeningBank;
        $log->bank_escrow_id = $rekeningescrow->id;
        $log->rekening_escrow_id = $rekeningescrow->id;
        $log->nama_pemilik = $rekeningescrow->nama_pemilik;
        $log->nomor_rekening = $rekeningescrow->nomor_rekening;
        $log->status = $rekeningescrow->status;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Rekening',
            'ref'               => '',
            'aksi'              => 'Hapus Data Rekening',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
}
