<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Finance\TipeAsetRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\TipeAset;
use Lotte\Models\Master\Log\LogTipeAset;
use Lotte\Models\Trans\TransLogAudit;

class TipeAsetController extends Controller
{
	protected $link = 'utama/finance/tipe-aset/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Tipe Aset Terdepresiasi");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#','Master Data Finance' => '#', 'Tipe Aset Terdepresiasi' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '10px',
			],
			/* --------------------------- */
			[
			    'data' => 'tipe',
			    'name' => 'tipe',
			    'label' => 'Tipe Asset',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '80px',
			],
			[
			    'data' => 'tingkat_depresiasi',
			    'name' => 'tingkat_depresiasi',
			    'label' => 'Tingkat Depresiasi (Bulan)',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '50px',
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '20px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '10px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TipeAset::with('creator')
						 ->select('*');

        //Filters
        if ($tipe = $request->tipe) {
            $records->where('tipe', 'ilike', '%' . $tipe . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.tipe-aset.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.finance.tipe-aset.create');
    }

    public function store(TipeAsetRequest $request)
    {
    	$tipeaset = new TipeAset;
    	$tipeaset->fill($request->all());
    	$tipeaset->save();

        //log insert
        $log = new LogTipeAset;
        $log->tipe_aset_id = $tipeaset->id;
        $log->tipe = $request->tipe;
        $log->tingkat_depresiasi = $request->tingkat_depresiasi;
        $log->status = $request->status;

        $log->save();

        // insert to log audit
                TransLogAudit::setLog([
                    'tanggal_transaksi' => date('Y-m-d'),
                    'type'              => 'Master Data Tipe Asset',
                    'ref'               => '',
                    'aksi'              => 'Tambah Data Tipe Asset',
                ]);

    	return response([
    		'status' => true,
    		'data'	=> $tipeaset
    	]);
    }

    public function edit($id)
    {
    	$record = TipeAset::find($id);

        return $this->render('modules.utama.finance.tipe-aset.edit', ['record' => $record]);
    }

    public function update(TipeAsetRequest $request, $id)
    {
    	$tipeaset = TipeAset::find($id);
    	$tipeaset->fill($request->all());
    	$tipeaset->save();

        //log update
        $log = new LogTipeAset;
        $log->tipe_aset_id = $tipeaset->id;
        $log->tipe = $tipeaset->tipe;
        $log->tingkat_depresiasi = $tipeaset->tingkat_depresiasi;
        $log->status = $tipeaset->status;

        $log->save();

        // insert to log audit
                TransLogAudit::setLog([
                    'tanggal_transaksi' => date('Y-m-d'),
                    'type'              => 'Master Data Tipe Asset',
                    'ref'               => '',
                    'aksi'              => 'Ubah Data Tipe Asset',
                ]);

    	return response([
    		'status' => true,
    		'data'	=> $tipeaset
    	]);
    }

    public function destroy($id)
    {
    	$tipeaset = TipeAset::find($id);
    	$tipeaset->delete();

        //log delete
        $log = new LogTipeAset;
        $log->tipe_aset_id = $tipeaset->id;
        $log->tipe = $tipeaset->tipe;
        $log->tingkat_depresiasi = $tipeaset->tingkat_depresiasi;
        $log->status = $tipeaset->status;

        $log->save();

        // insert to log audit
                TransLogAudit::setLog([
                    'tanggal_transaksi' => date('Y-m-d'),
                    'type'              => 'Master Data Tipe Asset',
                    'ref'               => '',
                    'aksi'              => 'Hapus Data Tipe Asset',
                ]);

    	return response([
    		'status' => true,
    	]);
    }
}
