<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Finance\SaldoMinimalRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Helpers\Untilities;

//Models
use Lotte\Models\Master\SaldoMinimal;
use Lotte\Models\Master\Log\LogSaldoMengendap;
use Lotte\Models\Trans\TransLogAudit;

class SaldoMinimalController extends Controller
{
	protected $link = 'utama/finance/saldo-minimal/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Saldo Minimal Mengendap");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Saldo Minimal Mengendap' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
                'data' => 'tahun_fiskal_id',
                'name' => 'tahun_fiskal_id',
                'label' => 'Tahun Fiskal',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
            [
			    'data' => 'saldo_minimal_mengendap',
			    'name' => 'saldo_minimal_mengendap',
			    'label' => 'Saldo Minimal Mengendap (Rp)',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = SaldoMinimal::with('creator')
						 ->select('*');
        // $record = Pajak::select('*');
        // dd($record);

		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        // if ($npwp = $request->npwp) {
        //     $records->where('npwp', 'like', '%' . $npwp . '%');
        // }
        // if ($nama = $request->nama) {
        //     $records->where('nama', 'like', '%' . $nama . '%');
        // }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tahun_fiskal_id',function($record){
                return $record->tahunfiskal->tgl_awal;
            })
            ->addColumn('status',function($record){
                if($record->tahunfiskal->status == 1){
                    return 'Berjalan';
                }else if($record->tahunfiskal->status == 0){
                    return 'Selesai';
                }
            })
            ->addColumn('saldo_minimal_mengendap',function($record){
                return rupiah($record->saldo_minimal_mengendap);
            })
            // ->addColumn('tinggi_rak', function ($record) {
            //     return $record->panjang_rak. 'cm'. '&nbsp;X&nbsp;' .$record->lebar_rak.'cm'. '&nbsp;X&nbsp;' .$record->tinggi_rak. 'cm';
            // })

            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.saldo-minimal.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.finance.saldo-minimal.create');
    }

    public function store(SaldoMinimalRequest $request)
    {
    	$saldo = new SaldoMinimal;
    	$saldo->fill($request->all());
    	$saldo->save();

        //log insert
        $log = new LogSaldoMengendap;
        $log->saldo_minimal_mengendap_id = $saldo->id;
        $log->saldo_minimal_mengendap = $request->saldo_minimal_mengendap;
        $log->tahun_fiskal_id = $request->tahun_fiskal_id;

        $log->save();


        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Saldo Minimal Mengendap',
            'ref'               => '',
            'aksi'              => 'Tambah Data Saldo Minimal Mengendap',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $saldo
    	]);
    }

    public function edit($id)
    {
    	$record = SaldoMinimal::find($id);

        return $this->render('modules.utama.finance.saldo-minimal.edit', ['record' => $record]);
    }

    public function update(SaldoMinimalRequest $request, $id)
    {
    	$saldo = SaldoMinimal::find($id);
    	$saldo->fill($request->all());
    	$saldo->save();

        //log update
        $log = new LogSaldoMengendap;
        $log->saldo_minimal_mengendap_id = $id;
        $log->saldo_minimal_mengendap = $saldo->saldo_minimal_mengendap;
        $log->tahun_fiskal_id = $saldo->tahun_fiskal_id;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Saldo Minimal Mengendap',
            'ref'               => '',
            'aksi'              => 'Ubah Data Saldo Minimal Mengendap',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $saldo
    	]);
    }

    public function destroy($id)
    {
    	$saldo = SaldoMinimal::find($id);
    	$saldo->delete();

        // log delete
        $log = new LogSaldoMengendap;
        $log->saldo_minimal_mengendap_id = $id;
        $log->saldo_minimal_mengendap = $saldo->saldo_minimal_mengendap;
        $log->tahun_fiskal_id = $saldo->tahun_fiskal_id;
        
        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Saldo Minimal Mengendap',
            'ref'               => '',
            'aksi'              => 'Hapus Data Saldo Minimal Mengendap',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
}
