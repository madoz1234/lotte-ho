<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

class GrupPajakController extends Controller
{
	protected $link = 'utama/finance/grup-pajak/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Grup Pajak");
		$this->setSubtitle("Data Grup Pajak Finance");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Grup Pajak' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'deskripsi',
			    'name' => 'deskripsi',
			    'label' => 'Deskripsi',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'pajak_pengiriman',
			    'name' => 'pajak_pengiriman',
			    'label' => 'Pajak Pengiriman',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'tipe_pajak',
			    'name' => 'tipe_pajak',
			    'label' => 'Tipe Pajak',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Dibuat Oleh',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.grup-pajak.index');
    }

    public function create()
    {
        return $this->render('modules.utama.finance.grup-pajak.create');
    }

    public function edit($id)
    {
        return $this->render('modules.utama.finance.grup-pajak.edit');
    }
}
