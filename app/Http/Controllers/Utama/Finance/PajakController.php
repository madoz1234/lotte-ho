<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Finance\PajakRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\Pajak;
use Lotte\Models\Master\Log\LogPajak;
use Lotte\Models\Trans\TransLogAudit;

class PajakController extends Controller
{
	protected $link = 'utama/finance/pajak/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Pajak");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Pajak' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'npwp',
			    'name' => 'npwp',
			    'label' => 'NPWP',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
            [
                'data' => 'alamat_npwp',
                'name' => 'alamat_npwp',
                'label' => 'Alamat',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                'width' => '150px',
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Pajak::with('creator')
						 ->select('*');
        // $record = Pajak::select('*');
        // dd($record);

		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($npwp = $request->npwp) {
            $records->where('npwp', 'like', '%' . $npwp . '%');
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('alamat_npwp',function($record){
                if($record->alamat_npwp == null){
                    return '-';
                }else{
                    return $record->alamat_npwp;
                }
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.pajak.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.finance.pajak.create');
    }

    public function store(PajakRequest $request)
    {
    	$pajak = new Pajak;
    	$pajak->fill($request->all());
    	$pajak->save();

        //log insert
        $log = new LogPajak;
        $log->pajak_id = $pajak->id;
        $log->npwp = $request->npwp;
        $log->nama = $request->nama;
        $log->alamat_npwp = $request->alamat_npwp;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Pajak',
            'ref'               => '',
            'aksi'              => 'Tambah Data Pajak',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $pajak
    	]);
    }

    public function edit($id)
    {
    	$record = Pajak::find($id);

        return $this->render('modules.utama.finance.pajak.edit', ['record' => $record]);
    }

    public function update(PajakRequest $request, $id)
    {
    	$pajak = Pajak::find($id);
    	$pajak->fill($request->all());
    	$pajak->save();

        //log update
        $log = new LogPajak;
        $log->pajak_id = $id;
        $log->npwp = $pajak->npwp;
        $log->nama = $pajak->nama;
        $log->alamat_npwp = $pajak->alamat_npwp;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Pajak',
            'ref'               => '',
            'aksi'              => 'Ubah Data Pajak',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $pajak
    	]);
    }

    public function destroy($id)
    {
    	$pajak = Pajak::find($id);
    	$pajak->delete();

        //log delete
        $log = new LogPajak;
        $log->pajak_id = $id;
        $log->npwp = $pajak->npwp;
        $log->nama = $pajak->nama;
        $log->alamat_npwp = $pajak->alamat_npwp;
        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Pajak',
            'ref'               => '',
            'aksi'              => 'Hapus Data Pajak',
        ]);

    	return response([
    		'status' => true,
    	]);
    }
}
