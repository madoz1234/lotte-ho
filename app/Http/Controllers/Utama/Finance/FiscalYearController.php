<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Finance\TahunFiskalRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;
use Session;

//Models
use Lotte\Models\Master\TahunFiskal;
use Lotte\Models\Master\Log\LogTahunFiskal;
use Lotte\Models\Trans\TransLogAudit;

class FiscalYearController extends Controller
{
	protected $link = 'utama/finance/fiscal-year/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Tahun Fiskal");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Tahun Fiskal' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'tgl_awal',
			    'name' => 'tgl_awal',
			    'label' => 'Mulai Tahun Fiskal',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'tgl_akhir',
			    'name' => 'tgl_akhir',
			    'label' => 'Selesai Tahun Fiskal',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'status',
			    'name' => 'status',
			    'label' => 'Status',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TahunFiskal::with('creator')
						 ->select('*');
        // $record = TahunFiskal::select('*');
        // dd($record);

		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if($tgl_awal = $request->tgl_awal){
            $date = \Carbon\Carbon::parse($tgl_awal)->format('Y-m-d'); 
            $records->where('tgl_awal', $date);
        }
        
        if($tgl_akhir = $request->tgl_akhir){
            $date = \Carbon\Carbon::parse($tgl_akhir)->format('Y-m-d'); 
            $records->where('tgl_akhir', $date);
        }

        // if ($tgl_awal = $request->tgl_awal) {
        //     $records->where('tgl_awal', 'like', '%' . $tgl_awal . '%');
        // }
        // if ($tgl_akhir = $request->tgl_akhir) {
        //     $records->where('tgl_akhir', 'like', '%' . $tgl_akhir . '%');
        // }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
			->addColumn('status',function($record){
				if($record->status == 1){
					return 'Berjalan';
				}else if($record->status == 0){
					return 'Selesai';
				}
			})
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.fiscal-year.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.finance.fiscal-year.create');
    }

    public function store(TahunFiskalRequest $request)
    {
    	// dd($request->all());
        $tahunfiskal = $request->all(); 

        if($tahunfiskal['status'] == 1){
            $check = TahunFiskal::where('status', 1)->first();
            // dd($check);
            if (!is_null($check)) {
                return response([
                    'error' => ['Hanya diperbolehkan 1 Tahun Fiskal yang Berjalan di satu waktu'],

                ], 422);
            } else {
                // return TahunFiskal::where(['status' => 1])->update($tahunfiskal);
                // dd($tahunfiskal);
                $tahunfiskal = new TahunFiskal;
                $tahunfiskal->fill($request->all());
                $tahunfiskal->save();
                

                //log insert
                $log = new LogTahunFiskal;
                $log->tahun_fiskal_id = $tahunfiskal->id;
                $log->tgl_awal = $request->tgl_awal;
                $log->tgl_akhir = $request->tgl_akhir;
                $log->status = $request->status;

                $log->save();

                

                return response([
                    'status' => true,
                    'data'  => $tahunfiskal
                ]);
            }
        }else{
            // return TahunFiskal::where(['status' => 1])->update($tahunfiskal);
                // dd($tahunfiskal);
                $tahunfiskal = new TahunFiskal;
                $tahunfiskal->fill($request->all());
                $tahunfiskal->save();
                

                //log insert
                $log = new LogTahunFiskal;
                $log->tahun_fiskal_id = $tahunfiskal->id;
                $log->tgl_awal = $request->tgl_awal;
                $log->tgl_akhir = $request->tgl_akhir;
                $log->status = $request->status;

                $log->save();

                // insert to log audit
                TransLogAudit::setLog([
                    'tanggal_transaksi' => date('Y-m-d'),
                    'type'              => 'Master Data Tahun Fiskal',
                    'ref'               => '',
                    'aksi'              => 'Tambah Data Tahun Fiskal',
                ]);

                return response([
                    'status' => true,
                    'data'  => $tahunfiskal
                ]);
        }


    }

    public function edit($id)
    {
    	$record = TahunFiskal::find($id);
        return $this->render('modules.utama.finance.fiscal-year.edit', ['record' => $record]);
    }

    public function update(TahunFiskalRequest $request, $id)
    {
        $data = $request->all();
    	$tahunfiskal = TahunFiskal::find($id);

        if (!is_null($tahunfiskal)) {
            $tahunfiskal->tgl_awal = $data['tgl_awal'];
            $tahunfiskal->tgl_akhir = $data['tgl_akhir'];
            if ($tahunfiskal['status'] != 0) {
                $tahunfiskal->status = $data['status'];
            }
        	$tahunfiskal->save();
        	//log insert
            $log = new LogTahunFiskal;
            $log->tahun_fiskal_id = $tahunfiskal->id;
            $log->tgl_awal = $tahunfiskal->tgl_awal;
            $log->tgl_akhir = $tahunfiskal->tgl_akhir;
            $log->status = $tahunfiskal->status;

            $log->save();

            // insert to log audit
                TransLogAudit::setLog([
                    'tanggal_transaksi' => date('Y-m-d'),
                    'type'              => 'Master Data Tahun Fiskal',
                    'ref'               => '',
                    'aksi'              => 'Ubah Data Tahun Fiskal',
                ]);

        	return response([
        		'status' => true,
        		'data'	=> $tahunfiskal
        	]);
        }
    }

    public function destroy($id)
    {
    	$tahunfiskal = TahunFiskal::find($id);
    	$tahunfiskal->delete();

    	//log insert
        $log = new LogTahunFiskal;
        $log->tahun_fiskal_id = $tahunfiskal->id;
        $log->tgl_awal = $tahunfiskal->tgl_awal;
        $log->tgl_akhir = $tahunfiskal->tgl_akhir;
        $log->status = $tahunfiskal->status;

        $log->save();

        // insert to log audit
                TransLogAudit::setLog([
                    'tanggal_transaksi' => date('Y-m-d'),
                    'type'              => 'Master Data Tahun Fiskal',
                    'ref'               => '',
                    'aksi'              => 'Hapus Data Tahun Fiskal',
                ]);

    	return response([
    		'status' => true,
    	]);
    }
}
