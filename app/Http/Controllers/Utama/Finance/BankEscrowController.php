<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Requests\Utama\Finance\BankEscrowRequest;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\BankEscrow;
use Lotte\Models\Master\Log\LogBank;
use Lotte\Models\Trans\TransLogAudit;

class BankEscrowController extends Controller
{
	protected $link = 'utama/finance/bank-escrow/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Bank");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Bank' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
            [
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama Bank',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
            [
                'data' => 'kode_swift',
                'name' => 'kode_swift',
                'label' => 'Kode Swift',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'kode_bank',
                'name' => 'kode_bank',
                'label' => 'Kode Bank',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
			[
			    'data' => 'alamat',
			    'name' => 'alamat',
			    'label' => 'Alamat',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'tgl_mulai',
			    'name' => 'tgl_mulai',
			    'label' => 'Tanggal Mulai Kerjasama',
			    'searchable' => false,
			    'sortable' => true,
			    'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = BankEscrow::with('creator')
						 ->select('*');
        // $record = BankEscrow::select('*');
        // dd($record);

		//Init Sort
        // if (!isset(request()->order[0]['column'])) {
        //     // $records->->sort();
        //     $records->orderBy('kode');
        // }

        //Filters
        if ($npwp = $request->npwp) {
            $records->where('npwp', 'like', '%' . $npwp . '%');
        }
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.bank-escrow.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('modules.utama.finance.bank-escrow.create');
    }

    public function store(BankEscrowRequest $request)
    {
    	$bankescrow = new BankEscrow;
    	$bankescrow->fill($request->all());
    	$bankescrow->save();

        //log insert
        $log = new LogBank;
        $log->kota_id = $bankescrow->id;
        $log->bank_escrow_id = $bankescrow->id;
        $log->nama = $request->nama;
        $log->alamat = $request->alamat;
        $log->kode_pos = $request->kode_pos;
        $log->telepon = $request->telepon;
        $log->email = $request->email;
        $log->tgl_mulai = $request->tgl_mulai;
        $log->kode_bank = $request->kode_bank;
        $log->kode_swift = $request->kode_swift;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Bank',
            'ref'               => '',
            'aksi'              => 'Tambah Data Bank',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $bankescrow
    	]);
    }

    public function edit($id)
    {
    	$record = BankEscrow::find($id);
        return $this->render('modules.utama.finance.bank-escrow.edit', ['record' => $record]);
    }

    public function update(BankEscrowRequest $request, $id)
    {
    	$bankescrow = BankEscrow::find($id);
    	$bankescrow->fill($request->all());
    	$bankescrow->save();

        //log update
        $log = new LogBank;
        $log->kota_id = $bankescrow->id;
        $log->bank_escrow_id = $bankescrow->id;
        $log->nama = $bankescrow->nama;
        $log->alamat = $bankescrow->alamat;
        $log->kode_pos = $bankescrow->kode_pos;
        $log->telepon = $bankescrow->telepon;
        $log->email = $bankescrow->email;
        $log->tgl_mulai = $bankescrow->tgl_mulai;
        $log->kode_bank = $bankescrow->kode_bank;
        $log->kode_swift = $bankescrow->kode_swift;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Bank',
            'ref'               => '',
            'aksi'              => 'Ubah Data Bank',
        ]);

    	return response([
    		'status' => true,
    		'data'	=> $bankescrow
    	]);
    }

    public function destroy($id)
    {
    	$bankescrow = BankEscrow::find($id);
    	$bankescrow->delete();

        //log delete
        $log = new LogBank;
        $log->kota_id = $bankescrow->id;
        $log->bank_escrow_id = $bankescrow->id;
        $log->nama = $bankescrow->nama;
        $log->alamat = $bankescrow->alamat;
        $log->kode_pos = $bankescrow->kode_pos;
        $log->telepon = $bankescrow->telepon;
        $log->email = $bankescrow->email;
        $log->tgl_mulai = $bankescrow->tgl_mulai;
        $log->kode_bank = $bankescrow->kode_bank;
        $log->kode_swift = $bankescrow->kode_swift;

        $log->save();

        // insert to log audit
        TransLogAudit::setLog([
            'tanggal_transaksi' => date('Y-m-d'),
            'type'              => 'Master Data Bank',
            'ref'               => '',
            'aksi'              => 'Hapus Data Bank',
        ]);


    	return response([
    		'status' => true,
    	]);
    }
}
