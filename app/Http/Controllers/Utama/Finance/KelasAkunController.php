<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

class KelasAkunController extends Controller
{
	protected $link = 'utama/finance/kelas-akun/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Kelas Akun");
		$this->setSubtitle("Data Kelas Akun Finance");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Kelas Akun' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'kelas_id',
			    'name' => 'kelas_id',
			    'label' => 'Kelas ID',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'nama_kelas',
			    'name' => 'nama_kelas',
			    'label' => 'Nama Kelas',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'kelas_tipe',
			    'name' => 'kelas_tipe',
			    'label' => 'Kelas Tipe',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.kelas-akun.index');
    }

    public function create()
    {
        return $this->render('modules.utama.finance.kelas-akun.create');
    }

    public function edit($id)
    {
        return $this->render('modules.utama.finance.kelas-akun.edit');
    }
}
