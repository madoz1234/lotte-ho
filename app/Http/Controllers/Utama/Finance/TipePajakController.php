<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

class TipePajakController extends Controller
{
	protected $link = 'utama/finance/tipe-pajak/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Tipe Pajak");
		$this->setSubtitle("Data Tipe Pajak Finance");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Tipe Pajak' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'bebas_pajak',
			    'name' => 'bebas_pajak',
			    'label' => 'Bebas Pajak',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Dibuat Pada',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Dibuat Oleh',
			    'searchable' => false,
			    'sortable' => false,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

    public function index()
    {
        return $this->render('modules.utama.finance.tipe-pajak.index');
    }

    public function create()
    {
        return $this->render('modules.utama.finance.tipe-pajak.create');
    }

    public function edit($id)
    {
        return $this->render('modules.utama.finance.tipe-pajak.edit');
    }
}
