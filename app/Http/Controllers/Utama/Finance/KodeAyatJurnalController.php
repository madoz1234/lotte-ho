<?php

namespace Lotte\Http\Controllers\Utama\Finance;

use Illuminate\Http\Request;

use Lotte\Http\Requests;
use Lotte\Http\Controllers\Controller;

//Libraries
use Datatables;
use Carbon\Carbon;

//Models
use Lotte\Models\Master\KodeAyatJurnal;
use Lotte\Models\Master\TipeCoa;
use Lotte\Models\Master\Coa;

class KodeAyatJurnalController extends Controller
{
	protected $link = 'utama/finance/kode-ayat-jurnal/';

	function __construct()
	{
		$this->setLink($this->link);
		$this->setTitle("Kode Ayat Jurnal");
		$this->setSubtitle("&nbsp;");
		$this->setModalSize("fullscreen");
		$this->setBreadcrumb(['Menu Utama' => '#', 'Master Data Finance' => '#', 'Kode Ayat Jurnal' => '#']);
	}


    // public function index()
    // {
    //     $data   = TipeCoa::orderBy('id')->get();
    //     $record = Coa::with('creator')
    //                      ->select('*');


    //     return $this->render('modules.utama.finance.kode-ayat-jurnal.index', ['data' => $data, 'record' => $record]);
    // }

    public function index($id=1)
    {
        $record = KodeAyatJurnal::find($id);


        return $this->render('modules.utama.finance.kode-ayat-jurnal.index', ['record' => $record]);
    }

    // public function update(Request $request, $id)
    // {
    //     $kodeayat = KodeAyatJurnal::find($id);
    //     $kodeayat->fill($request->all());
    //     $kodeayat->save();

    //     return response([
    //         'status' => true,
    //         'data'  => $kodeayat
    //     ]);
    // }
}
