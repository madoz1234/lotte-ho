<?php

namespace Lotte\Http\Controllers;

class HomeController extends Controller
{
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}

	public function getGrid()
	{
		return view('grid');
	}
	
}