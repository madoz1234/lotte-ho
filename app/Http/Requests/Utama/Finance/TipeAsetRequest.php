<?php

namespace Lotte\Http\Requests\Utama\Finance;

use Lotte\Http\Requests\Request;

class TipeAsetRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'tipe' => 'required|min:5|max:30|unique:ref_tipe_aset,tipe'.$ignore,
            'tingkat_depresiasi' => 'required|min:1|max:5'.$ignore,
            'status' => 'required|min:1|max:2'.$ignore,
        ];
    }
}