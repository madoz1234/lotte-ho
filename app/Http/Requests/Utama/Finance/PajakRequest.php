<?php

namespace Lotte\Http\Requests\Utama\Finance;

use Lotte\Http\Requests\Request;

class PajakRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'npwp' => 'required||max:15|unique:ref_pajak,npwp'.$ignore,
            // 'nama' => 'required|min:3|max:50|unique:ref_bank,nama'.$ignore,
            'nama' => 'required|max:30'.$ignore,
            // 'alamat_npwp' => 'required|max:200'.$ignore,
        ];
    }
}
