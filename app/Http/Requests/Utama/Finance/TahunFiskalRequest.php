<?php

namespace Lotte\Http\Requests\Utama\Finance;

use Lotte\Http\Requests\Request;

class TahunFiskalRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'tgl_awal' => 'required|unique:ref_tahun_fiskal,tgl_awal'.$ignore,
            'tgl_akhir' => 'required',
            'status' => 'required',
        ];
    }
}
