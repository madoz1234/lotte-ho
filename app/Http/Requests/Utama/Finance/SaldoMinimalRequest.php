<?php

namespace Lotte\Http\Requests\Utama\Finance;

use Lotte\Http\Requests\Request;

class SaldoMinimalRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'saldo_minimal_mengendap' => 'required|max:15'.$ignore,
            'tahun_fiskal_id' => 'required',
            'tahun_fiskal_id' => 'required|unique:ref_saldo_minimal_mengendap,tahun_fiskal_id'.$ignore,
        ];
    }
}
