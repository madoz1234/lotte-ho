<?php

namespace Lotte\Http\Requests\Utama\Finance;

use Lotte\Http\Requests\Request;

class PointRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'tgl_berlaku' => 'required|min:2|max:20'.$ignore,
            'konversi' => 'required|max:20'.$ignore,
            'status' => 'required',
            'faktor_konversi' => 'required',
            'faktor_reedem' => 'required',
        ];
    }
}
