<?php

namespace Lotte\Http\Requests\Utama\Finance;

use Lotte\Http\Requests\Request;

class RekeningEscrowRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'bank_escrow_id' => 'required|max:3'.$ignore,
            'nama_pemilik' => 'required|min:2|max:50'.$ignore,
            'nomor_rekening' => 'required|min:5|max:20|unique:ref_rekening_bank,nomor_rekening'.$ignore,
            'status' => 'required|max:2'.$ignore,
        ];
    }
}
