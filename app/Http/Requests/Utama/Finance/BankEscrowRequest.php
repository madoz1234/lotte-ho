<?php

namespace Lotte\Http\Requests\Utama\Finance;

use Lotte\Http\Requests\Request;

class BankEscrowRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'kota_id' => 'required|max:3'.$ignore,
            'kode_bank' => 'required|min:3|max:3'.$ignore,
            'nama' => 'required|min:3|max:50|unique:ref_bank,nama'.$ignore,
            'alamat' => 'required|min:4|max:200'.$ignore,
            'provinsi_id' => 'required|max:3'.$ignore,
            'telepon' => 'required|min:3|max:15'.$ignore,
            // 'email' => 'required|min:5|max:50'.$ignore,
            'tgl_mulai' => 'required|min:5|max:20'.$ignore,
            'kode_swift' => 'required|min:3|max:8'.$ignore,
        ];
    }
}