<?php

namespace Lotte\Http\Requests\Utama\Finance;

use Lotte\Http\Requests\Request;

class CoaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'tipe_coa_id' => 'required|max:30'.$ignore,
            'kode' => 'required|min:2|max:30|unique:ref_coa,kode'.$ignore,
            'nama' => 'required|min:2|max:30'.$ignore,
            'parent_kode' => 'required|min:2|max:10'.$ignore,
        ];
    }
}