<?php

namespace Lotte\Http\Requests\Utama\Produk;

use Lotte\Http\Requests\Request;

class ProdukRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'kode' => 'required|max:30|min:5|unique:ref_produk,kode'.$ignore,
            'nama' => 'required|max:255|min:5|unique:ref_produk,nama'.$ignore,
        ];
    }
}
