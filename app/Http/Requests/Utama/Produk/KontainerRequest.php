<?php

namespace Lotte\Http\Requests\Utama\Produk;

use Lotte\Http\Requests\Request;

class KontainerRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'nama' => 'required|max:20|min:2|unique:ref_kontainer,nama'.$ignore,
            'tinggi_luar' => 'required|max:15',
            'tinggi_dalam' => 'required|max:15',
            'panjang_atas' => 'required|max:15',
            'panjang_bawah' => 'required|max:15',
            'lebar_atas' => 'required|max:15',
            'lebar_bawah' => 'required|max:15',
            'volume' => 'required|max:15',
            'tipe_box' => 'required',
        ];
    }
}
