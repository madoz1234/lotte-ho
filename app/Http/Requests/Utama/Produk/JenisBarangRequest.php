<?php

namespace Lotte\Http\Requests\Utama\Produk;

use Lotte\Http\Requests\Request;

class JenisBarangRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'kode' => 'required|unique:ref_jenis_barang,kode'.$ignore,
            'jenis' => 'required|unique:ref_jenis_barang,jenis'.$ignore,
        ];
    }
}
