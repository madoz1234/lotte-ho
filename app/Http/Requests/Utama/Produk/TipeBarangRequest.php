<?php

namespace Lotte\Http\Requests\Utama\Produk;

use Lotte\Http\Requests\Request;

class TipeBarangRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'nama' => 'required|max:30|min:2|unique:ref_tipebarang,nama'.$ignore,
        ];
    }
}
