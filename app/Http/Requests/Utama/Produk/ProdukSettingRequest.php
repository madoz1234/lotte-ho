<?php

namespace Lotte\Http\Requests\Utama\Produk;

use Lotte\Http\Requests\Request;

class ProdukSettingRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        return [
            'tipe_barang_kode' => 'required',
            'jenis_barang_kode' => 'required',
            // 'uom1_satuan' => 'required',
            // // 'uom1_rak_id' => 'required',
            // 'uom1_barcode' => 'required',
            // // 'uom1_conversion' => 'required',
            // 'uom1_width' => 'required',
            // 'uom1_height' => 'required',
            // 'uom1_length' => 'required',
            // 'uom1_weight' => 'required',
            // 'uom1_boxtype' => 'required',
            // 'uom1_file' => 'required',
            // 'uom2_satuan' => 'required',
            // // 'uom2_rak_id' => 'required',
            // 'uom2_barcode' => 'required',
            // 'uom2_conversion' => 'required',
            // 'uom2_width' => 'required',
            // 'uom2_height' => 'required',
            // 'uom2_length' => 'required',
            // 'uom2_weight' => 'required',
            // 'uom2_boxtype' => 'required',
            // 'uom2_file' => 'required',
            // 'uom3_satuan' => 'required',
            // 'uom3_rak_id' => 'required',
            // 'uom3_barcode' => 'required',
            // 'uom3_conversion' => 'required',
            // 'uom3_width' => 'required',
            // 'uom3_height' => 'required',
            // 'uom3_length' => 'required',
            // 'uom3_weight' => 'required',
            // 'uom3_boxtype' => 'required',
            // 'uom3_file' => 'required',
            // 'uom4_satuan' => 'required',
            // 'uom4_rak_id' => 'required',
            // 'uom4_barcode' => 'required',
            // 'uom4_conversion' => 'required',
            // 'uom4_width' => 'required',
            // 'uom4_height' => 'required',
            // 'uom4_length' => 'required',
            // 'uom4_weight' => 'required',
            // 'uom4_boxtype' => 'required',
            // 'uom4_file' => 'required',
        ];
    }
}
