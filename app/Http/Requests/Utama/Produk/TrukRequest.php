<?php

namespace Lotte\Http\Requests\Utama\Produk;

use Lotte\Http\Requests\Request;

class TrukRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'tipe' => 'required|max:30|min:2|unique:ref_truk,tipe'.$ignore,
            'tinggi' => 'required|max:15',
            'panjang' => 'required|max:15',
            'lebar' => 'required|max:15',
            'kapasitas' => 'required|max:15',
            'volume' => 'required|max:15',
        ];
    }
}
