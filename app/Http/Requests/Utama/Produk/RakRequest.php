<?php

namespace Lotte\Http\Requests\Utama\Produk;

use Lotte\Http\Requests\Request;

class RakRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'tipe_rak' => 'required|max:30|min:2|unique:ref_rak,tipe_rak'.$ignore,
            'shelving' => 'required',
            'tinggi_rak' => 'required',
            'panjang_rak' => 'required',
            'lebar_rak' => 'required',
        ];
    }
}