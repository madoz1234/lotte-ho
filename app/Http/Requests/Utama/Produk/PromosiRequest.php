<?php

namespace Lotte\Http\Requests\Utama\Produk;

use Lotte\Http\Requests\Request;

class PromosiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'region_id' => 'required',
            'lsi_id' => 'required',
            'tgl_awal' => 'required',
            'tgl_akhir' => 'required',
            'nama' => 'required',
            'kode_promo' => 'required|unique:ref_promosi,kode_promo'.$ignore,
            'tipe_promo' => 'required',
            'tmuk_id' => 'required',
        ];
    }
}
