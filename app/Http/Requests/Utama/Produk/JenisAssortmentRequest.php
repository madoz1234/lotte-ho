<?php

namespace Lotte\Http\Requests\Utama\Produk;

use Lotte\Http\Requests\Request;

class JenisAssortmentRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            // 'nama' => 'required',
            'nama' => 'required|min:2|unique:ref_jenis_assortment,nama|max:10'.$ignore,
        ];
    }
}
