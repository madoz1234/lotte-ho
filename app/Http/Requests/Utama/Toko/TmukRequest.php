<?php

namespace Lotte\Http\Requests\Utama\Toko;

use Lotte\Http\Requests\Request;

class TmukRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';
        return [
            'lsi_id' => 'required',
            'kode' => 'required|max:10|min:2|unique:ref_tmuk,kode'.$ignore,
            'nama' => 'required',
            'alamat' => 'required',
            'provinsi_id' => 'required',
            'kota_id' => 'required',
            'kecamatan_id' => 'required',
            // 'kode_pos' => 'required',
            'asn' => 'required',
            'auto_approve' => 'required',
            'longitude' => 'required|max:10|min:2',
            'latitude' => 'required|max:10|min:2',
            'membercard_id' => 'required',
            'nama_pemilik' => 'required',
            'jenis_assortment_id' => 'required',
            'gross_area' => 'required',
            'selling_area' => 'required',
            'nama_cde' => 'required',
            'email_cde' => 'required',
            'rencana_pembukaan' => 'required',
            // 'aktual_pembukaan' => 'required',
            'pajak_id' => 'required',
            // 'nama_perusahaan' => 'required',
            // 'alamat_perusahaan' => 'required',
            'rekening_escrow_id' => 'required|unique:ref_tmuk,rekening_escrow_id'.$ignore,
            'bank_escrow_id' => 'required',
            // 'nomor_rekening' => 'required',
        ];
    }
}
