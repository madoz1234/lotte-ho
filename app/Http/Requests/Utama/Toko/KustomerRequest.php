<?php

namespace Lotte\Http\Requests\Utama\Toko;

use Lotte\Http\Requests\Request;

class KustomerRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'kode' => 'required',
            'tmuk_id' => 'required',
            'kota_id' => 'required',
            'kecamatan_id' => 'required',
            // 'pajak_id' => 'required',
            'jeniskustomer_id' => 'required',
            'membercard_id' => 'required',
            // 'kode' => 'required',
            // 'nama_pemilik' => 'required',
            // 'nama_perusahaan' => 'required',
            'alamat' => 'required',
            // 'kode_pos' => 'required',
            // 'longitude' => 'required',
            // 'latitude' => 'required',
            'limit_kredit' => 'required',
            'persen_diskon' => 'required',
            'cara_pembayaran' => 'required',
            'status_kredit' => 'required',
        ];
    }
}
