<?php

namespace Lotte\Http\Requests\Utama\Toko;

use Lotte\Http\Requests\Request;

class VendorLokalTmukRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            // 'kode' => 'required|max:5|min:2|unique:ref_vendor_tmuk,kode'.$ignore,
            'kode' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            // 'provinsi_id' => 'required',
            'provinsi_id' => 'required',
            'kota_id' => 'required',
            'kecamatan_id' => 'required',
            // 'kode_pos' => 'required',
            'telepon' => 'required',
            // 'email' => 'required',
            'top' => 'required',
            'lead_time' => 'required',
            // 'jadwal_order[]' => 'required',
            // 'tmuk_id[]' => 'required',
            // 'group' => 'required|array|exists:groups,id'
            // 'pajak_id' => 'required',
            // 'rekening_escrow_id' => 'required',
            // 'bank_escrow_id' => 'required',
        ];
    }
}

