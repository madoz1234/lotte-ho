<?php

namespace Lotte\Http\Requests\Utama\Toko;

use Lotte\Http\Requests\Request;

class MemberCardRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            // 'nomor' => 'required'.$ignore,
            // 'nama' => 'required'.$ignore,
            // 'telepon' => 'required|max:15'.$ignore,
            // 'email' => 'required|max:30'.$ignore,
            // 'jeniskustomer_id' => 'required'.$ignore,
            // 'nomor' => 'required',
            'nomor' => 'required|unique:ref_membercard,nomor'.$ignore,
            'nama' => 'required',
            // 'telepon' => 'required|max:15',
            // 'email' => 'required|max:30',
            'jeniskustomer_id' => 'required',

            // 'tmuk_kode' => 'required',
            'alamat' => 'required',
            // 'notes' => 'required',
            // 'limit_kredit' => 'required',
        ];
    }
}
