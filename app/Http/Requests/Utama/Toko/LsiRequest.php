<?php

namespace Lotte\Http\Requests\Utama\Toko;

use Lotte\Http\Requests\Request;

class LsiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';
        return [
            'region_id' => 'required',
            'kode' => 'required',
            'kode' => 'required|max:10|min:2|unique:ref_lsi,kode'.$ignore,
            'nama' => 'required',
            'alamat' => 'required',
            'provinsi_id' => 'required',
            'kota_id' => 'required',
            // 'kode_pos' => 'required',
            'telepon' => 'required',
            // 'email' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
            'pajak_id' => 'required',
            'rekening_escrow_id' => 'required',
            'bank_escrow_id' => 'required',
            'pajak_id' => 'required',
        ];
    }
}