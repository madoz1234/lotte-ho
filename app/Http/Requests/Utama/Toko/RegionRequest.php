<?php

namespace Lotte\Http\Requests\Utama\Toko;

use Lotte\Http\Requests\Request;

class RegionRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'kode' => 'required|max:5|min:2|unique:ref_region,kode'.$ignore,
            'area' => 'required|max:50|min:2|unique:ref_region,area'.$ignore,
        ];
    }
}
