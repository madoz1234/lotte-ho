<?php

namespace Lotte\Http\Requests\Utama\Toko;

use Lotte\Http\Requests\Request;

class JenisKustomerRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'kode' => 'required|unique:ref_jeniskustomer,kode'.$ignore,
            'jenis' => 'required|max:30|unique:ref_jeniskustomer,jenis'.$ignore,
        ];
    }
}
