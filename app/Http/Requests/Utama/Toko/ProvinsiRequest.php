<?php

namespace Lotte\Http\Requests\Utama\Toko;

use Lotte\Http\Requests\Request;

class ProvinsiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'nama' => 'required|unique:ref_provinsi,nama'.$ignore,
        ];
    }
}
