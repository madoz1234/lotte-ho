<?php

namespace Lotte\Http\Requests\Hakakses;

use Lotte\Http\Requests\Request;

class HakAksesRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';
        return [
            'name'        => 'required|unique:sys_users,name'.$ignore ,
            'description' => 'required',
        ];
    }
}