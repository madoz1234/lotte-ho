<?php

namespace Lotte\Http\Requests\Hakakses;

use Lotte\Http\Requests\Request;

class PenggunaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->get('_method');
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        if ($method == 'PUT') {
            return [
                'nama_lengkap'  => 'required|unique:ref_pengguna,nama_lengkap'.$ignore ,
                'telepon'       => 'required',
                'email'         => 'required|unique:ref_pengguna,email'.$ignore ,
                'lsi_code'      => 'required'
            ];
        }else{
            return [
                'nama_lengkap'  => 'required|unique:ref_pengguna,nama_lengkap'.$ignore ,
                'telepon'       => 'required',
                'email'         => 'required|unique:ref_pengguna,email'.$ignore ,
                'lsi_code'      => 'required',
                'password'      => 'required|min:6|required_with:konf_password|same:konf_password',
                'konf_password' => 'required|min:6'
            ];
        }
    }
}
// role[]
