<?php

namespace Lotte\Http\Requests\Transaksi;

use Lotte\Http\Requests\Request;

class OpeningTokoRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'nomor_pr' => 'required',
            // 'nomor_pr' => 'required|unique:trans_opening,nomor_pr'.$ignore,
            'tgl_buat' => 'required',
            'tmuk_id' => 'required',
            // 'tmuk_id' => 'required|unique:trans_opening,tmuk_kode'.$ignore,
            'upload_data_produk' => 'required',
        ];
    }
}
