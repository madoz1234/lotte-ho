<?php

namespace Lotte\Http\Requests\Transaksi\Finance;

use Lotte\Http\Requests\Request;

class JurnalManualRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'tanggal' => 'required',
            'tmuk_kode' => 'required',
        ];
    }
}