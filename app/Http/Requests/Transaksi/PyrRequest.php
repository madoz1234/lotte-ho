<?php

namespace Lotte\Http\Requests\Transaksi;

use Lotte\Http\Requests\Request;

class PyrRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'nomor_pyr' => 'required|unique:trans_pyr,nomor_pyr'.$ignore,
            'tgl_jatuh_tempo' => 'required',
            'vendor_lokal' => 'required',
        ];
    }
}
