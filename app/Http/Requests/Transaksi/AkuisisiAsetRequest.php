<?php

namespace Lotte\Http\Requests\Transaksi;

use Lotte\Http\Requests\Request;

class AkuisisiAsetRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $ignore = ($id = $this->get('id')) ? ', '.$this->get('id') : '';

        return [
            'tmuk_kode' => 'required',
            'tipe_id' => 'required',
            'nama' => 'required',
            'no_seri' => 'required',
            'tanggal_pembelian' => 'required',
            'nilai_pembelian' => 'required',
            'kondisi' => 'required',
        ];
    }
}
