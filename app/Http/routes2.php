<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/test', function () {
    // dd('tara');
    $req = [
        'tmuk' => '0600700018',
    ];

   \Lotte\Http\Controllers\API\PingController::sentLastOnline($req);
    // // $tpl = [
    // //         'pendapatan_usaha' => [
    // //             'text'  => 'Pendapatan Usaha',
    // //             'type'  => 'post',
    // //             'value' => 'positive', // positive, negative
    // //             'coa'   => ['4.1.0.0', '4.1.1.0', '4.2.0.0', '4.3.0.0'],
    // //         ],
    // //         'hpp' => [
    // //             'text'  => 'Harga Pokok Penjualan',
    // //             'type'  => 'post',
    // //             'value' => 'positive', // positive, negative
    // //             'coa'   => ['5.1.0.0'],
    // //         ],
    // //         'pendapatan_kotor' => [
    // //             'text' => 'Pendapatan Kotor',
    // //             'type' => 'dec',
    // //             'exp'  => ['pendapatan_usaha', 'hpp'],
    // //         ],
    // // ];
    // // $jurnal = \Lotte\Libraries\JurnalLib::generate('0600700018', '2018-04-01', '2018-07-31', $tpl);
    // // dd($jurnal);
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


Route::get('/', function () {
    return redirect('home');
});

Route::get('/penjualan', function () {
    return view('modules/dashboard/penjualan');
});

Route::get('/finance', function () {
    return view('modules/dashboard/finance');
});


Route::group(['middleware' => 'auth'], function () {

    /* base route */ ///
    Route::group(['prefix' => 'utama', 'namespace' => 'Utama'], function(){
        //Data Toko
        Route::group(['prefix' => 'toko', 'namespace' => 'Toko'], function(){
            //Region (Area)
            Route::post('region/grid', 'RegionController@grid');
            Route::resource('region', 'RegionController');
            //Toko LSI
            Route::get('lsi/on-change-pop-rekening/{id}', 'LsiController@onChangePopRekeningEscrow');
            Route::get('lsi/on-change-pop-bank/{id}', 'LsiController@onChangePopBankEscrow');
            Route::get('lsi/on-change-pop-pajak/{id}', 'LsiController@onChangePopPajak');
            Route::post('lsi/grid', 'LsiController@grid');
            Route::resource('lsi', 'LsiController');
            //Toko member card
            Route::post('member-card/grid', 'MemberCardController@grid');
            Route::resource('member-card', 'MemberCardController');
            //TMUK
            Route::get('tmuk/on-change-pop-rekening/{id}', 'TmukController@onChangePopRekeningEscrow');
            Route::get('tmuk/on-change-pop-rekening-pemilik/{id}', 'TmukController@onChangePopRekeningPemilik');
            Route::get('tmuk/on-change-pop-bank/{id}', 'TmukController@onChangePopBankEscrow');
            Route::get('tmuk/on-change-pop-pajak/{id}', 'TmukController@onChangePopPajak');
            Route::get('tmuk/on-change-pop/{id}', 'TmukController@onChangePop');
            Route::post('tmuk/grid', 'TmukController@grid');
            Route::resource('tmuk', 'TmukController');
            //Member
            Route::get('kustomer/on-change-pop-membercard/{id}', 'KustomerController@onChangePopMemberCard');
            Route::get('kustomer/on-change-pop-pajak/{id}', 'KustomerController@onChangePopPajak');
            Route::get('kustomer/importexcel', 'KustomerController@importexcel');
            Route::post('kustomer/postimportexcel', 'KustomerController@postImportExcel');
            Route::post('kustomer/grid', 'KustomerController@grid');
            Route::resource('kustomer', 'KustomerController');
            //Jenis Kostumer
            Route::post('jenis-kustomer/grid', 'JenisKustomerController@grid');
            Route::resource('jenis-kustomer', 'JenisKustomerController');
            Route::get('jenis-kustomer/detail/{id}', 'JenisKustomerController@detail');
            //KKI
            Route::get('kki/on-change-pop-lsi/{id}', 'KkiController@onChangePopLsi');
            Route::get('kki/on-change-pop-nomor/{id}', 'KkiController@onChangePopNomor');
            Route::post('kki/grid', 'KkiController@grid');
            Route::resource('kki', 'KkiController');
            //Vendor Lokal TMUK
            Route::get('vendor-lokal-tmuk/on-change-pop-rekening/{id}', 'VendorLokalTmukController@onChangePopRekeningEscrow');
            Route::get('vendor-lokal-tmuk/on-change-pop-bank/{id}', 'VendorLokalTmukController@onChangePopBankEscrow');
            Route::get('vendor-lokal-tmuk/on-change-pop-pajak/{id}', 'VendorLokalTmukController@onChangePopPajak');
            Route::post('vendor-lokal-tmuk/grid', 'VendorLokalTmukController@grid');
            Route::resource('vendor-lokal-tmuk', 'VendorLokalTmukController');
        });
        
        //Data Finance
        Route::group(['prefix' => 'finance', 'namespace' => 'Finance'], function(){
            //Bank Escrow
            Route::post('bank-escrow/grid', 'BankEscrowController@grid');
            Route::resource('bank-escrow', 'BankEscrowController');
            //Rekening Escrow
            Route::post('rekening-escrow/grid', 'RekeningEscrowController@grid');
            Route::resource('rekening-escrow', 'RekeningEscrowController');
            //Akun Gl
            Route::resource('akun-gl', 'AkunGlController');
            //Kelas Akun Gl
            Route::resource('kelas-akun', 'KelasAkunController');
            //Akun Group GL
            Route::resource('akun-grup', 'AkunGrupController');
            //kode ayat jurnal
            Route::post('kode-ayat-jurnal/grid', 'KodeAyatJurnalController@grid');
            Route::resource('kode-ayat-jurnal', 'KodeAyatJurnalController');
            Route::post('kode-ayat-jurnal/update', 'KodeAyatJurnalController@update');
            //Pajak
            Route::post('pajak/grid', 'PajakController@grid');
            Route::resource('pajak', 'PajakController');
            //Grup Pajak
            Route::resource('grup-pajak', 'GrupPajakController');

            //Tipe Pajak
            Route::resource('tipe-pajak', 'TipePajakController');

            //Fiscal Year
            Route::post('fiscal-year/grid', 'FiscalYearController@grid');
            Route::resource('fiscal-year', 'FiscalYearController');

            //Tipe Aset
            Route::post('tipe-aset/grid', 'TipeAsetController@grid');
            Route::resource('tipe-aset', 'TipeAsetController');

            // SECTION COA //
            Route::resource('coa', 'CoaController');
            // //Grid COA
            Route::post('coa/grid', 'CoaController@grid');
            Route::post('coa/show-form', 'CoaController@showForm');
            Route::post('coa/grid-coa', 'CoaController@gridCoa');
            Route::get('coa/create/{type}/{parent_kode}', 'CoaController@create');
            Route::get('coa/edit/{type}/{id}', 'CoaController@edit');

            //saldo minimal mengendap
            Route::post('saldo-minimal/grid', 'SaldoMinimalController@grid');
            Route::resource('saldo-minimal', 'SaldoMinimalController');

            //point
            Route::post('point/grid', 'PointController@grid');
            Route::resource('point', 'PointController');
        });

        //Data Rencana Planogram
        Route::group(['prefix' => 'planogram', 'namespace' => 'Planogram'], function(){
            //Rencana Planogram
            Route::get('rencana-planogram/products', 'RencanaPlanogramController@products');
            Route::post('rencana-planogram/products', 'RencanaPlanogramController@productsGrid');
            Route::post('rencana-planogram/grid', 'RencanaPlanogramController@grid');
            Route::post('rencana-planogram/save', 'RencanaPlanogramController@save');
            Route::resource('rencana-planogram', 'RencanaPlanogramController');
        });

        //Data Produk
        Route::group(['prefix' => 'produk', 'namespace' => 'Produk'], function(){
            //Rak
            Route::post('rak/grid', 'RakController@grid');
            Route::resource('rak', 'RakController');
            Route::get('rak/detail/{id}', 'RakController@detail');
            //Rak Per Assortment Type
            Route::resource('rak-assortment', 'RakAssortmentController');
            //Upload Produk
            Route::post('upload-produk/postimportexcel', 'UploadProdukController@postImportExcel');
            Route::get('upload-produk/importexcel', 'UploadProdukController@importexcel');
            Route::get('upload-produk/cekprice/{id}', 'UploadProdukController@cekprice');
            Route::get('upload-produk/cekstatus/{id}', 'UploadProdukController@cekstatus');
            Route::post('upload-produk/grid', 'UploadProdukController@grid');
            Route::resource('upload-produk', 'UploadProdukController');

            //Upload Non-Produk
            Route::post('upload-non-produk/postimportexcel', 'UploadNonProdukController@postImportExcel');
            Route::get('upload-non-produk/importexcel', 'UploadNonProdukController@importexcel');
            Route::post('upload-non-produk/grid', 'UploadNonProdukController@grid');
            Route::resource('upload-non-produk', 'UploadNonProdukController');
            
            //Aktivasi Produk Assortment
            Route::post('aktivasi-produk-assortment/postimportexcel', 'AktivasiProdukAssortmentController@postImportExcel');
            Route::get('aktivasi-produk-assortment/importexcel', 'AktivasiProdukAssortmentController@importexcel');
            Route::post('aktivasi-produk-assortment/grid', 'AktivasiProdukAssortmentController@grid');
            Route::resource('aktivasi-produk-assortment', 'AktivasiProdukAssortmentController');
            
            //Aktivasi Produk TMUK
            Route::post('aktivasi-produk-tmuk/form-aktivasi', 'AktivasiProdukTmukController@aktivasiProduk');
            Route::get('aktivasi-produk-tmuk/form-aktivasi', 'AktivasiProdukTmukController@aktivasiProdukForm');
            Route::post('aktivasi-produk-tmuk/postimportexcel', 'AktivasiProdukTmukController@postImportExcel');
            Route::get('aktivasi-produk-tmuk/importexcel', 'AktivasiProdukTmukController@importexcel');
            Route::post('aktivasi-produk-tmuk/grid', 'AktivasiProdukTmukController@grid');
            Route::resource('aktivasi-produk-tmuk', 'AktivasiProdukTmukController');
            //
            // Route::post('postimportexcel', 'AktivasiProdukTmukController@postImportExcel');
            // Route::get('aktivasi-produk-tmuk/importexcel', 'AktivasiProdukTmukController@importexcel');
            // Route::post('aktivasi-produk-tmuk/grid', 'AktivasiProdukTmukController@grid');
            // Route::resource('aktivasi-produk-tmuk', 'AktivasiProdukTmukController');
            //Kontainer
            Route::post('kontainer/grid', 'KontainerController@grid');
            Route::resource('kontainer', 'KontainerController');
            Route::get('kontainer/detail/{id}', 'KontainerController@detail');
            //Jenis Assortment 
            Route::post('jenis-assortment/save-assortment-produk', 'JenisAssortmentController@saveAssortmentProduk');
            Route::get('jenis-assortment/upload', 'JenisAssortmentController@upload');
            Route::get('jenis-assortment/detail/{id}', 'JenisAssortmentController@detail');
            Route::post('jenis-assortment/grid', 'JenisAssortmentController@grid');
            Route::resource('jenis-assortment', 'JenisAssortmentController');
            //unit uom
            Route::post('unit-uom/grid', 'UnitUomController@grid');
            Route::get('UNIT-UOM/upload', 'UnitUomController@upload');
            Route::resource('unit-uom', 'UnitUomController');
            //Truk
            Route::post('truk/grid', 'TrukController@grid');
            Route::resource('truk', 'TrukController');
            //Harga
            Route::get('harga/generate-form', 'HargaController@generateHargaForm');
            Route::post('harga/generate-form', 'HargaController@generateHarga');
            Route::post('harga/grid', 'HargaController@grid');
            Route::resource('harga', 'HargaController');
            //Kategori
            Route::post('kategori/gridkat4', 'KategoriController@gridKat4');
            Route::post('kategori/gridkat3', 'KategoriController@gridKat3');
            Route::post('kategori/gridkat2', 'KategoriController@gridKat2');
            Route::post('kategori/gridkat1', 'KategoriController@gridKat1');
            Route::post('kategori/griddivisi', 'KategoriController@gridDivisi');
            Route::post('kategori/grid', 'KategoriController@grid');
            Route::resource('kategori', 'KategoriController');
            //Promosi
            Route::get('promosi/on-change-pop-produk/{id}', 'PromosiController@onChangePopProduk');
            Route::get('promosi/on-change-pop-produk-by-kode/{id}/{class}', 'PromosiController@onChangePopProdukByKode');
            Route::post('promosi/grid', 'PromosiController@grid');
            Route::resource('promosi', 'PromosiController');
            //Produk
            Route::post('postimportexcel', 'ProdukController@postImportExcel');
            Route::get('list-produk/on-change-pop-rak/{id}', 'ProdukController@onChangePopRak');
            Route::get('list-produk/edit-non-gmd/{id}', 'ProdukController@editNonGmd');
            Route::get('list-produk/importexcel', 'ProdukController@importexcel');
            Route::post('list-produk/grid', 'ProdukController@grid');
            Route::resource('list-produk', 'ProdukController');
            //Tipe Barang
            Route::get('tipe-barang/detail/{id}', 'TipeBarangController@detail');
            Route::post('tipe-barang/grid', 'TipeBarangController@grid');
            Route::resource('tipe-barang', 'TipeBarangController');
            //Jenis Barang
            Route::post('jenis-barang/grid', 'JenisBarangController@grid');
            Route::resource('jenis-barang', 'JenisBarangController');
            Route::get('jenis-barang/detail/{id}', 'JenisBarangController@detail');
        });

         //Data Pengguna
        Route::group(['prefix' => 'pengguna', 'namespace' => 'Pengguna'], function(){
            //Pengguna
            Route::resource('pengguna', 'PenggunaController');
            //Hak Akses
            Route::resource('hak-akses', 'HakAksesController');
            //Persetujuan
            Route::resource('persetujuan', 'PersetujuanController');
        });
    }); 

    Route::group(['prefix' => 'transaksi', 'namespace' => 'Transaksi'], function(){
        //Data Pembelian
        Route::group(['prefix' => 'pembelian', 'namespace' => 'Pembelian'], function(){
            //Akun Pemesanan Po
            Route::get('pemesanan-po/print-picking-result/{id}', 'PemesananPoController@printPickingResult');
            Route::get('pemesanan-po/print-po-btdk/{id}', 'PemesananPoController@printPoBtdk');
            Route::get('pemesanan-po/print-po-stdk/{id}', 'PemesananPoController@printPoStdk');
            Route::post('pemesanan-po/grid', 'PemesananPoController@grid');
            Route::get('pemesanan-po/detail/{id}', 'PemesananPoController@detail');
            Route::resource('pemesanan-po', 'PemesananPoController');

            //Direct Invoice
            Route::get('direct-invoice/detail/{id}', 'DirectInvoiceController@detail');
            Route::get('direct-invoice/update-status/{id}', 'DirectInvoiceController@onChangePopStatus');
            Route::get('direct-invoice/update-status-tolak/{id}', 'DirectInvoiceController@onChangePopTolak');
            Route::post('direct-invoice/postimportpyrexcel', 'DirectInvoiceController@postImportPyrExcel');
            Route::get('direct-invoice/importpyrexcel', 'DirectInvoiceController@importpyrexcel');
            Route::post('direct-invoice/grid', 'DirectInvoiceController@grid');
            Route::resource('direct-invoice', 'DirectInvoiceController');

            //Reduce Escrow Balance
            Route::get('reduce-escrow-balance/on-change-pop-approval4/{id}', 'ReduceEscrowBalanceController@onChangePopApproval4');
            Route::get('reduce-escrow-balance/on-change-pop-approval3/{id}', 'ReduceEscrowBalanceController@onChangePopApproval3');
            Route::get('reduce-escrow-balance/on-change-pop-approval2/{id}', 'ReduceEscrowBalanceController@onChangePopApproval2');
            Route::get('reduce-escrow-balance/on-change-pop-approval1/{id}', 'ReduceEscrowBalanceController@onChangePopApproval1');
            Route::get('reduce-escrow-balance/on-change-pop-struk/{id}/{struk}', 'ReduceEscrowBalanceController@onChangePopStruk');
            Route::get('reduce-escrow-balance/detail-po/{id}', 'ReduceEscrowBalanceController@detailPo');
            Route::get('reduce-escrow-balance/detail/{id}', 'ReduceEscrowBalanceController@detail');
            Route::get('reduce-escrow-balance/print/{id}', 'ReduceEscrowBalanceController@prints');
            Route::post('reduce-escrow-balance/grid', 'ReduceEscrowBalanceController@grid');
            Route::resource('reduce-escrow-balance', 'ReduceEscrowBalanceController');

            //Top Up Escrow Balance
            Route::get('topup-escrow-balance/detail/{id}', 'TopUpEscrowBalanceController@show');
            Route::get('topup-escrow-balance/tolak/{id}', 'TopUpEscrowBalanceController@tolak');
            Route::get('topup-escrow-balance/verify/{id}', 'TopUpEscrowBalanceController@verify');
            Route::post('topup-escrow-balance/grid', 'TopUpEscrowBalanceController@grid');
            Route::resource('topup-escrow-balance', 'TopUpEscrowBalanceController');

            //Pembayaran Pemasok
            Route::get('pembayaran-pemasok/on-change-pop-setuju/{id}/{biaya}', 'PembayaranPemasokController@onChangePopSetuju');
            Route::get('pembayaran-pemasok/print-pemasok', 'PembayaranPemasokController@printPemasok');
            Route::post('pembayaran-pemasok/grid', 'PembayaranPemasokController@grid');
            Route::resource('pembayaran-pemasok', 'PembayaranPemasokController');
        });

        //Opening Toko
        Route::group(['prefix' => 'openingtoko', 'namespace' => 'OpeningToko'], function(){
            //Opening Toko
            Route::get('opening-toko/print-opening-toko/{id}', 'OpeningTokoController@printOpeningToko');
            Route::get('opening-toko/on-change-pop-tmuk/{id}', 'OpeningTokoController@onChangePopTmuk');
            Route::post('opening-toko/postimportpyrexcel', 'OpeningTokoController@postImportPyrExcel');
            Route::get('opening-toko/importpyrexcel/{id}', 'OpeningTokoController@importpyrexcel');
            Route::post('opening-toko/postimportexcel', 'OpeningTokoController@postImportExcel');
            Route::get('opening-toko/importexcel', 'OpeningTokoController@importexcel');
            Route::get('opening-toko/detail-pyr/{id}', 'OpeningTokoController@detailPyr');
            Route::post('opening-toko/grid', 'OpeningTokoController@grid');
            Route::resource('opening-toko', 'OpeningTokoController');
        });

        //Data Picking
        Route::group(['prefix' => 'picking', 'namespace' => 'Picking'], function(){
            //Picking
            Route::get('list-picking/hasil-pengambilan', 'PickingController@hasilPengambilan');
            Route::get('list-picking/picking-task/{id}', 'PickingController@pickingTask');
            Route::get('list-picking/picking-result/{id}', 'PickingController@pickingResult');
            Route::get('list-picking/detail/{id}', 'PickingController@detail');
            Route::post('list-picking/grid', 'PickingController@grid');
            Route::resource('list-picking', 'PickingController');
        });

        //Data Konfirmasi Retur
        Route::group(['prefix' => 'konfirmasiretur', 'namespace' => 'KonfirmasiRetur'], function(){
            //Konfirmasi Retur
            Route::get('konfirmasi-retur/print-konfirmasi', 'KonfirmasiReturController@printKonfirmasiReturn');
            Route::post('konfirmasi-retur/grid', 'KonfirmasiReturController@grid');
            Route::resource('konfirmasi-retur', 'KonfirmasiReturController');   
        });

        //Data Kubikasi
        Route::group(['prefix' => 'kubikasi', 'namespace' => 'Kubikasi'], function(){
            //Kontainer
            Route::get('kontainer/print-label-kontainer/{id}', 'KontainerController@printLabelKontainer');
            Route::post('kontainer/grid', 'KontainerController@grid');
            Route::get('kontainer/detail/{id}', 'KontainerController@show');
            Route::resource('kontainer', 'KontainerController');
            //Truk
            Route::get('truk/print-daftar-muatan/{id}', 'TrukController@printDaftarMuatan');
            Route::get('truk/print-surat-jalan/{id}', 'TrukController@printSuratJalan');
            Route::post('truk/grid', 'TrukController@grid');
            Route::post('truk/group', 'TrukController@group');
            Route::resource('truk', 'TrukController');
        });

        //Data Rute Pengiriman
        Route::group(['prefix' => 'rutepengiriman', 'namespace' => 'RutePengiriman'], function(){
            //Rute Pengiriman
            Route::post('rute-pengiriman/grid', 'RutePengirimanController@grid');
            Route::get('rute-pengiriman/detail/{id}', 'RutePengirimanController@show');
            Route::get('rute-pengiriman/print/{id}', 'RutePengirimanController@printRute');
            Route::resource('rute-pengiriman', 'RutePengirimanController');
        });

        //Data Member
        Route::group(['prefix' => 'member', 'namespace' => 'Member'], function(){
            //Point
            Route::post('point/grid', 'PointController@grid');
            Route::get('point/detail/{id}', 'PointController@show');
            Route::resource('point', 'PointController');

            Route::post('piutang/grid', 'PiutangController@grid');
            Route::get('piutang/detail/{id}', 'PiutangController@show');
            Route::resource('piutang', 'PiutangController');
        });

        //Data Finance
        Route::group(['prefix' => 'finance', 'namespace' => 'Finance'], function(){
            //Budget Entri
            Route::resource('budget-entri', 'BudgetEntriController');
            //Jurnal Manual
            Route::post('jurnal-manual/grid', 'JurnalManualController@grid');
            Route::resource('jurnal-manual', 'JurnalManualController');
            //Direct Invoice
            Route::get('direct/on-change-pop-po/{id}', 'DirectController@onChangePopPo');
            Route::get('direct/on-change-pop-pyr/{id}', 'DirectController@onChangePopPyr');
            Route::post('direct/postimportexcel2', 'DirectController@postImportExcel2');
            Route::post('direct/postimportexcel1', 'DirectController@postImportExcel1');
            Route::get('direct/importexcel', 'DirectController@importexcel');
            Route::get('direct/importexcel2', 'DirectController@importexcel2');
            Route::get('direct/detail/{id}', 'DirectController@detail');
            Route::post('direct/grid', 'DirectController@grid');
            Route::resource('direct', 'DirectController');
            //Aset
            Route::resource('aset', 'AsetController');
            //Pembatalan Jurnal
            Route::post('pembatalan-jurnal/grid', 'PembatalanJurnalController@grid');
            Route::resource('pembatalan-jurnal', 'PembatalanJurnalController');
            Route::get('pembatalan-jurnal/detail/{idtrans}', 'PembatalanJurnalController@detail');
            Route::get('pembatalan-jurnal/on-change-pop-pembatalan/{id}', 'PembatalanJurnalController@onChangePopPembatalan');
            //Report Jurnal
            Route::resource('report-jurnal', 'ReportJurnalController');
        });

    });

    //Export
    Route::group(['prefix' => 'export', 'namespace' => 'Export'], function () {
        //Export Toko
        Route::post('/export-region', 'ExportController@getRegion');
        Route::post('/export-jenis-kustomer', 'ExportController@getJenisKustomer');
        Route::post('/export-membercard', 'ExportController@getMemberCard');
        Route::post('/export-lsi', 'ExportController@getLsi');
        Route::post('/export-tmuk', 'ExportController@getTmuk');
        Route::post('/export-vendor-tmuk', 'ExportController@getVendorTmuk');
        Route::post('/export-kki', 'ExportController@getKki');
        // Route::post('/tamplate-kki', 'ExportController@getTamplateKki');

        //Export Finance
        Route::post('/export-pajak', 'ExportController@getPajak');
        Route::post('/export-bank-escrow', 'ExportController@getBankEscrow');
        Route::post('/export-rekening-escrow', 'ExportController@getRekeningEscrow');
        Route::post('/export-tahun-fiskal', 'ExportController@getTahunFiskal');
        Route::post('/export-tipe-aset', 'ExportController@getTipeAset');
        Route::post('/export-kontainer', 'ExportController@getKontainer');
        Route::post('/export-truk', 'ExportController@getTruk');
        Route::post('/export-rak', 'ExportController@getRak');
        Route::post('/export-unit-uom', 'ExportController@getUnitUom');
        Route::post('/export-kustomer', 'ExportController@getKustomer');
        Route::post('/type-coa', 'ExportController@getTypeCoa');
        Route::post('/tipe', 'ExportController@getTipe');
        Route::post('/coa1', 'ExportController@getCoa1');
        Route::post('/export-saldo-minimal', 'ExportController@getSaldoMinimal');
        Route::post('/export_point', 'ExportController@getPoint');
        Route::post('/export-konfirmasi-retur', 'ExportController@getKonfirmasiRetur');

        //Export Produk
        Route::post('/tamplate-gmd', 'ExportController@getTamplateGmd');
        Route::post('/tamplate-produk', 'ExportController@getTamplateProduk');
        Route::post('/tamplate-opening-toko', 'ExportController@getTamplateOpeningToko');
        Route::post('/export-assortment-type', 'ExportController@getAssortmentType');
        Route::post('/export-tipe-barang', 'ExportController@getTipeBarang');
        Route::post('/export-jenis-barang', 'ExportController@getJenisBarang');
        Route::post('/export-harga', 'ExportController@getHarga');
        Route::post('/export-produk-assortment', 'ExportController@getProdukAssortment');
        Route::post('/tamplate-produk-assortment', 'ExportController@getTamplateProdukAssortment');
        Route::post('/export-produk-tmuk', 'ExportController@getProdukTmuk');
        Route::post('/tamplate-produk-tmuk', 'ExportController@getTamplateProdukTmuk');
        Route::post('/tamplate-kustomer', 'ExportController@getTamplateKustomer');
        Route::post('/export-upload-produk', 'ExportController@getUploadProduk');
        Route::post('/export-upload-non-produk', 'ExportController@getUploadNonProduk');
        Route::post('/export-produk', 'ExportController@getProduk');
        Route::post('/kat1', 'ExportController@getKat1');
        Route::post('/kat2', 'ExportController@getKat2');
        Route::post('/kat3', 'ExportController@getKat3');
        Route::post('/kat4', 'ExportController@getKat4');
        Route::post('/divisi', 'ExportController@getDivisi');


        Route::post('/tamplate-dpo', 'ExportController@getTamplateDpo');
        Route::post('/tamplate-dpyr', 'ExportController@getTamplateDpyr');

        //Export Transaksi
        Route::post('/export-opening-toko', 'ExportController@getOpeningToko');
        Route::post('/download-opening-toko', 'ExportController@getDownloadOpeningToko');

    });

    Route::group(['prefix' => 'laporan', 'namespace' => 'Laporan'], function(){
        Route::resource('laporan', 'LaporanController');
        Route::get('html2pdf', 'LaporanController@html2pdf');
        Route::post('grid', 'LaporanController@grid');

        //keuangan
        Route::post('grid-laporan-keuangan', 'LaporanKeuanganController@grid');
        Route::post('print-laporan-laporan-keuangan', 'LaporanKeuanganController@printLaporanKeuangan');
        Route::post('exel-laporan-laporan-keuangan', 'LaporanKeuanganController@exelLaporanKeuangan');
        // Route::get('neraca', 'LaporanController@printNeraca');
        // Route::get('neraca-perbandingan-anggaran', 'LaporanController@printNeracaPerbandingan');
        // Route::get('laba-rugi', 'LaporanController@printLabaRugi');
        // Route::get('laba-rugi-perbandingan-anggaran', 'LaporanController@printLamaRugiPerbandinganAnggaran');
        // Route::get('laba-ditahan', 'LaporanController@printLabaDitahan');

        //akun buku besar
        Route::post('grid-akun-buku-besar', 'AkunBukuBesarController@grid');
        Route::post('print-laporan-akun-buku-besar', 'AkunBukuBesarController@printAkunBukuBesar');
        Route::post('exel-laporan-akun-buku-besar', 'AkunBukuBesarController@exelAkunBukuBesar');
        // Route::get('bukti-jurnal-umum', 'LaporanController@printBuktiJurnalUmum');
        // Route::get('daftar-akun', 'LaporanController@printDaftarAkun');
        // Route::get('daftar-history-gl', 'LaporanController@printDaftarHistoryGl');
        // Route::get('keseluruhan-jurnal', 'LaporanController@printKeseluruhanJurnal');
        // Route::get('neraca-saldo', 'LaporanController@printNeracaSaldo');
        // Route::get('ringkasan-buku-besar', 'LaporanController@printRingkasanBukuBesar');


        //akun kas & bank
        Route::post('grid-akun-kas-bank', 'AkunKasBankController@grid');
        Route::post('print-laporan-akun-kas-bank', 'AkunKasBankController@printAkunKasBank');
        Route::post('exel-laporan-akun-kas-bank', 'AkunKasBankController@exelAkunKasBank');
        // Route::get('akun-kas-bank', 'LaporanController@printAkunKasBank');
        // Route::get('arus-kas-per-akun', 'LaporanController@printArusKasPerAkun');

        //akun piutang dan member
        Route::post('grid-akun-piutang-member', 'AkunPiutangMemberController@grid');
        Route::post('print-laporan-akun-piutang-member', 'AkunPiutangMemberController@printAkunPiutangMember');
        Route::post('exel-laporan-akun-piutang-member', 'AkunPiutangMemberController@exelAkunPiutangMember');
        // Route::get('transaksi-belum-lunas', 'LaporanController@printTransaksiBelumLunas');
        // Route::get('laporan-penjualan-tmuk', 'LaporanController@printLaporanPenjualanPersediaan');
        // Route::get('laporan-piutang-pelanggan', 'LaporanController@printLaporanPiutangPelanggan');
        // Route::get('laporan-umur-piutang-pelanggan', 'LaporanController@printLaporanUmurPiutangPelanggan');
        // Route::get('pembayaran-member', 'LaporanController@printPembayaranMember');
        // Route::get('daftar-member', 'LaporanController@printDaftarMember');

        //Print Laporan Penjualan
        Route::post('grid-penjualan', 'PenjualanController@grid');
        Route::post('print-laporan-penjualan', 'PenjualanController@printPenjualan');
        Route::post('exel-laporan-penjualan', 'PenjualanController@exelPenjualan');

        // Route::get('penjualan-bulanan', 'LaporanController@printPenjualanBulanan');
        // Route::get('penjualan-harian', 'LaporanController@printPenjualanHarian');
        // Route::get('penjualan-mingguan', 'LaporanController@printPenjualanMingguan');
        // Route::get('penjualan-per-barang', 'LaporanController@printPernjualanPerBarang');

        //Print Laporan Pembelian
        Route::post('grid-pembelian', 'PembelianController@grid');
        Route::post('print-laporan-pembelian', 'PembelianController@printPembelian');
        Route::post('exel-laporan-pembelian', 'PembelianController@exelPembelian');

        // Route::get('lembar-po-btdk', 'LaporanController@printLembarPoBtdk');
        // Route::post('lembar-po-stdk', 'LaporanController@printLembarPoStdk');
        // Route::get('rekap-po-barang-trade-lsi', 'LaporanController@printRekapPoBarangTradeLsi');
        // Route::get('rekap-po-barang-non-trade-lsi', 'LaporanController@printRekapPoBarangNonTradeLsi');
        // Route::get('rekap-po-barang-trade-vendor-lokal', 'LaporanController@printRekapPoBarangTradeVendorLokal');
        // Route::get('lembar-picking-result', 'LaporanController@printLembarPickingResult');
        // Route::get('lembar-label-kontainer-tmuk', 'LaporanController@printLembarLabelKontainerTmuk');
        // Route::get('lembar-daftar-muatan-tmuk', 'LaporanController@printLembarDaftraMuatanTmuk');
        // Route::get('lembar-surat-jalan-tmuk', 'LaporanController@printLembarSuratJalanTmuk');
        // Route::get('rekap-delivery-tmuk', 'LaporanController@printRekapDeliveryTmuk');
        // Route::get('rekap-grn-tmuk', 'LaporanController@printRekapGrnTmuk');
        // Route::get('lembar-konfirmasi-return-tmuk', 'LaporanController@printLembarKonfirmasiReturnTmuk');
        // Route::get('rekap-sl-lsi-tmuk', 'LaporanController@printRekapSlLsiTmuk');

        //Print Laporan aktiva tetap
        Route::post('grid-aktiva-tetap', 'AktivaTetapController@grid');
        Route::post('print-laporan-aktiva-tetap', 'AktivaTetapController@printAktivaTetap');
        Route::post('exel-laporan-aktiva-tetap', 'AktivaTetapController@exelAktivaTetap');
        // Route::get('aktiva-tetap', 'LaporanController@printAktivaTetap');
        // Route::get('aktiva-tetap-per-tipe', 'LaporanController@printAktivaTetapPerTipe');

        //persediaan
        Route::post('grid-persediaan', 'PersediaanController@grid');
        Route::post('print-laporan-persediaan', 'PersediaanController@printPersediaan');
        Route::post('exel-laporan-persediaan', 'PersediaanController@exelPersediaan');
        // Route::get('rekap-grn-tmuk', 'LaporanController@printPersediaanRekapGrnTmuk');
        // Route::get('rekap-persediaan-tmuk', 'LaporanController@printPersediaanRekapPersediaanTmuk');
        // Route::get('rekap-pergerakan-persediaan-tmuk', 'LaporanController@printRekapPergerakanPersediaanTmuk');
        // Route::get('rekap-so-tmuk', 'LaporanController@printRekapSoTmuk');
        // Route::get('umur-persediaan', 'LaporanController@printUmurPersediaan');
    });

    Route::group(['namespace' => 'Beranda'], function(){
        Route::resource('home','DashboardController');
    });
    

    // Route::controllers([
    //     // Beranda
    //     'home'   => 'HomeController',
    // ]);

    // Route::controllers([
    //     // Beranda
    //     'outstanding'   => 'Beranda\OutstandingController',
    //     'finance'   => 'Beranda\FinanceController',
    //     'lokasi-toko'   => 'Beranda\LokasiTokoController',
    //     'chat'   => 'Beranda\ChatController',
    //     'monitoring'   => 'Beranda\MonitoringController',
    //     'ajax/option' => 'Option\AjaxOptionController',
    // ]);
    Route::post('chat/data', 'Beranda\ChatController@data');
    Route::post('chat', 'Beranda\ChatController@index');

});

// at API
Route::controller('api/ping', 'API\PingController');
Route::post('api/pr/send', 'API\PurchaseRequestController@sendPr');
Route::post('api/produk/sync-produk', 'API\ProdukController@syncProduk');
Route::post('api/produk/sync-harga', 'API\ProdukController@syncHarga');
Route::post('api/tmuk/get-saldo', 'API\TmukController@getSaldo');
Route::post('api/tmuk/get-saldo-escrow', 'API\TmukController@getSaldoEscrow');
Route::get('api/tmuk/get-mengendap', 'API\SaldoMengendapController@getSaldoMengendap');
Route::post('api/po/send-po', 'API\POController@sendPO');
Route::post('api/po/get-stock', 'API\POController@getStock');
Route::get('api/jenis-kustomer/get-kustomer', 'API\JenisKustomerController@getKustomer');
Route::get('api/kustomer/get-kustomer', 'API\KustomerController@getKustomer');
Route::post('api/point/get-point', 'API\PointController@getPoint');
Route::post('api/member/send', 'API\MemberController@sendMember');
Route::post('api/member/send-piutang', 'API\MemberController@sendMemberPiutangSync');
Route::post('api/member/send-point-member', 'API\MemberController@sendMemberPointSync');
Route::post('api/member/get-all', 'API\MemberController@getAll');
Route::post('api/member/send-poin', 'API\MemberController@sendMemberPoint');
Route::post('api/member/send-hutang', 'API\MemberController@sendMemberPiutang');
Route::post('api/transaksi/topup/send', 'API\TopUpController@sendTopup');
Route::post('api/transaksi/topup/send-detail', 'API\TopUpController@sendTopupDetail');
Route::post('api/po/get-po', 'API\POController@getPO');
Route::post('api/po/terima-po', 'API\POController@setTerimaPo');
Route::post('api/tmuk/get-scn', 'API\TmukController@getSaldoSCN');
Route::get('api/tmuk/get-locked', 'API\TmukController@getSaldoLocked');
Route::post('api/hpp/get-hpp', 'API\HPPController@getHpp');
Route::post('api/produk/get-setting', 'API\ProdukController@getSetting');
Route::post('api/produk/get-price', 'API\ProdukController@getPrice');
Route::post('api/pyr/send-pyr', 'API\PyrController@sendPyr');
Route::post('api/tmuk/get-vendor', 'API\TmukController@getVendor');
Route::post('api/topup/get-topup', 'API\TopUpController@getTopup');
Route::post('api/topup/get-topup-deposit', 'API\TopUpController@getTopupDeposit');
Route::post('api/topup/get-topup-year', 'API\TopUpController@getTopupYear');
Route::post('api/topup/get-topup-all', 'API\TopUpController@getTopupAll');
Route::post('api/topup/get-pay-week', 'API\TopUpController@getTopupWeek');
Route::post('api/topup/get-sum-tunai', 'API\TopUpController@getSumTunai');
Route::post('api/topup/get-sum-tunai-all', 'API\TopUpController@getSumTunaiAll');
Route::post('api/topup/get-sum-member', 'API\TopUpController@getSumMember');
Route::post('api/topup/get-pay-member', 'API\TopUpController@getTopupMember');
Route::post('api/tmuk/get-tmuk', 'API\TmukController@getTmuk');
Route::post('api/retur/send-retur', 'API\ReturController@sendRetur');
Route::post('api/pyr/get-pyr', 'API\PyrController@getPyr');
Route::post('api/hpp/get-map', 'API\HPPController@getMap');
Route::post('api/penjualan/sync-end-shift', 'API\PenjualanController@syncEndShift');
Route::post('api/penjualan/sync-penjualan', 'API\PenjualanController@syncPenjualan');
Route::get('api/bank/get-bank', 'API\BankController@getBank');
Route::get('api/divisi/get-divisi', 'API\DivisiController@getDivisi');
Route::get('api/category/get-category', 'API\CategoryController@getCategory');
Route::post('api/produk/get-promosi', 'API\ProdukController@getPromosi');
Route::post('api/pyr/get-pyrstatus', 'API\PyrController@getPyrStatus');
Route::post('api/produk/send-change', 'API\ProdukController@sendChange');
Route::post('api/ping/cek-ping', 'API\PingController@sentLastOnline');