<?php

namespace Lotte\Http\Middleware;

use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('mainMenu', function ($menu) {
            $menu->add('Dashboard', 'dashboard')
                 ->data(['icon'=>'dashboard', 'permission' => auth()->user()? auth()->user()->can('dashboard-'):false]);

            //Menu Utama     
            $menu->add('Menu Utama')
                 ->data(['icon' => 'sidebar', 'permission' => auth()->user()? auth()->user()->can('menu-utama-') :false]);    
            // $menu->add('Menu Utama')
            //      ->data(['icon' => 'sidebar', 'permission' => true]);

                 //Data Finance
                $menu->menuUtama->add('Master Data Finance')
                                ->data(['icon' => 'book', 'permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-') :false]);
                    $menu->masterDataFinance->add('Pajak', 'utama/finance/pajak')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-pajak-access'):false]);
                    $menu->masterDataFinance->add('Bank', 'utama/finance/bank-escrow')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-bank-access'):false]);
                    $menu->masterDataFinance->add('Rekening Bank', 'utama/finance/rekening-escrow')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-rekening-bank-access'):false]);
                    $menu->masterDataFinance->add('COA', 'utama/finance/coa')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-coa-access'):false]);
                    $menu->masterDataFinance->add('Tahun Fiskal', 'utama/finance/fiscal-year')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-tahun-fisikal-access'):false]);
                    $menu->masterDataFinance->add('Tipe Aset Terdepresiasi', 'utama/finance/tipe-aset')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-tipe-asset-terdepresiasi-access'):false]);
                    // $menu->masterDataFinance->add('Kode Ayat Jurnal', 'utama/finance/kode-ayat-jurnal');
                    $menu->masterDataFinance->add('Saldo Minimal Mengendap', 'utama/finance/saldo-minimal')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-saldo-minimal-mengendap-access'):false]);
                    $menu->masterDataFinance->add('Point', 'utama/finance/point')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-point-access'):false]);
                    // $menu->masterDataFinance->add('Akuisisi Aset', 'transaksi/finance/aset');

                   
                    // $menu->dataFinance->add('Akun GL', 'utama/finance/akun-gl');
                    // $menu->dataFinance->add('Akun Grup GL', 'utama/finance/akun-grup');
                    // $menu->dataFinance->add('Akun Kelas GL', 'utama/finance/kelas-akun');
                    // $menu->dataFinance->add('COA 1 - Kelas GL', 'utama/finance/kelas-akun');
                    // $menu->dataFinance->add('COA 2 - Grup GL', 'utama/finance/akun-grup');
                    // $menu->dataFinance->add('COA 3 - Akun GL', 'utama/finance/akun-gl');
                    // $menu->dataFinance->add('Grup Pajak', 'utama/finance/grup-pajak');
                    // $menu->dataFinance->add('Tipe Pajak Barang', 'utama/finance/tipe-pajak');
                    
                    // $menu->dataFinance->add('COA', 'utama/finance/coa');
                    // $menu->dataFinance->add('Kode Ayat Jurnal', 'utama/finance/pengaturan-gl');
                    // $menu->dataFinance->add('Pajak', 'utama/finance/pajak');
                    // $menu->dataFinance->add('Grup Pajak', 'utama/finance/grup-pajak');
                    // $menu->dataFinance->add('Tipe Pajak Barang', 'utama/finance/tipe-pajak');
                    // $menu->dataFinance->add('Tahun Fiskal', 'utama/finance/fiscal-year');
                    // $menu->dataFinance->add('Tipe Aset', 'utama/finance/tipe-aset');
                    // $menu->dataFinance->add('Entri Aset', 'utama/finance/aset');
                //Data Produk
                $menu->menuUtama->add('Master Data Produk')
                                ->data(['icon' =>'cubes', 'permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-'):false]);
                    $menu->masterDataProduk->add('Assortment Type', 'utama/produk/jenis-assortment')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-point-access'):false]);
                    $menu->masterDataProduk->add('Rak', 'utama/produk/rak')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-point-access'):false]);
                    // $menu->masterDataProduk->add('Rak by Assortment Type', 'utama/produk/rak-assortment')
                    //       ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-finance-point-access'):false]);
                    $menu->masterDataProduk->add('Tipe Barang', 'utama/produk/tipe-barang')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-tipe-barang-access'):false]);
                    $menu->masterDataProduk->add('Jenis Barang', 'utama/produk/jenis-barang')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-jenis-barang-access'):false]);
                    $menu->masterDataProduk->add('Unit UoM', 'utama/produk/unit-uom')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-unit-uom-access'):false]);
                    $menu->masterDataProduk->add('Upload Produk GMD', 'utama/produk/upload-produk')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-upload-produk-gmd-access'):false]);
                    $menu->masterDataProduk->add('Upload Produk Non GMD', 'utama/produk/upload-non-produk')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-upload-produk-non-gmd-access'):false]);
                    $menu->masterDataProduk->add('Produk', 'utama/produk/list-produk')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-produk-access'):false]);
                    $menu->masterDataProduk->add('Aktivasi Produk Assortment Type', 'utama/produk/aktivasi-produk-assortment')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-aktivasi-produk-assortment-type-access'):false]);
                    $menu->masterDataProduk->add('Aktivasi Produk TMUK', 'utama/produk/aktivasi-produk-tmuk')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-aktivasi-produk-tmuk-access'):false]);
                    $menu->masterDataProduk->add('Harga', 'utama/produk/harga')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-harga-access'):false]);
                    $menu->masterDataProduk->add('Kategori Produk', 'utama/produk/kategori')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-kategori-produk-access'):false]);
                    $menu->masterDataProduk->add('Kontainer', 'utama/produk/kontainer')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-kontainer-access'):false]);
                    $menu->masterDataProduk->add('Truk', 'utama/produk/truk')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-truk-access'):false]);
                    $menu->masterDataProduk->add('Promosi', 'utama/produk/promosi')
                            ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-promosi-access'):false]);

                //Data Toko
                $menu->menuUtama->add('Master Data Toko')
                                ->data(['icon' => 'shop', 'permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-'):false]);
                    $menu->masterDataToko->add('Region', 'utama/toko/region')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-region-access'):false]);

                    $menu->masterDataToko->add('Provinsi', 'utama/toko/provinsi')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-provinsi-access'):false]);
                    $menu->masterDataToko->add('Kota', 'utama/toko/kota')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-kota-access'):false]);
                    $menu->masterDataToko->add('Kecamatan', 'utama/toko/kecamatan')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-kecamatan-access'):false]);
                        

                    $menu->masterDataToko->add('LSI', 'utama/toko/lsi')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-lsi-access'):false]);
                    // $menu->masterDataToko->add('Jenis Kustomer', 'utama/toko/jenis-kustomer')
                        // ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-tmuk-access'):false]);
                    $menu->masterDataToko->add('TMUK', 'utama/toko/tmuk')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-promosi-access'):false]);
                    $menu->masterDataToko->add('Vendor Lokal TMUK', 'utama/toko/vendor-lokal-tmuk')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-vendor-lokal-tmuk-access'):false]);
                    $menu->masterDataToko->add('KKI TMUK','utama/toko/kki/')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-kki-tmuk-access'):false]);
                    // $menu->masterDataToko->add('Vendor Lokal TMUK', 'utama/toko/supplier')
                        //->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-produk-promosi-access'):false]);
                    $menu->masterDataToko->add('Jenis Kustomer', 'utama/toko/jenis-kustomer')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-jenis-kustomer-access'):false]);
                    $menu->masterDataToko->add('Kartu Anggota', 'utama/toko/member-card')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-kartu-anggota-access'):false]);
                    $menu->masterDataToko->add('Kustomer', 'utama/toko/kustomer')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-toko-kustomer-access'):false]);
                
                //Data Pengguna
                $menu->menuUtama->add('Master Data Pengguna  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
                                ->data(['icon' => 'users', 'permission' => auth()->user()? auth()->user()->can('menu-utama-master-pengguna-'):false])
                                ->nickname('users');
                    $menu->users->add('Hak Akses', 'utama/pengguna/hak-akses')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-pengguna-hak-akses-access'):false]);
                    // $menu->users->add('Hak Akses', 'utama/pengguna/hak-akses')
                    //     ->data(['permission' => true]);
                    $menu->users->add('Pengguna', 'utama/pengguna/pengguna')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-pengguna-pengguna-access'):false]);
                    $menu->users->add('Persetujuan Escrow', 'utama/pengguna/persetujuan')
                        ->data(['permission' => auth()->user()? auth()->user()->can('menu-utama-master-pengguna-persetujuan-escrow-access'):false]);
                    

                //Planogram
                $menu->menuUtama->add('Planogram', 'utama/planogram/rencana-planogram')
                                ->data(['icon' => 'inbox', 'permission' => auth()->user()? auth()->user()->can('menu-utama-planogram-'):false]);

                

            //Menu Transaksi
            $menu->add('Transaksi')
                 ->data(['icon' => 'exchange', 'permission' => auth()->user()? auth()->user()->can('transaksi-'):false]);
            //Data Pengambilan
            // $menu->transaksi->add('Opening Toko  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', 'transaksi/opening-toko')
            //                 ->data('icon', 'shopping bag icon')
            //                 ->nickname('opening-toko');
            //opening toko
            $menu->transaksi->add('Opening Toko', 'transaksi/openingtoko/opening-toko')
                            ->data(['icon' => 'shopping bag icon', 'permission' => auth()->user()? auth()->user()->can('transaksi-opening-toko-'):false])
                            ->nickname('opening-toko');

            //Data Pengambilan
            $menu->transaksi->add('Picking  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', 'transaksi/picking/list-picking')
                            ->data(['icon' => 'in cart', 'permission' => auth()->user()? auth()->user()->can('transaksi-picking-'):false])
                            ->nickname('picking');

            //Data Pemesanan Pembelian (PO)
            $menu->transaksi->add('Pembelian')
                            ->data(['icon' => 'file text outline', 'permission' => auth()->user()? auth()->user()->can('transaksi-pembelian-'):false])
                            ->nickname('po');
                $menu->po->add('Purchase Order (PO)', 'transaksi/pembelian/pemesanan-po')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-pembelian-purchase-order-access'):false]);
                $menu->po->add('Reduce Escrow Balance', 'transaksi/pembelian/reduce-escrow-balance')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-pembelian-reduce-escrow-balance-access'):false]);
                
                $menu->po->add('Payment Request (PyR)', 'transaksi/pembelian/direct-invoice')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-pembelian-payment-request-access'):false]);
                $menu->po->add('Pembayaran PyR', 'transaksi/pembelian/pembayaran-pemasok')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-pembelian-pembayaran-pyr-access'):false]);
                
                

            

            //Data Kubikasi
            $menu->transaksi->add('Kubikasi')
                            ->data(['icon' => 'shipping', 'permission'=> auth()->user()? auth()->user()->can('transaksi-kubikasi-'):false])
                            ->nickname('transaksiKubikasi');
                $menu->transaksiKubikasi->add('Kontainer', 'transaksi/kubikasi/kontainer')
                        ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-kubikasi-kontainer-access'):false]);
                $menu->transaksiKubikasi->add('Truk', 'transaksi/kubikasi/truk')
                        ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-kubikasi-truk-access'):false]);

            //Data Rute & Pengiriman
            $menu->transaksi->add('Rute & Pengiriman', 'transaksi/rutepengiriman/rute-pengiriman')
                            ->data(['icon' => 'map ]o, utline', 'permission' => auth()->user()? auth()->user()->can('transaksi-rute-pengiriman-'):false])
                            ->nickname('shipping');

            //Data Konfirmasi Retur
            $menu->transaksi->add('Konfirmasi Retur', 'transaksi/konfirmasiretur/konfirmasi-retur')
                            ->data(['icon' => 'reply', 'permission' => auth()->user()? auth()->user()->can('transaksi-konfirmasi-retur-'):false]);

            //Data Member
            $menu->transaksi->add('Member')
                            ->data(['icon' => 'user', 'permission' => auth()->user()? auth()->user()->can('transaksi-member-'):false])
                            ->nickname('transaksiMember');
                $menu->transaksiMember->add('Point', 'transaksi/member/point')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-member-point-access'):false]);
                $menu->transaksiMember->add('Piutang', 'transaksi/member/piutang')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-member-piutang-access'):false]);


            //Data Finance
            $menu->transaksi->add('Finance')
                            ->data(['icon' => 'money', 'permission' => auth()->user()? auth()->user()->can('transaksi-finance-'):false])
                            ->nickname('transaksiFinance');
                
                $menu->transaksiFinance->add('Top Up Escrow Balance', 'transaksi/pembelian/topup-escrow-balance')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-finance-top-up-escrow-balance-access'):false]);
                $menu->transaksiFinance->add('Direct Invoice', 'transaksi/finance/direct/')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-finance-direct-invoice-access'):false]);
                $menu->transaksiFinance->add('Jurnal Manual', 'transaksi/finance/jurnal-manual/')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-finance-jurnal-manual-access'):false]);
                $menu->transaksiFinance->add('Pembatalan Jurnal (Void)','transaksi/finance/pembatalan-jurnal/')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-finance-pembatalan-jurnal-access'):false]);
                $menu->transaksiFinance->add('Report Jurnal', 'transaksi/finance/report-jurnal/')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-finance-report-jurnal-access'):false]);

            //Data Aset
            $menu->transaksi->add('Aset')
                    ->data(['icon' => 'file', 'permission' => auth()->user()? auth()->user()->can('transaksi-aset-'):false])
                    ->nickname('transaksiAset');
                
                // $menu->transaksiAset->add('Top Up Escrow Balance', 'transaksi/pembelian/topup-escrow-balance');
                $menu->transaksiAset->add('Akuisisi Aset', 'transaksi/finance/aset')
                    ->data(['permission' => auth()->user()? auth()->user()->can('transaksi-aset-akuisisi-aset-access'):false]);

            //Menu Transaksi
            $menu->add('Laporan')
                 ->data(['icon' => 'folder open', 'permission' => auth()->user()? auth()->user()->can('laporan-rincian-'):false])
                 ->nickname('laporan');

            //Rincian
            $menu->laporan->add('Laporan Pusat')
                                 ->data(['icon' => 'list', 'permission' => auth()->user()? auth()->user()->can('laporan-rincian-rincian-'):false])
                                 ->nickname('LaporanPusat');
                // $menu->LaporanPusat->add('Pergerakan Persediaan')
                //     ->data(['permission' => auth()->user()? auth()->user()->can('laporan-rincian-rincian-pergerakan-persediaan-access'):false]);
                // $menu->LaporanPusat->add('Status Persediaan')
                //     ->data(['permission' => auth()->user()? auth()->user()->can('laporan-rincian-rincian-status-persediaan-access'):false]);
                // $menu->LaporanPusat->add('Suplier Inquiry')
                //     ->data(['permission' => auth()->user()? auth()->user()->can('laporan-rincian-rincian-suplier-inquiry-access'):false]);
                // $menu->LaporanPusat->add('Rekening Bank')
                //     ->data(['permission' => auth()->user()? auth()->user()->can('laporan-rincian-rincian-rekening-bank-access'):false]);
                //     ->data(['permission' => auth()->user()? auth()->user()->can('laporan-rincian-rincian-report-jurnal-access'):false]);
                // $menu->LaporanPusat->add('Rincian Neraca')
                //     ->data(['permission' => auth()->user()? auth()->user()->can('laporan-rincian-rincian-neraca-access'):false]);
                // $menu->LaporanPusat->add('Rincian Laba Rugi')

                //report jurnal sementara di hide
                // $menu->LaporanPusat->add('Report Jurnal', 'transaksi/finance/report-jurnal/')->data(['permission' => auth()->user()? auth()->user()->can('laporan-rincian-rincian-rincian-laba-rugi-access'):false]);
                
                $menu->LaporanPusat->add('Aktiva Tetap', 'laporan-pusat/index-home/aktiva-tetap')->data(['permission' => true]);
                $menu->LaporanPusat->add('Akun Buku Besar', 'laporan-pusat/index-home/akun-buku-besar')->data(['permission' => true]);
                $menu->LaporanPusat->add('Akun Kas Bank', 'laporan-pusat/index-home/akun-kas-bank')->data(['permission' => true]);
                $menu->LaporanPusat->add('Akun Utang Usaha', 'laporan-pusat/index-home/akun-utang-usaha')->data(['permission' => true]);
                $menu->LaporanPusat->add('Akun Piutang Member', 'laporan-pusat/index-home/akun-piutang-member')->data(['permission' => true]);
                $menu->LaporanPusat->add('keuangan', 'laporan-pusat/index-home/keuangan')->data(['permission' => true]);
                $menu->LaporanPusat->add('Pembelian', 'laporan-pusat/index-home/pembelian')->data(['permission' => true]);
                $menu->LaporanPusat->add('Penjualan', 'laporan-pusat/index-home/penjualan')->data(['permission' => true]);
                $menu->LaporanPusat->add('Persediaan', 'laporan-pusat/index-home/persediaan')->data(['permission' => true]);

                $menu->LaporanPusat->add('Rekap Akun Escrow','rincian/akun-escrow')
                    ->data(['permission' => true]);
            //Laporan
            $menu->laporan->add('Laporan TMUK', 'laporan/laporan')
                                 ->data(['icon' => 'file text outline', 'permission' => auth()->user()? auth()->user()->can('laporan-rincian-laporan-'):false])
                                 ->nickname('Laporan');
            // laporan log audit
            $menu->laporan->add('Laporan Audit', 'laporan/laporan-audit')
                                 ->data(['icon' => 'file alternate', 'permission' => auth()->user()? auth()->user()->can('laporan-rincian-laporan-'):false])
                                 ->nickname('LaporanAudit');

                // $menu->Laporan->add('Penjualan'); 
                // $menu->Laporan->add('Pembelian');
                // $menu->Laporan->add('Persediaan');
                // $menu->Laporan->add('Laporan Kantor Pusat');
                // $menu->Laporan->add('Keuangan');
                // $menu->LaporanAudit->add('LaporanAudit');
        });



        return $next($request);
    }
}
